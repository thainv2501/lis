export const generateMeetingId = () => {
  const characters = 'abcdefghijklmnopqrstuvwxyz';
  let randomId = '';
  for (let i = 0; i < 10; i++) {
    const randomIndex = Math.floor(Math.random() * characters.length);
    randomId += characters[randomIndex];
  }
  return `${randomId.substring(0, 3)}-${randomId.substring(
    3,
    7
  )}-${randomId.substring(7, 10)}`;
};
