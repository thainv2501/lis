import {
  Box,
  Stack,
  CircularProgress,
  Typography,
  Button,
  Zoom,
} from "@mui/material";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import GppBadIcon from "@mui/icons-material/GppBad";
import { React, useEffect } from "react";
import { Link, useSearchParams } from "react-router-dom";
import { useVerifyEmailMutation } from "../features/auth/authApiSlice";

const VerifyEmail = () => {
  const [verifyEmail, { isLoading, isError }] = useVerifyEmailMutation();
  const [searchParams, setSearchParams] = useSearchParams();

  const email = searchParams.get("email");
  const token = searchParams.get("token");

  useEffect(() => {
    verifyEmail({ email, token });
  }, []);

  let content = (
    <Box
      sx={{
        minHeight: "100vh",
        background: "linear-gradient(to right bottom, #430089, #82ffa1)",
      }}
      display={"flex"}
      flexDirection={"column"}
      justifyContent={"center"}
      justifyItems={"center"}
      alignContent={"center"}
      alignItems={"center"}
    >
      <Box
        sx={{ minWidth: 500, boxShadow: 3, p: 3, borderRadius: 2 }}
        display={"flex"}
        gap={3}
        flexDirection={"column"}
        justifyContent={"center"}
        justifyItems={"center"}
        alignContent={"center"}
        alignItems={"center"}
      >
        <Typography variant="h4" component="div">
          {isLoading ? "Verifying your email !" : "Verified Successful !"}
        </Typography>
        {isLoading ? (
          <Stack sx={{ color: "grey.500" }} spacing={2} direction="row">
            <CircularProgress />
          </Stack>
        ) : (
          <Zoom in={!isLoading}>
            {<CheckCircleOutlineIcon fontSize="large" />}
          </Zoom>
        )}
        {isLoading ? null : (
          <Link to={"/login"}>
            <Button size="small">Login</Button>
          </Link>
        )}
      </Box>
    </Box>
  );

  if (isError) {
    content = (
      <Box
        sx={{ minHeight: "100vh" }}
        display={"flex"}
        flexDirection={"column"}
        justifyContent={"center"}
        justifyItems={"center"}
        alignContent={"center"}
        alignItems={"center"}
      >
        <Box
          sx={{ minWidth: 500, boxShadow: 3, p: 3, borderRadius: 2 }}
          display={"flex"}
          gap={3}
          flexDirection={"column"}
          justifyContent={"center"}
          justifyItems={"center"}
          alignContent={"center"}
          alignItems={"center"}
        >
          <Typography variant="h4" component="div" color="red">
            Verified fail ! Try again !
          </Typography>
          <Zoom in={isError}>
            {<GppBadIcon fontSize="large" color="error" />}
          </Zoom>
        </Box>
      </Box>
    );
  }
  return content;
};

export default VerifyEmail;
