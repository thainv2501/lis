import { React } from "react";
import { Link } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Button } from "@mui/material";

const defaultTheme = createTheme();
const Unauthorized = () => {
  <ThemeProvider theme={defaultTheme}>
    <Grid container component="main" sx={{ height: "100vh" }}>
      <CssBaseline />
      <Grid
        item
        xs={false}
        sm={12}
        sx={{
          backgroundImage: "url(https://picsum.photos/1920/1080)",
          backgroundRepeat: "no-repeat",
          backgroundColor: (t) =>
            t.palette.mode === "light"
              ? t.palette.grey[50]
              : t.palette.grey[900],
          backgroundSize: "cover",
          backgroundPosition: "center",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          justifyItems: "center",
          gap: 4,
        }}
      >
        <Typography variant="h4" color={"red"}>
          Unauthorized ! Please Login again !
        </Typography>
        <Link to={"/login"}>
          <Button variant="contained">Login</Button>
        </Link>
      </Grid>
    </Grid>
  </ThemeProvider>;
};

export default Unauthorized;
