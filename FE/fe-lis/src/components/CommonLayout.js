import AdminPanelSettingsIcon from '@mui/icons-material/AdminPanelSettings';
import Logout from '@mui/icons-material/Logout';
import { Button, Tooltip } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import Divider from '@mui/material/Divider';
import ListItemIcon from '@mui/material/ListItemIcon';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import React, { useEffect, useState } from 'react';
import {
  FaBars,
  FaHome,
  FaQuestionCircle,
  FaRegCalendarAlt,
  FaRegUser,
  FaRocketchat,
  FaUserCircle,
  FaAngleDown,
  FaAngleLeft,
  FaVideo,
} from 'react-icons/fa';
import { ChevronRight } from '@mui/icons-material';
import { NavLink, Outlet, Link, useNavigate } from 'react-router-dom';
import '../public/commonLayout.css';
import useAuth from '../hooks/useAuth';
import bigLogo from '../images/big_logo.jpg';
import { useSendLogoutMutation } from '../features/auth/authApiSlice';
const CommonLayout = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isQuestionsExpanded, setIsQuestionsExpanded] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const toggleQuestions = () => {
    setIsQuestionsExpanded(!isQuestionsExpanded);
  };

  const navigate = useNavigate();

  const { userLogin } = useAuth();
  const userName = userLogin?.fullName;

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const [sendLogout, { isSuccess }] = useSendLogoutMutation();

  useEffect(() => {
    if (isSuccess) navigate('/login');
  }, [isSuccess, navigate]);

  const menuItem = [
    {
      path: '/common/courses-list',
      name: 'Home',
      icon: <FaHome />,
    },
    {
      path: '/common/training-slot',
      name: 'My Classes',
      icon: <FaRegCalendarAlt />,
    },
    {
      path: '/common/support-meetings',
      name: 'Live Meetings',
      icon: <FaVideo />,
    },
    {
      path: '/common/support-questions',
      name: 'Questions',
      icon: <FaQuestionCircle />,
      subItems: [
        {
          path: '/common/class-support?type=support-question',
          name: 'Class Support',
          icon: <ChevronRight />,
        },
        {
          path: '/common/subject-question?type=subject-question',
          name: 'Subjects Support',
          icon: <ChevronRight />,
        },
      ],
    },
  ];
  return (
    <div className="main">
      <div
        style={{ width: isOpen ? '250px' : '80px' }}
        className="sidebar-root"
      >
        <div className="sidebar-container">
          <div className="logo">
            <NavLink className="logo-link" to="/common/courses-list">
              <img className="logo-img" src={bigLogo} alt="LIS" />
            </NavLink>
          </div>
          <div className="info">
            <Button className="info-link" onClick={handleClick} fullWidth>
              <span className="info-icon">
                <FaUserCircle />
              </span>
              <span
                style={{
                  display: isOpen ? 'block' : 'none',
                  textOverflow: 'ellipsis',
                }}
              >
                {userName}
              </span>
            </Button>
            <Menu
              anchorEl={anchorEl}
              id="account-menu"
              open={open}
              onClose={handleClose}
              onClick={handleClose}
              PaperProps={{
                elevation: 0,
                sx: {
                  overflow: 'visible',
                  filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                  mt: 1.5,
                  '& .MuiAvatar-root': {
                    width: 32,
                    height: 32,
                    ml: -0.5,
                    mr: 1,
                  },
                  '&:before': {
                    content: '""',
                    display: 'block',
                    position: 'absolute',
                    top: 0,
                    right: 14,
                    width: 10,
                    height: 10,
                    bgcolor: 'background.paper',
                    transform: 'translateY(-50%) rotate(45deg)',
                    zIndex: 0,
                  },
                },
              }}
              transformOrigin={{ horizontal: 'right', vertical: 'top' }}
              anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
              <Link to={`user-details/${userLogin._id}`}>
                <MenuItem>
                  <Avatar /> Profile
                </MenuItem>
              </Link>

              <Divider />
              <MenuItem onClick={() => sendLogout()}>
                <ListItemIcon>
                  <Logout fontSize="small" />
                </ListItemIcon>
                Logout
              </MenuItem>
            </Menu>
          </div>
          <div className="bars">
            <FaBars onClick={toggle} />
          </div>

          <nav className="nav-menu-root">
            <ul className="menu-list">
              {menuItem.map((item, index) => (
                <li className="menu-item menu-subItem" key={index}>
                  {item.subItems ? ( // If this item has sub items, it's expandable
                    <Tooltip title={item.name} arrow placement="right">
                      <div
                        className="menu-item-link"
                        onClick={
                          item.name === 'Questions' ? toggleQuestions : null
                        }
                      >
                        <span className="menu-item-icon">{item.icon}</span>
                        <span className="menu-item-text">{item.name}</span>
                        {item.name === 'Questions' && isOpen && (
                          <span className="menu-item-arrow">
                            {isQuestionsExpanded ? (
                              <FaAngleDown />
                            ) : (
                              <FaAngleLeft />
                            )}
                          </span>
                        )}
                      </div>
                    </Tooltip>
                  ) : (
                    // Otherwise, it's a normal link
                    <Tooltip title={item.name} arrow placement="right">
                      <Link to={item.path} className="menu-item-link">
                        <span className="menu-item-icon">{item.icon}</span>
                        <span className="menu-item-text">{item.name}</span>
                      </Link>
                    </Tooltip>
                  )}

                  {item.subItems &&
                    item.name === 'Questions' &&
                    isQuestionsExpanded && ( // Render the expanded sub items if applicable
                      <ul className="sub-menu-list">
                        {item.subItems.map((subItem, subIndex) => (
                          <li className="sub-menu-item" key={subIndex}>
                            <Link to={subItem.path} className="menu-item-link">
                              <span className="menu-item-icon menu-subItem-icon">
                                {subItem.icon}
                              </span>{' '}
                              <span className="menu-item-text menu-subItem-text">
                                {subItem.name}
                              </span>
                            </Link>
                          </li>
                        ))}
                      </ul>
                    )}
                </li>
              ))}
              {userLogin.roles.includes('Admin') ? (
                <li className="menu-item">
                  <Tooltip title={'Admin Mode'} arrow placement="right">
                    <Link
                      to={'/dash-board/dashboard-infor'}
                      className="menu-item-link"
                    >
                      <span className="menu-item-icon">
                        {<AdminPanelSettingsIcon />}
                      </span>
                      <span className="menu-item-text">
                        {'Admin Dash Board'}
                      </span>
                    </Link>
                  </Tooltip>
                </li>
              ) : null}
            </ul>
          </nav>
        </div>
      </div>
      <Outlet />
    </div>
  );
};

export default CommonLayout;
