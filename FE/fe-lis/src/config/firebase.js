// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCHxbgL_WFJEkB17RAfIPxNnCgPKTpWTNE",
  authDomain: "learning-interaction-systems.firebaseapp.com",
  projectId: "learning-interaction-systems",
  storageBucket: "learning-interaction-systems.appspot.com",
  messagingSenderId: "152151116907",
  appId: "1:152151116907:web:5ce96533ae7716d199b52e",
  measurementId: "G-58BDZDKQKR",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const storage = getStorage(app);
