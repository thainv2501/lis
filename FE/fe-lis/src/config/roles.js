export const ROLES = {
  Guest: 'Guest',
  Admin: 'Admin',
  Trainer: 'Trainer',
  Trainee: 'Trainee',
};
export const adminRole = ['Admin'];
