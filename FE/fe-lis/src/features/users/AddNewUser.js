import AddIcon from "@mui/icons-material/Add";
import {
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Paper,
} from "@mui/material";
import Chip from "@mui/material/Chip";
import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import { useSnackbar } from "notistack";
import { React, useState, useEffect } from "react";
import Title from "../../components/Title";
import { useAddNewUserMutation } from "./usersApiSlice";

const EMAIL_REGEX = /(|^)[\w\d._%+-]+@(?:[\w\d-]+\.)+(\w{2,})(|$)/i;
const FULL_NAME_REGEX =
  /^[ A-Za-z0-9À-Ỹà-ỹĂ-Ắă-ằẤ-Ứấ-ứÂ-Ấâ-ấĨ-Ỹĩ-ỹĐđÊ-Ểê-ểÔ-Ốô-ốơ-ởƠ-Ớơ-ớƯ-Ứư-ứỲ-Ỵỳ-ỵ\s]+$/;
const PHONE_NUMBER_REGEX = /^[0-9]{10}$/;

const AddNewUser = ({ roles, handleCloseAddDialog, refetch }) => {
  const [openRolesDialog, setOpenRolesDialog] = useState(false);
  const [email, setEmail] = useState("");
  const [status, setStatus] = useState(true);
  const [fullName, setFullName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [note, setNote] = useState("");
  const [userRoles, setUserRoles] = useState([]);
  let rolesCode = [];
  roles.forEach((role) => {
    rolesCode.push(role?.code);
  });

  const { enqueueSnackbar } = useSnackbar();

  const [
    createNewUser,
    {
      isSuccess: createNewUserIsSuccess,
      isError: createNewUserIsError,
      error: createNewUserError,
    },
  ] = useAddNewUserMutation();

  const handleFullNameInput = (e) => {
    setFullName(e.target.value);
  };
  const handlePhoneNumberChange = (e) => {
    setPhoneNumber(e.target.value);
  };

  const handleDelete = (roleToDelete) => {
    setUserRoles((roles) => roles.filter((role) => role !== roleToDelete));
  };
  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const handleCreateClick = async () => {
    try {
      await createNewUser({ email, fullName, phoneNumber, roles: userRoles });
    } catch (error) {
      console.log(" Create Fail : " + error);
    }
  };

  useEffect(() => {
    if (createNewUserIsSuccess) {
      enqueueSnackbar("Create new user : " + email + " is succesful !", {
        variant: "success",
      });
      handleCloseAddDialog();
      refetch();
    }
    if (createNewUserIsError) {
      enqueueSnackbar(
        "Create new user fail : " + createNewUserError?.data?.message,
        {
          variant: "error",
        }
      );
    }
  }, [createNewUserIsError, createNewUserIsSuccess]);

  const handleCloseRolesDialog = () => {
    setOpenRolesDialog(false);
  };
  const handleOpenRolesDialog = () => {
    setOpenRolesDialog(true);
  };
  const handleAddRole = (role) => {
    if (!userRoles.includes(role)) setUserRoles((prev) => [...prev, role]);
    setOpenRolesDialog(false);
  };

  const canCreate =
    EMAIL_REGEX.test(email) &&
    FULL_NAME_REGEX.test(fullName) &&
    PHONE_NUMBER_REGEX.test(phoneNumber) &&
    userRoles?.length !== 0;

  return (
    <Paper
      sx={{
        p: 5,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        gap: 2,
      }}
    >
      <Title>New User</Title>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} mb={2}>
          <TextField
            required
            id="fullName"
            name="fullName"
            label="Full name"
            fullWidth
            value={fullName}
            helperText={
              FULL_NAME_REGEX.test(fullName) ? "" : "Invalid Full Name"
            }
            onChange={handleFullNameInput}
          />
        </Grid>
        <Grid item xs={12} sm={12} mb={2}>
          <TextField
            required
            id="email"
            name="Email"
            label="Email"
            helperText={EMAIL_REGEX.test(email) ? "" : "Invalid Email"}
            fullWidth
            value={email}
            onChange={handleEmailChange}
          />
        </Grid>
        <Grid item xs={12} sm={12} mb={2}>
          <TextField
            required
            id="phone"
            name="Phone"
            label="Phone Number"
            fullWidth
            value={phoneNumber}
            onChange={handlePhoneNumberChange}
            helperText={
              PHONE_NUMBER_REGEX.test(phoneNumber) ? "" : "Invalid Phone Number"
            }
          />
        </Grid>

        {/* Roles display */}
        <Grid item xs={12}>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Grid
              item
              sm={12}
              display={"flex"}
              flexDirection={"row"}
              justifyContent={"space-between"}
              alignItems={"center"}
            >
              <Title>Roles</Title>

              <Button onClick={handleOpenRolesDialog}>
                <AddIcon />
              </Button>
            </Grid>
            <Stack direction="row" spacing={1}>
              {userRoles.map((role, index) => {
                return (
                  <Chip
                    key={index}
                    label={role}
                    variant="outlined"
                    // onClick={handleClick}
                    onDelete={() => handleDelete(role)}
                  />
                );
              })}
            </Stack>
          </Paper>
        </Grid>
      </Grid>

      <Grid item xs={12} container justifyContent="flex-end">
        <Button
          disabled={!canCreate}
          variant="contained"
          onClick={handleCreateClick}
          sx={{ mt: 3, ml: 1 }}
          color="secondary"
        >
          Create
        </Button>
      </Grid>
      <Dialog onClose={handleCloseRolesDialog} open={openRolesDialog}>
        <DialogTitle>Add Role</DialogTitle>
        <DialogContent>
          <Stack direction={"row"} spacing={1}>
            {rolesCode
              ?.filter((role) => !userRoles.includes(role))
              .map((role, index) => (
                <Chip
                  onClick={() => handleAddRole(role)}
                  key={index}
                  label={role}
                  variant="outline"
                ></Chip>
              ))}
          </Stack>
        </DialogContent>
      </Dialog>
    </Paper>
  );
};

export default AddNewUser;
