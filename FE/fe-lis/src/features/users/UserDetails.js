import AddIcon from "@mui/icons-material/Add";
import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Paper,
  Skeleton,
} from "@mui/material";
import Chip from "@mui/material/Chip";
import Grid from "@mui/material/Grid";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import { deepPurple } from "@mui/material/colors";
import { useSnackbar } from "notistack";
import { React, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Title from "../../components/Title";
import { useGetMasterDataWithTypeQuery } from "../masterData/masterDataApiSlice";
import {
  useGetUsersWithUserIdQuery,
  useUpdateUserMutation,
} from "./usersApiSlice";
import useAuth from "../../hooks/useAuth";

const FULL_NAME_REGEX =
  /^[ A-Za-z0-9À-Ỹà-ỹĂ-Ắă-ằẤ-Ứấ-ứÂ-Ấâ-ấĨ-Ỹĩ-ỹĐđÊ-Ểê-ểÔ-Ốô-ốơ-ởƠ-Ớơ-ớƯ-Ứư-ứỲ-Ỵỳ-ỵ\s]+$/;
const PHONE_NUMBER_REGEX = /^[0-9]{10}$/;

const UserDetails = () => {
  const { userLogin, isAdmin } = useAuth();
  const navigate = useNavigate();
  const [canEdit, setCanEdit] = useState(false);
  const [openRolesDialog, setOpenRolesDialog] = useState(false);
  const [status, setStatus] = useState();
  const [fullName, setFullName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [note, setNote] = useState("");
  const [userRoles, setUserRoles] = useState([]);

  const { enqueueSnackbar } = useSnackbar();

  const { userId } = useParams();
  const {
    data: user,
    isSuccess: getUserSuccess,
    isLoading: getUserIsLoading,
    isError: getUserIsError,
    refetch,
  } = useGetUsersWithUserIdQuery(userId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const {
    data: roles,
    isSuccess: masterDataIsSuccess,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery("Role", {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  let rolesCode = [];
  roles?.forEach((role) => {
    rolesCode.push(role?.code);
  });

  const [
    updateUser,
    {
      isSuccess: updateUserIsSuccess,
      isError: updateUserIsError,
      error: updateError,
    },
  ] = useUpdateUserMutation();

  const handleEditClick = () => {
    setCanEdit(true);
  };
  const handleResetClick = () => {
    setCanEdit(false);
    setFullName(user.fullName);
    setPhoneNumber(user.phoneNumber);
    setUserRoles(user.roles);
    setStatus(user.active);
    setNote(user?.note);
  };

  const handleFullNameInput = (e) => {
    setFullName(e.target.value);
  };
  const handlePhoneNumberChange = (e) => {
    setPhoneNumber(e.target.value);
  };
  const handleNoteChange = (e) => {
    setNote(e.target.value);
  };

  const handleDelete = (roleToDelete) => {
    setUserRoles((roles) => roles.filter((role) => role !== roleToDelete));
  };
  const handleChangeStatus = () => {
    setStatus((prev) => !prev);
  };

  const handleSaveClick = async () => {
    if (canSave) {
      try {
        await updateUser({
          id: userId,
          email: user?.email,
          roles: userRoles,
          fullName: fullName,
          phoneNumber,
          active: status,
          note: note,
        });
      } catch (err) {
        console.error("Failed to save the user new information" + err);
        enqueueSnackbar("Failed to save the user new information : " + err, {
          variant: "error",
        });
      }
    }
  };

  const handleCloseRolesDialog = () => {
    setOpenRolesDialog(false);
  };
  const handleOpenRolesDialog = () => {
    setOpenRolesDialog(true);
  };
  const handleAddRole = (role) => {
    if (!userRoles.includes(role)) setUserRoles((prev) => [...prev, role]);
    setOpenRolesDialog(false);
  };

  useEffect(() => {
    if (getUserSuccess) {
      setUserRoles(user?.roles);
      setPhoneNumber(user?.phoneNumber);
      setFullName(user?.fullName);
      setNote(user?.note);
      setStatus(user?.active);
    }
  }, [getUserSuccess]);

  useEffect(() => {
    if (getUserIsError) {
      navigate("/dash-board/users-list");
      enqueueSnackbar("Get user fail !", { variant: "error" });
    }
  }, [getUserIsError]);

  useEffect(() => {
    if (updateUserIsSuccess) {
      enqueueSnackbar("update user is successful !", { variant: "success" });
      setCanEdit(false);
      refetch();
    }
    if (updateUserIsError) {
      enqueueSnackbar("update user is fail !" + updateError?.data?.message, {
        variant: "error",
      });
    }
  }, [updateUserIsSuccess, updateUserIsError]);

  const canSave =
    (fullName !== user?.fullName ||
      phoneNumber !== user?.phoneNumber ||
      userRoles !== user?.roles ||
      note !== user?.note ||
      status !== user?.active) &&
    FULL_NAME_REGEX.test(fullName) &&
    PHONE_NUMBER_REGEX.test(phoneNumber);

  let content;

  if (getUserIsLoading) {
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );
  }

  if (getUserSuccess) {
    const email = user?.email;
    const avtFirstLetter = email.charAt(0).toUpperCase();
    content = (
      <Paper
        sx={{
          p: 3,
          gap: 2,
        }}
      >
        <Title>USER INFORMATION</Title>
        <Avatar sx={{ bgcolor: deepPurple[500], mb: 2 }}>
          {avtFirstLetter}
        </Avatar>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={12} md={7} mb={2}>
            <TextField
              id="fullName"
              name="fullName"
              label="Full name"
              fullWidth
              value={fullName}
              disabled={!canEdit}
              helperText={
                FULL_NAME_REGEX.test(fullName) ? "" : "invalid user name"
              }
              onChange={handleFullNameInput}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={5} mb={2}>
            <TextField
              id="phone"
              name="Phone"
              label="Phone Number"
              fullWidth
              value={phoneNumber}
              disabled={!canEdit}
              onChange={handlePhoneNumberChange}
              helperText={
                PHONE_NUMBER_REGEX.test(phoneNumber)
                  ? ""
                  : "Invalid Phone Number"
              }
            />
          </Grid>
          <Grid item xs={12} sm={12} mb={2}>
            <TextField
              id="email"
              name="Email"
              label="Email"
              fullWidth
              value={user?.email}
              disabled
            />
          </Grid>

          {/* Roles display */}
          <Grid item xs={12}>
            <Paper
              sx={{
                p: 2,
                display: "flex",
                flexDirection: "column",
                gap: 2,
                marginBottom: "1rem",
              }}
            >
              <Grid
                item
                sm={12}
                display={"flex"}
                flexDirection={"row"}
                justifyContent={"space-between"}
                alignItems={"center"}
              >
                <Title>Roles</Title>

                {canEdit && isAdmin && !userRoles.includes("Admin") ? (
                  <Button onClick={handleOpenRolesDialog}>
                    <AddIcon />
                  </Button>
                ) : null}
              </Grid>
              <Stack direction="row" spacing={1}>
                {userRoles.map((role, index) => {
                  return (
                    <Chip
                      key={index}
                      label={role}
                      variant="outlined"
                      onDelete={
                        canEdit && isAdmin && !userRoles.includes("Admin")
                          ? () => handleDelete(role)
                          : null
                      }
                    />
                  );
                })}
              </Stack>
            </Paper>
          </Grid>

          <Grid item xs={12} sm={12} mb={2}>
            <Button
              disabled={!canEdit || !isAdmin || userRoles.includes("Admin")}
              onClick={handleChangeStatus}
            >
              <Chip
                label={status ? "active" : "Deactivated"}
                variant="outlined"
                color={status ? "success" : "error"}
              ></Chip>
            </Button>
          </Grid>
          <Grid item xs={12} sm={12} mb={2}>
            <TextField
              id="note"
              name="note"
              label="Note"
              multiline
              rows={4}
              fullWidth
              value={note}
              disabled={!canEdit}
              onChange={handleNoteChange}
            />
          </Grid>
        </Grid>

        <Grid item xs={12} container justifyContent="flex-start">
          {canEdit ? (
            <>
              <Button
                variant="contained"
                onClick={handleSaveClick}
                sx={{ mt: 3, ml: 1 }}
                color="secondary"
                disabled={!canSave}
              >
                Save
              </Button>

              <Button
                variant="contained"
                onClick={handleResetClick}
                sx={{ mt: 3, ml: 1 }}
              >
                reset
              </Button>
            </>
          ) : (
            <Button
              variant="contained"
              onClick={handleEditClick}
              sx={{ mt: 3, ml: 1 }}
            >
              Edit !
            </Button>
          )}
        </Grid>
        <Dialog onClose={handleCloseRolesDialog} open={openRolesDialog}>
          <DialogTitle>Add Role</DialogTitle>
          <DialogContent>
            <Stack direction={"row"} spacing={1}>
              {rolesCode
                ?.filter((roleCode) => !userRoles.includes(roleCode))
                .map((roleCode, index) => (
                  <Chip
                    onClick={() => handleAddRole(roleCode)}
                    key={index}
                    label={roleCode}
                    variant="outline"
                  ></Chip>
                ))}
            </Stack>
          </DialogContent>
        </Dialog>
      </Paper>
    );
  }

  return content;
};

export default UserDetails;
