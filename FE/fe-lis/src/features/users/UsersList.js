import BorderColorIcon from '@mui/icons-material/BorderColor';
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Skeleton,
  Tooltip,
  Typography,
  Backdrop,
  CircularProgress,
} from '@mui/material';
import Chip from '@mui/material/Chip';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useSnackbar } from 'notistack';
import { React, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import IconTextField from '../../components/IconTextField';
import Title from '../../components/Title';
import useAuth from '../../hooks/useAuth';
import { useGetMasterDataWithTypeQuery } from '../masterData/masterDataApiSlice';
import AddNewUser from './AddNewUser';
import {
  useDeleteUserMutation,
  useGetUsersWithSearchQuery,
  useUpdateUserMutation,
} from './usersApiSlice';

// Generate users usersData

const UsersList = () => {
  const searchURLDefault = '?page=1&role=all&active=all&keyword=';
  const { userLogin } = useAuth();
  const navigate = useNavigate();
  const { search } = useLocation();
  const { enqueueSnackbar } = useSnackbar();
  const [role, setRole] = useState('all');
  const [status, setStatus] = useState('all');
  const [keyword, setKeyword] = useState('');
  const [page, setPage] = useState(1);

  const [selectedUser, setSelectedUser] = useState();
  const [openStatusDialog, setOpenStatusDialog] = useState(false);
  const [openAddDialog, setOpenAddDialog] = useState(false);
  const [openActionDeleteDialog, setOpenActionDeleteDialog] = useState(false);

  useEffect(() => {
    setKeyword(new URLSearchParams(search).get('keyword') || '');
    setRole(new URLSearchParams(search).get('role') || 'all');
    setStatus(new URLSearchParams(search).get('active') || 'all');
    setPage(parseInt(new URLSearchParams(search).get('page')) || 1);
  }, [search]);

  const handleRoleFilterChange = (event) => {
    setRole(event.target.value);
  };
  const handleStatusFilterChange = (event) => {
    setStatus(event.target.value);
  };

  const {
    data: usersData,
    isLoading: usersDataIsLoading,
    isSuccess: usersDataIsSuccess,
    isError: usersDataIsError,
    error: usersDataError,
    refetch,
  } = useGetUsersWithSearchQuery(search === '' ? searchURLDefault : search, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const [openBackdrop, setOpenBackdrop] = useState(false);
  const [isRefetching, setIsRefetching] = useState(false);

  const handleOpenBackdrop = () => {
    setIsRefetching(true);
    setOpenBackdrop(true);
    refetch().then(() => {
      setIsRefetching(false);
    });
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  useEffect(() => {
    if (isRefetching) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [isRefetching]);

  const {
    data: roles,
    isSuccess: masterDataIsSuccess,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery('Role', {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const [
    updateUser,
    {
      isSuccess: updateUserIsSuccess,
      isError: updateUserIsError,
      error: updateUserError,
    },
  ] = useUpdateUserMutation();

  const [
    deleteUser,
    {
      isSuccess: deleteUserIsSuccess,
      isError: deleteUserIsError,
      error: deleteUserError,
    },
  ] = useDeleteUserMutation();

  const handleOpenStatusDialog = (user) => {
    setSelectedUser(user);
    setOpenStatusDialog(true);
  };

  const handleCloseStatusDialog = () => {
    setOpenStatusDialog(false);
  };

  const handleOpenActionDeleteDialog = (user) => {
    setSelectedUser(user);
    setOpenActionDeleteDialog(true);
  };

  const handleCloseActionDeleteDialog = () => {
    setOpenActionDeleteDialog(false);
  };

  const handleCloseAddDialog = () => {
    setOpenAddDialog(false);
  };

  const handleOpenAddDialog = () => {
    setOpenAddDialog(true);
  };

  const handleChangeActiveStatus = async (user) => {
    try {
      await updateUser({
        id: user.id,
        active: !user.active,
      });
    } catch (err) {
      console.error('Failed to save the user new information', err);
      enqueueSnackbar('Failed to save the user new information : ' + err, {
        variant: 'error',
      });
    }
  };

  const handleDeleteUser = async (user) => {
    try {
      await deleteUser({
        id: user.id,
      });
    } catch (err) {
      console.error('Failed to  delete user ', err);
      enqueueSnackbar('Failed to  delete user : ' + err, {
        variant: 'error',
      });
    }
  };

  useEffect(() => {
    if (updateUserIsSuccess) {
      enqueueSnackbar('Change Status is successful !', {
        variant: 'success',
      });
      setOpenStatusDialog(false);
      refetch();
    }
    if (updateUserIsError) {
      enqueueSnackbar(
        'Failed to save the user new information : ' +
          updateUserError?.data.message,
        {
          variant: 'error',
        }
      );
    }
  }, [updateUserIsError, updateUserIsSuccess]);

  useEffect(() => {
    // Delete User
    if (deleteUserIsSuccess) {
      enqueueSnackbar('delete is successful !', {
        variant: 'success',
      });
      setOpenActionDeleteDialog(false);
      refetch();
    }
    if (deleteUserIsError) {
      enqueueSnackbar(
        'Failed to delete user : ' + deleteUserError?.data.message,
        {
          variant: 'error',
        }
      );
    }
  }, [deleteUserIsError, deleteUserIsSuccess]);

  const handleKeywordInput = (event) => {
    setKeyword(event.target.value);
  };

  const handlePaginationChanged = (event, value) => {
    let query = new URLSearchParams({
      page: value,
      role,
      active: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };

  const handleSearch = async () => {
    handleOpenBackdrop();
    let query = new URLSearchParams({
      page: 1,
      role,
      active: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };

  const handleClickActionViewUser = (userId) => {
    navigate(`/dash-board/users-list/user-details/${userId}`);
  };

  let content;
  let tableContent = null;

  if (usersDataIsLoading) {
    content = (
      <Box sx={{ width: '100%' }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );
  }

  if (usersDataIsError) {
    tableContent = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Typography variant="h5" color={'red'}>
            {usersDataError?.data?.message}
          </Typography>
        </Grid>
      </Grid>
    );
  }

  if (masterDataIsError) {
    console.log(masterDataError);
  }
  if (usersDataIsSuccess) {
    const { users, usersCount } = usersData;
    const pages = Math.ceil(usersCount / 10);
    if (users?.filter((user) => user._id !== userLogin?._id).length === 0) {
      tableContent = (
        <Grid container spacing={3}>
          {/* Recent Dash Board */}
          <Grid item xs={12}>
            <Typography variant="h5" color={'red'}>
              THERE ARE NO DATA MATCH !
            </Typography>
          </Grid>
          <Pagination
            count={pages}
            page={page}
            variant="outlined"
            color="primary"
            sx={{ marginTop: '2rem', marginBottom: '1.5rem' }}
            onChange={handlePaginationChanged}
          />
        </Grid>
      );
    } else {
      tableContent = (
        <>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell sx={{ color: 'indianred' }}>No.</TableCell>
                <TableCell sx={{ color: 'indianred', width: '30%' }}>
                  Full Name
                </TableCell>
                <TableCell sx={{ color: 'indianred' }}>Phone number</TableCell>
                <TableCell sx={{ color: 'indianred' }}>Email</TableCell>
                <TableCell sx={{ color: 'indianred' }}>Role</TableCell>
                <TableCell sx={{ color: 'indianred' }}>Status</TableCell>
                <TableCell align="center" sx={{ color: 'indianred' }}>
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {users
                .filter((user) => user._id !== userLogin?._id)
                .map((user, index) => (
                  <TableRow key={user?.id} hover>
                    <TableCell sx={{ wordBreak: 'break-word', width: '10%' }}>
                      {(page - 1) * 10 + (index + 1)}
                    </TableCell>
                    <TableCell sx={{ wordBreak: 'break-word', width: '20%' }}>
                      {user?.fullName}
                    </TableCell>
                    <TableCell sx={{ wordBreak: 'break-word', width: '20%' }}>
                      {user?.phoneNumber}
                    </TableCell>
                    <TableCell>{user?.email}</TableCell>
                    <TableCell sx={{ wordBreak: 'break-word', width: '20%' }}>
                      {user?.roles?.map((role) => {
                        return `[${role}]`;
                      })}
                    </TableCell>
                    <TableCell>
                      <Button onClick={() => handleOpenStatusDialog(user)}>
                        {user?.active ? (
                          <Chip
                            label="Active"
                            color="success"
                            variant="outlined"
                          />
                        ) : (
                          <Chip
                            label="Deactivated"
                            color="error"
                            variant="outlined"
                          />
                        )}
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Box display={'flex'}>
                        <Tooltip title="View Details" placement="top" arrow>
                          <Button
                            color="success"
                            onClick={() => handleClickActionViewUser(user?.id)}
                          >
                            <BorderColorIcon />
                          </Button>
                        </Tooltip>
                      </Box>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
          <Box>
            {/* update user active dialog */}
            <Dialog open={openStatusDialog} onClose={handleCloseStatusDialog}>
              <DialogTitle>Update Status</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  Update status of user : {selectedUser?.email}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={handleCloseStatusDialog}
                  variant="contained"
                  color="warning"
                >
                  Cancel
                </Button>
                <Button
                  onClick={() => handleChangeActiveStatus(selectedUser)}
                  variant="contained"
                  color="success"
                >
                  Confirm
                </Button>
              </DialogActions>
            </Dialog>

            {/* Action delete Dialog */}
            <Dialog
              open={openActionDeleteDialog}
              onClose={handleCloseActionDeleteDialog}
            >
              <DialogTitle color={'red'}> DELETE USER </DialogTitle>
              <DialogContent>
                <DialogContentText>
                  You want delete user : {selectedUser?.email}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={handleCloseActionDeleteDialog}
                  variant="contained"
                  color="warning"
                >
                  Cancel
                </Button>
                <Button
                  onClick={() => handleDeleteUser(selectedUser)}
                  variant="contained"
                  color="success"
                >
                  Confirm
                </Button>
              </DialogActions>
            </Dialog>
          </Box>
          <Pagination
            count={pages}
            page={page}
            variant="outlined"
            color="primary"
            sx={{ marginTop: '2rem', marginBottom: '1.5rem' }}
            onChange={handlePaginationChanged}
          />
          <Backdrop
            open={openBackdrop}
            onClick={handleCloseBackdrop}
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
        </>
      );
    }
  }
  content = (
    <Box>
      {/* end Filter */}
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Grid
              item
              xs={12}
              display={'flex'}
              flexDirection={'row'}
              justifyContent={'space-between'}
            >
              <Title>User List</Title>
            </Grid>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              justifyContent={'flex-start'}
              alignItems={'center'}
              marginBottom={3}
            >
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="role-select-label">Roles</InputLabel>
                  <Select
                    value={role}
                    id="role-select"
                    label="Roles"
                    onChange={handleRoleFilterChange}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    {roles?.map((role, index) => (
                      <MenuItem key={index} value={role.code}>
                        {role.code}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="status-select">Status</InputLabel>
                  <Select
                    value={status}
                    id="status-select"
                    label="Status"
                    onChange={handleStatusFilterChange}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    <MenuItem value={'active'}>Active</MenuItem>
                    <MenuItem value={'deactivated'}>Deactivated</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={3}>
                <IconTextField
                  value={keyword}
                  fullWidth
                  id="keyword"
                  label="Keyword"
                  name="keyword"
                  autoComplete="keyword"
                  autoFocus
                  iconEnd={<ManageSearchIcon />}
                  onChange={handleKeywordInput}
                  size="small"
                />
              </Grid>
              <Grid item sm={12} md={3}>
                <Button variant="contained" onClick={handleSearch}>
                  Search
                </Button>
              </Grid>
            </Grid>
            <Divider sx={{ mb: 2 }}></Divider>
            <Box
              sx={{ mt: 1, mb: 1 }}
              fullWidth
              display={'flex'}
              justifyContent={'space-between'}
              alignItems={'center'}
            >
              <Typography component="h2" variant="h6" color="primary">
                Users Table
              </Typography>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleOpenAddDialog}
              >
                Add New User
              </Button>
            </Box>
            {tableContent}
          </Paper>
          <Dialog onClose={handleCloseAddDialog} open={openAddDialog}>
            <AddNewUser
              handleCloseAddDialog={handleCloseAddDialog}
              roles={roles}
              refetch={refetch}
            />
          </Dialog>
        </Grid>
      </Grid>
    </Box>
  );

  return content;
};

export default UsersList;
