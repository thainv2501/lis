import FindInPageIcon from '@mui/icons-material/FindInPage';
import {
  Box,
  Button,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Skeleton,
  Typography,
  Backdrop,
  CircularProgress,
} from '@mui/material';
import { useSnackbar } from 'notistack';
import { React, useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import IconTextField from '../../components/IconTextField';
import Title from '../../components/Title';
import { useGetMasterDataWithTypeQuery } from '../masterData/masterDataApiSlice';
import ClassTable from './ClassTable';
import { useGetClassWithSearchQuery } from './classApiSlice';
import ImportClassFromXLSX from './ImportClassFromXLSX';

const ClassList = () => {
  const {
    data: masterDataSemester,
    isSuccess: masterDataIsSuccess,
    isLoading: masterDataIsLoading,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery('Semester');

  const navigate = useNavigate();
  const searchURLDefault = `?page=1&semester=all&active=all&keyword=`;
  const { search } = useLocation();
  const [semester, setSemester] = useState('all');
  const [status, setStatus] = useState('all');
  const [keyword, setKeyword] = useState('');
  const [page, setPage] = useState(1);
  const { enqueueSnackbar } = useSnackbar();

  const [openImportXLSXDialog, setOpenImportXLSXDialog] = useState(false);

  const handleOpenImportXLSXDialog = () => {
    setOpenImportXLSXDialog(true);
  };
  const handleCloseImportXLSXDialog = () => {
    setOpenImportXLSXDialog(false);
  };

  useEffect(() => {
    setKeyword(new URLSearchParams(search).get('keyword') || '');
    setSemester(new URLSearchParams(search).get('semester') || 'all');
    setStatus(new URLSearchParams(search).get('active') || 'all');
    setPage(parseInt(new URLSearchParams(search).get('page')) || 1);
  }, [search]);

  const {
    data: classDataRes,
    isSuccess: getClassDataIsSuccess,
    isError: getClassDataIsError,
    isLoading: getClassDataIsLoading,
    error: getClassDataError,
    refetch,
  } = useGetClassWithSearchQuery(search === '' ? searchURLDefault : search, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const [openBackdrop, setOpenBackdrop] = useState(false);
  const [isRefetching, setIsRefetching] = useState(false);

  const handleOpenBackdrop = () => {
    setIsRefetching(true);
    setOpenBackdrop(true);
    refetch().then(() => {
      setIsRefetching(false);
    });
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  useEffect(() => {
    if (isRefetching) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [isRefetching]);

  const handleStatusFilterChange = (event) => {
    setStatus(event.target.value);
  };
  const handleSemesterFilterChange = (event) => {
    setSemester(event.target.value);
  };
  const handleKeywordChanged = (event) => {
    setKeyword(event.target.value);
  };
  const handleSearch = () => {
    handleOpenBackdrop();
    let query = new URLSearchParams({
      page: 1,
      semester,
      active: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };

  const handlePaginationChanged = (event, value) => {
    let query = new URLSearchParams({
      page: value,
      semester,
      active: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };

  let content;
  if (getClassDataIsLoading || masterDataIsLoading)
    content = (
      <Box sx={{ width: '100%' }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (getClassDataIsError || masterDataError) {
    const { masterData } = masterDataSemester;
    content = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Grid
              item
              xs={12}
              display={'flex'}
              flexDirection={'row'}
              justifyContent={'space-between'}
              sx={{ mb: 1 }}
            >
              <Title>Class List</Title>
            </Grid>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              justifyContent={'flex-start'}
              alignItems={'center'}
              marginBottom={3}
            >
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="semester-select-label">Semester</InputLabel>
                  <Select
                    id="semester-select"
                    label="Semester"
                    onChange={handleSemesterFilterChange}
                    value={semester}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    {masterData &&
                      masterData?.map((semester, index) => (
                        <MenuItem key={index} value={semester?._id}>
                          {semester.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="status-select">Status</InputLabel>
                  <Select
                    id="status-select"
                    label="Status"
                    onChange={handleStatusFilterChange}
                    value={status}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    <MenuItem value={'active'}>Active</MenuItem>
                    <MenuItem value={'deactivated'}>Deactivated</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={3}>
                <IconTextField
                  margin="normal"
                  fullWidth
                  id="keyword"
                  label="Keyword"
                  name="keyword"
                  value={keyword}
                  onChange={handleKeywordChanged}
                  autoFocus
                  iconEnd={<FindInPageIcon />}
                  size="small"
                />
              </Grid>
              <Grid item sm={12} md={3}>
                <Button variant="contained" onClick={handleSearch}>
                  Search
                </Button>
              </Grid>
            </Grid>
            <Box
              display={'flex'}
              justifyContent={'space-between'}
              alignItems={'center'}
            >
              <Typography component="h2" variant="h6" color="primary">
                Class Table
              </Typography>
              <Grid>
                <Button
                  variant="contained"
                  color="success"
                  sx={{ m: 2 }}
                  onClick={handleOpenImportXLSXDialog}
                >
                  import from xlsx
                </Button>
                <Link to={'add-new-class'}>
                  <Button variant="contained" color="secondary">
                    New Class
                  </Button>
                </Link>
              </Grid>
            </Box>
            <Grid>
              <Typography variant="h4" color={'red'}>
                {getClassDataError?.data.message}
                {masterDataError?.data?.message}
              </Typography>
            </Grid>
          </Paper>
        </Grid>
        <ImportClassFromXLSX
          handleCloseImportXLSXDialog={handleCloseImportXLSXDialog}
          openImportXLSXDialog={openImportXLSXDialog}
          refetch={refetch}
        />
      </Grid>
    );
  }

  if (getClassDataIsSuccess && masterDataIsSuccess) {
    const { masterData, currentSemester } = masterDataSemester;
    const { classData, classCount } = classDataRes;
    const pages = Math.ceil(classCount / 10);

    content = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Grid
              item
              xs={12}
              display={'flex'}
              flexDirection={'row'}
              justifyContent={'space-between'}
              sx={{ mb: 1 }}
            >
              <Title>Class List</Title>
            </Grid>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              justifyContent={'flex-start'}
              alignItems={'center'}
              marginBottom={3}
            >
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="semester-select-label">Semester</InputLabel>
                  <Select
                    id="semester-select"
                    label="Semester"
                    onChange={handleSemesterFilterChange}
                    value={semester}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    {masterData &&
                      masterData?.map((semester, index) => (
                        <MenuItem key={index} value={semester?._id}>
                          {semester?.name}
                        </MenuItem>
                      ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="status-select">Status</InputLabel>
                  <Select
                    id="status-select"
                    label="Status"
                    onChange={handleStatusFilterChange}
                    value={status}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    <MenuItem value={'active'}>Active</MenuItem>
                    <MenuItem value={'deactivated'}>Deactivated</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={3}>
                <IconTextField
                  fullWidth
                  id="keyword"
                  label="Keyword"
                  name="keyword"
                  value={keyword}
                  autoFocus
                  iconEnd={<FindInPageIcon />}
                  onChange={handleKeywordChanged}
                  size="small"
                />
              </Grid>
              <Grid item sm={12} md={3}>
                <Button variant="contained" onClick={handleSearch}>
                  Search
                </Button>
              </Grid>
            </Grid>
            <Divider sx={{ mb: 2 }}></Divider>
            <Box
              sx={{ mt: 1, mb: 1 }}
              display={'flex'}
              justifyContent={'space-between'}
              alignItems={'center'}
            >
              <Typography component="h2" variant="h6" color="primary">
                Class Table
              </Typography>
              <Grid>
                <Button
                  variant="contained"
                  color="success"
                  sx={{ m: 2 }}
                  onClick={handleOpenImportXLSXDialog}
                >
                  import from xlsx
                </Button>
                <Link to={'add-new-class'}>
                  <Button variant="contained" color="secondary">
                    New Class
                  </Button>
                </Link>
              </Grid>
            </Box>
            <Grid>
              <ClassTable
                page={page}
                classData={classData}
                navigate={navigate}
                enqueueSnackbar={enqueueSnackbar}
                refetch={refetch}
              />
            </Grid>
            <Pagination
              count={pages}
              page={page}
              variant="outlined"
              color="primary"
              sx={{ marginTop: '2rem', marginBottom: '1.5rem' }}
              onChange={handlePaginationChanged}
            />
          </Paper>
        </Grid>
        <ImportClassFromXLSX
          handleCloseImportXLSXDialog={handleCloseImportXLSXDialog}
          openImportXLSXDialog={openImportXLSXDialog}
          refetch={refetch}
        />
        <Backdrop
          open={openBackdrop}
          onClick={handleCloseBackdrop}
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Grid>
    );
  }
  // }
  return content;
};
export default ClassList;
