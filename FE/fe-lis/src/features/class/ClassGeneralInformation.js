import AddIcon from "@mui/icons-material/Add";
import {
  Box,
  Button,
  Dialog,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import Title from "../../components/Title";
import SubjectDialog from "../subjects/SubjectDialog";
import ClassTrainersDialog from "./ClassTrainersDialog";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import { useUpdateClassMutation } from "./classApiSlice";
import { enqueueSnackbar } from "notistack";

const ClassGeneralInformation = ({
  viewMode,
  masterDataSemester,
  classObj,
  semester,
  trainer,
  className,
  setClassName,
  setSemester,
  setTrainer,
  subject,
  setSubject,
  studentNumber,
  handleCreateClass,
  classData,
  refetch,
}) => {
  const [openSubjectDialog, setOpenSubjectDialog] = useState(false);
  const [openAddTrainersDialog, setOpenAddTrainersDialog] = useState(false);
  const [canEdit, setCanEdit] = useState(false);
  const canSave = className.trim() !== "";

  const [
    updateClass,
    {
      isSuccess: updateClassIsSuccess,
      isError: updateClassIsError,
      error: updateClassError,
    },
  ] = useUpdateClassMutation();

  const handleSemesterChange = (event) => {
    setSemester(event.target.value);
  };
  const handleSubjectChange = (event) => {
    setSubject(event.target.value);
  };
  const handleClassNameChange = (event) => {
    setClassName(event.target.value);
  };

  const handleOpenSubjectDialog = () => {
    setOpenSubjectDialog(true);
  };
  const handleCloseSubjectDialog = () => {
    setOpenSubjectDialog(false);
  };
  const handleOpenAddTrainersDialog = () => {
    setOpenAddTrainersDialog(true);
  };
  const handleCloseAddTrainersDialog = () => {
    setOpenAddTrainersDialog(false);
  };
  const handleUpdate = async () => {
    try {
      await updateClass({ id: classObj?._id, className });
    } catch (error) {
      enqueueSnackbar(`update class : ${classObj?.className} fail !`, {
        variant: "error",
      });
    }
    refetch();
  };

  useEffect(() => {
    if (updateClassIsSuccess) {
      enqueueSnackbar(`Update Class ${className} successful `, {
        variant: "success",
      });
    }
    setCanEdit(false);
  }, [updateClassIsSuccess]);
  useEffect(() => {
    if (updateClassIsError) {
      enqueueSnackbar(
        `update class : ${classObj?.className} error : ` +
          updateClassError?.data?.message,
        {
          variant: "error",
        }
      );
    }
  }, [updateClassIsError]);

  const canAdd =
    className.trim() !== "" &&
    subject !== "" &&
    trainer !== undefined &&
    semester !== "";
  return (
    <>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={12} md={6} mb={2}>
            <TextField
              required
              id="class-name"
              name="class-name"
              label="Class Name"
              value={className}
              onChange={handleClassNameChange}
              fullWidth
              disabled={classObj !== null && !canEdit}
            />
          </Grid>
          <Grid item sm={12} md={6} lg={6}>
            <TextField
              disabled
              id="student-number"
              name="Student Number"
              label="Student Number"
              value={
                classObj !== null ? classObj?.students.length : studentNumber
              }
              fullWidth
            />
          </Grid>
          <Grid item sm={12} md={6} lg={6}>
            {classObj !== null ? (
              <TextField
                disabled
                id="semester-code"
                name="semester-code"
                value={classObj?.semester?.code ? classObj?.semester?.code : ""}
                fullWidth
              />
            ) : (
              <FormControl fullWidth>
                <InputLabel id="semester-select-label" required>
                  Semester
                </InputLabel>
                <Select
                  id="semester-select"
                  label="Semester"
                  onChange={handleSemesterChange}
                  value={semester}
                  MenuProps={{
                    PaperProps: {
                      style: {
                        maxHeight: "200px",
                        width: 250,
                      },
                    },
                  }}
                >
                  {masterDataSemester?.masterData &&
                    masterDataSemester?.masterData?.map((semester, index) => (
                      <MenuItem key={index} value={semester}>
                        {semester.code}
                      </MenuItem>
                    ))}
                </Select>
              </FormControl>
            )}
          </Grid>
          <Grid item xs={12} sm={12} md={6} mb={2}>
            {classObj !== null ? (
              <TextField
                disabled
                id="semester-code"
                name="semester-code"
                value={classObj?.semester?.name ? classObj?.semester?.name : ""}
                fullWidth
              />
            ) : (
              <TextField
                disabled
                id="semester-name"
                name="semester-name"
                value={semester?.name ? semester?.name : ""}
                fullWidth
              />
            )}
          </Grid>
          <Grid item xs={12}>
            {classObj !== null ? (
              <>
                <DatePicker
                  sx={{ mr: 4 }}
                  label="From"
                  value={dayjs(classObj?.semester?.from)}
                  disabled
                />
                <DatePicker
                  label="To"
                  value={dayjs(classObj?.semester?.to)}
                  disablePast
                  disabled
                />
              </>
            ) : (
              <>
                <DatePicker
                  sx={{ mr: 4 }}
                  label="From"
                  value={dayjs(semester?.from)}
                  disabled
                />
                <DatePicker
                  label="To"
                  value={dayjs(semester?.to)}
                  disablePast
                  disabled
                />
              </>
            )}
          </Grid>
          {viewMode ? (
            <>
              <Grid item sm={12} md={6} lg={6}>
                <TextField
                  disabled
                  required
                  id="subject-code"
                  name="subject-code"
                  label="Subject Code"
                  value={
                    classObj?.subject?.subjectCode
                      ? classObj?.subject?.subjectCode
                      : ""
                  }
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} mb={2}>
                <TextField
                  disabled
                  id="subject-name"
                  name="subject-name"
                  fullWidth
                  value={classObj?.subject?.subjectName}
                />
              </Grid>
            </>
          ) : (
            <>
              {classObj !== null ? null : (
                <>
                  <Grid item xs={12} sm={12} md={6} mb={2}>
                    <Button
                      variant="outlined"
                      onClick={handleOpenSubjectDialog}
                    >
                      Select Subject *
                    </Button>
                  </Grid>
                  <Grid item xs={12} sm={12} md={6} mb={2}>
                    <Dialog
                      open={openSubjectDialog}
                      onClose={handleCloseSubjectDialog}
                      fullWidth
                      maxWidth={"md"}
                    >
                      <SubjectDialog
                        setSubject={setSubject}
                        setTrainer={setTrainer}
                        handleCloseSubjectDialog={handleCloseSubjectDialog}
                      />
                    </Dialog>
                  </Grid>
                </>
              )}
            </>
          )}
          {subject !== "" ? (
            <>
              <Grid item sm={12} md={6} lg={6}>
                <TextField
                  disabled
                  required
                  id="subject-code"
                  name="subject-code"
                  label="Subject Code"
                  value={subject?.subjectCode ? subject?.subjectCode : ""}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} mb={2}>
                <TextField
                  disabled
                  id="subject-name"
                  name="subject-name"
                  fullWidth
                  value={subject?.subjectName}
                />
              </Grid>
            </>
          ) : (
            <></>
          )}
          <Grid item sm={12}>
            <Paper sx={{ p: 2 }}>
              <Box display={"flex"} justifyContent={"space-between"}>
                <Title>Trainers *</Title>
                {classObj !== null ? null : (
                  <Button
                    disabled={subject === ""}
                    onClick={handleOpenAddTrainersDialog}
                  >
                    <AddIcon />
                  </Button>
                )}
              </Box>
              {classObj !== null ? (
                <>
                  <Grid container spacing={3}>
                    <Grid item sm={12} md={6} lg={6}>
                      <TextField
                        disabled
                        id="full-name"
                        name="Full Name"
                        label="Full Name"
                        value={
                          classObj?.trainer?.fullName
                            ? classObj?.trainer?.fullName
                            : ""
                        }
                        fullWidth
                      />
                    </Grid>
                    <Grid item sm={12} md={6} lg={6}>
                      <TextField
                        disabled
                        id="phone-number"
                        name="Phone Number"
                        label="Phone Number"
                        value={
                          classObj?.trainer?.phoneNumber
                            ? classObj?.trainer?.phoneNumber
                            : ""
                        }
                        fullWidth
                      />
                    </Grid>
                    <Grid item sm={12} md={6} lg={6}>
                      <TextField
                        disabled
                        id="email"
                        name="Email"
                        label="Email"
                        value={
                          classObj?.trainer?.email
                            ? classObj?.trainer?.email
                            : ""
                        }
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </>
              ) : (
                <>
                  <Grid container spacing={3}>
                    <Grid item sm={12} md={6} lg={6}>
                      <TextField
                        disabled
                        id="full-name"
                        name="Full Name"
                        label="Full Name"
                        value={trainer?.fullName ? trainer?.fullName : ""}
                        fullWidth
                      />
                    </Grid>
                    <Grid item sm={12} md={6} lg={6}>
                      <TextField
                        disabled
                        id="phone-number"
                        name="Phone Number"
                        label="Phone Number"
                        value={trainer?.phoneNumber ? trainer?.phoneNumber : ""}
                        fullWidth
                      />
                    </Grid>
                    <Grid item sm={12} md={6} lg={6}>
                      <TextField
                        disabled
                        id="email"
                        name="Email"
                        label="Email"
                        value={trainer?.email ? trainer?.email : ""}
                        fullWidth
                      />
                    </Grid>
                  </Grid>
                </>
              )}

              {subject !== "" ? (
                <Dialog
                  fullWidth
                  maxWidth={"md"}
                  open={openAddTrainersDialog}
                  onClose={handleCloseAddTrainersDialog}
                >
                  <ClassTrainersDialog
                    subjectId={subject?._id}
                    selectedTrainer={trainer}
                    setTrainer={setTrainer}
                    handleCloseAddTrainersDialog={handleCloseAddTrainersDialog}
                  />
                </Dialog>
              ) : null}
            </Paper>
          </Grid>

          <Grid item container justifyContent={"flex-start"} spacing={3}>
            {classObj !== null ? (
              <Grid item>
                <Button
                  variant="contained"
                  disabled={canEdit}
                  onClick={() => setCanEdit(true)}
                >
                  Edit
                </Button>
              </Grid>
            ) : (
              <Grid item>
                <Button
                  variant="contained"
                  disabled={!canAdd}
                  onClick={handleCreateClass}
                >
                  Add
                </Button>
              </Grid>
            )}
            {canEdit && classObj !== null ? (
              <>
                <Grid item>
                  <Button
                    variant="contained"
                    onClick={handleUpdate}
                    color="success"
                    disabled={!canSave}
                  >
                    Save
                  </Button>
                </Grid>

                <Grid item>
                  <Button
                    variant="contained"
                    onClick={() => {
                      setClassName(classData?.className);
                    }}
                  >
                    Reset
                  </Button>
                </Grid>
              </>
            ) : null}
          </Grid>
        </Grid>
      </LocalizationProvider>
    </>
  );
};

export default ClassGeneralInformation;
