import BorderColorIcon from "@mui/icons-material/BorderColor";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  Grid,
  Paper,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import Chip from "@mui/material/Chip";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import React, { useEffect, useState } from "react";
import * as XLSX from "xlsx";
import {
  useAddStudentToClassMutation,
  useAddStudentsToClassMutation,
  useGetClassWithIdQuery,
  useRemoveStudentOutClassMutation,
} from "./classApiSlice";
import { useNavigate } from "react-router-dom";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

const EMAIL_REGEX = /(|^)[\w\d._%+-]+@(?:[\w\d-]+\.)+(\w{2,})(|$)/i;

const ClassStudent = ({
  viewMode,
  classObj,
  setClassObj,
  excelFile,
  excelData,
  excelFileError,
  setExcelData,
  setExcelFile,
  setExcelFileError,
  selectedFile,
  setSelectedFile,
  enqueueSnackbar,
  handleOpenBackDrop,
  handleCloseBackDrop,
}) => {
  const navigate = useNavigate();
  // handle File
  const fileType = [
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ];

  const [selectedStudent, setSelectedStudent] = useState(null);
  const [email, setEmail] = useState("");

  const handleEmailChange = (event) => {
    setEmail(event.target.value);
  };

  const [openAddStudentDialog, setOpenAddStudentDialog] = useState(false);

  const handleOpenAddStudentDialog = () => {
    setOpenAddStudentDialog(true);
  };
  const handleCloseAddStudentDialog = () => {
    setOpenAddStudentDialog(false);
  };

  const [openRemoveStudentDialog, setOpenRemoveStudentDialog] = useState(false);

  const handleOpenRemoveStudentDialog = (user) => {
    setSelectedStudent(user);
    setOpenRemoveStudentDialog(true);
  };
  const handleCloseRemoveStudentDialog = () => {
    setOpenRemoveStudentDialog(false);
  };

  const [students, setStudents] = useState(null);

  const [
    addStudentsToClass,
    {
      isSuccess: addStudentsToClassIsSuccess,
      isError: addStudentsToClassIsError,
      isLoading: addStudentsToClassIsLoading,
      error: addStudentsToClassError,
    },
  ] = useAddStudentsToClassMutation();

  const {
    data: classData,
    isSuccess: getClassDataIsSuccess,
    isError: getCLassDataIsError,
    error: getClassDataError,
    refetch,
  } = useGetClassWithIdQuery(classObj?._id);

  const reformExcelDataToStudentsData = (excelData) => {
    let studentsData = [];
    if (excelData !== null) {
      excelData.forEach((data) => {
        const newStudent = {
          email: data?.["Email"],
          fullName: data?.["Full Name"],
          phoneNumber: data?.["Phone Number"],
        };
        studentsData = [...studentsData, newStudent];
      });
      return studentsData;
    }
  };
  const handleFile = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const checkValidSheet = (worksheet) => {
    try {
      return (
        worksheet?.A1.v.trim() === "Full Name" &&
        worksheet?.B1.v.trim() === "Phone Number" &&
        worksheet?.C1.v.trim() === "Email"
      );
    } catch (error) {
      return false;
    }
  };

  // submit function
  const handleSubmit = () => {
    if (excelFile !== null) {
      const workbook = XLSX.read(excelFile, { type: "buffer" });
      const worksheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[worksheetName];
      const data = XLSX.utils.sheet_to_json(worksheet);
      if (checkValidSheet(worksheet)) {
        setExcelData(data);
      } else {
        setExcelFileError("Please make sure your import right file !");
        setExcelFile(null);
        setExcelData(null);
      }
    } else {
      setExcelData(null);
    }
  };

  // submit function
  const handleImportStudentsToClass = async () => {
    try {
      await addStudentsToClass({ classId: classObj?._id, students });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (selectedFile) {
      if (selectedFile && fileType.includes(selectedFile.type)) {
        let reader = new FileReader();
        reader.readAsArrayBuffer(selectedFile);
        reader.onload = (e) => {
          setExcelFileError(null);
          setExcelFile(e.target.result);
        };
      } else {
        setExcelFileError("Please select only excel file types");
        setExcelFile(null);
      }
    } else {
      console.log("please select your xlsx file");
    }
  }, [selectedFile]);

  useEffect(() => {
    if (excelFile) {
      handleSubmit();
    }
  }, [excelFile]);

  useEffect(() => {
    if (excelData) {
      setStudents(reformExcelDataToStudentsData(excelData));
    }
  }, [excelData]);

  useEffect(() => {
    if (addStudentsToClassIsLoading) {
      handleOpenBackDrop();
    }
  }, [addStudentsToClassIsLoading]);
  useEffect(() => {
    if (addStudentsToClassIsError) {
      enqueueSnackbar(
        "Add students to class fails : " +
          addStudentsToClassError?.data?.message,
        { variant: "error" }
      );
      handleCloseBackDrop();
    }
  }, [addStudentsToClassIsError]);

  useEffect(() => {
    if (addStudentsToClassIsSuccess) {
      enqueueSnackbar("Add students to class successful !", {
        variant: "success",
      });
    }
    setSelectedFile(null);
    setExcelFileError(null);
    setExcelFile(null);
    setExcelData(null);
    setStudents(null);
    handleCloseBackDrop();
    refetch();
  }, [addStudentsToClassIsSuccess]);

  if (getClassDataIsSuccess) {
    setClassObj(classData);
  }

  const handleViewStudentDetails = (user) => {
    navigate(`/dash-board/users-list/user-details/${user?._id}`);
  };
  const [
    removeStudentOutClass,
    {
      isSuccess: removeStudentOutClassIsSuccess,
      isError: removeStudentOutClassIsError,
      isLoading: removeStudentOutClassIsLoading,
      error: removeStudentOutClassError,
    },
  ] = useRemoveStudentOutClassMutation();

  const handleRemoveStudentOutClass = async () => {
    try {
      await removeStudentOutClass({
        classId: classObj._id,
        studentId: selectedStudent._id,
      });
    } catch (error) {
      enqueueSnackbar("Error with remove function !", { variant: "error" });
    }
  };

  useEffect(() => {
    if (removeStudentOutClassIsSuccess) {
      enqueueSnackbar("Remove student successful !", { variant: "success" });
      refetch();
      handleCloseBackDrop();
      handleCloseRemoveStudentDialog();
    }
    if (removeStudentOutClassIsLoading) {
      handleOpenBackDrop();
    }
    if (removeStudentOutClassIsError) {
      enqueueSnackbar("Error : " + removeStudentOutClassError?.data?.message, {
        variant: "error",
      });
      handleCloseRemoveStudentDialog();
      handleCloseBackDrop();
    }
  }, [
    removeStudentOutClassIsSuccess,
    removeStudentOutClassIsLoading,
    removeStudentOutClassIsError,
  ]);

  const [
    addStudentToClass,
    {
      isSuccess: addStudentToClassIsSuccess,
      isError: addStudentToClassIsError,
      isLoading: addStudentToClassIsLoading,
      error: addStudentToClassError,
    },
  ] = useAddStudentToClassMutation();

  const handleAddStudentToClass = async () => {
    try {
      addStudentToClass({ classId: classObj?._id, email });
    } catch (error) {
      enqueueSnackbar("Error with add student to class function : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (addStudentToClassIsSuccess) {
      enqueueSnackbar("Add student successful !", { variant: "success" });
      setEmail("");
      refetch();
      handleCloseBackDrop();
    }
    if (addStudentToClassIsLoading) {
      handleOpenBackDrop();
    }
    if (addStudentToClassIsError) {
      enqueueSnackbar("Error : " + addStudentToClassError?.data?.message, {
        variant: "error",
      });
      handleCloseBackDrop();
    }
  }, [
    addStudentToClassIsSuccess,
    addStudentToClassIsError,
    addStudentToClassIsLoading,
  ]);

  const handleExportXLSX = () => {
    const dataSheet = [1, 2, 3, 4, 5].map((index) => {
      return {
        "Full Name": `Nguyễn Ví Dụ`,
        "Phone Number": `0000000000`,
        Email: `Example${index}@gmail.com`,
      };
    });
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(dataSheet);
    XLSX.utils.book_append_sheet(wb, ws, "MySheet1");
    XLSX.writeFile(
      wb,
      `${classObj.semester.code}-${classObj.className}-${classObj.subject.subjectCode}-StudentsTemplate.xlsx`
    );
  };

  const xlsxDataTableContent = (
    <>
      <Paper sx={{ p: 3, mb: 3 }}>
        <Grid container justifyContent={"space-between"}>
          <Grid item>
            <Typography variant="h6">XLSX Data Table</Typography>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              disabled={excelData === null || classObj === null}
              onClick={handleImportStudentsToClass}
            >
              {classObj === null ? "No Class to Import" : "Import"}
            </Button>
          </Grid>
        </Grid>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell sx={{ color: "indianred" }}>No.</TableCell>
              <TableCell sx={{ color: "indianred", width: "30%" }}>
                Full Name
              </TableCell>
              <TableCell sx={{ color: "indianred" }}>Phone Number</TableCell>
              <TableCell sx={{ color: "indianred" }}>Email</TableCell>
            </TableRow>
          </TableHead>
          {students !== null ? (
            <TableBody>
              {students.map((user, index) => (
                <TableRow key={index} hover>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {user?.fullName}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {user?.phoneNumber}
                  </TableCell>
                  <TableCell>{user?.email}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          ) : null}
        </Table>
      </Paper>
    </>
  );

  const tableContent = (
    <>
      <Paper sx={{ p: 3, mb: 3 }}>
        <Typography variant="h6">Student Table</Typography>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell sx={{ color: "indianred" }}>No.</TableCell>
              <TableCell sx={{ color: "indianred", width: "30%" }}>
                Full Name
              </TableCell>
              <TableCell sx={{ color: "indianred" }}>Phone Number</TableCell>
              <TableCell sx={{ color: "indianred" }}>Email</TableCell>
              <TableCell sx={{ color: "indianred" }}>Status</TableCell>
              <TableCell align="center" sx={{ color: "indianred" }}>
                Actions
              </TableCell>
            </TableRow>
          </TableHead>
          {classData?.students !== undefined ? (
            <TableBody>
              {classData?.students?.map((user, index) => (
                <TableRow key={index} hover>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {user?.fullName}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {user?.phoneNumber}
                  </TableCell>
                  <TableCell>{user?.email}</TableCell>

                  <TableCell>
                    <Button>
                      {user?.active ? (
                        <Chip
                          label="Active"
                          color="success"
                          variant="outlined"
                        />
                      ) : (
                        <Chip
                          label="Deactivated"
                          color="error"
                          variant="outlined"
                        />
                      )}
                    </Button>
                  </TableCell>
                  <TableCell>
                    <Box display={"flex"}>
                      <Tooltip title="View Details" placement="top" arrow>
                        <Button
                          color="success"
                          onClick={() => handleViewStudentDetails(user)}
                        >
                          <BorderColorIcon />
                        </Button>
                      </Tooltip>
                      <Tooltip title="Remove" placement="top" arrow>
                        <Button
                          color="error"
                          onClick={() => handleOpenRemoveStudentDialog(user)}
                        >
                          <DeleteForeverIcon />
                        </Button>
                      </Tooltip>
                    </Box>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          ) : null}
        </Table>
      </Paper>
    </>
  );

  return (
    <>
      <Grid container spacing={3}>
        <Grid
          container
          item
          xs={12}
          justifyContent={"flex-end"}
          alignItems={"center"}
          spacing={3}
        >
          <Grid item xs={12} md={4}>
            {selectedFile ? (
              <TextField
                variant="outlined"
                id="excel-file"
                name="excel-file"
                label="Excel file"
                value={excelFile === null ? "" : selectedFile?.name}
                error={excelFileError !== null}
                helperText={excelFileError !== null ? excelFileError : ""}
                disabled
                fullWidth
              />
            ) : null}
          </Grid>
          <Grid item>
            <label htmlFor="upload-xlsx">
              <input
                style={{ display: "none" }}
                id="upload-xlsx"
                name="upload-xlsx"
                type="file"
                onChange={handleFile}
              />

              <Button
                variant="contained"
                onClick={handleExportXLSX}
                disabled={!classObj}
              >
                XLSX Template
              </Button>
              <Button
                color="success"
                variant="contained"
                component="span"
                disabled={!classObj}
              >
                Import from xlsx
              </Button>
            </label>
            <Button
              color="primary"
              variant="contained"
              component="span"
              sx={{ ml: 3 }}
              onClick={handleOpenAddStudentDialog}
              disabled={!classObj}
            >
              Add a student
            </Button>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          {excelData !== null ? xlsxDataTableContent : null}
          {classObj ? tableContent : null}
        </Grid>
      </Grid>

      <Dialog
        open={openAddStudentDialog}
        onClose={handleCloseAddStudentDialog}
        fullWidth
        maxWidth={"md"}
      >
        <DialogTitle>ADD NEW STUDENT </DialogTitle>
        <Divider />
        <DialogContent>
          <Typography>Please input student email !</Typography>
          <TextField
            sx={{ mt: 2 }}
            required
            id="email"
            name="Email"
            label="Email"
            helperText={EMAIL_REGEX.test(email) ? "" : "Invalid Email"}
            fullWidth
            value={email}
            onChange={handleEmailChange}
          />
        </DialogContent>
        <DialogActions>
          <Button color="warning" onClick={handleCloseAddStudentDialog}>
            Cancel
          </Button>
          <Button
            color="success"
            onClick={() => {
              handleOpenBackDrop();
              handleCloseAddStudentDialog();
              handleAddStudentToClass();
            }}
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog
        open={openRemoveStudentDialog}
        onClose={handleCloseRemoveStudentDialog}
      >
        <DialogTitle>Remove Dialog</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Remove Student : {selectedStudent?.email}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleCloseRemoveStudentDialog}
            variant="contained"
            color="warning"
          >
            Cancel
          </Button>
          <Button
            onClick={() => handleRemoveStudentOutClass()}
            variant="contained"
            color="success"
          >
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ClassStudent;
