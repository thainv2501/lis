import React, { useEffect, useState } from "react";
import Title from "../../components/Title";
import { Grid, Paper } from "@mui/material";
import { useParams } from "react-router-dom";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { useGetMasterDataWithTypeQuery } from "../masterData/masterDataApiSlice";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";
import ClassGeneralInformation from "./ClassGeneralInformation";
import ClassStudent from "./ClassStudent";
import { useSnackbar } from "notistack";
import {
  useCreateClassMutation,
  useGetClassWithIdQuery,
} from "./classApiSlice";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const ClassInformation = () => {
  const { classId } = useParams();
  const viewMode = classId !== undefined;

  const [classIdParams, setClassIdParams] = useState(classId);

  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };

  const [value, setValue] = useState(0);
  const [studentNumber, setStudentNumber] = useState(0);
  const [className, setClassName] = useState("");
  const [semester, setSemester] = useState("");
  const [subject, setSubject] = useState("");
  const [trainer, setTrainer] = useState();

  // on change states
  const [selectedFile, setSelectedFile] = useState(null);
  const [excelFile, setExcelFile] = useState(null);
  const [excelFileError, setExcelFileError] = useState(null);

  // submit
  const [excelData, setExcelData] = useState(null);
  // it will contain array of objects

  const [classObj, setClassObj] = useState(null);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const {
    data: classData,
    isSuccess: getClassDataIsSuccess,
    isLoading: getClassDataIsLoading,
    isError: getCLassDataIsError,
    error: getClassDataError,
    refetch,
  } = useGetClassWithIdQuery(classIdParams, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (getCLassDataIsError && classIdParams !== undefined) {
      navigate("/dash-board/class-list");
      enqueueSnackbar(
        "Get class data fail : " + getClassDataError?.data?.message,
        { variant: "error" }
      );
    }
  }, [getCLassDataIsError]);

  useEffect(() => {
    if (getClassDataIsSuccess && classData && classId) {
      setClassObj(classData);
      setClassName(classData?.className);
      handleCloseBackDrop();
    }
    if (getClassDataIsLoading && classId) {
      handleOpenBackDrop();
    }
    if (getClassDataError && classId) {
      handleCloseBackDrop();
      enqueueSnackbar("Error : " + getClassDataError?.data?.message, {
        variant: "error",
      });
    }
  }, [getClassDataIsSuccess, getClassDataIsLoading, getCLassDataIsError]);

  const {
    data: masterDataSemester,
    isSuccess: masterDataIsSuccess,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery("Semester", {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const [
    createClass,
    {
      data: createdClassData,
      isSuccess: createClassIsSuccess,
      isLoading: createClassIsLoading,
      error: createClassError,
    },
  ] = useCreateClassMutation();

  const handleCreateClass = async () => {
    try {
      await createClass({
        className,
        subjectId: subject?._id,
        trainerId: trainer?._id,
        semesterId: semester?._id,
        studentIds: [],
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (createClassIsLoading) {
      handleOpenBackDrop();
    }
  }, [createClassIsLoading]);

  useEffect(() => {
    if (createClassIsSuccess) {
      const { createdClass } = createdClassData;
      setClassIdParams(createdClass?._id);
      enqueueSnackbar("Create new class successful !", { variant: "success" });
      handleCloseBackDrop();
    }
  }, [createClassIsSuccess]);

  useEffect(() => {
    if (createClassError) {
      enqueueSnackbar(
        "Create new class error : " + createClassError?.data?.message,
        { variant: "error" }
      );
      handleCloseBackDrop();
    }
  }, [createClassError]);

  return (
    <Paper
      sx={{
        p: 2,
        gap: 2,
      }}
    >
      {viewMode ? <Title>Class Details</Title> : <Title>New Class</Title>}
      <Grid container spacing={3}>
        <Box sx={{ width: "100%" }}>
          <Box
            sx={{
              ml: 2,
              mt: 2,
              borderBottom: 1,
              borderColor: "divider",
            }}
          >
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="basic tabs example"
            >
              <Tab label="Class General Information" {...a11yProps(0)} />
              <Tab label="Class Student" {...a11yProps(1)} />
            </Tabs>
          </Box>
          <TabPanel value={value} index={0}>
            <ClassGeneralInformation
              viewMode={viewMode}
              masterDataSemester={masterDataSemester}
              classObj={classObj}
              studentNumber={studentNumber}
              className={className}
              setClassName={setClassName}
              semester={semester}
              setSemester={setSemester}
              trainer={trainer}
              setTrainer={setTrainer}
              subject={subject}
              setSubject={setSubject}
              handleCreateClass={handleCreateClass}
              refetch={refetch}
              classData={classData}
            />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <ClassStudent
              viewMode={viewMode}
              classObj={classObj}
              excelFile={excelFile}
              excelData={excelData}
              excelFileError={excelFileError}
              selectedFile={selectedFile}
              setSelectedFile={setSelectedFile}
              setExcelData={setExcelData}
              setExcelFile={setExcelFile}
              setExcelFileError={setExcelFileError}
              enqueueSnackbar={enqueueSnackbar}
              handleOpenBackDrop={handleOpenBackDrop}
              handleCloseBackDrop={handleCloseBackDrop}
              setClassObj={setClassObj}
            />
          </TabPanel>
        </Box>
      </Grid>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={openBackDrop}
        onClick={handleCloseBackDrop}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </Paper>
  );
};

export default ClassInformation;
