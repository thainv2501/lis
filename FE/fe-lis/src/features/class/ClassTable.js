import FindInPageIcon from "@mui/icons-material/FindInPage";
import { Box, Button, Tooltip } from "@mui/material";
import Chip from "@mui/material/Chip";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { React, useEffect, useState } from "react";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { useUpdateClassMutation } from "./classApiSlice";
const ClassTable = ({
  page,
  classData,
  navigate,
  enqueueSnackbar,
  refetch,
}) => {
  const [openStatusDialog, setOpenStatusDialog] = useState(false);
  const [selectedClass, setSelectedClass] = useState(false);

  const [
    updateClass,
    {
      isSuccess: updateClassIsSuccess,
      isError: updateClassIsError,
      error: updateClassError,
    },
  ] = useUpdateClassMutation();
  const handleOpenStatusDialog = (data) => {
    setSelectedClass(data);
    setOpenStatusDialog(true);
  };
  const handleCloseStatusDialog = () => {
    setOpenStatusDialog(false);
  };
  const handleOpenView = (data) => {
    navigate(`/dash-board/classes-list/${data?._id}`);
  };
  const handleViewSchedule = (data) => {
    // navigate(`/dash-board/class-schedule/${data?._id}`);
    navigate(`/dash-board/class-schedules/${data?._id}`);
  };

  const handleChangeActiveStatus = async (data) => {
    try {
      await updateClass({ id: data?._id, active: !data?.active });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (updateClassIsSuccess) {
      enqueueSnackbar(`Update Class ${selectedClass?.className} is Success `, {
        variant: "success",
      });
      refetch();
      handleCloseStatusDialog();
    }
  }, [updateClassIsSuccess]);
  useEffect(() => {
    if (updateClassIsError) {
      enqueueSnackbar(`Update Class ${selectedClass?.className} is Fail `, {
        variant: "success",
      });
      handleCloseStatusDialog();
    }
  }, [updateClassIsError]);

  return (
    <>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell sx={{ color: "indianred" }}>No.</TableCell>
            <TableCell sx={{ color: "indianred" }}>Semester</TableCell>
            <TableCell sx={{ color: "indianred" }}>Class</TableCell>
            <TableCell sx={{ color: "indianred" }}>Trainer</TableCell>
            <TableCell sx={{ color: "indianred", width: "10%" }}>
              Status
            </TableCell>
            <TableCell align="center" sx={{ color: "indianred" }}>
              Actions
            </TableCell>
            <TableCell align="center" sx={{ color: "indianred" }}>
              Schedule
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {classData.map((data, index) => (
            <TableRow key={data?._id} hover>
              <TableCell sx={{ wordBreak: "break-word" }}>
                {(page - 1) * 10 + (index + 1)}
              </TableCell>
              <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                {data?.semester?.name}
              </TableCell>
              <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                {data?.className}-{data?.subject?.subjectCode}
              </TableCell>
              <TableCell>{data?.trainer?.email}</TableCell>
              <TableCell>
                <Button onClick={() => handleOpenStatusDialog(data)}>
                  {data?.active ? (
                    <Chip label="Active" color="success" variant="outlined" />
                  ) : (
                    <Chip
                      label="Deactivated"
                      color="error"
                      variant="outlined"
                    />
                  )}
                </Button>
              </TableCell>
              <TableCell>
                <Box display={"flex"} justifyContent={"center"}>
                  <Tooltip title="View Details" placement="top" arrow>
                    <Button
                      color="success"
                      onClick={() => handleOpenView(data)}
                    >
                      <FindInPageIcon />
                    </Button>
                  </Tooltip>
                </Box>
              </TableCell>
              <TableCell>
                <Button onClick={() => handleViewSchedule(data)}>View</Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      {/* Status dialog */}
      <Dialog open={openStatusDialog} onClose={handleCloseStatusDialog}>
        <DialogTitle>Change Status</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Change status of data : {selectedClass?.className}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleCloseStatusDialog}
            variant="contained"
            color="warning"
          >
            Cancel
          </Button>
          <Button
            onClick={() => handleChangeActiveStatus(selectedClass)}
            variant="contained"
            color="success"
          >
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ClassTable;
