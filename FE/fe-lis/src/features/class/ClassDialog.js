import FindInPageIcon from '@mui/icons-material/FindInPage';
import {
  Box,
  Button,
  Divider,
  Grid,
  Pagination,
  Paper,
  Skeleton,
  Typography,
  Autocomplete,
  TextField,
} from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useSnackbar } from 'notistack';
import { React, useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import IconTextField from '../../components/IconTextField';
import Title from '../../components/Title';
import { useGetClassWithSearchQuery } from './classApiSlice';
import useAuth from '../../hooks/useAuth';
function ClassDialog({
  setClassMeeting,
  semesters,
  currentSemester,
  handleCloseClassDialog,
}) {
  const { userLogin, isAdmin, isTrainer, isTrainee } = useAuth();
  const userId = userLogin?._id;
  const navigate = useNavigate();
  const searchURLDefault = `?page=1&semester=${currentSemester?._id}&active=active&keyword=`;
  const [search, setSearch] = useState(searchURLDefault);
  const [status, setStatus] = useState('active');
  const [semesterId, setSemesterId] = useState('');
  const [keyword, setKeyword] = useState('');
  const [page, setPage] = useState(1);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    setSearch(searchURLDefault);
  }, [searchURLDefault]);

  useEffect(() => {
    setKeyword(new URLSearchParams(search).get('keyword') || '');
    setSemesterId(
      new URLSearchParams(search).get('semester') || currentSemester?._id
    );
    setStatus(new URLSearchParams(search).get('active') || 'active');
    setPage(parseInt(new URLSearchParams(search).get('page')) || 1);
  }, [search]);

  const handleKeywordChanged = (event) => setKeyword(event.target.value);

  const handleSearch = () => {
    let query = new URLSearchParams({
      page: 1,
      semester: semesterId,
      active: 'active',
      keyword,
    }).toString();
    setSearch('?' + query);
  };

  const handlePaginationChanged = (event, value) => {
    let query = new URLSearchParams({
      page: value,
      semester: semesterId,
      active: status,
      keyword,
    }).toString();
    setSearch('?' + query);
  };
  const {
    data: classDataRes,
    isSuccess: getClassDataIsSuccess,
    isError: getClassDataIsError,
    isLoading: getClassDataIsLoading,
    error: getClassDataError,
    refetch,
  } = useGetClassWithSearchQuery(search === '' ? searchURLDefault : search, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const handleSelectedClass = (classMeeting) => {
    setClassMeeting(classMeeting);
    handleCloseClassDialog();
  };

  let content;
  if (getClassDataIsLoading)
    content = (
      <Box sx={{ width: '100%' }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (getClassDataIsError) {
    content = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Grid
              item
              xs={12}
              display={'flex'}
              flexDirection={'row'}
              justifyContent={'space-between'}
            >
              <Title>Class List</Title>
            </Grid>
            <Box
              display={'flex'}
              justifyContent={'flex-start'}
              alignItems={'center'}
              marginBottom={2}
              marginTop={2}
            >
              <Box sx={{ marginRight: '24px' }}>
                <Autocomplete
                  sx={{ width: '250px' }}
                  id="size-small-outlined"
                  size="small"
                  onChange={(event, newValue) => setSemesterId(newValue?._id)}
                  value={
                    semesters.find(
                      (semester) => semester?._id === semesterId
                    ) || null
                  }
                  options={semesters}
                  getOptionLabel={(option) => option.name}
                  renderInput={(params) => (
                    <TextField {...params} label="SEMESTER" />
                  )}
                />
              </Box>
              <Box sx={{ marginRight: '24px' }}>
                <IconTextField
                  fullWidth
                  id="keyword"
                  label="Keyword"
                  name="keyword"
                  value={keyword}
                  onChange={handleKeywordChanged}
                  autoFocus
                  size="small"
                  iconEnd={<FindInPageIcon />}
                />
              </Box>
              <Box>
                <Button variant="contained" onClick={handleSearch}>
                  Search
                </Button>
              </Box>
            </Box>
            <Box display={'flex'} justifyContent={'space-between'}>
              <Title>Class Table</Title>
            </Box>
            <Grid>
              <Typography variant="h4" color={'red'}>
                {getClassDataError?.data.message}
              </Typography>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  }

  if (getClassDataIsSuccess) {
    const { classData, classCount } = classDataRes;
    let classDataRelated = [];
    classDataRelated = classData.filter((classObj) => {
      return classObj?.trainer?._id === userId;
    });

    const pages = Math.ceil(classDataRelated.length / 10);

    content = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Grid
              item
              xs={12}
              display={'flex'}
              flexDirection={'row'}
              justifyContent={'space-between'}
            >
              <Title>Class List</Title>
            </Grid>
            <Box
              display={'flex'}
              justifyContent={'flex-start'}
              alignItems={'center'}
              marginBottom={2}
              marginTop={2}
            >
              <Box sx={{ marginRight: '24px' }}>
                <Autocomplete
                  sx={{ width: '250px' }}
                  id="size-small-outlined"
                  size="small"
                  onChange={(event, newValue) => setSemesterId(newValue?._id)}
                  value={
                    semesters.find(
                      (semester) => semester?._id === semesterId
                    ) || null
                  }
                  options={semesters}
                  getOptionLabel={(option) => option.name}
                  renderInput={(params) => (
                    <TextField {...params} label="SEMESTER" />
                  )}
                />
              </Box>
              <Box sx={{ marginRight: '24px' }}>
                <IconTextField
                  fullWidth
                  id="keyword"
                  label="Keyword"
                  name="keyword"
                  value={keyword}
                  onChange={handleKeywordChanged}
                  autoFocus
                  size="small"
                  iconEnd={<FindInPageIcon />}
                />
              </Box>
              <Box>
                <Button variant="contained" onClick={handleSearch}>
                  Search
                </Button>
              </Box>
            </Box>
            <Divider sx={{ mb: 2 }}></Divider>
            <Box display={'flex'} justifyContent={'space-between'}>
              <Title>Class Table</Title>
            </Box>
            <Grid>
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell sx={{ color: 'indianred', width: '10%' }}>
                      No.
                    </TableCell>
                    <TableCell sx={{ color: 'indianred', width: '20%' }}>
                      Semester
                    </TableCell>
                    <TableCell sx={{ color: 'indianred', width: '40%' }}>
                      Class
                    </TableCell>
                    <TableCell sx={{ color: 'indianred', width: '30%' }}>
                      Trainer
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {classDataRelated.map((data, index) => (
                    <TableRow
                      key={data?._id}
                      hover
                      sx={{ cursor: 'pointer' }}
                      onClick={() => handleSelectedClass(data)}
                    >
                      <TableCell sx={{ wordBreak: 'break-word' }}>
                        {(page - 1) * 10 + (index + 1)}
                      </TableCell>
                      <TableCell sx={{ wordBreak: 'break-word', width: '20%' }}>
                        {data?.semester?.name}
                      </TableCell>
                      <TableCell sx={{ wordBreak: 'break-word', width: '20%' }}>
                        {data?.className}-{data?.subject?.subjectCode}
                      </TableCell>
                      <TableCell>{data?.trainer?.email}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Grid>
            <Pagination
              count={pages}
              page={page}
              variant="outlined"
              color="primary"
              sx={{ paddingTop: '20px' }}
              onChange={handlePaginationChanged}
            />
          </Paper>
        </Grid>
      </Grid>
    );
  }

  return content;
}

export default ClassDialog;
