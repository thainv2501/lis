import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { React, useEffect, useState } from "react";
import dayjs from "dayjs";
import * as XLSX from "xlsx";
import { enqueueSnackbar } from "notistack";
import useAuth from "../../hooks/useAuth";
import { useImportClassesMutation } from "./classApiSlice";

const ImportClassFromXLSX = ({
  handleCloseImportXLSXDialog,
  openImportXLSXDialog,
  refetch,
}) => {
  const { userLogin } = useAuth();
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };

  const [
    importClasses,
    {
      isSuccess: importClassesIsSuccess,
      isError: importClassesIsError,
      isLoading: importClassesIsLoading,
      error: importClassesFromXLSXError,
    },
  ] = useImportClassesMutation();
  // on change states
  const [selectedFile, setSelectedFile] = useState(null);
  const [excelFile, setExcelFile] = useState(null);
  const [excelFileError, setExcelFileError] = useState(null);

  // submit
  const [excelData, setExcelData] = useState(null);
  // it will contain array of objects

  const [classes, setClasses] = useState(null);
  // handle File
  const fileType = [
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ];

  const checkValidSheet = (worksheet) => {
    try {
      return (
        worksheet?.A1.v.trim() === "Semester Code" &&
        worksheet?.B1.v.trim() === "Class Name" &&
        worksheet?.C1.v.trim() === "Subject Code" &&
        worksheet?.D1.v.trim() === "Trainer Email"
      );
    } catch (error) {
      return false;
    }
  };

  const reformExcelDataToClassesData = (excelData) => {
    let classesData = [];

    if (excelData !== null) {
      excelData.forEach((data) => {
        try {
          const semesterCode = data["Semester Code"].trim();
          const className = data["Class Name"].trim();
          const subjectCode = data["Subject Code"].trim();
          const trainerEmail = data["Trainer Email"].trim();

          const newClass = {
            semesterCode,
            className,
            subjectCode,
            trainerEmail,
          };

          classesData.push(newClass); // Push the new schedule into the array
        } catch (error) {
          // If any error occurs during processing, skip this data item
          return;
        }
      });
    }

    return classesData;
  };

  const handleFile = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  // submit function
  const handleSubmit = () => {
    if (excelFile !== null) {
      const workbook = XLSX.read(excelFile, { type: "buffer" });
      const worksheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[worksheetName];
      const data = XLSX.utils.sheet_to_json(worksheet);
      if (checkValidSheet(worksheet)) {
        setExcelData(data);
      } else {
        setExcelFileError("Please make sure your import right file !");
        setExcelFile(null);
        setExcelData(null);
        setClasses(null);
      }
    } else {
      setExcelData(null);
    }
  };

  useEffect(() => {
    if (selectedFile) {
      if (selectedFile && fileType.includes(selectedFile.type)) {
        let reader = new FileReader();
        reader.readAsArrayBuffer(selectedFile);
        reader.onload = (e) => {
          setExcelFileError(null);
          setExcelFile(e.target.result);
        };
      } else {
        setExcelFileError("Please select only excel file types");
        setExcelFile(null);
        setExcelData(null);
        setClasses(null);
      }
    } else {
      console.log("please select your xlsx file");
    }
  }, [selectedFile]);

  useEffect(() => {
    if (excelFile) {
      handleSubmit();
    }
  }, [excelFile]);

  useEffect(() => {
    if (excelData) {
      setClasses(reformExcelDataToClassesData(excelData));
    } else {
      setClasses(null);
    }
  }, [excelData]);

  const xlsxDataTableContent = (
    <>
      <Paper sx={{ p: 3, mb: 3 }}>
        <Grid container justifyContent={"space-between"}>
          <Grid item>
            <Typography variant="h6">XLSX Data Table</Typography>
          </Grid>
        </Grid>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell sx={{ color: "indianred" }}>No.</TableCell>
              <TableCell sx={{ color: "indianred" }}>Semester Code</TableCell>
              <TableCell sx={{ color: "indianred" }}>Class Name</TableCell>
              <TableCell sx={{ color: "indianred" }}>Subject Code</TableCell>
              <TableCell sx={{ color: "indianred" }}>Trainer Email</TableCell>
            </TableRow>
          </TableHead>
          {classes !== null ? (
            <TableBody>
              {classes.map((item, index) => (
                <TableRow key={index}>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {item.semesterCode}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {item.className}
                  </TableCell>
                  <TableCell>{item?.subjectCode}</TableCell>
                  <TableCell>{item?.trainerEmail}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          ) : null}
        </Table>
      </Paper>
    </>
  );

  const handleImportXlSXData = async () => {
    try {
      handleOpenBackDrop();
      await importClasses({
        excelData: classes,
      });
    } catch (error) {
      enqueueSnackbar("Error while import from XlSX", { variant: "error" });
    }
  };

  useEffect(() => {
    if (importClassesIsLoading) {
      handleOpenBackDrop();
    }
    if (importClassesIsError) {
      enqueueSnackbar(
        "Import from XLSX Error : " + importClassesFromXLSXError?.data?.message,
        { variant: "error" }
      );
      handleCloseBackDrop();
    }
    if (importClassesIsSuccess) {
      enqueueSnackbar("Import from XLSX Success", { variant: "success" });
      handleCloseBackDrop();
      handleCloseImportXLSXDialog();
      setExcelFile(null);
      setExcelData(null);
      setClasses(null);
      refetch();
    }
  }, [importClassesIsSuccess, importClassesIsError, importClassesIsLoading]);

  const handleExportXLSX = () => {
    const dataSheet = [1, 2, 3, 4, 5].map((index) => {
      return {
        "Semester Code": `SC-Example${index}`,
        "Class Name": `CN-Example${index}`,
        "Subject Code": `SJC-Example${index}`,
        "Trainer Email": `Email-Example${index}@gmail.com`,
      };
    });
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(dataSheet);
    XLSX.utils.book_append_sheet(wb, ws, "MySheet1");
    XLSX.writeFile(wb, `SemesterCode-ClassesName-ClassesImportTemplate.xlsx`);
  };

  return (
    <>
      <Dialog
        open={openImportXLSXDialog}
        keepMounted
        onClose={handleCloseImportXLSXDialog}
        aria-describedby="groups-dialog-slide-description"
        maxWidth="lg"
        className="dialog-groups"
      >
        <DialogTitle sx={{ padding: "0" }}>
          <Paper
            sx={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              paddingRight: "10px",
              paddingLeft: "10px",
              alignItems: "center",
            }}
          >
            <Box>Import Classes</Box>
            <Box>
              <IconButton>
                <CloseIcon
                  fontSize="inherit"
                  onClick={handleCloseImportXLSXDialog}
                />
              </IconButton>
            </Box>
          </Paper>
        </DialogTitle>
        <Grid
          container
          justifyContent={"center"}
          sx={{
            borderBottom: "1px solid #c8c8c8",
            padding: "24px 0",
            width: "100%",
          }}
        >
          <span>You could download template here</span>
          <Button onClick={handleExportXLSX}>Download</Button>
        </Grid>
        <DialogContent sx={{ width: "1200px" }}>
          <Grid container justifyContent={"center"} mt={2}>
            <span>You need to upload correct .xlsx file to import classes</span>
          </Grid>
          <Grid container justifyContent={"center"} padding={3}>
            <label htmlFor="upload-xlsx">
              <input
                style={{ display: "none" }}
                id="upload-xlsx"
                name="upload-xlsx"
                type="file"
                onChange={handleFile}
              />
              <Button color="success" variant="contained" component="span">
                Upload
              </Button>
            </label>
          </Grid>
          <Grid container justifyContent={"center"}>
            <Typography
              variant="subtitle1"
              color={excelFileError !== null ? "red" : ""}
            >
              {excelFileError !== null ? excelFileError : selectedFile?.name}
            </Typography>
          </Grid>
          <Grid>{xlsxDataTableContent}</Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseImportXLSXDialog}>Cancel</Button>
          <Button
            variant="contained"
            color="primary"
            disabled={excelData == null}
            onClick={handleImportXlSXData}
          >
            Save
          </Button>
        </DialogActions>
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>
    </>
  );
};

export default ImportClassFromXLSX;
