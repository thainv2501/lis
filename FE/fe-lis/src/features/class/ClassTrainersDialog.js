import SearchIcon from "@mui/icons-material/Search";
import {
  Box,
  Grid,
  Pagination,
  Paper,
  Skeleton,
  Typography,
} from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import { React } from "react";
import Title from "../../components/Title";
import { useGetSubjectWithSubjectIdQuery } from "../subjects/subjectsApiSlice";
const ClassTrainersDialog = ({
  subjectId,
  setTrainer,
  selectedTrainer,
  handleCloseAddTrainersDialog,
  handleScheduleObjChange,
}) => {
  const {
    data: subject,
    isSuccess: getSubjectIsSuccess,
    isError: getSubjectIsError,
    isLoading: getSubjectIsLoading,
    error: getSubjectError,
    refetch,
  } = useGetSubjectWithSubjectIdQuery(subjectId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });


  const handleSelectedTrainer = (trainer) => {
    if (setTrainer) {
      setTrainer(trainer);
    }
    if (handleScheduleObjChange) {
      handleScheduleObjChange("trainer", trainer);
    }
    handleCloseAddTrainersDialog();
  };

  let content;

  if (getSubjectIsLoading)
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (getSubjectIsError) {
    content = (
      <Paper sx={{ p: 2 }}>
        <Box>
          <Box
            sx={{
              pt: 2,
              pb: 2,
              display: "flex",
              justifyContent: "space-between",
            }}
          ></Box>

          {/* end Filter */}
          <Grid container spacing={3}>
            {/* Recent Dash Board */}
            <Grid item xs={12}>
              <Typography variant="h4" color={"red"}>
                {getSubjectError.data?.message}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    );
  }

  if (getSubjectIsSuccess) {
    content = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
            <Title>Trainers</Title>
            <Table size="small">
              <TableHead>
                <TableRow>
                  <TableCell sx={{ color: "indianred" }}>No.</TableCell>
                  <TableCell sx={{ color: "indianred", width: "30%" }}>
                    Full Name
                  </TableCell>
                  <TableCell sx={{ color: "indianred" }}>
                    Phone number
                  </TableCell>
                  <TableCell sx={{ color: "indianred" }}>Email</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {subject?.trainers
                  .filter((trainer) => trainer?._id !== selectedTrainer?._id)
                  .map((trainer, index) => (
                    <TableRow
                      onClick={() => handleSelectedTrainer(trainer)}
                      key={index}
                      sx={{
                        "&:last-child td, &:last-child th": {
                          borderBottom: "1px solid rgba(224, 224, 224, 1)",
                        },
                        "&:hover": {
                          backgroundColor: "#f5f5f5", // Change this to the desired hover color
                        },
                      }}
                    >
                      <TableCell sx={{ wordBreak: "break-word" }}>
                        {index + 1}
                      </TableCell>
                      <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                        {trainer?.fullName}
                      </TableCell>
                      <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                        {trainer?.phoneNumber}
                      </TableCell>
                      <TableCell>{trainer?.email}</TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Grid>
    );
  }

  return content;
};

export default ClassTrainersDialog;
