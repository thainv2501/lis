import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import CastForEducationIcon from '@mui/icons-material/CastForEducation';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import OndemandVideoIcon from '@mui/icons-material/OndemandVideo';
import RefreshIcon from '@mui/icons-material/Refresh';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Backdrop,
  Box,
  Breadcrumbs,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Typography,
  Chip,
  DialogContent,
  TextField,
  DialogActions,
  Dialog,
  DialogTitle,
  FormControlLabel,
  Checkbox,
  Autocomplete,
} from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Divider from '@mui/material/Divider';
import Skeleton from '@mui/material/Skeleton';
import dayjs from 'dayjs';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { MobileDateTimePicker } from '@mui/x-date-pickers/MobileDateTimePicker';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import {
  scheduleApiSlice,
  useGetSchedulesQuery,
} from '../schedule/scheduleApiSlice';
import {
  useGetMeetingsWithClassIdQuery,
  useAddNewMeetingMutation,
  useDeleteMeetingMutation,
  useUpdateMeetingTimeMutation,
} from '../meeting/meetingsApiSlice';
import { useGetClassWithIdQuery } from './classApiSlice';
import CreateGroupDialog from '../groups/CreateGroupDialog';
import useAuth from '../../hooks/useAuth';
import { generateMeetingId } from '../../utils/generateMeetingId';
import TimePickerDialog from './TimePickerDialog';
import { useGetClassBySemesterIdQuery } from '../class/classApiSlice';
const UserClassDetails = () => {
  const { userLogin, isAdmin, isTrainer, isTrainee } = useAuth();
  const userId = userLogin?._id;
  const navigate = useNavigate();
  const location = useLocation();
  const [page, setPage] = useState(1);
  const [selectedSchedule, setSelectedSchedule] = useState(null);
  const searchParams = new URLSearchParams(location.search);
  const classId = searchParams.get('classId');
  const semesterId = searchParams.get('semesterId');
  // const { classId } = useParams();
  const [classIdObj, setClassIdObj] = useState(classId);

  const { enqueueSnackbar } = useSnackbar();

  // open dialog create groups
  const [openCreateGroupsDialog, setOpenCreateGroupDialog] = useState(false);
  const handleClickOpenCreateGroupsDialog = (schedule) => {
    setSelectedSchedule(schedule);
    setOpenCreateGroupDialog(true);
  };

  const handleCloseCreateGroupsDialog = () => {
    setOpenCreateGroupDialog(false);
  };

  const {
    data: classData,
    isSuccess: getClassDataIsSuccess,
    isLoading: getClassDataIsLoading,
    isError: getClassDataIsError,
    error: getClassDataError,
  } = useGetClassWithIdQuery(classIdObj, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
    pollingInterval: 60000 * 5,
  });

  useEffect(() => {
    if (getClassDataIsError && classId !== undefined) {
      navigate('/common/courses-list');
      enqueueSnackbar(
        'Get class data fail : ' + getClassDataError?.data?.message,
        { variant: 'error' }
      );
    }
  }, [getClassDataIsError]);

  const [classes, setClasses] = useState([]);
  const {
    data: classDataBySemester,
    isSuccess: getClassDataBySemesterIsSuccess,
    isLoading: getClassDataBySemesterIsLoading,
    isError: getCLassDataBySemesterIsError,
    error: getClassDataBySemesterError,
  } = useGetClassBySemesterIdQuery(semesterId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (classDataBySemester && classData) {
      const filteredClasses = classDataBySemester.filter((classItem) => {
        return classItem?.subject?._id === classData?.subject?._id;
      });
      setClasses(filteredClasses);
    }
  }, [classDataBySemester, classData]);

  useEffect(() => {
    searchParams.set('classId', classIdObj);

    const newUrl = `${location.pathname}?${searchParams.toString()}`;

    window.history.replaceState(null, '', newUrl);
  }, [classIdObj, location.pathname]);

  const handleClassIdFilterChange = (event) => {
    handleOpenBackdropSchedules();
    setClassIdObj(event.target.value);
  };

  const {
    data: schedulesDataRes,
    isLoading: getSchedulesDataIsLoading,
    isSuccess: getSchedulesDataIsSuccess,
    isError: getSchedulesDataIsError,
    error: getSchedulesDataError,
    refetch,
  } = useGetSchedulesQuery(`?classId=${classIdObj}&page=${page}`, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const [openBackdropSchedules, setOpenBackdropSchedules] = useState(false);
  const [isRefetchingSchedules, setIsRefetchingSchedules] = useState(false);

  const handleOpenBackdropSchedules = () => {
    setIsRefetchingSchedules(true);
    setOpenBackdropSchedules(true);
    refetch().then(() => {
      setIsRefetchingSchedules(false);
    });
  };

  const handleCloseBackdropSchedules = () => {
    setOpenBackdropSchedules(false);
  };

  useEffect(() => {
    if (isRefetchingSchedules) {
      handleOpenBackdropSchedules();
    } else {
      handleCloseBackdropSchedules();
    }
  }, [isRefetchingSchedules]);

  const handlePaginationChanged = (event, value) => {
    setPage(value);
  };

  const handleSlotTime = (slot) => {
    let slotStartAtFloat = 7.5 + (slot - 1) * 1.5;
    if (slot >= 4) {
      slotStartAtFloat = 12.5 + (slot - 4) * 1.5;
    }
    const hours = Math.floor(slotStartAtFloat);
    const minutes = Math.floor((slotStartAtFloat - hours) * 60);
    const slotStartAtTime = dayjs().hour(hours).minute(minutes).format('HH:mm');
    const slotEndAtTime = dayjs()
      .hour(hours + 1)
      .minute(minutes + 30)
      .format('HH:mm');

    return `${slotStartAtTime} - ${slotEndAtTime}`;
  };

  const [openBackdrop, setOpenBackdrop] = useState(false);
  const [isRefetching, setIsRefetching] = useState(false);
  const {
    data: meetings,
    isSuccess: getMeetingsIsSuccess,
    isLoading: getMeetingsIsLoading,
    isError: getMeetingsIsError,
    refetch: getMeetingsRefetch,
  } = useGetMeetingsWithClassIdQuery(classIdObj, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const handleOpenBackdrop = () => {
    setIsRefetching(true);
    setOpenBackdrop(true);
    getMeetingsRefetch().then(() => {
      setIsRefetching(false);
    });
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  useEffect(() => {
    if (isRefetching) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [isRefetching]);

  // open dialog add class meeting
  const [openAddClassMeetingDialog, setOpenAddClassMeetingDialog] =
    useState(false);
  const handleClickOpenAddClassMeetingDialog = () => {
    setOpenAddClassMeetingDialog(true);
  };

  const handleCloseAddClassMeetingDialog = () => {
    setOpenAddClassMeetingDialog(false);
  };
  const [selectedMeeting, setSelectedMeeting] = useState(null);
  const [meetingName, setMeetingName] = useState('');
  const [selectedUser, setSelectedUser] = useState([]);
  const [dateTimeMeetingFrom, setDateTimeMeetingFrom] = useState(dayjs());
  const [dateTimeMeetingTo, setDateTimeMeetingTo] = useState(dayjs());
  const [classMeeting, setClassMeeting] = useState('');
  const [meetingNameError, setMeetingNameError] = useState(false);
  const [meetingNameHelperText, setMeetingNameHelperText] = useState('');

  const meetingNameRegExp = /^[\p{L}0-9\s]{3,50}$/u;

  const handleMeetingNameChange = (e) => {
    const value = e.target.value;
    // Check if the input matches the regular expression
    if (value.trim().length === 0) {
      setMeetingNameError(true);
      setMeetingNameHelperText('Meeting Name is required!');
    } else if (meetingNameRegExp.test(value)) {
      setMeetingNameError(false);
      setMeetingNameHelperText('');
    } else {
      setMeetingNameError(true);
      setMeetingNameHelperText('Meeting Name is invalid!');
    }
    setMeetingName(value);
  };

  // select with class meeting
  const [selectAll, setSelectAll] = useState(false);
  const [selectedStudents, setSelectedStudents] = useState([]);

  useEffect(() => {
    // Check if all students are selected
    if (selectedStudents.length === classData?.students?.length) {
      setSelectAll(true);
    } else {
      setSelectAll(false);
    }
  }, [selectedStudents, classData]);

  useEffect(() => {
    if (selectAll) {
      setSelectedStudents(classData?.students || []);
    }
  }, [selectAll, classData]);

  const handleToggleSelectAll = () => {
    setSelectAll((prev) => !prev);
    setSelectedStudents([]);
  };

  const handleUserSelection = (event, newValue) => {
    setSelectedStudents(newValue);
  };

  useEffect(() => {
    if (!selectAll) {
      setSelectedUser(selectedStudents.map((user) => user?._id));
    }
  }, [selectAll, selectedStudents, setSelectedUser]);

  const handleDateTimeMeetingFromChange = (date) => {
    setDateTimeMeetingFrom(date);
  };

  const handleDateTimeMeetingToChange = (date) => {
    setDateTimeMeetingTo(date);
  };

  // Create one to one meeting
  const setEmptyMeetingObj = () => {
    setMeetingName('');
    setSelectedUser([]);
    setSelectedStudents([]);
    setSelectAll(false);
    setDateTimeMeetingFrom(null);
    setDateTimeMeetingTo(null);
  };

  const [
    createNewMeeting,
    {
      isSuccess: createMeetingIsSuccess,
      isError: createMeetingIsError,
      error: createMeetingError,
    },
  ] = useAddNewMeetingMutation();

  // Create video conference
  const handleAddNewVideoConference = async () => {
    if (dayjs(dateTimeMeetingFrom).isAfter(dayjs(dateTimeMeetingTo))) {
      enqueueSnackbar('Your From Date is invalid compare to End Date', {
        variant: 'error',
      });
    } else {
      const meetingId = generateMeetingId();
      try {
        await createNewMeeting({
          createdBy: userId,
          meetingId: meetingId,
          meetingName: meetingName,
          meetingType: 'video-conference',
          invitedUsers: selectedUser,
          meetingFrom: dateTimeMeetingFrom,
          meetingTo: dateTimeMeetingTo,
          maxUsers: selectedUser && selectedUser.length,
          classId: classId,
          status: true,
        });
      } catch (error) {
        enqueueSnackbar('Create failed!: ' + error, {
          variant: 'error',
        });
      }
    }
  };

  useEffect(() => {
    if (createMeetingIsSuccess) {
      enqueueSnackbar('Create new meeting successfully!', {
        variant: 'success',
      });
      setEmptyMeetingObj();
      setOpenAddClassMeetingDialog(false);
      getMeetingsRefetch();
    }
    if (createMeetingIsError) {
      enqueueSnackbar('Create failed!: ' + createMeetingError?.data?.message, {
        variant: 'error',
      });
      setEmptyMeetingObj();
      setOpenAddClassMeetingDialog(false);
    }
  }, [createMeetingIsError, createMeetingIsSuccess]);

  // delete meeting
  const [
    deleteMeeting,
    {
      isSuccess: deleteMeetingIsSuccess,
      isLoading: deleteMeetingIsLoading,
      isError: deleteMeetingIsError,
      error: deleteMeetingError,
    },
  ] = useDeleteMeetingMutation();

  const [openDeleteMeetingDialog, setOpenDeleteMeetingDialog] = useState(false);

  const handleDeleteClicked = (data) => {
    setSelectedMeeting(data);
    handleClickOpenDeleteMeetingDialog();
  };
  const handleClickOpenDeleteMeetingDialog = () => {
    setOpenDeleteMeetingDialog(true);
  };

  const handleCloseDeleteMeetingDialog = () => {
    setOpenDeleteMeetingDialog(false);
  };

  const handleDeleteMeeting = async () => {
    try {
      await deleteMeeting({
        id: selectedMeeting?._id,
      }).then(() => {
        enqueueSnackbar(
          `Remove ${selectedMeeting?.meetingName} information is successfully!`,
          {
            variant: 'success',
          }
        );
      });
      getMeetingsRefetch();
      setSelectedMeeting(null);
      handleCloseDeleteMeetingDialog();
    } catch (error) {
      enqueueSnackbar('Remove failed!: ' + error, {
        variant: 'error',
      });
    }
  };

  const [openTimePickerDialog, setOpenTimePickerDialog] = useState(false);
  const handleOpenTimePickerDialog = (meeting) => {
    setSelectedMeeting(meeting);
    setOpenTimePickerDialog(true);
  };
  const handleCloseTimePickerDialog = () => {
    setOpenTimePickerDialog(false);
  };

  const [
    updateMeetingTime,
    {
      isSuccess: updateMeetingTimeIsSuccess,
      isLoading: updateMeetingTimeIsLoading,
      isError: updateMeetingTimeIsError,
      error: updateMeetingTimeError,
    },
  ] = useUpdateMeetingTimeMutation();
  useEffect(() => {
    if (updateMeetingTimeIsSuccess) {
      handleCloseTimePickerDialog();
      enqueueSnackbar('Class Meeting Started!', { variant: 'success' });
      getMeetingsRefetch();
    }
    if (updateMeetingTimeIsLoading) {
    }
    if (updateMeetingTimeIsError) {
      enqueueSnackbar(
        'Meeting Time Setup Fail : ' + updateMeetingTimeError?.message,
        { variant: 'error' }
      );
    }
  }, [
    updateMeetingTimeIsSuccess,
    updateMeetingTimeIsError,
    updateMeetingTimeIsLoading,
  ]);
  let content;
  if (schedulesDataRes?.schedulesCount === 0) {
    content = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Typography variant="h5" color={'red'}>
            NO SCHEDULE FOUND !
          </Typography>
        </Grid>
      </Grid>
    );
  }
  if (getClassDataIsLoading || getSchedulesDataIsLoading)
    content = (
      <Box sx={{ width: '100%' }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (schedulesDataRes?.schedulesCount !== 0 && getSchedulesDataIsSuccess) {
    const { schedules, schedulesCount } = schedulesDataRes;
    const pages = Math.ceil(schedulesCount / 10);
    content = (
      <>
        <Grid sx={{ justifyContent: 'center' }}>
          <Grid
            container
            alignItems={'center'}
            justifyContent="center"
            margin={'20px'}
            sx={{ mt: 6 }}
          >
            <Box sx={{ xs: 12 }}>
              <Pagination
                count={pages}
                page={page}
                onChange={handlePaginationChanged}
                variant="outlined"
                shape="rounded"
              />
            </Box>
            {/* <Button
              variant="contained"
              style={{ height: "32px", marginLeft: "12px" }}
            >
              ALL
            </Button> */}
            <Grid container justifyContent="center" sx={{ mt: 2 }}>
              <Typography>{schedules.length} of 10 items</Typography>
            </Grid>
          </Grid>
        </Grid>
        {schedules?.map((schedule, index) => (
          <Accordion key={schedule?._id} style={{ marginBottom: '24px' }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              sx={{
                border: '1px solid rgba(0, 0, 0, 0.12)',
                backgroundColor: '#F7F7F7',
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'flex-end',
              }}
            >
              <Box style={{ width: '100%', paddingBottom: '40px' }}>
                <Box className="slot-header">
                  <Grid container justifyContent={'space-between'}>
                    <Grid item xs={10}>
                      <Typography
                        variant="body2"
                        className="slot"
                        style={{
                          color: '#0078D8',
                          display: 'inline-block',
                          backgroundColor: '#D1DFF9',
                          padding: '5px',
                          borderRadius: '5px',
                        }}
                      >
                        Slot {(page - 1) * 10 + index + 1}
                      </Typography>
                    </Grid>
                    <Grid item container xs={2} justifyContent={'flex-end'}>
                      <Button variant="body2" className="slot">
                        <Link
                          to={`/common/courses-list/slot-details/${
                            schedule?._id
                          }?slot=${(page - 1) * 10 + index + 1}`}
                          style={{ color: '#0078D8' }}
                        >
                          Details
                        </Link>
                      </Button>
                      {userLogin._id === schedule.trainer || isAdmin ? (
                        <Button
                          onClick={() =>
                            handleClickOpenCreateGroupsDialog(schedule)
                          }
                        >
                          <GroupAddIcon />
                          Groups
                        </Button>
                      ) : null}
                    </Grid>
                  </Grid>

                  <Box>
                    <CalendarMonthIcon></CalendarMonthIcon>
                    <Typography
                      variant="body2"
                      component="span"
                      className="time"
                      color={'grey'}
                    >
                      {dayjs(schedule.day).format('DD/MM/YYYY')}
                      {' at '}
                      {handleSlotTime(schedule.slotInDay)}
                    </Typography>
                  </Box>
                </Box>
                <Box className="text-view-detail" style={{ marginTop: '5px' }}>
                  <Box>
                    <p>
                      <b>
                        <p
                          className="bevietnampro"
                          style={{ fontSize: '16px' }}
                        >
                          <Grid container spacing={3}>
                            {schedule.lessons.map((lesson, index) => (
                              <>
                                <Grid key={lesson._id} item xs={12}>
                                  <Typography>
                                    Chapter {lesson.lessonChapter}
                                  </Typography>
                                  <Typography>
                                    Lesson {lesson.lessonNumber} :{' '}
                                    {lesson.lessonContent}
                                  </Typography>
                                </Grid>
                              </>
                            ))}
                            <Grid item xs={12}>
                              <Typography> {schedule.content}</Typography>
                            </Grid>
                          </Grid>
                        </p>
                      </b>
                    </p>
                  </Box>
                </Box>
              </Box>
            </AccordionSummary>
            <AccordionDetails
              style={{
                border: '1px solid rgba(0, 0, 0, 0.12)',
                padding: 0,
              }}
            >
              <Grid container justifyContent="left" sx={{ mb: 2 }}>
                <div
                  style={{
                    width: '100%',
                  }}
                >
                  <div>
                    <div>
                      <div>
                        <div>
                          <div style={{ width: '100%' }}>
                            <div>
                              <div
                                style={{
                                  borderBottom: '1px solid rgba(0,0,0,.125)',
                                  width: '100%',
                                }}
                              >
                                <Typography
                                  sx={{
                                    padding: '0.5rem 1rem',
                                    width: '100%',
                                  }}
                                >
                                  QUESTION
                                </Typography>
                              </div>
                              <div>
                                {schedule.lessons &&
                                  schedule.lessons.map((lesson, index) =>
                                    lesson.constructiveQuestions.map(
                                      (constructiveQuestion, index) => (
                                        <Link
                                          to={`/common/question-details?cqid=${
                                            constructiveQuestion._id
                                          }&scheduleId=${schedule?._id}&slot=${
                                            (page - 1) * 10 + index + 1
                                          }`}
                                        >
                                          <Divider />
                                          <Box
                                            hover
                                            key={constructiveQuestion._id}
                                            sx={{
                                              ':hover': {
                                                bgcolor: '#D3D3D3',
                                                color: 'black',
                                              },
                                            }}
                                          >
                                            <div
                                              style={{
                                                display: 'flex',
                                                fontSize: '12px',
                                                width: '100%',
                                                padding: '0.5rem 1rem',
                                                justifyContent: 'space-between',
                                                alignItems: 'center',
                                              }}
                                            >
                                              <div
                                                style={{
                                                  display: 'flex',
                                                  alignItems: 'center',
                                                }}
                                              >
                                                <CastForEducationIcon
                                                  style={{ color: '#FD9246' }}
                                                />
                                                <span
                                                  style={{
                                                    width: '300px',
                                                    display: 'inherit',
                                                    marginLeft: '10px',
                                                  }}
                                                >
                                                  <Typography>
                                                    {constructiveQuestion.title}
                                                  </Typography>
                                                </span>
                                              </div>
                                              <div
                                                style={{
                                                  display: 'flex',
                                                  justifyContent: 'flex-end',
                                                  alignItems: 'center',
                                                }}
                                              >
                                                <span
                                                  style={{
                                                    marginRight: '8px',
                                                    padding: '4px 8px',
                                                    color: '#EB5757',
                                                    textAlign: 'right',
                                                  }}
                                                >
                                                  System
                                                </span>
                                                {constructiveQuestion?.from &&
                                                constructiveQuestion?.to &&
                                                dayjs(
                                                  constructiveQuestion?.from
                                                ).isBefore(dayjs()) &&
                                                dayjs(
                                                  constructiveQuestion?.to
                                                ).isAfter(dayjs()) ? (
                                                  <span
                                                    style={{
                                                      padding: '4px 8px',
                                                      background: '#DFF6DD',
                                                      color: '#00AC47',
                                                      borderRadius: '10px',
                                                    }}
                                                  >
                                                    On Going
                                                  </span>
                                                ) : constructiveQuestion?.done ? (
                                                  <span
                                                    style={{
                                                      padding: '4px 8px',
                                                      background: '#DFF6DD',
                                                      color: '#00AC47',
                                                      borderRadius: '10px',
                                                    }}
                                                  >
                                                    Finished
                                                  </span>
                                                ) : (
                                                  <span
                                                    style={{
                                                      padding: '4px 8px',
                                                      background: '#cccccc',
                                                      color: '#404040',
                                                      borderRadius: '10px',
                                                    }}
                                                  >
                                                    Not Yet
                                                  </span>
                                                )}
                                              </div>
                                            </div>
                                          </Box>
                                        </Link>
                                      )
                                    )
                                  )}
                                {schedule.constructiveQuestions &&
                                  schedule.constructiveQuestions.map(
                                    (constructiveQuestion, index) => (
                                      <Link
                                        to={`/common/question-details?cqid=${
                                          constructiveQuestion._id
                                        }&scheduleId=${schedule?._id}&slot=${
                                          (page - 1) * 10 + index + 1
                                        }`}
                                      >
                                        <Divider />
                                        <Box
                                          key={constructiveQuestion._id}
                                          sx={{
                                            ':hover': {
                                              bgcolor: '#D3D3D3',
                                              color: 'black',
                                            },
                                          }}
                                        >
                                          <div
                                            style={{
                                              display: 'flex',
                                              fontSize: '12px',
                                              width: '100%',
                                              padding: '0.5rem 1rem',
                                              justifyContent: 'space-between',
                                              alignItems: 'center',
                                            }}
                                          >
                                            <div
                                              style={{
                                                display: 'flex',
                                                alignItems: 'center',
                                              }}
                                            >
                                              <CastForEducationIcon
                                                style={{ color: '#FD9246' }}
                                              />
                                              <span
                                                style={{
                                                  width: '300px',
                                                  display: 'inherit',
                                                  marginLeft: '10px',
                                                }}
                                              >
                                                <Typography>
                                                  {constructiveQuestion.title}
                                                </Typography>
                                              </span>
                                            </div>
                                            <div
                                              style={{
                                                display: 'flex',
                                                justifyContent: 'flex-end',
                                                alignItems: 'center',
                                              }}
                                            >
                                              <span
                                                style={{
                                                  marginRight: '8px',
                                                  padding: '4px 8px',
                                                  color: '#EB5757',
                                                }}
                                              >
                                                Custom
                                              </span>
                                              {constructiveQuestion?.from &&
                                              constructiveQuestion?.to &&
                                              dayjs(
                                                constructiveQuestion?.from
                                              ).isBefore(dayjs()) &&
                                              dayjs(
                                                constructiveQuestion?.to
                                              ).isAfter(dayjs()) ? (
                                                <span
                                                  style={{
                                                    padding: '4px 8px',
                                                    background: '#DFF6DD',
                                                    color: '#00AC47',
                                                    borderRadius: '10px',
                                                  }}
                                                >
                                                  On Going
                                                </span>
                                              ) : constructiveQuestion?.done ? (
                                                <span
                                                  style={{
                                                    padding: '4px 8px',
                                                    background: '#DFF6DD',
                                                    color: '#00AC47',
                                                    borderRadius: '10px',
                                                  }}
                                                >
                                                  Finished
                                                </span>
                                              ) : (
                                                <span
                                                  style={{
                                                    padding: '4px 8px',
                                                    background: '#cccccc',
                                                    color: '#404040',
                                                    borderRadius: '10px',
                                                  }}
                                                >
                                                  Not Yet
                                                </span>
                                              )}
                                            </div>
                                          </div>
                                        </Box>
                                      </Link>
                                    )
                                  )}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Grid>
            </AccordionDetails>
          </Accordion>
        ))}
      </>
    );
  }

  // Map the array of schedules into an array of slots
  const slots =
    schedulesDataRes &&
    schedulesDataRes?.schedules?.map((schedule, index) => ({
      value: `${(page - 1) * 10 + index + 1}`,
      label: `Slot ${(page - 1) * 10 + index + 1}`,
      id: `#${schedule.id}`, // Assuming there's an 'id' property in the schedule object
    }));

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Grid container style={{ padding: '20px' }}>
        {/* Grid đầu tiên */}
        <Grid item xs={8}>
          <Box
            component="main"
            sx={{ flex: '1', xs: 6, p: 0, marginRight: '30px' }}
          >
            <Breadcrumbs aria-label="breadcrumb" style={{ fontSize: '20px' }}>
              <Link style={{ color: '#0078D8' }} to={'/common/courses-list'}>
                Home
              </Link>
              <Link>
                {classData?.className} - {classData?.subject?.subjectCode} -{' '}
                {classData?.subject?.subjectName}- {classData?.semester?.name}
              </Link>
            </Breadcrumbs>
            {/* end breadcrumb */}
            <Box sx={{ xs: 6, pt: 2 }}>
              <Grid container justifyContent="left" sx={{ mb: 2 }}>
                <div>
                  <span>
                    <h6 style={{ fontSize: '16px' }}>
                      Course code: {classData?.subject.subjectCode} ↔ [
                      {classData?.semester.name}]
                    </h6>
                  </span>
                  <span>
                    <h5 style={{ fontSize: '24px' }}>
                      {classData?.subject.subjectName} -{' '}
                      {classData?.subject.subjectDescription}
                    </h5>
                  </span>
                </div>
              </Grid>

              {/* end filter semesters */}
            </Box>
            {content}
          </Box>
        </Grid>
        <Grid item xs={4}>
          <Card
            sx={{
              width: '100%',
              marginTop: '40px',
            }}
          >
            <CardContent sx={{ p: 2 }}>
              <FormControl size="small" fullWidth>
                <InputLabel>Class</InputLabel>
                <Select
                  value={classIdObj}
                  label="Class"
                  onChange={handleClassIdFilterChange}
                >
                  {classes?.map((classData) => (
                    <MenuItem key={classData?._id} value={classData?._id}>
                      {classData?.className} - {classData?.subject?.subjectCode}{' '}
                      - {classData?.semester?.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Box
                display={'flex'}
                justifyContent={'space-between'}
                flexWrap={'nowrap'}
                marginTop={'20px'}
                marginBottom={'20px'}
              >
                <Paper
                  sx={{
                    backgroundColor: '#f7f7f7',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    fontWeight: '800',
                    color: '#00ac47',
                    width: '128px',
                    height: '90px',
                    fontSize: '1rem',
                  }}
                >
                  <span>0</span>
                  <span title="students" value="students">
                    online
                  </span>
                </Paper>
                <Paper
                  sx={{
                    backgroundColor: '#f7f7f7',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    fontWeight: '800',
                    color: '#00ac47',
                    width: '128px',
                    height: '90px',
                    fontSize: '1rem',
                  }}
                >
                  <span>{classData?.students.length}</span>
                  <span title="students" value="students">
                    students
                  </span>
                </Paper>
                <Paper
                  sx={{
                    backgroundColor: '#f7f7f7',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    fontWeight: '800',
                    width: '128px',
                    height: '90px',
                    fontSize: '1rem',
                  }}
                >
                  <span>
                    {schedulesDataRes?.schedulesCount
                      ? schedulesDataRes?.schedulesCount
                      : 0}
                  </span>
                  <span title="Slots" value="Slots">
                    slots
                  </span>
                </Paper>
              </Box>
              <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
                <Paper
                  sx={{
                    fontWeight: '600',
                    fontSize: '0.875rem',
                    backgroundColor: '#f7f7f7',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    p: 2,
                    width: '100%',
                    height: '100px',
                    marginBottom: '8px',
                  }}
                >
                  <span
                    title="Start date: 17:00 08/05/2023"
                    value="Start date: 17:00 08/05/2023"
                  >
                    Start date:{' '}
                    {dayjs(classData?.semester?.from).format('DD/MM/YYYY')}
                  </span>
                  <span
                    class="text-danger"
                    style={{ color: 'rgba(220, 53, 69, 1)' }}
                  >
                    End date &nbsp;&nbsp;:{' '}
                    {dayjs(classData?.semester?.to).format('DD/MM/YYYY')}
                  </span>
                </Paper>
              </Box>
              <Box>
                <span
                  style={{
                    fontWeight: '700',
                    fontSize: '1rem',
                  }}
                  title="undefined"
                >
                  Lecturer
                </span>
                <List
                  sx={{
                    width: '100%',
                    maxWidth: 360,
                    bgcolor: 'background.paper',
                  }}
                >
                  <ListItem
                    display="flex"
                    justifyContent="flex-start"
                    alignItems="center"
                    position="relative"
                    textAlign="left"
                    width="100%"
                  >
                    <ListItemAvatar>
                      <AccountCircleIcon
                        style={{ color: '#BDBDBD', fontSize: '50px' }}
                      ></AccountCircleIcon>
                    </ListItemAvatar>
                    <ListItemText
                      sx={{
                        flex: '1 1 auto',
                        minWidth: '0px',
                        marginTop: '4px',
                        marginBottom: '4px',
                      }}
                    >
                      <span
                        style={{
                          fontSize: '16px',
                          lineHeight: '1.5',
                          display: 'block',
                        }}
                        className="MuiTypography-root MuiTypography-body1 MuiListItemText-primary css-pk9ffv"
                      >
                        {classData?.trainer.fullName}
                      </span>{' '}
                      <span
                        style={{
                          fontSize: '12px',
                          lineHeight: '1.5',
                          display: 'block',
                          fontStyle: 'italic',
                          color: '#333',
                        }}
                        className="MuiTypography-root MuiTypography-body1 MuiListItemText-primary css-pk9ffv"
                      >
                        {classData?.trainer.email}
                      </span>
                    </ListItemText>
                  </ListItem>
                </List>
              </Box>
            </CardContent>
          </Card>
          <Paper
            sx={{
              mt: 3.5,
              fontWeight: '800',
              width: '100%',
              fontSize: '1rem',
              backgroundColor: '#fff',
              border: '1px solid #d9d9d9',
              borderRadius: '8px',
            }}
          >
            <Box sx={{ padding: '16px' }}>
              <span
                style={{
                  fontWeight: '600',
                  fontSize: '1.125rem',
                  lineHeight: '1.2',
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                Class meeting
                {isAdmin || classData?.trainer?._id === userId ? (
                  <IconButton onClick={handleClickOpenAddClassMeetingDialog}>
                    <AddIcon />
                  </IconButton>
                ) : (
                  <IconButton
                    onClick={handleOpenBackdrop}
                    className={openBackdrop ? 'reload-button-rotate' : ''}
                  >
                    <RefreshIcon />
                  </IconButton>
                )}
              </span>
              <ul
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  listStyle: 'none',
                  padding: '0',
                }}
              >
                {meetings &&
                  meetings?.map((meeting) => (
                    <li
                      key={meeting?._id}
                      style={{
                        marginTop: '10px',
                        borderLeft: '4px solid #0078d4',
                        borderRadius: '8px',
                        backgroundColor: '#f7f7f7',
                      }}
                    >
                      <Box
                        sx={{
                          padding: '11px 10px',
                          border: '1px solid #f7f7f7',
                          display: 'inline-flex',
                          textDecoration: 'none',
                          color: '#111',
                          borderRadius: '8px',
                          width: '100%',
                          minHeight: '100px',
                        }}
                      >
                        <Box
                          sx={{
                            width: '100%',
                            display: 'flex',
                            flexWrap: 'wrap',
                            justifyContent: 'space-between',
                            alignItems: 'flex-start',
                          }}
                        >
                          <Box
                            sx={{
                              flex: '0 0 auto',
                              pr: 1.5,
                              pl: 1.5,
                            }}
                          >
                            <Box>
                              <Box
                                sx={{
                                  overflow: 'hidden',
                                  textOverflow: 'ellipsis',
                                  whiteSpace: 'nowrap',
                                  fontWeight: '600',
                                }}
                              >
                                {' '}
                                {meeting?.meetingName}
                              </Box>
                              <Box
                                sx={{
                                  fontSize: '14px',
                                  color: 'rgba(0, 0, 0, 0.5)',
                                }}
                              >
                                {dayjs(meeting?.meetingFrom).format(
                                  'D MMM ddd HH:mm'
                                )}{' '}
                                -{' '}
                                {dayjs(meeting?.meetingTo).format(
                                  'D MMM ddd HH:mm'
                                )}
                              </Box>
                            </Box>
                            <Box sx={{ marginTop: '10px' }}>
                              {isAdmin || classData?.trainer?._id === userId ? (
                                dayjs().isBetween(
                                  meeting?.meetingFrom,
                                  meeting?.meetingTo,
                                  null,
                                  '[]'
                                ) ? (
                                  <Link
                                    to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                    target="_blank"
                                  >
                                    <Chip
                                      label="Join Now"
                                      color="success"
                                      variant="outlined"
                                      sx={{ cursor: 'pointer' }}
                                    />
                                  </Link>
                                ) : dayjs(meeting?.meetingTo).isBefore(
                                    dayjs()
                                  ) ? (
                                  <>
                                    <Chip
                                      label="Restart"
                                      sx={{ cursor: 'pointer' }}
                                      onClick={() =>
                                        handleOpenTimePickerDialog(meeting)
                                      }
                                    />
                                    <TimePickerDialog
                                      updateMeetingTime={updateMeetingTime}
                                      openTimePickerDialog={
                                        openTimePickerDialog
                                      }
                                      handleCloseTimePickerDialog={
                                        handleCloseTimePickerDialog
                                      }
                                      meeting={selectedMeeting}
                                    />
                                  </>
                                ) : dayjs(meeting?.meetingFrom).isAfter(
                                    dayjs()
                                  ) ? (
                                  <Link
                                    to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                    target="_blank"
                                  >
                                    <Chip
                                      color="primary"
                                      label="Upcoming"
                                      variant="outlined"
                                    />
                                  </Link>
                                ) : null
                              ) : dayjs().isBetween(
                                  meeting?.meetingFrom,
                                  meeting?.meetingTo,
                                  null,
                                  '[]'
                                ) ? (
                                <Link
                                  to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                  target="_blank"
                                >
                                  <Chip
                                    label="Join Now"
                                    color="success"
                                    variant="outlined"
                                    sx={{ cursor: 'pointer' }}
                                  />
                                </Link>
                              ) : dayjs(meeting?.meetingTo).isBefore(
                                  dayjs()
                                ) ? (
                                <Chip label="Ended" />
                              ) : dayjs(meeting?.meetingFrom).isAfter(
                                  dayjs()
                                ) ? (
                                <Link
                                  to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                  target="_blank"
                                >
                                  <Chip
                                    color="primary"
                                    label="Upcoming"
                                    variant="outlined"
                                  />
                                </Link>
                              ) : null}
                            </Box>
                          </Box>
                          <Box>
                            {/* Nút để xoá cuộc họp */}
                            {isAdmin || classData?.trainer?._id === userId ? (
                              // Chỉ hiển thị nút xoá khi người dùng là admin hoặc trainer
                              <IconButton
                                sx={{ cursor: 'pointer' }}
                                color="error"
                                aria-label="delete"
                                onClick={() => handleDeleteClicked(meeting)}
                              >
                                <CloseIcon />
                              </IconButton>
                            ) : null}
                          </Box>
                        </Box>
                      </Box>
                    </li>
                  ))}
              </ul>
            </Box>
          </Paper>
        </Grid>
        <CreateGroupDialog
          selectedSchedule={selectedSchedule}
          classData={classData}
          openCreateGroupsDialog={openCreateGroupsDialog}
          handleCloseCreateGroupsDialog={handleCloseCreateGroupsDialog}
        />

        <Dialog
          open={openAddClassMeetingDialog}
          keepMounted
          onClose={handleCloseAddClassMeetingDialog}
          aria-describedby="class-meeting-dialog-slide-description"
          maxWidth="lg"
        >
          <DialogTitle sx={{ padding: '0' }}>
            <Paper
              sx={{
                display: 'flex',
                justifyContent: 'space-between',
                width: '100%',
                paddingRight: '10px',
                paddingLeft: '10px',
                alignItems: 'center',
              }}
            >
              <Box>Class Meeting</Box>
              <Box>
                <IconButton onClick={handleCloseAddClassMeetingDialog}>
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              </Box>
            </Paper>
          </DialogTitle>
          <DialogContent sx={{ width: '800px' }}>
            <TextField
              margin="dense"
              label="Meeting Name"
              type="text"
              fullWidth
              required
              helperText={meetingNameHelperText}
              error={meetingNameError}
              value={meetingName}
              onChange={handleMeetingNameChange}
              sx={{ marginBottom: '10px' }}
            />
            <>
              {classData ? (
                <>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      margin="dense"
                      disabled
                      required
                      id="subject-code"
                      name="subject-code"
                      label="Subject Code"
                      value={
                        classData?.subject?.subjectCode
                          ? classData?.subject?.subjectCode
                          : ''
                      }
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      margin="dense"
                      disabled
                      id="class-name"
                      name="class-name"
                      label="Class Name"
                      fullWidth
                      value={classData?.className}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={selectAll}
                          onChange={handleToggleSelectAll}
                          color="primary"
                        />
                      }
                      label="Select All Students"
                    />
                    <Autocomplete
                      multiple
                      id="tags-outlined"
                      options={classData?.students}
                      value={selectedStudents}
                      onChange={handleUserSelection}
                      getOptionLabel={(option) => option.email}
                      filterSelectedOptions
                      renderInput={(params) => (
                        <TextField
                          margin="dense"
                          sx={{ maxWidth: '100%' }}
                          {...params}
                          required
                          label="Invite Users"
                          placeholder="Select a Users"
                        />
                      )}
                    />
                  </Grid>
                </>
              ) : (
                <></>
              )}
            </>
            {/* Date-Time Picker for 'From' date-time */}
            <MobileDateTimePicker
              sx={{
                marginTop: '15px',
                width: '100%',
              }}
              label="From"
              fullWidth
              disablePast
              value={dateTimeMeetingFrom}
              onChange={handleDateTimeMeetingFromChange}
              renderInput={(params) => <TextField {...params} />}
              PopperProps={{
                placement: 'right',
              }}
            />

            {/* Date-Time Picker for 'To' date-time */}
            <MobileDateTimePicker
              sx={{
                marginTop: '15px',
                width: '100%',
              }}
              label="To"
              disablePast
              fullWidth
              value={dateTimeMeetingTo}
              onChange={handleDateTimeMeetingToChange}
              renderInput={(params) => <TextField {...params} />}
              PopperProps={{
                placement: 'right',
              }}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseAddClassMeetingDialog}>Cancel</Button>
            <Button onClick={handleAddNewVideoConference} color="primary">
              Create
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          fullWidth
          maxWidth={'sm'}
          open={openDeleteMeetingDialog}
          onClose={handleCloseDeleteMeetingDialog}
        >
          <DialogTitle>Delete Meeting</DialogTitle>
          <Divider />
          <DialogContent>
            <Typography variant="h6">
              You want to delete meeting name:{' '}
              <span style={{ color: '#ff0000' }}>
                {selectedMeeting?.meetingName}
              </span>
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseDeleteMeetingDialog}>Cancel</Button>
            <Button color="error" onClick={handleDeleteMeeting}>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
        <Backdrop
          open={openBackdrop}
          onClick={handleCloseBackdrop}
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <Backdrop
          open={openBackdropSchedules}
          onClick={handleCloseBackdropSchedules}
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Grid>
    </LocalizationProvider>
  );
};

export default UserClassDetails;
