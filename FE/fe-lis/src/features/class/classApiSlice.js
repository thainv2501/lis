import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

const classAdapter = createEntityAdapter({});

const initialState = classAdapter.getInitialState();

export const classApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getClassWithSearch: builder.query({
      query: (searchURL) => ({
        url: "/class" + searchURL,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
    getClassWithId: builder.query({
      query: (classId) => ({
        url: "/class/" + classId,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
    getClassBySemesterId: builder.query({
      query: (semesterId) => ({
        url: "/class/semester/" + semesterId,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
    createClass: builder.mutation({
      query: (initialData) => ({
        url: "/class",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
    importClasses: builder.mutation({
      query: (initialData) => ({
        url: "/class/import",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
    addStudentsToClass: builder.mutation({
      query: (initialData) => ({
        url: "/class/add-students",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
    addStudentToClass: builder.mutation({
      query: (initialData) => ({
        url: "/class/add-student",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
    removeStudentOutClass: builder.mutation({
      query: (initialData) => ({
        url: "/class/remove-student",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
    updateClass: builder.mutation({
      query: (initialData) => ({
        url: "/class",
        method: "PATCH",
        body: {
          ...initialData,
        },
      }),
    }),
  }),
});

export const {
  useGetClassWithSearchQuery,
  useGetClassWithIdQuery,
  useGetClassBySemesterIdQuery,
  useCreateClassMutation,
  useUpdateClassMutation,
  useAddStudentsToClassMutation,
  useAddStudentToClassMutation,
  useRemoveStudentOutClassMutation,
  useImportClassesMutation,
} = classApiSlice;
