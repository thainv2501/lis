import { createEntityAdapter } from '@reduxjs/toolkit';
import { apiSlice } from '../../app/api/apiSlice';

const masterDataAdapter = createEntityAdapter({});

const initialState = masterDataAdapter.getInitialState();

export const masterDataApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getMasterData: builder.query({
      query: () => ({
        url: '/master-data',
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        const loadedMasterData = responseData.map((data) => {
          data.id = data._id;
          return data;
        });
        return masterDataAdapter.setAll(initialState, loadedMasterData);
      },
      providesTags: (result, error, arg) => {
        if (result?.ids) {
          return [
            { type: 'MasterData', id: 'LIST' },
            ...result.ids.map((id) => ({ type: 'MasterData', id })),
          ];
        } else return [{ type: 'MasterData', id: 'LIST' }];
      },
    }),
    getMasterDataWithSearch: builder.query({
      query: (searchURL) => ({
        url: '/master-data' + searchURL,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        const loadedMasterData = responseData.masterData.map((data) => {
          data.id = data._id;
          return data;
        });
        return { ...responseData, masterData: loadedMasterData };
      },
      providesTags: (result, error, arg) => {
        if (result?.ids) {
          return [
            { type: 'MasterData', id: 'LIST' },
            ...result.ids.map((id) => ({ type: 'MasterData', id })),
          ];
        } else return [{ type: 'MasterData', id: 'LIST' }];
      },
    }),
    getMasterDataWithType: builder.query({
      query: (type) => ({
        url: '/master-data/' + type,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    createMasterData: builder.mutation({
      query: (initialMasterData) => ({
        url: '/master-data',
        method: 'POST',
        body: {
          ...initialMasterData,
        },
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'MasterData', id: arg.id },
      ],
    }),
    updateMasterData: builder.mutation({
      query: (initialMasterData) => ({
        url: '/master-data',
        method: 'PATCH',
        body: {
          ...initialMasterData,
        },
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'MasterData', id: arg.id },
      ],
    }),
  }),
});

export const {
  useGetMasterDataQuery,
  useUpdateMasterDataMutation,
  useGetMasterDataWithTypeQuery,
  useCreateMasterDataMutation,
  useGetMasterDataWithSearchQuery,
} = masterDataApiSlice;
