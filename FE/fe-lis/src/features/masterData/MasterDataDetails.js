import {
  Button,
  Chip,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
} from "@mui/material";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import { React, useState } from "react";
import Title from "../../components/Title";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";

const MasterDataDetails = ({
  masterData,
  handleCloseViewDialog,
  refetch,
  updateMasterData,
}) => {
  const [dataName, setDataName] = useState(masterData?.name);
  const [description, setDescription] = useState(masterData?.description);
  const [status, setStatus] = useState(masterData?.active);
  const [from, setFrom] = useState(masterData?.from);
  const [to, setTo] = useState(masterData?.to);
  const [displayOrder, setDisplayOrder] = useState(
    masterData?.displayOrder || 0
  );

  const handleDataNameChange = (event) => {
    setDataName(event.target.value);
  };

  const handleDescriptionChange = (event) => {
    setDescription(event.target.value);
  };

  const handleChangeStatus = () => {
    setStatus((prev) => !prev);
  };

  const handleDisplayOrderChange = (event) => {
    const displayOrderInput = event.target.value;
    if (
      parseFloat(displayOrderInput) <= 0 ||
      parseFloat(displayOrderInput) > 100
    ) {
      setDisplayOrder(0);
    }

    setDisplayOrder(Math.ceil(displayOrderInput));
  };

  const handleUpdateMasterData = async () => {
    try {
      await updateMasterData({
        id: masterData?._id,
        name: dataName.trim(),
        active: status,
        displayOrder,
        from,
        to,
        description,
      });
      handleCloseViewDialog();
    } catch (error) {
      console.log("fail to change status : " + error);
    }
  };

  const canSave =
    status !== masterData?.active ||
    (dataName !== masterData?.name && dataName.trim() !== "") ||
    description !== masterData?.description ||
    (masterData?.displayOrder
      ? displayOrder !== masterData?.displayOrder
      : displayOrder !== 0) ||
    (((masterData?.from ? from !== masterData?.from : false) ||
      (masterData?.to ? to !== masterData?.to : false)) &&
      (masterData?.type === "Semester"
        ? dayjs(to).isAfter(dayjs(from))
        : true));
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Paper
        sx={{
          mt: 3,
          p: 5,
        }}
      >
        <Title>Master Data Details</Title>
        <Grid container spacing={3}>
          <Grid item sm={12} md={6} lg={6}>
            <FormControl fullWidth>
              <InputLabel id="master-data-type-select-label">Type</InputLabel>
              <Select
                id="master-data-type-select"
                label="Type"
                value={masterData?.type}
                disabled
              >
                <MenuItem value={masterData?.type}>{masterData?.type}</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={12} md={6} lg={6}>
            <TextField
              required
              type="number"
              id="display-order"
              name="Display Order"
              label="Display Order"
              value={displayOrder}
              InputProps={{
                inputProps: {
                  max: 100,
                  min: 0,
                },
              }}
              fullWidth
              onChange={handleDisplayOrderChange}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={6} mb={2}>
            <TextField
              required
              id="data-code"
              name="data-code"
              label="Data code"
              fullWidth
              value={masterData?.code}
              disabled
            />
          </Grid>
          <Grid item xs={12} sm={12} md={6} mb={2}>
            <TextField
              required
              id="data-name"
              name="data-name"
              label="Data Name"
              fullWidth
              value={dataName}
              onChange={handleDataNameChange}
            />
          </Grid>
          {masterData?.type === "Semester" ? (
            <Grid item xs={12}>
              <DatePicker
                sx={{ mr: 4 }}
                fullWidth
                label="From"
                value={dayjs(from)}
                disablePast
                onChange={(newValue) => setFrom(newValue)}
              />
              <DatePicker
                fullWidth
                label="To"
                value={dayjs(to)}
                disablePast
                minDate={dayjs(from)}
                onChange={(newValue) => setTo(newValue)}
              />
            </Grid>
          ) : null}
        </Grid>
        <Grid item sm={12} md={6} lg={6}>
          <Button onClick={handleChangeStatus}>
            <Chip
              label={status ? "Active" : "Deactivated"}
              color={status ? "success" : "error"}
              variant="outlined"
            />
          </Button>
        </Grid>
        <Grid xs={12} sm={12} mt={2}>
          <TextField
            id="description"
            name="description"
            label="Description"
            multiline
            rows={4}
            fullWidth
            value={description}
            onChange={handleDescriptionChange}
          />
        </Grid>

        <Grid item xs={12} container justifyContent="flex-end">
          <Button
            variant="contained"
            sx={{ mt: 3, ml: 1 }}
            color="primary"
            disabled={!canSave}
            onClick={handleUpdateMasterData}
          >
            Save
          </Button>
        </Grid>
      </Paper>
    </LocalizationProvider>
  );
};

export default MasterDataDetails;
