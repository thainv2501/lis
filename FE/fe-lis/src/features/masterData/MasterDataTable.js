import FindInPageIcon from "@mui/icons-material/FindInPage";
import { Box, Button, Tooltip } from "@mui/material";
import Chip from "@mui/material/Chip";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { useSnackbar } from "notistack";
import { React, useEffect, useState } from "react";
import MasterDataDetails from "./MasterDataDetails";
import { useUpdateMasterDataMutation } from "./masterDataApiSlice";

const MasterDataTable = ({ page, masterData, refetch }) => {
  const [selectedMasterData, setSelectedMasterData] = useState();
  const [openStatusDialog, setOpenStatusDialog] = useState(false);
  const [openActionDeleteDialog, setOpenActionDeleteDialog] = useState(false);
  const [openViewDialog, setOpenViewDialog] = useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const [
    updateMasterData,
    {
      isSuccess: updateMasterDataIsSuccess,
      isError: updateMasterDataIsError,
      error: updateMasterDataError,
    },
  ] = useUpdateMasterDataMutation();

  const handleOpenStatusDialog = (masterData) => {
    setSelectedMasterData(masterData);
    setOpenStatusDialog(true);
  };

  const handleCloseStatusDialog = () => {
    setOpenStatusDialog(false);
  };

  const handleChangeActiveStatus = async (masterData) => {
    try {
      await updateMasterData({
        id: masterData?._id,
        active: !masterData?.active,
      });
    } catch (error) {
      console.log("fail to update data : " + error);
    }
  };

  const handleOpenViewDialog = (masterData) => {
    setSelectedMasterData(masterData);
    setOpenViewDialog(true);
  };
  const handleCloseViewDialog = () => {
    setOpenViewDialog(false);
  };

  const handleOpenActionDeleteDialog = (masterData) => {
    setSelectedMasterData(masterData);
    setOpenActionDeleteDialog(true);
  };
  const handleCloseActionDeleteDialog = () => {
    setOpenActionDeleteDialog(false);
  };

  useEffect(() => {
    if (updateMasterDataIsSuccess) {
      enqueueSnackbar("update data is successful !", { variant: "success" });
      handleCloseStatusDialog();
      refetch();
    }
    if (updateMasterDataIsError) {
      enqueueSnackbar(
        "update data is fail : " + updateMasterDataError?.data?.message,
        { variant: "error" }
      );
    }
  }, [updateMasterDataIsError, updateMasterDataIsSuccess]);

  return (
    <>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell sx={{ color: "indianred" }}>No.</TableCell>
            <TableCell sx={{ color: "indianred" }}>Code</TableCell>
            <TableCell sx={{ color: "indianred" }}>Data Name</TableCell>
            <TableCell sx={{ color: "indianred" }}>Data Type</TableCell>
            <TableCell sx={{ color: "indianred" }}>Display Order</TableCell>
            <TableCell sx={{ color: "indianred", width: "10%" }}>
              Status
            </TableCell>
            <TableCell align="center" sx={{ color: "indianred" }}>
              Actions
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {masterData.map((data, index) => (
            <TableRow key={data?.id}>
              <TableCell sx={{ wordBreak: "break-word" }}>
                {(page - 1) * 10 + (index + 1)}
              </TableCell>
              <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                {data?.code}
              </TableCell>
              <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                {data?.name}
              </TableCell>
              <TableCell>{data?.type}</TableCell>
              <TableCell align="center">
                {data?.displayOrder ? data?.displayOrder : "0"}
              </TableCell>
              <TableCell>
                <Button onClick={() => handleOpenStatusDialog(data)}>
                  {data?.active ? (
                    <Chip label="Active" color="success" variant="outlined" />
                  ) : (
                    <Chip
                      label="Deactivated"
                      color="error"
                      variant="outlined"
                    />
                  )}
                </Button>
              </TableCell>
              <TableCell>
                <Box display={"flex"} justifyContent={"center"}>
                  <Tooltip title="View Details" placement="top" arrow>
                    <Button
                      color="success"
                      onClick={() => handleOpenViewDialog(data)}
                    >
                      <FindInPageIcon />
                    </Button>
                  </Tooltip>
                  {/* <Tooltip title="Delete" placement="top" arrow>
                    <Button
                      color="error"
                      onClick={() => handleOpenActionDeleteDialog(data)}
                    >
                      <DeleteForeverIcon />
                    </Button>
                  </Tooltip> */}
                </Box>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Box>
        {/* change master data status dialog */}
        <Dialog open={openStatusDialog} onClose={handleCloseStatusDialog}>
          <DialogTitle>Change Status</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Change status of data : {selectedMasterData?.name}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={handleCloseStatusDialog}
              variant="contained"
              color="warning"
            >
              Cancel
            </Button>
            <Button
              onClick={() => handleChangeActiveStatus(selectedMasterData)}
              variant="contained"
              color="success"
            >
              Confirm
            </Button>
          </DialogActions>
        </Dialog>

        {/* Action delete Dialog */}
        <Dialog
          open={openActionDeleteDialog}
          onClose={handleCloseActionDeleteDialog}
        >
          <DialogTitle color={"red"}> DELETE USER </DialogTitle>
          <DialogContent>
            <DialogContentText>
              {`You want delete data : ${selectedMasterData?.code} : ${selectedMasterData?.name}`}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              onClick={handleCloseActionDeleteDialog}
              variant="contained"
              color="warning"
            >
              Cancel
            </Button>
            <Button
              // onClick={() => handleDeleteUser(selectedUser)}
              variant="contained"
              color="success"
            >
              Confirm
            </Button>
          </DialogActions>
        </Dialog>

        {/* view master Data detail dialog */}
        <Dialog
          open={openViewDialog}
          onClose={handleCloseViewDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth
          maxWidth={"md"}
        >
          <MasterDataDetails
            handleCloseViewDialog={handleCloseViewDialog}
            refetch={refetch}
            updateMasterData={updateMasterData}
            masterData={selectedMasterData}
          />
        </Dialog>
      </Box>
    </>
  );
};

export default MasterDataTable;
