import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
} from "@mui/material";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import { useSnackbar } from "notistack";
import { React, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Title from "../../components/Title";
import { MASTER_DATA_TYPES } from "../../config/masterDataType";
import { useCreateMasterDataMutation } from "./masterDataApiSlice";

const AddNewMasterData = ({ handleCloseAddDialog, refetch }) => {
  const [from, setFrom] = useState(dayjs());
  const [to, setTo] = useState(dayjs().add(1, "day"));

  const [type, setType] = useState("");
  const [dataCode, setDataCode] = useState("");
  const [dataName, setDataName] = useState("");
  const [displayOrder, setDisplayOrder] = useState(0);
  const [description, setDescription] = useState("");
  const { enqueueSnackbar } = useSnackbar();

  const handleTypeChange = (event) => setType(event.target.value);
  const handleDataCodeChange = (event) => setDataCode(event.target.value);
  const handleDisplayOrderChange = (event) => {
    const displayOrderInput = event.target.value;
    if (
      parseFloat(displayOrderInput) <= 0 ||
      parseFloat(displayOrderInput) > 100
    ) {
      setDisplayOrder(0);
    }

    setDisplayOrder(Math.ceil(displayOrderInput));
  };
  const handleDataNameChange = (event) => setDataName(event.target.value);
  const handleDescriptionChange = (event) => setDescription(event.target.value);

  const canAdd =
    type.trim() !== "" &&
    dataCode.trim() !== "" &&
    dataName.trim() !== "" &&
    (type === "Semester" ? to.isAfter(from) : true);

  const [
    createNewMasterData,
    {
      isSuccess: createMasterDataIsSuccess,
      isError: createMasterDataIsError,
      error: createMasterDataError,
    },
  ] = useCreateMasterDataMutation();

  const handleAddNewMasterData = async () => {
    let masterDataObject = {};
    if (type === "Role") {
      masterDataObject = {
        type,
        code: dataCode.trim(),
        name: dataName.trim(),
        displayOrder,
        description,
      };
    }
    if (type === "Semester") {
      masterDataObject = {
        type,
        code: dataCode,
        name: dataName,
        displayOrder,
        from,
        to,
        description,
      };
    }
    try {
      await createNewMasterData(masterDataObject);
    } catch (error) {
      enqueueSnackbar("create fail : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (createMasterDataIsSuccess) {
      enqueueSnackbar("create new master data successful", {
        variant: "success",
      });
      handleCloseAddDialog();
    }
    if (createMasterDataIsError) {
      enqueueSnackbar("create fail : " + createMasterDataError?.data?.message, {
        variant: "error",
      });
    }
  }, [createMasterDataIsError, createMasterDataIsSuccess]);

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Paper
        sx={{
          mt: 3,
          p: 5,
        }}
      >
        <Title>Add New Master Data</Title>
        <Grid container spacing={3}>
          <Grid item sm={12} md={6} lg={6}>
            <FormControl fullWidth>
              <InputLabel id="master-data-type-select-label" required>
                Type
              </InputLabel>
              <Select
                id="master-data-type-select"
                label="Type"
                onChange={handleTypeChange}
                value={type}
              >
                {MASTER_DATA_TYPES.map((type, index) => (
                  <MenuItem key={index} value={type}>
                    {type}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item sm={12} md={6} lg={6}>
            <TextField
              required
              type="number"
              id="display-order"
              name="Display Order"
              label="Display Order"
              value={displayOrder}
              InputProps={{
                inputProps: {
                  max: 100,
                  min: 0,
                },
              }}
              fullWidth
              onChange={handleDisplayOrderChange}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={6} mb={2}>
            <TextField
              required
              id="data-code"
              name="data-code"
              label="Data code"
              fullWidth
              onChange={handleDataCodeChange}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={6} mb={2}>
            <TextField
              required
              id="data-name"
              name="data-name"
              label="Data Name"
              fullWidth
              onChange={handleDataNameChange}
            />
          </Grid>
          {type === "Semester" ? (
            <Grid item xs={12}>
              <DatePicker
                sx={{ mr: 4 }}
                fullWidth
                label="From"
                value={from}
                disablePast
                onChange={(newValue) => setFrom(newValue)}
              />
              <DatePicker
                fullWidth
                label="To"
                value={to}
                disablePast
                minDate={from}
                onChange={(newValue) => setTo(newValue)}
              />
            </Grid>
          ) : null}
          <Grid item xs={12}>
            <TextField
              id="description"
              name="description"
              label="Description"
              multiline
              rows={4}
              fullWidth
              onChange={handleDescriptionChange}
            />
          </Grid>

          <Grid item xs={12} container justifyContent="flex-end">
            <Button
              variant="contained"
              sx={{ mt: 3, ml: 1 }}
              color="primary"
              disabled={!canAdd}
              onClick={handleAddNewMasterData}
            >
              Add
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </LocalizationProvider>
  );
};

export default AddNewMasterData;
