import FindInPageIcon from '@mui/icons-material/FindInPage';
import {
  Backdrop,
  Box,
  Button,
  CircularProgress,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Skeleton,
  Typography,
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import { useSnackbar } from 'notistack';
import { React, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import IconTextField from '../../components/IconTextField';
import Title from '../../components/Title';
import { MASTER_DATA_TYPES } from '../../config/masterDataType';
import { useGetMasterDataWithSearchQuery } from '../masterData/masterDataApiSlice';
import AddNewMasterData from './AddNewMasterData';
import MasterDataTable from './MasterDataTable';

const MasterDataList = () => {
  const navigate = useNavigate();
  const searchURLDefault = '?page=1&type=all&active=all&keyword=';
  const { search } = useLocation();
  const [type, setType] = useState('all');
  const [status, setStatus] = useState('all');
  const [keyword, setKeyword] = useState('');
  const [page, setPage] = useState(1);
  const { enqueueSnackbar } = useSnackbar();

  const [openAddDialog, setOpenAddDialog] = useState(false);

  useEffect(() => {
    setKeyword(new URLSearchParams(search).get('keyword') || '');
    setType(new URLSearchParams(search).get('type') || 'all');
    setStatus(new URLSearchParams(search).get('active') || 'all');
    setPage(parseInt(new URLSearchParams(search).get('page')) || 1);
  }, [search]);

  const handleClickOpenAddDialog = () => {
    setOpenAddDialog(true);
  };

  const handleCloseAddDialog = () => {
    setOpenAddDialog(false);
  };

  const handleSearch = () => {
    handleOpenBackdrop();
    let query = new URLSearchParams({
      page: 1,
      type,
      active: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };
  const handleKeywordChanged = (event) => setKeyword(event.target.value);
  const handleTypeFilterChange = (event) => setType(event.target.value);
  const handleStatusFilterChange = (event) => {
    setStatus(event.target.value);
  };
  const handlePaginationChanged = (event, value) => {
    let query = new URLSearchParams({
      page: value,
      type,
      active: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };

  const {
    data,
    isLoading: getMasterDataIsLoading,
    isSuccess: getMasterDataIsSuccess,
    isError: getMasterDataIsError,
    error: getMasterDataError,
    refetch,
  } = useGetMasterDataWithSearchQuery(
    search === '' ? searchURLDefault : search
  );

  const [openBackdrop, setOpenBackdrop] = useState(false);
  const [isRefetching, setIsRefetching] = useState(false);

  const handleOpenBackdrop = () => {
    setIsRefetching(true);
    setOpenBackdrop(true);
    refetch().then(() => {
      setIsRefetching(false);
    });
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  useEffect(() => {
    if (isRefetching) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [isRefetching]);

  let content;

  if (getMasterDataIsLoading)
    content = (
      <Box sx={{ width: '100%' }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (getMasterDataIsError) {
    content = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
            <Grid
              item
              xs={12}
              display={'flex'}
              flexDirection={'row'}
              justifyContent={'space-between'}
              sx={{ mb: 1 }}
            >
              <Title>Master Data</Title>
            </Grid>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              justifyContent={'flex-start'}
              alignItems={'center'}
              marginBottom={3}
            >
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="master-data-type-select-label">
                    Type
                  </InputLabel>
                  <Select
                    id="master-data-type-select"
                    label="Type"
                    onChange={handleTypeFilterChange}
                    value={type}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    {MASTER_DATA_TYPES.map((type, index) => (
                      <MenuItem key={index} value={type}>
                        {type}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={2}>
                <FormControl size="small" fullWidth>
                  <InputLabel id="status-select">Status</InputLabel>
                  <Select
                    id="status-select"
                    label="Status"
                    onChange={handleStatusFilterChange}
                    value={status}
                  >
                    <MenuItem value={'all'}>All</MenuItem>
                    <MenuItem value={'active'}>Active</MenuItem>
                    <MenuItem value={'deactivated'}>Deactivated</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={12} lg={3}>
                <IconTextField
                  fullWidth
                  id="keyword"
                  label="key Word"
                  name="key-word"
                  value={keyword}
                  autoFocus
                  iconEnd={<FindInPageIcon />}
                  onChange={handleKeywordChanged}
                  size="small"
                />
              </Grid>
              <Grid item sm={12} md={3}>
                <Button variant="contained" onClick={handleSearch}>
                  Search
                </Button>
              </Grid>
            </Grid>
            <Box
              sx={{ mb: 1, mt: 1 }}
              fullWidth
              display={'flex'}
              justifyContent={'space-between'}
              alignItems={'center'}
            >
              <Title>Master Data Table</Title>
              <Button
                variant="contained"
                color="secondary"
                onClick={handleClickOpenAddDialog}
              >
                New Master Data
              </Button>
            </Box>
            <Grid>
              <Typography variant="h4" color={'red'}>
                {getMasterDataError?.data.message}
              </Typography>
            </Grid>
          </Paper>
        </Grid>
        {/* add new master Data dialog */}
        <Dialog
          open={openAddDialog}
          onClose={handleCloseAddDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth
          maxWidth={'md'}
        >
          <AddNewMasterData
            handleCloseAddDialog={handleCloseAddDialog}
            refetch={refetch}
          />
        </Dialog>
      </Grid>
    );
  }

  if (getMasterDataIsSuccess) {
    const { masterData, masterDataCount } = data;
    const pages = Math.ceil(masterDataCount / 10);
    if (masterDataCount !== 0) {
      content = (
        <Grid container spacing={3}>
          {/* Recent Dash Board */}
          <Grid item xs={12}>
            <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
              <Grid
                item
                xs={12}
                display={'flex'}
                flexDirection={'row'}
                justifyContent={'space-between'}
                sx={{ mb: 1 }}
              >
                <Title>Master Data</Title>
              </Grid>
              <Grid
                container
                rowSpacing={1}
                columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                justifyContent={'flex-start'}
                alignItems={'center'}
                marginBottom={3}
              >
                <Grid item sm={12} md={12} lg={2}>
                  <FormControl size="small" fullWidth>
                    <InputLabel id="master-data-type-select-label">
                      Type
                    </InputLabel>
                    <Select
                      id="master-data-type-select"
                      label="Type"
                      onChange={handleTypeFilterChange}
                      value={type}
                    >
                      <MenuItem value={'all'}>All</MenuItem>
                      {MASTER_DATA_TYPES.map((type, index) => (
                        <MenuItem key={index} value={type}>
                          {type}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item sm={12} md={12} lg={2}>
                  <FormControl size="small" fullWidth>
                    <InputLabel id="status-select">Status</InputLabel>
                    <Select
                      id="status-select"
                      label="Status"
                      onChange={handleStatusFilterChange}
                      value={status}
                    >
                      <MenuItem value={'all'}>All</MenuItem>
                      <MenuItem value={'active'}>Active</MenuItem>
                      <MenuItem value={'deactivated'}>Deactivated</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item sm={12} md={12} lg={3}>
                  <IconTextField
                    fullWidth
                    id="keyword"
                    label="Key Word"
                    name="key-word"
                    autoFocus
                    iconEnd={<FindInPageIcon />}
                    onChange={handleKeywordChanged}
                    size="small"
                  />
                </Grid>
                <Grid item sm={12} md={3}>
                  <Button variant="contained" onClick={handleSearch}>
                    Search
                  </Button>
                </Grid>
              </Grid>
              <Box
                sx={{ mt: 1, mb: 1 }}
                fullWidth
                display={'flex'}
                justifyContent={'space-between'}
              >
                <Typography component="h2" variant="h6" color="primary">
                  Master Data Table
                </Typography>
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={handleClickOpenAddDialog}
                >
                  New Master Data
                </Button>
              </Box>
              <MasterDataTable
                page={page}
                masterData={masterData}
                refetch={refetch}
              />
              <Pagination
                count={pages}
                page={page}
                variant="outlined"
                color="primary"
                sx={{ marginTop: '2rem', marginBottom: '1.5rem' }}
                onChange={handlePaginationChanged}
              />
            </Paper>
            {/* add new master Data dialog */}
            <Dialog
              open={openAddDialog}
              onClose={handleCloseAddDialog}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              fullWidth
              maxWidth={'md'}
            >
              <AddNewMasterData
                handleCloseAddDialog={handleCloseAddDialog}
                refetch={refetch}
              />
            </Dialog>
          </Grid>
          <Backdrop
            open={openBackdrop}
            onClick={handleCloseBackdrop}
            sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
        </Grid>
      );
    }
  }
  return content;
};

export default MasterDataList;
