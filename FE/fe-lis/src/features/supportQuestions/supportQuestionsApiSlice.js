import { createEntityAdapter } from '@reduxjs/toolkit';
import { apiSlice } from '../../app/api/apiSlice';

const supportQuestionsAdapter = createEntityAdapter({});

const initialState = supportQuestionsAdapter.getInitialState();

export const supportQuestionsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getSupportQuestions: builder.mutation({
      query: (initialData) => ({
        url: '/supportQuestions/supportQuestion',
        method: 'POST',
        body: {
          ...initialData,
        },
      }),
      transformResponse: (responseData) => {
        const loadedSupportQuestions = responseData.supportQuestions.map(
          (data) => {
            data.id = data._id;
            return data;
          }
        );
        return { ...responseData, supportQuestions: loadedSupportQuestions };
      },
    }),

    getSubjectQuestions: builder.mutation({
      query: (initialData) => ({
        url: '/supportQuestions/subjectQuestion',
        method: 'POST',
        body: {
          ...initialData,
        },
      }),
      transformResponse: (responseData) => {
        const loadedSubjectQuestions = responseData.subjectQuestions.map(
          (data) => {
            data.id = data._id;
            return data;
          }
        );
        return { ...responseData, subjectQuestions: loadedSubjectQuestions };
      },
    }),

    addNewSupportQuestion: builder.mutation({
      query: (initialData) => ({
        url: '/supportQuestions',
        method: 'POST',
        body: {
          ...initialData,
        },
      }),
    }),

    updateSupportQuestion: builder.mutation({
      query: (initialData) => ({
        url: '/supportQuestions',
        method: 'PATCH',
        body: {
          ...initialData,
        },
      }),
    }),

    updateSupportQuestionStatus: builder.mutation({
      query: (initialData) => ({
        url: '/supportQuestions/status',
        method: 'PATCH',
        body: {
          ...initialData,
        },
      }),
    }),

    deleteSupportQuestion: builder.mutation({
      query: (initialData) => ({
        url: '/supportQuestions',
        method: 'DELETE',
        body: {
          ...initialData,
        },
      }),
    }),

    getSupportQuestionById: builder.query({
      query: (supportQuestionId) => ({
        url: '/supportQuestions/' + supportQuestionId,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
  }),
});

export const {
  useGetSupportQuestionsMutation,
  useAddNewSupportQuestionMutation,
  useUpdateSupportQuestionMutation,
  useUpdateSupportQuestionStatusMutation,
  useDeleteSupportQuestionMutation,
  useGetSupportQuestionByIdQuery,
  useGetSubjectQuestionsMutation,
} = supportQuestionsApiSlice;
