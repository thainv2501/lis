import React from 'react';
import { Box, Grid, Pagination, Skeleton } from '@mui/material';
import CardSchedule from './CardSchedule';
const TabTrainingSlotContent = ({
  loading,
  schedules,
  schedulesCount,
  handlePaginationChanged,
  page,
}) => {
  const renderSkeletons = () => {
    return (
      <Grid
        sx={{ paddingBottom: '24px', minHeight: '565px' }}
        container
        spacing={3}
        columnSpacing={{ xs: 1, sm: 2, md: 3 }}
      >
        {[1, 2, 3, 4].map((index) => (
          <Grid key={index} item xl={3} lg={3} sm={6} md={4}>
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Box sx={{ mt: 2 }}></Box>
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
          </Grid>
        ))}
      </Grid>
    );
  };

  const renderSchedules = (schedules) => {
    return (
      <Grid
        sx={{ paddingBottom: '24px', minHeight: '760px' }}
        container
        spacing={3}
        columnSpacing={{ xs: 1, sm: 2, md: 3 }}
      >
        {schedules.map((schedule, index) => (
          <CardSchedule schedule={schedule} />
        ))}
      </Grid>
    );
  };
  let content = <Box>{renderSkeletons()}</Box>;
  if (schedules && loading) {
    content = (
      <Box>
        {schedulesCount && schedulesCount !== 0 ? (
          <Box sx={{ pt: 2 }}>
            {renderSchedules(schedules)}
            <Pagination
              count={Math.ceil(schedulesCount / 8)}
              page={page}
              onChange={handlePaginationChanged}
              variant="outlined"
              color="primary"
            />
          </Box>
        ) : (
          <Box sx={{ mt: 2 }}>
            <Box sx={{ width: '100%', textAlign: 'center' }}>
              <img
                src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
                alt="Img"
                style={{
                  marginBottom: '25px',
                  maxWidth: '200px',
                  verticalAlign: 'middle',
                }}
              />
              <Box>
                <h3
                  style={{
                    fontSize: '1.125rem',
                    lineHeight: '1.2',
                    color: '#0078d4',
                    marginBottom: '10px',
                  }}
                >
                  No slots now.
                </h3>
                <p style={{ margin: '0', fontSize: '20px' }}>
                  Please contact your school administration for more
                  information.
                </p>
              </Box>
            </Box>
          </Box>
        )}
      </Box>
    );
  }

  return content;
};

export default TabTrainingSlotContent;
