import React, { useEffect, useState } from 'react';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Box, Autocomplete, TextField } from '@mui/material';
import { useGetMasterDataWithTypeQuery } from '../masterData/masterDataApiSlice';
import { useFilterSchedulesByTabAndSemesterMutation } from '../schedule/scheduleApiSlice';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import TabTrainingSlotContent from './TabTrainingSlotContent';
import { useSnackbar } from 'notistack';
const TrainingSlot = () => {
  const { enqueueSnackbar } = useSnackbar();
  // master data
  const [semesters, setSemesters] = useState([]);
  const [semesterId, setSemesterId] = useState('');
  const [schedules, setSchedules] = useState([]);
  const [schedulesCount, setSchedulesCount] = useState(0);
  const [page, setPage] = useState(1);
  const [valueTab, setValueTab] = useState('all');
  const [loading, setLoading] = useState(true);

  const {
    data: masterDataSemester,
    isSuccess: masterDataIsSuccess,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery('Semester', {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (masterDataIsSuccess && masterDataSemester) {
      const { masterData, currentSemester } = masterDataSemester;
      setSemesters(masterData);
      setSemesterId(currentSemester?._id);
      fetchSchedulesBySemesterAndTab(currentSemester?._id, 'all', 1);
    }
  }, [masterDataIsSuccess, masterDataSemester]);

  // Schedules Data
  const [
    filterSchedulesByTabAndSemester,
    {
      data: schedulesData,
      isSuccess: getSchedulesDataIsSuccess,
      isLoading: getSchedulesDataIsLoading,
      isError: getSchedulesDataIsError,
      error: getSchedulesDataError,
    },
  ] = useFilterSchedulesByTabAndSemesterMutation();

  // Hàm fetch lịch trình theo semester và tab
  const fetchSchedulesBySemesterAndTab = async (semesterId, tab, page) => {
    try {
      setLoading(true);
      const schedulesResponse = await filterSchedulesByTabAndSemester({
        tab,
        semester: semesterId,
        page,
      });
      setLoading(false);
      setSchedules(schedulesResponse.data.schedules);
      setSchedulesCount(schedulesResponse.data.totalSchedulesCount);
    } catch (error) {
      console.error('Error fetching schedules:', error);
      setLoading(false);
    }
  };

  // useEffect(() => {
  //   fetchSchedulesBySemesterAndTab(semesterId, 'all', 1);
  // }, []);

  // Xử lý sự kiện khi người dùng thay đổi semester
  const handleSemesterChange = (event, newValue) => {
    if (!newValue) {
      enqueueSnackbar('Please Choose Semester!', {
        variant: 'warning',
      });
    } else {
      const selectedSemesterId = newValue ? newValue._id : null;
      setSemesterId(selectedSemesterId);
      fetchSchedulesBySemesterAndTab(selectedSemesterId, valueTab, 1);
    }
  };

  // Xử lý sự kiện khi người dùng thay đổi tab
  const handleChangeTab = (event, newValue) => {
    setValueTab(newValue);
    fetchSchedulesBySemesterAndTab(semesterId, newValue, 1);
  };

  // Xử lý sự kiện khi người dùng thay đổi trang
  const handlePaginationChanged = async (event, value) => {
    setPage(value);
    fetchSchedulesBySemesterAndTab(semesterId, valueTab, value);
  };

  let content;

  content = (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box component="main" sx={{ flex: '1', p: 4 }}>
        <Box sx={{ maxWidth: '1400px', margin: '0 auto' }}>
          <Box sx={{ maxWidth: '20vw', marginBottom: '24px' }}>
            <Autocomplete
              sx={{ width: '250px', mb: 2 }}
              id="size-small-outlined"
              size="small"
              onChange={handleSemesterChange}
              value={
                semesters.find((semester) => semester?._id === semesterId) ||
                null
              }
              options={semesters}
              getOptionLabel={(option) => option.name}
              renderInput={(params) => (
                <TextField {...params} label="SEMESTER" />
              )}
            />
          </Box>
          {/* end filter semesters */}
          {/* start card TrainingSlot */}
          <TabContext value={valueTab}>
            <Box>
              <TabList
                onChange={handleChangeTab}
                aria-label="lab API tabs example"
              >
                <Tab label="All" value="all" />
                <Tab label="On - Going" value="on-going" />
                <Tab label="Upcoming" value="upcoming" />
                <Tab label="Completed" value="completed" />
              </TabList>
            </Box>

            <TabPanel sx={{ pr: 0, pl: 0 }} value="all">
              <TabTrainingSlotContent
                loading={getSchedulesDataIsSuccess}
                schedules={schedules}
                schedulesCount={schedulesCount}
                handlePaginationChanged={handlePaginationChanged}
                page={page}
              />
            </TabPanel>
            <TabPanel sx={{ pr: 0, pl: 0 }} value="on-going">
              <TabTrainingSlotContent
                loading={getSchedulesDataIsSuccess}
                schedules={schedules}
                schedulesCount={schedulesCount}
                handlePaginationChanged={handlePaginationChanged}
                page={page}
              />
            </TabPanel>
            <TabPanel sx={{ pr: 0, pl: 0 }} value="upcoming">
              <TabTrainingSlotContent
                loading={getSchedulesDataIsSuccess}
                schedules={schedules}
                schedulesCount={schedulesCount}
                handlePaginationChanged={handlePaginationChanged}
                page={page}
              />
            </TabPanel>
            <TabPanel sx={{ pr: 0, pl: 0 }} value="completed">
              <TabTrainingSlotContent
                loading={getSchedulesDataIsSuccess}
                schedules={schedules}
                schedulesCount={schedulesCount}
                handlePaginationChanged={handlePaginationChanged}
                page={page}
              />
            </TabPanel>
          </TabContext>
          {/* end card TrainingSlot */}
        </Box>
      </Box>
    </LocalizationProvider>
  );
  return content;
};

export default TrainingSlot;
