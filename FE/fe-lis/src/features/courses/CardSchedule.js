import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Typography from '@mui/material/Typography';
import { Box, Grid } from '@mui/material';
import BookIcon from '@mui/icons-material/Book';
import RecentActorsIcon from '@mui/icons-material/RecentActors';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import CoPresentIcon from '@mui/icons-material/CoPresent';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';

const CardSchedule = ({ schedule }) => {
  const handleSlotTime = (slot) => {
    let slotStartAtFloat = 7.5 + (slot - 1) * 1.5;
    if (slot >= 4) {
      slotStartAtFloat = 12.5 + (slot - 4) * 1.5;
    }
    const hours = Math.floor(slotStartAtFloat);
    const minutes = Math.floor((slotStartAtFloat - hours) * 60);
    const slotStartAtTime = dayjs().hour(hours).minute(minutes).format('HH:mm');

    return `${slotStartAtTime}`;
  };

  return (
    <Grid key={schedule?._id} item xl={3} lg={3} sm={6} md={4}>
      <Card
        sx={{
          '& .MuiTypography-root': { mt: 0.5 },
          '& button': { mb: 0.5 },
          maxHeight: '380px',
        }}
      >
        <CardContent>
          <Box sx={{ height: '50px' }}>
            {schedule?.done ||
            dayjs().isAfter(schedule?.inClass?.semester?.to) ? (
              schedule?.constructiveQuestions.some((question) => {
                return (
                  question?.from &&
                  question?.to &&
                  dayjs().isBetween(question?.from, question?.to, null, '[]')
                );
              }) ||
              schedule?.lessons.some((lesson) => {
                return (
                  lesson.constructiveQuestions &&
                  lesson.constructiveQuestions.some((question) => {
                    return (
                      question?.from &&
                      question?.to &&
                      dayjs().isBetween(
                        question?.from,
                        question?.to,
                        null,
                        '[]'
                      )
                    );
                  })
                );
              }) ? (
                <span
                  style={{
                    marginBottom: '24px',
                    fontSize: '12px',
                    padding: '6px 8px',
                    fontWeight: '400',
                    borderRadius: '4px',
                    background: '#00ac47',
                    color: '#fff',
                    marginRight: '5px',
                    display: 'inline-block',
                    width: '85px',
                    textAlign: 'center',
                  }}
                >
                  On Going
                </span>
              ) : (
                <span
                  style={{
                    marginBottom: '24px',
                    fontSize: '12px',
                    padding: '6px 8px',
                    fontWeight: '400',
                    borderRadius: '4px',
                    background: 'rgba(108, 117, 125, 1)',
                    color: '#fff',
                    marginRight: '5px',
                    display: 'inline-block',
                    width: '85px',
                    textAlign: 'center',
                  }}
                >
                  Completed
                </span>
              )
            ) : (
              <span
                style={{
                  marginBottom: '24px',
                  fontSize: '12px',
                  padding: '6px 8px',
                  fontWeight: '400',
                  borderRadius: '4px',
                  background: '#0078d4',
                  color: '#fff',
                  marginRight: '5px',
                  display: 'inline-block',
                  width: '85px',
                  textAlign: 'center',
                }}
              >
                Upcoming
              </span>
            )}
          </Box>
          <Typography
            variant="h5"
            component="div"
            sx={{
              overflow: 'hidden',
              textOverflow: 'ellipsis',
              marginBottom: '12px',
              fontSize: '1.25rem',
              display: '-webkit-box',
              '-webkit-line-clamp': '3',
              '-webkit-box-orient': 'vertical',
              height: '90px',
              lineHeight: '30px',
            }}
          >
            <Link
              style={{
                overflow: 'hidden',
                fontWeight: '700',
                lineHeight: '1.2',
                color: '#111',
              }}
              to={`/common/courses-list/slot-details/${schedule?._id}`}
            >
              <span>
                {schedule?.lessons.map((lesson, index) => (
                  <React.Fragment key={lesson._id}>
                    {lesson?.lessonContent}
                    {index < schedule.lessons.length - 1 && ' '}
                  </React.Fragment>
                ))}
              </span>
            </Link>
          </Typography>

          <Typography
            sx={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column',
              justifyContent: 'flex-start',
              gap: 1,
            }}
            color="text.secondary"
          >
            <span
              style={{
                width: '100%',
                display: 'flex',
              }}
            >
              <BookIcon sx={{ marginRight: '5px' }} fontSize="small" />
              Subject:
            </span>
            <Link
              style={{
                display: 'inline-flex',
                alignItems: 'center',
                color: '#00ac47',
                width: '100%',
              }}
              to={`/common/courses-list/class-details?classId=${schedule?.inClass?._id}&semesterId=${schedule?.inClass?.semester?._id}`}
            >
              <span>{schedule?.inClass?.subject?.subjectName}</span>
            </Link>
          </Typography>
          <Typography
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: 1,
            }}
            color="text.secondary"
          >
            <RecentActorsIcon fontSize="small" />
            Class: {schedule?.inClass?.className}
          </Typography>
          <Typography
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: 1,
            }}
            color="text.secondary"
          >
            <AccessTimeIcon fontSize="small" /> Time:{' '}
            {handleSlotTime(schedule?.slotInDay)}{' '}
            {dayjs(schedule?.day).format('DD/MM/YYYY')}
          </Typography>
          <Typography
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: 1,
            }}
            color="text.secondary"
          >
            <CoPresentIcon fontSize="small" /> Number of students:{' '}
            {schedule?.inClass?.students.length}
          </Typography>
        </CardContent>
        <CardActions
          sx={{
            padding: '0 16px 16px',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <Link to={`/common/courses-list/slot-details/${schedule?._id}`}>
            <span
              style={{
                color: '#0078d4',
                fontSize: '1.125rem',
              }}
              title="Continue"
              value="Continue"
            >
              Continue <ArrowForwardIcon />
            </span>
          </Link>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default CardSchedule;
