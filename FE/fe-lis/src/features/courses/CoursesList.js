import {
  Box,
  Typography,
  Card,
  CardContent,
  CardActions,
  Button,
  Grid,
  TextField,
  Autocomplete,
  useScrollTrigger,
  Zoom,
  Skeleton,
} from '@mui/material';
import RecentActorsIcon from '@mui/icons-material/RecentActors';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import CoPresentIcon from '@mui/icons-material/CoPresent';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { useGetMasterDataWithTypeQuery } from '../masterData/masterDataApiSlice';
import { useGetClassBySemesterIdQuery } from '../class/classApiSlice';
import { useSnackbar } from 'notistack';
import useAuth from '../../hooks/useAuth';
import dayjs from 'dayjs';

const CoursesList = () => {
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const [isLoadingClasses, setIsLoadingClasses] = useState(true);
  const { enqueueSnackbar } = useSnackbar();

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const handleScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const handleScrollBottom = () => {
    window.scrollTo({
      top: document.documentElement.scrollHeight,
      behavior: 'smooth',
    });
  };

  // master data
  const [semesters, setSemesters] = useState([]);
  const [currentSemester, setCurrentSemester] = useState();
  const [semesterId, setSemesterId] = useState();
  const {
    data: masterDataSemester,
    isSuccess: masterDataIsSuccess,
    isLoading: masterDataIsLoading,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery('Semester', {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (masterDataIsSuccess) {
      const { masterData, currentSemester } = masterDataSemester;
      setSemesters(masterData);
      setCurrentSemester(currentSemester);
      setSemesterId(currentSemester?._id);
    }
  }, [masterDataIsSuccess, currentSemester]);

  const handleSemesterChange = (event, newValue) => {
    if (!newValue) {
      enqueueSnackbar('Please Choose Semester!', {
        variant: 'warning',
      });
    } else {
      setIsLoadingClasses(true);
      setSemesterId(newValue?._id);
    }
  };

  // class data
  const [classes, setClasses] = useState([]);
  const {
    data: classData,
    isSuccess: getClassDataIsSuccess,
    isLoading: getClassDataIsLoading,
    isError: getCLassDataIsError,
    error: getClassDataError,
  } = useGetClassBySemesterIdQuery(semesterId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });
  useEffect(() => {
    if (classData) {
      setTimeout(() => {
        setIsLoadingClasses(false);
        setClasses(classData);
      }, 300);
    }
  }, [classData]);

  return (
    <Box component="main" sx={{ flex: '1', p: 2 }}>
      {getClassDataIsLoading || masterDataIsLoading || isLoadingClasses ? (
        <Box sx={{ maxWidth: '1400px', margin: '0 auto' }}>
          <Grid
            sx={{ paddingBottom: '24px', minHeight: '565px' }}
            container
            spacing={3}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            {[1, 2, 3, 4].map((index) => (
              <Grid key={index} item xl={3} lg={3} sm={6} md={4}>
                <Skeleton variant="text" animation="wave" />
                <Skeleton variant="text" animation="wave" />
                <Skeleton variant="text" animation="wave" />
                <Skeleton variant="text" animation="wave" />
                <Box sx={{ mt: 2 }}></Box>
                <Skeleton variant="text" animation="wave" />
                <Skeleton variant="text" animation="wave" />
                <Skeleton variant="text" animation="wave" />
                <Skeleton variant="text" animation="wave" />
              </Grid>
            ))}
          </Grid>
        </Box>
      ) : (
        <>
          <Box sx={{ maxWidth: '1400px', margin: '0 auto' }}>
            <Box sx={{ maxWidth: '20vw' }}>
              <Autocomplete
                sx={{ width: '250px', mb: 2 }}
                id="size-small-outlined"
                size="small"
                onChange={handleSemesterChange}
                value={
                  semesters.find((semester) => semester?._id === semesterId) ||
                  null
                }
                options={semesters}
                getOptionLabel={(option) => option.name}
                renderInput={(params) => (
                  <TextField {...params} label="SEMESTER" />
                )}
              />
            </Box>
            {/* end filter semesters */}
            {/* start card CoursesList */}
            {getClassDataIsSuccess && classes.length !== 0 ? (
              <Box sx={{ pt: 2 }}>
                <Grid
                  sx={{ paddingBottom: '24px' }}
                  container
                  spacing={3}
                  columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                >
                  {classes.map((data, index) => (
                    <Grid key={index} item xl={3} lg={3} sm={6} md={4}>
                      <Card sx={{ '& .MuiTypography-root': { mt: 0.5 } }}>
                        <CardContent sx={{ paddingBottom: '0' }}>
                          <Link
                            className="class-link-hover"
                            to={`/common/courses-list/class-details?classId=${data?._id}&semesterId=${semesterId}`}
                          >
                            <Typography variant="h5" component="div">
                              {`(${data?.subject?.subjectCode} - [${data?.semester?.name}])`}
                            </Typography>
                            <Typography variant="h5" component="div" noWrap>
                              {data?.subject?.subjectName}
                            </Typography>
                          </Link>
                          <Typography
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                              gap: 1,
                            }}
                            color="text.secondary"
                          >
                            <RecentActorsIcon fontSize="small" />{' '}
                            {data?.className}
                          </Typography>
                          <Typography
                            sx={{
                              overflow: 'hidden',
                              textOverflow: 'ellipsis',
                              whiteSpace: 'nowrap',
                              fontWeight: '600',
                            }}
                            color="text.secondary"
                          >
                            <AccountCircleIcon
                              fontSize="small"
                              style={{
                                overflow: 'hidden',
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                              }}
                            />
                            {data?.trainer?.fullName}
                          </Typography>
                          <Typography
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                              gap: 1,
                            }}
                            color="text.secondary"
                          >
                            <CoPresentIcon fontSize="small" /> Number of
                            students: {data?.students?.length}
                          </Typography>
                        </CardContent>
                        <CardActions
                          sx={{
                            padding: '10px 16px',
                            display: 'flex',
                            alignItems: 'center',
                          }}
                        >
                          <Link
                            to={`/common/courses-list/class-details?classId=${data?._id}&semesterId=${semesterId}`}
                          >
                            <span
                              style={{ color: '#0078d4', fontSize: '1.125rem' }}
                              title="Go to course"
                              value="Go to course"
                            >
                              Go to course <ArrowForwardIcon />
                            </span>
                          </Link>
                        </CardActions>
                      </Card>
                    </Grid>
                  ))}
                </Grid>
              </Box>
            ) : (
              <Box sx={{ mt: 2 }}>
                <Box sx={{ width: '100%', textAlign: 'center' }}>
                  <img
                    src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
                    alt="Img"
                    style={{
                      marginBottom: '25px',
                      maxWidth: '200px',
                      verticalAlign: 'middle',
                    }}
                  />
                  <Box>
                    <h3
                      style={{
                        fontSize: '1.125rem',
                        lineHeight: '1.2',
                        color: '#0078d4',
                        marginBottom: '10px',
                      }}
                    >
                      There is no data.
                    </h3>
                    <p style={{ margin: '0', fontSize: '20px' }}>
                      Please contact your school administration for more
                      information.
                    </p>
                  </Box>
                </Box>
              </Box>
            )}
            {/* end CoursesList */}
          </Box>

          {/* back-to-top and top-to-back scroll */}
          <Zoom in={trigger}>
            <Button
              onClick={handleScrollTop}
              color="primary"
              sx={{
                position: 'fixed',
                bottom: 16,
                right: 16,
                color: '#ee2c74',
              }}
            >
              <KeyboardArrowUpIcon sx={{ fontSize: '1.75rem' }} />
            </Button>
          </Zoom>
          <Zoom in={!trigger}>
            <Button
              onClick={handleScrollBottom}
              color="primary"
              sx={{
                position: 'fixed',
                top: 16,
                right: 16,
                color: '#ee2c74',
              }}
            >
              <KeyboardArrowDownIcon sx={{ fontSize: '1.75rem' }} />
            </Button>
          </Zoom>
        </>
      )}
    </Box>
  );
};

export default CoursesList;
