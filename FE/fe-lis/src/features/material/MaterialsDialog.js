import DeleteIcon from "@mui/icons-material/Delete";
import {
  Backdrop,
  Button,
  Card,
  Dialog,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  TextField,
  Tooltip,
} from "@mui/material";
import useAuth from "../../hooks/useAuth";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";

import { storage } from "../../config/firebase";

import ClearIcon from "@mui/icons-material/Clear";
import LinkIcon from "@mui/icons-material/Link";
import UploadFileIcon from "@mui/icons-material/UploadFile";
import { Box } from "@mui/system";
import React, { useEffect, useState } from "react";
import Title from "../../components/Title";
import CircularProgress from "@mui/material/CircularProgress";
import { useSnackbar } from "notistack";

const urlPattern = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;

const MaterialsDialog = ({
  openMaterialsDialog,
  handleCloseMaterialsDialog,
  material,
  addNewMaterial,
  addNewMaterialIsSuccess,
  addNewMaterialIsError,
  addNewMaterialIsLoading,
  addNewMaterialError,
  updateMaterial,
}) => {
  const { userLogin } = useAuth();
  const { enqueueSnackbar } = useSnackbar();
  const [sources, setSources] = useState(material?.sources || []);
  const [openAddLinkDialog, setOpenAddLinkDialog] = useState(false);
  const [link, setLink] = useState("");
  const [validLink, setValidLink] = useState(false);
  const [content, setContent] = useState("");
  const [validContent, setValidContent] = useState(false);
  const [progress, setProgress] = useState(0);
  const [selectedFile, setSelectedFile] = useState(null);

  const [openBackDrop, setOpenBackDrop] = useState(false);

  useEffect(() => {
    setSources(material?.sources || []);
    setContent(material?.content || "");
  }, [material]);

  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };

  const handleOpenAddLinkDialog = () => {
    setOpenAddLinkDialog(true);
  };
  const handleCloseAddLinkDialog = () => {
    setOpenAddLinkDialog(false);
  };
  const handleLinkChange = (event) => {
    setLink(event.target.value);
  };
  const handleContentChange = (event) => {
    setContent(event.target.value);
  };
  const handleSaveLink = () => {
    const source = { name: "Link", url: link, type: "link" };
    setSources([...sources, source]);
    handleCloseAddLinkDialog();
    setLink("");
  };
  const handleDeleteSource = (selectedSource) => {
    const newSources = sources.filter(
      (oldSources) => oldSources !== selectedSource
    );
    setSources(newSources);
  };
  const handleFileChange = (event) => {
    if (selectedFile) {
      setSelectedFile(null);
    } else {
      setSelectedFile(event.target.files[0]);
    }
  };

  const handleUpload = () => {
    handleOpenBackDrop();
    const fileName = selectedFile.name;
    const storageRef = ref(storage, `/files/${fileName}`);
    const uploadTask = uploadBytesResumable(storageRef, selectedFile);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        const uploaded = Math.floor(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(uploaded);
      },
      (error) => {
        console.log(error);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          setSelectedFile(null);
          const source = { name: selectedFile?.name, url, type: "file" };
          setSources([...sources, source]);
          enqueueSnackbar(`Upload ${fileName} successful !`, {
            variant: "success",
          });
          handleCloseBackDrop();
        });
      }
    );
  };

  const handleCreateMaterial = async () => {
    try {
      await addNewMaterial({ content, sources, uploadedBy: userLogin?._id });
    } catch (error) {
      enqueueSnackbar("Add new material fail ! " + error, { variant: "error" });
    }
  };
  const handleUpdateMaterial = async () => {
    try {
      await updateMaterial({
        id: material?._id,
        content,
        sources,
        uploadedBy: userLogin?._id,
      });
    } catch (error) {
      enqueueSnackbar("update new material fail ! " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (addNewMaterialIsError) {
      enqueueSnackbar(
        "Add material fail : " + addNewMaterialError?.data?.message,
        { variant: "error" }
      );
    }
  }, [addNewMaterialIsError]);
  useEffect(() => {
    if (addNewMaterialIsLoading) {
      handleOpenBackDrop();
    }
  }, [addNewMaterialIsLoading]);
  useEffect(() => {
    if (addNewMaterialIsSuccess) {
      handleCloseMaterialsDialog();
      setSources([]);
      setContent("");
    }
  }, [addNewMaterialIsSuccess]);

  useEffect(() => {
    setValidLink(urlPattern.test(link));
  }, [link]);
  useEffect(() => {
    setValidContent(content.trim() !== "");
  }, [content]);

  const canSave = validContent && sources.length !== 0;

  return (
    <>
      <Dialog
        open={openMaterialsDialog}
        onClose={handleCloseMaterialsDialog}
        fullWidth
        maxWidth="md"
      >
        <DialogTitle>
          <Box display={"flex"} justifyContent={"space-between"}>
            <Title>Upload Materials</Title>
            <Button color="error" onClick={handleCloseMaterialsDialog}>
              <ClearIcon />
            </Button>
          </Box>
        </DialogTitle>
        <Divider />

        <DialogContent>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <TextField
                id="materials-content"
                name="materials content"
                label="Materials Contents"
                value={content}
                multiline
                rows={4}
                onChange={handleContentChange}
                fullWidth
              />
            </Grid>
            <Grid item container spacing={3} xs={10}>
              <Grid item>
                <label htmlFor="upload-file">
                  <input
                    style={{ display: "none" }}
                    id="upload-file"
                    name="upload-file"
                    type="file"
                    onChange={handleFileChange}
                  />
                  <Button color="primary" variant="outlined" component="span">
                    <UploadFileIcon />
                  </Button>
                </label>
              </Grid>
              <Grid item>
                <Button
                  color="primary"
                  variant="outlined"
                  onClick={handleOpenAddLinkDialog}
                >
                  <LinkIcon />
                </Button>
              </Grid>
            </Grid>
            <Grid container item xs={2} justifyContent={"flex-end"}>
              {material ? (
                <Button
                  color="success"
                  variant="contained"
                  disabled={!canSave}
                  onClick={handleUpdateMaterial}
                >
                  Update
                </Button>
              ) : (
                <Button
                  color="success"
                  variant="contained"
                  disabled={!canSave}
                  onClick={handleCreateMaterial}
                >
                  Save
                </Button>
              )}
            </Grid>
            {/* material file display */}
            {selectedFile !== null && (
              <Grid item xs={12}>
                <Card>
                  <Grid container alignItems={"center"}>
                    <Grid item xs={1} padding={2}>
                      <UploadFileIcon fontSize="large" />
                    </Grid>
                    <Grid item xs={9} padding={2}>
                      {selectedFile?.name}
                    </Grid>
                    <Grid item xs={1} padding={2}>
                      <Button variant="contained" onClick={handleUpload}>
                        Upload
                      </Button>
                    </Grid>
                  </Grid>
                </Card>
              </Grid>
            )}
            {/* material card */}
            {sources?.length !== 0
              ? sources?.map((source, index) => (
                  <Grid item xs={12} key={index}>
                    <Card>
                      <Grid container alignItems={"center"}>
                        <Grid item xs={1} padding={2}>
                          {source?.type === "link" ? (
                            <LinkIcon fontSize="large" />
                          ) : (
                            <UploadFileIcon fontSize="large" />
                          )}
                        </Grid>
                        <Tooltip title={source?.url} placement="top" arrow>
                          <Grid item xs={10} padding={2}>
                            <a
                              href={source?.url}
                              target="_blank"
                              rel="noopener"
                            >
                              {source?.name}
                            </a>
                          </Grid>
                        </Tooltip>
                        <Grid item xs={1} padding={2}>
                          <Button
                            color="error"
                            onClick={() => handleDeleteSource(source)}
                          >
                            <DeleteIcon />
                          </Button>
                        </Grid>
                      </Grid>
                    </Card>
                  </Grid>
                ))
              : null}
          </Grid>
        </DialogContent>
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>

      <Dialog open={openAddLinkDialog} onClose={handleCloseAddLinkDialog}>
        <DialogTitle>Add Link</DialogTitle>
        <DialogContent sx={{ display: "flex", flexDirection: "row", gap: 2 }}>
          <TextField
            id="link"
            name="link"
            label="Link"
            value={link}
            fullWidth
            onChange={handleLinkChange}
          />
          <Button
            variant="contained"
            color="success"
            onClick={() => handleSaveLink()}
            disabled={!validLink}
          >
            Save
          </Button>
          <Button
            variant="contained"
            color="error"
            onClick={handleCloseAddLinkDialog}
          >
            Cancel
          </Button>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default MaterialsDialog;
