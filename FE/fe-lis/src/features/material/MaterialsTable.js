import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  Link,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import AddIcon from "@mui/icons-material/Add";
import MaterialsDialog from "./MaterialsDialog";
import { Box } from "@mui/system";
import {
  useDeleteMaterialMutation,
  useUpdateMaterialMutation,
} from "./materialApiSlice";
import { enqueueSnackbar } from "notistack";

const MaterialsTable = ({
  scheduleId,
  lessonId,
  setMaterials,
  lessons,
  materials,
  addNewMaterial,
  addNewMaterialIsSuccess,
  addNewMaterialIsError,
  addNewMaterialIsLoading,
  addNewMaterialError,
  refetch,
}) => {
  const [openMaterialsDialog, setOpenMaterialsDialog] = useState(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const [selectedMaterial, setSelectedMaterial] = useState(null);

  const [
    updateMaterial,
    {
      isSuccess: updateMaterialIsSuccess,
      isError: updateMaterialIsError,
      error: updateMaterialError,
    },
  ] = useUpdateMaterialMutation();

  useEffect(() => {
    if (updateMaterialIsSuccess) {
      enqueueSnackbar("Material updated Successful !", { variant: "success" });
      handleCloseMaterialsDialog();
      refetch();
    }
  }, [updateMaterialIsSuccess]);

  useEffect(() => {
    if (updateMaterialIsError) {
      enqueueSnackbar(
        "Material updated error !" + updateMaterialError?.data?.message,
        { variant: "error" }
      );
    }
  }, [updateMaterialIsError]);

  const [
    deleteMaterial,
    {
      isSuccess: deleteMaterialIsSuccess,
      isError: deleteMaterialIsError,
      error: deleteMaterialError,
    },
  ] = useDeleteMaterialMutation();

  useEffect(() => {
    if (deleteMaterialIsSuccess) {
      enqueueSnackbar("Material delete Successful !", { variant: "success" });
      handleCloseDeleteDialog();
      refetch();
    }
  }, [deleteMaterialIsSuccess]);

  useEffect(() => {
    if (deleteMaterialIsError) {
      enqueueSnackbar(
        "Material delete error !" + deleteMaterialError?.data?.message,
        { variant: "error" }
      );
    }
  }, [deleteMaterialIsError]);

  const handleOpenMaterialsDialog = () => {
    setOpenMaterialsDialog(true);
  };

  const handleCloseMaterialsDialog = () => {
    setOpenMaterialsDialog(false);
  };

  const handleDeleteClicked = (data) => {
    setSelectedMaterial(data);
    handleOpenDeleteDialog();
  };
  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false);
  };

  const handleViewMaterial = (data) => {
    setSelectedMaterial(data);
    handleOpenMaterialsDialog();
  };
  const handleAddNewMaterial = () => {
    setSelectedMaterial(null);
    handleOpenMaterialsDialog();
  };
  const handleDeleteMaterial = async () => {
    try {
      await deleteMaterial({ id: selectedMaterial?._id });
    } catch (error) {
      enqueueSnackbar("Error while delete material", { variant: "error" });
    }
  };

  return (
    <>
      <Grid container spacing={3}>
        <Grid item container justifyContent={"flex-end"} xs={12}>
          <Tooltip title="Add Materials" placement="top" arrow>
            <Button
              onClick={() => {
                handleAddNewMaterial();
              }}
            >
              {scheduleId || lessonId ? <AddIcon /> : null}
            </Button>
          </Tooltip>
        </Grid>
        <Grid item xs={12}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell sx={{ color: "indianred" }}>No.</TableCell>
                <TableCell sx={{ color: "indianred" }}>Content</TableCell>
                <TableCell sx={{ color: "indianred" }}>File</TableCell>
                <TableCell sx={{ color: "indianred" }}>Author</TableCell>
                <TableCell sx={{ color: "indianred" }}>Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {materials &&
                materials.map((data, index) => (
                  <TableRow key={data?._id} hover>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      {index + 1}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {data?.content}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {data?.sources &&
                        data?.sources?.map((source) => (
                          <Box>
                            <Tooltip title={source?.url} placement="top" arrow>
                              <Link
                                href={source?.url}
                                variant="body2"
                                underline="always"
                                target="_blank"
                              >
                                - {source?.name}
                              </Link>
                            </Tooltip>
                          </Box>
                        ))}
                    </TableCell>
                    <TableCell>{data?.uploadedBy?.email}</TableCell>
                    <TableCell>
                      <Button
                        color="success"
                        onClick={() => handleViewMaterial(data)}
                      >
                        View
                      </Button>
                      <Button
                        color="error"
                        onClick={() => handleDeleteClicked(data)}
                      >
                        delete
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              {lessons &&
                lessons.map(
                  (lesson) =>
                    lesson?.materials &&
                    lesson?.materials.map((data, index) => (
                      <TableRow key={data?._id} hover>
                        <TableCell sx={{ wordBreak: "break-word" }}>
                          {index + 1}
                        </TableCell>
                        <TableCell
                          sx={{ wordBreak: "break-word", width: "20%" }}
                        >
                          {data?.content}
                        </TableCell>
                        <TableCell
                          sx={{ wordBreak: "break-word", width: "20%" }}
                        >
                          {data?.sources &&
                            data?.sources?.map((source) => (
                              <Box>
                                <Tooltip
                                  title={source?.url}
                                  placement="top"
                                  arrow
                                >
                                  <Link
                                    href={source?.url}
                                    variant="body2"
                                    underline="always"
                                    target="_blank"
                                  >
                                    - {source?.name}
                                  </Link>
                                </Tooltip>
                              </Box>
                            ))}
                        </TableCell>
                        <TableCell>{data?.uploadedBy?.email}</TableCell>
                        <TableCell>
                          <Button color="error" disabled>
                            lesson provided
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))
                )}
            </TableBody>
          </Table>
        </Grid>
      </Grid>

      <MaterialsDialog
        material={selectedMaterial}
        openMaterialsDialog={openMaterialsDialog}
        addNewMaterial={addNewMaterial}
        addNewMaterialIsSuccess={addNewMaterialIsSuccess}
        addNewMaterialIsError={addNewMaterialIsError}
        addNewMaterialIsLoading={addNewMaterialIsLoading}
        addNewMaterialError={addNewMaterialError}
        updateMaterial={updateMaterial}
        handleCloseMaterialsDialog={handleCloseMaterialsDialog}
      />

      <Dialog
        fullWidth
        maxWidth={"sm"}
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
      >
        <DialogTitle>Delete Material</DialogTitle>
        <Divider />
        <DialogContent>
          <Typography variant="h6">
            You want delete material content: {selectedMaterial?.content}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog}>Cancel</Button>
          <Button color="error" onClick={handleDeleteMaterial}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default MaterialsTable;
