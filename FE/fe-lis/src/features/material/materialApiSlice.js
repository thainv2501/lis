import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

// Tạo adapter cho danh sách Lesson
const materialsAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = materialsAdapter.getInitialState();

// Tạo slice API cho Lesson
export const materialsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    // Lấy danh sách Lesson
    getMaterial: builder.query({
      query: () => ({
        url: "/materials",
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    addNewMaterials: builder.mutation({
      query: (initialData) => ({
        url: "/materials",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    updateMaterial: builder.mutation({
      query: (initialData) => ({
        url: "/materials",
        method: "PATCH",
        body: {
          ...initialData,
        },
      }),
    }),

    deleteMaterial: builder.mutation({
      query: (initialData) => ({
        url: "/materials",
        method: "DELETE",
        body: {
          ...initialData,
        },
      }),
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const {
  useAddNewMaterialsMutation,
  useDeleteMaterialMutation,
  useUpdateMaterialMutation,
} = materialsApiSlice;
