import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

const replyAdapter = createEntityAdapter({});

const initialState = replyAdapter.getInitialState();

export const replyApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getAllReplyBySupportQuestionId: builder.mutation({
      query: (initialData) => ({
        url: '/replies/reply',
        method: 'POST',
        body: {
          ...initialData,
        },
      }),
      transformResponse: (responseData) => {
        const loadedReplies = responseData.replies.map((data) => {
          data.id = data._id;
          return data;
        });
        return { ...responseData, replies: loadedReplies };
      },
    }),

    addNewReply: builder.mutation({
      query: (initialReplyData) => ({
        url: '/replies',
        method: 'POST',
        body: {
          ...initialReplyData,
        },
      }),
      invalidatesTags: [{ type: "Reply", id: "LIST" }],
    }),

    updateReply: builder.mutation({
      query: (initialReplyData) => ({
        url: '/replies',
        method: 'PATCH',
        body: {
          ...initialReplyData,
        },
      }),
      invalidatesTags: (result, error, arg) => [{ type: "Reply", id: arg.id }],
    }),
    deleteReply: builder.mutation({
      query: (initialReplyData) => ({
        url: '/replies',
        method: 'DELETE',
        body: { ...initialReplyData },
      }),
      invalidatesTags: (result, error, arg) => [{ type: "Reply", id: arg.id }],
    }),
  }),
});

export const {
  useGetAllReplyBySupportQuestionIdMutation,
  useAddNewReplyMutation,
  useUpdateReplyMutation,
  useDeleteReplyMutation,
} = replyApiSlice;
