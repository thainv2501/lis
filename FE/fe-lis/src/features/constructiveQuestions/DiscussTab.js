import {
  Backdrop,
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogTitle,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Pagination,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { enqueueSnackbar } from "notistack";
import { React, useEffect, useMemo, useRef, useState } from "react";
import useAuth from "../../hooks/useAuth";
import { useGetGroupOfStudentIdQuery } from "../groups/groupsApiSlice";
import { useDeleteReplyMutation } from "../reply/replyApiSlice";
import Comment from "./Comment";
import {
  useCreateCommentMutation,
  useDeleteCommentMutation,
  useGetAllCommentsMutation,
  useGetCommentsWithFilterMutation,
} from "./commentApiSlice";
import { green } from "@mui/material/colors";
import { useGetRemainVotesOfUserMutation } from "./voteApiSlice";
import * as XLSX from "xlsx";

// Load the relative time plugin
dayjs.extend(relativeTime);

function DiscussTab({
  diffEndTimeToNow,
  schedule,
  CQId,
  constructiveQuestion,
  slotValue,
}) {
  const { userLogin, isAdmin } = useAuth();
  const [selectedComment, setSelectedComment] = useState(null);
  const [selectedReply, setSelectedReply] = useState(null);
  const [page, setPage] = useState(1);
  const [remainingVotes, setRemainingVotes] = useState({});

  const voteSettingTemplate = useMemo(() => {
    if (isAdmin || userLogin._id === schedule.trainer._id) {
      return {
        outsideVote: {
          redCard: constructiveQuestion.voteSetting.redCard.trainerVote,
          blueCard: constructiveQuestion.voteSetting.blueCard.trainerVote,
          greenCard: constructiveQuestion.voteSetting.greenCard.trainerVote,
          blackCard: constructiveQuestion.voteSetting.blackCard.trainerVote,
        },
        insideVote: {
          redCard: constructiveQuestion.voteSetting.redCard.trainerVote,
          blueCard: constructiveQuestion.voteSetting.blueCard.trainerVote,
          greenCard: constructiveQuestion.voteSetting.greenCard.trainerVote,
          blackCard: constructiveQuestion.voteSetting.blackCard.trainerVote,
        },
      };
    } else {
      return {
        outsideVote: {
          redCard: constructiveQuestion.voteSetting.redCard.outsideVote,
          blueCard: constructiveQuestion.voteSetting.blueCard.outsideVote,
          greenCard: constructiveQuestion.voteSetting.greenCard.outsideVote,
          blackCard: constructiveQuestion.voteSetting.blackCard.outsideVote,
        },
        insideVote: {
          redCard: constructiveQuestion.voteSetting.redCard.insideVote,
          blueCard: constructiveQuestion.voteSetting.blueCard.insideVote,
          greenCard: constructiveQuestion.voteSetting.greenCard.insideVote,
          blackCard: constructiveQuestion.voteSetting.blackCard.insideVote,
        },
      };
    }
  }, [isAdmin, userLogin._id, schedule.trainer._id, constructiveQuestion]);

  const handlePaginationChanged = (event, value) => {
    setPage(value);
  };

  const [filterType, setFilterType] = useState("inside-group");
  const [voteSetting, setVoteSetting] = useState(
    voteSettingTemplate.insideVote
  );
  const handleQuestionFilterChange = (event) => {
    setFilterType(event.target.value);
  };
  const commentRef = useRef();

  const [openBackdrop, setOpenBackdrop] = useState(false);
  const handleOpenBackdrop = () => {
    setOpenBackdrop(true);
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };
  const [openDeleteCommentDialog, setOpenDeleteCommentDialog] = useState(false);
  const handleOpenDeleteCommentDialog = () => {
    setOpenDeleteCommentDialog(true);
  };

  const handleCloseDeleteCommentDialog = () => {
    setOpenDeleteCommentDialog(false);
  };
  const [openDeleteReplyDialog, setOpenDeleteReplyDialog] = useState(false);
  const handleOpenDeleteReplyDialog = () => {
    setOpenDeleteReplyDialog(true);
  };

  const [
    deleteComment,
    {
      isSuccess: deleteCommentIsSuccess,
      isError: deleteCommentIsError,
      isLoading: deleteCommentIsLoading,
      error: deleteCommentError,
    },
  ] = useDeleteCommentMutation();
  const handleDeleteComment = async () => {
    try {
      await deleteComment({ id: selectedComment._id });
    } catch (error) {
      enqueueSnackbar("Error with delete Function : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (deleteCommentIsSuccess) {
      handleCloseDeleteCommentDialog();
      handleGetCommentsWithFilter();
    }
    if (deleteCommentIsError) {
      enqueueSnackbar("Error : " + deleteCommentError?.data?.message);
    }
    if (deleteCommentIsLoading) {
    }
  }, [deleteCommentIsSuccess, deleteCommentIsError, deleteCommentIsLoading]);

  const handleCloseDeleteReplyDialog = () => {
    setOpenDeleteReplyDialog(false);
  };

  const [
    deleteReply,
    {
      isSuccess: deleteReplyIsSuccess,
      isError: deleteReplyIsError,
      isLoading: deleteReplyIsLoading,
      error: deleteReplyError,
    },
  ] = useDeleteReplyMutation();

  const handleDeleteReply = async () => {
    try {
      await deleteReply({ id: selectedReply._id });
    } catch (error) {
      enqueueSnackbar("Error with DeleteFunction : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (deleteReplyIsSuccess) {
      handleGetCommentsWithFilter();
      handleCloseDeleteReplyDialog();
    }
    if (deleteReplyIsError) {
      enqueueSnackbar("Error : " + deleteReplyError?.data?.message, {
        variant: "error",
      });
    }
    if (deleteReplyIsLoading) {
    }
  }, [deleteReplyIsSuccess, deleteReplyIsError, deleteReplyIsLoading]);

  const [comments, setComments] = useState([]);
  const [group, setGroup] = useState();
  const {
    data: groupRes,
    isSuccess: getGroupOfStudentIdIsSuccess,
    isLoading: getGroupOfStudentIdIsLoading,
    isError: getGroupOfStudentIdIsError,
    error: getGroupOfStudentIdError,
  } = useGetGroupOfStudentIdQuery(
    `?scheduleId=${schedule._id}&studentId=${userLogin._id}`,
    { refetchOnFocus: true }
  );

  useEffect(() => {
    if (groupRes) {
      setGroup(groupRes);
    }
  }, [groupRes]);

  const [
    createComment,
    {
      isSuccess: createCommentIsSuccess,
      isLoading: createCommentIsLoading,
      isError: createCommentIsError,
      error: createCommentError,
    },
  ] = useCreateCommentMutation();

  const handlePostComment = async () => {
    try {
      if (commentRef.current.value.trim() !== "") {
        await createComment({
          scheduleId: schedule._id,
          CQId,
          createdBy: userLogin._id,
          content: commentRef.current.value.trim(),
        });
      }
    } catch (error) {
      enqueueSnackbar("Error While Post Comment !" + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (createCommentIsSuccess) {
      enqueueSnackbar("Posted Comment", { variant: "success" });
      handleGetCommentsWithFilter();
      commentRef.current.value = "";
      handleCloseBackdrop();
    }
    if (createCommentIsError) {
      enqueueSnackbar(
        "Posted Comment Error : " + createCommentError?.data?.message,
        { variant: "error" }
      );
      handleCloseBackdrop();
    }
    if (createCommentIsLoading) {
      handleOpenBackdrop();
    }
  }, [createCommentIsSuccess, createCommentIsError]);

  const [
    getCommentsWithFilter,
    {
      data: commentsRes,
      isSuccess: getCommentsWithFilterIsSuccess,
      isError: getCommentsWithFilterIsError,
      isLoading: getCommentsWithFilterIsLoading,
      error: getCommentsWithFilterError,
    },
  ] = useGetCommentsWithFilterMutation();

  useEffect(() => {
    if (getCommentsWithFilterIsLoading) {
      handleOpenBackdrop();
    }
    if (getCommentsWithFilterIsSuccess || getCommentsWithFilterIsError) {
      handleCloseBackdrop();
    }
  }, [
    getCommentsWithFilterIsLoading,
    getCommentsWithFilterIsSuccess,
    getCommentsWithFilterIsError,
  ]);

  const handleGetCommentsWithFilter = async () => {
    try {
      if (isAdmin || userLogin._id === schedule.trainer._id) {
        await getCommentsWithFilter({
          scheduleId: schedule._id,
          CQId,
          members: schedule.inClass.students,
          filter: filterType,
          page,
        });
      }
      if (!isAdmin && userLogin._id !== schedule.trainer._id && group) {
        await getCommentsWithFilter({
          scheduleId: schedule._id,
          CQId,
          members: group.members,
          filter: filterType,
          page,
        });
      }
    } catch (error) {
      enqueueSnackbar("Error While get Comments " + error, {
        variant: "error",
      });
    }
  };

  const [
    getRemainVotesOfUser,
    {
      data: remainingVotesRes,
      isSuccess: getRemainVotesOfUserIsSuccess,
      isError: getRemainVotesOfUserIsError,
      isLoading: getRemainVotesOfUserIsLoading,
      error: getRemainVotesOfUserError,
    },
  ] = useGetRemainVotesOfUserMutation();

  useEffect(() => {
    if (getRemainVotesOfUserIsLoading) {
      handleOpenBackdrop();
    }
    if (getRemainVotesOfUserIsSuccess || getRemainVotesOfUserIsError) {
      handleCloseBackdrop();
    }
  }, [
    getRemainVotesOfUserIsLoading,
    getRemainVotesOfUserIsSuccess,
    getRemainVotesOfUserIsError,
  ]);

  const handleGetRemainVotesWithFilter = async () => {
    try {
      if (isAdmin || userLogin._id === schedule.trainer._id) {
        await getRemainVotesOfUser({
          scheduleId: schedule._id,
          CQId,
          userId: userLogin._id,
          filter: filterType,
          isTrainer: true,
        });
      }
      if (!isAdmin && userLogin._id !== schedule.trainer._id && group) {
        await getRemainVotesOfUser({
          scheduleId: schedule._id,
          CQId,
          userId: userLogin._id,
          filter: filterType,
          isTrainer: false,
        });
      }
    } catch (error) {
      enqueueSnackbar("Error While get Comments " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    handleGetCommentsWithFilter();
    handleGetRemainVotesWithFilter();
  }, []);

  useEffect(() => {
    if (filterType === "inside-group") {
      setVoteSetting(voteSettingTemplate.insideVote);
    }
    if (filterType === "outside-group") {
      setVoteSetting(voteSettingTemplate.outsideVote);
    }
    handleGetCommentsWithFilter();
    handleGetRemainVotesWithFilter();
  }, [filterType, group, page]);

  useEffect(() => {
    if (commentsRes && commentsRes?.comments) {
      setComments(commentsRes.comments);
    }
  }, [commentsRes]);
  useEffect(() => {
    if (remainingVotesRes) {
      setRemainingVotes(remainingVotesRes);
    }
  }, [remainingVotesRes]);

  const [
    getAllComments,
    {
      data: allComments,
      isLoading: getAllCommentsIsLoading,
      isSuccess: getAllCommentsIsSuccess,
      isError: getAllCommentsIsError,
      error: getAllCommentsError,
    },
  ] = useGetAllCommentsMutation();

  const handleGetAllComments = async () => {
    try {
      await getAllComments({ scheduleId: schedule._id, CQId });
    } catch (error) {
      enqueueSnackbar("Error with getAll Function : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (getAllCommentsIsError) {
      enqueueSnackbar("Error : " + getAllCommentsError?.data?.message);
    }
    if (getAllCommentsIsLoading) {
    }
    if (getAllCommentsIsSuccess) {
      if (allComments && allComments?.length !== 0) {
        const commentsDataSheet = allComments?.map((comment) => {
          return {
            "Full Name": comment?.createdBy?.fullName,
            Email: comment.createdBy?.email,
            Comment: comment.content,
            Star: comment.votes.reduce((acc, vote) => acc + vote.star, 0),
          };
        });
        const wb = XLSX.utils.book_new();
        const ws = XLSX.utils.json_to_sheet(commentsDataSheet);
        XLSX.utils.book_append_sheet(wb, ws, "MySheet1");
        XLSX.writeFile(
          wb,
          `${schedule?.inClass?.className}-${schedule?.inClass?.subject?.subjectCode}-Slot_${slotValue}-Comments.xlsx`
        );
      } else {
        enqueueSnackbar("Something wrong with data", {
          variant: "error",
        });
      }
    }
  }, [getAllCommentsIsError, getAllCommentsIsLoading, getAllCommentsIsSuccess]);

  let commentsContent = null;

  if (getCommentsWithFilterIsSuccess && comments) {
    commentsContent =
      comments &&
      comments.map((comment) => (
        <Comment
          comment={comment}
          diffEndTimeToNow={diffEndTimeToNow}
          handleGetCommentsWithFilter={handleGetCommentsWithFilter}
          handleOpenDeleteCommentDialog={handleOpenDeleteCommentDialog}
          handleCloseBackdrop={handleCloseBackdrop}
          handleOpenBackdrop={handleOpenBackdrop}
          setSelectedComment={setSelectedComment}
          setSelectedReply={setSelectedReply}
          handleOpenDeleteReplyDialog={handleOpenDeleteReplyDialog}
          voteSetting={voteSetting}
          remainingVotes={remainingVotes}
          filterType={filterType}
          schedule={schedule}
          handleGetRemainVotesWithFilter={handleGetRemainVotesWithFilter}
        />
      ));
  }

  let discussContent = null;

  if (
    getGroupOfStudentIdIsError &&
    !isAdmin &&
    userLogin._id !== schedule.trainer._id
  ) {
    discussContent = (
      <Typography color={"red"}>
        you are not in any group ask trainer add you to group !
      </Typography>
    );
  }

  if (
    (getGroupOfStudentIdIsSuccess && group) ||
    isAdmin ||
    userLogin._id === schedule.trainer._id
  ) {
    const pages = Math.ceil(commentsRes?.commentsCount / 5) || 0;
    discussContent = (
      <>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            padding: "0.5rem",
            justifyContent: "space-between",
          }}
        >
          <Box>
            <FormControl
              fullWidth
              sx={{
                display: "inline-flex",
                flexDirection: "column",
                minWidth: "0",
                padding: "0",
                margin: "0",
                border: "0",
                verticalAlign: "top",
                width: "100%",
              }}
            >
              <InputLabel id="filter-select">Select</InputLabel>
              <Select
                value={filterType}
                id="filter-select"
                label="Select"
                onChange={handleQuestionFilterChange}
                size="small"
                sx={{
                  width: "250px",
                }}
              >
                <MenuItem value={"voted"}>Voted</MenuItem>
                <MenuItem value={"inside-group"}>Inside Group</MenuItem>
                <MenuItem value={"outside-group"}>Outside Group</MenuItem>
              </Select>
            </FormControl>
          </Box>
          <Box display={"flex"} gap={2}>
            {isAdmin || userLogin._id === schedule.trainer._id ? (
              <Button
                variant="contained"
                color="success"
                onClick={handleGetAllComments}
              >
                Get Comments
              </Button>
            ) : null}
            <Button
              variant="contained"
              color="success"
              onClick={() => {
                handleGetCommentsWithFilter();
                handleGetRemainVotesWithFilter();
              }}
            >
              Reload
            </Button>
          </Box>
        </Box>
        {diffEndTimeToNow > 0 && filterType === "inside-group" && (
          <>
            <Box sx={{ mt: 3, mb: 3 }}>
              <TextField
                multiline
                rows={4}
                fullWidth
                label={"Your Comment"}
                inputRef={commentRef}
              />
              <Grid container justifyContent={"flex-end"} mt={2}>
                <Button variant="contained" onClick={handlePostComment}>
                  Post
                </Button>
              </Grid>
            </Box>
            <Divider />
          </>
        )}
        {commentsContent}
        <Box sx={{ xs: 12 }}>
          <Pagination
            count={pages}
            page={page}
            onChange={handlePaginationChanged}
            variant="outlined"
            shape="rounded"
          />
        </Box>
        {/* delete comment dialog */}
        <Dialog
          maxWidth={"sm"}
          fullWidth
          open={openDeleteCommentDialog}
          onClose={handleCloseDeleteCommentDialog}
        >
          <DialogTitle>Delete Your Comment</DialogTitle>
          <DialogActions>
            <Button onClick={handleCloseDeleteCommentDialog}>Cancel</Button>
            <Button onClick={handleDeleteComment}>Confirm</Button>
          </DialogActions>
        </Dialog>
        {/* delete reply dialog */}
        <Dialog
          maxWidth={"sm"}
          fullWidth
          open={openDeleteReplyDialog}
          onClose={handleCloseDeleteReplyDialog}
        >
          <DialogTitle>Delete Your Reply</DialogTitle>
          <DialogActions>
            <Button onClick={handleCloseDeleteReplyDialog}>Cancel</Button>
            <Button onClick={handleDeleteReply}>Confirm</Button>
          </DialogActions>
        </Dialog>
        <Backdrop
          open={openBackdrop}
          onClick={handleCloseBackdrop}
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </>
    );
  }

  return discussContent;
}

export default DiscussTab;
