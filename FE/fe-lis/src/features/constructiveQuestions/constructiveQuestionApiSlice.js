import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

// Tạo adapter cho danh sách Lesson
const constructiveQuestionsAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = constructiveQuestionsAdapter.getInitialState();

// Tạo slice API cho Lesson
export const constructiveQuestionsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getCQsWithStatus: builder.query({
      query: (status) => ({
        url: "/constructiveQuestions/" + status,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    // Thêm mới Lesson
    addNewCQ: builder.mutation({
      query: (initialData) => ({
        url: "/constructiveQuestions",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    updateCQ: builder.mutation({
      query: (initialData) => ({
        url: "/constructiveQuestions",
        method: "PATCH",
        body: {
          ...initialData,
        },
      }),
    }),
    updateCQTime: builder.mutation({
      query: (initialData) => ({
        url: "/constructiveQuestions/time",
        method: "PATCH",
        body: {
          ...initialData,
        },
      }),
    }),

    deleteCQ: builder.mutation({
      query: ({ id }) => ({
        url: `/constructiveQuestions`,
        method: "DELETE",
        body: { id },
      }),
    }),

    getCQWithLessonId: builder.query({
      query: (id) => ({
        url: "/constructiveQuestions/lessonId/" + id,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    getCQWithId: builder.mutation({
      query: (initialData) => ({
        url: "/constructiveQuestions/getCQ",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const {
  useAddNewCQMutation,
  useUpdateCQMutation,
  useDeleteCQMutation,
  useUpdateCQTimeMutation,
  useGetCQWithIdMutation,
} = constructiveQuestionsApiSlice;
