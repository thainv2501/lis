import { React, useEffect, useState } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import {
  Paper,
  Box,
  Button,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  IconButton,
  TextField,
  InputLabel,
  Rating,
  Autocomplete,
  Skeleton,
  Typography,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import {
  useCreatePresentationMutation,
  useGetPresentationsMutation,
} from "./presentationApiSlice";
import { enqueueSnackbar } from "notistack";
import {
  useGetGroupOfStudentIdQuery,
  useGetGroupsByScheduleIdQuery,
} from "../groups/groupsApiSlice";
import useAuth from "../../hooks/useAuth";
import Presentation from "./Presentation";
import { useCreateGradeMutation } from "./gradeApiSlice";

function GradeTab({
  CQId,
  scheduleId,
  diffEndTimeToNow,
  schedule,
  handleOpenBackDrop,
  handleCloseBackDrop,
}) {
  const { userLogin, isAdmin } = useAuth();
  const isTrainer = userLogin?._id === schedule?.trainer?._id;
  const [presentations, setPresentations] = useState(null);
  const [
    getPresentations,
    {
      data: presentationsRes,
      isSuccess: getPresentationsIsSuccess,
      isLoading: getPresentationsIsLoading,
      isError: getPresentationsIsError,
      error: getPresentationsError,
    },
  ] = useGetPresentationsMutation();

  const handleGetPresentations = async () => {
    setPresentations(null);
    try {
      await getPresentations({ scheduleId, CQId });
    } catch (error) {
      enqueueSnackbar("Error with getFunctions : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    handleGetPresentations();
  }, []);

  useEffect(() => {
    if (presentationsRes && presentationsRes.length !== 0) {
      setPresentations(presentationsRes);
    }
  }, [presentationsRes]);

  useEffect(() => {
    if (getPresentationsIsSuccess) {
    }
    if (getPresentationsIsLoading) {
    }
    if (getPresentationsIsError) {
      enqueueSnackbar(
        "get presentation error : " + getPresentationsError?.data?.message
      );
    }
  }, [
    getPresentationsIsSuccess,
    getPresentationsIsError,
    getPresentationsIsLoading,
  ]);

  const {
    data: groupsData,
    isSuccess: getGroupsByScheduleIdIsSuccess,
    isLoading: getGroupsByScheduleIdIsLoading,
    isError: getGroupsByScheduleIdIsError,
    error: getGroupsByScheduleIdError,
    refetch,
  } = useGetGroupsByScheduleIdQuery(scheduleId);

  useEffect(() => {
    refetch();
  }, []);

  const [groups, setGroups] = useState([]);

  useEffect(() => {
    if (groupsData) {
      setGroups(groupsData);
      setPresentationGroup(groupsData[0]);
    }
  }, [groupsData]);

  // open dialog grading group
  const [selectedPresentation, setSelectedPresentation] = useState(null);
  const [openGradeGroupDialog, setOpenGradeGroupDialog] = useState(false);
  const handleClickOpenGradeGroupDialog = (presentation) => {
    setSelectedPresentation(presentation);
    setOpenGradeGroupDialog(true);
  };

  const handleCloseGradeGroupDialog = () => {
    setSelectedPresentation(null);
    setOpenGradeGroupDialog(false);
  };
  const [ratingGroup, setRatingGroup] = useState({
    keepTime: 0,
    meetRequirements: 0,
    presentations: 0,
    goodInformation: 0,
  });

  useEffect(() => {
    setRatingGroup(
      selectedPresentation?.grade?.find(
        (grade) => grade?.votedBy === userLogin._id
      ) || {
        keepTime: 0,
        meetRequirements: 0,
        presentations: 0,
        goodInformation: 0,
      }
    );
  }, [selectedPresentation]);

  const handleRatingGroupChange = (name, value) => {
    setRatingGroup((prevRatings) => ({
      ...prevRatings,
      [name]: value,
    }));
  };

  // Calculate and update myTotalRatingGroupScore whenever ratingGroup changes
  const [myTotalRatingGroupScore, setMyTotalRatingGroupScore] = useState(0);

  useEffect(() => {
    const calculateTotalRatingGroupScore = () => {
      const { keepTime, meetRequirements, presentations, goodInformation } =
        ratingGroup;
      const totalScore =
        (keepTime + meetRequirements + presentations + goodInformation) / 5;
      return totalScore;
    };

    setMyTotalRatingGroupScore(calculateTotalRatingGroupScore());
  }, [ratingGroup]);

  const [
    createGrade,
    {
      isError: createGradeIsError,
      isSuccess: createGradeIsSuccess,
      isLoading: createGradeIsLoading,
      error: createGradeError,
    },
  ] = useCreateGradeMutation();

  const handleSaveGroupScore = async () => {
    try {
      await createGrade({
        presentation: selectedPresentation._id,
        votedBy: userLogin._id,
        keepTime: ratingGroup.keepTime,
        meetRequirements: ratingGroup.meetRequirements,
        presentations: ratingGroup.presentations,
        goodInformation: ratingGroup.goodInformation,
      });
    } catch (error) {
      enqueueSnackbar("Error with grade function : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (createGradeIsSuccess) {
      handleGetPresentations();
      handleCloseGradeGroupDialog();
    }
    if (createGradeIsError) {
      enqueueSnackbar(
        "Grade Group Error : " + createGradeError?.data?.message,
        { variant: "error" }
      );
    }
    if (createGradeIsLoading) {
    }
  }, [createGradeIsSuccess, createGradeIsError, createGradeIsLoading]);

  // handle open dialog set round
  const [openSetRoundDialog, setOpenSetRoundDialog] = useState(false);
  const handleClickOpenSetRoundDialog = () => {
    setOpenSetRoundDialog(true);
  };

  const handleCloseSetRoundDialog = () => {
    setOpenSetRoundDialog(false);
  };

  const [
    createPresentation,
    {
      isSuccess: createPresentationIsSuccess,
      isLoading: createPresentationIsLoading,
      isError: createPresentationIsError,
      error: createPresentationError,
    },
  ] = useCreatePresentationMutation();

  // handle set round
  const [presentationGroup, setPresentationGroup] = useState(null);
  const [reviewGroup, setReviewGroup] = useState(null);

  const [remainingGroups, setRemainingGroups] = useState([]);
  useEffect(() => {
    const remainingGroups = groups.filter((group) => {
      return group.groupNumber !== presentationGroup.groupNumber;
    });
    setRemainingGroups(remainingGroups);
  }, [presentationGroup]);

  const handleSetRoundGroupPresentations = async () => {
    if (diffEndTimeToNow > 0 && (isAdmin || isTrainer)) {
      try {
        await createPresentation({
          scheduleId,
          CQId,
          presentationGroup: presentationGroup?._id,
          reviewGroup: reviewGroup?._id,
        });
      } catch (error) {
        enqueueSnackbar("error with create presentation Function : " + error, {
          variant: "error",
        });
      }
    } else {
      enqueueSnackbar("Error with time or access !");
    }
  };

  useEffect(() => {
    if (createPresentationIsSuccess) {
      handleGetPresentations();
      handleCloseSetRoundDialog();
    }
    if (createPresentationIsError) {
      enqueueSnackbar(
        "create presentation error : " + createPresentationError?.data?.message,
        { variant: "error" }
      );
    }
    if (createPresentationIsLoading) {
    }
  }, [
    createPresentationIsSuccess,
    createPresentationIsError,
    createPresentationIsLoading,
  ]);

  const [group, setGroup] = useState();

  let gradeBodyContent = (
    <Box sx={{ width: "100%" }}>
      <Skeleton />
      {/* Render a loop of 10 skeletons */}
      {Array.from({ length: 6 }).map((_, index) => (
        <div key={index}>
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
        </div>
      ))}
    </Box>
  );

  if (getPresentationsIsSuccess && presentations) {
    gradeBodyContent = (
      <>
        {presentations.map((presentation, index) => (
          <Presentation
            handleCloseBackDrop={handleCloseBackDrop}
            handleOpenBackDrop={handleOpenBackDrop}
            presentation={presentation}
            index={index}
            handleClickOpenGradeGroupDialog={handleClickOpenGradeGroupDialog}
            isTrainer={isTrainer}
            handleGetPresentations={handleGetPresentations}
            group={group}
            diffEndTimeToNow={diffEndTimeToNow}
          />
        ))}
      </>
    );
  }

  const {
    data: groupRes,
    isSuccess: getGroupOfStudentIdIsSuccess,
    isLoading: getGroupOfStudentIdIsLoading,
    isError: getGroupOfStudentIdIsError,
    error: getGroupOfStudentIdError,
  } = useGetGroupOfStudentIdQuery(
    `?scheduleId=${schedule._id}&studentId=${userLogin._id}`,
    { refetchOnFocus: true }
  );

  useEffect(() => {
    if (groupRes) {
      setGroup(groupRes);
    }
  }, [groupRes]);

  let gradeTabContent = (
    <Box sx={{ width: "100%" }}>
      <Skeleton />
      {/* Render a loop of 10 skeletons */}
      {Array.from({ length: 6 }).map((_, index) => (
        <div key={index}>
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
        </div>
      ))}
    </Box>
  );

  if (getGroupOfStudentIdIsError && !isAdmin && !isTrainer) {
    gradeTabContent = (
      <Typography color={"red"}>
        you are not in any group ask trainer add you to group !
      </Typography>
    );
  }

  if ((getGroupOfStudentIdIsSuccess && group) || isAdmin || isTrainer) {
    gradeTabContent = (
      <>
        <Box sx={{ display: "flex", flexWrap: "wrap" }}>
          <Box
            sx={{
              width: "100%",
              textAlign: "center",
              display: "flex",
              justifyContent: "space-evenly",
              padding: "0 15px",
            }}
          >
            <Button
              variant="contained"
              sx={{ minWidth: "200px", padding: "6px 16px" }}
              onClick={handleGetPresentations}
            >
              Refresh
            </Button>
            {isAdmin || isTrainer ? (
              <>
                {" "}
                <Button
                  variant="contained"
                  color="success"
                  sx={{ minWidth: "200px", padding: "6px 16px" }}
                >
                  Get individual score
                </Button>
                <Button
                  variant="contained"
                  sx={{ minWidth: "200px", padding: "6px 16px" }}
                  onClick={handleClickOpenSetRoundDialog}
                >
                  Set Round
                </Button>
                <Button
                  variant="contained"
                  color="success"
                  sx={{ minWidth: "200px", padding: "6px 16px" }}
                >
                  Get Grade
                </Button>
              </>
            ) : null}
          </Box>
          {gradeBodyContent}
        </Box>
        {/* Dialog Set Round For Presentations */}
        <Dialog
          open={openSetRoundDialog}
          keepMounted
          onClose={handleCloseSetRoundDialog}
          aria-describedby="set-round-dialog-slide-description"
          maxWidth="lg"
        >
          <DialogTitle sx={{ padding: "0", marginBottom: "24px" }}>
            <Paper
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
                paddingRight: "10px",
                paddingLeft: "10px",
                alignItems: "center",
              }}
            >
              <Box>Set Round Presentations</Box>
              <Box>
                <IconButton onClick={handleCloseSetRoundDialog}>
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              </Box>
            </Paper>
          </DialogTitle>
          <DialogContent
            sx={{
              width: "100%",
              paddingTop: "24px",
              paddingBottom: "24px",
            }}
          >
            <Paper sx={{ p: 2, width: "600px" }}>
              <table>
                <tr>
                  <th>
                    {" "}
                    <InputLabel sx={{ mr: 4, mb: 2 }}>Presenting: </InputLabel>
                  </th>
                  <td>
                    {" "}
                    <Autocomplete
                      sx={{ width: "200px", mb: 2 }}
                      id="size-small-outlined"
                      size="small"
                      options={groups}
                      value={presentationGroup}
                      getOptionLabel={(option) => `Group ${option.groupNumber}`}
                      onChange={(e, newValue) => setPresentationGroup(newValue)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Presenting"
                          placeholder="Presenting..."
                        />
                      )}
                    />
                  </td>
                </tr>
                <tr>
                  <th>
                    <InputLabel sx={{ mr: 4, mb: 2 }}>Reviewing: </InputLabel>
                  </th>

                  <td>
                    <Autocomplete
                      sx={{ width: "200px", mb: 2 }}
                      id="size-small-outlined"
                      size="small"
                      options={remainingGroups}
                      getOptionLabel={(option) => `Group ${option.groupNumber}`}
                      value={reviewGroup}
                      onChange={(e, newValue) => setReviewGroup(newValue)}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Reviewing"
                          placeholder="Reviewing..."
                        />
                      )}
                    />
                  </td>
                </tr>
              </table>
            </Paper>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseSetRoundDialog}>Close</Button>
            <Button
              onClick={handleSetRoundGroupPresentations}
              color="primary"
              variant="contained"
            >
              Set
            </Button>
          </DialogActions>
        </Dialog>
        {/* Dialog Grade For Group */}
        <Dialog
          open={openGradeGroupDialog}
          keepMounted
          onClose={handleCloseGradeGroupDialog}
          aria-describedby="grade-dialog-slide-description"
          maxWidth="lg"
        >
          <DialogTitle sx={{ padding: "0" }}>
            <Paper
              sx={{
                display: "flex",
                justifyContent: "space-between",
                width: "100%",
                paddingRight: "10px",
                paddingLeft: "10px",
                alignItems: "center",
              }}
            >
              <Box>Grade Presenting Group</Box>
              <Box>
                <IconButton onClick={handleCloseGradeGroupDialog}>
                  <CloseIcon fontSize="inherit" />
                </IconButton>
              </Box>
            </Paper>
          </DialogTitle>
          <DialogContent sx={{ width: "100%" }}>
            <p
              style={{
                marginTop: "1rem",
                marginBottom: "1rem",
                fontSize: "1rem",
              }}
            >
              You are grading for:{" "}
              <b>{`Group ${selectedPresentation?.presentationGroup?.groupNumber}`}</b>{" "}
              (<i style={{ fontSize: "0.875rem" }}>Click on stars to grade</i>)
            </p>
            <TableContainer
              sx={{
                border: "1px solid rgba(224, 224, 224, 1)",
                maxHeight: 440,
              }}
            >
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" sx={{ width: "140px" }}>
                      Keep time
                    </TableCell>
                    <TableCell align="center" sx={{ width: "160px" }}>
                      Meet requirements
                    </TableCell>
                    <TableCell align="center" sx={{ width: "100px" }}>
                      Presentations
                    </TableCell>
                    <TableCell align="center" sx={{ width: "160px" }}>
                      Good information
                    </TableCell>
                    <TableCell align="center" sx={{ width: "100px" }}>
                      Total
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow hover role="checkbox" tabIndex={-1}>
                    <TableCell align="center">
                      <Rating
                        name={"keep-time"}
                        value={ratingGroup.keepTime}
                        size="small"
                        onChange={(event, value) =>
                          handleRatingGroupChange("keepTime", value)
                        }
                      />
                    </TableCell>
                    <TableCell align="center">
                      <Rating
                        name={"meet-requirements"}
                        value={ratingGroup.meetRequirements}
                        size="small"
                        onChange={(event, value) =>
                          handleRatingGroupChange("meetRequirements", value)
                        }
                      />
                    </TableCell>
                    <TableCell align="center">
                      <Rating
                        name={"presentations"}
                        value={ratingGroup.presentations}
                        size="small"
                        onChange={(event, value) =>
                          handleRatingGroupChange("presentations", value)
                        }
                      />
                    </TableCell>
                    <TableCell align="center">
                      <Rating
                        name={"good-information"}
                        value={ratingGroup.goodInformation}
                        size="small"
                        onChange={(event, value) =>
                          handleRatingGroupChange("goodInformation", value)
                        }
                      />
                    </TableCell>
                    <TableCell align="center">
                      {myTotalRatingGroupScore}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseGradeGroupDialog}>Cancel</Button>
            <Button
              onClick={handleSaveGroupScore}
              color="primary"
              variant="contained"
            >
              Grade
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }

  return gradeTabContent;
}

export default GradeTab;
