import { Box, Button } from "@mui/material";
import React, { useEffect, useState } from "react";
import useAuth from "../../hooks/useAuth";
import { useRestartPresentationMutation } from "./presentationApiSlice";
import { enqueueSnackbar } from "notistack";
import { useGetGroupOfStudentIdQuery } from "../groups/groupsApiSlice";

const Presentation = ({
  presentation,
  index,
  handleClickOpenGradeGroupDialog,
  handleGetPresentations,
  isTrainer,
  group,
  diffEndTimeToNow,
  handleCloseBackDrop,
  handleOpenBackDrop,
}) => {
  const { userLogin, isAdmin } = useAuth();

  // const [] = useGetGroupOfStudentIdQuery();

  const isGraded = presentation.grade.some(
    (grade) => grade.votedBy === userLogin._id
  );

  // Function to calculate the total score for a single grade object
  const calculateTotalScore = (gradeObj) => {
    const { keepTime, meetRequirements, presentations, goodInformation } =
      gradeObj;
    return (keepTime + meetRequirements + presentations + goodInformation) / 5;
  };

  // Calculate the total scores for all grade objects and store them in an array
  const totalScores = presentation.grade.map((gradeObj) =>
    calculateTotalScore(gradeObj)
  );

  // Function to calculate the average of an array of numbers
  const calculateAverage = (arr) => {
    const sum = arr.reduce((acc, val) => acc + val, 0);
    return sum / arr.length || 0;
  };

  // Calculate the overall average of all the total scores
  const averageTotalScore = calculateAverage(totalScores);

  const [
    restartPresentation,
    {
      isLoading: restartPresentationIsSuccess,
      isError: restartPresentationIsError,
      isLoading: restartPresentationIsLoading,
      error: restartPresentationError,
    },
  ] = useRestartPresentationMutation();

  const handleRestart = async () => {
    try {
      await restartPresentation({ presentation: presentation._id });
    } catch (error) {
      enqueueSnackbar("error with restart function : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (restartPresentationError) {
      handleCloseBackDrop();
      enqueueSnackbar(
        "Restart error: " + restartPresentationError?.data?.message,
        { variant: "error" }
      );
    }
    if (restartPresentationIsLoading) {
      handleOpenBackDrop();
      // Handle loading state if needed
    }
    if (restartPresentationIsSuccess) {
      // Adding a delay of 2 seconds before running the success condition
      const delay = 2000; // 2000 milliseconds = 2 seconds

      setTimeout(() => {
        handleCloseBackDrop();
        handleGetPresentations();
      }, delay);
    }
  }, [
    restartPresentationError,
    restartPresentationIsLoading,
    restartPresentationIsSuccess,
  ]);

  return (
    <>
      <ul
        style={{ listStyle: "none", padding: "0", width: "100%" }}
        key={presentation?._id}
      >
        <li
          style={{
            border: "1px solid #ccc",
            marginTop: "24px",
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            borderRadius: "4px",
          }}
        >
          <ul
            style={{
              listStyle: "none",
              padding: "15px",
              width: "100%",
            }}
          >
            <li
              style={{
                borderBottom: "1px solid #ccc",
                paddingBottom: "15px",
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                borderRadius: "4px",
              }}
            >
              <span title="Round 1" value="Round 1">
                {`Round ${index + 1}`}
              </span>
              <Box>
                {(isAdmin || isTrainer) && diffEndTimeToNow > 0 ? (
                  <>
                    <Button variant="contained" onClick={handleRestart}>
                      Restart
                    </Button>
                  </>
                ) : null}
              </Box>
            </li>
            <li
              style={{
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                borderRadius: "4px",
                marginTop: "16px",
              }}
            >
              <Box sx={{ width: "40%" }}>
                {/* Make tooltip: Teachers and the other groups can grade on the preseting group. If you are a member of the presenting group, you can not do the grading in this round */}
                <span class="head-line">Presenting: </span>
                <b>{`Group ${presentation.presentationGroup.groupNumber}`}</b>
              </Box>
              <Box sx={{ width: "30%" }}>
                {!isGraded ? (
                  <span
                    title="Not Graded"
                    class="grade-label fs-14 not-graded"
                    value="Not Graded"
                    style={{
                      color: "#eb5757",
                      fontSize: "0.875rem",
                    }}
                  >
                    Not Graded
                  </span>
                ) : (
                  <span
                    title="Graded"
                    class="grade-label fs-14 graded"
                    value="Graded"
                    style={{
                      color: "#38cb89",
                      fontSize: "0.875rem",
                    }}
                  >
                    Graded
                  </span>
                )}
              </Box>
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {averageTotalScore}/4.0
              </Box>
              <Box sx={{ textAlign: "right", width: "30%" }}>
                {((group && presentation.reviewGroup._id === group._id) ||
                  isTrainer ||
                  isAdmin) &&
                diffEndTimeToNow > 0 ? (
                  <Button
                    variant="contained"
                    color="success"
                    onClick={() =>
                      handleClickOpenGradeGroupDialog(presentation)
                    }
                  >
                    Grade
                  </Button>
                ) : null}
              </Box>
            </li>
            <li
              style={{
                width: "100%",
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                borderRadius: "4px",
                marginTop: "16px",
              }}
            >
              <Box sx={{ width: "40%" }}>
                {/* Make tooltip: Teachers and the other groups can grade on the preseting group. If you are a member of the presenting group, you can not do the grading in this round */}
                <span cl ass="head-line">
                  Reviewing:{" "}
                </span>
                <b>{`Group ${presentation.reviewGroup.groupNumber}`}</b>
              </Box>
              <Box sx={{ textAlign: "right", width: "30%" }}></Box>
            </li>
          </ul>
        </li>
      </ul>
    </>
  );
};

export default Presentation;
