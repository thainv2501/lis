import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

// Tạo adapter cho danh sách Lesson
const commentAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = commentAdapter.getInitialState();

// Tạo slice API cho Lesson
export const commentApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getAllComments: builder.mutation({
      query: (initialData) => ({
        url: "/comments/all",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
    getCommentsWithFilter: builder.mutation({
      query: (initialData) => ({
        url: "/comments/filter",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    createComment: builder.mutation({
      query: (initialData) => ({
        url: "/comments",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    updateComment: builder.mutation({
      query: (initialData) => ({
        url: "/comments",
        method: "PATCH",
        body: {
          ...initialData,
        },
      }),
    }),

    deleteComment: builder.mutation({
      query: (initialData) => ({
        url: `/comments`,
        method: "DELETE",
        body: { ...initialData },
      }),
    }),

    getCommentWithId: builder.query({
      query: (id) => ({
        url: "/comments/" + id,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const {
  useCreateCommentMutation,
  useDeleteCommentMutation,
  useGetCommentWithIdQuery,
  useGetAllCommentsMutation,
  useGetCommentsWithFilterMutation,
  useUpdateCommentMutation,
} = commentApiSlice;
