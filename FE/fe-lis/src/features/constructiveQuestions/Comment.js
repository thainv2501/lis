import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import EditIcon from "@mui/icons-material/Edit";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import StarIcon from "@mui/icons-material/Star";
import {
  Avatar,
  Box,
  Button,
  Grid,
  IconButton,
  TextField,
} from "@mui/material";
import { green } from "@mui/material/colors";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { enqueueSnackbar } from "notistack";
import { React, useEffect, useRef, useState } from "react";
import useAuth from "../../hooks/useAuth";
import { useUpdateCommentMutation } from "./commentApiSlice";
import { useAddNewReplyMutation } from "../reply/replyApiSlice";
import Reply from "./Reply";
import { useVoteMutation } from "./voteApiSlice";
import { useLocation } from "react-router-dom";

// Load the relative time plugin
dayjs.extend(relativeTime);

const Comment = ({
  comment,
  diffEndTimeToNow,
  handleGetCommentsWithFilter,
  handleOpenDeleteCommentDialog,
  handleCloseBackdrop,
  handleOpenBackdrop,
  handleOpenDeleteReplyDialog,
  setSelectedReply,
  setSelectedComment,
  voteSetting,
  remainingVotes,
  filterType,
  schedule,
  handleGetRemainVotesWithFilter,
}) => {
  const location = useLocation();
  // Get the query parameters from the URL
  const queryParams = new URLSearchParams(location.search);

  // Get a specific parameter value
  const slotValue = queryParams.get("slot");
  // Get a specific parameter value
  const CQId = queryParams.get("cqid");
  // Get a specific parameter value
  const scheduleId = queryParams.get("scheduleId");
  // Group accordion
  const [expandedVote, setExpandedVote] = useState([]);
  const { userLogin, isAdmin } = useAuth();

  const commentRef = useRef();
  const replyRef = useRef();

  const votedByMe = comment.votes.some(
    (vote) => vote.votedBy === userLogin._id
  );

  const redVoted = comment.votes
    .filter((vote) => vote.card === "red")
    .some((vote) => vote.votedBy === userLogin._id);
  const greenVoted = comment.votes
    .filter((vote) => vote.card === "green")
    .some((vote) => vote.votedBy === userLogin._id);
  const blueVoted = comment.votes
    .filter((vote) => vote.card === "blue")
    .some((vote) => vote.votedBy === userLogin._id);
  const blackVoted = comment.votes
    .filter((vote) => vote.card === "black")
    .some((vote) => vote.votedBy === userLogin._id);

  const [
    vote,
    {
      isSuccess: voteIsSuccess,
      isLoading: voteIsLoading,
      isError: voteIsError,
      error: voteError,
    },
  ] = useVoteMutation();

  const handleVote = async ({ card, star }) => {
    try {
      await vote({
        scheduleId,
        CQId,
        commentId: comment._id,
        type: filterType,
        card,
        star,
        votedBy: userLogin._id,
        isTrainer: userLogin._id === schedule.trainer._id,
      });
    } catch (error) {
      enqueueSnackbar("Create Function error : " + error, { variant: "error" });
    }
  };

  useEffect(() => {
    if (voteIsSuccess) {
      handleGetRemainVotesWithFilter();
      handleGetCommentsWithFilter();
      handleCloseBackdrop();
      handleToggleVote(`vote${comment._id}`);
    }
    if (voteIsLoading) {
      handleOpenBackdrop();
    }
    if (voteIsError) {
      enqueueSnackbar("Voted error : " + voteError?.data?.message, {
        variant: "error",
      });
      handleCloseBackdrop();
    }
  }, [voteIsSuccess, voteIsError, voteIsLoading]);

  // Update the commentRef whenever the comment prop changes
  useEffect(() => {
    if (commentRef.current && comment) {
      commentRef.current.value = comment.content;
    }
  }, [comment]);

  const handleToggleVote = (panel) => {
    if (expandedVote.includes(panel)) {
      setExpandedVote((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    } else {
      setExpandedVote((prevExpanded) => [...prevExpanded, panel]);
    }
  };
  // Group accordion
  const [expandedReply, setExpandedReply] = useState([]);

  const handleToggleReply = (panel) => {
    if (expandedReply.includes(panel)) {
      setExpandedReply((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    } else {
      setExpandedReply((prevExpanded) => [...prevExpanded, panel]);
    }
  };
  const [expandedActions, setExpandedActions] = useState([]);

  const handleToggleActions = (panel) => {
    if (expandedActions.includes(panel)) {
      setExpandedActions((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    } else {
      setExpandedActions((prevExpanded) => [...prevExpanded, panel]);
    }
  };
  const [expandedEdit, setExpandedEdit] = useState([]);

  const handleToggleEdit = (panel) => {
    if (expandedEdit.includes(panel)) {
      setExpandedEdit((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    } else {
      setExpandedEdit((prevExpanded) => [...prevExpanded, panel]);
    }
  };

  const [
    updateComment,
    {
      isSuccess: updateCommentIsSuccess,
      isLoading: updateCommentIsLoading,
      isError: updateCommentIsError,
      error: updateCommentError,
    },
  ] = useUpdateCommentMutation();

  const handleUpdateComment = async () => {
    if (commentRef.current.value.trim() !== "") {
      try {
        await updateComment({
          commentId: comment._id,
          content: commentRef.current.value.trim(),
        });
      } catch (error) {
        enqueueSnackbar("Update comment error : " + error, {
          variant: "error",
        });
      }
    } else {
      commentRef.current.value = comment.content;
    }
  };

  useEffect(() => {
    if (updateCommentIsSuccess) {
      handleGetCommentsWithFilter();
      handleCloseBackdrop();
    }
    if (updateCommentIsError) {
      handleCloseBackdrop();
      enqueueSnackbar(
        "Edit comment error : " + updateCommentError?.data?.message,
        { variant: "error" }
      );
    }
    if (updateCommentIsLoading) {
      handleOpenBackdrop();
    }
  }, [updateCommentIsSuccess, updateCommentIsError, updateCommentIsLoading]);

  const [
    addNewReply,
    {
      isSuccess: addNewReplyIsSuccess,
      isLoading: addNewReplyIsLoading,
      isError: addNewReplyIsError,
      error: addNewReplyError,
    },
  ] = useAddNewReplyMutation();

  const handleReply = async () => {
    if (replyRef.current.value.trim() !== "") {
      try {
        await addNewReply({
          id: comment._id,
          content: replyRef.current.value.trim(),
          createdBy: userLogin._id,
          isComment: true,
        });
      } catch (error) {
        enqueueSnackbar("Error While add new reply : " + error, {
          variant: "error",
        });
      }
    }
  };

  useEffect(() => {
    if (addNewReplyIsSuccess) {
      handleGetCommentsWithFilter();
      handleCloseBackdrop();
    }
    if (addNewReplyIsLoading) {
      handleOpenBackdrop();
    }
    if (addNewReplyIsError) {
      enqueueSnackbar("Error reply : " + addNewReplyError?.data?.message, {
        variant: "error",
      });
      handleCloseBackdrop();
    }
  }, [addNewReplyIsSuccess, addNewReplyIsError, addNewReplyIsLoading]);

  return (
    <>
      <Box sx={{ marginTop: "25px" }} key={comment._id}>
        <Box sx={{ display: "flex" }}>
          <Box
            sx={{
              marginTop: "0",
              marginRight: "10px",
              display: "block",
            }}
          >
            <Avatar
              alt="Img"
              sx={{
                width: "36px",
                height: "36px",
                border: "1px solid #dddddd",
                borderRadius: "50%",
                objectFit: "cover",
                overflow: "hidden",
                transition: "0.3 all",
                backgroundColor: green[500],
              }}
            >
              {comment.createdBy.email.charAt(0).toUpperCase()}
            </Avatar>
          </Box>
          <Box sx={{ flexGrow: "1", maxWidth: "calc(100% - 40px)" }}>
            <Grid container justifyContent={"space-between"}>
              <Grid item>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    position: "relative",
                    marginBottom: "8px",
                  }}
                >
                  <Box className="rep-writer">
                    <span
                      style={{
                        fontSize: "12px",
                        color: "#333",
                        fontStyle: "italic",
                      }}
                    >
                      {comment.createdBy.email}
                    </span>
                  </Box>
                  <Box className="comment-writer">
                    <span
                      style={{
                        padding: "0",
                        color: "#333",
                        fontWeight: "600",
                        textDecoration: "none",
                      }}
                      className="user-name"
                    >
                      {comment.createdBy?.fullName}
                    </span>
                  </Box>
                  <Box
                    style={{ lineHeight: "1" }}
                    className="comment-user-info"
                  >
                    <time
                      style={{
                        fontSize: "12px",
                        color: "#333",
                        fontStyle: "italic",
                      }}
                    >
                      {dayjs(comment.createdAt).fromNow()}
                    </time>
                  </Box>
                </Box>
              </Grid>
              <Grid item>
                {userLogin._id === comment.createdBy._id &&
                  diffEndTimeToNow > 0 &&
                  !expandedActions.includes(`3dot-comment${comment._id}`) && (
                    <>
                      <IconButton
                        onClick={() =>
                          handleToggleActions(`3dot-comment${comment._id}`)
                        }
                      >
                        <MoreHorizIcon />
                      </IconButton>
                    </>
                  )}
                {expandedActions.includes(`3dot-comment${comment._id}`) && (
                  <>
                    <IconButton
                      onClick={() => {
                        handleToggleActions(`3dot-comment${comment._id}`);
                        handleToggleEdit(`edit-comment${comment._id}`);
                      }}
                    >
                      <EditIcon fontSize="small" color="success" />
                    </IconButton>
                    <IconButton
                      onClick={() => {
                        handleOpenDeleteCommentDialog();
                        setSelectedComment(comment);
                      }}
                    >
                      <DeleteForeverIcon fontSize="small" color="error" />
                    </IconButton>
                  </>
                )}
              </Grid>
            </Grid>

            <Box>
              {/* Content */}
              <Box
                sx={{
                  width: "100%",
                  display: "inline-block",
                  padding: "15px 8px 15px 15px",
                  background: "#f2f5f7",
                  borderRadius: "4px",
                  color: "#373a3c",
                  fontSize: "16px",
                }}
              >
                <Box
                  sx={{
                    maxHeight: "350px",
                    position: "relative",
                    width: "100%",
                    display: "inline-block",
                    overflowX: "auto",
                  }}
                >
                  <Box
                    sx={{
                      overflowWrap: "break-word",
                      overflow: "auto",
                    }}
                  >
                    <TextField
                      title="undefined"
                      fullWidth
                      variant="standard"
                      multiline
                      name={`comment${comment._id}`}
                      disabled={
                        !expandedEdit.includes(`edit-comment${comment._id}`)
                      }
                      rows={5}
                      inputRef={commentRef}
                      defaultValue={comment.content}
                    />
                  </Box>
                </Box>
              </Box>

              {/* Actions */}
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                  position: "relative",
                }}
              >
                {expandedEdit.includes(`edit-comment${comment._id}`) && (
                  <Button
                    onClick={() => {
                      handleToggleEdit(`edit-comment${comment._id}`);
                      handleUpdateComment();
                    }}
                  >
                    Save
                  </Button>
                )}
                {userLogin?._id !== comment.createdBy._id &&
                  diffEndTimeToNow > 0 && (
                    <>
                      <Box
                        sx={{ display: "flex", alignItems: "center" }}
                        className="cmt-actions"
                      >
                        <Button
                          onClick={() =>
                            handleToggleReply(`reply${comment._id}`)
                          }
                        >
                          Reply
                        </Button>
                        <Button
                          color={votedByMe ? "success" : "primary"}
                          onClick={() => handleToggleVote(`vote${comment._id}`)}
                        >
                          Vote
                        </Button>
                      </Box>
                      {expandedVote.includes(`vote${comment._id}`) && (
                        <span
                          style={{
                            fontSize: "18px",
                            borderRadius: "20px",
                            background: "#fff",
                            boxShadow: "0 4px 16px #0000001a",
                            padding: "0",
                            height: "60px",
                            position: "absolute",
                            left: "150px",
                            top: "-10px",
                            display: "inline-flex",
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer",
                            fontWeight: "400",
                          }}
                        >
                          <Button
                            style={{
                              backgroundColor: redVoted ? "#cccccc" : "",
                              position: "relative",
                              margin: "3px 10px",
                              display: "flex",
                              alignItems: "center",
                            }}
                            disabled={remainingVotes.redCard === 0 && !redVoted}
                            onClick={() =>
                              handleVote({
                                card: "red",
                                star: voteSetting.redCard.star,
                              })
                            }
                          >
                            <StarIcon
                              sx={{
                                color: "red",
                                marginRight: "8px",
                              }}
                              fontSize="large"
                            />
                            <span
                              style={{
                                position: "absolute",
                                top: "8px",
                                left: "21px",
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                                color: "white",
                              }}
                              className="star-value"
                            >
                              {voteSetting.redCard.star}
                            </span>
                            <span
                              style={{
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                              }}
                              className="vote-count"
                            >
                              {remainingVotes.redCard}
                            </span>
                          </Button>
                          <Button
                            style={{
                              backgroundColor: blueVoted ? "#cccccc" : "",
                              position: "relative",
                              margin: "3px 10px",
                              display: "flex",
                              alignItems: "center",
                            }}
                            disabled={
                              remainingVotes.blueCard === 0 && !blueVoted
                            }
                            onClick={() =>
                              handleVote({
                                card: "blue",
                                star: voteSetting.blueCard.star,
                              })
                            }
                          >
                            <StarIcon
                              sx={{
                                color: "blue",
                                marginRight: "8px",
                              }}
                              fontSize="large"
                            />
                            <span
                              style={{
                                position: "absolute",
                                top: "8px",
                                left: "21px",
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                                color: "white",
                              }}
                              className="star-value"
                            >
                              {voteSetting.blueCard.star}
                            </span>
                            <span
                              style={{
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                              }}
                              className="vote-count"
                            >
                              {remainingVotes.blueCard}
                            </span>
                          </Button>
                          <Button
                            style={{
                              backgroundColor: greenVoted ? "#cccccc" : "",
                              position: "relative",
                              margin: "3px 10px",
                              display: "flex",
                              alignItems: "center",
                            }}
                            disabled={
                              remainingVotes.greenCard === 0 && !greenVoted
                            }
                            onClick={() =>
                              handleVote({
                                card: "green",
                                star: voteSetting.greenCard.star,
                              })
                            }
                          >
                            <StarIcon
                              sx={{
                                color: "green",
                                marginRight: "8px",
                              }}
                              fontSize="large"
                            />
                            <span
                              style={{
                                position: "absolute",
                                top: "8px",
                                left: "21px",
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                                color: "white",
                              }}
                              className="star-value"
                            >
                              {voteSetting.greenCard.star}
                            </span>
                            <span
                              style={{
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                              }}
                              className="vote-count"
                            >
                              {remainingVotes.greenCard}
                            </span>
                          </Button>
                          <Button
                            style={{
                              backgroundColor: blackVoted ? "#cccccc" : "",
                              position: "relative",
                              margin: "3px 10px",
                              display: "flex",
                              alignItems: "center",
                            }}
                            disabled={
                              remainingVotes.blackCard === 0 && !blackVoted
                            }
                            onClick={() =>
                              handleVote({
                                card: "black",
                                star: voteSetting.blackCard.star,
                              })
                            }
                          >
                            <StarIcon
                              sx={{
                                color: "black",
                                marginRight: "8px",
                              }}
                              fontSize="large"
                            />
                            <span
                              style={{
                                position: "absolute",
                                top: "8px",
                                left: "21px",
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                                color: "white",
                              }}
                              className="star-value"
                            >
                              {voteSetting.blackCard.star}
                            </span>
                            <span
                              style={{
                                fontFamily: "sans-serif",
                                fontWeight: "500",
                                lineHeight: "30px",
                                display: "flex",
                                alignItems: "center",
                              }}
                              className="vote-count"
                            >
                              {remainingVotes.blackCard}
                            </span>
                          </Button>
                        </span>
                      )}
                    </>
                  )}

                <span
                  style={{
                    fontSize: "18px",
                    borderRadius: "20px",
                    background: "#fff",
                    boxShadow: "0 4px 16px #0000001a",
                    padding: "0",
                    height: "30px",
                    position: "absolute",
                    right: "18px",
                    top: "-21px",
                    display: "inline-flex",
                    justifyContent: "center",
                    alignItems: "center",
                    cursor: "pointer",
                    fontWeight: "400",
                  }}
                >
                  <span
                    style={{
                      position: "relative",
                      margin: "3px 10px",
                      display: "flex",
                      alignItems: "center",
                    }}
                  >
                    <StarIcon
                      sx={{
                        color: "#ffdc72",
                        fontSize: "23px",
                        marginRight: "8px",
                      }}
                    />
                    <span
                      style={{
                        fontFamily: "sans-serif",
                        fontWeight: "500",
                        lineHeight: "30px",
                        display: "flex",
                        alignItems: "center",
                      }}
                      className="vote-count"
                    >
                      {comment.votes.reduce((acc, vote) => acc + vote.star, 0)}
                    </span>
                  </span>
                </span>
              </Box>
              {/* Reply Box */}
              {diffEndTimeToNow > 0 &&
                expandedReply.includes(`reply${comment._id}`) && (
                  <Box
                    sx={{
                      width: "100%",
                      display: "inline-block",
                      padding: "15px 8px 15px 15px",
                      background: "#f2f5f7",
                      borderRadius: "4px",
                      color: "#373a3c",
                      fontSize: "16px",
                    }}
                  >
                    <Box
                      sx={{
                        maxHeight: "350px",
                        position: "relative",
                        width: "100%",
                        display: "inline-block",
                        overflowX: "auto",
                      }}
                    >
                      <Box
                        sx={{
                          overflowWrap: "break-word",
                          overflow: "auto",
                        }}
                      >
                        <TextField
                          title="undefined"
                          fullWidth
                          variant="standard"
                          multiline
                          rows={5}
                          label={`Reply ${comment.createdBy.fullName}`}
                          inputRef={replyRef}
                        />
                      </Box>
                    </Box>
                    <Grid container justifyContent={"flex-end"}>
                      <Button
                        color="error"
                        onClick={() => handleToggleReply(`reply${comment._id}`)}
                      >
                        Cancel
                      </Button>
                      <Button onClick={handleReply}>Post</Button>
                    </Grid>
                  </Box>
                )}
              {/* Replies*/}
              <Box
                sx={{
                  width: "100%",
                  display: "inline-block",
                  padding: "15px 8px 15px 15px",
                  borderRadius: "4px",
                  color: "#373a3c",
                  fontSize: "16px",
                }}
              >
                {comment.reply.map((rep) => (
                  <Reply
                    rep={rep}
                    diffEndTimeToNow={diffEndTimeToNow}
                    handleOpenDeleteReplyDialog={handleOpenDeleteReplyDialog}
                    setSelectedReply={setSelectedReply}
                  />
                ))}
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Comment;
