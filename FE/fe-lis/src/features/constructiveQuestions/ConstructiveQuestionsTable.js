import {
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
} from "@mui/material";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import ConstructiveQuestionDialog from "./ConstructiveQuestionDialog";
import FindInPageIcon from "@mui/icons-material/FindInPage";
import { useDeleteCQMutation } from "./constructiveQuestionApiSlice";

const ConstructiveQuestionsTable = ({
  scheduleId,
  lessonId,
  addNewCQ,
  updateCQ,
  lessons,
  updateCQIsSuccess,
  constructiveQuestions,
  updateLessonIsSuccess,
  deleteCQInLessonByCQId,
  deleteCQInLessonByCQIdIsSuccess,
  updateScheduleIsSuccess,
  lessonObj,
  refetch,
}) => {
  const [selectedCQ, setSelectedCQ] = useState(null);
  const { enqueueSnackbar } = useSnackbar();

  const [openConstructiveQuestionDialog, setOpenConstructiveQuestionDialog] =
    useState(false);

  const handleCloseConstructiveQuestionDialog = () => {
    setOpenConstructiveQuestionDialog(false);
  };
  const handleOpenConstructiveQuestionDialog = () => {
    setOpenConstructiveQuestionDialog(true);
  };
  const handleViewConstructiveQuestions = (data) => {
    setSelectedCQ(data);
    handleOpenConstructiveQuestionDialog();
  };
  const handleAddNewConstructiveQuestion = () => {
    setSelectedCQ(null);
    handleOpenConstructiveQuestionDialog();
  };
  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);

  const [
    deleteCQ,
    {
      isSuccess: deleteCQIsSuccess,
      isError: deleteCQIsError,
      error: deleteCQError,
    },
  ] = useDeleteCQMutation();

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false);
  };
  const handleOpenDeleteDialog = (CQ) => {
    setSelectedCQ(CQ);
    setOpenDeleteDialog(true);
  };
  const handleDeleteCQ = async () => {
    try {
      if (lessonId) {
        await deleteCQInLessonByCQId({
          lessonId: lessonObj?._id,
          CQId: selectedCQ?._id,
        });
      }
      if (scheduleId) {
        await deleteCQ({ id: selectedCQ?._id });
      }
    } catch (error) {
      enqueueSnackbar("Delete Constructive Question Fail !", {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (updateLessonIsSuccess || updateScheduleIsSuccess) {
      handleCloseConstructiveQuestionDialog();
    }
  }, [updateLessonIsSuccess, updateScheduleIsSuccess]);
  useEffect(() => {
    if (updateCQIsSuccess) {
      handleCloseConstructiveQuestionDialog();
    }
  }, [updateCQIsSuccess]);
  useEffect(() => {
    if (deleteCQInLessonByCQIdIsSuccess) {
      handleCloseDeleteDialog();
    }
  }, [deleteCQInLessonByCQIdIsSuccess]);

  useEffect(() => {
    if (deleteCQIsSuccess) {
      enqueueSnackbar("Delete CQ is successful", { variant: "success" });
      handleCloseDeleteDialog();
      refetch();
    }
  }, [deleteCQIsSuccess]);
  useEffect(() => {
    if (deleteCQIsError) {
      enqueueSnackbar("Delete CQ is error " + deleteCQError?.data?.message, {
        variant: "error",
      });
      handleCloseDeleteDialog();
    }
  }, [deleteCQIsError]);

  return (
    <>
      <Grid container spacing={3}>
        <Grid item container spacing={3}>
          <Grid item container spacing={3} xs={12} justifyContent={"flex-end"}>
            <Grid item>
              {scheduleId || lessonId ? (
                <Button
                  color="success"
                  variant="contained"
                  onClick={handleAddNewConstructiveQuestion}
                >
                  New Question
                </Button>
              ) : null}
            </Grid>
          </Grid>
          {/* heading 1 */}
        </Grid>
        <Grid item container spacing={3}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell sx={{ color: "indianred" }}>No.</TableCell>
                <TableCell sx={{ color: "indianred" }}>Title</TableCell>
                <TableCell sx={{ color: "indianred", width: "40%" }}>
                  Content
                </TableCell>
                <TableCell sx={{ color: "indianred" }}>Author</TableCell>
                <TableCell align="center" sx={{ color: "indianred" }}>
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {constructiveQuestions &&
                constructiveQuestions.map((data, index) => (
                  <TableRow key={data?._id}>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      {index + 1}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {data?.title}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {data?.content}
                    </TableCell>
                    <TableCell>{data?.createdBy?.fullName}</TableCell>
                    <TableCell>
                      <Tooltip title="View Details" placement="top" arrow>
                        <Button
                          color="success"
                          onClick={() => handleViewConstructiveQuestions(data)}
                        >
                          <FindInPageIcon />
                        </Button>
                      </Tooltip>
                      <Button
                        color="error"
                        onClick={() => handleOpenDeleteDialog(data)}
                      >
                        Delete
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}

              {lessons &&
                lessons.map(
                  (lesson) =>
                    lesson?.constructiveQuestions &&
                    lesson?.constructiveQuestions.map((data, index) => (
                      <TableRow key={data?._id}>
                        <TableCell sx={{ wordBreak: "break-word" }}>
                          {index + 1}
                        </TableCell>
                        <TableCell
                          sx={{ wordBreak: "break-word", width: "20%" }}
                        >
                          {data?.title}
                        </TableCell>
                        <TableCell
                          sx={{ wordBreak: "break-word", width: "20%" }}
                        >
                          {data?.content}
                        </TableCell>
                        <TableCell>{data?.createdBy?.fullName}</TableCell>
                        <TableCell>
                          <Button
                            color="error"
                            onClick={() => handleOpenDeleteDialog(data)}
                            disabled
                          >
                            lesson provided
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))
                )}
            </TableBody>
          </Table>
        </Grid>
      </Grid>
      <ConstructiveQuestionDialog
        openConstructiveQuestionDialog={openConstructiveQuestionDialog}
        handleCloseConstructiveQuestionDialog={
          handleCloseConstructiveQuestionDialog
        }
        selectedCQ={selectedCQ}
        addNewCQ={addNewCQ}
        updateCQ={updateCQ}
      />
      <Dialog
        fullWidth
        maxWidth={"sm"}
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
      >
        <DialogTitle color={"red"}>Delete Question</DialogTitle>
        <Divider />
        <DialogContent>Delete question : {selectedCQ?.title}</DialogContent>
        <Divider />

        <DialogActions>
          <Button color="warning" onClick={handleCloseDeleteDialog}>
            Cancel
          </Button>
          <Button color="success" onClick={handleDeleteCQ}>
            Confirm
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ConstructiveQuestionsTable;
