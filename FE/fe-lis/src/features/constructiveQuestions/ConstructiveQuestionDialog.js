import {
  Box,
  Button,
  Checkbox,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Grid,
  IconButton,
  InputLabel,
  Paper,
  TextField,
} from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { React, useEffect, useState } from "react";
import { useSnackbar } from "notistack";
import useAuth from "../../hooks/useAuth";

import CloseIcon from "@mui/icons-material/Close";

const ConstructiveQuestionDialog = ({
  selectedCQ,
  openConstructiveQuestionDialog,
  handleCloseConstructiveQuestionDialog,
  addNewCQ,
  updateCQ,
}) => {
  const defaultCQ = {
    title: "",
    content: "",
    displaySetting: {
      displayStudentsName: true,
      allowOutsideGroupViewComments: true,
    },
    voteSetting: {
      redCard: {
        trainerVote: { star: 4, quantity: 2 },
        insideVote: { star: 4, quantity: 2 },
        outsideVote: { star: 4, quantity: 2 },
      },
      blueCard: {
        trainerVote: { star: 3, quantity: 2 },
        insideVote: { star: 3, quantity: 2 },
        outsideVote: { star: 3, quantity: 2 },
      },
      greenCard: {
        trainerVote: { star: 2, quantity: 2 },
        insideVote: { star: 2, quantity: 2 },
        outsideVote: { star: 2, quantity: 2 },
      },
      blackCard: {
        trainerVote: { star: 1, quantity: 2 },
        insideVote: { star: 1, quantity: 2 },
        outsideVote: { star: 1, quantity: 2 },
      },
    },
  };
  const { userLogin } = useAuth();
  const { enqueueSnackbar } = useSnackbar();
  const [constructiveQuestionObj, setConstructiveQuestionObj] =
    useState(defaultCQ);

  useEffect(() => {
    setConstructiveQuestionObj(selectedCQ || defaultCQ);
  }, [selectedCQ]);

  const handleChangeConstructiveQuestionObj = (key, value) => {
    setConstructiveQuestionObj((prevValues) => {
      const updatedValues = { ...prevValues };
      const keys = key.split(".");
      let nestedValue = updatedValues;
      for (let i = 0; i < keys.length; i++) {
        const currentKey = keys[i];
        if (i === keys.length - 1) {
          nestedValue[currentKey] = value;
        } else {
          nestedValue[currentKey] = { ...nestedValue[currentKey] };
          nestedValue = nestedValue[currentKey];
        }
      }
      return updatedValues;
    });
  };

  const handleConstructiveQuestionStringTypeChange = (event, key) => {
    let value = event.target.value;
    handleChangeConstructiveQuestionObj(key, value);
  };

  const validateAndReturnValue = (value, min, max) => {
    let validValue = min;
    try {
      if (value === "") {
        validValue = min;
      } else {
        validValue = parseInt(value);
        if (validValue > max) validValue = max;
      }
    } catch (error) {
      validValue = min;
    }
    return validValue;
  };

  const handleIntegerTypeChange = (event, key, min, max) => {
    let value = validateAndReturnValue(event.target.value, min, max);
    handleChangeConstructiveQuestionObj(key, value);
  };

  const handleCheckTypeChange = (event, key) => {
    handleChangeConstructiveQuestionObj(key, event.target.checked);
  };

  const handleUpdateCQ = async () => {
    setConstructiveQuestionObj({
      ...constructiveQuestionObj,
      title: constructiveQuestionObj.title.trim(),
      content: constructiveQuestionObj.content.trim(),
    });
    try {
      await updateCQ({
        constructiveQuestionId: constructiveQuestionObj?._id,
        updates: {
          ...constructiveQuestionObj,
          title: constructiveQuestionObj.title.trim(),
          content: constructiveQuestionObj.content.trim(),
        },
      });
    } catch (error) {
      enqueueSnackbar("Error when update CQ" + error, { variant: "error" });
    }
  };

  const canCreate =
    constructiveQuestionObj?.content?.trim() !== "" &&
    constructiveQuestionObj?.title.trim() !== "";

  const handleCreateNewConstructiveQuestion = async () => {
    try {
      await addNewCQ({
        title: constructiveQuestionObj?.title,
        content: constructiveQuestionObj?.content,
        createdBy: userLogin?._id,
        displaySetting: constructiveQuestionObj?.displaySetting,
        voteSetting: constructiveQuestionObj?.voteSetting,
      });
      setConstructiveQuestionObj(defaultCQ);
    } catch (error) {
      enqueueSnackbar("Error when create new CQ ", { variant: "success" });
    }
  };

  return (
    <>
      <Dialog
        open={openConstructiveQuestionDialog}
        onClose={handleCloseConstructiveQuestionDialog}
        aria-describedby="mentors-dialog-slide-description"
        maxWidth="md"
      >
        <DialogTitle sx={{ padding: "0" }}>
          <Paper
            sx={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              paddingRight: "10px",
              paddingLeft: "10px",
              alignItems: "center",
            }}
          >
            <Box>Constructive Question</Box>
            <Box>
              <IconButton>
                <CloseIcon
                  fontSize="inherit"
                  onClick={handleCloseConstructiveQuestionDialog}
                />
              </IconButton>
            </Box>
          </Paper>
        </DialogTitle>
        <DialogContent sx={{ width: "100%" }}>
          <TextField
            autoFocus
            margin="dense"
            label="Title"
            type="text"
            value={constructiveQuestionObj?.title}
            fullWidth
            required
            variant="outlined"
            onChange={(event) =>
              handleConstructiveQuestionStringTypeChange(event, "title")
            }
          />

          <TextField
            margin="dense"
            label="Content"
            type="text"
            fullWidth
            required
            multiline
            rows={5}
            variant="outlined"
            value={constructiveQuestionObj?.content}
            onChange={(event) =>
              handleConstructiveQuestionStringTypeChange(event, "content")
            }
          />

          <Grid sx={{ display: "flex", flexDirection: "column" }}>
            <InputLabel sx={{ marginTop: "5px" }}>Display settings</InputLabel>
            <FormControlLabel
              control={
                <Checkbox
                  name="showStudentName"
                  checked={
                    constructiveQuestionObj?.displaySetting?.displayStudentsName
                  }
                  onChange={(event) =>
                    handleCheckTypeChange(
                      event,
                      "displaySetting.displayStudentsName"
                    )
                  }
                />
              }
              label="Displays student's name in comments"
            />

            <FormControlLabel
              control={
                <Checkbox
                  name="allowOutsideGroup"
                  checked={
                    constructiveQuestionObj?.displaySetting
                      ?.allowOutsideGroupViewComments
                  }
                  onChange={(event) =>
                    handleCheckTypeChange(
                      event,
                      "displaySetting.allowOutsideGroupViewComments"
                    )
                  }
                />
              }
              label="Allows outside group to view comments"
            />
          </Grid>

          <InputLabel sx={{ marginTop: "5px" }}>Vote settings</InputLabel>
          <TableContainer
            sx={{
              border: "1px solid rgba(224, 224, 224, 1)",
              maxHeight: 440,
            }}
          >
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell align="center" colSpan={2}>
                    Inside
                  </TableCell>
                  <TableCell align="center" colSpan={2}>
                    Outside
                  </TableCell>
                  <TableCell align="center" colSpan={2}>
                    Trainer
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell align="center" sx={{ width: "100px" }}>
                    Star
                  </TableCell>
                  <TableCell align="center" sx={{ width: "100px" }}>
                    Quantity
                  </TableCell>
                  <TableCell align="center" sx={{ width: "100px" }}>
                    Star
                  </TableCell>
                  <TableCell align="center" sx={{ width: "100px" }}>
                    Quantity
                  </TableCell>
                  <TableCell align="center" sx={{ width: "100px" }}>
                    Star
                  </TableCell>
                  <TableCell align="center" sx={{ width: "100px" }}>
                    Quantity
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow hover role="checkbox" tabIndex={-1}>
                  <TableCell>Red Card (Highest)</TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-inside"
                      name="star-inside"
                      min="1"
                      max="5"
                      value={
                        constructiveQuestionObj?.voteSetting?.redCard
                          ?.insideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.redCard.insideVote.star",
                          1,
                          5
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-inside"
                      name="quantity-inside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.redCard
                          ?.insideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.redCard.insideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-outside"
                      name="star-outside"
                      min="1"
                      max="5"
                      value={
                        constructiveQuestionObj?.voteSetting?.redCard
                          ?.outsideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.redCard.outsideVote.star",
                          1,
                          5
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-outside"
                      name="quantity-outside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.redCard
                          ?.outsideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.redCard.outsideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-trainer"
                      name="star-trainer"
                      min="1"
                      max="5"
                      value={
                        constructiveQuestionObj?.voteSetting?.redCard
                          ?.trainerVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.redCard.trainerVote.star",
                          1,
                          5
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-trainer"
                      name="quantity-trainer"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.redCard
                          ?.trainerVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.redCard.trainerVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                </TableRow>
                <TableRow hover role="checkbox" tabIndex={-1}>
                  <TableCell>Blue Card</TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-inside"
                      name="star-inside"
                      min="1"
                      max="4"
                      value={
                        constructiveQuestionObj?.voteSetting?.blueCard
                          ?.insideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blueCard.insideVote.star",
                          1,
                          4
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-inside"
                      name="quantity-inside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.blueCard
                          ?.insideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blueCard.insideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-outside"
                      name="star-outside"
                      min="1"
                      max="4"
                      value={
                        constructiveQuestionObj?.voteSetting?.blueCard
                          ?.outsideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blueCard.outsideVote.star",
                          1,
                          4
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-outside"
                      name="quantity-outside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.blueCard
                          ?.outsideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blueCard.outsideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-trainer"
                      name="star-trainer"
                      min="1"
                      max="4"
                      value={
                        constructiveQuestionObj?.voteSetting?.blueCard
                          ?.trainerVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blueCard.trainerVote.star",
                          1,
                          4
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-trainer"
                      name="quantity-trainer"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.blueCard
                          ?.trainerVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blueCard.trainerVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                </TableRow>
                <TableRow hover role="checkbox" tabIndex={-1}>
                  <TableCell>Green Card</TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-inside"
                      name="star-inside"
                      min="1"
                      max="3"
                      value={
                        constructiveQuestionObj?.voteSetting?.greenCard
                          ?.insideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.greenCard.insideVote.star",
                          1,
                          3
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-inside"
                      name="quantity-inside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.greenCard
                          ?.insideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.greenCard.insideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-outside"
                      name="star-outside"
                      min="1"
                      max="3"
                      value={
                        constructiveQuestionObj?.voteSetting?.greenCard
                          ?.outsideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.greenCard.outsideVote.star",
                          1,
                          3
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-outside"
                      name="quantity-outside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.greenCard
                          ?.outsideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.greenCard.outsideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-trainer"
                      name="star-trainer"
                      min="1"
                      max="3"
                      value={
                        constructiveQuestionObj?.voteSetting?.greenCard
                          ?.trainerVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.greenCard.trainerVote.star",
                          1,
                          3
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-trainer"
                      name="quantity-trainer"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.greenCard
                          ?.trainerVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.greenCard.trainerVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                </TableRow>
                <TableRow hover role="checkbox" tabIndex={-1}>
                  <TableCell>Black Card (Lowest)</TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-inside"
                      name="star-inside"
                      min="1"
                      max="2"
                      value={
                        constructiveQuestionObj?.voteSetting?.blackCard
                          ?.insideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blackCard.insideVote.star",
                          1,
                          2
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-inside"
                      name="quantity-inside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.blackCard
                          ?.insideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blackCard.insideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-outside"
                      name="star-outside"
                      min="1"
                      max="2"
                      value={
                        constructiveQuestionObj?.voteSetting?.blackCard
                          ?.outsideVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blackCard.outsideVote.star",
                          1,
                          2
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-outside"
                      name="quantity-outside"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.blackCard
                          ?.outsideVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blackCard.outsideVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="star-trainer"
                      name="star-trainer"
                      min="1"
                      max="2"
                      value={
                        constructiveQuestionObj?.voteSetting?.blackCard
                          ?.trainerVote?.star
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blackCard.trainerVote.star",
                          1,
                          2
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                  <TableCell align="center">
                    <input
                      type="number"
                      id="quantity-trainer"
                      name="quantity-trainer"
                      min="1"
                      max="10"
                      value={
                        constructiveQuestionObj?.voteSetting?.blackCard
                          ?.trainerVote?.quantity
                      }
                      onChange={(event) =>
                        handleIntegerTypeChange(
                          event,
                          "voteSetting.blackCard.trainerVote.quantity",
                          1,
                          10
                        )
                      }
                      style={{
                        border: "1px solid rgba(224, 224, 224, 1)",
                        width: "35px",
                        height: "25px",
                      }}
                    />
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Grid sx={{ display: "flex", flexDirection: "column" }}>
            <InputLabel sx={{ marginTop: "5px" }}>Pass criteria</InputLabel>
            <FormControlLabel
              control={<Checkbox name="viewQuestion" />}
              label="View Question"
            />
            <FormControlLabel
              control={<Checkbox name="noOfCommentsPosted" />}
              label="No. of comments posted"
            />
            <FormControlLabel
              control={<Checkbox name="noOfStarsRatedByOthers" />}
              label="No. of stars rated by others"
            />
            <FormControlLabel
              control={<Checkbox name="noOfVotes" />}
              label="No. of votes"
            />
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            color="primary"
            onClick={() =>
              setConstructiveQuestionObj(selectedCQ ? selectedCQ : defaultCQ)
            }
          >
            Reset
          </Button>
          <Button onClick={handleCloseConstructiveQuestionDialog} color="error">
            Close
          </Button>
          {selectedCQ ? (
            <>
              <Button
                color="success"
                onClick={handleUpdateCQ}
                disabled={!canCreate}
              >
                Save
              </Button>
            </>
          ) : (
            <Button
              color="primary"
              onClick={handleCreateNewConstructiveQuestion}
              disabled={!canCreate}
            >
              Create
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  );
};

export default ConstructiveQuestionDialog;
