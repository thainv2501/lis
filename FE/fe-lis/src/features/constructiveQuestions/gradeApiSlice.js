import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

// Tạo adapter cho danh sách Lesson
const gradeAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = gradeAdapter.getInitialState();

// Tạo slice API cho Lesson
export const gradeApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getGrade: builder.mutation({
      query: (initialData) => ({
        url: "/grade",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    createGrade: builder.mutation({
      query: (initialData) => ({
        url: "/grade/create",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const { useCreateGradeMutation, useGetGradeMutation } = gradeApiSlice;
