import { Avatar, Button, Grid, IconButton, TextField } from "@mui/material";
import { green } from "@mui/material/colors";
import { Box } from "@mui/system";
import React, { useEffect, useRef, useState } from "react";
import useAuth from "../../hooks/useAuth";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import EditIcon from "@mui/icons-material/Edit";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import { useUpdateReplyMutation } from "../reply/replyApiSlice";
import { enqueueSnackbar } from "notistack";

// Load the relative time plugin
dayjs.extend(relativeTime);

const Reply = ({
  rep,
  diffEndTimeToNow,
  handleOpenDeleteReplyDialog,
  setSelectedReply,
}) => {
  const { userLogin } = useAuth();
  const replyRef = useRef();
  useEffect(() => {
    if (replyRef.current && rep) {
      replyRef.current.value = rep.content;
    }
  }, [rep]);
  const [expandedActions, setExpandedActions] = useState([]);

  const handleToggleActions = (panel) => {
    if (expandedActions.includes(panel)) {
      setExpandedActions((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    } else {
      setExpandedActions((prevExpanded) => [...prevExpanded, panel]);
    }
  };
  const [expandedEdit, setExpandedEdit] = useState([]);

  const handleToggleEdit = (panel) => {
    if (expandedEdit.includes(panel)) {
      setExpandedEdit((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    } else {
      setExpandedEdit((prevExpanded) => [...prevExpanded, panel]);
    }
  };

  const [
    updateReply,
    {
      isSuccess: updateReplyIsSuccess,
      isError: updateReplyIsError,
      isLoading: updateReplyIsLoading,
      error: updateReplyError,
    },
  ] = useUpdateReplyMutation();

  const handleUpdateReply = async () => {
    if (replyRef.current.value.trim() !== "") {
      try {
        await updateReply({
          replyId: rep._id,
          content: replyRef.current.value.trim(),
        });
      } catch (error) {
        enqueueSnackbar("Error with updateFunction : " + error, {
          variant: "error",
        });
      }
    } else {
      replyRef.current.value = rep.content;
    }
  };

  useEffect(() => {
    if (updateReplyIsLoading) {
    }
    if (updateReplyIsSuccess) {
    }
    if (updateReplyIsError) {
      enqueueSnackbar("Update Reply Error : " + updateReplyError?.data?.error, {
        variant: "error",
      });
    }
  }, [updateReplyIsSuccess, updateReplyIsError, updateReplyIsLoading]);

  return (
    <>
      {/* reply 1 */}
      <Box sx={{ marginTop: "25px" }} key={rep._id}>
        <Box sx={{ display: "flex" }}>
          <Box
            sx={{
              marginTop: "0",
              marginRight: "10px",
              display: "block",
            }}
          >
            <Avatar
              alt="Img"
              sx={{
                width: "36px",
                height: "36px",
                border: "1px solid #dddddd",
                borderRadius: "50%",
                objectFit: "cover",
                overflow: "hidden",
                transition: "0.3 all",
                backgroundColor: green[500],
              }}
            >
              {rep.createdBy.email.charAt(0).toUpperCase()}
            </Avatar>
          </Box>
          <Box
            sx={{
              flexGrow: "1",
              maxWidth: "calc(100% - 40px)",
            }}
          >
            <Grid container justifyContent={"space-between"}>
              <Grid item>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    position: "relative",
                    marginBottom: "8px",
                  }}
                >
                  <Box className="rep-writer">
                    <span
                      style={{
                        fontSize: "12px",
                        color: "#333",
                        fontStyle: "italic",
                      }}
                    >
                      {rep.createdBy.email}
                    </span>
                  </Box>
                  <Box className="rep-writer">
                    <span
                      style={{
                        padding: "0",
                        color: "#333",
                        fontWeight: "600",
                        textDecoration: "none",
                      }}
                      className="user-name"
                    >
                      {rep.createdBy.fullName}
                    </span>
                  </Box>
                  <Box style={{ lineHeight: "1" }} className="rep-user-info">
                    <time
                      style={{
                        fontSize: "12px",
                        color: "#333",
                        fontStyle: "italic",
                      }}
                    >
                      {dayjs(rep.createdDate).fromNow()}
                    </time>
                  </Box>
                </Box>
              </Grid>
              <Grid item>
                {userLogin._id === rep.createdBy._id &&
                  diffEndTimeToNow > 0 &&
                  !expandedActions.includes(`3dot-reply${rep._id}`) && (
                    <>
                      <IconButton
                        onClick={() =>
                          handleToggleActions(`3dot-reply${rep._id}`)
                        }
                      >
                        <MoreHorizIcon />
                      </IconButton>
                    </>
                  )}
                {expandedActions.includes(`3dot-reply${rep._id}`) && (
                  <>
                    <IconButton
                      onClick={() => {
                        handleToggleActions(`3dot-reply${rep._id}`);
                        handleToggleEdit(`edit-reply${rep._id}`);
                      }}
                    >
                      <EditIcon fontSize="small" color="success" />
                    </IconButton>
                    <IconButton
                      onClick={() => {
                        handleOpenDeleteReplyDialog();
                        setSelectedReply(rep);
                      }}
                    >
                      <DeleteForeverIcon fontSize="small" color="error" />
                    </IconButton>
                  </>
                )}
              </Grid>
            </Grid>
            <Box>
              {/* Content */}
              <Box
                sx={{
                  width: "100%",
                  display: "inline-block",
                  padding: "15px 8px 15px 15px",
                  background: "#f2f5f7",
                  borderRadius: "4px",
                  color: "#373a3c",
                  fontSize: "16px",
                }}
              >
                <Box
                  sx={{
                    maxHeight: "350px",
                    position: "relative",
                    width: "100%",
                    display: "inline-block",
                    overflowX: "auto",
                  }}
                >
                  <Box
                    sx={{
                      overflowWrap: "break-word",
                      overflow: "auto",
                    }}
                  >
                    <TextField
                      title="undefined"
                      fullWidth
                      variant="standard"
                      multiline
                      rows={5}
                      disabled={!expandedEdit.includes(`edit-reply${rep._id}`)}
                      defaultValue={rep.content}
                      inputRef={replyRef}
                    />
                  </Box>
                </Box>
              </Box>
              {expandedEdit.includes(`edit-reply${rep._id}`) && (
                <Button
                  onClick={() => {
                    handleToggleEdit(`edit-reply${rep._id}`);
                    handleUpdateReply();
                  }}
                >
                  Save
                </Button>
              )}
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Reply;
