import { React, useEffect, useState } from "react";
import { Avatar, Box, CircularProgress, Typography } from "@mui/material";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import { styled } from "@mui/material/styles";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import RemoveIcon from "@mui/icons-material/Remove";
import CheckIcon from "@mui/icons-material/Check";
import { useGetGroupOfStudentIdQuery } from "../groups/groupsApiSlice";
import useAuth from "../../hooks/useAuth";
import StarsIcon from "@mui/icons-material/Stars";
import { green } from "@mui/material/colors";

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgba(255, 255, 255, .05)"
      : "rgba(0, 0, 0, .03)",
  flexDirection: "row-reverse",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)",
  },
  "& .MuiAccordionSummary-content": {
    margin: 0,
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: "1px solid rgba(0, 0, 0, .125)",
}));

function GroupTab({ schedule }) {
  const { userLogin, isAdmin } = useAuth();
  const [group, setGroup] = useState();
  const {
    data: groupRes,
    isSuccess: getGroupOfStudentIdIsSuccess,
    isLoading: getGroupOfStudentIdIsLoading,
    isError: getGroupOfStudentIdIsError,
    error: getGroupOfStudentIdError,
  } = useGetGroupOfStudentIdQuery(
    `?scheduleId=${schedule._id}&studentId=${userLogin._id}`,
    { refetchOnFocus: true }
  );

  useEffect(() => {
    if (groupRes) {
      setGroup(groupRes);
    }
  }, [groupRes]);

  // Group accordion
  const [expanded, setExpanded] = useState([]);

  const handleChange = (panel) => (event, isExpanded) => {
    if (isExpanded) {
      setExpanded((prevExpanded) => [...prevExpanded, panel]);
    } else {
      setExpanded((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    }
  };

  // Student detail accordion
  const [expandedStudent, setExpandedStudent] = useState([]);

  const handleExpandedStudentChange = (panel) => (event, isExpanded) => {
    if (isExpanded) {
      setExpandedStudent((prevExpanded) => [...prevExpanded, panel]);
    } else {
      setExpandedStudent((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    }
  };
  let groupTabContent = null;

  if (getGroupOfStudentIdIsError) {
    groupTabContent = (
      <Typography color={"red"}>
        you are not in any group ask trainer add you to group !
      </Typography>
    );
  }

  if (getGroupOfStudentIdIsSuccess && group) {
    groupTabContent = (
      <Box
        sx={{
          marginBottom: "20px",
          border: "1px solid rgba(0, 0, 0, 0.12)",
          borderRadius: "4px",
        }}
      >
        <Accordion
          expanded={expanded.includes("panel1")}
          onChange={handleChange("panel1")}
        >
          <AccordionSummary
            aria-controls="panel1d-content"
            id="panel1d-header"
            sx={{
              padding: "4px 12px",
              fontSize: "16px",
              alignItems: "center",
              justifyContent: "center",
              position: "relative",
              boxSizing: "border-box",
              outline: "0px",
              border: "0px",
              margin: "0px",
              borderRadius: "0px",
              cursor: "pointer",
              userSelect: "none",
              verticalAlign: "middle",
              appearance: "none",
              textDecoration: "none",
              color: "inherit",
              display: "flex",
              minHeight: "48px",
              transition:
                "min-height 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
              backgroundColor: "rgba(0, 0, 0, 0.03)",
              flexDirection: "row-reverse",
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                marginLeft: "10px",
              }}
            >
              <span
                style={{
                  fontWeight: "600",
                  marginRight: "0.5rem",
                }}
              >
                Group {group.groupNumber}
              </span>
              <span
                style={{
                  fontWeight: "600",
                }}
              >
                {`( ${group.members.length} students)`}
              </span>
              <i
                style={{
                  fontFamily: "sans-serif",
                  fontWeight: "500",
                }}
              >
                &nbsp;- Your group
              </i>
            </Box>
          </AccordionSummary>
          <AccordionDetails
            sx={{
              padding: "10px 25px 25px",
              color: "rgba(0, 0, 0, 0.87)",
            }}
          >
            {/* Student 1 */}
            {group.members.map((member) => (
              <Accordion
                key={member._id}
                expanded={expandedStudent.includes(`panel${member._id}`)}
                onChange={handleExpandedStudentChange(`panel${member._id}`)}
                sx={{ border: "none" }}
              >
                <AccordionSummary
                  aria-controls="panel1d-content"
                  id="panel1d-header"
                  sx={{
                    fontSize: "16px",
                    alignItems: "center",
                    justifyContent: "center",
                    position: "relative",
                    boxSizing: "border-box",
                    outline: "0px",
                    border: "0px",
                    margin: "0px",
                    borderRadius: "0px",
                    cursor: "pointer",
                    userSelect: "none",
                    verticalAlign: "middle",
                    appearance: "none",
                    textDecoration: "none",
                    color: "inherit",
                    display: "flex",
                    minHeight: "48px",
                    transition:
                      "min-height 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                    backgroundColor: "inherit",
                    flexDirection: "row-reverse",
                    padding: "0",
                  }}
                >
                  <Box sx={{ minHeight: "30px" }}>
                    <Box sx={{ marginLeft: "8px" }}>
                      <Box sx={{ marginBottom: "1rem", marginTop: "1rem" }}>
                        <Box
                          sx={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <Box
                            sx={{
                              display: "flex",
                              alignItems: "center",
                              fontWeight: "600",
                              flex: "0 0 auto",
                              width: "50%",
                            }}
                          >
                            <Box
                              sx={{
                                position: "relative",
                                display: "inline-block",
                                height: "40px",
                                width: "40px",
                              }}
                            >
                              <CircularProgress
                                size={40}
                                thickness={5}
                                sx={{
                                  position: "absolute",
                                  color: "rgb(216, 216, 216)",
                                }}
                                variant="determinate"
                                value={100}
                              />
                              <CircularProgress
                                size={40}
                                thickness={5}
                                sx={{
                                  display: "inline-block",
                                  transition:
                                    "transform 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                                  color: "rgb(237, 108, 2)",
                                }}
                                variant="determinate"
                                value={75}
                              />
                              <Avatar
                                sx={{
                                  width: "40px",
                                  height: "40px",
                                  borderRadius: "100%",
                                  verticalAlign: "middle",
                                  position: "absolute",
                                  left: "0",
                                  top: "0",
                                  transform: "scale(0.7)",
                                  border: "none",
                                  backgroundColor: green[500],
                                }}
                              >
                                {member.email.charAt(0).toUpperCase()}
                              </Avatar>
                            </Box>
                            <Box
                              sx={{
                                display: "flex",
                                flexWrap: "wrap",
                              }}
                            >
                              <Box
                                sx={{
                                  flex: "0 0 auto",
                                  width: "100%",
                                  paddingRight: "0.5rem",
                                  marginTop: "0",
                                }}
                              >
                                <span
                                  style={{
                                    marginLeft: "0.5rem",
                                    marginRight: "0.25rem",
                                  }}
                                  title="Mai Chí Thiện"
                                  class="ms-2 me-1"
                                  value="Mai Chí Thiện"
                                >
                                  {member.fullName}
                                  {group?.leader?._id === member._id && (
                                    <StarsIcon color="warning" />
                                  )}{" "}
                                </span>
                              </Box>
                              <Box
                                sx={{
                                  flex: "0 0 auto",
                                  width: "100%",
                                  paddingRight: "0.5rem",
                                  marginTop: "0",
                                }}
                              >
                                <span
                                  style={{
                                    fontStyle: "italic",
                                    fontWeight: "500",
                                    fontFamily: "Line Awesome Free",
                                    marginLeft: "0.5rem",
                                    marginRight: "0.25rem",
                                  }}
                                  class="ms-2 me-1 color-italic"
                                >
                                  {member.email}
                                </span>
                              </Box>
                            </Box>
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                  </Box>
                </AccordionSummary>
                <AccordionDetails
                  sx={{
                    padding: "0 25px",
                    color: "rgba(0, 0, 0, 0.87)",
                    borderTop: "none",
                  }}
                >
                  <Box
                    sx={{
                      marginLeft: "55px",
                      fontFamily: "sans-serif",
                      fontSize: "1.125rem",
                    }}
                  >
                    <Box sx={{ height: "24px" }}>
                      <span
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                        class="left"
                      >
                        <RemoveIcon
                          sx={{
                            color: "#eb5757",
                            height: "100%",
                            lineHeight: "24px",
                          }}
                        />
                        <span
                          style={{ height: "100%" }}
                          title="View question"
                          value="View question"
                          class="ms-1"
                        >
                          View question
                        </span>
                      </span>
                    </Box>
                    <Box
                      sx={{
                        height: "24px",
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <span
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                        class="left"
                      >
                        <CheckIcon
                          sx={{
                            color: "#00ac47",
                            height: "100%",
                            lineHeight: "24px",
                          }}
                        />
                        <span
                          style={{ height: "100%" }}
                          title="No. of comments posted"
                          value="No. of comments posted"
                          class="ms-1"
                        >
                          No. of comments posted
                        </span>
                      </span>
                      <span
                        style={{
                          textAlign: "right",
                          fontWeight: "600",
                        }}
                        title="1/1"
                        class="right text-lightbold"
                        value="1/1"
                      >
                        1/1
                      </span>
                    </Box>
                    <Box
                      sx={{
                        height: "24px",
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <span
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                        class="left"
                      >
                        <CheckIcon
                          sx={{
                            color: "#00ac47",
                            height: "100%",
                            lineHeight: "24px",
                          }}
                        />
                        <span
                          style={{ height: "100%" }}
                          title="No. of stars rated by others"
                          value="No. of stars rated by others"
                          class="ms-1"
                        >
                          No. of stars rated by others
                        </span>
                      </span>
                      <span
                        style={{
                          textAlign: "right",
                          fontWeight: "600",
                        }}
                        title="16/1"
                        class="right text-lightbold"
                        value="16/1"
                      >
                        16/1
                      </span>
                    </Box>
                    <Box
                      sx={{
                        height: "24px",
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <span
                        style={{
                          height: "100%",
                          display: "flex",
                          alignItems: "center",
                        }}
                        class="left"
                      >
                        <CheckIcon
                          sx={{
                            color: "#00ac47",
                            height: "100%",
                            lineHeight: "24px",
                          }}
                        />
                        <span
                          style={{ height: "100%" }}
                          title="No. of votes"
                          value="No. of votes"
                          class="ms-1"
                        >
                          No. of votes
                        </span>
                      </span>
                      <span
                        style={{
                          textAlign: "right",
                          fontWeight: "600",
                        }}
                        title="9/1"
                        class="right text-lightbold"
                        value="9/1"
                      >
                        9/1
                      </span>
                    </Box>
                  </Box>
                </AccordionDetails>
              </Accordion>
            ))}
          </AccordionDetails>
        </Accordion>
      </Box>
    );
  }
  return groupTabContent;
}

export default GroupTab;
