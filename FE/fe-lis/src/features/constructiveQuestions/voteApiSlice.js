import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

// Tạo adapter cho danh sách Lesson
const voteAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = voteAdapter.getInitialState();

// Tạo slice API cho Lesson
export const voteApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getVotes: builder.query({
      query: () => ({
        url: "/votes",
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    getRemainVotesOfUser: builder.mutation({
      query: (initialData) => ({
        url: "/votes/remain",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    vote: builder.mutation({
      query: (initialData) => ({
        url: "/votes",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    updateVote: builder.mutation({
      query: (initialData) => ({
        url: "/votes",
        method: "PATCH",
        body: {
          ...initialData,
        },
      }),
    }),

    deleteVote: builder.mutation({
      query: (initialData) => ({
        url: `/votes`,
        method: "DELETE",
        body: { ...initialData },
      }),
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const { useVoteMutation, useGetRemainVotesOfUserMutation } =
  voteApiSlice;
