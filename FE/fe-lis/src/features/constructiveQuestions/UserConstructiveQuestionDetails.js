import { React, useEffect, useState } from "react";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import {
  useGetMeetingsWithClassIdQuery,
  useAddNewMeetingMutation,
  useDeleteMeetingMutation,
  useUpdateMeetingTimeMutation,
} from "../meeting/meetingsApiSlice";
import {
  Box,
  Breadcrumbs,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  List,
  ListItemAvatar,
  ListItemButton,
  Paper,
  Rating,
  Skeleton,
  Chip,
  Backdrop,
  CircularProgress,
  Typography,
  TextField,
  Autocomplete,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import Timer from "react-compound-timer";
import Title from "../../components/Title";
import { useLocation, useNavigate } from "react-router-dom";
import { useSnackbar } from "notistack";
import { Link } from "react-router-dom";
import OndemandVideoIcon from "@mui/icons-material/OndemandVideo";
import { useGetGroupOfStudentIdQuery } from "../groups/groupsApiSlice";
import CloseIcon from "@mui/icons-material/Close";
import RefreshIcon from "@mui/icons-material/Refresh";
import QuizIcon from "@mui/icons-material/Quiz";
import { generateMeetingId } from "../../utils/generateMeetingId";
import dayjs from "dayjs";
import useAuth from "../../hooks/useAuth";
import { useGetScheduleByIdQuery } from "../schedule/scheduleApiSlice";
import StudentsTab from "../slots/StudentsTab";
import DiscussTab from "./DiscussTab";
import GradeTab from "./GradeTab";
import GroupTab from "./GroupTab";
import { useGetCQWithIdMutation } from "./constructiveQuestionApiSlice";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { MobileDateTimePicker } from "@mui/x-date-pickers/MobileDateTimePicker";
function UserConstructiveQuestionDetails() {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const { userLogin, isAdmin, isTrainer, isTrainee } = useAuth();
  const userId = userLogin?._id;

  const location = useLocation();
  // Get the query parameters from the URL
  const queryParams = new URLSearchParams(location.search);

  // Get a specific parameter value
  const slotValue = queryParams.get("slot");
  // Get a specific parameter value
  const CQId = queryParams.get("cqid");
  // Get a specific parameter value
  const scheduleId = queryParams.get("scheduleId");

  const [constructiveQuestion, setConstructiveQuestion] = useState();
  const [schedule, setSchedule] = useState();
  const [diffEndTimeToNow, setDiffEndTimeToNow] = useState(0);

  const [
    getCQById,
    {
      data: constructiveQuestionRes,
      isSuccess: getCQByIdIsSuccess,
      isLoading: getCQByIdIsLoading,
      isError: getCQByIdIsError,
      error: getCQByIdError,
    },
  ] = useGetCQWithIdMutation();

  const {
    data: scheduleRes,
    isSuccess: getScheduleByIdIsSuccess,
    isLoading: getScheduleByIdIsLoading,
    isError: getScheduleByIdIsError,
    error: getScheduleByIdError,
    refetch: refetchSchedule,
  } = useGetScheduleByIdQuery(scheduleId);

  const handleGetCQWithId = async () => {
    refetchSchedule();
    try {
      await getCQById({ id: CQId });
    } catch (error) {
      enqueueSnackbar("Error with get function !", { variant: "error" });
      navigate(
        `/common/courses-list/class-details?classId=${schedule?.inClass?._id}&semesterId=${schedule?.inClass?.semester?._id}`
      );
    }
  };

  useEffect(() => {
    handleGetCQWithId();
  }, []);

  useEffect(() => {
    handleGetCQWithId();
  }, [CQId]);

  useEffect(() => {
    if (scheduleRes) {
      setSchedule(scheduleRes);
    }
  }, [scheduleRes]);

  useEffect(() => {
    if (constructiveQuestionRes && schedule) {
      const { lessons } = schedule;

      // Search for the question with the specific id in the lessons' questions arrays
      for (const lesson of lessons) {
        const foundQuestion = lesson.constructiveQuestions.find(
          (question) => question._id === CQId
        );

        if (foundQuestion) {
          setConstructiveQuestion(foundQuestion);
          const to = dayjs(foundQuestion?.to);
          const now = dayjs();
          setDiffEndTimeToNow(to.diff(now));
          return; // Return the function early if the question is found
        } else {
          const to = dayjs(constructiveQuestionRes?.to);
          const now = dayjs();
          setDiffEndTimeToNow(to.diff(now));
          setConstructiveQuestion(constructiveQuestionRes);
        }
      }
    }
  }, [constructiveQuestionRes, schedule, getCQByIdIsSuccess]);

  // Tab Lesson / Material / Mentor Handle
  const [valueTab, setValueTab] = useState("1");

  const handleChangeTab = (event, newValue) => {
    setValueTab(newValue);
  };

  // open dialog update meeting
  const [openMeetingDialog, setOpenMeetingDialog] = useState(false);
  const [meetingCode, setMeetingCode] = useState("");
  const [isUpdated, setIsUpdated] = useState(false);
  const handleClickOpenMeetingDialog = () => {
    setOpenMeetingDialog(true);
  };

  const handleCloseMeetingDialog = () => {
    setOpenMeetingDialog(false);
  };

  const handleMeetingCodeChange = (event) => {
    setMeetingCode(event.target.value);
  };

  const handleIsUpdatedChange = () => {
    if (meetingCode === "") {
      setIsUpdated(false);
    } else {
      setIsUpdated(true);
    }
    setOpenMeetingDialog(false);
  };

  // Handle ratings groupmates with Trainee role
  // open dialog grading group
  const [openGradeDialog, setOpenGradeDialog] = useState(false);
  const handleClickOpenGradeDialog = () => {
    setOpenGradeDialog(true);
  };

  const handleCloseGradeDialog = () => {
    setOpenGradeDialog(false);
  };
  const [ratings, setRatings] = useState([
    {
      name: "Trần Văn Cương",
      id: "HE153073",
      hardWorking: 0,
      goodKnowledgeSkills: 0,
      teamworking: 0,
      total: 0,
    },
    {
      name: "Lê Tùng Anh",
      id: "HE153074",
      hardWorking: 0,
      goodKnowledgeSkills: 0,
      teamworking: 0,
      total: 0,
    },
    {
      name: "Nguyễn Việt Thái",
      id: "HE153075",
      hardWorking: 0,
      goodKnowledgeSkills: 0,
      teamworking: 0,
      total: 0,
    },
    {
      name: "Đỗ Hoàng Hải",
      id: "HE153076",
      hardWorking: 0,
      goodKnowledgeSkills: 0,
      teamworking: 0,
      total: 0,
    },
    {
      name: "Dương Thị Thu Hiền",
      id: "HE153077",
      hardWorking: 0,
      goodKnowledgeSkills: 0,
      teamworking: 0,
      total: 0,
    },
  ]);

  const handleRatingChange = (event, newValue, index, category) => {
    setRatings((prevRatings) => {
      const updatedRatings = [...prevRatings];
      updatedRatings[index][category] = newValue;
      updatedRatings[index].total = calculateTotalRating(updatedRatings[index]);
      return updatedRatings;
    });
  };

  const calculateTotalRating = (student) => {
    const { hardWorking, goodKnowledgeSkills, teamworking } = student;
    return (hardWorking + goodKnowledgeSkills + teamworking) / 5;
  };

  const [avarage, setAvarage] = useState(0);
  const handleAvarageChange = () => {
    setAvarage(
      (ratings.reduce((sum, student) => sum + student.total, 0) / 5).toFixed(1)
    );
    setOpenGradeDialog(false);
  };

  const [group, setGroup] = useState(null);
  const [groupMembers, setGroupMembers] = useState([]);
  const {
    data: groupRes,
    isSuccess: getGroupOfStudentIdIsSuccess,
    isLoading: getGroupOfStudentIdIsLoading,
    isError: getGroupOfStudentIdIsError,
    error: getGroupOfStudentIdError,
  } = useGetGroupOfStudentIdQuery(
    `?scheduleId=${scheduleId}&studentId=${userId}`,
    { refetchOnFocus: true }
  );

  useEffect(() => {
    if (groupRes) {
      setGroup(groupRes);
      setGroupMembers(
        groupRes?.members.filter((member) => {
          return member?._id !== userId;
        })
      );
    }
  }, [groupRes]);

  const [isRefetching, setIsRefetching] = useState(false);
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };

  const {
    data: meetings,
    isSuccess: getMeetingsIsSuccess,
    isLoading: getMeetingsIsLoading,
    isError: getMeetingsIsError,
    refetch: getMeetingsRefetch,
  } = useGetMeetingsWithClassIdQuery(schedule?.inClass?._id, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (isRefetching) {
      handleOpenBackDrop();
    } else {
      handleCloseBackDrop();
    }
  }, [isRefetching]);

  const handleLoadMeetingData = () => {
    setIsRefetching(true);
    setOpenBackDrop(true);
    getMeetingsRefetch().then(() => {
      setIsRefetching(false);
    });
  };
  let breadcrumbContent = (
    <Breadcrumbs aria-label="breadcrumb" style={{ fontSize: "20px" }}>
      <Link style={{ color: "#0078D8" }} to={"/common/courses-list"}>
        Home
      </Link>
      <Box sx={{ width: "100%" }}>
        <Skeleton />
      </Box>
    </Breadcrumbs>
  );

  let tableOfContent = (
    <Box sx={{ width: "100%" }}>
      <Skeleton />
      {/* Render a loop of 10 skeletons */}
      {Array.from({ length: 6 }).map((_, index) => (
        <div key={index}>
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
        </div>
      ))}
    </Box>
  );

  let groupContent = (
    <Box sx={{ width: "100%" }}>
      <Skeleton />
      {/* Render a loop of 10 skeletons */}
      {Array.from({ length: 6 }).map((_, index) => (
        <div key={index}>
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
        </div>
      ))}
    </Box>
  );

  useEffect(() => {
    if (getScheduleByIdIsError || getCQByIdIsError) {
      enqueueSnackbar("Something went wrong !", { variant: "error" });
      navigate("/common/courses-list");
    }
  }, [getCQByIdIsError, getScheduleByIdIsError]);

  if (getScheduleByIdIsSuccess && schedule) {
    breadcrumbContent = (
      <Breadcrumbs aria-label="breadcrumb" style={{ fontSize: "20px" }}>
        <Link style={{ color: "#0078D8" }} to={"/common/courses-list"}>
          Home
        </Link>
        <Link
          style={{ color: "#0078D8" }}
          to={`/common/courses-list/class-details?classId=${schedule?.inClass?._id}&semesterId=${schedule?.inClass?.semester?._id}`}
        >
          {schedule?.inClass?.className} -{" "}
          {schedule?.inClass?.subject?.subjectCode} -{" "}
          {schedule?.inClass?.subject?.subjectName}-{" "}
          {schedule?.inClass?.semester?.name}
        </Link>
        <Typography variant="h6">Slot {slotValue}</Typography>
      </Breadcrumbs>
    );
    tableOfContent = (
      <Paper
        sx={{
          mt: 3.5,
          fontWeight: "800",
          width: "100%",
          fontSize: "1rem",
          backgroundColor: "#fff",
          border: "1px solid #d9d9d9",
          borderRadius: "8px",
          padding: "20px",
          marginBottom: "20px",
        }}
      >
        <h3
          style={{
            fontSize: "1.125rem",
            fontWeight: "600",
            lineHeight: "1.2",
            margin: "0 0 20px",
          }}
        >
          Table of contents
        </h3>
        <List
          sx={{
            width: "100%",
            bgcolor: "background.paper",
            listStyle: "none",
            margin: "0",
            padding: "8px 0",
            position: "relative",
          }}
        >
          {schedule?.lessons?.map((lesson) =>
            lesson?.constructiveQuestions?.map((constructiveQuestion) => (
              <Link
                to={`/common/question-details?cqid=${constructiveQuestion._id}&scheduleId=${scheduleId}`}
                key={constructiveQuestion._id}
              >
                <ListItemButton
                  key={constructiveQuestion?._id}
                  sx={{
                    alignItems: "flex-start",
                    width: "100%",
                    textAlign: "left",
                    padding: "8px 16px",
                    position: "relative",
                    boxSizing: "border-box",
                    backgroundColor:
                      CQId === constructiveQuestion?._id ? "#f5f5f5" : "",
                  }}
                >
                  <ListItemAvatar sx={{ minWidth: "45px" }}>
                    <QuizIcon fontSize="large" color="warning" />
                  </ListItemAvatar>
                  <Box sx={{ display: "flex", flexDirection: "column" }}>
                    <span
                      style={{
                        fontWeight: "600",
                        color: "#3c3c3c",
                        lineHeight: "1.4",
                        marginBottom: "10px",
                      }}
                    >
                      {`(Question) ${constructiveQuestion?.title}`}
                    </span>
                    <Box
                      sx={{
                        padding: "4px 8px",
                        borderRadius: "8px",
                        fontSize: "0.75rem",
                        display: "inline-block",
                        textTransform: "unset",
                        fontWeight: "400",
                        letterSpacing: "0.5px",
                        marginRight: "5px",
                        marginBottom: "5px",
                        width: "fit-content",
                        background: "#dff6dd",
                        color: "#00ac47",
                      }}
                      className="activity-state-label"
                    >
                      {constructiveQuestion?.from &&
                      constructiveQuestion?.to &&
                      dayjs(constructiveQuestion?.from).isBefore(dayjs()) &&
                      dayjs(constructiveQuestion?.to).isAfter(dayjs()) ? (
                        <span>On Going</span>
                      ) : constructiveQuestion?.done &&
                        constructiveQuestion?.from &&
                        constructiveQuestion?.to &&
                        dayjs().isAfter(dayjs(constructiveQuestion?.to)) ? (
                        <span>Finished</span>
                      ) : (
                        <span>Not Yet</span>
                      )}
                    </Box>
                  </Box>
                </ListItemButton>
              </Link>
            ))
          )}
          {schedule?.constructiveQuestions.map((constructiveQuestion) => (
            <Link
              to={`/common/question-details?cqid=${constructiveQuestion._id}&scheduleId=${scheduleId}`}
              key={constructiveQuestion?._id}
            >
              <ListItemButton
                key={constructiveQuestion?._id}
                sx={{
                  alignItems: "flex-start",
                  width: "100%",
                  textAlign: "left",
                  padding: "8px 16px",
                  position: "relative",
                  boxSizing: "border-box",
                  backgroundColor:
                    CQId === constructiveQuestion?._id ? "#f5f5f5" : "",
                }}
              >
                <ListItemAvatar sx={{ minWidth: "45px" }}>
                  <QuizIcon fontSize="large" color="warning" />
                </ListItemAvatar>
                <Box sx={{ display: "flex", flexDirection: "column" }}>
                  <span
                    style={{
                      fontWeight: "600",
                      color: "#3c3c3c",
                      lineHeight: "1.4",
                      marginBottom: "10px",
                    }}
                  >
                    {`(Question) ${constructiveQuestion?.title}`}
                  </span>
                  <Box
                    sx={{
                      padding: "4px 8px",
                      borderRadius: "8px",
                      fontSize: "0.75rem",
                      display: "inline-block",
                      textTransform: "unset",
                      fontWeight: "400",
                      letterSpacing: "0.5px",
                      marginRight: "5px",
                      marginBottom: "5px",
                      width: "fit-content",
                      background: "#dff6dd",
                      color: "#00ac47",
                    }}
                    className="activity-state-label"
                  >
                    {constructiveQuestion?.from &&
                    constructiveQuestion?.to &&
                    dayjs(constructiveQuestion?.from).isBefore(dayjs()) &&
                    dayjs(constructiveQuestion?.to).isAfter(dayjs()) ? (
                      <span>On Going</span>
                    ) : constructiveQuestion?.done &&
                      constructiveQuestion?.from &&
                      constructiveQuestion?.to &&
                      dayjs().isAfter(dayjs(constructiveQuestion?.to)) ? (
                      <span>Finished</span>
                    ) : (
                      <span>Not Yet</span>
                    )}
                  </Box>
                </Box>
              </ListItemButton>
            </Link>
          ))}
        </List>
      </Paper>
    );
    if (
      userLogin._id === schedule.inClass.trainer._id ||
      userLogin._id === schedule.trainer._id ||
      isAdmin
    ) {
      groupContent = <StudentsTab schedule={schedule} />;
    } else {
      groupContent = <GroupTab schedule={schedule} />;
    }
  }

  let content = null;

  if (getCQByIdIsLoading) {
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        {/* Render a loop of 10 skeletons */}
        {Array.from({ length: 10 }).map((_, index) => (
          <div key={index}>
            <Skeleton animation={false} />
            <Skeleton animation="wave" />
          </div>
        ))}
      </Box>
    );
  }

  if (getCQByIdIsSuccess && constructiveQuestion) {
    content = (
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Paper sx={{ width: "100%", p: 3 }}>
          <Box>
            {breadcrumbContent}
            <Box sx={{ marginBottom: "24px", marginTop: "1rem" }}>
              <h1
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                  fontWeight: "600",
                  fontSize: "1.5rem",
                  margin: "0",
                }}
              >
                <span
                  style={{
                    overflow: "hidden",
                    textOverflow: "ellipsis",
                    whiteSpace: "nowrap",
                  }}
                >
                  {`(Question) ${constructiveQuestion?.title}`}
                </span>
                <Button variant="contained" onClick={() => handleGetCQWithId()}>
                  reload
                </Button>
              </h1>
            </Box>
          </Box>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            justifyContent={"space-between"}
            alignItems={"flex-start"}
            marginBottom={3}
          >
            <Grid item sx={6} md={8}>
              <Box sx={{ paddingBottom: "20px" }}>
                <Box
                  sx={{
                    padding: "16px",
                    border: "1px dashed #e6e6e6",
                    borderLeft: "4px solid #0078d4",
                    borderRadius: "8px",
                    background: "#fafbfc",
                    width: "100%",
                    display: "inline-block",
                  }}
                >
                  <h3 style={{ fontSize: "1.25rem", margin: "0 0 8px 0" }}>
                    Content
                  </h3>
                  <Box sx={{ overflowWrap: "break-word", overflow: "auto" }}>
                    <span title="undefined">
                      <p
                        style={{
                          marginTop: "0",
                          marginBottom: "1rem",
                          fontSize: "16px",
                        }}
                      >
                        {`${constructiveQuestion?.content}`}
                      </p>
                    </span>
                  </Box>
                </Box>
              </Box>
              <Paper sx={{ padding: 1, color: "blue" }}>
                {diffEndTimeToNow <= 0 ? (
                  <Typography color={"red"}>Discuss time is over</Typography>
                ) : (
                  <Timer
                    initialTime={diffEndTimeToNow}
                    direction="backward"
                    timeToUpdate={10}
                    checkpoints={[
                      {
                        time: 0,
                        callback: () => setDiffEndTimeToNow(0),
                      },
                    ]}
                  >
                    <Timer.Days />
                    {` Day : `}
                    <Timer.Hours />
                    {` Hours : `}
                    <Timer.Minutes />
                    {` Minutes : `}
                    <Timer.Seconds />
                    {` Seconds `}
                  </Timer>
                )}
              </Paper>

              <Box sx={{ width: "100%", typography: "body1" }}>
                <TabContext value={valueTab}>
                  <Box>
                    <TabList
                      onChange={handleChangeTab}
                      aria-label="lab API tabs example"
                    >
                      <Tab label="Group" value="1" />
                      <Tab label="Discuss" value="2" />
                      <Tab label="Grade" value="3" />
                    </TabList>
                  </Box>
                  <TabPanel sx={{ pr: 0, pl: 0 }} value="1">
                    {groupContent}
                  </TabPanel>
                  <TabPanel sx={{ pr: 0, pl: 0 }} value="2">
                    <DiscussTab
                      diffEndTimeToNow={diffEndTimeToNow}
                      schedule={schedule}
                      CQId={constructiveQuestion?._id}
                      constructiveQuestion={constructiveQuestion}
                      slotValue={slotValue}
                    />
                  </TabPanel>
                  <TabPanel sx={{ pr: 0, pl: 0 }} value="3">
                    <GradeTab
                      handleOpenBackDrop={handleOpenBackDrop}
                      handleCloseBackDrop={handleCloseBackDrop}
                      scheduleId={scheduleId}
                      CQId={CQId}
                      schedule={schedule}
                      diffEndTimeToNow={diffEndTimeToNow}
                    />
                  </TabPanel>
                </TabContext>
              </Box>
            </Grid>
            <Grid item xs={6} md={4}>
              <Paper
                sx={{
                  fontWeight: "800",
                  width: "100%",
                  fontSize: "1rem",
                  backgroundColor: "#fff",
                  border: "1px solid #d9d9d9",
                  borderRadius: "8px",
                }}
              >
                <Box sx={{ padding: "16px" }}>
                  <span
                    style={{
                      fontWeight: "600",
                      fontSize: "1.125rem",
                      lineHeight: "1.2",
                      marginBottom: "20px",
                      display: "block",
                    }}
                  >
                    Group meeting
                  </span>

                  {!isUpdated ? (
                    <Typography
                      sx={{
                        color: "#eb5757",
                        fontStyle: "italic",
                        fontSize: "0.95rem",
                        marginTop: "0",
                        marginBottom: "1rem",
                        fontFamily: "sans-serif",
                        fontWeight: "400",
                        boxSizing: "border-box",
                      }}
                    >
                      No meeting video link, click the below button to update
                    </Typography>
                  ) : (
                    <Link
                      to={`https://meet.google.com/${meetingCode}`}
                      target="_blank"
                      style={{ textDecoration: "none" }}
                    >
                      <Button
                        sx={{
                          width: "100%",
                          textAlign: "center",
                          padding: "10px 16px",
                          marginBottom: "1rem",
                        }}
                        variant="contained"
                        color="success"
                      >
                        Meeting
                      </Button>
                    </Link>
                  )}
                  <Button
                    variant="contained"
                    sx={{
                      width: "100%",
                      textAlign: "center",
                      padding: "10px 16px",
                    }}
                    onClick={() => {
                      if (group?.leader?._id === userId) {
                        handleClickOpenMeetingDialog();
                      } else {
                        enqueueSnackbar(
                          "You are not the leader of the group!",
                          {
                            variant: "warning",
                          }
                        );
                      }
                    }}
                  >
                    Update
                  </Button>

                  <Dialog
                    open={openMeetingDialog}
                    keepMounted
                    onClose={handleCloseMeetingDialog}
                    aria-describedby="meeting-dialog-slide-description"
                    maxWidth="md"
                  >
                    <DialogTitle sx={{ padding: "0" }}>
                      <Paper
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                          width: "100%",
                          paddingRight: "10px",
                          paddingLeft: "10px",
                          alignItems: "center",
                        }}
                      >
                        <Box>Update Meeting Video Link</Box>
                        <Box>
                          <IconButton onClick={handleCloseMeetingDialog}>
                            <CloseIcon fontSize="inherit" />
                          </IconButton>
                        </Box>
                      </Paper>
                    </DialogTitle>
                    <DialogContent sx={{ width: "100%" }}>
                      <p
                        style={{
                          marginTop: "1rem",
                          marginBottom: "1rem",
                          fontSize: "1rem",
                        }}
                      >
                        Add meeting video code
                      </p>
                      <Box sx={{ display: "flex", alignItems: "center" }}>
                        <span
                          title={`https://meet.google.com/`}
                          style={{ marginRight: "0.25rem", fontSize: "1rem" }}
                          value={`https://meet.google.com/`}
                        >
                          {`https://meet.google.com/`}
                        </span>
                        <FormControl sx={{ width: "300px" }}>
                          <TextField
                            label="code"
                            type="text"
                            autoFocus
                            defaultValue={meetingCode}
                            onChange={handleMeetingCodeChange}
                            size="small"
                          />
                        </FormControl>
                      </Box>
                    </DialogContent>

                    <DialogActions>
                      <Button onClick={handleCloseMeetingDialog}>Cancel</Button>
                      <Button
                        onClick={handleIsUpdatedChange}
                        color="primary"
                        variant="contained"
                      >
                        Save
                      </Button>
                    </DialogActions>
                  </Dialog>
                </Box>
              </Paper>

              <Paper
                sx={{
                  mt: 3.5,
                  fontWeight: "800",
                  width: "100%",
                  fontSize: "1rem",
                  backgroundColor: "#fff",
                  border: "1px solid #d9d9d9",
                  borderRadius: "8px",
                }}
              >
                <Box sx={{ padding: "16px" }}>
                  <span
                    style={{
                      fontWeight: "600",
                      fontSize: "1.125rem",
                      lineHeight: "1.2",
                      marginBottom: "20px",
                      display: "block",
                    }}
                  >
                    Individual grade
                  </span>

                  <Typography
                    sx={{
                      color: "#eb5757",
                      fontStyle: "italic",
                      fontSize: "0.95rem",
                      marginTop: "0",
                      marginBottom: "1rem",
                      fontFamily: "sans-serif",
                      fontWeight: "400",
                      boxSizing: "border-box",
                    }}
                  >
                    You need grade on groupmates to view your points
                  </Typography>

                  <Button
                    variant="contained"
                    sx={{
                      width: "100%",
                      textAlign: "center",
                      padding: "10px 16px",
                    }}
                    onClick={handleClickOpenGradeDialog}
                  >
                    Grade On Groupmates
                  </Button>
                  <Dialog
                    open={openGradeDialog}
                    keepMounted
                    onClose={handleCloseGradeDialog}
                    aria-describedby="grade-dialog-slide-description"
                    maxWidth="lg"
                  >
                    <DialogTitle sx={{ padding: "0" }}>
                      <Paper
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                          width: "100%",
                          paddingRight: "10px",
                          paddingLeft: "10px",
                          alignItems: "center",
                        }}
                      >
                        <Box>Grading For Groupmates</Box>
                        <Box>
                          <IconButton>
                            <CloseIcon
                              fontSize="inherit"
                              onClick={handleCloseGradeDialog}
                            />
                          </IconButton>
                        </Box>
                      </Paper>
                    </DialogTitle>
                    <DialogContent sx={{ width: "100%" }}>
                      <p
                        style={{
                          marginTop: "1rem",
                          marginBottom: "1rem",
                          fontSize: "1rem",
                        }}
                      >
                        You are grading for groupmates (
                        <i style={{ fontSize: "0.875rem" }}>
                          Click on stars to grade
                        </i>
                        )
                      </p>
                      <TableContainer
                        sx={{
                          border: "1px solid rgba(224, 224, 224, 1)",
                          maxHeight: 440,
                        }}
                      >
                        <Table stickyHeader aria-label="sticky table">
                          <TableHead>
                            <TableRow>
                              <TableCell>Name</TableCell>
                              <TableCell align="center" sx={{ width: "140px" }}>
                                Roll number
                              </TableCell>
                              <TableCell align="center" sx={{ width: "140px" }}>
                                Hard-working
                              </TableCell>
                              <TableCell align="center" sx={{ width: "180px" }}>
                                Good knowledge/Skills
                              </TableCell>
                              <TableCell align="center" sx={{ width: "100px" }}>
                                Teamworking
                              </TableCell>
                              <TableCell align="center" sx={{ width: "100px" }}>
                                Total
                              </TableCell>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            {ratings.map((student, index) => (
                              <TableRow
                                key={index}
                                hover
                                role="checkbox"
                                tabIndex={-1}
                              >
                                <TableCell>{student.name}</TableCell>
                                <TableCell align="center">
                                  {student.id}
                                </TableCell>
                                <TableCell align="center">
                                  <Rating
                                    name={`hard-working-${index}`}
                                    value={student.hardWorking}
                                    onChange={(event, newValue) =>
                                      handleRatingChange(
                                        event,
                                        newValue,
                                        index,
                                        "hardWorking"
                                      )
                                    }
                                    size="small"
                                  />
                                </TableCell>
                                <TableCell align="center">
                                  <Rating
                                    name={`good-knowledge-skills-${index}`}
                                    value={student.goodKnowledgeSkills}
                                    onChange={(event, newValue) =>
                                      handleRatingChange(
                                        event,
                                        newValue,
                                        index,
                                        "goodKnowledgeSkills"
                                      )
                                    }
                                    size="small"
                                  />
                                </TableCell>
                                <TableCell align="center">
                                  <Rating
                                    name={`teamworking-${index}`}
                                    value={student.teamworking}
                                    onChange={(event, newValue) =>
                                      handleRatingChange(
                                        event,
                                        newValue,
                                        index,
                                        "teamworking"
                                      )
                                    }
                                    size="small"
                                  />
                                </TableCell>

                                <TableCell align="center">
                                  {student.total.toFixed(1)}
                                </TableCell>
                              </TableRow>
                            ))}
                          </TableBody>
                        </Table>
                      </TableContainer>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={handleCloseGradeDialog}>Cancel</Button>
                      <Button
                        onClick={handleAvarageChange}
                        color="primary"
                        variant="contained"
                      >
                        Grade
                      </Button>
                    </DialogActions>
                  </Dialog>
                </Box>
              </Paper>

              <Paper
                sx={{
                  mt: 3.5,
                  fontWeight: "800",
                  width: "100%",
                  fontSize: "1rem",
                  backgroundColor: "#fff",
                  border: "1px solid #d9d9d9",
                  borderRadius: "8px",
                }}
              >
                <Box sx={{ padding: "16px" }}>
                  <span
                    style={{
                      fontWeight: "600",
                      fontSize: "1.125rem",
                      lineHeight: "1.2",
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    Class meeting
                    <IconButton
                      onClick={handleLoadMeetingData}
                      className={openBackDrop ? "reload-button-rotate" : ""}
                    >
                      <RefreshIcon />
                    </IconButton>
                  </span>
                  <ul
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      listStyle: "none",
                      padding: "0",
                    }}
                  >
                    {meetings &&
                      meetings?.map((meeting) => (
                        <li
                          key={meeting?._id}
                          style={{
                            marginTop: "10px",
                            borderLeft: "4px solid #0078d4",
                            borderRadius: "8px",
                          }}
                        >
                          <Box
                            sx={{
                              padding: "11px 10px",
                              backgroundColor: "#f7f7f7",
                              border: "1px solid #f7f7f7",
                              display: "inline-flex",
                              textDecoration: "none",
                              color: "#111",
                              borderRadius: "8px",
                              width: "100%",
                            }}
                          >
                            <Box
                              sx={{
                                width: "100%",
                                display: "flex",
                                flexWrap: "wrap",
                                justifyContent: "space-between",
                                alignItems: "center",
                              }}
                            >
                              <Box
                                sx={{
                                  flex: "0 0 auto",
                                  pr: 1.5,
                                  pl: 1.5,
                                }}
                              >
                                <Box>
                                  <Box
                                    sx={{
                                      overflow: "hidden",
                                      textOverflow: "ellipsis",
                                      whiteSpace: "nowrap",
                                      fontWeight: "600",
                                    }}
                                  >
                                    {" "}
                                    {meeting?.meetingName}
                                  </Box>
                                  <Box
                                    sx={{
                                      fontSize: "14px",
                                      color: "rgba(0, 0, 0, 0.5)",
                                    }}
                                  >
                                    {dayjs(meeting?.meetingFrom).format(
                                      "D MMM ddd HH:mm"
                                    )}{" "}
                                    -{" "}
                                    {dayjs(meeting?.meetingTo).format(
                                      "D MMM ddd HH:mm"
                                    )}
                                  </Box>
                                </Box>
                              </Box>
                              <Box>
                                {" "}
                                {dayjs().isBetween(
                                  meeting?.meetingFrom,
                                  meeting?.meetingTo,
                                  null,
                                  "[]"
                                ) ? (
                                  <Link
                                    to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                    target="_blank"
                                  >
                                    <Chip
                                      label="Join Now"
                                      color="success"
                                      variant="outlined"
                                      sx={{ cursor: "pointer" }}
                                    />
                                  </Link>
                                ) : dayjs(meeting?.meetingTo).isBefore(
                                    dayjs()
                                  ) ? (
                                  <Chip label="Ended" />
                                ) : dayjs(meeting?.meetingFrom).isAfter(
                                    dayjs()
                                  ) ? (
                                  <Link
                                    to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                    target="_blank"
                                  >
                                    <Chip
                                      color="primary"
                                      label="Upcoming"
                                      variant="outlined"
                                    />
                                  </Link>
                                ) : null}
                              </Box>
                            </Box>
                          </Box>
                        </li>
                      ))}
                  </ul>
                </Box>
              </Paper>
              {tableOfContent}
            </Grid>
          </Grid>
          <Backdrop
            sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={openBackDrop}
            onClick={handleCloseBackDrop}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
        </Paper>
      </LocalizationProvider>
    );
  }

  return content;
}

export default UserConstructiveQuestionDetails;
