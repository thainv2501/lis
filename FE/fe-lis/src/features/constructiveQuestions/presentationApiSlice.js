import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

// Tạo adapter cho danh sách Lesson
const presentationAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = presentationAdapter.getInitialState();

// Tạo slice API cho Lesson
export const presentationApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getPresentations: builder.mutation({
      query: (initialData) => ({
        url: "/presentations/get",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),

    createPresentation: builder.mutation({
      query: (initialData) => ({
        url: "/presentations",
        method: "POST",
        body: {
          ...initialData,
        },
      }),
    }),
    restartPresentation: builder.mutation({
      query: (initialData) => ({
        url: "/presentations/restart",
        method: "PATCH",
        body: {
          ...initialData,
        },
      }),
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const {
  useGetPresentationsMutation,
  useCreatePresentationMutation,
  useRestartPresentationMutation,
} = presentationApiSlice;
