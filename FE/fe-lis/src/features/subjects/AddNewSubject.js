import { React, useEffect, useState, useMemo } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Pagination from '@mui/material/Pagination';
import TableSortLabel from '@mui/material/TableSortLabel';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Paper, Box, Grid, TextField, Checkbox, Button } from '@mui/material';
import Title from '../../components/Title';
import { useNavigate } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useAddNewSubjectMutation } from './subjectsApiSlice';
import { useGetUsersWithRoleQuery } from '../users/usersApiSlice';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Parser from 'html-react-parser';

const rowsPerPageMentor = 5;
function AddNewSubject() {
  const parser = new DOMParser();
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const [subjectCode, setSubjectCode] = useState('');
  const [subjectName, setSubjectName] = useState('');
  const [subjectDescription, setSubjectDescription] = useState('');
  const [chapterLimit, setChapterLimit] = useState(1);
  const [trainers, setTrainers] = useState([]);
  const [lessons, setLessons] = useState();
  const [materials, setMaterials] = useState([]);
  const [codeError, setCodeError] = useState(false);
  const [codeHelperText, setCodeHelperText] = useState('');
  const [nameError, setNameError] = useState(false);
  const [nameHelperText, setNameHelperText] = useState('');
  const [chapterLimitError, setChapterLimitError] = useState(false);
  const [chapterLimitHelperText, setChapterLimitHelperText] = useState('');
  // Define the regular expression for subject code
  const codeRegExp = /^[A-Za-z]{3}\d{3,4}[A-Za-z]{0,1}$/;
  const nameRegExp = /^[\p{L}0-9\s\-:_]{3,200}$/u;
  const handleSubjectCodeChange = (e) => {
    const value = e.target.value;
    // Check if the input matches the regular expression
    if (value.trim().length === 0) {
      setCodeError(true);
      setCodeHelperText('Subject Code is required!');
    } else if (codeRegExp.test(value)) {
      setCodeError(false);
      setCodeHelperText('');
    } else {
      setCodeError(true);
      setCodeHelperText(
        'Subject Code must start with 3 English letters and end with 3 or 4 digits'
      );
    }
    setSubjectCode(value);
  };

  const handleSubjectNameChange = (e) => {
    const value = e.target.value;
    // Check if the input matches the regular expression
    if (value.trim().length === 0) {
      setNameError(true);
      setNameHelperText('Subject Name is required!');
    } else if (nameRegExp.test(value)) {
      setNameError(false);
      setNameHelperText('');
    } else {
      setNameError(true);
      setNameHelperText('Subject Name is invalid!');
    }
    setSubjectName(value);
  };

  const handleChapterLimitChange = (e) => {
    let value = parseInt(e.target.value);
    if (isNaN(value) || value > 100) {
      setChapterLimitError(true);
      setChapterLimitHelperText('The maximum chapter limit is 100!');
      value = 100;
    }
    if (value < 1) {
      setChapterLimitError(true);
      setChapterLimitHelperText('The minimum chapter limit is 1!');
      value = 1;
    }

    if (value > 1 && value < 100) {
      setChapterLimitError(false);
      setChapterLimitHelperText('');
    }
    setChapterLimit(value);
  };

  const [
    createNewSubject,
    {
      isSuccess: createSubjectIsSuccess,
      isError: createSubjectIsError,
      error: createSubjectError,
    },
  ] = useAddNewSubjectMutation();

  const handleAddNewSubject = async () => {
    try {
      await createNewSubject({
        subjectCode: subjectCode,
        subjectName: subjectName,
        subjectDescription: subjectDescription,
        chapterLimit: chapterLimit,
        trainers: selectedMentors,
        // lessons: lessonsData?.length,
        // materials: materials,
      });
      setSelectedMentors([]);
    } catch (error) {
      enqueueSnackbar('Create failed!: ' + error, {
        variant: 'error',
      });
    }
  };

  useEffect(() => {
    if (createSubjectIsSuccess) {
      enqueueSnackbar('Create new subject successfully!', {
        variant: 'success',
      });
      navigate('/dash-board/subjects-list');
    }
    if (createSubjectIsError) {
      enqueueSnackbar('Create failed!: ' + createSubjectError?.data?.message, {
        variant: 'error',
      });
    }
  }, [createSubjectIsError, createSubjectIsSuccess]);

  // Trainers Data
  const {
    data: trainersData,
    isLoading: getTrainersIsLoading,
    isSuccess: getTrainersIsSuccess,
    isError: getTrainersIsError,
    error: getTrainersError,
  } = useGetUsersWithRoleQuery('Trainer');

  useEffect(() => {
    if (getTrainersIsSuccess) {
      setTrainers(trainersData);
    }
  }, [getTrainersIsSuccess, trainers]);

  useEffect(() => {
    if (getTrainersIsSuccess) {
      setTrainers(trainersData);
    }
  }, [getTrainersIsSuccess, trainers]);

  const [selectedMentors, setSelectedMentors] = useState([]);

  const handleSelectAllMentors = (event) => {
    if (event.target.checked) {
      const allMentors = trainersData?.map((mentor) => mentor?._id);
      setSelectedMentors(allMentors);
    } else {
      setSelectedMentors([]);
    }
  };

  const handleSelectMentor = (event, mentor) => {
    const selectedIndex = selectedMentors.indexOf(mentor?._id);
    let newSelectedMentors = [];

    if (selectedIndex === -1) {
      newSelectedMentors = [...selectedMentors, mentor?._id];
    } else if (selectedIndex === 0) {
      newSelectedMentors = [...selectedMentors.slice(1)];
    } else if (selectedIndex === selectedMentors.length - 1) {
      newSelectedMentors = [...selectedMentors.slice(0, -1)];
    } else if (selectedIndex > 0) {
      newSelectedMentors = [
        ...selectedMentors.slice(0, selectedIndex),
        ...selectedMentors.slice(selectedIndex + 1),
      ];
    }

    setSelectedMentors(newSelectedMentors);
  };

  // Sort and Paging table mentor
  const [orderBy, setOrderBy] = useState('');
  const [order, setOrder] = useState('asc');
  const handleSort = (property) => (event) => {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  };

  const compareValues = (value1, value2, order) => {
    let result = 0;
    if (typeof value1 === 'number' && typeof value2 === 'number') {
      result = value1 - value2;
    } else {
      const str1 = String(value1).toLowerCase();
      const str2 = String(value2).toLowerCase();
      if (str1 < str2) {
        result = -1;
      } else if (str1 > str2) {
        result = 1;
      }
    }
    return order === 'desc' ? result * -1 : result;
  };
  const [pageMentor, setPageMentor] = useState(1);
  const handlePageMentorChange = (event, value) => {
    setPageMentor(value);
  };
  const pageMentorCount = Math.ceil(trainers.length / rowsPerPageMentor);

  const visibleRows = useMemo(() => {
    const comparator = (a, b) => {
      return compareValues(a[orderBy], b[orderBy], order);
    };
    const trainersTemp = [...trainers];
    let trainersIndexField = [];
    trainersTemp.map((trainer, index) => {
      trainersIndexField.push({ ...trainer, index });
    });
    const sortedData = [...trainersIndexField].sort(comparator);

    const addedPageMentorItems = sortedData.slice(
      (pageMentor - 1) * rowsPerPageMentor,
      pageMentor * rowsPerPageMentor
    );

    return addedPageMentorItems;
  }, [trainers, orderBy, order, pageMentor, rowsPerPageMentor]);

  // Tab Lesson / Material / Mentor Handle
  const [valueTab, setValueTab] = useState('1');

  const handleChangeTab = (event, newValue) => {
    setValueTab(newValue);
  };
  const [dataCKEditor, setDataCKEditor] = useState('');

  const handleCKEditorChange = (event, editor) => {
    setDataCKEditor(Parser(editor.getData()));
  };

  return (
    <Paper sx={{ p: 2 }}>
      <Box>
        <Title>Add New Subject</Title>
        <TextField
          autoFocus
          margin="dense"
          label="Subject Code"
          type="text"
          fullWidth
          required
          variant="outlined"
          error={codeError}
          success={!codeError}
          helperText={codeHelperText}
          InputProps={{
            inputProps: { maxLength: 7 },
          }}
          onChange={handleSubjectCodeChange}
          value={subjectCode}
        />
        <TextField
          autoFocus
          margin="dense"
          label="Subject Name"
          type="text"
          fullWidth
          required
          helperText={nameHelperText}
          error={nameError}
          onChange={handleSubjectNameChange}
          value={subjectName}
        />
        <TextField
          margin="dense"
          fullWidth
          autoFocus
          label="Chapter Limit"
          type="number"
          required
          error={chapterLimitError}
          helperText={chapterLimitHelperText}
          InputProps={{
            inputProps: { maxLength: 2 },
          }}
          value={chapterLimit}
          onChange={handleChapterLimitChange}
        />
        <TextField
          autoFocus
          margin="dense"
          id="subjectDescription"
          name="subjectDescription"
          label="Subject Description"
          fullWidth
          autoComplete="family-name"
          multiline
          rows={3}
          maxRows={5}
          onChange={(e) => setSubjectDescription(e.target.value)}
        />

        {/* <CKEditor
          style
          editor={ClassicEditor}
          onChange={(event, editor) => {
            handleCKEditorChange(event, editor);
          }}
        />

        <div>
          <>{dataCKEditor}</>
        </div> */}

        <Grid item xs={12}>
          <Box sx={{ width: '100%', typography: 'body1' }}>
            <TabContext value={valueTab}>
              <Box>
                <TabList
                  onChange={handleChangeTab}
                  aria-label="lab API tabs example"
                >
                  <Tab label="Mentor" value="1" />
                  <Tab label="Material" value="2" />
                </TabList>
              </Box>
              <TabPanel sx={{ pr: 0, pl: 0 }} value="1">
                {' '}
                {/* Mentors display */}
                <Grid item xs={12}>
                  <Paper
                    sx={{ p: 2, display: 'flex', flexDirection: 'column' }}
                  >
                    <Title>Mentors Summary</Title>
                    <Table size="medium">
                      <TableHead>
                        <TableRow>
                          <TableCell padding="checkbox">
                            <Checkbox
                              indeterminate={
                                selectedMentors.length > 0 &&
                                selectedMentors.length < trainersData?.length
                              }
                              checked={
                                trainersData?.length > 0 &&
                                selectedMentors.length === trainersData?.length
                              }
                              onChange={handleSelectAllMentors}
                            />
                          </TableCell>
                          <TableCell align="left" sx={{ color: 'indianred' }}>
                            <TableSortLabel
                              active={orderBy === 'index'}
                              direction={order}
                              onClick={handleSort('index')}
                            >
                              No.
                            </TableSortLabel>
                          </TableCell>
                          <TableCell align="left" sx={{ color: 'indianred' }}>
                            <TableSortLabel
                              active={orderBy === 'fullName'}
                              direction={order}
                              onClick={handleSort('fullName')}
                            >
                              Name
                            </TableSortLabel>
                          </TableCell>
                          <TableCell align="left" sx={{ color: 'indianred' }}>
                            <TableSortLabel
                              active={orderBy === 'phoneNumber'}
                              direction={order}
                              onClick={handleSort('phoneNumber')}
                            >
                              Phone Number
                            </TableSortLabel>
                          </TableCell>
                          <TableCell align="left" sx={{ color: 'indianred' }}>
                            <TableSortLabel
                              active={orderBy === 'email'}
                              direction={order}
                              onClick={handleSort('email')}
                            >
                              Email
                            </TableSortLabel>
                          </TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {visibleRows?.map((mentor) => (
                          <TableRow
                            key={mentor?.index + 1}
                            sx={{
                              '&:last-child td, &:last-child th': {
                                borderBottom:
                                  '1px solid rgba(224, 224, 224, 1)',
                              },
                            }}
                          >
                            <TableCell padding="checkbox">
                              <Checkbox
                                checked={
                                  selectedMentors.indexOf(mentor?._id) !== -1
                                }
                                onChange={(event) =>
                                  handleSelectMentor(event, mentor)
                                }
                              />
                            </TableCell>
                            <TableCell align="left" component="th" scope="row">
                              {mentor?.index + 1}
                            </TableCell>
                            <TableCell align="left">
                              {mentor?.fullName}
                            </TableCell>
                            <TableCell align="left">
                              {mentor?.phoneNumber}
                            </TableCell>
                            <TableCell align="left">{mentor?.email}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                    <Pagination
                      sx={{ paddingTop: '18px' }}
                      count={pageMentorCount}
                      page={pageMentor}
                      onChange={handlePageMentorChange}
                      variant="outlined"
                      color="primary"
                    />
                  </Paper>
                </Grid>
              </TabPanel>
              <TabPanel sx={{ pr: 0, pl: 0 }} value="2">
                <Grid item xs={12} sx={{ width: '100%' }}>
                  <Box sx={{ mt: 2 }}>
                    <Box sx={{ width: '100%', textAlign: 'center' }}>
                      <img
                        src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
                        alt="Img"
                        style={{
                          marginBottom: '25px',
                          maxWidth: '200px',
                          verticalAlign: 'middle',
                        }}
                      />
                      <Box>
                        <h3
                          style={{
                            fontSize: '1.125rem',
                            lineHeight: '1.2',
                            color: 'red',
                            marginBottom: '10px',
                          }}
                        >
                          This function is pending.
                        </h3>
                      </Box>
                    </Box>
                  </Box>
                </Grid>
              </TabPanel>
            </TabContext>
          </Box>
        </Grid>
        <Button variant="contained" onClick={handleAddNewSubject}>
          Add New Subject
        </Button>
      </Box>
    </Paper>
  );
}

export default AddNewSubject;
