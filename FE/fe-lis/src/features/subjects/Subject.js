import { React, useState } from "react";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import { useGetSubjectsQuery } from "./subjectsApiSlice";
import { useNavigate } from "react-router-dom";

import { Menu, MenuItem, Chip, IconButton } from "@mui/material";
import { Edit, Delete } from "@mui/icons-material";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";
import dayjs from "dayjs";

const Subject = ({ subjectId }) => {
  const navigate = useNavigate();
  const { subject } = useGetSubjectsQuery("subjectsList", {
    selectFromResult: ({ data }) => ({
      subject: data?.entities[subjectId],
    }),
  });
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleEditSubject = () =>
    navigate(`/dash-board/subject-details/${subjectId}`);

  return (
    <TableRow
      key={subjectId}
      sx={{
        "&:last-child td, &:last-child th": {
          borderBottom: "1px solid rgba(224, 224, 224, 1)",
        },
      }}
    >
      <TableCell align="left" component="th" scope="row">
        {subjectId}
      </TableCell>
      <TableCell align="left">{subject?.subjectName}</TableCell>
      <TableCell align="left">{subject?.subjectCode}</TableCell>
      <TableCell align="left">
        {dayjs(subject?.createdDate).format("YYYY-MM-DD")}
      </TableCell>
      <TableCell align="left">{subject?.lessons.length}</TableCell>
      <TableCell align="left">
        {subject?.status ? (
          <Chip label="Active" color="success" variant="outlined" />
        ) : (
          <Chip label="Inactive" color="error" variant="outlined" />
        )}
      </TableCell>
      <TableCell align="left">
        <IconButton aria-label="settings" onClick={handleClick}>
          <MoreHorizIcon />
        </IconButton>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
          sx={{ boxShadow: "0 2px 3px rgba(0, 0, 0, 0.2)" }}
        >
          <MenuItem sx={{ fontSize: "0.9rem" }} onClick={handleEditSubject}>
            <Edit sx={{ marginRight: 1, fontSize: "0.9rem" }} />
            Edit
          </MenuItem>
          <MenuItem
            sx={{ fontSize: "0.9rem" }}
            onClick={() => console.log("Delete clicked")}
          >
            <Delete sx={{ marginRight: 1, fontSize: "0.9rem" }} />
            Delete
          </MenuItem>
        </Menu>
      </TableCell>
    </TableRow>
  );
};

export default Subject;
