import BorderColorIcon from '@mui/icons-material/BorderColor';
import SearchIcon from '@mui/icons-material/Search';
import {
  Box,
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Pagination,
  Paper,
  Select,
  Skeleton,
  Tooltip,
  Typography,
  Backdrop,
  CircularProgress,
} from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import { useSnackbar } from 'notistack';
import { React, useEffect, useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import IconTextField from '../../components/IconTextField';
import Title from '../../components/Title';
import {
  useGetSubjectsWithSearchQuery,
  useUpdateSubjectMutation,
} from './subjectsApiSlice';
import dayjs from 'dayjs';

const statuses = [
  { name: 'All', value: 'all' },
  { name: 'Active', value: 'active' },
  { name: 'Deactived', value: 'deactived' },
];
const rowsPerPage = 10;
const SubjectsList = () => {
  const navigate = useNavigate();
  const searchURLDefault = '?page=1&status=all&keyword=';
  const { search } = useLocation();
  const { enqueueSnackbar } = useSnackbar();
  const [page, setPage] = useState(1);
  const [status, setStatus] = useState('all');
  const [keyword, setKeyword] = useState('');

  const [selectedSubject, setSelectedSubject] = useState();
  const [openStatusDialog, setOpenStatusDialog] = useState(false);

  useEffect(() => {
    setKeyword(new URLSearchParams(search).get('keyword') || '');
    setStatus(new URLSearchParams(search).get('status') || 'all');
    setPage(parseInt(new URLSearchParams(search).get('page')) || 1);
  }, [search]);

  const handleSearch = () => {
    handleOpenBackdrop();
    let query = new URLSearchParams({
      page: 1,
      status: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };

  const handleKeywordChanged = (event) => setKeyword(event.target.value);
  const handleStatusFilterChange = (event) => {
    setStatus(event.target.value);
  };
  const handlePaginationChanged = (event, value) => {
    let query = new URLSearchParams({
      page: value,
      status: status,
      keyword,
    }).toString();
    navigate('?' + query);
  };

  // Subjects Data
  const [openBackdrop, setOpenBackdrop] = useState(false);
  const [isRefetching, setIsRefetching] = useState(false);

  const {
    data: subjectsData,
    isLoading: getSubjectsDataIsLoading,
    isSuccess: getSubjectsDataIsSuccess,
    isError: getSubjectsDataIsError,
    error: getSubjectDataError,
    refetch,
  } = useGetSubjectsWithSearchQuery(search === '' ? searchURLDefault : search, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const handleOpenBackdrop = () => {
    setIsRefetching(true);
    setOpenBackdrop(true);
    refetch().then(() => {
      setIsRefetching(false);
    });
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  useEffect(() => {
    if (isRefetching) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [isRefetching]);

  const [
    updateSubject,
    {
      isSuccess: updateSubjectIsSuccess,
      isError: updateSubjectIsError,
      error: updateSubjectError,
    },
  ] = useUpdateSubjectMutation();

  const handleOpenStatusDialog = (subject) => {
    setSelectedSubject(subject);
    setOpenStatusDialog(true);
  };

  const handleCloseStatusDialog = () => {
    setOpenStatusDialog(false);
  };

  const handleChangeStatus = async (subject) => {
    try {
      await updateSubject({
        id: subject._id,
        status: !subject.status,
      });
    } catch (err) {
      console.error('Failed to save the subject new information', err);
      enqueueSnackbar('Failed to save the subject new information: ' + err, {
        variant: 'error',
      });
    }
  };

  useEffect(() => {
    if (updateSubjectIsSuccess) {
      enqueueSnackbar('Change status is successfully!', {
        variant: 'success',
      });
      setOpenStatusDialog(false);
      refetch();
    }
    if (updateSubjectIsError) {
      enqueueSnackbar(
        'Failed to save the subject new information: ' +
          updateSubjectError?.data.message,
        {
          variant: 'error',
        }
      );
    }
  }, [updateSubjectIsError, updateSubjectIsSuccess]);

  // Sort and Paging table mentor
  const [orderBy, setOrderBy] = useState('');
  const [order, setOrder] = useState('asc');
  const handleSort = (property) => (event) => {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  };

  const compareValues = (value1, value2, order) => {
    let result = 0;
    if (typeof value1 === 'number' && typeof value2 === 'number') {
      result = value1 - value2;
    } else {
      const str1 = String(value1).toLowerCase();
      const str2 = String(value2).toLowerCase();
      if (str1 < str2) {
        result = -1;
      } else if (str1 > str2) {
        result = 1;
      }
    }
    return order === 'desc' ? result * -1 : result;
  };

  const handleClickActionViewSubject = (subjectId) => {
    navigate(`/dash-board/subjects-list/subject-details/${subjectId}`);
  };

  let content;

  if (getSubjectsDataIsLoading)
    content = (
      <Box sx={{ width: '100%' }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (getSubjectsDataIsError) {
    content = (
      <Paper sx={{ p: 2 }}>
        <Box>
          <Box
            sx={{
              pt: 2,
              pb: 2,
              display: 'flex',
              justifyContent: 'space-between',
            }}
          >
            <Title>Subjects</Title>
            <Link to={'/dash-board/add-new-subject'}>
              <Button variant="contained">Create New</Button>
            </Link>
          </Box>
          <Box sx={{ pb: 2 }}>
            <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
              <Title>What are you looking for?</Title>
              <Grid
                container
                rowSpacing={1}
                columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                justifyContent={'flex-start'}
                alignItems={'center'}
                marginBottom={3}
              >
                <Grid item sm={12} md={3}>
                  <FormControl size="small" fullWidth>
                    <InputLabel id="status-select">Status</InputLabel>
                    <Select
                      value={status}
                      id="status-select"
                      label="Status"
                      onChange={handleStatusFilterChange}
                    >
                      <MenuItem value={'all'}>All</MenuItem>
                      <MenuItem value={'active'}>Active</MenuItem>
                      <MenuItem value={'deactivated'}>Deactivated</MenuItem>
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item sm={12} md={6}>
                  <IconTextField
                    fullWidth
                    id="keyword"
                    label="Keyword"
                    name="keyword"
                    autoComplete="keyword"
                    placeholder="Search by subject code and name..."
                    autoFocus
                    iconEnd={<SearchIcon />}
                    value={keyword}
                    onChange={handleKeywordChanged}
                    size="small"
                  />
                </Grid>
                <Grid item sm={12} md={3}>
                  <Button variant="contained" onClick={handleSearch}>
                    Search
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </Box>
          {/* end Filter */}
          <Grid container spacing={3}>
            {/* Recent Dash Board */}
            <Grid item xs={12}>
              <Typography variant="h4" color={'red'}>
                {getSubjectDataError?.data?.message}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    );
  }

  if (getSubjectsDataIsSuccess) {
    const { subjects, subjectsCount } = subjectsData;
    const pages = Math.ceil(subjectsCount / 10);
    if (subjectsCount !== 0) {
      content = (
        <Paper sx={{ p: 2 }}>
          <Box>
            <Box
              sx={{
                pt: 2,
                pb: 2,
                display: 'flex',
                justifyContent: 'space-between',
              }}
            >
              <Title>Subjects</Title>
              <Link to={'/dash-board/add-new-subject'}>
                <Button color="secondary" variant="contained">
                  Create New
                </Button>
              </Link>
            </Box>
            <Box sx={{ pb: 2 }}>
              <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                <Title>What are you looking for?</Title>
                <Grid
                  container
                  rowSpacing={1}
                  columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                  justifyContent={'flex-start'}
                  alignItems={'center'}
                  marginBottom={3}
                >
                  <Grid item sm={12} md={3}>
                    <FormControl size="small" fullWidth>
                      <InputLabel id="status-select">Status</InputLabel>
                      <Select
                        value={status}
                        id="status-select"
                        label="Status"
                        onChange={handleStatusFilterChange}
                      >
                        <MenuItem value={'all'}>All</MenuItem>
                        <MenuItem value={'active'}>Active</MenuItem>
                        <MenuItem value={'deactivated'}>Deactivated</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item sm={12} md={6}>
                    <IconTextField
                      fullWidth
                      id="keyword"
                      label="Keyword"
                      name="keyword"
                      autoComplete="keyword"
                      placeholder="Search by subject code and name..."
                      autoFocus
                      value={keyword}
                      iconEnd={<SearchIcon />}
                      onChange={handleKeywordChanged}
                      size="small"
                    />
                  </Grid>
                  <Grid item sm={12} md={3}>
                    <Button variant="contained" onClick={handleSearch}>
                      Search
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Box>
            {/* end Filter */}
            <Grid container spacing={3}>
              {/* Recent Dash Board */}
              <Grid item xs={12}>
                <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                  <Title>Subject Summary</Title>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          <TableSortLabel
                            active={orderBy === 'index'}
                            direction={order}
                            onClick={handleSort('index')}
                          >
                            No.
                          </TableSortLabel>
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          <TableSortLabel
                            active={orderBy === 'subjectName'}
                            direction={order}
                            onClick={handleSort('subjectName')}
                          >
                            Name
                          </TableSortLabel>
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          <TableSortLabel
                            active={orderBy === 'subjectCode'}
                            direction={order}
                            onClick={handleSort('subjectCode')}
                          >
                            Code
                          </TableSortLabel>
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          Created Date
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          Lessons
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          Status
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          Actions
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {subjects?.map((subject, index) => (
                        <TableRow
                          key={(page - 1) * 10 + (index + 1)}
                          sx={{
                            '&:last-child td, &:last-child th': {
                              borderBottom: '1px solid rgba(224, 224, 224, 1)',
                            },
                          }}
                          hover
                        >
                          <TableCell align="left" component="th" scope="row">
                            {(page - 1) * 10 + (index + 1)}
                          </TableCell>
                          <TableCell align="left">
                            {subject?.subjectName}
                          </TableCell>
                          <TableCell align="left">
                            {subject?.subjectCode}
                          </TableCell>
                          <TableCell align="left">
                            {dayjs(subject?.createdDate).format('YYYY-MM-DD')}
                          </TableCell>
                          <TableCell align="left">
                            <Link
                              to={`/dash-board/lessons-list/${subject?._id}`}
                            >
                              <Button>{subject?.lessons.length}</Button>
                            </Link>
                          </TableCell>
                          <TableCell align="left">
                            <Button
                              onClick={() => handleOpenStatusDialog(subject)}
                            >
                              {subject?.status ? (
                                <Chip
                                  label="Active"
                                  color="success"
                                  variant="outlined"
                                />
                              ) : (
                                <Chip
                                  label="Deactived"
                                  color="error"
                                  variant="outlined"
                                />
                              )}
                            </Button>
                          </TableCell>
                          <TableCell align="left">
                            <Box display={'flex'}>
                              <Tooltip
                                title="View Details"
                                placement="top"
                                arrow
                              >
                                <Button
                                  color="success"
                                  onClick={() =>
                                    handleClickActionViewSubject(subject?._id)
                                  }
                                >
                                  <BorderColorIcon />
                                </Button>
                              </Tooltip>
                            </Box>
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                  <Box>
                    {/* update user active dialog */}
                    <Dialog
                      open={openStatusDialog}
                      onClose={handleCloseStatusDialog}
                    >
                      <DialogTitle>Update Status</DialogTitle>
                      <DialogContent>
                        <DialogContentText>
                          Update status of subject:{' '}
                          {selectedSubject?.subjectCode}
                        </DialogContentText>
                      </DialogContent>
                      <DialogActions>
                        <Button
                          onClick={handleCloseStatusDialog}
                          variant="contained"
                          color="warning"
                        >
                          Cancel
                        </Button>
                        <Button
                          onClick={() => handleChangeStatus(selectedSubject)}
                          variant="contained"
                          color="success"
                        >
                          Confirm
                        </Button>
                      </DialogActions>
                    </Dialog>
                  </Box>
                  <Pagination
                    sx={{ paddingTop: '18px' }}
                    count={pages}
                    page={page}
                    onChange={handlePaginationChanged}
                    variant="outlined"
                    color="primary"
                  />
                </Paper>
              </Grid>
              <Backdrop
                open={openBackdrop}
                onClick={handleCloseBackdrop}
                sx={{
                  color: '#fff',
                  zIndex: (theme) => theme.zIndex.drawer + 1,
                }}
              >
                <CircularProgress color="inherit" />
              </Backdrop>
            </Grid>
          </Box>
        </Paper>
      );
    }
  }

  return content;
};

export default SubjectsList;
