import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {
  Box,
  Breadcrumbs,
  Grid,
  Paper,
  Autocomplete,
  Tooltip,
  Card,
  Menu,
  MenuItem,
  IconButton,
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Typography,
  Divider,
  Backdrop,
  CircularProgress,
} from "@mui/material";
import Pagination from "@mui/material/Pagination";
import { TextField } from "@mui/material";
import { Button } from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import {
  useGetSubjectQuestionsMutation,
  useAddNewSupportQuestionMutation,
  useDeleteSupportQuestionMutation,
  useUpdateSupportQuestionMutation,
} from "../supportQuestions/supportQuestionsApiSlice";

import Tab from "@mui/material/Tab";
import React, { useState, useEffect } from "react";
import { Link, useLocation } from "react-router-dom";
import SubjectQuestionDialog from "./SubjectQuestionDialog";
import UploadFileIcon from "@mui/icons-material/UploadFile";
import LinkIcon from "@mui/icons-material/Link";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import useAuth from "../../hooks/useAuth";
import { useSnackbar } from "notistack";
import { useGetMasterDataWithTypeQuery } from "../masterData/masterDataApiSlice";
import { useGetClassBySemesterIdQuery } from "../class/classApiSlice";
const imageTypes = ["png", "jpg", "jpeg", "gif", "bmp", "webp"];

const SubjectQuestion = () => {
  const { enqueueSnackbar } = useSnackbar();
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const type = searchParams.get("type");

  const [valueTab, setValueTab] = useState("my-question");
  const [page, setPage] = useState(1);
  const [keywordMyQuestion, setKeywordMyQuestion] = useState("");
  const [keywordAllQuestion, setKeywordAllQuestion] = useState("");
  const [subjectsMyQuestion, setSubjectsMyQuestion] = useState([]);
  const [subjectsAllQuestion, setSubjectsAllQuestion] = useState([]);
  const [subjectMyQuestion, setSubjectMyQuestion] = useState(null);
  const [subjectAllQuestion, setSubjectAllQuestion] = useState(null);
  const [selectedSubjectQuestion, setSelectedSubjectQuestion] = useState(null);
  const [anchorEls, setAnchorEls] = useState({});

  const handleClick = (event, subjectQuestionId) => {
    setAnchorEls({ ...anchorEls, [subjectQuestionId]: event.currentTarget });
  };

  const handleClose = (subjectQuestionId) => {
    setAnchorEls({ ...anchorEls, [subjectQuestionId]: null });
  };
  const [openSubjectQuestionDialog, setOpenSubjectQuestionDialog] =
    useState(false);

  const handleAddNewSubjectQuestion = () => {
    setSelectedSubjectQuestion(null);
    handleClickOpenSubjectQuestionDialog();
  };
  const handleClickOpenSubjectQuestionDialog = () => {
    setOpenSubjectQuestionDialog(true);
  };

  const handleCloseSubjectQuestionDialog = () => {
    setOpenSubjectQuestionDialog(false);
  };

  // Thêm state để lưu trạng thái hiển thị nội dung source
  const [expandedMap, setExpandedMap] = useState({});

  // Hàm xử lý khi nhấn nút "Xem thêm" hoặc "Ẩn đi"
  const handleToggleExpand = (questionId) => {
    setExpandedMap((prevMap) => ({
      ...prevMap,
      [questionId]: !prevMap[questionId],
    }));
  };

  // master data
  const [semesters, setSemesters] = useState([]);
  const [currentSemester, setCurrentSemester] = useState();
  const [semesterIdMyQuestion, setSemesterIdMyQuestion] = useState();
  const [semesterIdAllQuestion, setSemesterIdAllQuestion] = useState();
  const {
    data: masterDataSemester,
    isSuccess: masterDataIsSuccess,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery("Semester", {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (masterDataIsSuccess) {
      const { masterData, currentSemester } = masterDataSemester;
      setSemesters(masterData);
      setCurrentSemester(currentSemester);
      setSemesterIdMyQuestion(currentSemester?._id);
      setSemesterIdAllQuestion(currentSemester?._id);
    }
  }, [masterDataIsSuccess, currentSemester]);

  const handleSemesterChangeMyQuestion = async (event, newValue) => {
    setSemesterIdMyQuestion(newValue?._id);
  };

  const handleSemesterChangeAllQuestion = async (event, newValue) => {
    setSemesterIdAllQuestion(newValue?._id);
  };
  const {
    data: classData,
    isSuccess: getClassDataIsSuccess,
    isLoading: getClassDataIsLoading,
    isError: getCLassDataIsError,
    error: getClassDataError,
  } = useGetClassBySemesterIdQuery(
    valueTab === "my-question" ? semesterIdMyQuestion : semesterIdAllQuestion,
    {
      refetchOnFocus: true,
      refetchOnMountOrArgChange: true,
    }
  );

  useEffect(() => {
    if (classData) {
      let filteredClasses = [...classData];

      const subjectsMap = new Map();
      filteredClasses.forEach((classObj) => {
        if (classObj?.subject) {
          subjectsMap.set(classObj.subject._id, classObj.subject);
        }
      });
      const uniqueSubjects = Array.from(subjectsMap.values());

      if (uniqueSubjects.length === 0) {
        enqueueSnackbar("No subject found!", {
          variant: "error",
        });
        if (valueTab === "my-question") {
          setSubjectsMyQuestion([]);
          setSubjectMyQuestion(null);
        } else {
          setSubjectsAllQuestion([]);
          setSubjectAllQuestion(null);
        }
      } else {
        if (valueTab === "my-question") {
          setSubjectsMyQuestion(uniqueSubjects || []);
          if (
            !subjectMyQuestion ||
            !uniqueSubjects.includes(subjectMyQuestion)
          ) {
            setSubjectMyQuestion(uniqueSubjects[0] || null);
          }
        } else if (valueTab === "all-question") {
          setSubjectsAllQuestion(uniqueSubjects || []);
          if (
            !subjectAllQuestion ||
            !uniqueSubjects.includes(subjectAllQuestion)
          ) {
            setSubjectAllQuestion(uniqueSubjects[0] || null);
          }
        }
      }
    }
  }, [classData, valueTab]);

  const [subjectQuestions, setSubjectQuestions] = useState([]);
  const [subjectQuestionsCount, setSubjectQuestionsCount] = useState(0);
  const [
    getSubjectQuestions,
    {
      data: subjectQuestionsData,
      isLoading: getSubjectQuestionsDataIsLoading,
      isSuccess: getSubjectQuestionsDataIsSuccess,
      isError: getSubjectQuestionsDataIsError,
      error: getSubjectQuestionsDataError,
    },
  ] = useGetSubjectQuestionsMutation();

  const fetchAllSubjectQuestions = async (page, subjectId) => {
    try {
      const subjectQuestionsResponse = await getSubjectQuestions({
        page: page,
        tab: valueTab,
        type: type,
        subjectId: subjectId,
      });
      setSubjectQuestions(subjectQuestionsResponse.data.subjectQuestions);
      setSubjectQuestionsCount(
        subjectQuestionsResponse.data.totalSubjectQuestionsCount
      );
    } catch (error) {
      // Handle error if the API call fails
      console.error("Error fetching subject questions:", error);
    }
  };

  useEffect(() => {
    fetchAllSubjectQuestions(1, subjectMyQuestion?._id);
  }, [subjectMyQuestion]);

  useEffect(() => {
    fetchAllSubjectQuestions(1, subjectAllQuestion?._id);
  }, [subjectAllQuestion]);

  const handleSubjectChangeMyQuestion = async (event, newValue) => {
    setSubjectMyQuestion(newValue);
    try {
      const subjectQuestionsResponse = await getSubjectQuestions({
        page: 1,
        tab: valueTab,
        type: type,
        subjectId: newValue?._id,
      });
      setSubjectQuestions(subjectQuestionsResponse.data.subjectQuestions);
      setSubjectQuestionsCount(
        subjectQuestionsResponse.data.totalSubjectQuestionsCount
      );
    } catch (error) {
      // Handle error if the API call fails
      console.error("Error fetching subject questions:", error);
    }
  };

  const handleSubjectChangeAllQuestion = async (event, newValue) => {
    setSubjectAllQuestion(newValue);
    try {
      const subjectQuestionsResponse = await getSubjectQuestions({
        page: 1,
        tab: valueTab,
        type: type,
        subjectId: newValue?._id,
      });
      setSubjectQuestions(subjectQuestionsResponse.data.subjectQuestions);
      setSubjectQuestionsCount(
        subjectQuestionsResponse.data.totalSubjectQuestionsCount
      );
    } catch (error) {
      // Handle error if the API call fails
      console.error("Error fetching subject questions:", error);
    }
  };

  const handleChangeTab = async (event, newValue) => {
    setValueTab(newValue);
    try {
      const selectedSubjectId =
        newValue === "my-question"
          ? subjectMyQuestion?._id
          : subjectAllQuestion?._id;

      const subjectQuestionsResponse = await getSubjectQuestions({
        page: 1,
        tab: newValue,
        type: type,
        subjectId: selectedSubjectId,
      });
      setSubjectQuestions(subjectQuestionsResponse.data.subjectQuestions);
      setSubjectQuestionsCount(
        subjectQuestionsResponse.data.totalSubjectQuestionsCount
      );
    } catch (error) {
      // Handle error if the API call fails
      console.error("Error fetching subject questions:", error);
    }
  };

  const handlePaginationChanged = async (event, value) => {
    setPage(value);
    try {
      const subjectQuestionsResponse = await getSubjectQuestions({
        page: value,
        tab: valueTab,
        type: type,
        subjectId:
          valueTab === "my-question"
            ? subjectMyQuestion?._id
            : subjectAllQuestion?._id,
      });
      setSubjectQuestions(subjectQuestionsResponse.data.subjectQuestions);
      setSubjectQuestionsCount(
        subjectQuestionsResponse.data.totalSubjectQuestionsCount
      );
    } catch (error) {
      // Handle error if the API call fails
      console.error("Error fetching subject questions:", error);
    }
  };

  const handleKeywordMyQuestionChange = async (event) => {
    setKeywordMyQuestion(event.target.value);
  };

  const handleKeywordAllQuestionChange = async (event) => {
    setKeywordAllQuestion(event.target.value);
  };

  const handleSearch = async () => {
    try {
      const subjectQuestionsResponse = await getSubjectQuestions({
        page: 1,
        tab: valueTab,
        type: type,
        keyword:
          valueTab === "my-question" ? keywordMyQuestion : keywordAllQuestion,
        subjectId:
          valueTab === "my-question"
            ? subjectMyQuestion?._id
            : subjectAllQuestion?._id,
      });
      setSubjectQuestions(subjectQuestionsResponse.data.subjectQuestions);
      setSubjectQuestionsCount(
        subjectQuestionsResponse.data.totalSubjectQuestionsCount
      );
    } catch (error) {
      // Handle error if the API call fails
      console.error("Error fetching meetings:", error);
    }
  };

  const getTimeAgo = (createdAt) => {
    const now = dayjs();
    const targetDate = dayjs(createdAt);
    const diffInSeconds = now.diff(targetDate, "second");
    const diffInMinutes = Math.floor(diffInSeconds / 60);
    const diffInHours = Math.floor(diffInMinutes / 60);
    const diffInDays = Math.floor(diffInHours / 24);
    const diffInMonths = Math.floor(diffInDays / 30);
    const diffInYears = Math.floor(diffInDays / 365);

    if (diffInSeconds < 60) {
      return `${diffInSeconds} second${diffInSeconds > 1 ? "s" : ""} ago`;
    } else if (diffInMinutes < 60) {
      return `${diffInMinutes} minute${diffInMinutes > 1 ? "s" : ""} ago`;
    } else if (diffInHours < 24) {
      return `${diffInHours} hour${diffInHours > 1 ? "s" : ""} ago`;
    } else if (diffInDays < 30) {
      return `${diffInDays} day${diffInDays > 1 ? "s" : ""} ago`;
    } else if (diffInMonths < 12) {
      return `${diffInMonths} month${diffInMonths > 1 ? "s" : ""} ago`;
    } else {
      return `${diffInYears} year${diffInYears > 1 ? "s" : ""} ago`;
    }
  };

  const [
    addNewSupportQuestion,
    {
      isSuccess: addNewSupportQuestionIsSuccess,
      isLoading: addNewSupportQuestionIsLoading,
      isError: addNewSupportQuestionIsError,
      error: addNewSupportQuestionError,
    },
  ] = useAddNewSupportQuestionMutation();

  const handleViewSubjectQuestion = (data) => {
    setSelectedSubjectQuestion(data);
    handleClickOpenSubjectQuestionDialog();
  };

  // delete support question
  const [
    deleteSupportQuestion,
    {
      isSuccess: deleteSupportQuestionIsSuccess,
      isLoading: deleteSupportQuestionIsLoading,
      isError: deleteSupportQuestionIsError,
      error: deleteSupportQuestionError,
    },
  ] = useDeleteSupportQuestionMutation();

  const [openDeleteQuestionDialog, setOpenDeleteQuestionDialog] =
    useState(false);

  const handleDeleteClicked = (data) => {
    setSelectedSubjectQuestion(data);
    handleClickOpenDeleteQuestionDialog();
  };
  const handleClickOpenDeleteQuestionDialog = () => {
    setOpenDeleteQuestionDialog(true);
  };

  const handleCloseDeleteQuestionDialog = () => {
    setOpenDeleteQuestionDialog(false);
  };

  const handleDeleteSupportQuestion = async () => {
    try {
      await deleteSupportQuestion({
        id: selectedSubjectQuestion?._id,
      }).then(() => {
        enqueueSnackbar("Remove question information is successfully!", {
          variant: "success",
        });
      });
    } catch (error) {
      enqueueSnackbar("Remove failed!: " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (deleteSupportQuestionIsSuccess) {
      handleCloseDeleteQuestionDialog();
      if (subjectQuestions.length === 1 && page > 1) {
        fetchAllSubjectQuestions(
          (prevPage) => prevPage - 1,
          valueTab === "my-question"
            ? subjectMyQuestion?._id
            : subjectAllQuestion?._id
        );
        setPage((prevPage) => prevPage - 1);
      } else {
        fetchAllSubjectQuestions(
          page,
          valueTab === "my-question"
            ? subjectMyQuestion?._id
            : subjectAllQuestion?._id
        );
      }
    }
  }, [deleteSupportQuestionIsSuccess]);

  const [
    updateSupportQuestion,
    {
      isSuccess: updateSupportQuestionIsSuccess,
      isLoading: updateSupportQuestionIsLoading,
      isError: updateSupportQuestionIsError,
      error: updateSupportQuestionError,
    },
  ] = useUpdateSupportQuestionMutation();

  const [openBackdrop, setOpenBackdrop] = useState(false);
  const handleOpenBackdrop = () => {
    setOpenBackdrop(true);
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  useEffect(() => {
    if (getSubjectQuestionsDataIsLoading) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [getSubjectQuestionsDataIsLoading]);
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Grid container style={{ padding: "20px" }}>
        {/* Grid đầu tiên */}
        <Grid item xs={12}>
          <Box
            component="main"
            sx={{ flex: "1", xs: 6, p: 2, marginRight: "50px" }}
          >
            <Breadcrumbs aria-label="breadcrumb" style={{ fontSize: "20px" }}>
              <Link
                style={{ color: "#0078D8", textDecoration: "underline" }}
                to={"/common/courses-list"}
              >
                Home
              </Link>
              <Typography variant="h6">
                {type === "subject-question"
                  ? "Subjects Support"
                  : "Class Support"}
              </Typography>
            </Breadcrumbs>
            {/* end breadcrumb */}
            <Box sx={{ marginTop: "20px", width: "100%", typography: "body1" }}>
              <TabContext value={valueTab}>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                  <TabList
                    onChange={handleChangeTab}
                    aria-label="lab API tabs example"
                  >
                    <Tab label="My Question" value="my-question" />
                    <Tab label="All Question" value="all-question" />
                  </TabList>
                </Box>
                <TabPanel
                  sx={{
                    width: "100%",
                    padding: "0px",
                    marginTop: "30px",
                    borderRadius: "20px",
                  }}
                  value="my-question"
                >
                  <Box>
                    <Paper
                      container
                      style={{
                        width: "100%",
                        padding: "30px 20px",
                      }}
                    >
                      <Box
                        sx={{
                          display: "flex",
                          justifyContent: "space-between",
                          alignItems: "center",
                        }}
                      >
                        <Box
                          sx={{
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <Box
                            sx={{
                              display: "flex",
                              alignItems: "center",
                              marginRight: "20px",
                            }}
                          >
                            <Autocomplete
                              sx={{ width: "250px" }}
                              id="size-small-outlined"
                              size="small"
                              onChange={handleSemesterChangeMyQuestion}
                              value={
                                semesters.find(
                                  (semester) =>
                                    semester?._id === semesterIdMyQuestion
                                ) || null
                              }
                              options={semesters}
                              getOptionLabel={(option) => option.name}
                              renderInput={(params) => (
                                <TextField {...params} label="SEMESTER" />
                              )}
                            />
                          </Box>
                          <Box
                            sx={{
                              display: "flex",
                              alignItems: "center",
                              marginRight: "20px",
                            }}
                          >
                            <Autocomplete
                              sx={{ width: "300px" }}
                              id="size-small-outlined"
                              size="small"
                              options={subjectsMyQuestion}
                              getOptionLabel={(option) => {
                                return `${option.subjectCode} - ${option.subjectName}`;
                              }}
                              value={
                                subjectsMyQuestion.find(
                                  (s) => s?._id === subjectMyQuestion?._id
                                ) || null
                              }
                              onChange={handleSubjectChangeMyQuestion}
                              renderInput={(params) => (
                                <TextField
                                  {...params}
                                  label="Subject"
                                  placeholder="Subject..."
                                />
                              )}
                            />
                          </Box>
                          <Grid marginRight={"20px"}>
                            <TextField
                              sx={{ width: "250px" }}
                              autoFocus
                              placeholder="Type something here..."
                              size="small"
                              fullWidth
                              value={keywordMyQuestion}
                              onChange={handleKeywordMyQuestionChange}
                              label="Keyword"
                            />
                          </Grid>
                          <Grid alignItems={"center"}>
                            <Button variant="contained" onClick={handleSearch}>
                              Search
                            </Button>
                          </Grid>
                        </Box>
                      </Box>
                    </Paper>
                    <Grid container justifyContent={"flex-end"} m={2}>
                      <Button
                        onClick={handleAddNewSubjectQuestion}
                        variant="contained"
                      >
                        Add New Question
                      </Button>
                    </Grid>
                    {subjectQuestions && subjectQuestions?.length > 0 ? (
                      <Box>
                        <Box
                          sx={{
                            padding: "0px",
                            justifyContent: "space-between",
                            border: "none",
                            marginBottom: "24px",
                          }}
                        >
                          {subjectQuestions &&
                            subjectQuestions?.map((subjectQuestion) => (
                              <Box
                                key={subjectQuestion?._id}
                                sx={{ display: "flex", paddingTop: "20px" }}
                              >
                                <Box
                                  sx={{
                                    marginTop: "0",
                                    marginRight: "10px",
                                    display: "block",
                                  }}
                                >
                                  <img
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAhBSURBVHgB7Z3bbxRVHMe/u93KvSx3hNpuIw8g14IXFE1aQR/UBBJMTExMF6PyYqS8+GQCxD8AeNDo0y6i0ZiI+CaJSRcSEEJIC2IAuXS5FVFqSwsFehvPb/dsO7Tb7ezMmZ3f7J5P8tsZSgmd+fb3O7/zO7cAfIZhGBFxqRNG12phYXkfNpmZzhGWFHZVWAvdBwKBFviIABgjxKGXv0rYRnklC0M9CaQFPEz3QsROMIWdYFKkKNIi1cEbEsL2IS1eEprHIZGENQprMvjRJCwKzZBQO4R1GPxpFRYz0m1oaUEPLR/er8SMUhDOSHtUzCgeYkaBhStI0mGkE4ltwhrhTpbnJZRR7hHJyS4UANcFE2LViUsM6b5SMZMUtkUIl4CLBOESRjr87Ra3TSh+sYiIsCb5zK7hiofJuF4qQmUjKazejT6ccg8TYjWISzNKVywiIqxZvItGKEapYOIH3CEucRRfYmEHege75TtRhpKQKLNAit1RaLIRF7ZdRY3SsWBSLGqvVkGTCyou1zsVzZFgWqy8cSyabcG0WLZxJJotwbRYjrEtmt0skRIMLZZ96N3Z6mDnLZhMU6PQOCVqJ+XPKySK/4AKuHugUUlUhMZ9Vr/ZsmCy3EQVDN0pVgu1Y7VWy1j5CNaK0i43uUkSadHGTUIstWGyAh2Bxi0iwiy1Z+N6mBzPaoKmENSPN55mRTAdCgtHEuOExpwhUaadEWgKRQTpaRRjMqaH6azQM8i7asbyslweRt6lxSo8maGqrGT1MOldrdB4SU22vtlYHqZ0lFRji6wajPIw7V2smDGyLcvmYdq7+DAqY8zmYb7sd3X33ceXF/bj0K0juNvbDRU0PP02PlncAA8ZlTGGzH9rpJfVROAzuvq6se3k5zjTcQ4qWTvH8yG/zFq5oRGSkSHR018nu8Qu/aRcrLJAGSJTKsGAjeY/DAlmDK8d9hU3e27j29YDUM3CyfMwZ+JMMKBOTslIYfawOviQE+3NGDQMqGbt7FrRwLNZURzN3JgF82U4PHr7FNxgzazlYMRQWEwJJl2uDj7kbOd5uMGiadVgxFBYzHhYHXzIrQe3ceeR49nPowg/UYHKKfPBjFTK6mvBzt29Ajd4ee5zCAVCYMYm+sgIthI+5PidZqimLBDExsoNYEhKI1972NkO9e3XuzWbUDtzKRiSCokh0Zj5cgbv/f4eXLvfBlWUixDYsGgz3l/0DkQpCAyhJcjVFKgj8CHJezcwKTQ5ZU6YXj4Vz89eifXzX8Lqmcu4ipWhPiBU2wldofcLu3zhYQM3L6DvxI8YvPUX8LAHjpg0DZM+/BqB8gnwIamQyKqHaMbo6cSjX79A37HvqXcPFQSrlvtVLGIGCcZzos3gAB7+8Bn6/1Q7hzVUtQI+pprSepaC9R7+RrlYRGjJK/AxYZ6CDfaj74jlFTjWERkghUQfw1MwSjIGu/6FaoKzqxCY7OuplmHX9ppyQiobdIFgJcsKRl6wFGzg2mm4QWjxOvgdfoIN9GPgwjGoJ4CyymXwOySY+gElBxj32jHY+TdUE5xTnTKf08lOsIFbF1N9MNVMeG2rGDsph89hKNiFo1BNqPZNhFa/hSKgkyodSTDaJKX/ykkoQwxGTtiwFeWvfpC6LwJSgt0FI4Izn0IwvACOCJWLBGMpyl/YLPpd01FEJDMexoZJDWPv6NM/aOD+I+vt24PURz/sUiaccuoEVnM7rrITbCRdD/pw4FQbfj7Zht8v/4dC8saKeYh/9CwY0UKCJcCU823d+Hj/aZy57k3U3rBsHpiRDMplmawyRaL9Xi+2xps9E4tmCtRW8Wr/6KyzTOrUAmbsPXQJ59rUrPOyQ8XEckTmOJsvopgEfWQEc6d4Z5MHfQP47vh1eMmKqgpM4ZVwpDTKCHYQjPjjehe6HWR3Klj/zFwwI0EfLEOiV+2WmaULK8CMBH2kBJNraBNgwrGL7fCaJQumgRFD53Ka6zW/gAG9/YM4frkDXrKqKoy5FaxmVg3NlzALFgcDrrX3pFJ6L9m45kkwI5G5GRKMS1g8c6OLFhjCK2pEKv/ei1VgxGMn3Y4sYXseFn87+w+8YtbUJ/BVdDUqJrNK5x+bPjZSsDg8rnq0XC18hkj9rdeXzsWhT9ehtppVdYNOco+bv5BtJ5yd0IsjuBAXgm0xfyGbYDRxz9s0TZNh1BZ8o4ZhZfLhwrRbTZ7Es+2XqDe45Iv1DS7lN2ov8474WCdF5Nqkmdoy8jK9729hyXm0x5hTiWRbtheaQrM31zksOVdgSy8r9SN+Cwn1u2pyfUPOyXrSy7ZAUyjGfdfjzq6UZ4Ho0Og+e8c7d4WwtCmFDo2uM24ozGBp/rIMjfVgOLuqCMi8W0tYnnAuM5ft0Khml9XT+Yi8VgjIyvEuaFRBYu3J5x/YPcc5Dp9uOcuIfUKsKPJEH7ztDbYP3tZH2xce22IRjvaa06LljSOxCMebA2rRLONYLMLxOlJTH00Px4wNvRvHYhFKFv7SDyIzHp3yj4ZS96gKsQjl+6WKENkIfX4mQQJtHznrySmubHArpxhQuxZBaZJEOgQmoRhX9kKgH1QWM0uxyk/PXOuGWITrW0gLb6sTlxiK39uSwrZYGSJxguu7jdADSG+jhKQYq/30TPRstW6LVXCobaM6pFE8xAzTYWxFi+F/4WJGOrEqLYxh4VoN/nQI22mUgkdZQbyIqLAmgx9NwhoNJkKxO2jEGD48tQHenbqUQHqtXFxVhUIVrE+GMYaPeiRbCXcEJEGoMEv7YByke24imWEtWDaM9PFZEaRHB6rlfXiEmek0WVJer8r7hFsdXLf4H+v6TC9ogJXLAAAAAElFTkSuQmCC"
                                    alt="Img"
                                    style={{
                                      width: "36px",
                                      height: "36px",
                                      border: "1px solid #dddddd",
                                      borderRadius: "50%",
                                      objectFit: "cover",
                                      overflow: "hidden",
                                      transition: "0.3 all",
                                    }}
                                  />
                                </Box>
                                <Box
                                  sx={{
                                    flexGrow: "1",
                                    maxWidth: "calc(100% - 40px)",
                                    fontFamily: "sans-serif",
                                    fontWeight: "500",
                                    fontSize: "1rem",
                                  }}
                                >
                                  <Box
                                    sx={{
                                      display: "flex",
                                      flexDirection: "row",
                                      justifyContent: "space-between",
                                      alignItems: "center",
                                    }}
                                  >
                                    <Link
                                      style={{
                                        textDecoration: "none",
                                        cursor: "pointer",
                                      }}
                                      to={`/common/support-question-details/${subjectQuestion?._id}`}
                                    >
                                      <Box
                                        sx={{
                                          display: "flex",
                                          flexDirection: "column",
                                          position: "relative",
                                          marginBottom: "8px",
                                        }}
                                      >
                                        <Box className="comment-writer">
                                          <span
                                            style={{
                                              padding: "0",
                                              color: "#333",
                                              fontWeight: "600",
                                              textDecoration: "none",
                                            }}
                                            className="user-name"
                                          >
                                            {
                                              subjectQuestion?.createdBy
                                                ?.fullName
                                            }
                                          </span>
                                        </Box>
                                        <Box
                                          style={{ lineHeight: "1" }}
                                          className="comment-user-info"
                                        >
                                          <time
                                            style={{
                                              fontSize: "12px",
                                              color: "#333",
                                              fontStyle: "italic",
                                            }}
                                          >
                                            {getTimeAgo(
                                              subjectQuestion?.createdDate
                                            )}
                                          </time>
                                          <span
                                            class="text-info"
                                            style={{
                                              fontSize: "12px",
                                              marginLeft: "5px",
                                              color:
                                                subjectQuestion?.status ===
                                                "discussing"
                                                  ? "#17a2b8"
                                                  : subjectQuestion?.status ===
                                                    "answered"
                                                  ? "#28a745"
                                                  : "",
                                            }}
                                          >
                                            {subjectQuestion?.status !==
                                            "no-answer"
                                              ? subjectQuestion?.status
                                              : ""}
                                          </span>
                                        </Box>
                                      </Box>
                                    </Link>
                                    <Box>
                                      {userId ===
                                        subjectQuestion.createdBy._id && (
                                        <>
                                          <IconButton
                                            onClick={(event) =>
                                              handleClick(
                                                event,
                                                subjectQuestion._id
                                              )
                                            }
                                            aria-controls={`dropdown-menu-${subjectQuestion._id}`}
                                            aria-haspopup="true"
                                          >
                                            <MoreVertIcon />
                                          </IconButton>
                                          <Menu
                                            id={`dropdown-menu-${subjectQuestion._id}`}
                                            anchorEl={
                                              anchorEls[subjectQuestion._id]
                                            }
                                            open={Boolean(
                                              anchorEls[subjectQuestion._id]
                                            )}
                                            onClose={() =>
                                              handleClose(subjectQuestion._id)
                                            }
                                            anchorOrigin={{
                                              vertical: "bottom",
                                              horizontal: "bottom",
                                            }}
                                            transformOrigin={{
                                              vertical: "top",
                                              horizontal: "top",
                                            }}
                                            getContentAnchorEl={null}
                                          >
                                            <MenuItem
                                              sx={{
                                                width: "200px",
                                                display: "flex",
                                                justifyContent: "space-between",
                                                alignItems: "center",
                                              }}
                                              onClick={() => {
                                                handleViewSubjectQuestion(
                                                  subjectQuestion
                                                );
                                                handleClose(
                                                  subjectQuestion._id
                                                );
                                              }}
                                            >
                                              Edit
                                            </MenuItem>
                                            <MenuItem
                                              sx={{
                                                width: "200px",
                                                display: "flex",
                                                justifyContent: "space-between",
                                                alignItems: "center",
                                              }}
                                              onClick={() => {
                                                handleDeleteClicked(
                                                  subjectQuestion
                                                );
                                                handleClose(
                                                  subjectQuestion._id
                                                );
                                              }}
                                            >
                                              Delete
                                            </MenuItem>
                                          </Menu>
                                        </>
                                      )}
                                    </Box>
                                  </Box>
                                  <Box>
                                    {/* Content */}
                                    <Box
                                      sx={{
                                        width: "100%",
                                        display: "inline-block",
                                        padding: "15px",
                                        background: "#f2f5f7",
                                        borderRadius: "4px",
                                        color: "#373a3c",
                                        fontSize: "16px",
                                      }}
                                    >
                                      <Box
                                        sx={{
                                          maxHeight: "55px",
                                          position: "relative",
                                          width: "100%",
                                          display: "inline-block",
                                          overflow: "hidden",
                                          textOverflow: "ellipsis",
                                          whiteSpace: "nowrap",
                                        }}
                                      >
                                        <Box
                                          sx={{
                                            fontWeight: "bold",
                                            fontSize: "1.3rem",
                                            lineHeight: "1.5",
                                            color: "#212529",
                                          }}
                                        >
                                          {subjectQuestion?.title}
                                        </Box>
                                        <span title="undefined">
                                          {subjectQuestion?.content}
                                        </span>
                                      </Box>
                                      <Box>
                                        {/* Nếu đã hiển thị hết source hoặc không có source thì ẩn đi nút "Xem thêm" */}
                                        {!expandedMap[subjectQuestion._id] &&
                                          subjectQuestion.sources.length >=
                                            1 && (
                                            <Button
                                              sx={{
                                                padding: "0",
                                                marginBottom: "5px",
                                              }}
                                              onClick={() =>
                                                handleToggleExpand(
                                                  subjectQuestion._id
                                                )
                                              }
                                            >
                                              <span>
                                                <b style={{ color: "green" }}>
                                                  Show
                                                </b>{" "}
                                                info sources{" "}
                                                <b
                                                  style={{
                                                    fontSize: "1.25rem",
                                                  }}
                                                >
                                                  ↑↓
                                                </b>
                                              </span>
                                            </Button>
                                          )}
                                        {/* Hiển thị nút "Ẩn đi" khi đã mở rộng nội dung */}
                                        {expandedMap[subjectQuestion._id] && (
                                          <Button
                                            sx={{
                                              padding: "0",
                                              marginBottom: "5px",
                                            }}
                                            onClick={() =>
                                              handleToggleExpand(
                                                subjectQuestion._id
                                              )
                                            }
                                          >
                                            <span>
                                              <b style={{ color: "red" }}>
                                                Hidden
                                              </b>{" "}
                                              info sources
                                              <b
                                                style={{ fontSize: "1.25rem" }}
                                              >
                                                ↑↓
                                              </b>
                                            </span>
                                          </Button>
                                        )}
                                      </Box>
                                      {subjectQuestion?.sources?.length !==
                                        0 && (
                                        <Box
                                          sx={{
                                            maxHeight: expandedMap[
                                              subjectQuestion?._id
                                            ]
                                              ? "1000px"
                                              : "0",
                                            overflow: "hidden",
                                            transitionDuration: "300ms",
                                            transition:
                                              "max-height 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                                          }}
                                        >
                                          {expandedMap[subjectQuestion._id] && (
                                            <Grid container spacing={2}>
                                              {subjectQuestion?.sources?.map(
                                                (source, index) => (
                                                  <Grid item xs={6} key={index}>
                                                    <Card>
                                                      <Box
                                                        sx={{
                                                          height: "80px",
                                                          display: "flex",
                                                          alignItems: "center",
                                                          paddingLeft: "5px",
                                                          gap: 1,
                                                        }}
                                                      >
                                                        <Box
                                                          sx={{
                                                            height: "100%",
                                                          }}
                                                        >
                                                          {source?.type ===
                                                          "link" ? (
                                                            <Box
                                                              sx={{
                                                                height: "100%",
                                                                width: "80px",
                                                                display: "flex",
                                                                alignItems:
                                                                  "center",
                                                                justifyContent:
                                                                  "center",
                                                                borderRight:
                                                                  "1px solid rgba(0, 0, 0, 0.2)",
                                                                borderLeft:
                                                                  "1px solid rgba(0, 0, 0, 0.2)",
                                                              }}
                                                            >
                                                              <LinkIcon fontSize="large" />
                                                            </Box>
                                                          ) : imageTypes.includes(
                                                              source?.name
                                                                .split(".")
                                                                .pop()
                                                            ) ? (
                                                            <img
                                                              src={source?.url}
                                                              alt={source?.name}
                                                              style={{
                                                                height: "100%",
                                                                width: "80px",
                                                                objectFit:
                                                                  "cover",
                                                                verticalAlign:
                                                                  "middle",
                                                              }}
                                                            />
                                                          ) : (
                                                            <Box
                                                              sx={{
                                                                height: "100%",
                                                                width: "80px",
                                                                display: "flex",
                                                                alignItems:
                                                                  "center",
                                                                justifyContent:
                                                                  "center",
                                                                borderRight:
                                                                  "1px solid rgba(0, 0, 0, 0.2)",
                                                                borderLeft:
                                                                  "1px solid rgba(0, 0, 0, 0.2)",
                                                              }}
                                                            >
                                                              <UploadFileIcon fontSize="large" />
                                                            </Box>
                                                          )}
                                                        </Box>
                                                        <Tooltip
                                                          title={source?.url}
                                                          placement="top"
                                                          arrow
                                                        >
                                                          <Box
                                                            sx={{
                                                              overflow:
                                                                "hidden",
                                                              whiteSpace:
                                                                "nowrap",
                                                              textOverflow:
                                                                "ellipsis",
                                                              maxWidth: "100%",
                                                            }}
                                                          >
                                                            <Link
                                                              to={source?.url}
                                                              target="_blank"
                                                            >
                                                              {source?.type ===
                                                              "link"
                                                                ? source?.url
                                                                : source?.name}
                                                            </Link>
                                                          </Box>
                                                        </Tooltip>
                                                      </Box>
                                                    </Card>
                                                  </Grid>
                                                )
                                              )}
                                            </Grid>
                                          )}
                                        </Box>
                                      )}
                                    </Box>
                                  </Box>
                                </Box>
                              </Box>
                            ))}
                        </Box>
                        <Pagination
                          count={Math.ceil(subjectQuestionsCount / 5)}
                          page={page}
                          onChange={handlePaginationChanged}
                          variant="outlined"
                          color="primary"
                        />
                      </Box>
                    ) : (
                      <Box sx={{ mt: 2 }}>
                        <Box sx={{ width: "100%", textAlign: "center" }}>
                          <img
                            src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
                            alt="Img"
                            style={{
                              marginBottom: "25px",
                              maxWidth: "200px",
                              verticalAlign: "middle",
                            }}
                          />
                          <Box>
                            <h3
                              style={{
                                fontSize: "1.125rem",
                                lineHeight: "1.2",
                                color: "#0078d4",
                                marginBottom: "10px",
                              }}
                            >
                              No subject questions now.
                            </h3>
                            <p style={{ margin: "0", fontSize: "20px" }}>
                              Please contact your school administration for more
                              information or add new question.
                            </p>
                          </Box>
                        </Box>
                      </Box>
                    )}
                  </Box>
                </TabPanel>
                <TabPanel
                  sx={{
                    width: "100%",
                    padding: "0px",
                    marginTop: "30px",
                    borderRadius: "20px",
                  }}
                  value="all-question"
                >
                  <Paper
                    container
                    style={{ width: "100%", padding: "30px 20px" }}
                  >
                    <Grid container display={"flex"}>
                      <Grid marginRight={"20px"}>
                        <Box>
                          <Autocomplete
                            sx={{ width: "250px" }}
                            id="size-small-outlined"
                            size="small"
                            onChange={handleSemesterChangeAllQuestion}
                            value={
                              semesters.find(
                                (semester) =>
                                  semester?._id === semesterIdAllQuestion
                              ) || null
                            }
                            options={semesters}
                            getOptionLabel={(option) => option.name}
                            renderInput={(params) => (
                              <TextField {...params} label="SEMESTER" />
                            )}
                          />
                        </Box>
                      </Grid>
                      <Grid marginRight={"20px"}>
                        <Box>
                          <Autocomplete
                            className="custom-autocomplete"
                            sx={{ width: "300px" }}
                            id="size-small-outlined"
                            size="small"
                            options={subjectsAllQuestion}
                            getOptionLabel={(option) => {
                              return `${option.subjectCode} - ${option.subjectName}`;
                            }}
                            value={
                              subjectsAllQuestion.find(
                                (s) => s?._id === subjectAllQuestion?._id
                              ) || null
                            }
                            onChange={handleSubjectChangeAllQuestion}
                            renderInput={(params) => (
                              <TextField
                                {...params}
                                label="Subject"
                                placeholder="Subject..."
                              />
                            )}
                          />
                        </Box>
                      </Grid>
                      <Grid marginRight={"20px"}>
                        <TextField
                          sx={{ width: "250px" }}
                          autoFocus
                          placeholder="Type something here..."
                          size="small"
                          fullWidth
                          value={keywordAllQuestion}
                          onChange={handleKeywordAllQuestionChange}
                          label="Keyword"
                        />
                      </Grid>
                      <Grid alignItems={"center"}>
                        <Button variant="contained" onClick={handleSearch}>
                          Search
                        </Button>
                      </Grid>
                    </Grid>
                  </Paper>
                  {subjectQuestions && subjectQuestions?.length > 0 ? (
                    <Box>
                      <Box
                        sx={{
                          padding: "0px",
                          justifyContent: "space-between",
                          border: "none",
                          marginBottom: "24px",
                        }}
                      >
                        {subjectQuestions &&
                          subjectQuestions?.map((subjectQuestion) => (
                            <Box
                              key={subjectQuestion?._id}
                              sx={{ display: "flex", paddingTop: "20px" }}
                            >
                              <Box
                                sx={{
                                  marginTop: "0",
                                  marginRight: "10px",
                                  display: "block",
                                }}
                              >
                                <img
                                  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAhBSURBVHgB7Z3bbxRVHMe/u93KvSx3hNpuIw8g14IXFE1aQR/UBBJMTExMF6PyYqS8+GQCxD8AeNDo0y6i0ZiI+CaJSRcSEEJIC2IAuXS5FVFqSwsFehvPb/dsO7Tb7ezMmZ3f7J5P8tsZSgmd+fb3O7/zO7cAfIZhGBFxqRNG12phYXkfNpmZzhGWFHZVWAvdBwKBFviIABgjxKGXv0rYRnklC0M9CaQFPEz3QsROMIWdYFKkKNIi1cEbEsL2IS1eEprHIZGENQprMvjRJCwKzZBQO4R1GPxpFRYz0m1oaUEPLR/er8SMUhDOSHtUzCgeYkaBhStI0mGkE4ltwhrhTpbnJZRR7hHJyS4UANcFE2LViUsM6b5SMZMUtkUIl4CLBOESRjr87Ra3TSh+sYiIsCb5zK7hiofJuF4qQmUjKazejT6ccg8TYjWISzNKVywiIqxZvItGKEapYOIH3CEucRRfYmEHege75TtRhpKQKLNAit1RaLIRF7ZdRY3SsWBSLGqvVkGTCyou1zsVzZFgWqy8cSyabcG0WLZxJJotwbRYjrEtmt0skRIMLZZ96N3Z6mDnLZhMU6PQOCVqJ+XPKySK/4AKuHugUUlUhMZ9Vr/ZsmCy3EQVDN0pVgu1Y7VWy1j5CNaK0i43uUkSadHGTUIstWGyAh2Bxi0iwiy1Z+N6mBzPaoKmENSPN55mRTAdCgtHEuOExpwhUaadEWgKRQTpaRRjMqaH6azQM8i7asbyslweRt6lxSo8maGqrGT1MOldrdB4SU22vtlYHqZ0lFRji6wajPIw7V2smDGyLcvmYdq7+DAqY8zmYb7sd3X33ceXF/bj0K0juNvbDRU0PP02PlncAA8ZlTGGzH9rpJfVROAzuvq6se3k5zjTcQ4qWTvH8yG/zFq5oRGSkSHR018nu8Qu/aRcrLJAGSJTKsGAjeY/DAlmDK8d9hU3e27j29YDUM3CyfMwZ+JMMKBOTslIYfawOviQE+3NGDQMqGbt7FrRwLNZURzN3JgF82U4PHr7FNxgzazlYMRQWEwJJl2uDj7kbOd5uMGiadVgxFBYzHhYHXzIrQe3ceeR49nPowg/UYHKKfPBjFTK6mvBzt29Ajd4ee5zCAVCYMYm+sgIthI+5PidZqimLBDExsoNYEhKI1972NkO9e3XuzWbUDtzKRiSCokh0Zj5cgbv/f4eXLvfBlWUixDYsGgz3l/0DkQpCAyhJcjVFKgj8CHJezcwKTQ5ZU6YXj4Vz89eifXzX8Lqmcu4ipWhPiBU2wldofcLu3zhYQM3L6DvxI8YvPUX8LAHjpg0DZM+/BqB8gnwIamQyKqHaMbo6cSjX79A37HvqXcPFQSrlvtVLGIGCcZzos3gAB7+8Bn6/1Q7hzVUtQI+pprSepaC9R7+RrlYRGjJK/AxYZ6CDfaj74jlFTjWERkghUQfw1MwSjIGu/6FaoKzqxCY7OuplmHX9ppyQiobdIFgJcsKRl6wFGzg2mm4QWjxOvgdfoIN9GPgwjGoJ4CyymXwOySY+gElBxj32jHY+TdUE5xTnTKf08lOsIFbF1N9MNVMeG2rGDsph89hKNiFo1BNqPZNhFa/hSKgkyodSTDaJKX/ykkoQwxGTtiwFeWvfpC6LwJSgt0FI4Izn0IwvACOCJWLBGMpyl/YLPpd01FEJDMexoZJDWPv6NM/aOD+I+vt24PURz/sUiaccuoEVnM7rrITbCRdD/pw4FQbfj7Zht8v/4dC8saKeYh/9CwY0UKCJcCU823d+Hj/aZy57k3U3rBsHpiRDMplmawyRaL9Xi+2xps9E4tmCtRW8Wr/6KyzTOrUAmbsPXQJ59rUrPOyQ8XEckTmOJsvopgEfWQEc6d4Z5MHfQP47vh1eMmKqgpM4ZVwpDTKCHYQjPjjehe6HWR3Klj/zFwwI0EfLEOiV+2WmaULK8CMBH2kBJNraBNgwrGL7fCaJQumgRFD53Ka6zW/gAG9/YM4frkDXrKqKoy5FaxmVg3NlzALFgcDrrX3pFJ6L9m45kkwI5G5GRKMS1g8c6OLFhjCK2pEKv/ei1VgxGMn3Y4sYXseFn87+w+8YtbUJ/BVdDUqJrNK5x+bPjZSsDg8rnq0XC18hkj9rdeXzsWhT9ehtppVdYNOco+bv5BtJ5yd0IsjuBAXgm0xfyGbYDRxz9s0TZNh1BZ8o4ZhZfLhwrRbTZ7Es+2XqDe45Iv1DS7lN2ov8474WCdF5Nqkmdoy8jK9729hyXm0x5hTiWRbtheaQrM31zksOVdgSy8r9SN+Cwn1u2pyfUPOyXrSy7ZAUyjGfdfjzq6UZ4Ho0Og+e8c7d4WwtCmFDo2uM24ozGBp/rIMjfVgOLuqCMi8W0tYnnAuM5ft0Khml9XT+Yi8VgjIyvEuaFRBYu3J5x/YPcc5Dp9uOcuIfUKsKPJEH7ztDbYP3tZH2xce22IRjvaa06LljSOxCMebA2rRLONYLMLxOlJTH00Px4wNvRvHYhFKFv7SDyIzHp3yj4ZS96gKsQjl+6WKENkIfX4mQQJtHznrySmubHArpxhQuxZBaZJEOgQmoRhX9kKgH1QWM0uxyk/PXOuGWITrW0gLb6sTlxiK39uSwrZYGSJxguu7jdADSG+jhKQYq/30TPRstW6LVXCobaM6pFE8xAzTYWxFi+F/4WJGOrEqLYxh4VoN/nQI22mUgkdZQbyIqLAmgx9NwhoNJkKxO2jEGD48tQHenbqUQHqtXFxVhUIVrE+GMYaPeiRbCXcEJEGoMEv7YByke24imWEtWDaM9PFZEaRHB6rlfXiEmek0WVJer8r7hFsdXLf4H+v6TC9ogJXLAAAAAElFTkSuQmCC"
                                  alt="Img"
                                  style={{
                                    width: "36px",
                                    height: "36px",
                                    border: "1px solid #dddddd",
                                    borderRadius: "50%",
                                    objectFit: "cover",
                                    overflow: "hidden",
                                    transition: "0.3 all",
                                  }}
                                />
                              </Box>
                              <Box
                                sx={{
                                  flexGrow: "1",
                                  maxWidth: "calc(100% - 40px)",
                                  fontFamily: "sans-serif",
                                  fontWeight: "500",
                                  fontSize: "1rem",
                                }}
                              >
                                <Box
                                  sx={{
                                    display: "flex",
                                    flexDirection: "row",
                                    justifyContent: "space-between",
                                    alignItems: "center",
                                  }}
                                >
                                  <Link
                                    style={{
                                      textDecoration: "none",
                                      cursor: "pointer",
                                    }}
                                    to={`/common/support-question-details/${subjectQuestion?._id}`}
                                  >
                                    <Box
                                      sx={{
                                        display: "flex",
                                        flexDirection: "column",
                                        position: "relative",
                                        marginBottom: "8px",
                                      }}
                                    >
                                      <Box className="comment-writer">
                                        <span
                                          style={{
                                            padding: "0",
                                            color: "#333",
                                            fontWeight: "600",
                                            textDecoration: "none",
                                          }}
                                          className="user-name"
                                        >
                                          {subjectQuestion?.createdBy?.fullName}
                                        </span>
                                      </Box>
                                      <Box
                                        style={{ lineHeight: "1" }}
                                        className="comment-user-info"
                                      >
                                        <time
                                          style={{
                                            fontSize: "12px",
                                            color: "#333",
                                            fontStyle: "italic",
                                          }}
                                        >
                                          {getTimeAgo(
                                            subjectQuestion?.createdDate
                                          )}
                                        </time>
                                        <span
                                          class="text-info"
                                          style={{
                                            fontSize: "12px",
                                            marginLeft: "5px",
                                            color:
                                              subjectQuestion?.status ===
                                              "discussing"
                                                ? "#17a2b8"
                                                : subjectQuestion?.status ===
                                                  "answered"
                                                ? "#28a745"
                                                : "",
                                          }}
                                        >
                                          {subjectQuestion?.status !==
                                          "no-answer"
                                            ? subjectQuestion?.status
                                            : ""}
                                        </span>
                                      </Box>
                                    </Box>
                                  </Link>
                                  <Box>
                                    {userId ===
                                      subjectQuestion.createdBy._id && (
                                      <>
                                        <IconButton
                                          onClick={(event) =>
                                            handleClick(
                                              event,
                                              subjectQuestion._id
                                            )
                                          }
                                          aria-controls={`dropdown-menu-${subjectQuestion._id}`}
                                          aria-haspopup="true"
                                        >
                                          <MoreVertIcon />
                                        </IconButton>
                                        <Menu
                                          id={`dropdown-menu-${subjectQuestion._id}`}
                                          anchorEl={
                                            anchorEls[subjectQuestion._id]
                                          }
                                          open={Boolean(
                                            anchorEls[subjectQuestion._id]
                                          )}
                                          onClose={() =>
                                            handleClose(subjectQuestion._id)
                                          }
                                          anchorOrigin={{
                                            vertical: "bottom",
                                            horizontal: "bottom",
                                          }}
                                          transformOrigin={{
                                            vertical: "top",
                                            horizontal: "top",
                                          }}
                                          getContentAnchorEl={null}
                                        >
                                          <MenuItem
                                            sx={{
                                              width: "200px",
                                              display: "flex",
                                              justifyContent: "space-between",
                                              alignItems: "center",
                                            }}
                                            onClick={() => {
                                              handleViewSubjectQuestion(
                                                subjectQuestion
                                              );
                                              handleClose(subjectQuestion._id);
                                            }}
                                          >
                                            Edit
                                          </MenuItem>
                                          <MenuItem
                                            sx={{
                                              width: "200px",
                                              display: "flex",
                                              justifyContent: "space-between",
                                              alignItems: "center",
                                            }}
                                            onClick={() => {
                                              handleDeleteClicked(
                                                subjectQuestion
                                              );
                                              handleClose(subjectQuestion._id);
                                            }}
                                          >
                                            Delete
                                          </MenuItem>
                                        </Menu>
                                      </>
                                    )}
                                  </Box>
                                </Box>
                                <Box>
                                  {/* Content */}
                                  <Box
                                    sx={{
                                      width: "100%",
                                      display: "inline-block",
                                      padding: "15px",
                                      background: "#f2f5f7",
                                      borderRadius: "4px",
                                      color: "#373a3c",
                                      fontSize: "16px",
                                    }}
                                  >
                                    <Box
                                      sx={{
                                        maxHeight: "55px",
                                        position: "relative",
                                        width: "100%",
                                        display: "inline-block",
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                      }}
                                    >
                                      <Box
                                        sx={{
                                          fontWeight: "bold",
                                          fontSize: "1.3rem",
                                          lineHeight: "1.5",
                                          color: "#212529",
                                        }}
                                      >
                                        {subjectQuestion?.title}
                                      </Box>
                                      <span title="undefined">
                                        {subjectQuestion?.content}
                                      </span>
                                    </Box>
                                    <Box>
                                      {/* Nếu đã hiển thị hết source hoặc không có source thì ẩn đi nút "Xem thêm" */}
                                      {!expandedMap[subjectQuestion._id] &&
                                        subjectQuestion.sources.length >= 1 && (
                                          <Button
                                            sx={{
                                              padding: "0",
                                              marginBottom: "5px",
                                            }}
                                            onClick={() =>
                                              handleToggleExpand(
                                                subjectQuestion._id
                                              )
                                            }
                                          >
                                            <span>
                                              <b style={{ color: "green" }}>
                                                Show
                                              </b>{" "}
                                              info sources{" "}
                                              <b
                                                style={{
                                                  fontSize: "1.25rem",
                                                }}
                                              >
                                                ↑↓
                                              </b>
                                            </span>
                                          </Button>
                                        )}
                                      {/* Hiển thị nút "Ẩn đi" khi đã mở rộng nội dung */}
                                      {expandedMap[subjectQuestion._id] && (
                                        <Button
                                          sx={{
                                            padding: "0",
                                            marginBottom: "5px",
                                          }}
                                          onClick={() =>
                                            handleToggleExpand(
                                              subjectQuestion._id
                                            )
                                          }
                                        >
                                          <span>
                                            <b style={{ color: "red" }}>
                                              Hidden
                                            </b>{" "}
                                            info sources
                                            <b style={{ fontSize: "1.25rem" }}>
                                              ↑↓
                                            </b>
                                          </span>
                                        </Button>
                                      )}
                                    </Box>
                                    {subjectQuestion?.sources?.length !== 0 && (
                                      <Box
                                        sx={{
                                          maxHeight: expandedMap[
                                            subjectQuestion?._id
                                          ]
                                            ? "1000px"
                                            : "0",
                                          overflow: "hidden",
                                          transitionDuration: "300ms",
                                          transition:
                                            "max-height 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                                        }}
                                      >
                                        {expandedMap[subjectQuestion._id] && (
                                          <Grid container spacing={2}>
                                            {subjectQuestion?.sources?.map(
                                              (source, index) => (
                                                <Grid item xs={6} key={index}>
                                                  <Card>
                                                    <Box
                                                      sx={{
                                                        height: "80px",
                                                        display: "flex",
                                                        alignItems: "center",
                                                        paddingLeft: "5px",
                                                        gap: 1,
                                                      }}
                                                    >
                                                      <Box
                                                        sx={{
                                                          height: "100%",
                                                        }}
                                                      >
                                                        {source?.type ===
                                                        "link" ? (
                                                          <Box
                                                            sx={{
                                                              height: "100%",
                                                              width: "80px",
                                                              display: "flex",
                                                              alignItems:
                                                                "center",
                                                              justifyContent:
                                                                "center",
                                                              borderRight:
                                                                "1px solid rgba(0, 0, 0, 0.2)",
                                                              borderLeft:
                                                                "1px solid rgba(0, 0, 0, 0.2)",
                                                            }}
                                                          >
                                                            <LinkIcon fontSize="large" />
                                                          </Box>
                                                        ) : imageTypes.includes(
                                                            source?.name
                                                              .split(".")
                                                              .pop()
                                                          ) ? (
                                                          <img
                                                            src={source?.url}
                                                            alt={source?.name}
                                                            style={{
                                                              height: "100%",
                                                              width: "80px",
                                                              objectFit:
                                                                "cover",
                                                              verticalAlign:
                                                                "middle",
                                                            }}
                                                          />
                                                        ) : (
                                                          <Box
                                                            sx={{
                                                              height: "100%",
                                                              width: "80px",
                                                              display: "flex",
                                                              alignItems:
                                                                "center",
                                                              justifyContent:
                                                                "center",
                                                              borderRight:
                                                                "1px solid rgba(0, 0, 0, 0.2)",
                                                              borderLeft:
                                                                "1px solid rgba(0, 0, 0, 0.2)",
                                                            }}
                                                          >
                                                            <UploadFileIcon fontSize="large" />
                                                          </Box>
                                                        )}
                                                      </Box>
                                                      <Tooltip
                                                        title={source?.url}
                                                        placement="top"
                                                        arrow
                                                      >
                                                        <Box
                                                          sx={{
                                                            overflow: "hidden",
                                                            whiteSpace:
                                                              "nowrap",
                                                            textOverflow:
                                                              "ellipsis",
                                                            maxWidth: "100%",
                                                          }}
                                                        >
                                                          <Link
                                                            to={source?.url}
                                                            target="_blank"
                                                          >
                                                            {source?.type ===
                                                            "link"
                                                              ? source?.url
                                                              : source?.name}
                                                          </Link>
                                                        </Box>
                                                      </Tooltip>
                                                    </Box>
                                                  </Card>
                                                </Grid>
                                              )
                                            )}
                                          </Grid>
                                        )}
                                      </Box>
                                    )}
                                  </Box>
                                </Box>
                              </Box>
                            </Box>
                          ))}
                      </Box>
                      <Pagination
                        count={Math.ceil(subjectQuestionsCount / 5)}
                        page={page}
                        onChange={handlePaginationChanged}
                        variant="outlined"
                        color="primary"
                      />
                    </Box>
                  ) : (
                    <Box sx={{ mt: 2 }}>
                      <Box sx={{ width: "100%", textAlign: "center" }}>
                        <img
                          src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
                          alt="Img"
                          style={{
                            marginBottom: "25px",
                            maxWidth: "200px",
                            verticalAlign: "middle",
                          }}
                        />
                        <Box>
                          <h3
                            style={{
                              fontSize: "1.125rem",
                              lineHeight: "1.2",
                              color: "#0078d4",
                              marginBottom: "10px",
                            }}
                          >
                            No subject questions now.
                          </h3>
                          <p style={{ margin: "0", fontSize: "20px" }}>
                            Please contact your school administration for more
                            information or add new question.
                          </p>
                        </Box>
                      </Box>
                    </Box>
                  )}
                </TabPanel>
              </TabContext>
            </Box>
          </Box>
        </Grid>
        <SubjectQuestionDialog
          subjectMyQuestion={subjectMyQuestion}
          subjectAllQuestion={subjectAllQuestion}
          type={type}
          selectedSubjectQuestion={selectedSubjectQuestion}
          openSubjectQuestionDialog={openSubjectQuestionDialog}
          handleCloseSubjectQuestionDialog={handleCloseSubjectQuestionDialog}
          addNewSupportQuestion={addNewSupportQuestion}
          addNewSupportQuestionIsSuccess={addNewSupportQuestionIsSuccess}
          addNewSupportQuestionIsLoading={addNewSupportQuestionIsLoading}
          addNewSupportQuestionIsError={addNewSupportQuestionIsError}
          addNewSupportQuestionError={addNewSupportQuestionError}
          updateSupportQuestion={updateSupportQuestion}
          updateSupportQuestionIsSuccess={updateSupportQuestionIsSuccess}
          fetchAllSubjectQuestions={fetchAllSubjectQuestions}
          page={page}
          setPage={setPage}
          valueTab={valueTab}
        />

        <Dialog
          fullWidth
          maxWidth={"sm"}
          open={openDeleteQuestionDialog}
          onClose={handleCloseDeleteQuestionDialog}
        >
          <DialogTitle>Delete Support Question</DialogTitle>
          <Divider />
          <DialogContent>
            <Typography variant="h6">
              You want to delete support question title:{" "}
              <span style={{ color: "#ff0000" }}>
                {selectedSubjectQuestion?.title}
              </span>
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseDeleteQuestionDialog}>Cancel</Button>
            <Button color="error" onClick={handleDeleteSupportQuestion}>
              Delete
            </Button>
          </DialogActions>
        </Dialog>

        <Backdrop
          open={openBackdrop}
          onClick={handleCloseBackdrop}
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Grid>
    </LocalizationProvider>
  );
};

export default SubjectQuestion;
