import { createEntityAdapter } from '@reduxjs/toolkit';
import { apiSlice } from '../../app/api/apiSlice';

const subjectsAdapter = createEntityAdapter({});

const initialState = subjectsAdapter.getInitialState();

export const subjectsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getSubjects: builder.query({
      query: () => ({
        url: '/subjects/allSubjects/all',
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
      providesTags: (result, error, arg) => {
        if (result?.ids) {
          return [
            { type: 'Subject', id: 'LIST' },
            ...result.ids.map((id) => ({ type: 'Subject', id })),
          ];
        } else return [{ type: 'Subject', id: 'LIST' }];
      },
    }),
    getSubjectsWithSearch: builder.query({
      query: (searchURL) => ({
        url: '/subjects' + searchURL,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        const loadedSubjects = responseData.subjects.map((data) => {
          data.id = data._id;
          return data;
        });
        return { ...responseData, subjects: loadedSubjects };
      },
      providesTags: (result, error, arg) => {
        if (result?.ids) {
          return [
            { type: 'Subject', id: 'LIST' },
            ...result.ids.map((id) => ({ type: 'Subject', id })),
          ];
        } else return [{ type: 'Subject', id: 'LIST' }];
      },
    }),
    getSubjectWithSubjectId: builder.query({
      query: (subjectId) => ({
        url: '/subjects/' + subjectId,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
    getSubjectsWithStatus: builder.query({
      query: (status) => ({
        url: '/subjects/' + status,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
    addNewSubject: builder.mutation({
      query: (initialSubjectData) => ({
        url: '/subjects',
        method: 'POST',
        body: {
          ...initialSubjectData,
        },
      }),
      invalidatesTags: [{ type: 'Subject', id: 'LIST' }],
    }),
    updateSubject: builder.mutation({
      query: (initialSubjectData) => ({
        url: '/subjects',
        method: 'PATCH',
        body: {
          ...initialSubjectData,
        },
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'Subject', id: arg.id },
      ],
    }),
    deleteSubject: builder.mutation({
      query: ({ id }) => ({
        url: `/subjects`,
        method: 'PATCH',
        body: { id },
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'Subject', id: arg.id },
      ],
    }),
    getUsersWithTrainerRole: builder.query({
      query: (roles) => ({
        url: '/subjects/' + roles,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
  }),
});

export const {
  useGetSubjectsQuery,
  useGetSubjectsWithStatusQuery,
  useGetSubjectsWithSearchQuery,
  useGetSubjectWithSubjectIdQuery,
  useAddNewSubjectMutation,
  useUpdateSubjectMutation,
  useDeleteSubjectMutation,
} = subjectsApiSlice;
