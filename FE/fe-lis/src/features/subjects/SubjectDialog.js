import SearchIcon from "@mui/icons-material/Search";
import {
  Box,
  Button,
  Grid,
  Pagination,
  Paper,
  Skeleton,
  Typography,
} from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import { useSnackbar } from "notistack";
import { React, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import IconTextField from "../../components/IconTextField";
import Title from "../../components/Title";
import { useGetSubjectsWithSearchQuery } from "./subjectsApiSlice";

const SubjectDialog = ({
  setSubject,
  setTrainer,
  handleCloseSubjectDialog,
}) => {
  const navigate = useNavigate();
  const searchURLDefault = "?page=1&status=active&keyword=";
  const { search } = useLocation();
  const { enqueueSnackbar } = useSnackbar();
  const [page, setPage] = useState(1);
  const [status, setStatus] = useState("active");
  const [keyword, setKeyword] = useState("");

  useEffect(() => {
    setKeyword(new URLSearchParams(search).get("keyword") || "");
    setStatus(new URLSearchParams(search).get("status") || "active");
    setPage(parseInt(new URLSearchParams(search).get("page")) || 1);
  }, [search]);

  const handleSearch = () => {
    let query = new URLSearchParams({
      page: 1,
      status: "active",
      keyword,
    }).toString();
    navigate("?" + query);
  };

  const handleKeywordChanged = (event) => setKeyword(event.target.value);

  const handlePaginationChanged = (event, value) => {
    let query = new URLSearchParams({
      page: value,
      status: status,
      keyword,
    }).toString();
    navigate("?" + query);
  };

  // Subjects Data
  const {
    data: subjectsData,
    isLoading: getSubjectsDataIsLoading,
    isSuccess: getSubjectsDataIsSuccess,
    isError: getSubjectsDataIsError,
    error: getSubjectDataError,
    refetch,
  } = useGetSubjectsWithSearchQuery(search === "" ? searchURLDefault : search, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const handleSelectedSubject = (subject) => {
    setSubject(subject);
    setTrainer();
    handleCloseSubjectDialog();
  };

  let content;

  if (getSubjectsDataIsLoading)
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (getSubjectsDataIsError) {
    content = (
      // <p className={getSubjectDataIsError ? 'errmsg' : 'offscreen'}>
      //   {getSubjectDataError?.data?.message}
      // </p>

      <Paper sx={{ p: 2 }}>
        <Box>
          <Box
            sx={{
              pt: 2,
              pb: 2,
              display: "flex",
              justifyContent: "space-between",
            }}
          ></Box>
          <Box sx={{ pb: 2 }}>
            <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
              <Grid
                container
                rowSpacing={1}
                columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                justifyContent={"flex-start"}
                alignItems={"center"}
                marginBottom={3}
              >
                <Grid item sm={12} md={6}>
                  <IconTextField
                    fullWidth
                    id="keyword"
                    label="Keyword"
                    name="keyword"
                    autoComplete="keyword"
                    placeholder="Search by subject code and name..."
                    autoFocus
                    iconEnd={<SearchIcon />}
                    value={keyword}
                    onChange={handleKeywordChanged}
                  />
                </Grid>
                <Grid item sm={12} md={3}>
                  <Button variant="contained" onClick={handleSearch}>
                    Search
                  </Button>
                </Grid>
              </Grid>
            </Paper>
          </Box>
          {/* end Filter */}
          <Grid container spacing={3}>
            {/* Recent Dash Board */}
            <Grid item xs={12}>
              <Typography variant="h4" color={"red"}>
                {getSubjectDataError.data?.message}
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Paper>
    );
  }
  if (getSubjectsDataIsSuccess) {
    const { subjects, subjectsCount } = subjectsData;
    const pages = Math.ceil(subjectsCount / 10);
    if (subjectsCount !== 0) {
      content = (
        <Paper sx={{ p: 2 }}>
          <Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "space-between",
              }}
            ></Box>
            <Box sx={{ pb: 2 }}>
              <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
                <Grid
                  container
                  rowSpacing={1}
                  columnSpacing={{ xs: 1, sm: 2, md: 3 }}
                  justifyContent={"flex-start"}
                  alignItems={"center"}
                  marginBottom={3}
                >
                  <Grid item sm={12} md={6}>
                    <IconTextField
                      fullWidth
                      id="keyword"
                      label="Keyword"
                      name="keyword"
                      autoComplete="keyword"
                      placeholder="Search by subject code and name..."
                      autoFocus
                      value={keyword}
                      iconEnd={<SearchIcon />}
                      onChange={handleKeywordChanged}
                    />
                  </Grid>
                  <Grid item sm={12} md={3}>
                    <Button variant="contained" onClick={handleSearch}>
                      Search
                    </Button>
                  </Grid>
                </Grid>
              </Paper>
            </Box>
            {/* end Filter */}
            <Grid container spacing={3}>
              {/* Recent Dash Board */}
              <Grid item xs={12}>
                <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
                  <Title>Subject Summary</Title>
                  <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell align="left" sx={{ color: "indianred" }}>
                          <TableSortLabel
                          // active={orderBy === 'id'}
                          // direction={order}
                          // onClick={handleSort('id')}
                          >
                            No.
                          </TableSortLabel>
                        </TableCell>
                        <TableCell
                          align="left"
                          sx={{ color: "indianred", width: "50%" }}
                        >
                          <TableSortLabel
                          // active={orderBy === 'subjectName'}
                          // direction={order}
                          // onClick={handleSort('subjectName')}
                          >
                            Name
                          </TableSortLabel>
                        </TableCell>
                        <TableCell align="left" sx={{ color: "indianred" }}>
                          <TableSortLabel
                          // active={orderBy === 'subjectCode'}
                          // direction={order}
                          // onClick={handleSort('subjectCode')}
                          >
                            Code
                          </TableSortLabel>
                        </TableCell>

                        <TableCell align="center" sx={{ color: "indianred" }}>
                          Lessons
                        </TableCell>
                        <TableCell align="center" sx={{ color: "indianred" }}>
                          Trainers
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {subjects?.map((subject, index) => (
                        <TableRow
                          onClick={() => handleSelectedSubject(subject)}
                          key={index}
                          sx={{
                            "&:last-child td, &:last-child th": {
                              borderBottom: "1px solid rgba(224, 224, 224, 1)",
                            },
                            "&:hover": {
                              backgroundColor: "#f5f5f5", // Change this to the desired hover color
                            },
                          }}
                        >
                          <TableCell align="left" component="th" scope="row">
                            {index + 1}
                          </TableCell>
                          <TableCell align="left">
                            {subject?.subjectName}
                          </TableCell>
                          <TableCell align="left">
                            {subject?.subjectCode}
                          </TableCell>

                          <TableCell align="center">
                            {subject?.lessons?.length}
                          </TableCell>
                          <TableCell align="center">
                            {subject?.trainers?.length}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>

                  <Pagination
                    sx={{ paddingTop: "18px" }}
                    count={pages}
                    page={page}
                    onChange={handlePaginationChanged}
                    variant="outlined"
                    color="primary"
                  />
                </Paper>
              </Grid>
            </Grid>
          </Box>
        </Paper>
      );
    }
  }

  return content;
};

export default SubjectDialog;
