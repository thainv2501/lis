import { React, useState, useEffect, useMemo } from 'react';
import Grid from '@mui/material/Grid';
import TextField from '@mui/material/TextField';
import Title from '../../components/Title';
import { styled } from '@mui/material/styles';
import {
  Button,
  Box,
  Paper,
  IconButton,
  Dialog,
  Typography,
  Pagination,
  DialogActions,
  DialogContent,
  Checkbox,
  DialogTitle,
  Tooltip,
  Divider,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import Switch from '@mui/material/Switch';
import FormControlLabel from '@mui/material/FormControlLabel';
import DeleteIcon from '@mui/icons-material/Delete';
import { Edit, Delete } from '@mui/icons-material';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useParams } from 'react-router-dom';
import {
  useGetSubjectWithSubjectIdQuery,
  useUpdateSubjectMutation,
} from './subjectsApiSlice';

import { useGetUsersWithRoleQuery } from '../users/usersApiSlice';

const IOSSwitch = styled((props) => (
  <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
  width: 42,
  height: 26,
  padding: 0,
  '& .MuiSwitch-switchBase': {
    padding: 0,
    margin: 2,
    transitionDuration: '300ms',
    '&.Mui-checked': {
      transform: 'translateX(16px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
        opacity: 1,
        border: 0,
      },
      '&.Mui-disabled + .MuiSwitch-track': {
        opacity: 0.5,
      },
    },
    '&.Mui-focusVisible .MuiSwitch-thumb': {
      color: '#33cf4d',
      border: '6px solid #fff',
    },
    '&.Mui-disabled .MuiSwitch-thumb': {
      color:
        theme.palette.mode === 'light'
          ? theme.palette.grey[100]
          : theme.palette.grey[600],
    },
    '&.Mui-disabled + .MuiSwitch-track': {
      opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
    },
  },
  '& .MuiSwitch-thumb': {
    boxSizing: 'border-box',
    width: 22,
    height: 22,
  },
  '& .MuiSwitch-track': {
    borderRadius: 26 / 2,
    backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
    opacity: 1,
    transition: theme.transitions.create(['background-color'], {
      duration: 500,
    }),
  },
}));
const rowsPerAddedPageMentor = 5;
const rowsPerRemainingPageMentor = 5;
const SubjectDetails = () => {
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const [canEdit, setCanEdit] = useState(false);
  const [canSave, setCanSave] = useState(false);
  const { subjectId } = useParams();
  const {
    data: subject,
    isSuccess: getSubjectIsSuccess,
    isLoading: getSubjectIsLoading,
    refetch,
  } = useGetSubjectWithSubjectIdQuery(subjectId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const [
    updateSubject,
    {
      isSuccess: updateSubjectIsSuccess,
      isError: updateSubjectIsError,
      error: updateSubjectError,
    },
  ] = useUpdateSubjectMutation();

  const [subjectCode, setSubjectCode] = useState('');
  const [subjectName, setSubjectName] = useState('');
  const [subjectDescription, setSubjectDescription] = useState('');
  const [createdDate, setCreatedDate] = useState('');
  const [chapterLimit, setChapterLimit] = useState(0);
  const [status, setStatus] = useState(true);
  const [trainers, setTrainers] = useState([]);
  const [trainersMore, setTrainersMore] = useState([]);
  const [lessons, setLessons] = useState();
  const [materials, setMaterials] = useState([]);
  const [openMentorsDialog, setOpenMentorsDialog] = useState(false);
  const [openMaterialsDialog, setOpenMaterialsDialog] = useState(false);
  const [selectedRemainingMentors, setSelectedRemainingMentors] = useState([]);
  const handleSubjectNameChange = (e) => {
    setSubjectName(e.target.value);
  };
  const handleSubjectDescriptionChange = (e) => {
    setSubjectDescription(e.target.value);
  };
  const handleChapterLimitChange = (e) => {
    setChapterLimit(e.target.value);
  };
  const handleStatusChange = (e) => {
    setStatus(e.target.checked);
  };
  const handleSaveClick = async () => {
    if (canSave) {
      try {
        await updateSubject({
          id: subjectId,
          subjectName: subjectName,
          subjectDescription: subjectDescription,
          chapterLimit: chapterLimit,
          status: status,
          trainers: trainersMore,
          // lessons: subject?.lessons,
          // material: subject?.materials,
        }).then(() => {
          enqueueSnackbar('Save the subject new information is successfully!', {
            variant: 'success',
          });
          refetch();
        });
        setCanEdit(false);
        setCanSave(false);
      } catch (err) {
        console.error('Failed to save the subject new information', err);
        enqueueSnackbar(
          'Failed to save the subject new information : ' +
            updateSubjectError?.data.message,
          {
            variant: 'error',
          }
        );
      }
    }
  };

  useEffect(() => {
    setCanSave(
      subjectName !== subject?.subjectName ||
        subjectDescription !== subject?.subjectDescription ||
        chapterLimit !== subject?.chapterLimit ||
        status !== subject?.status ||
        selectedRemainingMentors ||
        trainers
    );
  }, [
    subjectName,
    subjectDescription,
    chapterLimit,
    status,
    selectedRemainingMentors,
    trainers,
  ]);

  useEffect(() => {
    if (getSubjectIsSuccess) {
      setSubjectCode(subject?.subjectCode);
      setSubjectName(subject?.subjectName);
      setSubjectDescription(subject?.subjectDescription);
      setCreatedDate(subject?.createdDate);
      setChapterLimit(subject?.chapterLimit);
      setStatus(subject?.status);
      setTrainers(subject?.trainers);

      // setLessons(subject?.lessons);
      // setMaterials(subject?.materials);
    }
  }, [getSubjectIsSuccess]);

  // Sort and Paging table mentor
  const [orderBy, setOrderBy] = useState('');
  const [order, setOrder] = useState('asc');

  const handleSort = (property) => (event) => {
    const isDesc = orderBy === property && order === 'desc';
    setOrder(isDesc ? 'asc' : 'desc');
    setOrderBy(property);
  };

  const compareValues = (value1, value2, order) => {
    let result = 0;
    if (typeof value1 === 'number' && typeof value2 === 'number') {
      result = value1 - value2;
    } else {
      const str1 = String(value1).toLowerCase();
      const str2 = String(value2).toLowerCase();
      if (str1 < str2) {
        result = -1;
      } else if (str1 > str2) {
        result = 1;
      }
    }
    return order === 'desc' ? result * -1 : result;
  };
  const [addedPageMentor, setAddedPageMentor] = useState(1);
  const handleAddedPageMentorChange = (event, value) => {
    setAddedPageMentor(value);
  };
  const addedPageMentorCount = Math.ceil(
    trainers.length / rowsPerAddedPageMentor
  );
  // const addedPageMentorItems = trainers.slice(
  //   (addedPageMentor - 1) * rowsPerAddedPageMentor,
  //   addedPageMentor * rowsPerAddedPageMentor
  // );

  const visibleRows = useMemo(() => {
    const comparator = (a, b) => {
      return compareValues(a[orderBy], b[orderBy], order);
    };
    const trainersTemp = [...trainers];
    let trainersIndexField = [];
    trainersTemp.map((trainer, index) => {
      trainersIndexField.push({ ...trainer, index });
    });
    const sortedData = [...trainersIndexField].sort(comparator);

    const addedPageMentorItems = sortedData.slice(
      (addedPageMentor - 1) * rowsPerAddedPageMentor,
      addedPageMentor * rowsPerAddedPageMentor
    );

    return addedPageMentorItems;
  }, [trainers, orderBy, order, addedPageMentor, rowsPerAddedPageMentor]);
  //==========================================================================
  const handleEditClick = () => {
    setCanEdit(true);
  };

  const handleClickOpenMentorsDialog = () => {
    setOpenMentorsDialog(true);
  };

  const handleCloseMentorsDialog = () => {
    setOpenMentorsDialog(false);
    setSelectedRemainingMentors([]);
  };

  const handleClickOpenMaterialsDialog = () => {
    setOpenMaterialsDialog(true);
  };

  const handleCloseMaterialsDialog = () => {
    setOpenMaterialsDialog(false);
  };

  // Remaining Trainers Data
  const {
    data: trainersData,
    isLoading: getTrainersIsLoading,
    isSuccess: getTrainersIsSuccess,
    isError: getTrainersIsError,
    error: getTrainersError,
  } = useGetUsersWithRoleQuery('Trainer');

  const [remainingMentors, setRemainingMentors] = useState([]);
  useEffect(() => {
    if (getTrainersIsSuccess) {
      const remainingMentors = trainersData.filter(function (trainer) {
        return !trainers.some(function (mentor) {
          return mentor._id === trainer._id;
        });
      });
      setRemainingMentors(remainingMentors);
    }
  }, [getTrainersIsSuccess, trainers]);

  // Checkbox Handle
  const handleSelectAllRemainingMentors = (event) => {
    if (event.target.checked) {
      const allRemainingMentors = remainingMentors.map((mentor) => mentor);
      setSelectedRemainingMentors(allRemainingMentors);
    } else {
      setSelectedRemainingMentors([]);
    }
  };

  const handleSelectRemainingMentor = (event, selectingMentor) => {
    const selectedIndex = selectedRemainingMentors.findIndex(
      (mentor) => mentor?._id === selectingMentor?._id
    );
    let newSelectedRemainingMentors = [];

    if (selectedIndex === -1) {
      newSelectedRemainingMentors = [
        ...selectedRemainingMentors,
        selectingMentor,
      ];
    } else if (selectedIndex === 0) {
      newSelectedRemainingMentors = [...selectedRemainingMentors.slice(1)];
    } else if (selectedIndex === selectedRemainingMentors.length - 1) {
      newSelectedRemainingMentors = [...selectedRemainingMentors.slice(0, -1)];
    } else if (selectedIndex > 0) {
      newSelectedRemainingMentors = [
        ...selectedRemainingMentors.slice(0, selectedIndex),
        ...selectedRemainingMentors.slice(selectedIndex + 1),
      ];
    }

    setSelectedRemainingMentors(newSelectedRemainingMentors);
  };
  const handleAddMoreMentorToSubject = () => {
    const idsAddedMoreTrainers = [];
    // const newSelectedRemainingMentors = selectedRemainingMentors.map(
    //   (selectedRemainingMentor) => {
    //     delete selectedRemainingMentor.index;
    //     return { ...selectedRemainingMentor };
    //   }
    // );
    const trainersMore = [...trainers, ...selectedRemainingMentors];
    trainersMore.map((addedMoreTrainer) => {
      idsAddedMoreTrainers.push(addedMoreTrainer?._id);
    });
    setTrainers(trainersMore);
    setTrainersMore(idsAddedMoreTrainers);
    setOpenMentorsDialog(false);
    setSelectedRemainingMentors([]);
  };

  const [selectedMentor, setSelectedMentor] = useState(null);

  const [openDeleteMentorDialog, setOpenDeleteMentorDialog] = useState(false);

  const handleDeleteClicked = (mentor) => {
    setSelectedMentor(mentor);
    handleClickOpenDeleteMentorDialog();
  };
  const handleClickOpenDeleteMentorDialog = () => {
    setOpenDeleteMentorDialog(true);
  };

  const handleCloseDeleteMentorDialog = () => {
    setOpenDeleteMentorDialog(false);
  };

  const handleRemoveMentorFromSubject = () => {
    if (selectedMentor) {
      const mentorId = selectedMentor?._id;
      const index = trainers.findIndex((mentor) => mentor?._id === mentorId);
      if (index !== -1) {
        const updatedAddedMentors = [...trainers];
        updatedAddedMentors.splice(index, 1);
        setTrainers(updatedAddedMentors);
        const removedMentor = trainers.find(
          (mentor) => mentor?._id === mentorId
        );
        if (removedMentor) {
          const updatedAllMentors = [...remainingMentors];
          updatedAllMentors.push(removedMentor);
          setRemainingMentors(updatedAllMentors);

          enqueueSnackbar(
            `Remove ${selectedMentor?.email} from ${subjectCode} is successfully!`,
            {
              variant: 'success',
            }
          );
          handleCloseDeleteMentorDialog();
        }
      } else {
        enqueueSnackbar('No mentor selected!', {
          variant: 'warning',
        });
      }
    }

    if (visibleRows.length === 1 && addedPageMentor > 1) {
      setAddedPageMentor(addedPageMentor - 1);
    }
  };

  // Paging for Remaining Page Mentor
  const [orderByRemainingMentors, setOrderByRemainingMentors] = useState('');
  const [orderRemainingMentors, setOrderRemainingMentors] = useState('asc');
  const [remainingPageMentor, setRemainingPageMentor] = useState(1);
  const handleRemainingPageMentorChange = (event, value) => {
    setRemainingPageMentor(value);
  };
  const remainingPageMentorCount = Math.ceil(
    remainingMentors.length / rowsPerRemainingPageMentor
  );
  const handleSortRemainingMentors = (property) => (event) => {
    const isDesc =
      orderByRemainingMentors === property && orderRemainingMentors === 'desc';
    setOrderRemainingMentors(isDesc ? 'asc' : 'desc');
    setOrderByRemainingMentors(property);
  };
  // const remainingPageMentorItems = remainingMentors.slice(
  //   (remainingPageMentor - 1) * rowsPerRemainingPageMentor,
  //   remainingPageMentor * rowsPerRemainingPageMentor
  // );

  const visibleRowsRemaining = useMemo(() => {
    const comparator = (a, b) => {
      return compareValues(
        a[orderByRemainingMentors],
        b[orderByRemainingMentors],
        orderRemainingMentors
      );
    };
    const trainersTemp = [...remainingMentors];
    let remainingTrainersIndexField = [];
    trainersTemp.map((trainer, index) => {
      remainingTrainersIndexField.push({ ...trainer, index });
    });
    const sortedData = [...remainingTrainersIndexField].sort(comparator);

    const remainingPageMentorItems = sortedData.slice(
      (remainingPageMentor - 1) * rowsPerRemainingPageMentor,
      remainingPageMentor * rowsPerRemainingPageMentor
    );

    return remainingPageMentorItems;
  }, [
    remainingMentors,
    orderByRemainingMentors,
    orderRemainingMentors,
    remainingPageMentor,
    rowsPerRemainingPageMentor,
  ]);

  // Tab Lesson / Material / Mentor Handle
  const [valueTab, setValueTab] = useState('1');

  const handleChangeTab = (event, newValue) => {
    setValueTab(newValue);
  };

  return (
    <Paper
      sx={{
        mt: 3,
        p: 5,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        gap: 2,
      }}
    >
      <Grid
        justifyContent="space-between"
        alignItems="center"
        container
        spacing={3}
      >
        <Grid
          item
          xs={12}
          sm={6}
          container
          alignItems="center"
          justifyContent="flex-start"
        >
          <Title>General Information</Title>
        </Grid>
        <Grid item xs={12} sm={6} container justifyContent="flex-end">
          {canEdit ? (
            <Button
              variant="contained"
              onClick={handleSaveClick}
              color="secondary"
              disabled={!canSave}
            >
              Save
            </Button>
          ) : (
            <Button variant="contained" onClick={handleEditClick}>
              Edit !
            </Button>
          )}
        </Grid>
        <Grid item xs={6}>
          <TextField
            id="subjectCode"
            name="subjectCode"
            label="Subject Code"
            fullWidth
            autoComplete="given-name"
            disabled
            required
            value={subjectCode}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            id="subjectName"
            name="subjectName"
            label="Subject Name"
            fullWidth
            required
            disabled={!canEdit}
            value={subjectName}
            onChange={handleSubjectNameChange}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            fullWidth
            id="createdDate"
            name="createdDate"
            label="Created Date"
            autoFocus
            disabled
            value={createdDate}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            fullWidth
            autoFocus
            label="Chapter Limit"
            type="number"
            required
            InputProps={{
              inputProps: { min: 0, max: 50 },
            }}
            InputLabelProps={{ shrink: true }}
            disabled={!canEdit}
            value={chapterLimit}
            onChange={handleChapterLimitChange}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="subjectDescription"
            name="subjectDescription"
            label="Subject Description"
            fullWidth
            autoFocus
            disabled={!canEdit}
            multiline
            rows={3}
            maxRows={5}
            value={subjectDescription}
            onChange={handleSubjectDescriptionChange}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControlLabel
            control={
              <IOSSwitch
                sx={{ m: 1 }}
                checked={status}
                onChange={handleStatusChange}
                disabled={!canEdit}
              />
            }
            label={status ? 'Active' : 'Deactived'}
          />
        </Grid>
        <Grid item xs={12}>
          <Box sx={{ width: '100%', typography: 'body1' }}>
            <TabContext value={valueTab}>
              <Box>
                <TabList
                  onChange={handleChangeTab}
                  aria-label="lab API tabs example"
                >
                  <Tab label="Mentor" value="1" />
                  <Tab label="Material" value="2" />
                </TabList>
              </Box>
              <TabPanel sx={{ pr: 0, pl: 0 }} value="1">
                {/* Mentors display */}
                <Grid item xs={12}>
                  <Paper
                    sx={{
                      p: 2,
                      display: 'flex',
                      flexDirection: 'column',
                      gap: 2,
                    }}
                  >
                    <Grid
                      item
                      sm={12}
                      display={'flex'}
                      justifyContent={'space-between'}
                      alignItems={'center'}
                    >
                      <Title>Mentors Summary</Title>
                      {canEdit ? (
                        <IconButton aria-label="add-mentors" size="large">
                          <AddIcon
                            fontSize="inherit"
                            color="success"
                            onClick={handleClickOpenMentorsDialog}
                          />
                        </IconButton>
                      ) : null}
                    </Grid>
                    <Grid item xs={12} sx={{ width: '100%' }}>
                      {trainers.length > 0 ? (
                        <Paper
                          sx={{
                            p: 2,
                            display: 'flex',
                            flexDirection: 'column',
                          }}
                        >
                          <Table fullWidth>
                            <TableHead>
                              <TableRow>
                                <TableCell
                                  align="left"
                                  sx={{ color: 'indianred' }}
                                >
                                  <TableSortLabel
                                    active={orderBy === 'index'}
                                    direction={order}
                                    onClick={handleSort('index')}
                                  >
                                    No.
                                  </TableSortLabel>
                                </TableCell>
                                <TableCell
                                  align="left"
                                  sx={{ color: 'indianred' }}
                                >
                                  <TableSortLabel
                                    active={orderBy === 'fullName'}
                                    direction={order}
                                    onClick={handleSort('fullName')}
                                  >
                                    Name
                                  </TableSortLabel>
                                </TableCell>
                                <TableCell
                                  align="left"
                                  sx={{ color: 'indianred' }}
                                >
                                  <TableSortLabel
                                    active={orderBy === 'phoneNumber'}
                                    direction={order}
                                    onClick={handleSort('phoneNumber')}
                                  >
                                    Phone Number
                                  </TableSortLabel>
                                </TableCell>
                                <TableCell
                                  align="left"
                                  sx={{ color: 'indianred' }}
                                >
                                  <TableSortLabel
                                    active={orderBy === 'email'}
                                    direction={order}
                                    onClick={handleSort('email')}
                                  >
                                    Email
                                  </TableSortLabel>
                                </TableCell>
                                <TableCell
                                  align="left"
                                  sx={{ color: 'indianred' }}
                                >
                                  Actions
                                </TableCell>
                              </TableRow>
                            </TableHead>
                            <TableBody>
                              {visibleRows.map((mentor) => (
                                <TableRow
                                  key={mentor?.index + 1}
                                  sx={{
                                    '&:last-child td, &:last-child th': {
                                      borderBottom:
                                        '1px solid rgba(224, 224, 224, 1)',
                                    },
                                  }}
                                >
                                  <TableCell
                                    align="left"
                                    component="th"
                                    scope="row"
                                  >
                                    {mentor?.index + 1}
                                  </TableCell>
                                  <TableCell align="left">
                                    {mentor?.fullName}
                                  </TableCell>
                                  <TableCell align="left">
                                    {mentor?.phoneNumber}
                                  </TableCell>
                                  <TableCell align="left">
                                    {mentor?.email}
                                  </TableCell>

                                  <TableCell align="left">
                                    <Box display={'flex'}>
                                      <Tooltip
                                        title={`Remove from ${subjectCode}`}
                                        placement="top"
                                        arrow
                                      >
                                        <Button
                                          disabled={!canEdit}
                                          onClick={() =>
                                            handleDeleteClicked(mentor)
                                          }
                                        >
                                          <DeleteIcon />
                                        </Button>
                                      </Tooltip>
                                    </Box>
                                  </TableCell>
                                </TableRow>
                              ))}
                            </TableBody>
                          </Table>
                          <Pagination
                            sx={{ paddingTop: '18px' }}
                            count={addedPageMentorCount}
                            page={addedPageMentor}
                            onChange={handleAddedPageMentorChange}
                            variant="outlined"
                            color="primary"
                          />
                        </Paper>
                      ) : (
                        <Box sx={{ mt: 2 }}>
                          <Box sx={{ width: '100%', textAlign: 'center' }}>
                            <img
                              src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
                              alt="Img"
                              style={{
                                marginBottom: '25px',
                                maxWidth: '200px',
                                verticalAlign: 'middle',
                              }}
                            />
                            <Box>
                              <h3
                                style={{
                                  fontSize: '1.125rem',
                                  lineHeight: '1.2',
                                  color: '#0078d4',
                                  marginBottom: '10px',
                                }}
                              >
                                No mentors now.
                              </h3>
                              <p style={{ margin: '0', fontSize: '20px' }}>
                                Please add more information.
                              </p>
                            </Box>
                          </Box>
                        </Box>
                      )}
                    </Grid>
                  </Paper>
                </Grid>
              </TabPanel>
              <TabPanel sx={{ pr: 0, pl: 0 }} value="2">
                <Grid item xs={12}>
                  <Paper
                    sx={{
                      p: 2,
                      display: 'flex',
                      flexDirection: 'column',
                      gap: 2,
                    }}
                  >
                    <Grid item xs={12} sx={{ width: '100%' }}>
                      <Box sx={{ mt: 2 }}>
                        <Box sx={{ width: '100%', textAlign: 'center' }}>
                          <img
                            src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
                            alt="Img"
                            style={{
                              marginBottom: '25px',
                              maxWidth: '200px',
                              verticalAlign: 'middle',
                            }}
                          />
                          <Box>
                            <h3
                              style={{
                                fontSize: '1.125rem',
                                lineHeight: '1.2',
                                color: 'red',
                                marginBottom: '10px',
                              }}
                            >
                              This function is pending.
                            </h3>
                          </Box>
                        </Box>
                      </Box>
                    </Grid>
                  </Paper>
                </Grid>
              </TabPanel>
            </TabContext>
          </Box>
        </Grid>

        {/* Mentors dialog */}
        <Dialog
          open={openMentorsDialog}
          keepMounted
          onClose={handleCloseMentorsDialog}
          aria-describedby="mentors-dialog-slide-description"
          maxWidth="md"
        >
          <DialogTitle>Add Mentor</DialogTitle>
          <DialogContent sx={{ width: '100%' }}>
            <Grid item xs={12} sx={{ width: '100%' }}>
              {remainingMentors.length > 0 ? (
                <Paper
                  sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                  }}
                >
                  <Table fullWidth>
                    <TableHead>
                      <TableRow>
                        <TableCell padding="checkbox">
                          <Checkbox
                            indeterminate={
                              selectedRemainingMentors.length > 0 &&
                              selectedRemainingMentors.length <
                                remainingMentors.length
                            }
                            checked={
                              selectedRemainingMentors.length ===
                              remainingMentors.length
                            }
                            onChange={handleSelectAllRemainingMentors}
                          />
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          <TableSortLabel
                            active={orderByRemainingMentors === 'index'}
                            direction={orderRemainingMentors}
                            onClick={handleSortRemainingMentors('index')}
                          >
                            No.
                          </TableSortLabel>
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          <TableSortLabel
                            active={orderByRemainingMentors === 'fullName'}
                            direction={orderRemainingMentors}
                            onClick={handleSortRemainingMentors('fullName')}
                          >
                            Name
                          </TableSortLabel>
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          <TableSortLabel
                            active={orderByRemainingMentors === 'phoneNumber'}
                            direction={orderRemainingMentors}
                            onClick={handleSortRemainingMentors('phoneNumber')}
                          >
                            Phone Number
                          </TableSortLabel>
                        </TableCell>
                        <TableCell align="left" sx={{ color: 'indianred' }}>
                          <TableSortLabel
                            active={orderByRemainingMentors === 'email'}
                            direction={orderRemainingMentors}
                            onClick={handleSortRemainingMentors('email')}
                          >
                            Email
                          </TableSortLabel>
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {visibleRowsRemaining.map((mentor) => (
                        <TableRow
                          key={mentor?.index + 1}
                          sx={{
                            '&:last-child td, &:last-child th': {
                              borderBottom: '1px solid rgba(224, 224, 224, 1)',
                            },
                          }}
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              checked={
                                selectedRemainingMentors.findIndex(
                                  (m) => m?._id === mentor?._id
                                ) !== -1
                              }
                              onChange={(event) =>
                                handleSelectRemainingMentor(event, mentor)
                              }
                            />
                          </TableCell>
                          <TableCell align="left" component="th" scope="row">
                            {mentor?.index + 1}
                          </TableCell>
                          <TableCell align="left">{mentor?.fullName}</TableCell>
                          <TableCell align="left">
                            {mentor?.phoneNumber}
                          </TableCell>
                          <TableCell align="left">{mentor?.email}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                  <Pagination
                    sx={{ paddingTop: '18px' }}
                    count={remainingPageMentorCount}
                    page={remainingPageMentor}
                    onChange={handleRemainingPageMentorChange}
                    variant="outlined"
                    color="primary"
                  />
                </Paper>
              ) : (
                <p>No result found.</p>
              )}
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseMentorsDialog}>Cancel</Button>
            <Button onClick={handleAddMoreMentorToSubject} color="primary">
              Add mentor
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          fullWidth
          maxWidth={'sm'}
          open={openDeleteMentorDialog}
          onClose={handleCloseDeleteMentorDialog}
        >
          <DialogTitle>Remove Mentor</DialogTitle>
          <Divider />
          <DialogContent>
            <Typography
              variant="h6"
              sx={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                width: '100%',
                textAlign: 'center',
              }}
            >
              Do you really want to remove this mentor?{' '}
              <span style={{ textAlign: 'center', color: '#ff0000' }}>
                {selectedMentor?.fullName}
              </span>
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseDeleteMentorDialog}>Cancel</Button>
            <Button color="error" onClick={handleRemoveMentorFromSubject}>
              Remove
            </Button>
          </DialogActions>
        </Dialog>
      </Grid>
    </Paper>
  );
};

export default SubjectDetails;
