import {
  Box,
  Button,
  CircularProgress,
  Stack,
  Typography,
} from "@mui/material";
import { React } from "react";
import { Link } from "react-router-dom";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import gmailPNG from "../../images/—Pngtree—vector email icon_3987423.png";

const RegisterSuccessfulNotification = () => {
  let content = (
    <Box
      sx={{
        minHeight: "100vh",
        background: "linear-gradient(to right bottom, #430089, #82ffa1)",
      }}
      display={"flex"}
      flexDirection={"column"}
      justifyContent={"center"}
      justifyItems={"center"}
      alignContent={"center"}
      alignItems={"center"}
    >
      <Box
        sx={{ minWidth: 500, boxShadow: 3, p: 3, borderRadius: 2 }}
        display={"flex"}
        gap={3}
        flexDirection={"column"}
        justifyContent={"center"}
        justifyItems={"center"}
        alignContent={"center"}
        alignItems={"center"}
      >
        <Box
          component="img"
          sx={{
            height: 266,
            width: 350,
            maxHeight: { xs: 233, md: 200 },
            maxWidth: { xs: 350, md: 250 },
          }}
          src={gmailPNG}
        />

        <Typography variant="h4" component="div">
          {"REGISTER SUCCESSFUL !"}
        </Typography>

        {<CheckCircleOutlineIcon fontSize="large" color="success" />}
        <Typography variant="h6" component="div">
          {"We sent an verify to your email !"}
        </Typography>
        <Typography variant="h6" component="div">
          {"Please check your email and confirm !"}
        </Typography>
        <Link to={"/login"}>
          <Button size="small">Login</Button>
        </Link>
      </Box>
    </Box>
  );

  return content;
};

export default RegisterSuccessfulNotification;
