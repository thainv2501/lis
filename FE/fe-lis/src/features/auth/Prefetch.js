import { store } from "../../app/store";
import { masterDataApiSlice } from "../masterData/masterDataApiSlice";
import { useEffect } from "react";
import { Outlet } from "react-router-dom";

const Prefetch = () => {
  useEffect(() => {
    store.dispatch(
      masterDataApiSlice.util.prefetch("getMasterDataWithType", "Semester", {
        force: true,
      })
    );
  }, []);

  return <Outlet />;
};
export default Prefetch;
