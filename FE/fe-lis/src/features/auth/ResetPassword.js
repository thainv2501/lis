import { React, useState, useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import HttpsOutlinedIcon from "@mui/icons-material/HttpsOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useNavigate, useSearchParams } from "react-router-dom";
import IconTextField from "../../components/IconTextField";
import { useSnackbar } from "notistack";
import {
  useResetPasswordMutation,
  useVerifyEmailMutation,
} from "./authApiSlice";

const PWD_REGEX = /^[A-z0-9!@#$%]{4,12}$/;

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const defaultTheme = createTheme();
const ResetPassword = () => {
  const [
    resetPassword,
    { isSuccess, isError: updateIsError, error: updateError },
  ] = useResetPasswordMutation();
  const [verifyEmail, { isLoading, isError, error }] = useVerifyEmailMutation();
  const [searchParams, setSearchParams] = useSearchParams();

  const email = searchParams.get("email");
  const token = searchParams.get("token");

  useEffect(() => {
    verifyEmail({ email, token });
  }, []);

  const { enqueueSnackbar } = useSnackbar();

  const navigate = useNavigate();

  const [password, setPassword] = useState("");
  const [validPassword, setValidPassword] = useState(false);
  const [checkPassword, setCheckPassword] = useState("");
  const [validCheckPassword, setValidCheckPassword] = useState(false);

  useEffect(() => {
    setValidPassword(PWD_REGEX.test(password));
  }, [password]);
  useEffect(() => {
    setValidCheckPassword(password === checkPassword);
  }, [checkPassword, password]);

  useEffect(() => {
    if (isError) {
      enqueueSnackbar(error?.data?.message, { variant: "error" });
    }
  }, [isError, error]);

  useEffect(() => {
    if (updateIsError) {
      enqueueSnackbar(updateError?.data?.message, { variant: "error" });
    }
  }, [updateIsError, updateError]);

  const onPasswordChanged = (e) => setPassword(e.target.value);
  const onCheckPasswordChanged = (e) => setCheckPassword(e.target.value);

  const canResetPassword =
    [validPassword].every(Boolean) && !isLoading && !isError;

  const onRegisterClicked = async (e) => {
    e.preventDefault();
    if ((canResetPassword, validCheckPassword)) {
      await resetPassword({ email, password });
    } else {
      enqueueSnackbar("Recheck your password again ! password do not match !", {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (isSuccess) {
      setPassword("");
      navigate("/login");
    }
  }, [isSuccess, navigate]);

  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          {isLoading ? (
            <Typography component="h1" variant="h5">
              Verify Email loading !
            </Typography>
          ) : null}
          <Typography component="h1" variant="h5">
            Reset Password
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={onRegisterClicked}
            sx={{ mt: 3 }}
          >
            <Grid item xs={12}>
              <IconTextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                iconEnd={<HttpsOutlinedIcon />}
                onChange={onPasswordChanged}
              />
            </Grid>
            <Grid item xs={12}>
              <IconTextField
                margin="normal"
                required
                fullWidth
                name="check-password"
                label="Check Password"
                type="password"
                id="check-password"
                iconEnd={<HttpsOutlinedIcon />}
                onChange={onCheckPasswordChanged}
              />
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              disabled={!canResetPassword}
            >
              Reset password
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link to={"/login"} variant="body2">
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />

        {/* snackBar */}
      </Container>
    </ThemeProvider>
  );
};

export default ResetPassword;
