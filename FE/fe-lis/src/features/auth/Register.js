import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import HttpsOutlinedIcon from "@mui/icons-material/HttpsOutlined";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { useSnackbar } from "notistack";
import { React, useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import IconTextField from "../../components/IconTextField";
import { useRegisterMutation } from "../auth/authApiSlice";

const EMAIL_REGEX = /(|^)[\w\d._%+-]+@(?:[\w\d-]+\.)+(\w{2,})(|$)/i;
const PWD_REGEX = /^[A-z0-9!@#$%]{4,12}$/;
const FULL_NAME_REGEX =
  /^[ A-Za-z0-9À-Ỹà-ỹĂ-Ắă-ằẤ-Ứấ-ứÂ-Ấâ-ấĨ-Ỹĩ-ỹĐđÊ-Ểê-ểÔ-Ốô-ốơ-ởƠ-Ớơ-ớƯ-Ứư-ứỲ-Ỵỳ-ỵ\s]+$/;
const PHONE_NUMBER_REGEX = /^[0-9]{10}$/;

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const defaultTheme = createTheme();

export const Register = () => {
  const { enqueueSnackbar } = useSnackbar();

  const [register, { isLoading, isSuccess, isError, error }] =
    useRegisterMutation();

  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [validEmail, setValidEmail] = useState(false);
  const [fullName, setFullName] = useState("");
  const [validFullName, setValidFullName] = useState(false);
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [validPhoneNumber, setValidPhoneNumber] = useState(false);
  const [validPassword, setValidPassword] = useState(false);

  useEffect(() => {
    setValidFullName(FULL_NAME_REGEX.test(fullName) && fullName.trim() !== "");
  }, [fullName]);

  useEffect(() => {
    setValidEmail(EMAIL_REGEX.test(email));
  }, [email]);

  useEffect(() => {
    setValidPassword(PWD_REGEX.test(password));
  }, [password]);

  useEffect(() => {
    setValidPhoneNumber(PHONE_NUMBER_REGEX.test(phoneNumber));
  }, [phoneNumber]);

  useEffect(() => {
    if (isError) {
      enqueueSnackbar(error?.data?.message, { variant: "error" });
    }
  }, [isError]);

  const onEmailChanged = (e) => setEmail(e.target.value);
  const onPasswordChanged = (e) => setPassword(e.target.value);
  const onRePasswordChanged = (e) => setRePassword(e.target.value);
  const onFullNameChanged = (e) => setFullName(e.target.value);
  const onPhoneNumberChanged = (e) => setPhoneNumber(e.target.value);

  const canRegister =
    [validEmail, validPassword, validFullName, validPhoneNumber].every(
      Boolean
    ) && !isLoading;

  const onRegisterClicked = async (e) => {
    e.preventDefault();
    if (canRegister) {
      if (rePassword === password) {
        await register({
          fullName: fullName,
          phoneNumber,
          email,
          password,
        });
      } else {
        enqueueSnackbar("Password is not match ! Please check !", {
          variant: "error",
        });
      }
    }
  };

  useEffect(() => {
    if (isSuccess) {
      setEmail("");
      setPassword("");
      navigate("/registerSuccessful");
    }
  }, [isSuccess, navigate]);


  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={onRegisterClicked}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  required
                  id="full-name"
                  label="Full Name"
                  name="fullName"
                  helperText={validFullName ? "" : "Please enter your name"}
                  onChange={onFullNameChanged}
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <TextField
                  fullWidth
                  required
                  id="phone-number"
                  label="Phone number"
                  name="phoneNumber"
                  helperText={
                    validPhoneNumber ? "" : "Please enter your phone number"
                  }
                  inputProps={{ maxLength: 10 }}
                  onChange={onPhoneNumberChanged}
                />
              </Grid>
              <Grid item xs={12}>
                <IconTextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email"
                  name="email"
                  autoComplete="email"
                  helperText={validEmail ? "" : "please enter your email"}
                  iconEnd={<AccountCircleOutlinedIcon />}
                  onChange={onEmailChanged}
                />
              </Grid>
              <Grid item xs={12}>
                <IconTextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  helperText={validPassword ? "" : "Please enter your password"}
                  iconEnd={<HttpsOutlinedIcon />}
                  onChange={onPasswordChanged}
                />
              </Grid>
              <Grid item xs={12}>
                <IconTextField
                  margin="normal"
                  required
                  fullWidth
                  name="re-password"
                  label="Re-Password"
                  type="password"
                  id="re-password"
                  iconEnd={<HttpsOutlinedIcon />}
                  onChange={onRePasswordChanged}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              disabled={!canRegister}
            >
              Sign Up
            </Button>
            <Grid container justifyContent="flex-end">
              <Link to={"/login"} variant="body2">
                <Button>Already have an account? Sign in</Button>
              </Link>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
};
