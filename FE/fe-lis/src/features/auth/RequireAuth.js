import { useLocation, Navigate, Outlet } from "react-router-dom";
import useAuth from "../../hooks/useAuth";

const RequireAuth = ({ allowedRoles }) => {
  const location = useLocation();
  const { userLogin } = useAuth();

  const content = userLogin?.roles.some((role) =>
    allowedRoles.includes(role)
  ) ? (
    userLogin.roles.every((role) => role === "Guest") ? (
      <Navigate to="/waitingPage" state={{ from: location }} replace />
    ) : (
      <Outlet />
    )
  ) : (
    <Navigate to="/login" state={{ from: location }} replace />
  );
  return content;
};
export default RequireAuth;
