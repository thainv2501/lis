import { React, useState, useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useNavigate } from "react-router-dom";
import IconTextField from "../../components/IconTextField";
import { useSnackbar } from "notistack";
import { useSendResetPasswordMutation } from "./authApiSlice";

const EMAIL_REGEX = /(|^)[\w\d._%+-]+@(?:[\w\d-]+\.)+(\w{2,})(|$)/i;

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link color="inherit" href="https://mui.com/">
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const defaultTheme = createTheme();
const ForgotPassword = () => {
  const { enqueueSnackbar } = useSnackbar();

  const [sendResetPassword, { isLoading, isSuccess, isError, error }] =
    useSendResetPasswordMutation();

  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [validEmail, setValidEmail] = useState(false);

  useEffect(() => {
    setValidEmail(EMAIL_REGEX.test(email));
  }, [email]);

  useEffect(() => {
    if (isError) {
      enqueueSnackbar(error?.data?.message, { variant: "error" });
    }
  }, [isError]);

  const onEmailChanged = (e) => setEmail(e.target.value);

  const canSendResetPassword = [validEmail].every(Boolean) && !isLoading;

  const onRegisterClicked = async (e) => {
    e.preventDefault();
    if (canSendResetPassword) {
      await sendResetPassword({ email });
    }
  };

  useEffect(() => {
    if (isSuccess) {
      setEmail("");
      navigate("/login");
    }
  }, [isSuccess, navigate]);

  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Forgot Password
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={onRegisterClicked}
            sx={{ mt: 3 }}
          >
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <IconTextField
                  margin="normal"
                  required
                  fullWidth
                  id="email"
                  label="Email"
                  name="email"
                  autoComplete="email"
                  iconEnd={<AccountCircleOutlinedIcon />}
                  onChange={onEmailChanged}
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              disabled={!canSendResetPassword}
            >
              Reset password
            </Button>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />

        {/* snackBar */}
      </Container>
    </ThemeProvider>
  );
};

export default ForgotPassword;
