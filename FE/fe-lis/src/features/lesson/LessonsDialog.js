import {
  Button,
  Checkbox,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
} from "@mui/material";
import { Box } from "@mui/system";
import { enqueueSnackbar } from "notistack";
import React, { useEffect, useMemo, useState } from "react";
import { useParams } from "react-router-dom";

const LessonsDialog = ({
  lessons,
  handleCloseAddLessonsDialog,
  openLessonsDialog,
  handleScheduleObjChange,
  scheduleLessons,
  updateSchedule,
}) => {
  const { scheduleId } = useParams();
  // Use the correct name for the setter function in useState
  const [selectedLessons, setSelectedLessons] = useState([]);

  const canSave = selectedLessons.length !== 0;

  const handleLessonCheckClicked = (event, data) => {
    if (event.target.checked) {
      setSelectedLessons([...selectedLessons, data]);
    } else {
      const newSelectedLessons = selectedLessons.filter((old) => old !== data);
      setSelectedLessons(newSelectedLessons);
    }
  };

  const handleSave = async () => {
    try {
      if (scheduleId) {
        updateSchedule({
          scheduleId: scheduleId,
          lessons: selectedLessons,
        });
      } else {
        handleScheduleObjChange("lessons", selectedLessons);
      }
    } catch (error) {
      enqueueSnackbar("Error with handleUpdate Function !", {
        variant: "error",
      });
    }
    handleCloseAddLessonsDialog();
  };

  return (
    <Dialog
      fullWidth
      maxWidth={"md"}
      open={openLessonsDialog}
      onClose={handleCloseAddLessonsDialog}
    >
      <DialogTitle>Select Lessons</DialogTitle>
      <DialogContent>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell sx={{ color: "indianred" }}>Select</TableCell>
              <TableCell sx={{ color: "indianred" }}>No.</TableCell>
              <TableCell sx={{ color: "indianred" }}>Chapter</TableCell>
              <TableCell sx={{ color: "indianred", width: "60%" }}>
                Content
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {lessons &&
              lessons
                .filter((lesson) => lesson?.status === true)
                .map((data, index) => (
                  <TableRow key={data?._id} hover>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      <Checkbox
                        checked={selectedLessons.includes(data)}
                        onChange={(event) =>
                          handleLessonCheckClicked(event, data)
                        }
                      />
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      {index + 1}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      Chapter {data?.lessonChapter}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      Lesson {data?.lessonNumber} : {data?.lessonContent}
                    </TableCell>
                  </TableRow>
                ))}
          </TableBody>
        </Table>
      </DialogContent>
      <DialogActions>
        <Button color="error" onClick={handleCloseAddLessonsDialog}>
          Cancel
        </Button>
        <Button color="success" disabled={!canSave} onClick={handleSave}>
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default LessonsDialog;
