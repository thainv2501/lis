import EditIcon from "@mui/icons-material/Edit";
import SearchIcon from "@mui/icons-material/Search";
import {
  Box,
  Button,
  Chip,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Pagination,
  Paper,
  Tooltip,
  Select,
  TextField,
  Typography,
} from "@mui/material";
import Skeleton from "@mui/material/Skeleton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import { useSnackbar } from "notistack";
import { React, useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import IconTextField from "../../components/IconTextField";
import Title from "../../components/Title";
import {
  useAddNewLessonMutation,
  useGetLessonWithSearchQuery,
  useUpdateLessonMutation,
} from "./lessonApiSlice";
import { useGetSubjectWithSubjectIdQuery } from "../subjects/subjectsApiSlice";
import useAuth from "../../hooks/useAuth";
import ImportXLSXLessonsDialog from "./ImportXLSXLessonsDialog";

const statuses = [
  { name: "All", value: "all" },
  { name: "Active", value: "active" },
  { name: "Deactived", value: "deactived" },
];
const LessonList = () => {
  const { userLogin } = useAuth();
  const { subjectId } = useParams();
  const navigate = useNavigate();
  const searchURLDefault = "?page=1&status=all&keyword=";
  const { search } = useLocation();
  const { enqueueSnackbar } = useSnackbar();
  const [page, setPage] = useState(1);
  const [status, setStatus] = useState("all");
  const [keyword, setKeyword] = useState("");
  const [subjectName, setSubjectName] = useState();

  const [selectedLesson, setSelectedLesson] = useState();
  const [openStatusDialog, setOpenStatusDialog] = useState(false);

  useEffect(() => {
    setKeyword(new URLSearchParams(search).get("keyword") || "");
    setStatus(new URLSearchParams(search).get("status") || "all");
    setPage(parseInt(new URLSearchParams(search).get("page")) || 1);
  }, [search]);

  const handleSearch = () => {
    let query = new URLSearchParams({
      page: 1,
      status: status,
      keyword,
    }).toString();
    navigate("?" + query);
  };

  const handleKeywordChanged = (event) => setKeyword(event.target.value);
  const handleStatusFilterChange = (event) => {
    setStatus(event.target.value);
  };
  const handlePaginationChanged = (event, value) => {
    let query = new URLSearchParams({
      page: value,
      status: status,
      keyword,
    }).toString();
    navigate("?" + query);
  };

  // subject Data
  const {
    data: subjectData,
    isLoading: getSubjectDataIsLoading,
    isSuccess: getSubjectDataIsSuccess,
    isError: getSubjectDataIsError,
    error: getSubjectDataError,
  } = useGetSubjectWithSubjectIdQuery(subjectId);

  useEffect(() => {
    if (getSubjectDataIsSuccess) {
      setSubjectName(subjectData?.subjectName);
      let chapterArray = [];
      for (let index = 1; index <= subjectData?.chapterLimit; index++) {
        chapterArray.push(index);
      }
      setChapterLimitArray(chapterArray);
    }
  }, [getSubjectDataIsSuccess]);

  // Lessons Data
  const {
    data: lessonsData,
    isLoading: getLessonsDataIsLoading,
    isSuccess: getLessonsDataIsSuccess,
    isError: getLessonsDataIsError,
    error: getLessonDataError,
    refetch,
  } = useGetLessonWithSearchQuery(
    subjectId + (search === "" ? searchURLDefault : search)
  );

  const [
    updateLesson,
    {
      isSuccess: updateLessonIsSuccess,
      isError: updateLessonIsError,
      error: updateLessonError,
    },
  ] = useUpdateLessonMutation();

  const handleOpenStatusDialog = (lesson) => {
    setSelectedLesson(lesson);
    setOpenStatusDialog(true);
  };

  const handleCloseStatusDialog = () => {
    setOpenStatusDialog(false);
  };

  const handleChangeStatus = async (lesson) => {
    try {
      await updateLesson({
        lessonId: lesson.id,
        status: !lesson.status,
      });
    } catch (err) {
      console.error("Failed to save the lesson new information", err);
      enqueueSnackbar("Failed to save the lesson new information: " + err, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (updateLessonIsSuccess) {
      enqueueSnackbar("Change status is successfully!", {
        variant: "success",
      });
      setOpenStatusDialog(false);
      refetch();
    }
    if (updateLessonIsError) {
      enqueueSnackbar(
        "Failed to save the lesson new information: " +
          updateLessonError?.data.message,
        {
          variant: "error",
        }
      );
    }
  }, [updateLessonIsError, updateLessonIsSuccess]);

  const [chapterLimitArray, setChapterLimitArray] = useState([1]);
  const [lessonNumber, setLessonNumber] = useState(1);
  const [lessonNumberError, setLessonNumberError] = useState(false);
  const [openImportXLSXLessonsDialog, setOpenImportXLSXLessonsDialog] =
    useState(false);
  const [lessonNumberHelperText, setLessonNumberHelperText] = useState("");
  const [lessonChapter, setLessonChapter] = useState("");
  const [lessonContent, setLessonContent] = useState("");

  const [
    createNewLesson,
    {
      isSuccess: createLessonIsSuccess,
      isError: createLessonIsError,
      error: createLessonError,
    },
  ] = useAddNewLessonMutation();

  const handleAddNewLesson = async () => {
    try {
      await createNewLesson({
        subject: subjectId,
        lessonChapter,
        lessonNumber,
        lessonContent,
        createdBy: userLogin?._id,
      });
    } catch (error) {
      enqueueSnackbar("Create failed!: " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (createLessonIsSuccess) {
      enqueueSnackbar("Create new lesson successfully!", {
        variant: "success",
      });
      handleCloseDialogAddLesson();
    }
    if (createLessonIsError) {
      enqueueSnackbar("Create failed!: " + createLessonError?.data?.message, {
        variant: "error",
      });
    }
  }, [createLessonIsError, createLessonIsSuccess]);

  const [openDialogAddLesson, setOpenDialogAddLesson] = useState(false);

  const handleClickOpenDialogAddLesson = () => {
    setOpenDialogAddLesson(true);
  };

  const handleCloseDialogAddLesson = () => {
    setOpenDialogAddLesson(false);
  };

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClickActionViewLesson = (lessonId) => {
    navigate(`/dash-board/lesson-details/${lessonId}`);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLessonContentChange = (event) => {
    setLessonContent(event.target.value);
  };

  const handleChapterChange = (event) => {
    setLessonChapter(event.target.value);
  };

  const handleLessonNumberChange = (e) => {
    let value = parseInt(e.target.value);
    if (isNaN(value) || value > 50) {
      setLessonNumberError(true);
      setLessonNumberHelperText("The maximum limit is 50!");
      value = 1;
    }
    if (value < 1) {
      setLessonNumberError(true);
      setLessonNumberHelperText("The minimum limit is 1!");
      value = 1;
    }

    if (value > 1 && value < 50) {
      setLessonNumberError(false);
      setLessonNumberHelperText("");
    }
    setLessonNumber(value);
  };

  let content;

  if (getLessonsDataIsLoading)
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );
  let tableContent = null;

  if (getLessonsDataIsError) {
    tableContent = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Typography variant="h5" color={"red"}>
            {getLessonDataError?.data?.message}
          </Typography>
        </Grid>
      </Grid>
    );
  }

  if (getLessonsDataIsSuccess) {
    const { lessons, lessonsCount } = lessonsData;
    const pages = Math.ceil(lessonsCount / 10);
    tableContent = (
      <>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="left" sx={{ color: "indianred" }}>
                <TableSortLabel>No.</TableSortLabel>
              </TableCell>

              <TableCell align="left" sx={{ color: "indianred", width: "50%" }}>
                Content
              </TableCell>
              <TableCell align="left" sx={{ color: "indianred" }}>
                Created By
              </TableCell>
              <TableCell align="center" sx={{ color: "indianred" }}>
                Status
              </TableCell>
              <TableCell align="left" sx={{ color: "indianred" }}>
                Actions
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {lessons?.map((lesson, index) => (
              <TableRow
                key={index}
                sx={{
                  "&:last-child td, &:last-child th": {
                    borderBottom: "1px solid rgba(224, 224, 224, 1)",
                  },
                }}
                hover
              >
                <TableCell align="left" component="th" scope="row">
                  {(page - 1) * 10 + index + 1}
                </TableCell>

                <TableCell align="left">
                  <Typography>
                    Chapter {lesson?.lessonChapter} - Lesson{" "}
                    {lesson?.lessonNumber}{" "}
                  </Typography>
                  <Typography>{lesson?.lessonContent}</Typography>
                </TableCell>
                <TableCell align="left">
                  {lesson?.createdBy?.fullName}
                </TableCell>
                <TableCell align="center">
                  <Button onClick={() => handleOpenStatusDialog(lesson)}>
                    {lesson?.status ? (
                      <Chip label="Active" color="success" variant="outlined" />
                    ) : (
                      <Chip
                        label="Deactived"
                        color="error"
                        variant="outlined"
                      />
                    )}
                  </Button>
                </TableCell>
                <TableCell>
                  <Box display={"flex"}>
                    <Tooltip title="View Details" placement="top" arrow>
                      <Button
                        color="success"
                        onClick={() => handleClickActionViewLesson(lesson?.id)}
                      >
                        <EditIcon />
                      </Button>
                    </Tooltip>
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <Pagination
          sx={{ paddingTop: "18px" }}
          count={pages}
          page={page}
          onChange={handlePaginationChanged}
          variant="outlined"
          color="primary"
        />
      </>
    );
  }

  content = (
    <Paper sx={{ p: 2 }}>
      <Box>
        <Box
          sx={{
            pt: 2,
            pb: 2,
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Title>
            {subjectData?.subjectCode} - {subjectData?.subjectName} Lessons
          </Title>
          <Grid display={"flex"} gap={3}>
            <Button
              variant="contained"
              disabled={subjectName === undefined}
              color="success"
              onClick={() => setOpenImportXLSXLessonsDialog(true)}
            >
              import from xlsx
            </Button>
            <ImportXLSXLessonsDialog
              subject={subjectData}
              createdBy={userLogin?._id}
              openImportXLSXLessonsDialog={openImportXLSXLessonsDialog}
              setOpenImportXLSXLessonsDialog={setOpenImportXLSXLessonsDialog}
              enqueueSnackbar={enqueueSnackbar}
              refetch={refetch}
            />
            <Button
              variant="contained"
              onClick={handleClickOpenDialogAddLesson}
              disabled={subjectName === undefined}
            >
              Create New
            </Button>
          </Grid>
          <Dialog
            open={openDialogAddLesson}
            onClose={handleCloseDialogAddLesson}
            fullWidth
            maxWidth={"md"}
          >
            <DialogTitle>Add New Lesson</DialogTitle>
            <DialogContent sx={{ p: 3 }}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={3}>
                  <FormControl fullWidth>
                    <InputLabel id="chapter-select-label" required>
                      Chapter
                    </InputLabel>
                    <Select
                      id="chapter-select"
                      label="Chapter"
                      onChange={handleChapterChange}
                      value={lessonChapter}
                    >
                      {chapterLimitArray?.map((chapter, index) => (
                        <MenuItem key={index} value={chapter}>
                          Chapter {chapter}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} md={12}>
                  <TextField
                    margin="dense"
                    autoFocus
                    label="Lesson Number"
                    type="number"
                    required
                    error={lessonNumberError}
                    helperText={lessonNumberHelperText}
                    InputProps={{
                      inputProps: { maxLength: 2 },
                    }}
                    value={lessonNumber}
                    onChange={handleLessonNumberChange}
                  />
                </Grid>
                <Grid item xs={12} md={12}>
                  <TextField
                    id="lesson-content"
                    name="lesson-content"
                    label="Lesson Content"
                    multiline
                    rows={4}
                    fullWidth
                    value={lessonContent}
                    onChange={handleLessonContentChange}
                  />
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseDialogAddLesson}>Cancel</Button>
              <Button
                onClick={handleAddNewLesson}
                variant="contained"
                autoFocus
              >
                Save
              </Button>
            </DialogActions>
          </Dialog>
        </Box>
        <Box sx={{ pb: 2 }}>
          <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
            <Title>What are you looking for?</Title>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 2, md: 3 }}
              justifyContent={"flex-start"}
              alignItems={"center"}
              marginBottom={3}
            >
              <Grid item sm={12} md={3}>
                <FormControl fullWidth>
                  <InputLabel id="status-select">Statuses</InputLabel>
                  <Select
                    value={status}
                    id="status-select"
                    label="Status"
                    onChange={handleStatusFilterChange}
                  >
                    <MenuItem value={"all"}>All</MenuItem>
                    <MenuItem value={"active"}>Active</MenuItem>
                    <MenuItem value={"deactivated"}>Deactivated</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={12} md={6}>
                <IconTextField
                  fullWidth
                  id="keyword"
                  label="Keyword"
                  name="keyword"
                  autoComplete="keyword"
                  placeholder="Search by lesson id and topic..."
                  autoFocus
                  value={keyword}
                  iconEnd={<SearchIcon />}
                  onChange={handleKeywordChanged}
                />
              </Grid>
              <Grid item sm={12} md={3}>
                <Button variant="contained" onClick={handleSearch}>
                  Search
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Box>
        {/* end Filter */}
        <Grid container spacing={3}>
          {/* Recent Dash Board */}
          <Grid item xs={12}>
            <Paper sx={{ p: 2, display: "flex", flexDirection: "column" }}>
              <Title>Lesson Summary</Title>
              {tableContent}
              <Box>
                {/* update user active dialog */}
                <Dialog
                  open={openStatusDialog}
                  onClose={handleCloseStatusDialog}
                >
                  <DialogTitle>Update Status</DialogTitle>
                  <DialogContent>
                    <DialogContentText>
                      Update status of Lesson have topic :
                      {selectedLesson?.lessonContent}
                    </DialogContentText>
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={handleCloseStatusDialog}
                      variant="contained"
                      color="warning"
                    >
                      Cancel
                    </Button>
                    <Button
                      onClick={() => handleChangeStatus(selectedLesson)}
                      variant="contained"
                      color="success"
                    >
                      Confirm
                    </Button>
                  </DialogActions>
                </Dialog>
              </Box>
            </Paper>
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );

  return content;
};

export default LessonList;
