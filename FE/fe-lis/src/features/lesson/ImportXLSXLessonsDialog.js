import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import Tab from "@mui/material/Tab";
import { React, useEffect, useState } from "react";
import * as XLSX from "xlsx";
import CloseIcon from "@mui/icons-material/Close";
import { useImportLessonsFromXLSXMutation } from "./lessonApiSlice";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";

const ImportXLSXLessonsDialog = ({
  openImportXLSXLessonsDialog,
  setOpenImportXLSXLessonsDialog,
  createdBy,
  subject,
  refetch,
  enqueueSnackbar,
}) => {
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };
  // on change states
  const [selectedFile, setSelectedFile] = useState(null);
  const [excelFile, setExcelFile] = useState(null);
  const [excelFileError, setExcelFileError] = useState(null);

  // submit
  const [excelData, setExcelData] = useState(null);
  // it will contain array of objects

  const [lessons, setLessons] = useState(null);
  // handle File
  const fileType = [
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ];

  const [
    importLessonsFromXLSX,
    {
      isSuccess: importFromXlSXIsSuccuss,
      isError: importFromXlSXIsError,
      isLoading: importFromXlSXIsLoading,
      error: importFromXlSXError,
    },
  ] = useImportLessonsFromXLSXMutation();

  const checkValidSheet = (worksheet) => {
    try {
      return (
        worksheet?.A1.v.trim() === "Chapter" &&
        worksheet?.B1.v.trim() === "Lesson Number" &&
        worksheet?.C1.v.trim() === "Content"
      );
    } catch (error) {
      return false;
    }
  };

  const reformExcelDataToLessonsData = (excelData) => {
    let lessonsData = [];
    if (excelData !== null) {
      excelData.forEach((data) => {
        const newLesson = {
          lessonChapter: data?.["Chapter"],
          lessonNumber: data?.["Lesson Number"],
          lessonContent: data?.["Content"],
        };
        lessonsData = [...lessonsData, newLesson];
      });
      return lessonsData;
    }
  };

  const handleCloseImportXLSXLessonsDialog = () => {
    setOpenImportXLSXLessonsDialog(false);
  };
  const handleFile = (e) => {
    setSelectedFile(e.target.files[0]);
  };
  const handleImportXlSXData = async () => {
    handleOpenBackDrop();
    await importLessonsFromXLSX({ subject: subject._id, lessons, createdBy });
  };

  useEffect(() => {
    if (importFromXlSXIsSuccuss) {
      enqueueSnackbar("Add lessons to class successful !", {
        variant: "success",
      });
      setSelectedFile(null);
      setExcelFileError(null);
      setExcelFile(null);
      setExcelData(null);
      setLessons(null);
      handleCloseBackDrop();
      handleCloseImportXLSXLessonsDialog();
      refetch();
    }
  }, [importFromXlSXIsSuccuss]);
  useEffect(() => {
    if (importFromXlSXIsLoading) {
      handleOpenBackDrop();
    }
  }, [importFromXlSXIsLoading]);
  useEffect(() => {
    if (importFromXlSXIsError) {
      enqueueSnackbar(
        "Add lessons to class Fail : " + importFromXlSXError?.data?.message,
        {
          variant: "error",
        }
      );
      handleCloseBackDrop();
    }
  }, [importFromXlSXIsError]);

  // submit function
  const handleSubmit = () => {
    if (excelFile !== null) {
      const workbook = XLSX.read(excelFile, { type: "buffer" });
      const worksheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[worksheetName];
      const data = XLSX.utils.sheet_to_json(worksheet);
      if (checkValidSheet(worksheet)) {
        setExcelData(data);
      } else {
        setExcelFileError("Please make sure your import right file !");
        setExcelFile(null);
        setExcelData(null);
      }
    } else {
      setExcelData(null);
    }
  };

  useEffect(() => {
    if (selectedFile) {
      if (selectedFile && fileType.includes(selectedFile.type)) {
        let reader = new FileReader();
        reader.readAsArrayBuffer(selectedFile);
        reader.onload = (e) => {
          setExcelFileError(null);
          setExcelFile(e.target.result);
        };
      } else {
        setExcelFileError("Please select only excel file types");
        setExcelFile(null);
      }
    } else {
      console.log("please select your xlsx file");
    }
  }, [selectedFile]);

  useEffect(() => {
    if (excelFile) {
      handleSubmit();
    }
  }, [excelFile]);

  useEffect(() => {
    if (excelData) {
      setLessons(reformExcelDataToLessonsData(excelData));
    } else {
      setLessons(null);
    }
  }, [excelData]);

  const handleExportXLSX = () => {
    const dataSheet = [1, 2, 3, 4, 5].map((index) => {
      return {
        Chapter: index,
        "Lesson Number": index,
        Content: `Content-Example${index}`,
      };
    });
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(dataSheet);
    XLSX.utils.book_append_sheet(wb, ws, "MySheet1");
    XLSX.writeFile(
      wb,
      `${subject.subjectCode}-${subject.subjectName}-LessonsImportTemplate.xlsx`
    );
  };

  const xlsxDataTableContent = (
    <>
      <Paper sx={{ p: 3, mb: 3 }}>
        <Grid container justifyContent={"space-between"}>
          <Grid item>
            <Typography variant="h6">XLSX Data Table</Typography>
          </Grid>
        </Grid>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell sx={{ color: "indianred" }}>No.</TableCell>
              <TableCell sx={{ color: "indianred", width: "30%" }}>
                Chapter
              </TableCell>
              <TableCell sx={{ color: "indianred" }}>Lesson Number</TableCell>
              <TableCell sx={{ color: "indianred" }}>Lesson Content</TableCell>
            </TableRow>
          </TableHead>
          {lessons !== null ? (
            <TableBody>
              {lessons.map((user, index) => (
                <TableRow key={index}>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {user?.lessonChapter}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {user?.lessonNumber}
                  </TableCell>
                  <TableCell>{user?.lessonContent}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          ) : null}
        </Table>
      </Paper>
    </>
  );

  return (
    <>
      <Dialog
        open={openImportXLSXLessonsDialog}
        keepMounted
        onClose={handleCloseImportXLSXLessonsDialog}
        aria-describedby="groups-dialog-slide-description"
        maxWidth="lg"
        className="dialog-groups"
      >
        <DialogTitle sx={{ padding: "0" }}>
          <Paper
            sx={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              paddingRight: "10px",
              paddingLeft: "10px",
              alignItems: "center",
            }}
          >
            <Box>Import Lessons</Box>
            <Box>
              <IconButton>
                <CloseIcon
                  fontSize="inherit"
                  onClick={handleCloseImportXLSXLessonsDialog}
                />
              </IconButton>
            </Box>
          </Paper>
        </DialogTitle>
        <DialogContent sx={{ width: "1200px" }}>
          <Grid
            container
            justifyContent={"center"}
            sx={{
              borderBottom: "1px solid #c8c8c8",
              padding: "24px 0",
              width: "100%",
            }}
          >
            <span>You could download template here</span>
            <Button onClick={handleExportXLSX}>Download</Button>
          </Grid>
          <Grid container justifyContent={"center"} padding={3}>
            <Grid container justifyContent={"center"} mt={2}>
              <span>
                You need to upload correct .xlsx file to import lessons
              </span>
            </Grid>
            <label htmlFor="upload-xlsx">
              <input
                style={{ display: "none" }}
                id="upload-xlsx"
                name="upload-xlsx"
                type="file"
                onChange={handleFile}
              />
              <Button color="success" variant="contained" component="span">
                Upload
              </Button>
            </label>
          </Grid>
          <Grid container justifyContent={"center"}>
            <Typography
              variant="subtitle1"
              color={excelFileError !== null ? "red" : ""}
            >
              {excelFileError !== null ? excelFileError : selectedFile?.name}
            </Typography>
          </Grid>
          <Grid>{xlsxDataTableContent}</Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseImportXLSXLessonsDialog}>Cancel</Button>
          <Button
            variant="contained"
            color="primary"
            disabled={excelData == null}
            onClick={handleImportXlSXData}
          >
            Save
          </Button>
        </DialogActions>
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>
    </>
  );
};

export default ImportXLSXLessonsDialog;
