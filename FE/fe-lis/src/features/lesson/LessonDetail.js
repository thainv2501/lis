import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import MaterialsTable from "../material/MaterialsTable";
import {
  Chip,
  Grid,
  Paper,
  Skeleton,
  Tab,
  TextField,
  Button,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect, useState } from "react";
import Title from "../../components/Title";
import {
  useDeleteCQInLessonByCQIdMutation,
  useGetLessonsWithLessonIdQuery,
  useUpdateLessonMutation,
} from "./lessonApiSlice";
import { useNavigate, useParams } from "react-router-dom";
import { useAddNewMaterialsMutation } from "../material/materialApiSlice";
import { enqueueSnackbar } from "notistack";
import ConstructiveQuestionsTable from "../constructiveQuestions/ConstructiveQuestionsTable";
import {
  useAddNewCQMutation,
  useUpdateCQMutation,
} from "../constructiveQuestions/constructiveQuestionApiSlice";

const LessonDetail = () => {
  const navigate = useNavigate();
  const { lessonId } = useParams();
  const [tabValue, setTabValue] = useState("1");
  const [lessonContent, setLessonContent] = useState("");
  const [lessonObj, setLessonObj] = useState({});
  const [status, setStatus] = useState();
  const [editMode, setEditMode] = useState(false);

  const handleChangeTab = (event, newValue) => {
    setTabValue(newValue);
  };
  const handleLessonContentChange = (event) => {
    setLessonContent(event.target.value);
  };

  const [
    deleteCQInLessonByCQId,
    {
      isSuccess: deleteCQInLessonByCQIdIsSuccess,
      isError: deleteCQInLessonByCQIdIsError,
      isLoading: deleteCQInLessonByCQIdIsLoading,
      error: deleteCQInLessonByCQIdCQError,
    },
  ] = useDeleteCQInLessonByCQIdMutation();

  const [
    addNewMaterial,
    {
      data: createdMaterialRes,
      isSuccess: addNewMaterialIsSuccess,
      isLoading: addNewMaterialIsLoading,
      isError: addNewMaterialIsError,
      error: addNewMaterialError,
    },
  ] = useAddNewMaterialsMutation();

  const [
    updateLesson,
    {
      isSuccess: updateLessonIsSuccess,
      isLoading: updateLessonIsLoading,
      isError: updateLessonIsError,
      error: updateLessonError,
    },
  ] = useUpdateLessonMutation();

  const {
    data: lessonData,
    isSuccess: getLessonDataIsSuccess,
    isError: getLessonDataIsError,
    isLoading: getLessonDataIsLoading,
    error: getLessonDataError,
    refetch,
    isFetching,
  } = useGetLessonsWithLessonIdQuery(lessonId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const [
    updateCQ,
    {
      isSuccess: updateCQIsSuccess,
      isError: updateCQIsError,
      isLoading: updateCQIsLoading,
      error: updateCQError,
    },
  ] = useUpdateCQMutation();

  const [
    addNewCQ,
    {
      data: createdCQRes,
      isSuccess: addNewCQIsSuccess,
      isError: addNewCQIsError,
      isLoading: addNewCQIsLoading,
      error: addNewCQError,
    },
  ] = useAddNewCQMutation();

  if (getLessonDataIsError) {
    navigate("/404");
  }

  useEffect(() => {
    setLessonObj(lessonData);
    setLessonContent(
      lessonData?.lessonContent ? lessonData?.lessonContent : ""
    );
    setStatus(lessonData?.status);
  }, [lessonData]);

  useEffect(() => {
    if (addNewMaterialIsSuccess) {
      try {
        const { createdMaterial } = createdMaterialRes;
        handleAddNewMaterialToLesson(createdMaterial?._id);
      } catch (error) {
        enqueueSnackbar("Add material to lesson fail ! try again !", {
          variant: "error",
        });
      }
    }
  }, [addNewMaterialIsSuccess]);

  useEffect(() => {
    if (updateLessonIsSuccess || deleteCQInLessonByCQIdIsSuccess) {
      enqueueSnackbar("Lesson get new updated !", {
        variant: "success",
      });
      refetch();
      setEditMode(false);
    }
  }, [updateLessonIsSuccess, deleteCQInLessonByCQIdIsSuccess]);
  useEffect(() => {
    if (updateLessonIsError || deleteCQInLessonByCQIdIsError) {
      enqueueSnackbar("Lesson get new updated fail !", {
        variant: "error",
      });
    }
  }, [updateLessonIsError, deleteCQInLessonByCQIdIsError]);

  const handleAddNewMaterialToLesson = async (materialId) => {
    try {
      await updateLesson({
        lessonId,
        materialId,
      });
    } catch (error) {
      enqueueSnackbar("Lesson Add new material fail !", {
        variant: "error",
      });
    }
  };
  const handleUpdateLessonInformation = async () => {
    try {
      await updateLesson({
        lessonId,
        lessonContent,
        status,
      });
    } catch (error) {
      enqueueSnackbar("Error while get new update!", {
        variant: "error",
      });
    }
  };
  const handleResetInformation = () => {
    setLessonContent(lessonObj?.lessonContent);
    setStatus(lessonObj?.status);
    setEditMode(false);
  };

  useEffect(() => {
    if (updateCQIsSuccess) {
      enqueueSnackbar("Constructive Question Updated ", {
        variant: "success",
      });
      refetch();
    }
  }, [updateCQIsSuccess]);

  useEffect(() => {
    if (updateCQIsError) {
      enqueueSnackbar(
        "Constructive Question Updated Fail : " + addNewCQError?.data?.message,
        { variant: "error" }
      );
    }
  }, [updateCQIsError]);

  useEffect(() => {
    if (addNewCQIsSuccess) {
      const { createdCQ } = createdCQRes;
      enqueueSnackbar("New Constructive Question Created ", {
        variant: "success",
      });
      handleAddCQToLesson(createdCQ?._id);
    }
  }, [addNewCQIsSuccess]);

  useEffect(() => {
    if (addNewCQIsError) {
      enqueueSnackbar(
        "Constructive Question Created Fail : " + addNewCQError?.data?.message,
        { variant: "error" }
      );
    }
  }, [addNewCQIsError]);

  const handleAddCQToLesson = async (CQId) => {
    await updateLesson({
      lessonId,
      CQId,
    });
  };

  const canSave = lessonContent.trim() !== "";

  let content = null;
  useEffect(() => {
    if (getLessonDataIsLoading || isFetching) {
      content = (
        <Box sx={{ width: "100%" }}>
          <Skeleton />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
          <Skeleton animation="wave" />
          <Skeleton animation={false} />
        </Box>
      );
    }
  }, [getLessonDataIsLoading, isFetching]);

  if (lessonData) {
    content = (
      <Paper sx={{ p: 2 }}>
        <Title>
          {lessonData?.subject?.subjectCode} - Chapter{" "}
          {lessonData?.lessonChapter} - Lesson {lessonData?.lessonNumber}{" "}
          Details Information
        </Title>
        <Box sx={{ width: "100%", typography: "body1" }}>
          <TabContext value={tabValue}>
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <TabList
                onChange={handleChangeTab}
                aria-label="lab API tabs example"
              >
                <Tab label="General information" value="1" />
                <Tab label="Materials" value="2" />
                <Tab label="Constructive Questions" value="3" />
              </TabList>
            </Box>
            {/* General Information */}
            <TabPanel value="1">
              <Grid container spacing={3}>
                <Grid item container spacing={3}>
                  <Grid item container spacing={3} xs={12}>
                    <Grid item>
                      <Button
                        variant="contained"
                        onClick={() => setEditMode(true)}
                        disabled={editMode}
                      >
                        Change
                      </Button>
                    </Grid>
                    {editMode ? (
                      <>
                        <Grid item>
                          <Button
                            variant="contained"
                            color="success"
                            onClick={handleUpdateLessonInformation}
                            disabled={!canSave}
                          >
                            Save
                          </Button>
                        </Grid>
                        <Grid item>
                          <Button
                            variant="contained"
                            onClick={handleResetInformation}
                          >
                            Reset
                          </Button>
                        </Grid>
                      </>
                    ) : null}
                  </Grid>
                  <Grid item xs={12} md={3} lg={3}>
                    <TextField
                      disabled
                      id="subject-code"
                      name="subject code"
                      label="Subject Code"
                      value={lessonObj?.subject?.subjectCode}
                      fullWidth
                    />
                  </Grid>
                  <Grid item xs={12} md={3} lg={4}>
                    <TextField
                      disabled
                      id="Subject-name"
                      name="Subject Name"
                      label="Subject Name"
                      value={lessonObj?.subject?.subjectName}
                      fullWidth
                    />
                  </Grid>
                </Grid>
                <Grid container item spacing={3}>
                  <Grid item xs={12} md={3} lg={3}>
                    <TextField
                      disabled
                      id="Chapter"
                      name="Chapter"
                      label="Chapter"
                      fullWidth
                      value={lessonObj?.lessonChapter}
                    />
                  </Grid>
                  <Grid item xs={12} md={3} lg={4}>
                    <TextField
                      disabled
                      id="lesson-number"
                      name="Lesson Number"
                      label="Lesson Number"
                      fullWidth
                      value={lessonObj?.lessonNumber}
                    />
                  </Grid>
                </Grid>
                <Grid container item spacing={3}>
                  <Grid item xs={12} md={3} lg={3}>
                    <Button
                      onClick={() => setStatus((prev) => !prev)}
                      disabled={!editMode}
                    >
                      <Chip
                        label={status ? "Active" : "Deactivated"}
                        color={status ? "success" : "error"}
                      />
                    </Button>
                  </Grid>
                </Grid>
                <Grid item xs={12} md={12} lg={12}>
                  <TextField
                    disabled={!editMode}
                    id="lesson-content"
                    name="Lesson Content"
                    label="Lesson Content"
                    multiline
                    rows={5}
                    fullWidth
                    value={lessonContent}
                    onChange={handleLessonContentChange}
                  />
                </Grid>
              </Grid>
            </TabPanel>
            {/* Materials */}
            <TabPanel value="2">
              <Grid item xs={12}>
                <MaterialsTable
                  lessonId={lessonId}
                  materials={lessonObj?.materials ? lessonObj?.materials : []}
                  addNewMaterial={addNewMaterial}
                  addNewMaterialIsSuccess={addNewMaterialIsSuccess}
                  addNewMaterialIsError={addNewMaterialIsError}
                  addNewMaterialIsLoading={addNewMaterialIsLoading}
                  addNewMaterialError={addNewMaterialError}
                  refetch={refetch}
                />
              </Grid>
            </TabPanel>
            {/* Constructor Questions */}
            <TabPanel value="3">
              <ConstructiveQuestionsTable
                refetch={refetch}
                lessonId={lessonId}
                addNewCQ={addNewCQ}
                updateCQ={updateCQ}
                updateCQIsSuccess={updateCQIsSuccess}
                constructiveQuestions={lessonObj?.constructiveQuestions}
                updateLessonIsSuccess={updateLessonIsSuccess}
                deleteCQInLessonByCQId={deleteCQInLessonByCQId}
                lessonObj={lessonObj}
                deleteCQInLessonByCQIdIsSuccess={
                  deleteCQInLessonByCQIdIsSuccess
                }
              />
            </TabPanel>
          </TabContext>
        </Box>
      </Paper>
    );
  }

  return content;
};

export default LessonDetail;
