import {
  Button,
  Chip,
  Dialog,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import React, { useState } from "react";
import { Box } from "@mui/system";
import { useParams } from "react-router-dom";
import { enqueueSnackbar } from "notistack";

const LessonsTable = ({
  lessons,
  handleOpenAddLessonsDialog,
  handleCloseAddLessonsDialog,
  handleScheduleObjChange,
  updateSchedule,
}) => {
  const { scheduleId } = useParams();
  const handleRemoveLesson = async (data) => {
    try {
      const newLessons = lessons.filter((lesson) => lesson._id !== data._id);
      if (scheduleId) {
        updateSchedule({
          scheduleId: scheduleId,
          lessons: newLessons,
        });
      } else {
        handleScheduleObjChange("lessons", newLessons);
      }
    } catch (error) {
      enqueueSnackbar("Error with handleUpdate Function !", {
        variant: "error",
      });
    }
  };
  return (
    <>
      <Grid container spacing={3}>
        <Grid item container justifyContent={"flex-end"}>
          <Tooltip title="Add Lesson" placement="top" arrow>
            <Button onClick={handleOpenAddLessonsDialog}>
              <AddIcon />
            </Button>
          </Tooltip>
        </Grid>
        <Grid item xs={12}>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell sx={{ color: "indianred" }}>No.</TableCell>
                <TableCell sx={{ color: "indianred" }}>Chapter</TableCell>
                <TableCell sx={{ color: "indianred", width: "50%" }}>
                  Content
                </TableCell>
                <TableCell sx={{ color: "indianred" }}>Author</TableCell>
                <TableCell align="center" sx={{ color: "indianred" }}>
                  Actions
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {lessons.map((data, index) => (
                <TableRow key={data?._id}>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    <Typography>Chapter {data?.lessonChapter}</Typography>
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    <Typography>Lesson {data?.lessonNumber}</Typography>
                    {data?.lessonContent}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {data?.createdBy?.fullName}
                  </TableCell>
                  <TableCell>
                    <Box display={"flex"} justifyContent={"center"}>
                      <Button
                        color="error"
                        onClick={() => handleRemoveLesson(data)}
                      >
                        remove
                      </Button>
                    </Box>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </Grid>
      </Grid>
    </>
  );
};

export default LessonsTable;
