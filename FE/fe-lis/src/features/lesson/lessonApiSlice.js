import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

// Tạo adapter cho danh sách Lesson
const lessonsAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = lessonsAdapter.getInitialState();

// Tạo slice API cho Lesson
export const lessonsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    // Lấy danh sách Lesson
    getLesson: builder.query({
      query: () => ({
        url: "/lessons",
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        // Chuyển đổi dữ liệu trả về và cập nhật danh sách Lesson
        const loadedLessons = responseData.map((lesson) => {
          lesson.id = lesson._id;
          return lesson;
        });
        return lessonsAdapter.setAll(initialState, loadedLessons);
      },
      providesTags: (result, error, arg) => {
        // Tạo các tags để quản lý cache
        if (result?.ids) {
          return [
            { type: "Lesson", id: "LIST" },
            ...result.ids.map((id) => ({ type: "Lesson", id })),
          ];
        } else {
          return [{ type: "Lesson", id: "LIST" }];
        }
      },
    }),

    // Lấy danh sách Lesson với điều kiện tìm kiếm
    getLessonWithSearch: builder.query({
      query: (searchURL) => ({
        url: "/lessons/" + searchURL,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        // Chuyển đổi dữ liệu trả về và cập nhật danh sách Lesson
        const loadedLessons = responseData.lessons.map((data) => {
          data.id = data._id;
          return data;
        });
        return { ...responseData, lessons: loadedLessons };
      },
      providesTags: (result, error, arg) => {
        // Tạo các tags để quản lý cache
        if (result?.ids) {
          return [
            { type: "Lesson", id: "LIST" },
            ...result.ids.map((id) => ({ type: "Lesson", id })),
          ];
        } else {
          return [{ type: "Lesson", id: "LIST" }];
        }
      },
    }),

    // Lấy danh sách Lesson với trạng thái
    getLessonWithStatus: builder.query({
      query: (status) => ({
        url: "/lessons/" + status,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    // Thêm mới Lesson
    addNewLesson: builder.mutation({
      query: (initialLessonData) => ({
        url: "/lessons",
        method: "POST",
        body: {
          ...initialLessonData,
        },
      }),
      invalidatesTags: [{ type: "Lesson", id: "LIST" }],
    }),
    // Thêm mới Lessons
    importLessonsFromXLSX: builder.mutation({
      query: (initialLessonData) => ({
        url: "/lessons/add-lessons",
        method: "POST",
        body: {
          ...initialLessonData,
        },
      }),
      invalidatesTags: [{ type: "Lesson", id: "LIST" }],
    }),

    // Cập nhật Lesson
    updateLesson: builder.mutation({
      query: (initialLessonData) => ({
        url: "/lessons",
        method: "PATCH",
        body: {
          ...initialLessonData,
        },
      }),
      invalidatesTags: (result, error, arg) => [{ type: "Lesson", id: arg.id }],
    }),
    // Cập nhật Lesson
    deleteCQInLessonByCQId: builder.mutation({
      query: (initialLessonData) => ({
        url: "/lessons/CQ",
        method: "DELETE",
        body: {
          ...initialLessonData,
        },
      }),
      invalidatesTags: (result, error, arg) => [{ type: "Lesson", id: arg.id }],
    }),

    // Xóa Lesson
    deleteLesson: builder.mutation({
      query: ({ id }) => ({
        url: `/lessons`,
        method: "PATCH",
        body: { id },
      }),
      invalidatesTags: (result, error, arg) => [{ type: "Lesson", id: arg.id }],
    }),

    getLessonsWithLessonId: builder.query({
      query: (lessonId) => ({
        url: "/lessons/lesson-details/" + lessonId,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const {
  useGetLessonQuery,
  useGetLessonWithStatusQuery,
  useGetLessonWithSearchQuery,
  useImportLessonsFromXLSXMutation,
  useAddNewLessonMutation,
  useUpdateLessonMutation,
  useDeleteLessonMutation,
  useDeleteCQInLessonByCQIdMutation,
  useGetLessonsWithLessonIdQuery,
} = lessonsApiSlice;
