import {
  Box,
  Breadcrumbs,
  Typography,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Card,
  CardContent,
  CardActions,
  Button,
  Grid,
} from "@mui/material";
import RecentActorsIcon from "@mui/icons-material/RecentActors";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import CoPresentIcon from "@mui/icons-material/CoPresent";
import React, { useState } from "react";
import { Link } from "react-router-dom";

const MeetingList = () => {
  const [semester, setSemester] = useState("");
  const handleChangeSemester = (event) => {
    setSemester(event.target.value);
  };

  return (
    <Box component="main" sx={{ flex: "1", p: 2 }}>
      <Breadcrumbs aria-label="breadcrumb">
        <Link underline="hover" color="inherit" href="/">
          MUI
        </Link>
        <Link
          underline="hover"
          color="inherit"
          href="/material-ui/getting-started/installation/"
        >
          Core
        </Link>
        <Typography color="text.primary">Breadcrumbs</Typography>
      </Breadcrumbs>
      {/* end breadcrumb */}
      <Box sx={{ pt: 2 }}>
        <Grid container justifyContent="left" sx={{ mb: 2 }}>
          <Grid item>
            <Button>Complete</Button>
          </Grid>
          <Grid item>
            <Button>On Going</Button>
          </Grid>
          <Grid item>
            <Button>Upcoming</Button>
          </Grid>
        </Grid>
        <Box sx={{ maxWidth: "20vw" }}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Semester</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={semester}
              label="Semester"
              onChange={handleChangeSemester}
            >
              <MenuItem value={"se1"}>1</MenuItem>
              <MenuItem value={"se2"}>2</MenuItem>
              <MenuItem value={"se3"}>3</MenuItem>
            </Select>
          </FormControl>
        </Box>
        {/* end filter semesters */}
        {/* start card CoursesList */}
        <Box sx={{ pt: 2 }}>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            {[1, 2, 3, 4, 5].map((element) => {
              return (
                <Grid item xl={3} lg={3} sm={6} md={4}>
                  <Card
                    sx={{
                      "& .MuiTypography-root": { mt: 0.5 },
                      "& button": { mb: 0.5 },
                    }}
                  >
                    <CardContent>
                      <Button variant="contained" disabled>
                        Complete
                      </Button>
                      <Button variant="contained" color="success">
                        Ongoing
                      </Button>
                      <Button variant="contained" color="primary">
                        Upcoming
                      </Button>
                      <Typography variant="h5" component="div">
                        {`(Subject Code)`}
                      </Typography>
                      <Typography variant="h5" component="div" noWrap>
                        {`Subject Name along describe`}
                      </Typography>
                      <Typography
                        sx={{ display: "flex", alignItems: "center", gap: 1 }}
                        color="text.secondary"
                      >
                        <RecentActorsIcon fontSize="small" /> class Name
                      </Typography>
                      <Typography
                        sx={{ display: "flex", alignItems: "center", gap: 1 }}
                        color="text.secondary"
                      >
                        <AccountCircleIcon fontSize="small" /> Add by manager
                        account id
                      </Typography>
                      <Typography
                        sx={{ display: "flex", alignItems: "center", gap: 1 }}
                        color="text.secondary"
                      >
                        <CoPresentIcon fontSize="small" /> Teacher/Mentors
                      </Typography>
                    </CardContent>
                    <CardActions>
                      <Link to={"/common/course/:id"}>
                        <Button size="large">Go to course</Button>
                      </Link>
                    </CardActions>
                  </Card>
                </Grid>
              );
            })}
          </Grid>
        </Box>
        {/* end CoursesList */}
      </Box>
    </Box>
  );
};

export default MeetingList;
