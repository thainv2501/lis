import React from 'react';
import { Box, Grid, Pagination, Skeleton } from '@mui/material';
import CardMeeting from './CardMeeting';
const TabMeetingContent = ({
  loading,
  meetings,
  meetingsCount,
  handlePaginationChanged,
  page,
}) => {
  const renderSkeletons = () => {
    return (
      <Grid
        sx={{ paddingBottom: '24px', minHeight: '565px' }}
        container
        spacing={3}
        columnSpacing={{ xs: 1, sm: 2, md: 3 }}
      >
        {[1, 2, 3, 4].map((index) => (
          <Grid key={index} item xl={3} lg={3} sm={6} md={4}>
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Box sx={{ mt: 2 }}></Box>
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
            <Skeleton variant="text" animation="wave" />
          </Grid>
        ))}
      </Grid>
    );
  };

  const renderMeetings = () => {
    return (
      <Grid
        sx={{ paddingBottom: '24px', minHeight: '565px' }}
        container
        spacing={3}
        columnSpacing={{ xs: 1, sm: 2, md: 3 }}
      >
        {meetings &&
          meetings.map((meeting, index) => <CardMeeting meeting={meeting} />)}
      </Grid>
    );
  };
  let content;

  content = (
    <Box>
      {loading ? (
        <Box>{renderSkeletons()}</Box>
      ) : meetingsCount !== 0 ? (
        <Box sx={{ pt: 2 }}>
          {renderMeetings()}
          <Pagination
            count={Math.ceil(meetingsCount / 8)}
            page={page}
            onChange={handlePaginationChanged}
            variant="outlined"
            color="primary"
          />
        </Box>
      ) : (
        <Box sx={{ mt: 2 }}>
          <Box sx={{ width: '100%', textAlign: 'center' }}>
            <img
              src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
              alt="Img"
              style={{
                marginBottom: '25px',
                maxWidth: '200px',
                verticalAlign: 'middle',
              }}
            />
            <Box>
              <h3
                style={{
                  fontSize: '1.125rem',
                  lineHeight: '1.2',
                  color: '#0078d4',
                  marginBottom: '10px',
                }}
              >
                No meetings now.
              </h3>
              <p style={{ margin: '0', fontSize: '20px' }}>
                Please contact your school administration for more information.
              </p>
            </Box>
          </Box>
        </Box>
      )}
    </Box>
  );
  return content;
};

export default TabMeetingContent;
