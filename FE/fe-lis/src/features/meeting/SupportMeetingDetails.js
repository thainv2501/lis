import { React, useEffect, useState, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import {
  useGetMeetingWithMeetingIdQuery,
  useUpdateMeetingMutation,
} from '../meeting/meetingsApiSlice';
import { useSnackbar } from 'notistack';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { MobileDateTimePicker } from '@mui/x-date-pickers/MobileDateTimePicker';
import dayjs from 'dayjs';

import {
  Button,
  Grid,
  Paper,
  Box,
  InputLabel,
  FormControl,
  Select,
  Menu,
  MenuItem,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  TextField,
  Pagination,
  Checkbox,
  Skeleton,
  Typography,
  Chip,
  Tooltip,
  IconButton,
  FormControlLabel,
  Autocomplete,
} from '@mui/material';
import Switch from '@mui/material/Switch';
import { styled } from '@mui/material/styles';
import { useGetUsersWithRoleQuery } from '../users/usersApiSlice';
import useAuth from '../../hooks/useAuth';
const IOSSwitch = styled((props) => (
  <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
  width: 42,
  height: 26,
  padding: 0,
  '& .MuiSwitch-switchBase': {
    padding: 0,
    margin: 2,
    transitionDuration: '300ms',
    '&.Mui-checked': {
      transform: 'translateX(16px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
        opacity: 1,
        border: 0,
      },
      '&.Mui-disabled + .MuiSwitch-track': {
        opacity: 0.5,
      },
    },
    '&.Mui-focusVisible .MuiSwitch-thumb': {
      color: '#33cf4d',
      border: '6px solid #fff',
    },
    '&.Mui-disabled .MuiSwitch-thumb': {
      color:
        theme.palette.mode === 'light'
          ? theme.palette.grey[100]
          : theme.palette.grey[600],
    },
    '&.Mui-disabled + .MuiSwitch-track': {
      opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
    },
  },
  '& .MuiSwitch-thumb': {
    boxSizing: 'border-box',
    width: 22,
    height: 22,
  },
  '& .MuiSwitch-track': {
    borderRadius: 26 / 2,
    backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
    opacity: 1,
    transition: theme.transitions.create(['background-color'], {
      duration: 500,
    }),
  },
}));
function SupportMeetingDetails() {
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();
  const { meetingId } = useParams();
  const [canSave, setCanSave] = useState(false);
  const [meetingName, setMeetingName] = useState('');
  const [meetingType, setMeetingType] = useState('');
  const [selectedUser, setSelectedUser] = useState([]);
  const [dateTimeMeetingFrom, setDateTimeMeetingFrom] = useState(null);
  const [dateTimeMeetingTo, setDateTimeMeetingTo] = useState(null);
  const [maxUsers, setMaxUsers] = useState(1);
  const [classMeeting, setClassMeeting] = useState('');
  const [status, setStatus] = useState(false);
  const [meetingNameError, setMeetingNameError] = useState(false);
  const [meetingNameHelperText, setMeetingNameHelperText] = useState('');
  const [maxUsersError, setMaxUsersError] = useState(false);
  const [maxUsersHelperText, setMaxUsersHelperText] = useState('');
  const meetingNameRegExp = /^[\p{L}0-9\s]{3,50}$/u;

  const handleMeetingNameChange = (e) => {
    const value = e.target.value;
    // Check if the input matches the regular expression
    if (value.trim().length === 0) {
      setMeetingNameError(true);
      setMeetingNameHelperText('Meeting Name is required!');
    } else if (meetingNameRegExp.test(value)) {
      setMeetingNameError(false);
      setMeetingNameHelperText('');
    } else {
      setMeetingNameError(true);
      setMeetingNameHelperText('Meeting Name is invalid!');
    }
    setMeetingName(value);
  };

  const handleSelectedUserChange = (event) => {
    setSelectedUser([event.target.value]);
  };

  const handleDateTimeMeetingFromChange = (date) => {
    setDateTimeMeetingFrom(date);
  };

  const handleDateTimeMeetingToChange = (date) => {
    setDateTimeMeetingTo(date);
  };

  const handleMaxUsersChange = (e) => {
    let value = parseInt(e.target.value);
    if (isNaN(value) || value > 100) {
      setMaxUsersError(true);
      setMaxUsersHelperText('The maximum users is 100!');
      value = 100;
    }
    if (value < 2) {
      setMaxUsersError(true);
      setMaxUsersHelperText('The minimum users is 2!');
      value = 2;
    }

    if (value > 2 && value < 100) {
      setMaxUsersError(false);
      setMaxUsersHelperText('');
    }
    setMaxUsers(value);
  };

  const handleStatusChange = (e) => {
    setStatus(e.target.checked);
  };
  const {
    data: meeting,
    isSuccess: getMeetingIsSuccess,
    isLoading: getMeetingIsLoading,
    refetch,
  } = useGetMeetingWithMeetingIdQuery(meetingId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (getMeetingIsSuccess) {
      setMeetingName(meeting?.meetingName);
      setMeetingType(meeting?.meetingType);
      if (
        meeting?.meetingType === 'one-to-one' ||
        meeting?.meetingType === 'video-conference'
      ) {
        setSelectedUser(meeting?.invitedUsers);
      }
      setDateTimeMeetingFrom(meeting?.meetingFrom);
      setDateTimeMeetingTo(meeting?.meetingTo);
      setMaxUsers(meeting?.maxUsers);
      if (meeting?.meetingType === 'video-conference') {
        setClassMeeting(meeting?.class);
      }
      setStatus(meeting?.status);
    }
  }, [getMeetingIsSuccess, meeting]);

  const [
    updateMeeting,
    {
      isSuccess: updateMeetingIsSuccess,
      isError: updateMeetingIsError,
      error: updateMeetingError,
    },
  ] = useUpdateMeetingMutation();

  const handleSaveClick = async () => {
    if (dayjs(dateTimeMeetingFrom).isAfter(dayjs(dateTimeMeetingTo))) {
      enqueueSnackbar('Your From Date is invalid compare to End Date', {
        variant: 'error',
      });
    } else {
      try {
        if (meetingType === 'one-to-one') {
          await updateMeeting({
            meetingId: meetingId,
            meetingName: meetingName,
            invitedUsers: selectedUser,
            meetingFrom: dateTimeMeetingFrom,
            meetingTo: dateTimeMeetingTo,
            maxUsers: 1,
            status: status,
          }).then(() => {
            enqueueSnackbar(
              'Save the meeting new information is successfully!',
              {
                variant: 'success',
              }
            );
            navigate('/common/support-meetings');
            refetch();
          });
        }
        if (meetingType === 'video-conference') {
          await updateMeeting({
            meetingId: meetingId,
            meetingName: meetingName,
            invitedUsers: selectedUser,
            meetingFrom: dateTimeMeetingFrom,
            meetingTo: dateTimeMeetingTo,
            maxUsers: selectedUser.length,
            status: status,
          }).then(() => {
            enqueueSnackbar(
              'Save the meeting new information is successfully!',
              {
                variant: 'success',
              }
            );
            navigate('/common/support-meetings');
            refetch();
          });
        }
        if (meetingType === 'anyone-can-join') {
          await updateMeeting({
            meetingId: meetingId,
            meetingName: meetingName,
            invitedUsers: [],
            meetingFrom: dateTimeMeetingFrom,
            meetingTo: dateTimeMeetingTo,
            maxUsers: maxUsers,
            status: status,
          }).then(() => {
            enqueueSnackbar(
              'Save the meeting new information is successfully!',
              {
                variant: 'success',
              }
            );
            navigate('/common/support-meetings');
            refetch();
          });
        }
      } catch (err) {
        console.error('Failed to save the meeting new information', err);
        enqueueSnackbar(
          'Failed to save the meeting new information : ' +
            updateMeetingError?.data.message,
          {
            variant: 'error',
          }
        );
      }
    }
  };

  const {
    data: traineesData,
    isLoading: getTraineesIsLoading,
    isSuccess: getTraineesIsSuccess,
    isError: getTraineesIsError,
    error: getTraineesError,
  } = useGetUsersWithRoleQuery('Trainee');

  const [trainees, setTrainees] = useState([]);
  useEffect(() => {
    if (getTraineesIsSuccess) {
      const traineesWithoutMe = traineesData.filter((trainee) => {
        return trainee?._id !== userId;
      });
      setTrainees(traineesWithoutMe);
    }
  }, [getTraineesIsSuccess]);

  useEffect(() => {
    setCanSave(
      meetingName !== meeting?.meetingName ||
        JSON.stringify(selectedUser) !==
          JSON.stringify(meeting?.invitedUsers) ||
        !dayjs(dateTimeMeetingFrom).isSame(dayjs(meeting?.meetingFrom)) ||
        !dayjs(dateTimeMeetingTo).isSame(dayjs(meeting?.meetingTo)) ||
        maxUsers !== meeting?.maxUsers ||
        status !== meeting?.status
    );
  }, [
    meetingName,
    selectedUser,
    dateTimeMeetingFrom,
    dateTimeMeetingTo,
    maxUsers,
    status,
  ]);

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Paper
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flex: '1',
        }}
      >
        <Paper sx={{ p: 4, maxWidth: '700px' }}>
          <Typography
            sx={{ textAlign: 'center', fontSize: '2rem', color: 'red' }}
          >
            Edit Meeting
          </Typography>
          <TextField
            margin="dense"
            label="Meeting Name"
            type="text"
            fullWidth
            required
            sx={{
              marginBottom: '10px',
              marginTop: '15px',
            }}
            helperText={meetingNameHelperText}
            error={meetingNameError}
            value={meetingName}
            onChange={handleMeetingNameChange}
          />
          {meetingType === 'one-to-one' ? (
            <FormControl sx={{ marginTop: '5px' }} fullWidth>
              <InputLabel id="user-select">Invite User</InputLabel>
              <Select
                value={selectedUser[0]}
                id="user-select"
                label="Invite User"
                onChange={handleSelectedUserChange}
              >
                {trainees.map((trainee, index) => (
                  <MenuItem index={index} value={trainee?._id}>
                    {`${trainee?.email} - ${trainee?.fullName}`}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          ) : (
            ''
          )}

          {meetingType === 'video-conference' ? (
            <>
              <TextField
                margin="dense"
                disabled
                id="subject-code"
                name="subject-code"
                label="Subject Code"
                value={
                  classMeeting?.subject?.subjectCode
                    ? classMeeting?.subject?.subjectCode
                    : ''
                }
                fullWidth
              />
              <TextField
                margin="dense"
                disabled
                id="class-name"
                name="class-name"
                label="Class Name"
                fullWidth
                value={classMeeting?.className}
              />
              <Autocomplete
                multiple
                id="tags-outlined"
                options={classMeeting?.students}
                onChange={(event, newValue) => {
                  const selectedIds = newValue.map((user) => user?._id);
                  setSelectedUser(selectedIds);
                }}
                getOptionLabel={(option) => option.fullName}
                filterSelectedOptions
                value={selectedUser.map((userId) =>
                  classMeeting?.students.find((user) => user?._id === userId)
                )}
                renderInput={(params) => (
                  <TextField
                    margin="dense"
                    sx={{ maxWidth: '100%' }}
                    {...params}
                    label="Invite Users"
                    placeholder="Select a Users"
                  />
                )}
              />
            </>
          ) : (
            ''
          )}
          {meetingType === 'anyone-can-join' ? (
            <TextField
              margin="dense"
              fullWidth
              value={maxUsers}
              onChange={handleMaxUsersChange}
              error={maxUsersError}
              helperText={maxUsersHelperText}
              InputProps={{
                inputProps: { maxLength: 3 },
              }}
              label="Maximum People"
              type="number"
              required
            />
          ) : (
            ''
          )}

          {/* Date-Time Picker for 'From' date-time */}
          <MobileDateTimePicker
            sx={{
              marginTop: '15px',
              width: '100%',
            }}
            label="From"
            fullWidth
            disablePast
            value={dayjs(dateTimeMeetingFrom)}
            onChange={handleDateTimeMeetingFromChange}
            renderInput={(params) => <TextField {...params} />}
            PopperProps={{
              placement: 'right',
            }}
          />

          {/* Date-Time Picker for 'To' date-time */}
          <MobileDateTimePicker
            sx={{
              marginTop: '15px',
              width: '100%',
            }}
            label="To"
            disablePast
            fullWidth
            value={dayjs(dateTimeMeetingTo)}
            onChange={handleDateTimeMeetingToChange}
            renderInput={(params) => <TextField {...params} />}
            PopperProps={{
              placement: 'right',
            }}
          />
          <FormControlLabel
            control={
              <IOSSwitch
                sx={{ m: 1 }}
                checked={status}
                onChange={handleStatusChange}
              />
            }
            label={status ? 'Cancel Meeting' : 'Cancelled'}
          />
          <br></br>
          <Button
            onClick={handleSaveClick}
            variant="contained"
            disabled={!canSave}
          >
            Save
          </Button>
        </Paper>
      </Paper>
    </LocalizationProvider>
  );
}

export default SupportMeetingDetails;
