import { createEntityAdapter } from '@reduxjs/toolkit';
import { apiSlice } from '../../app/api/apiSlice';

const meetingsAdapter = createEntityAdapter({});

const initialState = meetingsAdapter.getInitialState();

export const meetingsApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getMeetings: builder.query({
      query: () => ({
        url: '/meetings',
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    getMeetingsWithSearch: builder.mutation({
      query: (initialMeetingData) => ({
        url: '/meetings/meetingSearch',
        method: 'POST',
        body: {
          ...initialMeetingData,
        },
      }),
      invalidatesTags: [{ type: 'Meeting', id: 'LIST' }],
      transformResponse: (responseData) => {
        const loadedMeetings = responseData.meetings.map((data) => {
          data.id = data._id;
          return data;
        });
        return { ...responseData, meetings: loadedMeetings };
      },
    }),
    addNewMeeting: builder.mutation({
      query: (initialMeetingData) => ({
        url: '/meetings',
        method: 'POST',
        body: {
          ...initialMeetingData,
        },
      }),
      invalidatesTags: [{ type: 'Meeting', id: 'LIST' }],
    }),

    updateMeeting: builder.mutation({
      query: (initialMeetingData) => ({
        url: '/meetings',
        method: 'PATCH',
        body: {
          ...initialMeetingData,
        },
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'Meeting', id: arg.id },
      ],
    }),
    getMeetingWithMeetingId: builder.query({
      query: (meetingId) => ({
        url: '/meetings/' + meetingId,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    getMeetingsWithClassId: builder.query({
      query: (classId) => ({
        url: '/meetings/class/' + classId,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    deleteMeeting: builder.mutation({
      query: (initialData) => ({
        url: '/meetings',
        method: 'DELETE',
        body: {
          ...initialData,
        },
      }),
    }),

    updateMeetingTime: builder.mutation({
      query: (initialMeetingData) => ({
        url: '/meetings/restart',
        method: 'PATCH',
        body: {
          ...initialMeetingData,
        },
      }),
      invalidatesTags: (result, error, arg) => [
        { type: 'Meeting', id: arg.id },
      ],
    }),
  }),
});

export const {
  useGetMeetingsWithSearchMutation,
  useGetMeetingsQuery,
  useAddNewMeetingMutation,
  useUpdateMeetingMutation,
  useGetMeetingWithMeetingIdQuery,
  useGetMeetingsWithClassIdQuery,
  useDeleteMeetingMutation,
  useUpdateMeetingTimeMutation,
} = meetingsApiSlice;
