import { ZegoUIKitPrebuilt } from '@zegocloud/zego-uikit-prebuilt';
import React, { useState, useEffect, useRef } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useGetMeetingWithMeetingIdQuery } from './meetingsApiSlice';
import useAuth from '../../hooks/useAuth';
import dayjs from 'dayjs';
import { Link } from 'react-router-dom';
import { Button } from '@mui/material';
import { ZIM } from 'zego-zim-web';
function JoinMeeting() {
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const { meetingId } = useParams();
  const [isAllowed, setIsAllowed] = useState(false);
  const [errorMeeting, setErrorMeeting] = useState('');
  const [isMeetingEnded, setIsMeetingEnded] = useState(false);
  const {
    data: meeting,
    isSuccess: getMeetingIsSuccess,
    isLoading: getMeetingIsLoading,
  } = useGetMeetingWithMeetingIdQuery(meetingId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (getMeetingIsSuccess) {
      const isCreator = meeting.createdBy === userId;
      const currentDate = dayjs();
      const meetingFrom = dayjs(meeting.meetingFrom);
      const meetingTo = dayjs(meeting.meetingTo);
      if (!meeting.status) {
        setErrorMeeting('Meeting is cancelled.');
        setIsAllowed(false);
      } else if (meeting.meetingType === 'one-to-one') {
        if (meeting.invitedUsers[0] === userId || isCreator || isAdmin) {
          if (currentDate.isBetween(meetingFrom, meetingTo, null, '[]')) {
            setIsAllowed(true);
          } else if (currentDate.isAfter(meetingTo)) {
            setErrorMeeting('Meeting has ended.');
            setIsAllowed(false);
          } else if (currentDate.isBefore(meetingFrom)) {
            setErrorMeeting(
              `Meeting is on ${dayjs(meetingFrom).format(
                'D MMM ddd HH:mm'
              )} - ${dayjs(meetingTo).format('D MMM ddd HH:mm')}.`
            );
            setIsAllowed(false);
          }
        }
      } else if (meeting.meetingType === 'video-conference') {
        const index = meeting.invitedUsers.findIndex(
          (invitedUser) => invitedUser === userId
        );
        if (index !== -1 || isCreator || isAdmin) {
          if (currentDate.isBetween(meetingFrom, meetingTo, null, '[]')) {
            setIsAllowed(true);
          } else if (currentDate.isAfter(meetingTo)) {
            setErrorMeeting('Meeting has ended.');
            setIsAllowed(false);
          } else if (currentDate.isBefore(meetingFrom)) {
            setErrorMeeting(
              `Meeting is on ${dayjs(meetingFrom).format(
                'D MMM ddd HH:mm'
              )} - ${dayjs(meetingTo).format('D MMM ddd HH:mm')}.`
            );
            setIsAllowed(false);
          }
        }
      } else if (meeting.meetingType === 'anyone-can-join') {
        if (userLogin || isCreator || isAdmin) {
          if (currentDate.isBetween(meetingFrom, meetingTo, null, '[]')) {
            setIsAllowed(true);
          } else if (currentDate.isAfter(meetingTo)) {
            setErrorMeeting('Meeting has ended.');
            setIsAllowed(false);
          } else if (currentDate.isBefore(meetingFrom)) {
            setErrorMeeting(
              `Meeting is on ${dayjs(meetingFrom).format(
                'D MMM ddd HH:mm'
              )} - ${dayjs(meetingTo).format('D MMM ddd HH:mm')}.`
            );
            setIsAllowed(false);
          }
        }
      } else {
        setErrorMeeting('You are not invited to the meeting.');
        setIsAllowed(false);
      }
    }
  }, [getMeetingIsSuccess]);

  const [zp, setZp] = useState(null);
  const elementRef = useRef(null);
  useEffect(() => {
    let newZp = null;
    if (getMeetingIsSuccess && isAllowed) {
      if (!zp) {
        const appId = 853558447;
        const serverSecret = '3874f44c54e30ed703e444feda31b42f';
        const kitToken = ZegoUIKitPrebuilt.generateKitTokenForTest(
          // parseInt(process.env.REACT_APP_ZEGOCLOUD_APP_ID),
          // process.env.REACT_APP_ZEGOCLOUD_SERVER_SECRET,
          appId,
          serverSecret,
          meetingId,
          userId,
          userLogin?.fullName ? userLogin.fullName : userId
        );
        newZp = ZegoUIKitPrebuilt.create(kitToken);
        setZp(newZp);
        newZp?.joinRoom({
          container: elementRef.current,
          maxUsers: 50,
          sharedLinks: [
            {
              name: 'Personal link',
              url: window.location.href,
            },
          ],
          scenario: {
            mode: ZegoUIKitPrebuilt.VideoConference,
          },
          onUserAvatarSetter: (userList) => {
            userList.forEach((user) => {
              user.setUserAvatar(
                'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg'
              );
            });
          },
          videoResolutionList: [
            ZegoUIKitPrebuilt.VideoResolution_180P,
            ZegoUIKitPrebuilt.VideoResolution_360P,
            ZegoUIKitPrebuilt.VideoResolution_480P,
            ZegoUIKitPrebuilt.VideoResolution_720P,
          ],
          videoResolutionDefault: ZegoUIKitPrebuilt.VideoResolution_720P,
        });
      }
    }

    // Clean up function to leave the room when the component unmounts
    return () => {
      if (newZp) {
        newZp.destroy();
        setZp(null);
      }
    };
  }, [getMeetingIsSuccess, isAllowed, meetingId, userId, userLogin?.fullName]);

  useEffect(() => {
    if (isMeetingEnded && zp) {
      zp.endVideoConference(); // Kết thúc cuộc họp
      zp.leaveRoom(); // Để đảm bảo rằng người dùng đang trong cuộc họp cũng sẽ bị đẩy ra
      setIsAllowed(false); // Không cho phép truy cập sau khi kết thúc cuộc họp
    }
  }, [isMeetingEnded, zp]);

  const handleEndMeeting = () => {
    setIsMeetingEnded(true); // Khi người tạo bấm kết thúc cuộc họp, set isMeetingEnded thành true
  };

  return getMeetingIsSuccess ? (
    isAllowed ? (
      <div
        style={{
          display: 'flex',
          height: '100vh',
          flexDirection: 'column',
          width: '100%',
        }}
      >
        <div
          className="myCallContainer"
          ref={elementRef}
          style={{ width: '100%', height: '100%' }}
        ></div>
      </div>
    ) : (
      <div
        style={{
          display: 'flex',
          flex: '1',
          height: '100vh',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <div
          style={{
            height: '500px',
            width: '1340px',
            margin: '0 auto',
            backgroundColor: '#f7f7f7',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: '50px',
          }}
        >
          <img
            src="https://cdni.iconscout.com/illustration/premium/thumb/no-data-found-8867280-7265556.png"
            alt="Img"
            style={{
              marginBottom: '25px',
              maxWidth: '200px',
              verticalAlign: 'middle',
            }}
          />
          <span
            style={{
              fontSize: '1.75rem',
              marginBottom: '24px',
              color: 'red',
              textAlign: 'center',
            }}
          >
            {errorMeeting}
          </span>
          <Link to={'/common/support-meetings'}>
            <Button variant="contained" color="success">
              Return to meeting screen
            </Button>
          </Link>
        </div>
      </div>
    )
  ) : (
    <></>
  );
}

export default JoinMeeting;
