import { React, useEffect, useState } from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import { Grid, Typography, Chip, IconButton, Tooltip } from '@mui/material';

import dayjs from 'dayjs';

import { Link } from 'react-router-dom';
import DateRangeIcon from '@mui/icons-material/DateRange';
import ContentPasteIcon from '@mui/icons-material/ContentPaste';
import BookIcon from '@mui/icons-material/Book';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import RecentActorsIcon from '@mui/icons-material/RecentActors';
import useAuth from '../../hooks/useAuth';
import { useSnackbar } from 'notistack';
import CopyToClipboard from 'react-copy-to-clipboard';

const CardMeeting = ({ meeting }) => {
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const [copiedMeetingId, setCopiedMeetingId] = useState(null);
  const handleCopyChange = (meetingId) => {
    setCopiedMeetingId(meetingId);

    setTimeout(() => {
      setCopiedMeetingId(null);
    }, 4000); // Reset copiedMeetingId after 4 seconds
  };
  const { enqueueSnackbar } = useSnackbar();
  const handleSnackbar = () => {
    enqueueSnackbar('You cannot access this!', {
      variant: 'warning',
    });
  };
  return (
    <Grid key={meeting?._id} item xl={3} lg={3} sm={7} md={4}>
      <Card sx={{ '& .MuiTypography-root': { mt: 0.5 } }}>
        <CardContent
          sx={{
            paddingBottom: '0',
          }}
        >
          {meeting?.status ? (
            dayjs().isBetween(
              meeting?.meetingFrom,
              meeting?.meetingTo,
              null,
              '[]'
            ) ? (
              <Link to={`/meeting/${meeting?.meetingId}`} target="_blank">
                <Chip
                  sx={{ cursor: 'pointer' }}
                  label="Join Now"
                  color="success"
                  variant="outlined"
                />
              </Link>
            ) : dayjs(meeting?.meetingTo).isBefore(dayjs()) ? (
              <Chip label="Ended" />
            ) : dayjs(meeting?.meetingFrom).isAfter(dayjs()) ? (
              <Chip color="primary" label="Upcoming" variant="outlined" />
            ) : null
          ) : (
            <Chip label="Cancelled" color="error" variant="outlined" />
          )}
          <CopyToClipboard
            text={`${window.location.origin}/meeting/${meeting?.meetingId}`}
            onCopy={() => handleCopyChange(meeting?.meetingId)}
          >
            <Tooltip
              title={
                copiedMeetingId === meeting?.meetingId
                  ? 'Copied'
                  : 'Copy meeting link'
              }
              placement="top"
            >
              <IconButton sx={{ float: 'right', p: 0 }}>
                <ContentCopyIcon />
              </IconButton>
            </Tooltip>
          </CopyToClipboard>
          <Tooltip title={meeting?.meetingName} placement="top">
            <Typography
              sx={{
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
              }}
              variant="h5"
              component="div"
            >
              {meeting?.meetingName}
            </Typography>
          </Tooltip>
          <Tooltip title={meeting?.class?.className} placement="top">
            <Typography
              sx={{
                display: 'flex',
                alignItems: 'center',
                gap: 1,
                fontSize: '15px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
              }}
              color="text.secondary"
            >
              <RecentActorsIcon fontSize="small" /> Class:
              {meeting?.class?.className}
            </Typography>
          </Tooltip>
          <Tooltip title={meeting?.class?.subject?.subjectName} placement="top">
            <Typography
              sx={{
                display: 'flex',
                alignItems: 'center',
                gap: 1,
                fontSize: '15px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
              }}
              color="text.secondary"
            >
              <BookIcon fontSize="small" /> Subject:{' '}
              {meeting?.class?.subject?.subjectName}
            </Typography>
          </Tooltip>
          <Typography
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: 1,
              fontSize: '15px',
            }}
            color="text.secondary"
          >
            <DateRangeIcon fontSize="small" /> Date:{' '}
            {dayjs(meeting?.meetingFrom).format('D MMM HH:mm')} -&gt;{' '}
            {dayjs(meeting?.meetingTo).format('D MMM HH:mm')}
          </Typography>
          <Typography
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: 1,
              fontSize: '15px',
            }}
            color="text.secondary"
          >
            <ContentPasteIcon fontSize="small" /> Type: {meeting?.meetingType}
          </Typography>
        </CardContent>
        <CardActions
          sx={{
            padding: '10px 16px',
            display: 'flex',
            alignItems: 'center',
          }}
        >
          {isAdmin || userId === meeting?.createdBy ? (
            <Link
              to={`/common/support-meetings/support-meeting-details/${meeting?.meetingId}`}
            >
              <span
                style={{
                  color: '#0078d4',
                  fontSize: '1.125rem',
                }}
                title="Go to details"
                value="Go to details"
              >
                Go to details <ArrowForwardIcon />
              </span>
            </Link>
          ) : (
            <Link to={''}>
              <span
                style={{
                  color: '#0078d4',
                  fontSize: '1.125rem',
                }}
                title="Go to details"
                value="Go to details"
                onClick={handleSnackbar}
              >
                Go to details <ArrowForwardIcon />
              </span>
            </Link>
          )}
        </CardActions>
      </Card>
    </Grid>
  );
};

export default CardMeeting;
