import { React, useEffect, useState } from 'react';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import {
  Button,
  Grid,
  Paper,
  Box,
  InputLabel,
  FormControl,
  Select,
  MenuItem,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Checkbox,
  Typography,
  IconButton,
  FormControlLabel,
  Autocomplete,
} from '@mui/material';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { MobileDateTimePicker } from '@mui/x-date-pickers/MobileDateTimePicker';
import dayjs from 'dayjs';

import { useSnackbar } from 'notistack';
import { styled } from '@mui/material/styles';

import CloseIcon from '@mui/icons-material/Close';
import Switch from '@mui/material/Switch';
import useAuth from '../../hooks/useAuth';
import { useGetUsersWithRoleQuery } from '../users/usersApiSlice';
import {
  useAddNewMeetingMutation,
  useGetMeetingsWithSearchMutation,
} from './meetingsApiSlice';
import { useGetMasterDataWithTypeQuery } from '../masterData/masterDataApiSlice';
import ClassDialog from '../class/ClassDialog';
import { generateMeetingId } from '../../utils/generateMeetingId';
import TabMeetingContent from './TabMeetingContent';
const IOSSwitch = styled((props) => (
  <Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />
))(({ theme }) => ({
  width: 42,
  height: 26,
  padding: 0,
  '& .MuiSwitch-switchBase': {
    padding: 0,
    margin: 2,
    transitionDuration: '300ms',
    '&.Mui-checked': {
      transform: 'translateX(16px)',
      color: '#fff',
      '& + .MuiSwitch-track': {
        backgroundColor: theme.palette.mode === 'dark' ? '#2ECA45' : '#65C466',
        opacity: 1,
        border: 0,
      },
      '&.Mui-disabled + .MuiSwitch-track': {
        opacity: 0.5,
      },
    },
    '&.Mui-focusVisible .MuiSwitch-thumb': {
      color: '#33cf4d',
      border: '6px solid #fff',
    },
    '&.Mui-disabled .MuiSwitch-thumb': {
      color:
        theme.palette.mode === 'light'
          ? theme.palette.grey[100]
          : theme.palette.grey[600],
    },
    '&.Mui-disabled + .MuiSwitch-track': {
      opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
    },
  },
  '& .MuiSwitch-thumb': {
    boxSizing: 'border-box',
    width: 22,
    height: 22,
  },
  '& .MuiSwitch-track': {
    borderRadius: 26 / 2,
    backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
    opacity: 1,
    transition: theme.transitions.create(['background-color'], {
      duration: 500,
    }),
  },
}));

const SupportMeetings = () => {
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;

  // master data semester
  const [semesters, setSemesters] = useState([]);
  const [currentSemester, setCurrentSemester] = useState('');
  const [semesterId, setSemesterId] = useState('');
  const [loading, setLoading] = useState(true);
  const {
    data: masterDataSemester,
    isSuccess: masterDataIsSuccess,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery('Semester', {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (masterDataIsSuccess) {
      const { masterData, currentSemester } = masterDataSemester;
      setSemesters(masterData);
      setCurrentSemester(currentSemester);
      setSemesterId(currentSemester?._id);
    }
  }, [masterDataIsSuccess, currentSemester]);

  const { enqueueSnackbar } = useSnackbar();
  const [meetingsCount, setMeetingsCount] = useState(0);
  const [page, setPage] = useState(1);
  const [valueTab, setValueTab] = useState('all');

  // Meetings Data
  const [meetings, setMeetings] = useState([]);

  const [
    getMeetingsWithSearch,
    {
      data: meetingsData,
      isSuccess: getMeetingsDataIsSuccess,
      isLoading: getMeetingsDataIsLoading,
      isError: getMeetingsDataIsError,
      error: getMeetingsDataError,
    },
  ] = useGetMeetingsWithSearchMutation();

  const fetchAllMeetings = async (page, tab) => {
    try {
      setLoading(true);
      const meetingsResponse = await getMeetingsWithSearch({
        page,
        tab,
      });
      setLoading(false);
      setMeetings(meetingsResponse.data.meetings);
      setMeetingsCount(meetingsResponse.data.totalMeetingsCount);
    } catch (error) {
      // Handle error if the API call fails
      console.error('Error fetching meetings:', error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchAllMeetings(1, 'all');
  }, []);

  const handleChangeTab = async (event, newValue) => {
    setValueTab(newValue);
    fetchAllMeetings(1, newValue);
  };

  const handlePaginationChanged = async (event, value) => {
    setPage(value);
    fetchAllMeetings(value, valueTab);
  };

  // Open Add Meeting Dialog
  const [openAddMeetingDialog, setOpenAddMeetingDialog] = useState(false);
  const handleOpenAddMeetingDialog = (subject) => {
    setOpenAddMeetingDialog(true);
  };

  const handleCloseAddMeetingDialog = () => {
    setOpenAddMeetingDialog(false);
  };

  // Open Add One to One Meeting Dialog
  const [openAddOneToOneMeetingDialog, setOpenAddOneToOneMeetingDialog] =
    useState(false);
  const handleOpenAddOneToOneMeetingDialog = (subject) => {
    setOpenAddMeetingDialog(false);
    setOpenAddOneToOneMeetingDialog(true);
  };

  const handleCloseAddOneToOneMeetingDialog = () => {
    setOpenAddOneToOneMeetingDialog(false);
    setEmptyMeetingObj();
  };

  // Open Add Video Conference Meeting Dialog
  const [
    openAddVideoConferenceMeetingDialog,
    setOpenAddVideoConferenceMeetingDialog,
  ] = useState(false);
  const handleOpenAddVideoConferenceMeetingDialog = (subject) => {
    setOpenAddMeetingDialog(false);
    setOpenAddVideoConferenceMeetingDialog(true);
  };

  const handleCloseAddVideoConferenceMeetingDialog = () => {
    setOpenAddVideoConferenceMeetingDialog(false);
    setEmptyMeetingObj();
  };

  const [anyoneCanJoin, setAnyoneCanJoin] = useState(true);
  const handleAnyoneCanJoinChange = (e) => {
    setAnyoneCanJoin(e.target.checked);
    setClassMeeting('');
  };

  // Open Class Dialog
  const [openClassDialog, setOpenClassDialog] = useState(false);

  const handleOpenClassDialog = () => {
    setOpenClassDialog(true);
  };

  const handleCloseClassDialog = () => {
    setOpenClassDialog(false);
  };
  // Remaining Trainers Data
  const {
    data: traineesData,
    isLoading: getTraineesIsLoading,
    isSuccess: getTraineesIsSuccess,
    isError: getTraineesIsError,
    error: getTraineesError,
  } = useGetUsersWithRoleQuery('Trainee');

  const [trainees, setTrainees] = useState([]);
  useEffect(() => {
    if (getTraineesIsSuccess) {
      const traineesWithoutMe = traineesData.filter((trainee) => {
        return trainee?._id !== userId;
      });
      setTrainees(traineesWithoutMe);
    }
  }, [getTraineesIsSuccess]);

  const [meetingName, setMeetingName] = useState('');
  const [selectedUser, setSelectedUser] = useState([]);
  const [dateTimeMeetingFrom, setDateTimeMeetingFrom] = useState(dayjs());
  const [dateTimeMeetingTo, setDateTimeMeetingTo] = useState(dayjs());
  const [maxUsers, setMaxUsers] = useState('');
  const [classMeeting, setClassMeeting] = useState('');
  const [meetingNameError, setMeetingNameError] = useState(false);
  const [meetingNameHelperText, setMeetingNameHelperText] = useState('');
  const [maxUsersError, setMaxUsersError] = useState(false);
  const [maxUsersHelperText, setMaxUsersHelperText] = useState('');

  const meetingNameRegExp = /^[\p{L}0-9\s]{3,50}$/u;

  const handleMeetingNameChange = (e) => {
    const value = e.target.value;
    // Check if the input matches the regular expression
    if (value.trim().length === 0) {
      setMeetingNameError(true);
      setMeetingNameHelperText('Meeting Name is required!');
    } else if (meetingNameRegExp.test(value)) {
      setMeetingNameError(false);
      setMeetingNameHelperText('');
    } else {
      setMeetingNameError(true);
      setMeetingNameHelperText('Meeting Name is invalid!');
    }
    setMeetingName(value);
  };

  const handleSelectedUserChange = (event) => {
    setSelectedUser([event.target.value]);
  };

  // select with class meeting
  const [selectAll, setSelectAll] = useState(false);
  const [selectedStudents, setSelectedStudents] = useState([]);

  useEffect(() => {
    // Check if all students are selected
    if (selectedStudents.length === classMeeting?.students?.length) {
      setSelectAll(true);
    } else {
      setSelectAll(false);
    }
  }, [selectedStudents, classMeeting]);

  useEffect(() => {
    if (selectAll) {
      setSelectedStudents(classMeeting?.students || []);
    }
  }, [selectAll, classMeeting]);

  const handleToggleSelectAll = () => {
    setSelectAll((prev) => !prev);
    setSelectedStudents([]);
  };

  const handleUserSelection = (event, newValue) => {
    setSelectedStudents(newValue);
  };

  useEffect(() => {
    if (!selectAll) {
      setSelectedUser(selectedStudents.map((user) => user?._id));
    }
  }, [selectAll, selectedStudents, setSelectedUser]);

  const handleDateTimeMeetingFromChange = (date) => {
    setDateTimeMeetingFrom(date);
  };

  const handleDateTimeMeetingToChange = (date) => {
    setDateTimeMeetingTo(date);
  };

  const handleMaxUsersChange = (e) => {
    let value = parseInt(e.target.value);
    if (isNaN(value) || value > 100) {
      setMaxUsersError(true);
      setMaxUsersHelperText('The maximum users is 100!');
      value = 100;
    }
    if (value < 2) {
      setMaxUsersError(true);
      setMaxUsersHelperText('The minimum users is 2!');
      value = 2;
    }

    if (value > 2 && value < 100) {
      setMaxUsersError(false);
      setMaxUsersHelperText('');
    }
    setMaxUsers(value);
  };

  // Create one to one meeting
  const setEmptyMeetingObj = () => {
    setMeetingName('');
    setSelectedUser([]);
    setSelectedStudents([]);
    setSelectAll(false);
    setMaxUsers('');
    setDateTimeMeetingFrom(null);
    setDateTimeMeetingTo(null);
    setClassMeeting('');
  };

  const [
    createNewMeeting,
    {
      isSuccess: createMeetingIsSuccess,
      isError: createMeetingIsError,
      error: createMeetingError,
    },
  ] = useAddNewMeetingMutation();

  const handleAddNewMeeting = async () => {
    if (dayjs(dateTimeMeetingFrom).isAfter(dayjs(dateTimeMeetingTo))) {
      enqueueSnackbar('Your From Date is invalid compare to End Date', {
        variant: 'error',
      });
    } else {
      const meetingId = generateMeetingId();
      try {
        await createNewMeeting({
          createdBy: userId,
          meetingId: meetingId,
          meetingName: meetingName,
          meetingType: 'one-to-one',
          invitedUsers: [selectedUser[0]],
          meetingFrom: dateTimeMeetingFrom,
          meetingTo: dateTimeMeetingTo,
          maxUsers: 1,
          status: true,
        });
        setEmptyMeetingObj();
        setOpenAddOneToOneMeetingDialog(false);
      } catch (error) {
        enqueueSnackbar('Create failed!: ' + error, {
          variant: 'error',
        });
      }
    }
  };
  // Create video conference
  const handleAddNewVideoConference = async () => {
    if (dayjs(dateTimeMeetingFrom).isAfter(dayjs(dateTimeMeetingTo))) {
      enqueueSnackbar('Your From Date is invalid compare to End Date', {
        variant: 'error',
      });
    } else {
      const meetingId = generateMeetingId();
      try {
        await createNewMeeting({
          createdBy: userId,
          meetingId: meetingId,
          meetingName: meetingName,
          meetingType: anyoneCanJoin ? 'anyone-can-join' : 'video-conference',
          invitedUsers: !anyoneCanJoin ? selectedUser : [],
          meetingFrom: dateTimeMeetingFrom,
          meetingTo: dateTimeMeetingTo,
          maxUsers: !anyoneCanJoin
            ? selectedUser && selectedUser.length
            : maxUsers,
          classId: classMeeting?._id,
          status: true,
        });
        setEmptyMeetingObj();
        setOpenAddVideoConferenceMeetingDialog(false);
      } catch (error) {
        enqueueSnackbar('Create failed!: ' + error, {
          variant: 'error',
        });
      }
    }
  };

  useEffect(() => {
    if (createMeetingIsSuccess) {
      enqueueSnackbar('Create new meeting successfully!', {
        variant: 'success',
      });
      fetchAllMeetings(1, 'all');
      setEmptyMeetingObj();
    }
    if (createMeetingIsError) {
      enqueueSnackbar('Create failed!: ' + createMeetingError?.data?.message, {
        variant: 'error',
      });
      setEmptyMeetingObj();
    }
  }, [createMeetingIsError, createMeetingIsSuccess]);

  let content;

  content = (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Paper sx={{ p: 5, width: '100%' }}>
        {/* Create New Meeting */}
        <Box
          sx={{
            pt: 2,
            pb: 2,
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          {/* {isAdmin || isTrainer ? (
            <Button variant="contained" onClick={handleOpenAddMeetingDialog}>
              Create New
            </Button>
          ) : (
            <></>
          )} */}

          {/* Create New Meeting */}
          <Dialog
            open={openAddMeetingDialog}
            keepMounted
            onClose={handleCloseAddMeetingDialog}
            aria-describedby="meetings-dialog-slide-description"
            maxWidth="md"
          >
            <DialogTitle sx={{ padding: '0' }}>
              <Paper
                sx={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                  paddingRight: '10px',
                  paddingLeft: '10px',
                  alignItems: 'center',
                }}
              >
                <Box>Meeting Type</Box>
                <Box>
                  <IconButton onClick={handleCloseAddMeetingDialog}>
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                </Box>
              </Paper>
            </DialogTitle>
            <DialogContent sx={{ width: '100%' }}>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  paddingTop: '20px',
                }}
              >
                {/* One to One */}
                <Card
                  sx={{
                    width: '345px',
                    marginRight: '20px',
                    cursor: 'pointer',
                  }}
                  onClick={handleOpenAddOneToOneMeetingDialog}
                >
                  <CardMedia
                    sx={{ height: 140 }}
                    image="https://assets-global.website-files.com/5f55ff47b6d23a11cb496a69/620e598f899e81e9d6ec92e1_Group%2019.png"
                    title="one to one"
                  />
                  <CardContent>
                    <Typography
                      gutterBottom
                      variant="h5"
                      component="div"
                      sx={{ textAlign: 'center' }}
                    >
                      One to One
                    </Typography>
                    <Typography
                      variant="body2"
                      color="text.secondary"
                      sx={{ textAlign: 'center' }}
                    >
                      Create a personal single person meeting.
                    </Typography>
                  </CardContent>
                </Card>
                {/* Dialog card first */}
                <Dialog
                  open={openAddOneToOneMeetingDialog}
                  keepMounted
                  onClose={handleCloseAddOneToOneMeetingDialog}
                  aria-describedby="one-to-one-dialog-slide-description"
                  maxWidth="lg"
                >
                  <DialogTitle sx={{ padding: '0' }}>
                    <Paper
                      sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        width: '100%',
                        paddingRight: '10px',
                        paddingLeft: '10px',
                        alignItems: 'center',
                      }}
                    >
                      <Box>One to One</Box>
                      <Box>
                        <IconButton
                          onClick={handleCloseAddOneToOneMeetingDialog}
                        >
                          <CloseIcon fontSize="inherit" />
                        </IconButton>
                      </Box>
                    </Paper>
                  </DialogTitle>
                  <DialogContent sx={{ width: '800px' }}>
                    <TextField
                      margin="dense"
                      label="Meeting Name"
                      type="text"
                      fullWidth
                      required
                      sx={{
                        marginBottom: '10px',
                        marginTop: '15px',
                      }}
                      helperText={meetingNameHelperText}
                      error={meetingNameError}
                      value={meetingName}
                      onChange={handleMeetingNameChange}
                    />
                    <FormControl sx={{ marginTop: '5px' }} fullWidth required>
                      <InputLabel id="user-select">Invite User</InputLabel>
                      <Select
                        value={selectedUser.length > 0 ? selectedUser[0] : ''}
                        id="user-select"
                        label="Invite User"
                        onChange={handleSelectedUserChange}
                      >
                        {trainees.map((trainee, index) => (
                          <MenuItem
                            key={index}
                            index={index}
                            value={trainee?._id}
                          >
                            {`${trainee?.email} - ${trainee?.fullName}`}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    {/* Date-Time Picker for 'From' date-time */}
                    <MobileDateTimePicker
                      sx={{
                        marginTop: '15px',
                        width: '100%',
                      }}
                      label="From"
                      fullWidth
                      disablePast
                      value={dateTimeMeetingFrom}
                      onChange={handleDateTimeMeetingFromChange}
                      renderInput={(params) => <TextField {...params} />}
                      PopperProps={{
                        placement: 'right',
                      }}
                    />

                    {/* Date-Time Picker for 'To' date-time */}
                    <MobileDateTimePicker
                      sx={{
                        marginTop: '15px',
                        width: '100%',
                      }}
                      label="To"
                      disablePast
                      fullWidth
                      value={dateTimeMeetingTo}
                      onChange={handleDateTimeMeetingToChange}
                      renderInput={(params) => <TextField {...params} />}
                      PopperProps={{
                        placement: 'right',
                      }}
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button onClick={handleCloseAddOneToOneMeetingDialog}>
                      Cancel
                    </Button>
                    <Button onClick={handleAddNewMeeting} color="primary">
                      Create
                    </Button>
                  </DialogActions>
                </Dialog>

                {/* Video Conference */}
                <Card
                  sx={{ width: '345px', cursor: 'pointer' }}
                  onClick={handleOpenAddVideoConferenceMeetingDialog}
                >
                  <CardMedia
                    sx={{ height: 140 }}
                    image="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABa1BMVEX///8AAADk9v8/bY5AcJPT7/s2Xn2x5PnW1tbn+f/d3d3r/v/dY25Bc5YrS2IxNTcJDxM8aIgiO03/zr6Pucqksbe6ydCz5Z+Q2Pn/qluhz+ImQlgXKTb/3s//mDh1zvnaSlSV1qTE09u1zddckLlKgKqMl5wiNUQzUGYUIi6dR062eUFujGEjIyNYhJmsw82bNTyWqrO2bChbg2VIfpnJWmSzUFmlbjt0p4ClYiR6Nzz/1sWNMDaPQEdghJ9bocJzfIFNU1bX6PEZGxuLs3xwqMLFxcUOGB8/REdja29KPDf/59d/iY5jY2Orq6vs7OyZmZnZr6LBnJBuWVLGQ0xJISRTMRL/pFHom1OPc2qHwpU7SzRPT09cdoFjLDF7n60mHxw5LircvrHCqp5kUUp4aWKrin/lx7rkuKpaYWRrJCk1GBo9JQ2OVR98Uy3fhTEtHhDOiUkjEBIiMCVKX0I3Ul8wPSt8ute+86lDNvXvAAALx0lEQVR4nO2di1vb1hnGLdtzEALfKAYKThN3syGNS2mTNCvdasstGEO4mDUdK90gJSSs6da1Sds/fzbnpsu5WdKxLHLe5+nT1kjH30/nOxcdvfJJpbS0tLS0tLS0tLS0tLS0tLS0tLS0tMQ6mJ6afE0fBKSbPuwaSVH3cHpkvv1C3FGPqML+SHxbSeMbqrAlD9iKO9iAaknyHSSxAoEKcp1OM+44Q6gpA+joQdca5eWBysv5SdZyeY2E3BUDkjZYzkGVshOtTDZTWsZRt0SAW+jIlRwGzEy+spkGClzUo6JGuIwBc3FHL6dsXq4p7icV0IHIH/qb3hRNRI4CzTYkKnEKXgYCmIuwR3AqumJJuSUY/RSH8NDTi+ZWl8tRKV8isWRL+ejKzaFys7BHPeQQnnqqcEVunJUUadK5SMtd8VTiqbAZriHA7UgDMbYxoaqCwdDPaYgH4IQGBGxwCg2k2dkHQ83ORl1wA6YpjJg9O50GB6BmGHUcxi2kyEuGPT5siOzbYUgIB8M8u8Bg+g4Tfhd10augDmHIsoTlaIPoEMABYifawvPZEISPHmL9E3zybiDdcitYIT69A0MOU4cPZ7A+A5/cmiC9GwHhI00YqzShJtSE8SsKwps/Wtx7hAVH/HcmSP+KgDAR0oSacPKlCd9ywvqGS/NhBFa6mqHKcKseAeH7f3DJDCELPHstWGEKcZdYi54wHUKYMEwhLpmaUBPKEZqWZaYH/5iREQ4amAWKnQRCy15vtbvd9s5GTRSRJOGg/9jYGRbZWrdFh6ontNJ7Bla7xg9IjtCqtUmRe2nuweoJrXnDpZ00rxplCM30jrvIee7RqgmtPcOjgs1BlCA0bZ9faY93uGJCPyAfUUxIAeQiKib0pihQOxRhm1YkO1HVEpo2DuGs1zvH/7PBjEdIaG3gUs57vTP8P8y8UEtoIfvUeX9uc3Pu2XMUD7OGxHWIijg+Ghb5PbpsLdYZaglRFV7Mbc7NzW1Wd79HScW64iJCE6X95bDEuepu9QJVYhyEJsyo87lrVavV3RPwSZdFICK0oGvw5Bpwc1Bk9Zx/0ZQSWnDc6pNwqmf8NBURwqw4w9esWoV5scM4RS0h9E9dAwJCVIk1xhUXEJoVZxUCwl3wUTMWQvDnC0c46IpXwhH2nYSwJcZI+JxCuB6QcB0SOrJ093nshD3nBY+mDi+dRfZiJIQd35yjHcILHrQdwnCPnWkBPmJ1z2oJ4aTU0bW/AJ8UA48W4HzjJSZEfRdraqp2PIQ5ZRxtonjOwQfMGYiQEM6S8BC7+8zg573imTe6DegPJlhzm7svYJNhJql4TgPjNXovh0WSWVKBcYLqeSm+tehdHh318bT0irmYISS0rlAhx/2jo0t0ydg3FyoJrfS852YcqVu3GYtSfELTtOuM1+V25hmLGeoIzXS9SA8GRERfsOESWjXGFbtWsU5dH1FGaFVErw7VadXIITTNuqDEZoV2miJCx40qU23KNWcTmukrQXkG9dZaEaElutzX6o5Uh1Lvq9Z9J6ohpC/P+HXli4dJaEnU4FC+PlUJoWN5xvj351h/gfrsB3Y8LELHNXv145+xPoX6D/lC74KNEkILd3kf3SEitqKZh1/Dvxe98TAIyTV7+t/bREtEH6ADvHfCKgjxvOOHO3fohNg75ZtNMgjxsuuPDr7b3yw59Qoe4pkvqSBEc8ef2YAYsSmZpU0K4O0lt34Cx3jmvEqytOBPUR/hzM/gIM+EmU6IpvA/8QCX/gcO8sxQVRDCIr/mAs7co6YpnRAl6cdcwqWntDRVQTgvU4UzM+CoKylCOFS4AL/xEcLeZl41YRqO9n8TEII0LUhlaYGSpH5CmKbuUV8F4Z4cIRgxOlKE4O2SpwLCv9MS30O4etMJ4Vt/nHfXEk8I3rHkvM0tQWhJtkNI6HY/0T1R1kiEdYvtiRrW4ptD3i9HSPja5mHH9/lHTt3zCcxOi1K+NnAv/epjpz7w6VPwxVccX9tQf+XwJd+bqAk14SRLE77lhBvrLlVCqAbHw1qYQtyaj4AwQS7ogfLlPd7PYiTdQZvJXsfM+fmWxBOCmXdH+JsKQQiFbt6RCcWWYx9hJHdPFMJBa0jbtm2KPcvyLmhrWGRabIMehwvatOyNq26n0+y2KqYgdGkXdGWvXeh0Cu26yFg9Dhe07Xgm1pznByRFaFrrDo+pwFitntD7TKzLswjLuaDttrtI6nO68RH6HmoWmQ/x5QjNmu83Qa5410O1C5ry1LYTzgVdozxaZtn21BPSbN4c34RUHVJ/d9P/3HA8hPgZjWH0Lo57EvGIXdDkmvWOL0iRzNRXTIgea14cbQ704gTFYwclJI/ZTl4Oi3yGLMI7sRDicJ5DYxv29zArUeiJQj1zH9mskEmH1bjV+tpgOMScuHtpCAhEhPAx2yW2yiFr4kYshDBJj4g3ERnb7GCEKCuIc7xahca2eFzQYODqUFzQIf2lLhc0/JYYHbTHEbqgkUfYSXgMPpswF3RYn/cEu6AvIyGcGBd0G/zd6YKG4xerb5fsaS78Luh2LIR1b5oiRy+rWxCPFnDWfURGC5ikrCFW7YiPJm1oeN5Fr8ww3xcUEsJJWwe+SUUmEaxpm+JZG3LbXQ7fM9vcfXYuCEfeBX0+nAcOLhps10Y3nlmbhazsxtlJv08syzshXND4dqx32e+f4BcQK6wiFRKaFtOy3GlVArqgKy3GT2J26zadUR2hZXN3iOjS/LwiF3SF6zFtUV/NV+eCFlpMW7QVVJ4LOi3cVINm2Ffmgqbe3LtFe6eb44KmvcPtFaWPVuWClgA0jKYfkUlo2lKbavgRFbmgJXzsQ/m7eDah5LZEPi+7ahf0L588xvoS6v5r/GffRITpgiarrm8efIH1FdSH5IfTx+OCbiO+x3eJFrB+X3jNikfogl59z6FFot8Qo3d+qtQF/e3du1TCAeN9eIj3JTaWCxp1ow8YgAO9gYeM0QX9KxOQIBZtKUK7SAH0EC42qBdNBSHq9f7BIVxY+BUc5HkhmOHcg7f2DR7g4m/goKb7oil0Qb/mAi58CY4ayQX9BZcQ5alyF3QazmYe8wkXwFEjuaDf4xN+BY4amwuan6QLv/8CWEZwQa/wAVGajs0FLSL89vqoURy0ZTlCrgtaE761hKZrOJRsh5DQcgsRugUJF/mEi+CL6+5zoyBs1V1qg08/cem+T6CnKbrPrUMXtOdTMOCvPHDqQ7/AF7fd57bCEJLd2iZfwXbwiHwXFoXKjVaHaCcd3vvokyW4HVJWuJMO3A0J7bqWnEpcBdt2iXdDSoG+bhsSJsYlnEf7koGdsgpswNQTcMYqQsyvccqdFK3gjdfgDlVPOIT76Bys0sSLbJ6HkpS3ASJsiKQSk7PBY4bsV8fdMhem6bajEuOOW17b4iQlWzyuJK8SUY5yN3gklUh2CEwIYjaDluP4VUhaolHMJyhPs1myV6Nw42q8IbCxtpwrAcTZCVdplawZU18g3XrSJHI9ISkmQ86QCw6WJwD34FQ8qCZWp8OcvcmA17uubomPSrS2bngVDisxOTeBwVRMgX9vr9G1MprgoNSgaoX+sVMwmBG/lR8MJGzk6BpxYII3lH+i649CgdPL0YySy1KEo+0BHhlhFBuSo2A0YaBCcwEJM5NPiNauynmqVvlCy0L5ySXMlnj9trRKUQAqIpyNYjv2aJJUEWE2EwFhJhLArJLRYlBs+DwtRVOFpA7B46C1iAgHiOGWWNeiAszOgkCaaObNIAySMblG0LlusTH6JWULlHmaOgT/sRwd4WzwWghxqq8o+G7+Ibo/LNIJo+m4YxFMpKlUClqrV+iIEXVsYxcatzqpFEpTFmLcoQYSvssZJGkqdYD88dt5GmEmmluZ8QqvnoIfASErNduN8rJXjCnmJKuMV4fR6qnQJZ9YtdCS8E1djTolq968X7RPrnacC/v7cUejQJ5HwQc3rTG2/M+gDvZvTnM83Wc9YzuYnkq+poVPELW0tLS0tLS0tLS0tLS0tLS0tLS03kr9H3ZEHyMAgwMrAAAAAElFTkSuQmCC"
                    title="video conference"
                  />
                  <CardContent>
                    <Typography
                      gutterBottom
                      variant="h5"
                      component="div"
                      sx={{ textAlign: 'center' }}
                    >
                      Video Conference
                    </Typography>
                    <Typography
                      variant="body2"
                      color="text.secondary"
                      sx={{ textAlign: 'center' }}
                    >
                      Invite multiple persons to the meeting.
                    </Typography>
                  </CardContent>
                </Card>
                {/* Dialog card second */}
                <Dialog
                  open={openAddVideoConferenceMeetingDialog}
                  keepMounted
                  onClose={handleCloseAddVideoConferenceMeetingDialog}
                  aria-describedby="video-conference-dialog-slide-description"
                  maxWidth="lg"
                >
                  <DialogTitle sx={{ padding: '0' }}>
                    <Paper
                      sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        width: '100%',
                        paddingRight: '10px',
                        paddingLeft: '10px',
                        alignItems: 'center',
                      }}
                    >
                      <Box>Video Conference</Box>
                      <Box>
                        <IconButton
                          onClick={handleCloseAddVideoConferenceMeetingDialog}
                        >
                          <CloseIcon fontSize="inherit" />
                        </IconButton>
                      </Box>
                    </Paper>
                  </DialogTitle>
                  <DialogContent sx={{ width: '800px' }}>
                    <FormControlLabel
                      control={
                        <IOSSwitch
                          sx={{ m: 1 }}
                          checked={anyoneCanJoin}
                          onChange={handleAnyoneCanJoinChange}
                        />
                      }
                      label={
                        anyoneCanJoin
                          ? 'Anyone can join'
                          : 'You must select student'
                      }
                    />
                    <TextField
                      margin="dense"
                      label="Meeting Name"
                      type="text"
                      fullWidth
                      required
                      helperText={meetingNameHelperText}
                      error={meetingNameError}
                      value={meetingName}
                      onChange={handleMeetingNameChange}
                      sx={{ marginBottom: '10px' }}
                    />
                    {!anyoneCanJoin ? (
                      <>
                        <>
                          <Grid item xs={12} sm={12} md={6} mb={2}>
                            <Button
                              sx={{ marginTop: '10px' }}
                              variant="outlined"
                              onClick={handleOpenClassDialog}
                            >
                              Select Class
                            </Button>
                          </Grid>
                          <Grid item xs={12} sm={12} md={6} mb={2}>
                            <Dialog
                              open={openClassDialog}
                              onClose={handleCloseClassDialog}
                              fullWidth
                              maxWidth={'md'}
                            >
                              <ClassDialog
                                setClassMeeting={setClassMeeting}
                                semesters={semesters}
                                currentSemester={currentSemester}
                                handleCloseClassDialog={handleCloseClassDialog}
                              />
                            </Dialog>
                          </Grid>
                        </>
                        {classMeeting !== '' ? (
                          <>
                            <Grid item sm={12} md={6} lg={6}>
                              <TextField
                                margin="dense"
                                disabled
                                required
                                id="subject-code"
                                name="subject-code"
                                label="Subject Code"
                                value={
                                  classMeeting?.subject?.subjectCode
                                    ? classMeeting?.subject?.subjectCode
                                    : ''
                                }
                                fullWidth
                              />
                            </Grid>
                            <Grid item xs={12} sm={12} md={6}>
                              <TextField
                                margin="dense"
                                disabled
                                id="class-name"
                                name="class-name"
                                label="Class Name"
                                fullWidth
                                value={classMeeting?.className}
                              />
                            </Grid>
                            <Grid item sm={12} md={6} lg={6}>
                              <FormControlLabel
                                control={
                                  <Checkbox
                                    checked={selectAll}
                                    onChange={handleToggleSelectAll}
                                    color="primary"
                                  />
                                }
                                label="Select All Students"
                              />
                              <Autocomplete
                                multiple
                                id="tags-outlined"
                                options={classMeeting?.students}
                                value={selectedStudents}
                                onChange={handleUserSelection}
                                getOptionLabel={(option) => option.email}
                                filterSelectedOptions
                                renderInput={(params) => (
                                  <TextField
                                    margin="dense"
                                    sx={{ maxWidth: '100%' }}
                                    {...params}
                                    required
                                    label="Invite Users"
                                    placeholder="Select a Users"
                                  />
                                )}
                              />
                            </Grid>
                          </>
                        ) : (
                          <></>
                        )}
                      </>
                    ) : (
                      <TextField
                        margin="dense"
                        fullWidth
                        value={maxUsers}
                        onChange={handleMaxUsersChange}
                        error={maxUsersError}
                        helperText={maxUsersHelperText}
                        InputProps={{
                          inputProps: { maxLength: 3 },
                        }}
                        label="Maximum People"
                        type="number"
                        required
                      />
                    )}
                    {/* Date-Time Picker for 'From' date-time */}
                    <MobileDateTimePicker
                      sx={{
                        marginTop: '15px',
                        width: '100%',
                      }}
                      label="From"
                      fullWidth
                      disablePast
                      value={dateTimeMeetingFrom}
                      onChange={handleDateTimeMeetingFromChange}
                      renderInput={(params) => <TextField {...params} />}
                      PopperProps={{
                        placement: 'right',
                      }}
                    />

                    {/* Date-Time Picker for 'To' date-time */}
                    <MobileDateTimePicker
                      sx={{
                        marginTop: '15px',
                        width: '100%',
                      }}
                      label="To"
                      disablePast
                      fullWidth
                      value={dateTimeMeetingTo}
                      onChange={handleDateTimeMeetingToChange}
                      renderInput={(params) => <TextField {...params} />}
                      PopperProps={{
                        placement: 'right',
                      }}
                    />
                  </DialogContent>
                  <DialogActions>
                    <Button
                      onClick={handleCloseAddVideoConferenceMeetingDialog}
                    >
                      Cancel
                    </Button>
                    <Button
                      onClick={handleAddNewVideoConference}
                      color="primary"
                    >
                      Create
                    </Button>
                  </DialogActions>
                </Dialog>
              </Box>
            </DialogContent>
          </Dialog>
        </Box>
        {/* End of Create New Meeting */}

        {/* Filter By All, Join Now, Ended, Upcoming, Cancelled */}
        <TabContext value={valueTab}>
          <Box>
            <TabList
              onChange={handleChangeTab}
              aria-label="lab API tabs example"
            >
              <Tab label="All" value="all" />
              <Tab label="Join Now" value="joinNow" />
              <Tab label="Ended" value="ended" />
              <Tab label="Upcoming" value="upcoming" />
              <Tab label="Cancelled" value="cancelled" />
            </TabList>
          </Box>

          <TabPanel sx={{ pr: 0, pl: 0 }} value="all">
            <TabMeetingContent
              loading={loading}
              meetings={meetings}
              meetingsCount={meetingsCount}
              handlePaginationChanged={handlePaginationChanged}
              page={page}
            />
          </TabPanel>
          <TabPanel sx={{ pr: 0, pl: 0 }} value="joinNow">
            <TabMeetingContent
              loading={loading}
              meetings={meetings}
              meetingsCount={meetingsCount}
              handlePaginationChanged={handlePaginationChanged}
              page={page}
            />
          </TabPanel>
          <TabPanel sx={{ pr: 0, pl: 0 }} value="ended">
            <TabMeetingContent
              loading={loading}
              meetings={meetings}
              meetingsCount={meetingsCount}
              handlePaginationChanged={handlePaginationChanged}
              page={page}
            />
          </TabPanel>
          <TabPanel sx={{ pr: 0, pl: 0 }} value="upcoming">
            <TabMeetingContent
              loading={loading}
              meetings={meetings}
              meetingsCount={meetingsCount}
              handlePaginationChanged={handlePaginationChanged}
              page={page}
            />
          </TabPanel>
          <TabPanel sx={{ pr: 0, pl: 0 }} value="cancelled">
            <TabMeetingContent
              loading={loading}
              meetings={meetings}
              meetingsCount={meetingsCount}
              handlePaginationChanged={handlePaginationChanged}
              page={page}
            />
          </TabPanel>
        </TabContext>
        {/* End of Filter By All, Join Now, Ended, Upcoming, Cancelled */}
      </Paper>
    </LocalizationProvider>
  );

  return content;
};

export default SupportMeetings;
