import { React, useEffect, useState, useRef } from 'react';
import {
  Box,
  Button,
  IconButton,
  TextField,
  MenuItem,
  Menu,
} from '@mui/material';
import { useSnackbar } from 'notistack';
import useAuth from '../../hooks/useAuth';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import dayjs from 'dayjs';
import { useUpdateReplyMutation } from '../reply/replyApiSlice';
function Reply({
  reply,
  selectedReply,
  setSelectedReply,
  handleClickOpenDeleteReplyDialog,
  fetchAllReplies,
  page,
  repliesIsEditting,
  setReliesIsEditting,
  handleOpenBackdrop,
  handleCloseBackdrop,
}) {
  const { enqueueSnackbar } = useSnackbar();
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const [content, setContent] = useState('');
  const [editingReplyId, setEditingReplyId] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const [canSave, setCanSave] = useState(false);
  const [anchorEls, setAnchorEls] = useState({});
  const handleClickReply = (event, replyId) => {
    setAnchorEls({ ...anchorEls, [replyId]: event.currentTarget });
  };
  const handleCloseReply = (replyId) => {
    setAnchorEls({ ...anchorEls, [replyId]: null });
  };

  const getTimeAgo = (createdAt) => {
    const now = dayjs();
    const targetDate = dayjs(createdAt);
    const diffInSeconds = now.diff(targetDate, 'second');
    const diffInMinutes = Math.floor(diffInSeconds / 60);
    const diffInHours = Math.floor(diffInMinutes / 60);
    const diffInDays = Math.floor(diffInHours / 24);
    const diffInMonths = Math.floor(diffInDays / 30);
    const diffInYears = Math.floor(diffInDays / 365);

    if (diffInSeconds < 60) {
      return `${diffInSeconds} second${diffInSeconds > 1 ? 's' : ''} ago`;
    } else if (diffInMinutes < 60) {
      return `${diffInMinutes} minute${diffInMinutes > 1 ? 's' : ''} ago`;
    } else if (diffInHours < 24) {
      return `${diffInHours} hour${diffInHours > 1 ? 's' : ''} ago`;
    } else if (diffInDays < 30) {
      return `${diffInDays} day${diffInDays > 1 ? 's' : ''} ago`;
    } else if (diffInMonths < 12) {
      return `${diffInMonths} month${diffInMonths > 1 ? 's' : ''} ago`;
    } else {
      return `${diffInYears} year${diffInYears > 1 ? 's' : ''} ago`;
    }
  };

  // Hàm xử lý sự kiện khi bấm nút Edit
  const handleEditClick = () => {
    setSelectedReply(reply);
    setEditingReplyId(reply._id);
    setIsEditing(true);
    setContent(reply?.content || '');
    setCanSave(false);
    setReliesIsEditting((prevState) => [...prevState]);
  };

  // Hàm xử lý sự kiện khi bấm nút Delete
  const handleDeleteClick = () => {
    setSelectedReply(reply);
    handleClickOpenDeleteReplyDialog();
  };

  const handleContentChange = (event) => {
    setContent(event.target.value);
  };

  const [
    updateReply,
    {
      isSuccess: updateReplyIsSuccess,
      isLoading: updateReplyIsLoading,
      isError: updateReplyIsError,
      error: updateReplyError,
    },
  ] = useUpdateReplyMutation();

  const handleUpdateReply = async () => {
    try {
      await updateReply({
        replyId: reply?._id,
        content: content,
      });
      enqueueSnackbar('Update reply information is successfully!', {
        variant: 'success',
      });
      setEditingReplyId(null); // Kết thúc edit
      fetchAllReplies(page);
    } catch (error) {
      enqueueSnackbar('Update failed!: ' + error, {
        variant: 'error',
      });
    }
  };

  useEffect(() => {
    if (updateReplyIsLoading) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [updateReplyIsLoading]);

  // Hàm xử lý khi bấm nút Cancel để hủy edit
  const handleCancelEdit = () => {
    setIsEditing(false);
    setEditingReplyId(null); // Kết thúc edit
  };

  useEffect(() => {
    setCanSave(content !== reply?.content);
  }, [content]);

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box sx={{ marginTop: '25px' }}>
        <Box sx={{ display: 'flex' }}>
          <Box
            sx={{
              marginTop: '0',
              marginRight: '10px',
              display: 'block',
            }}
          >
            <img
              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAhBSURBVHgB7Z3bbxRVHMe/u93KvSx3hNpuIw8g14IXFE1aQR/UBBJMTExMF6PyYqS8+GQCxD8AeNDo0y6i0ZiI+CaJSRcSEEJIC2IAuXS5FVFqSwsFehvPb/dsO7Tb7ezMmZ3f7J5P8tsZSgmd+fb3O7/zO7cAfIZhGBFxqRNG12phYXkfNpmZzhGWFHZVWAvdBwKBFviIABgjxKGXv0rYRnklC0M9CaQFPEz3QsROMIWdYFKkKNIi1cEbEsL2IS1eEprHIZGENQprMvjRJCwKzZBQO4R1GPxpFRYz0m1oaUEPLR/er8SMUhDOSHtUzCgeYkaBhStI0mGkE4ltwhrhTpbnJZRR7hHJyS4UANcFE2LViUsM6b5SMZMUtkUIl4CLBOESRjr87Ra3TSh+sYiIsCb5zK7hiofJuF4qQmUjKazejT6ccg8TYjWISzNKVywiIqxZvItGKEapYOIH3CEucRRfYmEHege75TtRhpKQKLNAit1RaLIRF7ZdRY3SsWBSLGqvVkGTCyou1zsVzZFgWqy8cSyabcG0WLZxJJotwbRYjrEtmt0skRIMLZZ96N3Z6mDnLZhMU6PQOCVqJ+XPKySK/4AKuHugUUlUhMZ9Vr/ZsmCy3EQVDN0pVgu1Y7VWy1j5CNaK0i43uUkSadHGTUIstWGyAh2Bxi0iwiy1Z+N6mBzPaoKmENSPN55mRTAdCgtHEuOExpwhUaadEWgKRQTpaRRjMqaH6azQM8i7asbyslweRt6lxSo8maGqrGT1MOldrdB4SU22vtlYHqZ0lFRji6wajPIw7V2smDGyLcvmYdq7+DAqY8zmYb7sd3X33ceXF/bj0K0juNvbDRU0PP02PlncAA8ZlTGGzH9rpJfVROAzuvq6se3k5zjTcQ4qWTvH8yG/zFq5oRGSkSHR018nu8Qu/aRcrLJAGSJTKsGAjeY/DAlmDK8d9hU3e27j29YDUM3CyfMwZ+JMMKBOTslIYfawOviQE+3NGDQMqGbt7FrRwLNZURzN3JgF82U4PHr7FNxgzazlYMRQWEwJJl2uDj7kbOd5uMGiadVgxFBYzHhYHXzIrQe3ceeR49nPowg/UYHKKfPBjFTK6mvBzt29Ajd4ee5zCAVCYMYm+sgIthI+5PidZqimLBDExsoNYEhKI1972NkO9e3XuzWbUDtzKRiSCokh0Zj5cgbv/f4eXLvfBlWUixDYsGgz3l/0DkQpCAyhJcjVFKgj8CHJezcwKTQ5ZU6YXj4Vz89eifXzX8Lqmcu4ipWhPiBU2wldofcLu3zhYQM3L6DvxI8YvPUX8LAHjpg0DZM+/BqB8gnwIamQyKqHaMbo6cSjX79A37HvqXcPFQSrlvtVLGIGCcZzos3gAB7+8Bn6/1Q7hzVUtQI+pprSepaC9R7+RrlYRGjJK/AxYZ6CDfaj74jlFTjWERkghUQfw1MwSjIGu/6FaoKzqxCY7OuplmHX9ppyQiobdIFgJcsKRl6wFGzg2mm4QWjxOvgdfoIN9GPgwjGoJ4CyymXwOySY+gElBxj32jHY+TdUE5xTnTKf08lOsIFbF1N9MNVMeG2rGDsph89hKNiFo1BNqPZNhFa/hSKgkyodSTDaJKX/ykkoQwxGTtiwFeWvfpC6LwJSgt0FI4Izn0IwvACOCJWLBGMpyl/YLPpd01FEJDMexoZJDWPv6NM/aOD+I+vt24PURz/sUiaccuoEVnM7rrITbCRdD/pw4FQbfj7Zht8v/4dC8saKeYh/9CwY0UKCJcCU823d+Hj/aZy57k3U3rBsHpiRDMplmawyRaL9Xi+2xps9E4tmCtRW8Wr/6KyzTOrUAmbsPXQJ59rUrPOyQ8XEckTmOJsvopgEfWQEc6d4Z5MHfQP47vh1eMmKqgpM4ZVwpDTKCHYQjPjjehe6HWR3Klj/zFwwI0EfLEOiV+2WmaULK8CMBH2kBJNraBNgwrGL7fCaJQumgRFD53Ka6zW/gAG9/YM4frkDXrKqKoy5FaxmVg3NlzALFgcDrrX3pFJ6L9m45kkwI5G5GRKMS1g8c6OLFhjCK2pEKv/ei1VgxGMn3Y4sYXseFn87+w+8YtbUJ/BVdDUqJrNK5x+bPjZSsDg8rnq0XC18hkj9rdeXzsWhT9ehtppVdYNOco+bv5BtJ5yd0IsjuBAXgm0xfyGbYDRxz9s0TZNh1BZ8o4ZhZfLhwrRbTZ7Es+2XqDe45Iv1DS7lN2ov8474WCdF5Nqkmdoy8jK9729hyXm0x5hTiWRbtheaQrM31zksOVdgSy8r9SN+Cwn1u2pyfUPOyXrSy7ZAUyjGfdfjzq6UZ4Ho0Og+e8c7d4WwtCmFDo2uM24ozGBp/rIMjfVgOLuqCMi8W0tYnnAuM5ft0Khml9XT+Yi8VgjIyvEuaFRBYu3J5x/YPcc5Dp9uOcuIfUKsKPJEH7ztDbYP3tZH2xce22IRjvaa06LljSOxCMebA2rRLONYLMLxOlJTH00Px4wNvRvHYhFKFv7SDyIzHp3yj4ZS96gKsQjl+6WKENkIfX4mQQJtHznrySmubHArpxhQuxZBaZJEOgQmoRhX9kKgH1QWM0uxyk/PXOuGWITrW0gLb6sTlxiK39uSwrZYGSJxguu7jdADSG+jhKQYq/30TPRstW6LVXCobaM6pFE8xAzTYWxFi+F/4WJGOrEqLYxh4VoN/nQI22mUgkdZQbyIqLAmgx9NwhoNJkKxO2jEGD48tQHenbqUQHqtXFxVhUIVrE+GMYaPeiRbCXcEJEGoMEv7YByke24imWEtWDaM9PFZEaRHB6rlfXiEmek0WVJer8r7hFsdXLf4H+v6TC9ogJXLAAAAAElFTkSuQmCC"
              alt="Img"
              style={{
                width: '36px',
                height: '36px',
                border: '1px solid #dddddd',
                borderRadius: '50%',
                objectFit: 'cover',
                overflow: 'hidden',
                transition: '0.3 all',
              }}
            />
          </Box>
          <Box
            sx={{
              flexGrow: '1',
              maxWidth: 'calc(100% - 40px)',
              fontFamily: 'sans-serif',
              fontWeight: '500',
              fontSize: '1rem',
            }}
          >
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  position: 'relative',
                  marginBottom: '8px',
                }}
              >
                <Box className="comment-writer">
                  <span
                    style={{
                      padding: '0',
                      color: '#333',
                      fontWeight: '600',
                      textDecoration: 'none',
                    }}
                    className="user-name"
                  >
                    {reply?.createdBy?.fullName}
                  </span>
                </Box>
                <Box style={{ lineHeight: '1' }} className="comment-user-info">
                  <time
                    style={{
                      fontSize: '12px',
                      color: '#333',
                      fontStyle: 'italic',
                    }}
                  >
                    {getTimeAgo(reply?.createdDate)}
                  </time>
                </Box>
              </Box>
              <Box>
                {(!editingReplyId || editingReplyId !== reply._id) &&
                  reply?.createdBy?._id === userId && (
                    <>
                      <IconButton
                        onClick={(event) => handleClickReply(event, reply?._id)}
                        aria-controls={`dropdown-menu-${reply?._id}`}
                        aria-haspopup="true"
                      >
                        <MoreVertIcon />
                      </IconButton>
                      <Menu
                        id={`dropdown-menu-${reply?._id}`}
                        anchorEl={anchorEls[reply?._id]}
                        open={Boolean(anchorEls[reply?._id])}
                        onClose={() => handleCloseReply(reply?._id)}
                        anchorOrigin={{
                          vertical: 'bottom',
                          horizontal: 'bottom',
                        }}
                        transformOrigin={{
                          vertical: 'top',
                          horizontal: 0,
                        }}
                      >
                        <MenuItem
                          sx={{
                            width: '100px',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}
                          onClick={() => {
                            handleEditClick();
                            handleCloseReply(reply?._id);
                          }}
                        >
                          Edit
                        </MenuItem>
                        <MenuItem
                          sx={{
                            width: '100px',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}
                          onClick={() => {
                            handleDeleteClick();
                            handleCloseReply(reply?._id);
                          }}
                        >
                          Delete
                        </MenuItem>
                      </Menu>
                    </>
                  )}
              </Box>
            </Box>
            <Box>
              {/* Content */}
              {isEditing &&
              (editingReplyId === reply._id ||
                repliesIsEditting.includes(reply._id)) ? (
                <Box>
                  <TextField
                    className="textfield-borderRadius"
                    margin="dense"
                    id={`question-reply-${reply?._id}`}
                    name={`question-reply-${reply?._id}`}
                    placeholder="Type something here..."
                    multiline
                    value={content}
                    onChange={handleContentChange}
                    fullWidth
                  />
                  <Box
                    sx={{
                      display: 'flex',
                      marginTop: '10px',
                    }}
                  >
                    <Button
                      onClick={handleCancelEdit}
                      sx={{ marginRight: '15px' }}
                    >
                      Cancel
                    </Button>
                    <Button
                      disabled={!canSave}
                      onClick={handleUpdateReply}
                      variant="contained"
                    >
                      Save
                    </Button>
                  </Box>
                </Box>
              ) : (
                <Box
                  sx={{
                    width: '100%',
                    display: 'inline-block',
                    padding: '15px 8px 15px 15px',
                    background: '#f2f5f7',
                    borderRadius: '4px',
                    color: '#373a3c',
                    fontSize: '16px',
                  }}
                >
                  <Box
                    sx={{
                      maxHeight: '350px',
                      position: 'relative',
                      width: '100%',
                      display: 'inline-block',
                      overflowX: 'auto',
                    }}
                  >
                    <Box
                      sx={{
                        overflowWrap: 'break-word',
                        overflow: 'auto',
                      }}
                    >
                      <span title="undefined">{reply?.content}</span>
                    </Box>
                  </Box>
                </Box>
              )}
            </Box>
          </Box>
        </Box>
      </Box>
    </LocalizationProvider>
  );
}

export default Reply;
