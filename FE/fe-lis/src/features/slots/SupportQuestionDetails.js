import { React, useEffect, useState, useRef } from 'react';
import {
  Box,
  Grid,
  Button,
  Typography,
  Dialog,
  DialogContent,
  DialogActions,
  DialogTitle,
  TextField,
  Tooltip,
  MenuItem,
  Autocomplete,
  Pagination,
  Menu,
  Divider,
  Backdrop,
  CircularProgress,
  useScrollTrigger,
  Zoom,
  Breadcrumbs,
} from '@mui/material';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import LinkIcon from '@mui/icons-material/Link';
import { useParams, useLocation, Link } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import CheckIcon from '@mui/icons-material/Check';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import useAuth from '../../hooks/useAuth';
import {
  useGetSupportQuestionByIdQuery,
  useUpdateSupportQuestionStatusMutation,
} from '../supportQuestions/supportQuestionsApiSlice';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import dayjs from 'dayjs';
import Card from '@mui/material/Card';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import {
  useGetAllReplyBySupportQuestionIdMutation,
  useAddNewReplyMutation,
  useDeleteReplyMutation,
} from '../reply/replyApiSlice';
import Reply from './Reply';
const status = [
  { id: 'no-answer', name: 'No Answer' },
  { id: 'discussing', name: 'Discussing' },
  { id: 'answered', name: 'Answered' },
];
const imageTypes = ['png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp'];
function SupportQuestionDetail() {
  const { enqueueSnackbar } = useSnackbar();
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const { supportQuestionId } = useParams();
  const [page, setPage] = useState(1);
  const [statusQuestion, setStatusQuestion] = useState('');
  const [content, setContent] = useState('');
  const [supportQuestionObj, setSupportQuestionObj] = useState(null);
  const [selectedReply, setSelectedReply] = useState(null);

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const handleScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const handleScrollBottom = () => {
    window.scrollTo({
      top: document.documentElement.scrollHeight,
      behavior: 'smooth',
    });
  };
  const {
    data: supportQuestion,
    isSuccess: getSupportQuestionIsSuccess,
    isLoading: getSupportQuestionIsLoading,
    refetch,
  } = useGetSupportQuestionByIdQuery(supportQuestionId, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (supportQuestion) {
      setSupportQuestionObj(supportQuestion);
      setStatusQuestion(supportQuestion?.status);
    }
  }, [supportQuestion]);

  useEffect(() => {
    if (getSupportQuestionIsLoading) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [getSupportQuestionIsLoading]);

  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  // handle delete support question
  const [openDeleteReplyDialog, setOpenDeleteReplyDialog] = useState(false);

  const handleClickOpenDeleteReplyDialog = () => {
    setOpenDeleteReplyDialog(true);
  };

  const handleCloseDeleteReplyDialog = () => {
    setOpenDeleteReplyDialog(false);
  };

  // Thêm state để lưu trạng thái hiển thị nội dung source
  const [expandedMap, setExpandedMap] = useState({});

  // Hàm xử lý khi nhấn nút "Xem thêm" hoặc "Ẩn đi"
  const handleToggleExpand = (questionId) => {
    setExpandedMap((prevMap) => ({
      ...prevMap,
      [questionId]: !prevMap[questionId],
    }));
  };

  const getTimeAgo = (createdAt) => {
    const now = dayjs();
    const targetDate = dayjs(createdAt);
    const diffInSeconds = now.diff(targetDate, 'second');
    const diffInMinutes = Math.floor(diffInSeconds / 60);
    const diffInHours = Math.floor(diffInMinutes / 60);
    const diffInDays = Math.floor(diffInHours / 24);
    const diffInMonths = Math.floor(diffInDays / 30);
    const diffInYears = Math.floor(diffInDays / 365);

    if (diffInSeconds < 60) {
      return `${diffInSeconds} second${diffInSeconds > 1 ? 's' : ''} ago`;
    } else if (diffInMinutes < 60) {
      return `${diffInMinutes} minute${diffInMinutes > 1 ? 's' : ''} ago`;
    } else if (diffInHours < 24) {
      return `${diffInHours} hour${diffInHours > 1 ? 's' : ''} ago`;
    } else if (diffInDays < 30) {
      return `${diffInDays} day${diffInDays > 1 ? 's' : ''} ago`;
    } else if (diffInMonths < 12) {
      return `${diffInMonths} month${diffInMonths > 1 ? 's' : ''} ago`;
    } else {
      return `${diffInYears} year${diffInYears > 1 ? 's' : ''} ago`;
    }
  };

  const [selectedItem, setSelectedItem] = useState('latest');
  const [replies, setReplies] = useState([]);
  const [repliesCount, setRepliesCount] = useState(0);
  const [
    getAllReplyBySupportQuestionId,
    {
      data: repliesData,
      isLoading: getRepliesDataIsLoading,
      isSuccess: getRepliesDataIsSuccess,
      isError: getRepliesDataIsError,
      error: getRepliesDataError,
    },
  ] = useGetAllReplyBySupportQuestionIdMutation();

  const fetchAllReplies = async (page) => {
    try {
      const repliesResponse = await getAllReplyBySupportQuestionId({
        id: supportQuestionId,
        filter: 'latest',
        page: page,
      });
      setReplies(repliesResponse.data.replies);
      setRepliesCount(repliesResponse.data.totalRepliesCount);
    } catch (error) {
      // Handle error if the API call fails
      console.error('Error fetching support questions:', error);
    }
  };

  useEffect(() => {
    fetchAllReplies(1);
  }, []);

  const handlePaginationChanged = async (event, value) => {
    setPage(value);
    try {
      const repliesResponse = await getAllReplyBySupportQuestionId({
        id: supportQuestionObj?._id,
        filter: selectedItem,
        page: value,
      });

      setReplies(repliesResponse.data.replies);
      setRepliesCount(repliesResponse.data.totalRepliesCount);
    } catch (error) {
      // Handle error if the API call fails
      console.error('Error fetching support questions:', error);
    }
  };

  const handleMenuItemClick = async (value) => {
    setSelectedItem(value);
    handleClose();
    try {
      const repliesResponse = await getAllReplyBySupportQuestionId({
        id: supportQuestionObj?._id,
        filter: value,
        page: 1,
      });

      setReplies(repliesResponse.data.replies);
      setRepliesCount(repliesResponse.data.totalRepliesCount);
    } catch (error) {
      // Handle error if the API call fails
      console.error('Error fetching support questions:', error);
    }
  };

  const handleContentChange = (event) => {
    setContent(event.target.value);
  };

  const [openBackdrop, setOpenBackdrop] = useState(false);

  const handleOpenBackdrop = () => {
    setOpenBackdrop(true);
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  const [
    addNewReply,
    {
      isSuccess: addNewReplyIsSuccess,
      isLoading: addNewReplyIsLoading,
      isError: addNewReplyIsError,
      error: addNewReplyError,
    },
  ] = useAddNewReplyMutation();

  const handleAddNewReply = async () => {
    if (content === '') {
      enqueueSnackbar('Content is not Empty!', {
        variant: 'error',
      });
    } else {
      try {
        await addNewReply({
          content: content,
          createdBy: userId,
          id: supportQuestionObj?._id,
          isComment: false,
        });
      } catch (error) {
        enqueueSnackbar('Create failed!: ' + error, {
          variant: 'error',
        });
      }
    }
  };

  useEffect(() => {
    if (addNewReplyIsSuccess) {
      setContent('');
      fetchAllReplies(1);
      refetch();
    }
  }, [addNewReplyIsSuccess]);

  useEffect(() => {
    if (addNewReplyIsLoading) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [addNewReplyIsLoading]);
  const [
    deleteReply,
    {
      isSuccess: deleteReplyIsSuccess,
      isLoading: deleteReplyIsLoading,
      isError: deleteReplyIsError,
      error: deleteReplyError,
    },
  ] = useDeleteReplyMutation();

  const handleDeleteReply = async () => {
    try {
      await deleteReply({
        id: selectedReply?._id,
      }).then(() => {
        enqueueSnackbar('Remove reply information is successfully!', {
          variant: 'success',
        });
      });
    } catch (error) {
      enqueueSnackbar('Remove failed!: ' + error, {
        variant: 'error',
      });
    }
  };

  useEffect(() => {
    if (deleteReplyIsSuccess) {
      setOpenDeleteReplyDialog(false);
      setSelectedReply(null);
      if (replies.length === 1 && page > 1) {
        fetchAllReplies((prevPage) => prevPage - 1);
        setPage((prevPage) => prevPage - 1);
      } else {
        fetchAllReplies(page);
      }
      refetch();
    }
  }, [deleteReplyIsSuccess]);

  useEffect(() => {
    if (deleteReplyIsLoading) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [deleteReplyIsLoading]);

  // update support question by status
  const [
    updateSupportQuestionStatus,
    {
      isSuccess: updateSupportQuestionStatusIsSuccess,
      isLoading: updateSupportQuestionStatusIsLoading,
      isError: updateSupportQuestionStatusIsError,
      error: updateSupportQuestionStatusError,
    },
  ] = useUpdateSupportQuestionStatusMutation();

  const handleSupportQuestionStatusChange = async (event, newValue) => {
    setStatusQuestion(newValue?.id);
    try {
      await updateSupportQuestionStatus({
        questionId: supportQuestionId,
        newStatus: newValue?.id,
      });
      enqueueSnackbar('Update question status is successfully!', {
        variant: 'success',
      });
      refetch();
    } catch (error) {
      enqueueSnackbar('Update failed!: ' + error, {
        variant: 'error',
      });
    }
  };

  useEffect(() => {
    if (updateSupportQuestionStatusIsLoading) {
      handleOpenBackdrop();
    } else {
      handleCloseBackdrop();
    }
  }, [updateSupportQuestionStatusIsLoading]);

  const [repliesIsEditting, setReliesIsEditting] = useState([]);
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box sx={{ margin: '0 auto', p: 3, width: '1140px' }}>
        <Box sx={{ marginBottom: '24px' }}>
          <Breadcrumbs aria-label="breadcrumb" style={{ fontSize: '20px' }}>
            <Link
              style={{ color: '#0078D8', textDecoration: 'underline' }}
              to={'/common/courses-list'}
            >
              Home
            </Link>
            {supportQuestion?.type === 'subject-question' ? (
              <Link
                style={{ color: '#0078D8', textDecoration: 'underline' }}
                to={`/common/${supportQuestion?.type}?type=${supportQuestion?.type}`}
              >
                Subjects support
              </Link>
            ) : (
              <Link
                style={{ color: '#0078D8', textDecoration: 'underline' }}
                to={`/common/class-support?type=${supportQuestion?.type}`}
              >
                Class support
              </Link>
            )}

            <span>{supportQuestion?.title}</span>
          </Breadcrumbs>
        </Box>
        <Box>
          {/* Question Details */}
          <Box sx={{ display: 'flex' }}>
            <Box
              sx={{
                marginTop: '0',
                marginRight: '10px',
                display: 'block',
              }}
            >
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAhBSURBVHgB7Z3bbxRVHMe/u93KvSx3hNpuIw8g14IXFE1aQR/UBBJMTExMF6PyYqS8+GQCxD8AeNDo0y6i0ZiI+CaJSRcSEEJIC2IAuXS5FVFqSwsFehvPb/dsO7Tb7ezMmZ3f7J5P8tsZSgmd+fb3O7/zO7cAfIZhGBFxqRNG12phYXkfNpmZzhGWFHZVWAvdBwKBFviIABgjxKGXv0rYRnklC0M9CaQFPEz3QsROMIWdYFKkKNIi1cEbEsL2IS1eEprHIZGENQprMvjRJCwKzZBQO4R1GPxpFRYz0m1oaUEPLR/er8SMUhDOSHtUzCgeYkaBhStI0mGkE4ltwhrhTpbnJZRR7hHJyS4UANcFE2LViUsM6b5SMZMUtkUIl4CLBOESRjr87Ra3TSh+sYiIsCb5zK7hiofJuF4qQmUjKazejT6ccg8TYjWISzNKVywiIqxZvItGKEapYOIH3CEucRRfYmEHege75TtRhpKQKLNAit1RaLIRF7ZdRY3SsWBSLGqvVkGTCyou1zsVzZFgWqy8cSyabcG0WLZxJJotwbRYjrEtmt0skRIMLZZ96N3Z6mDnLZhMU6PQOCVqJ+XPKySK/4AKuHugUUlUhMZ9Vr/ZsmCy3EQVDN0pVgu1Y7VWy1j5CNaK0i43uUkSadHGTUIstWGyAh2Bxi0iwiy1Z+N6mBzPaoKmENSPN55mRTAdCgtHEuOExpwhUaadEWgKRQTpaRRjMqaH6azQM8i7asbyslweRt6lxSo8maGqrGT1MOldrdB4SU22vtlYHqZ0lFRji6wajPIw7V2smDGyLcvmYdq7+DAqY8zmYb7sd3X33ceXF/bj0K0juNvbDRU0PP02PlncAA8ZlTGGzH9rpJfVROAzuvq6se3k5zjTcQ4qWTvH8yG/zFq5oRGSkSHR018nu8Qu/aRcrLJAGSJTKsGAjeY/DAlmDK8d9hU3e27j29YDUM3CyfMwZ+JMMKBOTslIYfawOviQE+3NGDQMqGbt7FrRwLNZURzN3JgF82U4PHr7FNxgzazlYMRQWEwJJl2uDj7kbOd5uMGiadVgxFBYzHhYHXzIrQe3ceeR49nPowg/UYHKKfPBjFTK6mvBzt29Ajd4ee5zCAVCYMYm+sgIthI+5PidZqimLBDExsoNYEhKI1972NkO9e3XuzWbUDtzKRiSCokh0Zj5cgbv/f4eXLvfBlWUixDYsGgz3l/0DkQpCAyhJcjVFKgj8CHJezcwKTQ5ZU6YXj4Vz89eifXzX8Lqmcu4ipWhPiBU2wldofcLu3zhYQM3L6DvxI8YvPUX8LAHjpg0DZM+/BqB8gnwIamQyKqHaMbo6cSjX79A37HvqXcPFQSrlvtVLGIGCcZzos3gAB7+8Bn6/1Q7hzVUtQI+pprSepaC9R7+RrlYRGjJK/AxYZ6CDfaj74jlFTjWERkghUQfw1MwSjIGu/6FaoKzqxCY7OuplmHX9ppyQiobdIFgJcsKRl6wFGzg2mm4QWjxOvgdfoIN9GPgwjGoJ4CyymXwOySY+gElBxj32jHY+TdUE5xTnTKf08lOsIFbF1N9MNVMeG2rGDsph89hKNiFo1BNqPZNhFa/hSKgkyodSTDaJKX/ykkoQwxGTtiwFeWvfpC6LwJSgt0FI4Izn0IwvACOCJWLBGMpyl/YLPpd01FEJDMexoZJDWPv6NM/aOD+I+vt24PURz/sUiaccuoEVnM7rrITbCRdD/pw4FQbfj7Zht8v/4dC8saKeYh/9CwY0UKCJcCU823d+Hj/aZy57k3U3rBsHpiRDMplmawyRaL9Xi+2xps9E4tmCtRW8Wr/6KyzTOrUAmbsPXQJ59rUrPOyQ8XEckTmOJsvopgEfWQEc6d4Z5MHfQP47vh1eMmKqgpM4ZVwpDTKCHYQjPjjehe6HWR3Klj/zFwwI0EfLEOiV+2WmaULK8CMBH2kBJNraBNgwrGL7fCaJQumgRFD53Ka6zW/gAG9/YM4frkDXrKqKoy5FaxmVg3NlzALFgcDrrX3pFJ6L9m45kkwI5G5GRKMS1g8c6OLFhjCK2pEKv/ei1VgxGMn3Y4sYXseFn87+w+8YtbUJ/BVdDUqJrNK5x+bPjZSsDg8rnq0XC18hkj9rdeXzsWhT9ehtppVdYNOco+bv5BtJ5yd0IsjuBAXgm0xfyGbYDRxz9s0TZNh1BZ8o4ZhZfLhwrRbTZ7Es+2XqDe45Iv1DS7lN2ov8474WCdF5Nqkmdoy8jK9729hyXm0x5hTiWRbtheaQrM31zksOVdgSy8r9SN+Cwn1u2pyfUPOyXrSy7ZAUyjGfdfjzq6UZ4Ho0Og+e8c7d4WwtCmFDo2uM24ozGBp/rIMjfVgOLuqCMi8W0tYnnAuM5ft0Khml9XT+Yi8VgjIyvEuaFRBYu3J5x/YPcc5Dp9uOcuIfUKsKPJEH7ztDbYP3tZH2xce22IRjvaa06LljSOxCMebA2rRLONYLMLxOlJTH00Px4wNvRvHYhFKFv7SDyIzHp3yj4ZS96gKsQjl+6WKENkIfX4mQQJtHznrySmubHArpxhQuxZBaZJEOgQmoRhX9kKgH1QWM0uxyk/PXOuGWITrW0gLb6sTlxiK39uSwrZYGSJxguu7jdADSG+jhKQYq/30TPRstW6LVXCobaM6pFE8xAzTYWxFi+F/4WJGOrEqLYxh4VoN/nQI22mUgkdZQbyIqLAmgx9NwhoNJkKxO2jEGD48tQHenbqUQHqtXFxVhUIVrE+GMYaPeiRbCXcEJEGoMEv7YByke24imWEtWDaM9PFZEaRHB6rlfXiEmek0WVJer8r7hFsdXLf4H+v6TC9ogJXLAAAAAElFTkSuQmCC"
                alt="Img"
                style={{
                  width: '36px',
                  height: '36px',
                  border: '1px solid #dddddd',
                  borderRadius: '50%',
                  objectFit: 'cover',
                  overflow: 'hidden',
                  transition: '0.3 all',
                }}
              />
            </Box>
            <Box
              sx={{
                flexGrow: '1',
                maxWidth: 'calc(100% - 40px)',
                fontFamily: 'sans-serif',
                fontWeight: '500',
                fontSize: '1rem',
              }}
            >
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    position: 'relative',
                    marginBottom: '8px',
                  }}
                >
                  <Box className="comment-writer">
                    <span
                      style={{
                        padding: '0',
                        color: '#333',
                        fontWeight: '600',
                        textDecoration: 'none',
                      }}
                      className="user-name"
                    >
                      {supportQuestionObj?.createdBy?.fullName}
                    </span>
                  </Box>
                  <Box
                    style={{ lineHeight: '1' }}
                    className="comment-user-info"
                  >
                    <time
                      style={{
                        fontSize: '12px',
                        color: '#333',
                        fontStyle: 'italic',
                      }}
                    >
                      {getTimeAgo(supportQuestionObj?.createdDate)}
                    </time>
                  </Box>
                </Box>
                {(
                  supportQuestionObj?.type === 'subject-question'
                    ? supportQuestionObj?.subject?.trainers.some((trainer) => {
                        return trainer?._id === userId;
                      })
                    : supportQuestionObj?.schedule?.trainer?._id === userId ||
                      isAdmin
                ) ? (
                  <Box>
                    <Autocomplete
                      sx={{ width: '200px' }}
                      id="size-small-outlined"
                      size="small"
                      options={status}
                      value={
                        status.find(
                          (status) => status?.id === statusQuestion
                        ) || null
                      }
                      onChange={handleSupportQuestionStatusChange}
                      getOptionLabel={(option) => option.name}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Status"
                          placeholder="Status..."
                        />
                      )}
                    />
                  </Box>
                ) : (
                  <></>
                )}
              </Box>
              <Box>
                {/* Content */}
                <Box
                  sx={{
                    width: '100%',
                    display: 'inline-block',
                    padding: '15px',
                    background: '#f2f5f7',
                    borderRadius: '4px',
                    color: '#373a3c',
                    fontSize: '16px',
                  }}
                >
                  <Box
                    sx={{
                      maxHeight: '350px',
                      position: 'relative',
                      width: '100%',
                      display: 'inline-block',
                      overflowX: 'auto',
                    }}
                  >
                    <Box
                      sx={{
                        overflowWrap: 'break-word',
                        overflow: 'auto',
                      }}
                    >
                      <Box
                        sx={{
                          fontWeight: 'bold',
                          fontSize: '1.3rem',
                          lineHeight: '1.5',
                          color: '#212529',
                        }}
                      >
                        {supportQuestionObj?.title}
                      </Box>
                      <span title="undefined">
                        {supportQuestionObj?.content}
                      </span>
                    </Box>
                    <Box>
                      {/* Nếu đã hiển thị hết source hoặc không có source thì ẩn đi nút "Xem thêm" */}
                      {!expandedMap[supportQuestionObj?._id] &&
                        supportQuestionObj?.sources.length >= 1 && (
                          <Button
                            sx={{
                              padding: '0',
                              marginBottom: '5px',
                            }}
                            onClick={() =>
                              handleToggleExpand(supportQuestionObj?._id)
                            }
                          >
                            <span>
                              <b style={{ color: 'green' }}>Show</b> info
                              sources <b style={{ fontSize: '1.25rem' }}>↑↓</b>
                            </span>
                          </Button>
                        )}
                      {/* Hiển thị nút "Ẩn đi" khi đã mở rộng nội dung */}
                      {expandedMap[supportQuestionObj?._id] && (
                        <Button
                          sx={{
                            padding: '0',
                            marginBottom: '5px',
                          }}
                          onClick={() =>
                            handleToggleExpand(supportQuestionObj?._id)
                          }
                        >
                          <span>
                            <b style={{ color: 'red' }}>Hidden</b> info sources
                            <b style={{ fontSize: '1.25rem' }}>↑↓</b>
                          </span>
                        </Button>
                      )}
                    </Box>
                    {supportQuestionObj?.sources?.length !== 0 && (
                      <Box
                        sx={{
                          maxHeight: expandedMap[supportQuestionObj?._id]
                            ? '1000px'
                            : '0',
                          overflow: 'hidden',
                          transitionDuration: '300ms',
                          transition:
                            'max-height 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
                        }}
                      >
                        {expandedMap[supportQuestionObj?._id] && (
                          <Grid container spacing={2}>
                            {supportQuestionObj?.sources?.map(
                              (source, index) => (
                                <Grid item xs={6} key={index}>
                                  <Card>
                                    <Box
                                      sx={{
                                        height: '80px',
                                        display: 'flex',
                                        alignItems: 'center',
                                        paddingLeft: '5px',
                                        gap: 1,
                                      }}
                                    >
                                      <Box sx={{ height: '100%' }}>
                                        {source?.type === 'link' ? (
                                          <Box
                                            sx={{
                                              height: '100%',
                                              width: '80px',
                                              display: 'flex',
                                              alignItems: 'center',
                                              justifyContent: 'center',
                                              borderRight:
                                                '1px solid rgba(0, 0, 0, 0.2)',
                                              borderLeft:
                                                '1px solid rgba(0, 0, 0, 0.2)',
                                            }}
                                          >
                                            <LinkIcon fontSize="large" />
                                          </Box>
                                        ) : imageTypes.includes(
                                            source?.name.split('.').pop()
                                          ) ? (
                                          <img
                                            src={source?.url}
                                            alt={source?.name}
                                            style={{
                                              height: '100%',
                                              width: '80px',
                                              objectFit: 'cover',
                                              verticalAlign: 'middle',
                                            }}
                                          />
                                        ) : (
                                          <Box
                                            sx={{
                                              height: '100%',
                                              width: '80px',
                                              display: 'flex',
                                              alignItems: 'center',
                                              justifyContent: 'center',
                                              borderRight:
                                                '1px solid rgba(0, 0, 0, 0.2)',
                                              borderLeft:
                                                '1px solid rgba(0, 0, 0, 0.2)',
                                            }}
                                          >
                                            <UploadFileIcon fontSize="large" />
                                          </Box>
                                        )}
                                      </Box>
                                      <Tooltip
                                        title={source?.url}
                                        placement="top"
                                        arrow
                                      >
                                        <Box
                                          sx={{
                                            overflow: 'hidden',
                                            whiteSpace: 'nowrap',
                                            textOverflow: 'ellipsis',
                                            maxWidth: '100%',
                                          }}
                                        >
                                          <Link
                                            to={source?.url}
                                            target="_blank"
                                          >
                                            {source?.type === 'link'
                                              ? source?.url
                                              : source?.name}
                                          </Link>
                                        </Box>
                                      </Tooltip>
                                    </Box>
                                  </Card>
                                </Grid>
                              )
                            )}
                          </Grid>
                        )}
                      </Box>
                    )}
                  </Box>
                </Box>
              </Box>
            </Box>
          </Box>
          <Box
            sx={{ display: 'flex', marginTop: '25px', alignItems: 'center' }}
          >
            <Box
              sx={{
                marginTop: '0',
                marginRight: '10px',
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGwAAABsCAYAAACPZlfNAAAACXBIWXMAACE4AAAhOAFFljFgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAhBSURBVHgB7Z3bbxRVHMe/u93KvSx3hNpuIw8g14IXFE1aQR/UBBJMTExMF6PyYqS8+GQCxD8AeNDo0y6i0ZiI+CaJSRcSEEJIC2IAuXS5FVFqSwsFehvPb/dsO7Tb7ezMmZ3f7J5P8tsZSgmd+fb3O7/zO7cAfIZhGBFxqRNG12phYXkfNpmZzhGWFHZVWAvdBwKBFviIABgjxKGXv0rYRnklC0M9CaQFPEz3QsROMIWdYFKkKNIi1cEbEsL2IS1eEprHIZGENQprMvjRJCwKzZBQO4R1GPxpFRYz0m1oaUEPLR/er8SMUhDOSHtUzCgeYkaBhStI0mGkE4ltwhrhTpbnJZRR7hHJyS4UANcFE2LViUsM6b5SMZMUtkUIl4CLBOESRjr87Ra3TSh+sYiIsCb5zK7hiofJuF4qQmUjKazejT6ccg8TYjWISzNKVywiIqxZvItGKEapYOIH3CEucRRfYmEHege75TtRhpKQKLNAit1RaLIRF7ZdRY3SsWBSLGqvVkGTCyou1zsVzZFgWqy8cSyabcG0WLZxJJotwbRYjrEtmt0skRIMLZZ96N3Z6mDnLZhMU6PQOCVqJ+XPKySK/4AKuHugUUlUhMZ9Vr/ZsmCy3EQVDN0pVgu1Y7VWy1j5CNaK0i43uUkSadHGTUIstWGyAh2Bxi0iwiy1Z+N6mBzPaoKmENSPN55mRTAdCgtHEuOExpwhUaadEWgKRQTpaRRjMqaH6azQM8i7asbyslweRt6lxSo8maGqrGT1MOldrdB4SU22vtlYHqZ0lFRji6wajPIw7V2smDGyLcvmYdq7+DAqY8zmYb7sd3X33ceXF/bj0K0juNvbDRU0PP02PlncAA8ZlTGGzH9rpJfVROAzuvq6se3k5zjTcQ4qWTvH8yG/zFq5oRGSkSHR018nu8Qu/aRcrLJAGSJTKsGAjeY/DAlmDK8d9hU3e27j29YDUM3CyfMwZ+JMMKBOTslIYfawOviQE+3NGDQMqGbt7FrRwLNZURzN3JgF82U4PHr7FNxgzazlYMRQWEwJJl2uDj7kbOd5uMGiadVgxFBYzHhYHXzIrQe3ceeR49nPowg/UYHKKfPBjFTK6mvBzt29Ajd4ee5zCAVCYMYm+sgIthI+5PidZqimLBDExsoNYEhKI1972NkO9e3XuzWbUDtzKRiSCokh0Zj5cgbv/f4eXLvfBlWUixDYsGgz3l/0DkQpCAyhJcjVFKgj8CHJezcwKTQ5ZU6YXj4Vz89eifXzX8Lqmcu4ipWhPiBU2wldofcLu3zhYQM3L6DvxI8YvPUX8LAHjpg0DZM+/BqB8gnwIamQyKqHaMbo6cSjX79A37HvqXcPFQSrlvtVLGIGCcZzos3gAB7+8Bn6/1Q7hzVUtQI+pprSepaC9R7+RrlYRGjJK/AxYZ6CDfaj74jlFTjWERkghUQfw1MwSjIGu/6FaoKzqxCY7OuplmHX9ppyQiobdIFgJcsKRl6wFGzg2mm4QWjxOvgdfoIN9GPgwjGoJ4CyymXwOySY+gElBxj32jHY+TdUE5xTnTKf08lOsIFbF1N9MNVMeG2rGDsph89hKNiFo1BNqPZNhFa/hSKgkyodSTDaJKX/ykkoQwxGTtiwFeWvfpC6LwJSgt0FI4Izn0IwvACOCJWLBGMpyl/YLPpd01FEJDMexoZJDWPv6NM/aOD+I+vt24PURz/sUiaccuoEVnM7rrITbCRdD/pw4FQbfj7Zht8v/4dC8saKeYh/9CwY0UKCJcCU823d+Hj/aZy57k3U3rBsHpiRDMplmawyRaL9Xi+2xps9E4tmCtRW8Wr/6KyzTOrUAmbsPXQJ59rUrPOyQ8XEckTmOJsvopgEfWQEc6d4Z5MHfQP47vh1eMmKqgpM4ZVwpDTKCHYQjPjjehe6HWR3Klj/zFwwI0EfLEOiV+2WmaULK8CMBH2kBJNraBNgwrGL7fCaJQumgRFD53Ka6zW/gAG9/YM4frkDXrKqKoy5FaxmVg3NlzALFgcDrrX3pFJ6L9m45kkwI5G5GRKMS1g8c6OLFhjCK2pEKv/ei1VgxGMn3Y4sYXseFn87+w+8YtbUJ/BVdDUqJrNK5x+bPjZSsDg8rnq0XC18hkj9rdeXzsWhT9ehtppVdYNOco+bv5BtJ5yd0IsjuBAXgm0xfyGbYDRxz9s0TZNh1BZ8o4ZhZfLhwrRbTZ7Es+2XqDe45Iv1DS7lN2ov8474WCdF5Nqkmdoy8jK9729hyXm0x5hTiWRbtheaQrM31zksOVdgSy8r9SN+Cwn1u2pyfUPOyXrSy7ZAUyjGfdfjzq6UZ4Ho0Og+e8c7d4WwtCmFDo2uM24ozGBp/rIMjfVgOLuqCMi8W0tYnnAuM5ft0Khml9XT+Yi8VgjIyvEuaFRBYu3J5x/YPcc5Dp9uOcuIfUKsKPJEH7ztDbYP3tZH2xce22IRjvaa06LljSOxCMebA2rRLONYLMLxOlJTH00Px4wNvRvHYhFKFv7SDyIzHp3yj4ZS96gKsQjl+6WKENkIfX4mQQJtHznrySmubHArpxhQuxZBaZJEOgQmoRhX9kKgH1QWM0uxyk/PXOuGWITrW0gLb6sTlxiK39uSwrZYGSJxguu7jdADSG+jhKQYq/30TPRstW6LVXCobaM6pFE8xAzTYWxFi+F/4WJGOrEqLYxh4VoN/nQI22mUgkdZQbyIqLAmgx9NwhoNJkKxO2jEGD48tQHenbqUQHqtXFxVhUIVrE+GMYaPeiRbCXcEJEGoMEv7YByke24imWEtWDaM9PFZEaRHB6rlfXiEmek0WVJer8r7hFsdXLf4H+v6TC9ogJXLAAAAAElFTkSuQmCC"
                alt="Img"
                style={{
                  width: '36px',
                  height: '36px',
                  border: '1px solid #dddddd',
                  borderRadius: '50%',
                  objectFit: 'cover',
                  overflow: 'hidden',
                  transition: '0.3 all',
                }}
              />
            </Box>
            <Box sx={{ width: '100%' }}>
              <TextField
                className="textfield-borderRadius"
                margin="dense"
                id="question-reply"
                name="question-reply"
                label="Type something here"
                placeholder="Type something here..."
                multiline
                value={content}
                onChange={handleContentChange}
                fullWidth
              />
            </Box>
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'flex-end',
              width: '100%',
              marginTop: '25px',
              alignItems: 'center',
            }}
          >
            {' '}
            {/* {(supportQuestionObj?.type === 'subject-question'
              ? supportQuestionObj?.subject?.trainers.some((trainer) => {
                  return trainer?._id === userId;
                })
              : supportQuestionObj?.schedule?.trainer?._id === userId ||
                isAdmin) && (
              <Button
                sx={{ marginRight: '20px' }}
                variant="outlined"
                color="success"
              >
                Set up meeting
              </Button>
            )} */}
            <Button variant="contained" onClick={handleAddNewReply}>
              Comment
            </Button>
          </Box>
          <Box
            sx={{
              marginTop: '25px',
              marginLeft: '46px',
              borderTop: '1px solid rgb(228, 232, 236)',
              borderBottom: '1px solid rgb(228, 232, 236)',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              padding: '8px 16px',
              cursor: 'pointer',
            }}
          >
            <Typography sx={{ lineHeight: '24px' }}>
              {supportQuestionObj?.reply
                ? supportQuestionObj?.reply.length > 1
                  ? `${supportQuestionObj?.reply?.length} comments`
                  : `${supportQuestionObj?.reply?.length} comment`
                : ''}
            </Typography>
            <Box>
              <Button
                onClick={handleClick}
                aria-controls="dropdown-menu"
                aria-haspopup="true"
              >
                {selectedItem === 'latest' ? 'Latest' : 'Oldest'}{' '}
                <ImportExportIcon />
              </Button>
              <Menu
                id="dropdown-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={handleClose}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'bottom',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'top',
                }}
                getContentAnchorEl={null}
              >
                <MenuItem
                  sx={{
                    width: '200px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                  onClick={() => handleMenuItemClick('latest')}
                >
                  Latest {selectedItem === 'latest' ? <CheckIcon /> : ''}
                </MenuItem>
                <MenuItem
                  sx={{
                    width: '200px',
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                  onClick={() => handleMenuItemClick('oldest')}
                >
                  Oldest {selectedItem === 'oldest' ? <CheckIcon /> : ''}
                </MenuItem>
              </Menu>
            </Box>
          </Box>
          <Box>
            {replies?.length !== 0 ? (
              <>
                <Box>
                  {/* User Comment */}
                  {replies?.map((reply, index) => (
                    <Reply
                      key={reply?._id}
                      reply={reply}
                      selectedReply={selectedReply}
                      setSelectedReply={setSelectedReply}
                      handleClickOpenDeleteReplyDialog={
                        handleClickOpenDeleteReplyDialog
                      }
                      fetchAllReplies={fetchAllReplies}
                      page={page}
                      repliesIsEditting={repliesIsEditting}
                      setReliesIsEditting={setReliesIsEditting}
                      handleOpenBackdrop={handleOpenBackdrop}
                      handleCloseBackdrop={handleCloseBackdrop}
                    />
                  ))}
                </Box>
                <Box
                  sx={{
                    display: 'flex',
                    marginTop: '25px',
                    justifyContent: 'flex-end',
                  }}
                >
                  <Pagination
                    count={Math.ceil(repliesCount / 8)}
                    page={page}
                    onChange={handlePaginationChanged}
                    variant="outlined"
                    color="primary"
                  />
                </Box>
              </>
            ) : (
              <></>
            )}
          </Box>
          <Dialog
            fullWidth
            maxWidth={'sm'}
            open={openDeleteReplyDialog}
            onClose={handleCloseDeleteReplyDialog}
          >
            <DialogTitle>Delete Reply</DialogTitle>
            <Divider />
            <DialogContent>
              <Typography variant="h6">Do you want to delete reply?</Typography>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseDeleteReplyDialog}>Cancel</Button>
              <Button color="error" onClick={handleDeleteReply}>
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </Box>
      </Box>
      <Backdrop
        open={openBackdrop}
        onClick={handleCloseBackdrop}
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      {/* back-to-top and top-to-back scroll */}
      <Zoom in={trigger}>
        <Button
          onClick={handleScrollTop}
          color="primary"
          sx={{
            position: 'fixed',
            bottom: 16,
            right: 16,
            color: '#ee2c74',
          }}
        >
          <KeyboardArrowUpIcon sx={{ fontSize: '1.75rem' }} />
        </Button>
      </Zoom>
      <Zoom in={!trigger}>
        <Button
          onClick={handleScrollBottom}
          color="primary"
          sx={{
            position: 'fixed',
            top: 16,
            right: 16,
            color: '#ee2c74',
          }}
        >
          <KeyboardArrowDownIcon sx={{ fontSize: '1.75rem' }} />
        </Button>
      </Zoom>
    </LocalizationProvider>
  );
}

export default SupportQuestionDetail;
