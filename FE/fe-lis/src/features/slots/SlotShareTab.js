import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import EditIcon from '@mui/icons-material/Edit';
import LinkIcon from '@mui/icons-material/Link';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import {
  Avatar,
  Backdrop,
  Box,
  Button,
  Card,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Divider,
  Grid,
  IconButton,
  Tooltip,
  Typography,
} from '@mui/material';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import { styled } from '@mui/material/styles';
import dayjs from 'dayjs';
import { React, useEffect, useState } from 'react';
import MaterialsDialog from '../material/MaterialsDialog';
import useAuth from '../../hooks/useAuth';
import {
  useAddNewMaterialsMutation,
  useDeleteMaterialMutation,
  useUpdateMaterialMutation,
} from '../material/materialApiSlice';
import { enqueueSnackbar } from 'notistack';
import { useUpdateScheduleMutation } from '../schedule/scheduleApiSlice';
import { Link } from 'react-router-dom';
const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  '&:not(:last-child)': {
    borderBottom: 0,
  },
  '&:before': {
    display: 'none',
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === 'dark'
      ? 'rgba(255, 255, 255, .05)'
      : 'rgba(0, 0, 0, .03)',
  flexDirection: 'row-reverse',
  '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
    transform: 'rotate(90deg)',
  },
  '& .MuiAccordionSummary-content': {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

const imageTypes = ['png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp'];

const SlotShareTab = ({ schedule, refetch }) => {
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };
  const [selectedMaterial, setSelectedMaterial] = useState(null);
  const { userLogin, isAdmin } = useAuth();
  const [openMaterialsDialog, setOpenMaterialsDialog] = useState(false);

  const handleOpenMaterialsDialog = () => {
    setOpenMaterialsDialog(true);
  };
  const handleCloseMaterialsDialog = () => {
    setOpenMaterialsDialog(false);
  };

  const [openDeleteDialog, setOpenDeleteDialog] = useState(false);
  const handleDeleteClicked = (data) => {
    setSelectedMaterial(data);
    handleOpenDeleteDialog();
  };
  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const handleCloseDeleteDialog = () => {
    setOpenDeleteDialog(false);
  };

  const handleViewMaterial = (data) => {
    setSelectedMaterial(data);
    handleOpenMaterialsDialog();
  };

  const handleAddNewMaterial = () => {
    setSelectedMaterial(null);
    handleOpenMaterialsDialog();
  };
  //  accordion
  const [expanded, setExpanded] = useState([]);

  const handleChange = (panel) => (event, isExpanded) => {
    if (isExpanded) {
      setExpanded((prevExpanded) => [...prevExpanded, panel]);
    } else {
      setExpanded((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    }
  };
  const [
    addNewMaterial,
    {
      data: createdMaterialRes,
      isSuccess: addNewMaterialIsSuccess,
      isLoading: addNewMaterialIsLoading,
      isError: addNewMaterialIsError,
      error: addNewMaterialError,
    },
  ] = useAddNewMaterialsMutation();

  useEffect(() => {
    if (addNewMaterialIsSuccess) {
      try {
        const { createdMaterial } = createdMaterialRes;
        handleAddNewMaterialToSchedule(createdMaterial?._id);
      } catch (error) {
        enqueueSnackbar('Add material to lesson fail ! try again !', {
          variant: 'error',
        });
      }
    }
  }, [addNewMaterialIsSuccess]);

  const [
    updateSchedule,
    {
      isSuccess: updateScheduleIsSuccess,
      isLoading: updateScheduleIsLoading,
      isError: updateScheduleIsError,
      error: updateScheduleError,
    },
  ] = useUpdateScheduleMutation();

  useEffect(() => {
    if (updateScheduleIsLoading) {
      handleOpenBackDrop();
    }
  }, [updateScheduleIsLoading]);
  useEffect(() => {
    if (updateScheduleIsSuccess) {
      enqueueSnackbar('Update Schedule Successful !', { variant: 'success' });
      refetch();
      handleCloseBackDrop();
    }
  }, [updateScheduleIsSuccess]);
  useEffect(() => {
    if (updateScheduleIsError) {
      enqueueSnackbar(
        'Update Schedule Error : ' + updateScheduleError?.data?.message,
        {
          variant: 'error',
        }
      );
      handleCloseBackDrop();
    }
  }, [updateScheduleIsError]);

  const handleAddNewMaterialToSchedule = async (materialId) => {
    try {
      updateSchedule({
        scheduleId: schedule._id,
        material: materialId,
      });
    } catch (error) {
      enqueueSnackbar('Error with handleUpdate Function !', {
        variant: 'error',
      });
    }
  };

  const [
    updateMaterial,
    {
      isSuccess: updateMaterialIsSuccess,
      isError: updateMaterialIsError,
      error: updateMaterialError,
    },
  ] = useUpdateMaterialMutation();

  useEffect(() => {
    if (updateMaterialIsSuccess) {
      enqueueSnackbar('Material updated Successful !', { variant: 'success' });
      handleCloseMaterialsDialog();
      refetch();
    }
  }, [updateMaterialIsSuccess]);

  useEffect(() => {
    if (updateMaterialIsError) {
      enqueueSnackbar(
        'Material updated error !' + updateMaterialError?.data?.message,
        { variant: 'error' }
      );
    }
  }, [updateMaterialIsError]);

  const [
    deleteMaterial,
    {
      isSuccess: deleteMaterialIsSuccess,
      isError: deleteMaterialIsError,
      error: deleteMaterialError,
    },
  ] = useDeleteMaterialMutation();

  const handleDeleteMaterial = async () => {
    try {
      await deleteMaterial({ id: selectedMaterial?._id });
    } catch (error) {
      enqueueSnackbar('Error while delete material', { variant: 'error' });
    }
  };

  useEffect(() => {
    if (deleteMaterialIsSuccess) {
      enqueueSnackbar('Material delete Successful !', { variant: 'success' });
      handleCloseDeleteDialog();
      refetch();
    }
  }, [deleteMaterialIsSuccess]);

  useEffect(() => {
    if (deleteMaterialIsError) {
      enqueueSnackbar(
        'Material delete error !' + deleteMaterialError?.data?.message,
        { variant: 'error' }
      );
    }
  }, [deleteMaterialIsError]);

  return (
    <>
      <Grid
        container
        justifyContent={'space-between'}
        mt={0.000001}
      >
        <Grid item>
          {userLogin._id === schedule.inClass.trainer._id ||
          userLogin._id === schedule.trainer._id ||
          isAdmin ? (
            <Button
              variant="contained"
              color="success"
              onClick={handleAddNewMaterial}
            >
              Add New Materials
            </Button>
          ) : null}
        </Grid>
        <Grid item>
          <Button variant="contained" onClick={() => refetch()}>
            Reload
          </Button>
        </Grid>
      </Grid>
      {schedule &&
        schedule.materials &&
        [...schedule.materials]
          .sort((a, b) => {
            // Assuming the 'content' field is a string or can be compared directly
            return b.createdAt.localeCompare(a.createdAt);
          })
          .map((material) => (
            <Box
              key={material._id}
              mt={2}
              sx={{
                fontFamily: 'sans-serif',
                fontWeight: '400',
                fontSize: '0.875rem',
              }}
            >
              {/* Comment wrapper         */}
              <Box sx={{ marginTop: '25px' }}>
                <Box sx={{ display: 'flex' }}>
                  <Box
                    sx={{
                      marginTop: '0',
                      marginRight: '10px',
                      display: 'block',
                    }}
                  >
                    <Avatar>
                      {material.uploadedBy.email.charAt(0).toUpperCase()}
                    </Avatar>
                  </Box>
                  <Box sx={{ flexGrow: '1', maxWidth: 'calc(100% - 40px)' }}>
                    <Grid container justifyContent={'space-between'}>
                      <Grid item>
                        <Box
                          sx={{
                            display: 'flex',
                            flexDirection: 'column',
                            position: 'relative',
                            marginBottom: '8px',
                          }}
                        >
                          <Box className="comment-writer">
                            <span
                              style={{
                                padding: '0',
                                color: '#333',
                                fontWeight: '600',
                                textDecoration: 'none',
                              }}
                              className="user-name"
                            >
                              {material?.uploadedBy?.fullName}
                            </span>
                          </Box>
                          <Box
                            style={{ lineHeight: '1' }}
                            className="comment-user-info"
                          >
                            <time
                              style={{
                                fontSize: '12px',
                                color: '#333',
                                fontStyle: 'italic',
                              }}
                            >
                              {dayjs(material?.createdAt).format('DD/MM/YYYY')}
                            </time>
                          </Box>
                        </Box>
                      </Grid>
                      <Grid item>
                        {userLogin._id === material.uploadedBy._id && (
                          <>
                            <IconButton
                              onClick={() => handleViewMaterial(material)}
                            >
                              <EditIcon fontSize="small" color="success" />
                            </IconButton>
                            <IconButton
                              onClick={() => handleDeleteClicked(material)}
                            >
                              <DeleteForeverIcon
                                fontSize="small"
                                color="error"
                              />
                            </IconButton>
                          </>
                        )}
                      </Grid>
                    </Grid>
                    <Box>
                      {/* Content */}
                      <Box
                        sx={{
                          width: '100%',
                          display: 'inline-block',
                          padding: '15px 8px 15px 15px',
                          background: '#f2f5f7',
                          borderRadius: '4px',
                          color: '#373a3c',
                          fontSize: '16px',
                        }}
                      >
                        <Box
                          sx={{
                            maxHeight: '350px',
                            position: 'relative',
                            width: '100%',
                            display: 'inline-block',
                            overflowX: 'auto',
                          }}
                        >
                          <Box
                            sx={{
                              overflowWrap: 'break-word',
                              overflow: 'auto',
                            }}
                          >
                            <span title="undefined">
                              <Typography>{material?.content}</Typography>
                            </span>
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                    <Accordion
                      expanded={expanded.includes(`panel${material._id}`)}
                      onChange={handleChange(`panel${material._id}`)}
                    >
                      <AccordionSummary
                        aria-controls="panel1d-content"
                        id="panel1d-header"
                        sx={{
                          padding: '4px 12px',
                          fontSize: '16px',
                          alignItems: 'center',
                          justifyContent: 'center',
                          position: 'relative',
                          boxSizing: 'border-box',
                          outline: '0px',
                          border: '0px',
                          margin: '0px',
                          borderRadius: '0px',
                          cursor: 'pointer',
                          userSelect: 'none',
                          verticalAlign: 'middle',
                          appearance: 'none',
                          textDecoration: 'none',
                          color: 'inherit',
                          display: 'flex',
                          minHeight: '48px',
                          transition:
                            'min-height 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
                          backgroundColor: 'rgba(0, 0, 0, 0.03)',
                          flexDirection: 'row-reverse',
                        }}
                      >
                        <Box
                          sx={{
                            display: 'flex',
                            alignItems: 'center',
                            marginLeft: '10px',
                          }}
                        >
                          <span
                            style={{
                              fontWeight: '600',
                              marginRight: '0.5rem',
                            }}
                          >
                            Materials List
                          </span>
                        </Box>
                      </AccordionSummary>
                      <AccordionDetails
                        sx={{
                          padding: '10px 25px',
                          color: 'rgba(0, 0, 0, 0.87)',
                        }}
                      >
                        {material?.sources?.length !== 0
                          ? material?.sources?.map((source, index) => (
                              <Grid item xs={12} key={index}>
                                <Card>
                                  <Box
                                    sx={{
                                      height: '80px',
                                      display: 'flex',
                                      alignItems: 'center',
                                      paddingLeft: '5px',
                                      gap: 1,
                                    }}
                                  >
                                    <Box sx={{ height: '100%' }}>
                                      {source?.type === 'link' ? (
                                        <Box
                                          sx={{
                                            height: '100%',
                                            width: '80px',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            borderRight:
                                              '1px solid rgba(0, 0, 0, 0.2)',
                                            borderLeft:
                                              '1px solid rgba(0, 0, 0, 0.2)',
                                          }}
                                        >
                                          <LinkIcon fontSize="large" />
                                        </Box>
                                      ) : imageTypes.includes(
                                          source?.name.split('.').pop()
                                        ) ? (
                                        <img
                                          src={source?.url}
                                          alt={source?.name}
                                          style={{
                                            height: '100%',
                                            width: '80px',
                                            objectFit: 'cover',
                                            verticalAlign: 'middle',
                                          }}
                                        />
                                      ) : (
                                        <Box
                                          sx={{
                                            height: '100%',
                                            width: '80px',
                                            display: 'flex',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            borderRight:
                                              '1px solid rgba(0, 0, 0, 0.2)',
                                            borderLeft:
                                              '1px solid rgba(0, 0, 0, 0.2)',
                                          }}
                                        >
                                          <UploadFileIcon fontSize="large" />
                                        </Box>
                                      )}
                                    </Box>
                                    <Tooltip
                                      title={source?.url}
                                      placement="top"
                                      arrow
                                    >
                                      <Box
                                        sx={{
                                          overflow: 'hidden',
                                          whiteSpace: 'nowrap',
                                          textOverflow: 'ellipsis',
                                          maxWidth: '100%',
                                        }}
                                      >
                                        <Link to={source?.url} target="_blank">
                                          {source?.type === 'link'
                                            ? source?.url
                                            : source?.name}
                                        </Link>
                                      </Box>
                                    </Tooltip>
                                  </Box>
                                </Card>
                              </Grid>
                            ))
                          : null}
                      </AccordionDetails>
                    </Accordion>
                  </Box>
                </Box>
              </Box>
            </Box>
          ))}
      {schedule &&
        schedule.lessons &&
        schedule.lessons.map((lesson) =>
          [...lesson.materials]
            .sort((a, b) => {
              // Assuming the 'content' field is a string or can be compared directly
              return b.createdAt.localeCompare(a.createdAt);
            })
            .map((material) => (
              <Box
                key={material._id}
                mt={2}
                sx={{
                  fontFamily: 'sans-serif',
                  fontWeight: '400',
                  fontSize: '0.875rem',
                }}
              >
                {/* Comment wrapper         */}
                <Box sx={{ marginTop: '25px' }}>
                  <Box sx={{ display: 'flex' }}>
                    <Box
                      sx={{
                        marginTop: '0',
                        marginRight: '10px',
                        display: 'block',
                      }}
                    >
                      <Avatar>
                        {material.uploadedBy.email.charAt(0).toUpperCase()}
                      </Avatar>
                    </Box>
                    <Box sx={{ flexGrow: '1', maxWidth: 'calc(100% - 40px)' }}>
                      <Grid container justifyContent={'space-between'}>
                        <Grid item>
                          <Box
                            sx={{
                              display: 'flex',
                              flexDirection: 'column',
                              position: 'relative',
                              marginBottom: '8px',
                            }}
                          >
                            <Box className="comment-writer">
                              <span
                                style={{
                                  padding: '0',
                                  color: '#333',
                                  fontWeight: '600',
                                  textDecoration: 'none',
                                }}
                                className="user-name"
                              >
                                {material?.uploadedBy?.fullName}
                              </span>
                            </Box>
                            <Box
                              style={{ lineHeight: '1' }}
                              className="comment-user-info"
                            >
                              <time
                                style={{
                                  fontSize: '12px',
                                  color: '#333',
                                  fontStyle: 'italic',
                                }}
                              >
                                {dayjs(material?.createdAt).format(
                                  'DD/MM/YYYY'
                                )}
                              </time>
                            </Box>
                          </Box>
                        </Grid>
                        <Grid item>
                          {/* <IconButton>
                          <EditIcon fontSize="small" color="success" />
                        </IconButton>
                        <IconButton>
                          <DeleteForeverIcon fontSize="small" color="error" />
                        </IconButton> */}
                        </Grid>
                      </Grid>
                      <Box>
                        {/* Content */}
                        <Box
                          sx={{
                            width: '100%',
                            display: 'inline-block',
                            padding: '15px 8px 15px 15px',
                            background: '#f2f5f7',
                            borderRadius: '4px',
                            color: '#373a3c',
                            fontSize: '16px',
                          }}
                        >
                          <Box
                            sx={{
                              maxHeight: '350px',
                              position: 'relative',
                              width: '100%',
                              display: 'inline-block',
                              overflowX: 'auto',
                            }}
                          >
                            <Box
                              sx={{
                                overflowWrap: 'break-word',
                                overflow: 'auto',
                              }}
                            >
                              <span title="undefined">
                                <Typography>{material?.content}</Typography>
                              </span>
                            </Box>
                          </Box>
                        </Box>
                      </Box>
                      <Accordion
                        expanded={expanded.includes(`panel${material._id}`)}
                        onChange={handleChange(`panel${material._id}`)}
                      >
                        <AccordionSummary
                          aria-controls="panel1d-content"
                          id="panel1d-header"
                          sx={{
                            padding: '4px 12px',
                            fontSize: '16px',
                            alignItems: 'center',
                            justifyContent: 'center',
                            position: 'relative',
                            boxSizing: 'border-box',
                            outline: '0px',
                            border: '0px',
                            margin: '0px',
                            borderRadius: '0px',
                            cursor: 'pointer',
                            userSelect: 'none',
                            verticalAlign: 'middle',
                            appearance: 'none',
                            textDecoration: 'none',
                            color: 'inherit',
                            display: 'flex',
                            minHeight: '48px',
                            transition:
                              'min-height 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms',
                            backgroundColor: 'rgba(0, 0, 0, 0.03)',
                            flexDirection: 'row-reverse',
                          }}
                        >
                          <Box
                            sx={{
                              display: 'flex',
                              alignItems: 'center',
                              marginLeft: '10px',
                            }}
                          >
                            <span
                              style={{
                                fontWeight: '600',
                                marginRight: '0.5rem',
                              }}
                            >
                              Materials List
                            </span>
                          </Box>
                        </AccordionSummary>
                        <AccordionDetails
                          sx={{
                            padding: '10px 25px',
                            color: 'rgba(0, 0, 0, 0.87)',
                          }}
                        >
                          {material?.sources?.length !== 0
                            ? material?.sources?.map((source, index) => (
                                <Grid item xs={12} key={index}>
                                  <Card>
                                    <Grid container alignItems={'center'}>
                                      <Grid item xs={1} padding={2}>
                                        {source?.type === 'link' ? (
                                          <LinkIcon fontSize="large" />
                                        ) : (
                                          <UploadFileIcon fontSize="large" />
                                        )}
                                      </Grid>
                                      <Tooltip
                                        title={source?.url}
                                        placement="top"
                                        arrow
                                      >
                                        <Grid item xs={10} padding={2}>
                                          <a
                                            href={source?.url}
                                            target="_blank"
                                            rel="noopener"
                                          >
                                            {source?.name}
                                          </a>
                                        </Grid>
                                      </Tooltip>
                                    </Grid>
                                  </Card>
                                </Grid>
                              ))
                            : null}
                        </AccordionDetails>
                      </Accordion>
                    </Box>
                  </Box>
                </Box>
              </Box>
            ))
        )}

      <MaterialsDialog
        material={selectedMaterial}
        openMaterialsDialog={openMaterialsDialog}
        addNewMaterial={addNewMaterial}
        addNewMaterialIsSuccess={addNewMaterialIsSuccess}
        addNewMaterialIsError={addNewMaterialIsError}
        addNewMaterialIsLoading={addNewMaterialIsLoading}
        addNewMaterialError={addNewMaterialError}
        updateMaterial={updateMaterial}
        handleCloseMaterialsDialog={handleCloseMaterialsDialog}
      />
      <Dialog
        fullWidth
        maxWidth={'sm'}
        open={openDeleteDialog}
        onClose={handleCloseDeleteDialog}
      >
        <DialogTitle>Delete Material</DialogTitle>
        <Divider />
        <DialogContent>
          <Typography variant="h6">
            You want delete material content: {selectedMaterial?.content}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseDeleteDialog}>Cancel</Button>
          <Button color="error" onClick={handleDeleteMaterial}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>

      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={openBackDrop}
        onClick={handleCloseBackDrop}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </>
  );
};

export default SlotShareTab;
