import { React, useEffect, useState, useRef } from 'react';
import {
  Paper,
  Box,
  Grid,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  IconButton,
  TextField,
  Tooltip,
  Backdrop,
  CircularProgress,
} from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { ref, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import { storage } from '../../config/firebase';
import Card from '@mui/material/Card';
import CloseIcon from '@mui/icons-material/Close';
import DeleteIcon from '@mui/icons-material/Delete';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import LinkIcon from '@mui/icons-material/Link';
import useAuth from '../../hooks/useAuth';
const imageTypes = ['png', 'jpg', 'jpeg', 'gif', 'bmp', 'webp'];
const urlPattern = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;
const SupportQuestionDialog = ({
  schedule,
  openSupportQuestionDialog,
  handleCloseSupportQuestionDialog,
  selectedSupportQuestion,
  addNewSupportQuestion,
  addNewSupportQuestionIsSuccess,
  addNewSupportQuestionIsLoading,
  addNewSupportQuestionIsError,
  addNewSupportQuestionError,
  updateSupportQuestion,
  updateSupportQuestionIsSuccess,
  updateSupportQuestionIsLoading,
  updateSupportQuestionIsError,
  updateSupportQuestionError,
  fetchAllSupportQuestions,
  page,
  setPage,
}) => {
  const { userLogin, isTrainer, isTrainee, isAdmin } = useAuth();
  const userId = userLogin?._id;
  const { enqueueSnackbar } = useSnackbar();
  const [sources, setSources] = useState([]);
  const [selectedFile, setSelectedFile] = useState(null);
  const [link, setLink] = useState('');
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [validTitle, setValidTitle] = useState(false);
  const [validContent, setValidContent] = useState(false);
  const [validLink, setValidLink] = useState(false);
  const [progress, setProgress] = useState(0);
  const fileInputRef = useRef(null);

  useEffect(() => {
    setTitle(selectedSupportQuestion?.title || '');
    setContent(selectedSupportQuestion?.content || '');
    setSources(selectedSupportQuestion?.sources || []);
  }, [selectedSupportQuestion]);

  const [openAddLinkDialog, setOpenAddLinkDialog] = useState(false);
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };
  const handleOpenAddLinkDialog = () => {
    setOpenAddLinkDialog(true);
  };
  const handleCloseAddLinkDialog = () => {
    setOpenAddLinkDialog(false);
  };
  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const handleContentChange = (event) => {
    setContent(event.target.value);
  };

  const handleFileChange = (event) => {
    if (selectedFile) {
      setSelectedFile('');
    } else {
      setSelectedFile(event.target.files[0]);
    }
  };

  const handleLinkChange = (event) => {
    setLink(event.target.value);
  };

  const handleSaveLink = () => {
    const source = { name: 'Link', url: link, type: 'link' };
    setSources([...sources, source]);
    handleCloseAddLinkDialog();
    setLink('');
  };

  const handleDeleteSource = (selectedSource) => {
    const newSources = sources.filter(
      (oldSources) => oldSources !== selectedSource
    );
    setSources(newSources);
  };

  const handleUpload = () => {
    if (!selectedFile) {
      // Xử lý trường hợp không có file được chọn
      return;
    }
    handleOpenBackDrop();
    const fileName = selectedFile.name;
    const storageRef = ref(storage, `/files/${fileName}`);
    const uploadTask = uploadBytesResumable(storageRef, selectedFile);
    uploadTask.on(
      'state_changed',
      (snapshot) => {
        const uploaded = Math.floor(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
        setProgress(uploaded);
      },
      (error) => {
        console.log(error);
      },
      () => {
        getDownloadURL(uploadTask.snapshot.ref).then((url) => {
          setSelectedFile(null);
          const source = { name: selectedFile?.name, url, type: 'file' };
          setSources([...sources, source]);
          enqueueSnackbar(`Upload ${fileName} successful !`, {
            variant: 'success',
          });
          setProgress(0);
          handleCloseBackDrop();
        });
      }
    );
  };

  useEffect(() => {
    setValidTitle(title.trim() !== '');
  }, [title]);

  useEffect(() => {
    setValidContent(content.trim() !== '');
  }, [content]);

  useEffect(() => {
    setValidLink(urlPattern.test(link));
  }, [link]);

  const canSave = validTitle && validContent;

  const handleAddNewSupportQuestion = async () => {
    if (title.length > 200) {
      enqueueSnackbar('Title must be less than 200 characters', {
        variant: 'error',
      });
    } else {
      try {
        await addNewSupportQuestion({
          type: 'support-question',
          scheduleId: schedule?._id,
          title: title,
          content: content,
          sources: sources,
          createdBy: userId,
        });
      } catch (error) {
        enqueueSnackbar('Create failed!: ' + error, {
          variant: 'error',
        });
      }
    }
  };

  useEffect(() => {
    if (addNewSupportQuestionIsLoading) {
      handleOpenBackDrop();
    } else {
      handleCloseBackDrop();
    }
  }, [addNewSupportQuestionIsLoading]);

  useEffect(() => {
    if (addNewSupportQuestionIsSuccess) {
      enqueueSnackbar('Add new question information is successfully!', {
        variant: 'success',
      });
      handleCloseSupportQuestionDialog();
      fetchAllSupportQuestions(1);
      setPage(1);
      setTitle('');
      setContent('');
      setSources([]);
    }
  }, [addNewSupportQuestionIsSuccess]);

  // update question
  const handleUpdateSupportQuestion = async () => {
    try {
      await updateSupportQuestion({
        questionId: selectedSupportQuestion?._id,
        title: title,
        content: content,
        sources: sources,
      });
    } catch (error) {
      enqueueSnackbar('Update failed!: ' + error, {
        variant: 'error',
      });
    }
  };

  useEffect(() => {
    if (updateSupportQuestionIsLoading) {
      handleOpenBackDrop();
    } else {
      handleCloseBackDrop();
    }
  }, [updateSupportQuestionIsLoading]);

  useEffect(() => {
    if (updateSupportQuestionIsSuccess) {
      enqueueSnackbar('Update question information is successfully!', {
        variant: 'success',
      });
      handleCloseSupportQuestionDialog();
      fetchAllSupportQuestions(page);
    }
  }, [updateSupportQuestionIsSuccess]);
  return (
    <Grid
      sx={{
        p: 2,
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <Dialog
        open={openSupportQuestionDialog}
        keepMounted
        onClose={handleCloseSupportQuestionDialog}
        aria-describedby="post-new-question-dialog-slide-description"
        maxWidth="md"
      >
        <DialogTitle sx={{ padding: '0' }}>
          <Paper
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              width: '100%',
              paddingRight: '10px',
              paddingLeft: '10px',
              alignItems: 'center',
            }}
          >
            <Box>Support Question</Box>
            <Box>
              <IconButton onClick={handleCloseSupportQuestionDialog}>
                <CloseIcon fontSize="inherit" />
              </IconButton>
            </Box>
          </Paper>
        </DialogTitle>
        <DialogContent sx={{ width: '100%' }}>
          <Grid container>
            <Grid item xs={12}>
              <TextField
                sx={{ mt: 2 }}
                margin="dense"
                label="Title"
                type="text"
                fullWidth
                required
                value={title}
                onChange={handleTitleChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                margin="dense"
                id="question-content"
                name="question-content"
                label="Content"
                value={content}
                required
                multiline
                rows={4}
                onChange={handleContentChange}
                fullWidth
              />
            </Grid>
            <Grid sx={{ mt: 2 }} item container xs={9}>
              <Grid item>
                <label htmlFor="upload-file">
                  <input
                    style={{ display: 'none' }}
                    ref={fileInputRef}
                    id="upload-file"
                    name="upload-file"
                    type="file"
                    onChange={handleFileChange}
                  />
                  <Tooltip title="File upload" placement="bottom">
                    <IconButton
                      size="large"
                      sx={{
                        marginRight: '20px',
                      }}
                      color="primary"
                      variant="outlined"
                      component="span"
                    >
                      <UploadFileIcon />
                    </IconButton>
                  </Tooltip>
                </label>
              </Grid>
              <Grid item>
                <Tooltip title="Add link" placement="bottom">
                  <IconButton
                    size="large"
                    color="primary"
                    variant="outlined"
                    onClick={handleOpenAddLinkDialog}
                  >
                    <LinkIcon />
                  </IconButton>
                </Tooltip>
              </Grid>
            </Grid>
            <Grid
              sx={{ mt: 3 }}
              container
              item
              xs={3}
              display={'inline-flex'}
              justifyContent={'flex-end'}
              alignItems={'center'}
            >
              {selectedSupportQuestion ? (
                <>
                  <Button
                    color="success"
                    variant="contained"
                    disabled={!canSave}
                    onClick={handleUpdateSupportQuestion}
                  >
                    Update
                  </Button>
                </>
              ) : (
                <>
                  <Button
                    onClick={handleCloseSupportQuestionDialog}
                    sx={{ marginRight: '10px' }}
                  >
                    Cancel
                  </Button>
                  <Button
                    color="success"
                    variant="contained"
                    disabled={!canSave}
                    onClick={handleAddNewSupportQuestion}
                  >
                    Save
                  </Button>
                </>
              )}
            </Grid>
            {/* question file display */}
            {selectedFile !== null && (
              <Grid sx={{ mt: 2 }} item xs={12}>
                <Card>
                  <Grid container alignItems={'center'}>
                    <Grid item xs={1} padding={2}>
                      <UploadFileIcon fontSize="large" />
                    </Grid>
                    <Tooltip title={selectedFile?.name} placement="top" arrow>
                      <Grid
                        sx={{ overflow: 'hidden', textOverflow: 'ellipsis' }}
                        item
                        xs={9}
                        padding={2}
                      >
                        {selectedFile?.name}
                      </Grid>
                    </Tooltip>
                    <Grid item xs={1} padding={2}>
                      <Button variant="contained" onClick={handleUpload}>
                        Upload
                      </Button>
                    </Grid>
                  </Grid>
                </Card>
              </Grid>
            )}
            {/* question card */}
            {sources?.length !== 0
              ? sources?.map((source, index) => (
                  <Grid sx={{ mt: 2 }} item xs={12} key={index}>
                    <Card>
                      <Box
                        sx={{
                          display: 'flex',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}
                      >
                        <Box
                          sx={{
                            height: '80px',
                            display: 'flex',
                            alignItems: 'center',
                            paddingLeft: '5px',
                            gap: 1,
                          }}
                        >
                          <Box sx={{ height: '100%' }}>
                            {source?.type === 'link' ? (
                              <Box
                                sx={{
                                  height: '100%',
                                  width: '80px',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                  borderRight: '1px solid rgba(0, 0, 0, 0.2)',
                                  borderLeft: '1px solid rgba(0, 0, 0, 0.2)',
                                }}
                              >
                                <LinkIcon fontSize="large" />
                              </Box>
                            ) : imageTypes.includes(
                                source?.name.split('.').pop()
                              ) ? (
                              <img
                                src={source?.url}
                                alt={source?.name}
                                style={{
                                  height: '100%',
                                  width: '80px',
                                  objectFit: 'cover',
                                  verticalAlign: 'middle',
                                }}
                              />
                            ) : (
                              <Box
                                sx={{
                                  height: '100%',
                                  width: '80px',
                                  display: 'flex',
                                  alignItems: 'center',
                                  justifyContent: 'center',
                                  borderRight: '1px solid rgba(0, 0, 0, 0.2)',
                                  borderLeft: '1px solid rgba(0, 0, 0, 0.2)',
                                }}
                              >
                                <UploadFileIcon fontSize="large" />
                              </Box>
                            )}
                          </Box>
                          <Tooltip title={source?.url} placement="top" arrow>
                            <Box
                              sx={{
                                overflow: 'hidden',
                                whiteSpace: 'nowrap',
                                textOverflow: 'ellipsis',
                                maxWidth: '100%',
                              }}
                            >
                              <Link to={source?.url} target="_blank">
                                {source?.type === 'link'
                                  ? source?.url
                                  : source?.name}
                              </Link>
                            </Box>
                          </Tooltip>
                        </Box>
                        <Box sx={{ marginRight: '10px' }}>
                          <IconButton
                            color="error"
                            onClick={() => handleDeleteSource(source)}
                          >
                            <DeleteIcon />
                          </IconButton>
                        </Box>
                      </Box>
                    </Card>
                  </Grid>
                ))
              : null}
          </Grid>
        </DialogContent>
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>

      <Dialog open={openAddLinkDialog} onClose={handleCloseAddLinkDialog}>
        <DialogTitle>Add Link</DialogTitle>
        <DialogContent>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              gap: 2,
              pt: 1,
            }}
          >
            <TextField
              size="small"
              id="link"
              name="link"
              label="Link"
              value={link}
              fullWidth
              onChange={handleLinkChange}
            />
            <Button
              sx={{ height: '40px' }}
              size="small"
              variant="contained"
              color="success"
              onClick={() => handleSaveLink()}
              disabled={!validLink}
            >
              Save
            </Button>
            <Button
              sx={{ height: '40px' }}
              variant="contained"
              color="error"
              onClick={handleCloseAddLinkDialog}
            >
              Cancel
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </Grid>
  );
};

export default SupportQuestionDialog;
