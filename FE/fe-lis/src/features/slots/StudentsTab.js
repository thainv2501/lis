import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import CloseIcon from "@mui/icons-material/Close";
import {
  Avatar,
  Backdrop,
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  TextField,
} from "@mui/material";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import { styled } from "@mui/material/styles";
import { React, useEffect, useState } from "react";
import {
  useCombineNewGroupsMutation,
  useGetGroupsByScheduleIdQuery,
  useMoveStudentToGroupMutation,
  useRemoveStudentOutOfGroupMutation,
} from "../groups/groupsApiSlice";
import { enqueueSnackbar } from "notistack";
import StarsIcon from "@mui/icons-material/Stars";
import useAuth from "../../hooks/useAuth";
import { green } from "@mui/material/colors";

const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
  border: `1px solid ${theme.palette.divider}`,
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
}));

const AccordionSummary = styled((props) => (
  <MuiAccordionSummary
    expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.9rem" }} />}
    {...props}
  />
))(({ theme }) => ({
  backgroundColor:
    theme.palette.mode === "dark"
      ? "rgba(255, 255, 255, .05)"
      : "rgba(0, 0, 0, .03)",
  flexDirection: "row-reverse",
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(90deg)",
  },
  "& .MuiAccordionSummary-content": {
    marginLeft: theme.spacing(1),
  },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
  padding: theme.spacing(2),
  borderTop: "1px solid rgba(0, 0, 0, .125)",
}));

const StudentsTab = ({ schedule }) => {
  const { isAdmin, userLogin } = useAuth();
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };
  const [selectedMember, setSelectedMember] = useState(null);
  const [oldGroup, setOldGroup] = useState(null);
  const [newGroup, setNewGroup] = useState(null);
  const [openMoveStudentDialog, setMoveStudentDialog] = useState(false);
  const handleClickOpenMoveStudentDialog = (member, group) => {
    setSelectedMember(member);
    setOldGroup(group);
    setMoveStudentDialog(true);
  };

  const handleCloseMoveStudentDialog = () => {
    setMoveStudentDialog(false);
  };

  // open dialog remove student from group
  const [openRemoveStudentDialog, setRemoveStudentDialog] = useState(false);
  const handleClickOpenRemoveStudentDialog = (member, group) => {
    setOldGroup(group);
    setSelectedMember(member);
    setRemoveStudentDialog(true);
  };

  const handleCloseRemoveStudentDialog = () => {
    setRemoveStudentDialog(false);
  };

  //  accordion
  const [expanded, setExpanded] = useState([]);

  const handleChange = (panel) => (event, isExpanded) => {
    if (isExpanded) {
      setExpanded((prevExpanded) => [...prevExpanded, panel]);
    } else {
      setExpanded((prevExpanded) =>
        prevExpanded.filter((item) => item !== panel)
      );
    }
  };

  const {
    data: groupsData,
    isSuccess: getGroupsByScheduleIdIsSuccess,
    isLoading: getGroupsByScheduleIdIsLoading,
    isError: getGroupsByScheduleIdIsError,
    error: getGroupsByScheduleIdError,
    refetch,
  } = useGetGroupsByScheduleIdQuery(schedule._id);

  useEffect(() => {
    refetch();
  }, []);

  const [groups, setGroups] = useState([]);

  useEffect(() => {
    if (groupsData) {
      setGroups(groupsData);
    }
  }, [groupsData]);

  const [remainingStudents, setRemainingStudents] = useState([]);

  useEffect(() => {
    const studentsInGroups = new Set(
      groups.flatMap((group) => group.members.map((student) => student._id))
    );
    const studentsInClass = schedule.inClass.students;

    // Find students who are not in any group
    const remainingStudents = studentsInClass.filter((student) => {
      const studentId = student._id;
      return !studentsInGroups.has(studentId);
    });

    setRemainingStudents(remainingStudents);
  }, [groups, schedule]);

  const [
    moveStudentToGroup,
    {
      isSuccess: moveStudentToGroupIsSuccess,
      isError: moveStudentToGroupIsError,
      isLoading: moveStudentToGroupIsLoading,
      error: moveStudentToGroupError,
    },
  ] = useMoveStudentToGroupMutation();

  const handleMoveStudentToGroup = async () => {
    try {
      await moveStudentToGroup({
        oldGroupId: oldGroup?._id,
        newGroupId: newGroup,
        studentId: selectedMember._id,
      });
    } catch (error) {
      enqueueSnackbar("Error with move Function " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (moveStudentToGroupIsSuccess) {
      enqueueSnackbar("Student moved !", {
        variant: "success",
      });
      refetch();
      handleCloseMoveStudentDialog();
      handleCloseBackDrop();
    }
    if (moveStudentToGroupIsError) {
      enqueueSnackbar(
        "Error with remove  !" + moveStudentToGroupError?.data?.message,
        {
          variant: "error",
        }
      );
      handleCloseBackDrop();
    }
    if (moveStudentToGroupIsLoading) {
      handleOpenBackDrop();
    }
  }, [
    moveStudentToGroupIsSuccess,
    moveStudentToGroupIsError,
    moveStudentToGroupIsLoading,
  ]);

  const [
    removeStudentOutOfGroup,
    {
      isSuccess: removeStudentToGroupIsSuccess,
      isError: removeStudentToGroupIsError,
      isLoading: removeStudentToGroupIsLoading,
      error: removeStudentToGroupError,
    },
  ] = useRemoveStudentOutOfGroupMutation();

  const handleRemoveStudentOutOfGroup = async () => {
    try {
      await removeStudentOutOfGroup({
        groupId: oldGroup._id,
        studentId: selectedMember._id,
      });
    } catch (error) {
      enqueueSnackbar("Error with remove Function !" + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (removeStudentToGroupIsSuccess) {
      enqueueSnackbar("Student removed !", {
        variant: "success",
      });
      refetch();
      handleCloseRemoveStudentDialog();
      handleCloseBackDrop();
    }
    if (removeStudentToGroupIsError) {
      enqueueSnackbar(
        "Error with remove  !" + removeStudentToGroupError?.data?.message,
        {
          variant: "error",
        }
      );
      handleCloseBackDrop();
    }
    if (removeStudentToGroupIsLoading) {
      handleOpenBackDrop();
    }
  }, [
    removeStudentToGroupIsSuccess,
    removeStudentToGroupIsError,
    removeStudentToGroupIsLoading,
  ]);

  const [
    combineNewGroup,
    {
      isSuccess: combineNewGroupIsSuccess,
      isError: combineNewGroupIsError,
      isLoading: combineNewGroupIsLoading,
      error: combineNewGroupError,
    },
  ] = useCombineNewGroupsMutation();

  const handleCombineNewGroup = async () => {
    try {
      await combineNewGroup({
        scheduleId: schedule._id,
        students: remainingStudents,
        groupNumber:
          groups.length >= 1 ? groups[groups.length - 1].groupNumber + 1 : 1,
      });
    } catch (error) {
      enqueueSnackbar("Error with combine function : " + error, {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (combineNewGroupIsLoading) {
      handleOpenBackDrop();
    }
    if (combineNewGroupIsSuccess) {
      refetch();
      handleCloseBackDrop();
    }
    if (combineNewGroupIsError) {
      handleCloseBackDrop();
      enqueueSnackbar(
        "Combine Error : " + combineNewGroupError?.data?.message,
        { variant: "error" }
      );
    }
  }, [
    combineNewGroupIsSuccess,
    combineNewGroupIsError,
    combineNewGroupIsLoading,
  ]);

  return (
    <>
      <Box
        sx={{
          marginBottom: "20px",
          borderRadius: "4px",
        }}
      >
        {remainingStudents.length > 0 && (
          <Accordion
            expanded={expanded.includes("panel1")}
            onChange={handleChange("panel1")}
          >
            <AccordionSummary
              aria-controls="panel1d-content"
              id="panel1d-header"
              sx={{
                padding: "4px 12px",
                fontSize: "16px",
                alignItems: "center",
                justifyContent: "center",
                position: "relative",
                boxSizing: "border-box",
                outline: "0px",
                border: "0px",
                margin: "0px",
                borderRadius: "0px",
                cursor: "pointer",
                userSelect: "none",
                verticalAlign: "middle",
                appearance: "none",
                textDecoration: "none",
                color: "inherit",
                display: "flex",
                minHeight: "48px",
                transition:
                  "min-height 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                backgroundColor: "rgba(0, 0, 0, 0.03)",
                flexDirection: "row-reverse",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  marginLeft: "10px",
                }}
              >
                <span
                  style={{
                    fontWeight: "600",
                    marginRight: "0.5rem",
                  }}
                  title="Waiting list"
                  value="Waiting list"
                >
                  Waiting list
                </span>
                <span>{`(${remainingStudents.length} students)`}</span>
              </Box>
            </AccordionSummary>
            <AccordionDetails
              sx={{
                padding: "10px 25px",
                color: "rgba(0, 0, 0, 0.87)",
              }}
            >
              {userLogin._id === schedule?.trainer?._id && (
                <Button onClick={handleCombineNewGroup}>
                  Combine to a group
                </Button>
              )}
              {isAdmin && (
                <Button onClick={handleCombineNewGroup}>
                  Combine to a group
                </Button>
              )}
              {remainingStudents.map((student) => (
                <>
                  <Box sx={{ minHeight: "30px" }} key={student._id}>
                    <Box sx={{ marginBottom: "4px" }}>
                      <Box sx={{ marginBottom: "1rem", marginTop: "1rem" }}>
                        <Box
                          sx={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <Box
                            sx={{
                              display: "flex",
                              alignItems: "center",
                              fontWeight: "600",
                              flex: "0 0 auto",
                              width: "50%",
                            }}
                          >
                            <Avatar sx={{ backgroundColor: green[500] }}>
                              {student.email.charAt(0).toUpperCase()}
                            </Avatar>
                            <Box
                              sx={{
                                display: "flex",
                                flexWrap: "wrap",
                              }}
                            >
                              <Box
                                sx={{
                                  flex: "0 0 auto",
                                  width: "100%",
                                  paddingRight: "0.5rem",
                                  marginTop: "0",
                                }}
                              ></Box>
                              <Box
                                sx={{
                                  flex: "0 0 auto",
                                  width: "100%",
                                  paddingRight: "0.5rem",
                                  marginTop: "0",
                                }}
                              >
                                <span
                                  style={{
                                    marginLeft: "0.5rem",
                                    marginRight: "0.25rem",
                                  }}
                                >
                                  {student.fullName}
                                </span>
                              </Box>
                              <Box
                                sx={{
                                  flex: "0 0 auto",
                                  width: "100%",
                                  paddingRight: "0.5rem",
                                  marginTop: "0",
                                }}
                              >
                                <span
                                  style={{
                                    fontStyle: "italic",
                                    fontWeight: "500",
                                    fontFamily: "Line Awesome Free",
                                    marginLeft: "0.5rem",
                                    marginRight: "0.25rem",
                                  }}
                                  class="ms-2 me-1 color-italic"
                                >
                                  {student.email}
                                </span>
                              </Box>
                            </Box>
                          </Box>
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "flex-end",
                              alignItems: "center ",
                            }}
                          >
                            {userLogin._id === schedule.inClass.trainer._id ||
                            userLogin._id === schedule.trainer._id ||
                            isAdmin ? (
                              <Button
                                variant="outlined"
                                onClick={() =>
                                  handleClickOpenMoveStudentDialog(
                                    student,
                                    null
                                  )
                                }
                              >
                                Move
                              </Button>
                            ) : null}
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                  </Box>
                </>
              ))}
            </AccordionDetails>
          </Accordion>
        )}
      </Box>
      {groups.map((group) => (
        <>
          <Box
            key={group._id}
            sx={{
              marginBottom: "20px",
              border: "1px solid rgba(0, 0, 0, 0.12)",
              borderRadius: "4px",
            }}
          >
            <Accordion
              expanded={expanded.includes(`panel${group._id}`)}
              onChange={handleChange(`panel${group._id}`)}
            >
              <AccordionSummary
                aria-controls="panel2d-content"
                id="panel2d-header"
                sx={{
                  padding: "4px 12px",
                  fontSize: "16px",
                  alignItems: "center",
                  justifyContent: "center",
                  position: "relative",
                  boxSizing: "border-box",
                  outline: "0px",
                  border: "0px",
                  margin: "0px",
                  borderRadius: "0px",
                  cursor: "pointer",
                  userSelect: "none",
                  verticalAlign: "middle",
                  appearance: "none",
                  textDecoration: "none",
                  color: "inherit",
                  display: "flex",
                  minHeight: "48px",
                  transition:
                    "min-height 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, background-color 150ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
                  backgroundColor: "rgba(0, 0, 0, 0.03)",
                  flexDirection: "row-reverse",
                }}
              >
                <Box
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    marginLeft: "10px",
                  }}
                >
                  <span
                    style={{
                      fontWeight: "600",
                      marginRight: "0.5rem",
                    }}
                    title="Group 1"
                    class="text-semibold me-2"
                    value="Group 1"
                  >
                    {`Group ${group.groupNumber}`}
                  </span>
                  <span
                    style={{
                      fontWeight: "600",
                    }}
                  >
                    {`(${group.members.length} students)`}
                  </span>
                </Box>
              </AccordionSummary>
              <AccordionDetails
                sx={{
                  padding: "10px 25px",
                  color: "rgba(0, 0, 0, 0.87)",
                }}
              >
                {group.members.map((member) => (
                  <>
                    {/* Student 1 */}
                    <Box key={member._id} sx={{ minHeight: "30px" }}>
                      <Box sx={{ marginBottom: "4px" }}>
                        <Box sx={{ marginBottom: "1rem", marginTop: "1rem" }}>
                          <Box
                            sx={{
                              display: "flex",
                              justifyContent: "space-between",
                            }}
                          >
                            <Box
                              sx={{
                                display: "flex",
                                alignItems: "center",
                                fontWeight: "600",
                                flex: "0 0 auto",
                                width: "50%",
                              }}
                            >
                              <Avatar sx={{ backgroundColor: green[500] }}>
                                {member.email.charAt(0).toUpperCase()}
                              </Avatar>
                              <Box
                                sx={{
                                  display: "flex",
                                  flexWrap: "wrap",
                                }}
                              >
                                <Box
                                  sx={{
                                    flex: "0 0 auto",
                                    width: "100%",
                                    paddingRight: "0.5rem",
                                    marginTop: "0",
                                  }}
                                >
                                  <span
                                    style={{
                                      marginLeft: "0.5rem",
                                      marginRight: "0.25rem",
                                    }}
                                    title="Mai Chí Thiện"
                                    class="ms-2 me-1"
                                    value="Mai Chí Thiện"
                                  >
                                    {member.fullName}{" "}
                                    {group?.leader?._id === member._id && (
                                      <StarsIcon color="warning" />
                                    )}
                                  </span>
                                </Box>
                                <Box
                                  sx={{
                                    flex: "0 0 auto",
                                    width: "100%",
                                    paddingRight: "0.5rem",
                                    marginTop: "0",
                                  }}
                                >
                                  <span
                                    style={{
                                      fontStyle: "italic",
                                      fontWeight: "500",
                                      fontFamily: "Line Awesome Free",
                                      marginLeft: "0.5rem",
                                      marginRight: "0.25rem",
                                    }}
                                    class="ms-2 me-1 color-italic"
                                  >
                                    {member.email}
                                  </span>
                                </Box>
                              </Box>
                            </Box>
                            <Box
                              sx={{
                                display: "flex",
                                justifyContent: "flex-end",
                                flexDirection: "column",
                              }}
                            >
                              {userLogin._id === schedule.inClass.trainer._id ||
                              userLogin._id === schedule.trainer._id ||
                              isAdmin ? (
                                <>
                                  <Button
                                    variant="outlined"
                                    color="error"
                                    sx={{ marginBottom: "5px" }}
                                    onClick={() =>
                                      handleClickOpenRemoveStudentDialog(
                                        member,
                                        group
                                      )
                                    }
                                  >
                                    Remove
                                  </Button>
                                  <Button
                                    variant="outlined"
                                    onClick={() =>
                                      handleClickOpenMoveStudentDialog(
                                        member,
                                        group
                                      )
                                    }
                                  >
                                    Move
                                  </Button>{" "}
                                </>
                              ) : null}
                            </Box>
                          </Box>
                        </Box>
                      </Box>
                    </Box>
                  </>
                ))}
              </AccordionDetails>
            </Accordion>
          </Box>
        </>
      ))}
      {/* Dialog remove student from group */}
      <Dialog
        open={openRemoveStudentDialog}
        onClose={handleCloseRemoveStudentDialog}
      >
        <DialogTitle>Remove Student</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {`Remove Student ${selectedMember?.email} from Group ${oldGroup?.groupNumber}`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="warning"
            onClick={handleCloseRemoveStudentDialog}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            color="success"
            onClick={handleRemoveStudentOutOfGroup}
          >
            Confirm
          </Button>
        </DialogActions>
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>
      {/* Dialog move student from group */}
      <Dialog
        open={openMoveStudentDialog}
        keepMounted
        onClose={handleCloseMoveStudentDialog}
        aria-describedby="move-student-dialog-slide-description"
        maxWidth="md"
      >
        <DialogTitle
          sx={{
            padding: "0",
            marginBottom: "24px",
          }}
        >
          <Paper
            sx={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              paddingRight: "10px",
              paddingLeft: "10px",
              alignItems: "center",
            }}
          >
            <Box>Move Student</Box>
            <Box>
              <Button onClick={handleCloseMoveStudentDialog}>
                <CloseIcon fontSize="medium" />
              </Button>
            </Box>
          </Paper>
        </DialogTitle>
        <DialogContent
          sx={{
            width: "100%",
            paddingTop: "24px",
            paddingBottom: "24px",
          }}
        >
          <Paper sx={{ p: 2, width: "600px", mt: 2 }}>
            <table>
              <tr
                style={{
                  display: "flex",
                  alignItems: "center",
                  margin: "0",
                  padding: "0",
                }}
              >
                <th>
                  {" "}
                  <InputLabel sx={{ mr: 4 }}>All Groups: </InputLabel>
                </th>
                <td>
                  <FormControl sx={{ m: 1, minWidth: 120 }} size="small">
                    <InputLabel id="demo-select-small-label">Group</InputLabel>
                    <Select
                      labelId="demo-select-small-label"
                      id="demo-select-small"
                      label="Group"
                      value={newGroup}
                      onChange={(event) => setNewGroup(event.target.value)}
                    >
                      {groups &&
                        groups
                          .filter((group) => group !== oldGroup)
                          .map((group) => (
                            <MenuItem key={group._id} value={group._id}>
                              {`Group ${group.groupNumber}`}
                            </MenuItem>
                          ))}
                    </Select>
                  </FormControl>
                </td>
              </tr>
            </table>
          </Paper>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseMoveStudentDialog}>Cancel</Button>
          <Button
            onClick={handleMoveStudentToGroup}
            color="primary"
            variant="contained"
          >
            Move
          </Button>
        </DialogActions>
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>
    </>
  );
};

export default StudentsTab;
