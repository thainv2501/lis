import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Divider,
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
} from "@mui/material";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import { enqueueSnackbar } from "notistack";
import { useState } from "react";

export default function TimePickerDialog({
  handleCloseTimePickerDialog,
  openTimePickerDialog,
  updateCQTime,
  CQ,
  schedule,
}) {
  const [from, setFrom] = useState(dayjs().format("YYYY-MM-DD HH:mm:ss"));
  const [to, setTo] = useState(dayjs().format("YYYY-MM-DD HH:mm:ss"));
  const [day, setDay] = useState(0);
  const [hours, setHours] = useState(0);
  const [minutes, setMinutes] = useState(0);

  const [value, setValue] = useState("specific-time-mode");

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const handleStart = async () => {
    if (value === "specific-time-mode") {
      const currentDate = dayjs();
      // Create a Day.js duration object
      // Add days, hours, and minutes separately
      const resultDate = currentDate
        .add(day, "day")
        .add(hours, "hour")
        .add(minutes, "minute");

      try {
        await updateCQTime({
          constructiveQuestionId: CQ?._id,
          scheduleId: schedule?._id,
          from,
          to: resultDate,
        });
      } catch (error) {
        enqueueSnackbar("Error While UpdateFunction : " + error);
      }
    }
    if (value === "specific-date-mode") {
      if (
        dayjs(from).isAfter(dayjs(to)) ||
        dayjs(from).format("YYYY-MM-DD HH:mm:ss") === "Invalid Date" ||
        dayjs(to).format("YYYY-MM-DD HH:mm:ss") === "Invalid Date"
      ) {
        enqueueSnackbar("Your From Date is invalid compare to End Date", {
          variant: "error",
        });
      } else {
        try {
          await updateCQTime({
            constructiveQuestionId: CQ?._id,
            scheduleId: schedule?._id,
            from,
            to,
          });
        } catch (error) {
          enqueueSnackbar("Error While UpdateFunction : " + error);
        }
      }
    }
  };

  const handleDayNumberChange = (event, min, max) => {
    const inputValue = event.target.value;
    const minAllowed = min;
    const maxAllowed = max;
    let sanitizedValue = inputValue;

    // Perform validation to ensure the value is within the allowed range
    if (inputValue < minAllowed) {
      sanitizedValue = minAllowed;
    } else if (inputValue > maxAllowed) {
      sanitizedValue = maxAllowed;
    }

    // Update the state with the sanitized value
    setDay(sanitizedValue);
  };
  const handleHoursNumberChange = (event, min, max) => {
    const inputValue = event.target.value;
    const minAllowed = min;
    const maxAllowed = max;
    let sanitizedValue = inputValue;

    // Perform validation to ensure the value is within the allowed range
    if (inputValue < minAllowed) {
      sanitizedValue = minAllowed;
    } else if (inputValue > maxAllowed) {
      sanitizedValue = maxAllowed;
    }

    // Update the state with the sanitized value
    setHours(sanitizedValue);
  };
  const handleMinutesNumberChange = (event, min, max) => {
    const inputValue = event.target.value;
    const minAllowed = min;
    const maxAllowed = max;
    let sanitizedValue = inputValue;

    // Perform validation to ensure the value is within the allowed range
    if (inputValue < minAllowed) {
      sanitizedValue = minAllowed;
    } else if (inputValue > maxAllowed) {
      sanitizedValue = maxAllowed;
    }

    // Update the state with the sanitized value
    setMinutes(sanitizedValue);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Dialog
        fullWidth
        maxWidth={"sm"}
        open={openTimePickerDialog}
        onClose={handleCloseTimePickerDialog}
      >
        <DialogContent>Set Time</DialogContent>
        <Divider></Divider>
        <DialogContent>
          <FormControl fullWidth={true}>
            <FormLabel id="demo-radio-buttons-group-label">Time Mode</FormLabel>
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              name="radio-buttons-group"
              value={value}
              onChange={handleChange}
            >
              <FormControlLabel
                value="specific-time-mode"
                control={<Radio />}
                label="Allow students activities on this question about  : "
              />
              <Box
                display={"flex"}
                gap={2}
                component="form"
                noValidate
                autoComplete="off"
              >
                <TextField
                  sx={{ width: "30%" }}
                  id="outlined-basic"
                  label="Day"
                  variant="outlined"
                  type="number"
                  value={day}
                  inputProps={{
                    min: 1,
                    max: 365,
                  }}
                  onChange={(event) => handleDayNumberChange(event, 1, 365)}
                />
                <TextField
                  sx={{ width: "30%" }}
                  id="outlined-basic"
                  label="Hours"
                  variant="outlined"
                  value={hours}
                  type="number"
                  inputProps={{
                    min: 1,
                    max: 23,
                  }}
                  onChange={(event) => handleHoursNumberChange(event, 1, 23)}
                />
                <TextField
                  sx={{ width: "30%" }}
                  id="outlined-basic"
                  label="Minutes"
                  value={minutes}
                  variant="outlined"
                  type="number"
                  inputProps={{
                    min: 1,
                    max: 59,
                  }}
                  onChange={(event) => handleMinutesNumberChange(event, 1, 59)}
                />
              </Box>
              <FormControlLabel
                value="specific-date-mode"
                control={<Radio />}
                label="Allow students activities on this question until  : "
              />
              <Grid>
                <DateTimePicker
                  label="To"
                  value={dayjs(to)}
                  disablePast
                  minDate={dayjs(from)}
                  onChange={(value) => setTo(value)}
                />
              </Grid>
            </RadioGroup>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button color="error" onClick={handleCloseTimePickerDialog}>
            Cancel
          </Button>
          <Button color="success" onClick={handleStart}>
            Start
          </Button>
        </DialogActions>
      </Dialog>
    </LocalizationProvider>
  );
}
