import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import BookIcon from "@mui/icons-material/Book";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import OndemandVideoIcon from "@mui/icons-material/OndemandVideo";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {
  useGetMeetingsWithClassIdQuery,
  useAddNewMeetingMutation,
  useDeleteMeetingMutation,
  useUpdateMeetingTimeMutation,
} from "../meeting/meetingsApiSlice";
import { MobileDateTimePicker } from "@mui/x-date-pickers/MobileDateTimePicker";
import {
  Avatar,
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Divider,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Paper,
  Skeleton,
  TextField,
  Typography,
  IconButton,
  Chip,
  Autocomplete,
  FormControlLabel,
  Checkbox,
} from "@mui/material";
import RefreshIcon from "@mui/icons-material/Refresh";
import AddIcon from "@mui/icons-material/Add";
import CloseIcon from "@mui/icons-material/Close";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Tab from "@mui/material/Tab";
import { styled } from "@mui/material/styles";
import { useSnackbar } from "notistack";
import { React, useEffect, useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import ContentTab from "./ContentTab";
import StudentsTab from "./StudentsTab";
import { generateMeetingId } from "../../utils/generateMeetingId";

import { Breadcrumbs, DialogTitle } from "@material-ui/core";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import dayjs from "dayjs";
import Parser from "html-react-parser";
import { useLocation, useParams } from "react-router-dom";
import {
  useGetScheduleByIdQuery,
  useUpdateScheduleMutation,
} from "../schedule/scheduleApiSlice";
import SlotShareTab from "./SlotShareTab";
import useAuth from "../../hooks/useAuth";
import SupportQuestionsTab from "./SupportQuestionsTab";
import TimePickerDialog from "../class/TimePickerDialog";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
function SlotDetails() {
  const { userLogin, isAdmin, isTrainer, isTrainee } = useAuth();
  const userId = userLogin?._id;

  const [scheduleContent, setScheduleContent] = useState("");
  const [openScheduleContentDialog, setOpenScheduleContentDialog] =
    useState(false);
  const handleOpenScheduleContentDialog = () => {
    setOpenScheduleContentDialog(true);
  };
  const handleCloseScheduleContentDialog = () => {
    setOpenScheduleContentDialog(false);
  };
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };
  const navigate = useNavigate();
  const [schedule, setSchedule] = useState();
  // State to track if data has been fetched from the API
  const { scheduleId } = useParams();
  const location = useLocation();
  // Get the query parameters from the URL
  const queryParams = new URLSearchParams(location.search);

  // Get a specific parameter value
  const slotValue = queryParams.get("slot");

  const { enqueueSnackbar } = useSnackbar();

  // Tab Lesson / Material / Mentor Handle
  const [valueTab, setValueTab] = useState("1");

  const handleChangeTab = (event, newValue) => {
    setValueTab(newValue);
  };

  const {
    data: scheduleData,
    isSuccess: getScheduleByIdIsSuccess,
    isLoading: getScheduleByIdIsLoading,
    isError: getScheduleByIdIsError,
    error: getScheduleByIdError,
    refetch,
  } = useGetScheduleByIdQuery(scheduleId, {
    refetchOnFocus: true,
  });

  useEffect(() => {
    if (scheduleData) {
      setSchedule(scheduleData);
      setScheduleContent(scheduleData.content);
    }
  }, [scheduleData]);

  const [isRefetching, setIsRefetching] = useState(false);
  const {
    data: meetings,
    isSuccess: getMeetingsIsSuccess,
    isLoading: getMeetingsIsLoading,
    isError: getMeetingsIsError,
    refetch: getMeetingsRefetch,
  } = useGetMeetingsWithClassIdQuery(schedule?.inClass?._id, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (isRefetching) {
      handleLoadMeetingData();
    } else {
      handleCloseBackDrop();
    }
  }, [isRefetching]);

  const handleLoadMeetingData = () => {
    setIsRefetching(true);
    setOpenBackDrop(true);
    getMeetingsRefetch().then(() => {
      setIsRefetching(false);
    });
  };

  const [
    updateSchedule,
    {
      isSuccess: updateScheduleIsSuccess,
      isLoading: updateScheduleIsLoading,
      isError: updateScheduleIsError,
      error: updateScheduleError,
    },
  ] = useUpdateScheduleMutation();

  const handleUpdateGeneralInformation = async () => {
    try {
      updateSchedule({
        scheduleId: scheduleId,
        content: scheduleContent,
      });
    } catch (error) {
      enqueueSnackbar("Error with handleUpdate Function !", {
        variant: "error",
      });
    }
  };

  const handleEditSlotContent = () => {
    handleOpenScheduleContentDialog();
  };

  useEffect(() => {
    if (updateScheduleIsSuccess) {
      enqueueSnackbar("Update Schedule Successful !", { variant: "success" });
      handleCloseBackDrop();
      refetch();
      handleCloseScheduleContentDialog();
    }
  }, [updateScheduleIsSuccess]);
  useEffect(() => {
    if (updateScheduleIsLoading) {
      handleOpenBackDrop();
    }
  }, [updateScheduleIsLoading]);

  useEffect(() => {
    if (updateScheduleIsError) {
      handleCloseBackDrop();
      enqueueSnackbar(
        "Update Schedule Error : " + updateScheduleError?.data?.message,
        {
          variant: "error",
        }
      );
    }
  }, [updateScheduleIsError]);

  const handleScheduleObjChange = (key, value) => {
    setSchedule((prevValues) => {
      const updatedValues = { ...prevValues };
      const keys = key.split(".");
      let nestedValue = updatedValues;
      for (let i = 0; i < keys.length; i++) {
        const currentKey = keys[i];
        if (i === keys.length - 1) {
          nestedValue[currentKey] = value;
        } else {
          nestedValue[currentKey] = { ...nestedValue[currentKey] };
          nestedValue = nestedValue[currentKey];
        }
      }
      return updatedValues;
    });
  };

  if (getScheduleByIdIsError && scheduleId !== undefined) {
    enqueueSnackbar(getScheduleByIdError?.data?.message);
    navigate("/404");
  }

  const handleSlotTime = (slot) => {
    let slotStartAtFloat = 7.5 + (slot - 1) * 1.5;
    if (slot >= 4) {
      slotStartAtFloat = 12.5 + (slot - 4) * 1.5;
    }
    const hours = Math.floor(slotStartAtFloat);
    const minutes = Math.floor((slotStartAtFloat - hours) * 60);
    const slotStartAtTime = dayjs().hour(hours).minute(minutes).format("HH:mm");
    const slotEndAtTime = dayjs()
      .hour(hours + 1)
      .minute(minutes + 30)
      .format("HH:mm");

    return `${slotStartAtTime} - ${slotEndAtTime}`;
  };

  const countConstructiveQuestions = (lessons) => {
    let totalConstructiveQuestions = 0;
    if (lessons && Array.isArray(lessons)) {
      for (const lesson of lessons) {
        if (
          lesson.constructiveQuestions &&
          Array.isArray(lesson.constructiveQuestions)
        ) {
          totalConstructiveQuestions += lesson.constructiveQuestions.length;
        }
      }
    }
    return totalConstructiveQuestions;
  };

  // open dialog add class meeting
  const [openAddClassMeetingDialog, setOpenAddClassMeetingDialog] =
    useState(false);
  const handleClickOpenAddClassMeetingDialog = () => {
    setOpenAddClassMeetingDialog(true);
  };

  const handleCloseAddClassMeetingDialog = () => {
    setOpenAddClassMeetingDialog(false);
  };
  const [selectedMeeting, setSelectedMeeting] = useState(null);
  const [meetingName, setMeetingName] = useState("");
  const [selectedUser, setSelectedUser] = useState([]);
  const [dateTimeMeetingFrom, setDateTimeMeetingFrom] = useState(dayjs());
  const [dateTimeMeetingTo, setDateTimeMeetingTo] = useState(dayjs());
  const [classMeeting, setClassMeeting] = useState("");
  const [meetingNameError, setMeetingNameError] = useState(false);
  const [meetingNameHelperText, setMeetingNameHelperText] = useState("");

  const meetingNameRegExp = /^[\p{L}0-9\s]{3,50}$/u;

  const handleMeetingNameChange = (e) => {
    const value = e.target.value;
    // Check if the input matches the regular expression
    if (value.trim().length === 0) {
      setMeetingNameError(true);
      setMeetingNameHelperText("Meeting Name is required!");
    } else if (meetingNameRegExp.test(value)) {
      setMeetingNameError(false);
      setMeetingNameHelperText("");
    } else {
      setMeetingNameError(true);
      setMeetingNameHelperText("Meeting Name is invalid!");
    }
    setMeetingName(value);
  };

  // select with class meeting
  const [selectAll, setSelectAll] = useState(false);
  const [selectedStudents, setSelectedStudents] = useState([]);

  useEffect(() => {
    // Check if all students are selected
    if (selectedStudents.length === schedule?.inClass?.students?.length) {
      setSelectAll(true);
    } else {
      setSelectAll(false);
    }
  }, [selectedStudents, schedule?.inClass]);

  useEffect(() => {
    if (selectAll) {
      setSelectedStudents(schedule?.inClass?.students || []);
    }
  }, [selectAll, schedule?.inClass]);

  const handleToggleSelectAll = () => {
    setSelectAll((prev) => !prev);
    setSelectedStudents([]);
  };

  const handleUserSelection = (event, newValue) => {
    setSelectedStudents(newValue);
  };

  useEffect(() => {
    if (!selectAll) {
      setSelectedUser(selectedStudents.map((user) => user?._id));
    }
  }, [selectAll, selectedStudents, setSelectedUser]);

  const handleDateTimeMeetingFromChange = (date) => {
    setDateTimeMeetingFrom(date);
  };

  const handleDateTimeMeetingToChange = (date) => {
    setDateTimeMeetingTo(date);
  };

  // Create one to one meeting
  const setEmptyMeetingObj = () => {
    setMeetingName("");
    setSelectedUser([]);
    setSelectedStudents([]);
    setSelectAll(false);
    setDateTimeMeetingFrom(null);
    setDateTimeMeetingTo(null);
  };

  const [
    createNewMeeting,
    {
      isSuccess: createMeetingIsSuccess,
      isError: createMeetingIsError,
      error: createMeetingError,
    },
  ] = useAddNewMeetingMutation();

  // Create video conference
  const handleAddNewVideoConference = async () => {
    if (dayjs(dateTimeMeetingFrom).isAfter(dayjs(dateTimeMeetingTo))) {
      enqueueSnackbar("Your From Date is invalid compare to End Date", {
        variant: "error",
      });
    } else {
      const meetingId = generateMeetingId();
      try {
        await createNewMeeting({
          createdBy: userId,
          meetingId: meetingId,
          meetingName: meetingName,
          meetingType: "video-conference",
          invitedUsers: selectedUser,
          meetingFrom: dateTimeMeetingFrom,
          meetingTo: dateTimeMeetingTo,
          maxUsers: selectedUser && selectedUser.length,
          classId: schedule?.inClass?._id,
          status: true,
        });
      } catch (error) {
        enqueueSnackbar("Create failed!: " + error, {
          variant: "error",
        });
      }
    }
  };

  useEffect(() => {
    if (createMeetingIsSuccess) {
      enqueueSnackbar("Create new meeting successfully!", {
        variant: "success",
      });
      setEmptyMeetingObj();
      setOpenAddClassMeetingDialog(false);
      getMeetingsRefetch();
    }
    if (createMeetingIsError) {
      enqueueSnackbar("Create failed!: " + createMeetingError?.data?.message, {
        variant: "error",
      });
      setEmptyMeetingObj();
      setOpenAddClassMeetingDialog(false);
    }
  }, [createMeetingIsError, createMeetingIsSuccess]);

  // delete meeting
  const [
    deleteMeeting,
    {
      isSuccess: deleteMeetingIsSuccess,
      isLoading: deleteMeetingIsLoading,
      isError: deleteMeetingIsError,
      error: deleteMeetingError,
    },
  ] = useDeleteMeetingMutation();

  const [openDeleteMeetingDialog, setOpenDeleteMeetingDialog] = useState(false);

  const handleDeleteClicked = (data) => {
    setSelectedMeeting(data);
    handleClickOpenDeleteMeetingDialog();
  };
  const handleClickOpenDeleteMeetingDialog = () => {
    setOpenDeleteMeetingDialog(true);
  };

  const handleCloseDeleteMeetingDialog = () => {
    setOpenDeleteMeetingDialog(false);
  };

  const handleDeleteMeeting = async () => {
    try {
      await deleteMeeting({
        id: selectedMeeting?._id,
      }).then(() => {
        enqueueSnackbar(
          `Remove ${selectedMeeting?.meetingName} information is successfully!`,
          {
            variant: "success",
          }
        );
      });
      getMeetingsRefetch();
      setSelectedMeeting(null);
      handleCloseDeleteMeetingDialog();
    } catch (error) {
      enqueueSnackbar("Remove failed!: " + error, {
        variant: "error",
      });
    }
  };

  const [openTimePickerDialog, setOpenTimePickerDialog] = useState(false);
  const handleOpenTimePickerDialog = (meeting) => {
    setSelectedMeeting(meeting);
    setOpenTimePickerDialog(true);
  };
  const handleCloseTimePickerDialog = () => {
    setOpenTimePickerDialog(false);
  };

  const [
    updateMeetingTime,
    {
      isSuccess: updateMeetingTimeIsSuccess,
      isLoading: updateMeetingTimeIsLoading,
      isError: updateMeetingTimeIsError,
      error: updateMeetingTimeError,
    },
  ] = useUpdateMeetingTimeMutation();
  useEffect(() => {
    if (updateMeetingTimeIsSuccess) {
      handleCloseTimePickerDialog();
      enqueueSnackbar("Class Meeting Started!", { variant: "success" });
      getMeetingsRefetch();
    }
    if (updateMeetingTimeIsLoading) {
    }
    if (updateMeetingTimeIsError) {
      enqueueSnackbar(
        "Meeting Time Setup Fail : " + updateMeetingTimeError?.message,
        { variant: "error" }
      );
    }
  }, [
    updateMeetingTimeIsSuccess,
    updateMeetingTimeIsError,
    updateMeetingTimeIsLoading,
  ]);

  let content = null;
  if (getScheduleByIdIsLoading) {
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        {/* Render a loop of 10 skeletons */}
        {Array.from({ length: 10 }).map((_, index) => (
          <div key={index}>
            <Skeleton animation={false} />
            <Skeleton animation="wave" />
          </div>
        ))}
      </Box>
    );
  }
  if (getScheduleByIdIsSuccess && schedule) {
    content = (
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Paper sx={{ width: "100%", p: 3 }}>
          <Box sx={{ marginBottom: "1rem" }}>
            <Breadcrumbs aria-label="breadcrumb" style={{ fontSize: "20px" }}>
              <Link style={{ color: "#0078D8" }} to={"/common/courses-list"}>
                Home
              </Link>
              <Link
                style={{ color: "#0078D8" }}
                to={`/common/courses-list/class-details?classId=${schedule?.inClass?._id}&semesterId=${schedule?.inClass?.semester?._id}`}
              >
                {schedule?.inClass?.className} -{" "}
                {schedule?.inClass?.subject?.subjectCode} -{" "}
                {schedule?.inClass?.subject?.subjectName}-{" "}
                {schedule?.inClass?.semester?.name}
              </Link>
              <Typography variant="h6">Slot {slotValue}</Typography>
            </Breadcrumbs>
          </Box>
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
            justifyContent={"space-between"}
            alignItems={"flex-start"}
            marginBottom={3}
          >
            <Grid item sx={6} md={8}>
              <Paper sx={{ p: 2 }}>
                <Box mb={3}>
                  {userLogin._id === schedule.inClass.trainer._id ||
                  userLogin._id === schedule.trainer._id ||
                  isAdmin ? (
                    <Grid item container justifyContent={"flex-end"}>
                      <Button
                        variant="outlined"
                        onClick={handleEditSlotContent}
                      >
                        Edit Content
                      </Button>
                    </Grid>
                  ) : null}
                  <Grid container spacing={3}>
                    {schedule?.lessons &&
                      schedule?.lessons.map((lesson) => (
                        <Grid item xs={12}>
                          <Typography>
                            Chapter {lesson?.lessonChapter}
                          </Typography>
                          <Typography>
                            Lesson {lesson?.lessonNumber} :{" "}
                            {lesson?.lessonContent}
                          </Typography>
                        </Grid>
                      ))}
                    <Grid item xs={12}>
                      <Typography>{schedule?.content}</Typography>
                    </Grid>
                  </Grid>
                </Box>
                <Box>
                  <ul
                    style={{
                      listStyle: "none",
                      fontSize: "0.875rem",
                      marginBottom: "16px",
                      padding: "0",
                    }}
                  >
                    <li
                      style={{
                        display: "inline-flex",
                        lineHeight: "24px",
                        color: "#666",
                        marginRight: "24px",
                        marginBottom: "8px",
                      }}
                    >
                      <CalendarMonthIcon />
                      <span>
                        {dayjs(schedule?.day).format("DD/MM/YYYY")} at{" "}
                        {handleSlotTime(schedule?.slotInDay)}
                      </span>
                    </li>
                    <li
                      style={{
                        display: "inline-flex",
                        lineHeight: "24px",
                        color: "#666",
                        marginRight: "24px",
                        marginBottom: "8px",
                      }}
                    >
                      <LibraryBooksIcon />
                      <span>{`${schedule?.inClass?.subject?.subjectCode} ↔ [${schedule?.inClass?.semester?.name}]`}</span>
                    </li>
                    <li
                      style={{
                        display: "inline-flex",
                        lineHeight: "24px",
                        color: "#666",
                        marginRight: "24px",
                        marginBottom: "8px",
                      }}
                    >
                      <BookIcon />
                      <span>{schedule?.inClass?.className} </span>
                    </li>
                  </ul>
                </Box>
                <Box sx={{ width: "100%", typography: "body1" }}>
                  <TabContext value={valueTab}>
                    <Box>
                      <TabList
                        onChange={handleChangeTab}
                        aria-label="lab API tabs example"
                      >
                        <Tab label="Questions" value="1" />
                        <Tab label="Groups" value="2" />
                        <Tab label="Reference" value="3" />
                        <Tab label="Support Question" value="4" />
                      </TabList>
                    </Box>
                    {/* Content Tab */}
                    <TabPanel sx={{ pr: 0, pl: 0 }} value="1">
                      <ContentTab
                        scheduleSlot={slotValue}
                        refetch={refetch}
                        scheduleCQ={schedule?.constructiveQuestions}
                        lessons={schedule?.lessons}
                        handleScheduleObjChange={handleScheduleObjChange}
                        schedule={schedule}
                        updateSchedule={updateSchedule}
                      />
                    </TabPanel>
                    {/* Student Tab */}
                    <TabPanel sx={{ pr: 0, pl: 0 }} value="2">
                      <StudentsTab
                        scheduleId={scheduleId}
                        schedule={schedule}
                      />
                    </TabPanel>
                    {/* Slot Share */}
                    <TabPanel sx={{ pr: 0, pl: 0 }} value="3">
                      <SlotShareTab schedule={schedule} refetch={refetch} />
                    </TabPanel>
                    {/* Support Question Tab */}
                    <TabPanel sx={{ pr: 0, pl: 0 }} value="4">
                      <SupportQuestionsTab schedule={schedule} />
                    </TabPanel>
                  </TabContext>
                </Box>
              </Paper>
            </Grid>
            <Grid item xs={6} md={4}>
              <Paper>
                <Card sx={{ width: "100%" }}>
                  <CardContent sx={{ p: 2 }}>
                    <Typography
                      textAlign={"center"}
                      gutterBottom
                      variant="h5"
                      component="div"
                    >
                      {`${schedule?.inClass?.className}_${schedule?.inClass?.subject?.subjectCode}_${schedule?.inClass?.semester?.name}`}
                    </Typography>
                    <Box
                      display={"flex"}
                      justifyContent={"space-between"}
                      flexWrap={"nowrap"}
                    >
                      <Paper
                        sx={{
                          backgroundColor: "#f7f7f7",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                          justifyContent: "center",
                          fontWeight: "800",
                          color: "#00ac47",
                          margin: "8px",
                          width: "128px",
                          height: "90px",
                          fontSize: "1rem",
                        }}
                      >
                        <span>{schedule?.inClass?.students?.length || 0}</span>
                        <span title="students" value="students">
                          students
                        </span>
                      </Paper>
                      <Paper
                        sx={{
                          backgroundColor: "#f7f7f7",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                          justifyContent: "center",
                          fontWeight: "800",
                          color: "#00ac47",
                          margin: "8px",
                          width: "128px",
                          height: "90px",
                          fontSize: "1rem",
                        }}
                      >
                        <span>{schedule?.inClass?.students?.length || 0}</span>
                        <span title="students" value="students">
                          students
                        </span>
                      </Paper>
                      <Paper
                        sx={{
                          backgroundColor: "#f7f7f7",
                          display: "flex",
                          flexDirection: "column",
                          alignItems: "center",
                          justifyContent: "center",
                          fontWeight: "800",
                          margin: "8px",
                          width: "128px",
                          height: "90px",
                          fontSize: "1rem",
                        }}
                      >
                        <span>
                          {schedule?.constructiveQuestions?.length +
                            countConstructiveQuestions(schedule?.lessons)}
                        </span>
                        <span title="activities" value="activities">
                          activities
                        </span>
                      </Paper>
                    </Box>
                    <Box sx={{ display: "flex", flexWrap: "wrap" }}>
                      <Paper
                        sx={{
                          fontWeight: "600",
                          fontSize: "0.875rem",
                          backgroundColor: "#f7f7f7",
                          display: "flex",
                          flexDirection: "column",
                          justifyContent: "center",
                          p: 2,
                          margin: "8px",
                          width: "100%",
                          height: "100px",
                        }}
                      >
                        <span>
                          Start date:{" "}
                          {dayjs(schedule?.day).format("DD/MM/YYYY")}
                        </span>
                        <span
                          class="text-danger"
                          style={{ color: "rgba(220, 53, 69, 1)" }}
                        >
                          End date &nbsp;&nbsp;:{" "}
                          {dayjs(schedule?.day).format("DD/MM/YYYY")}
                        </span>
                      </Paper>
                    </Box>
                    <Box>
                      <span
                        style={{ fontWeight: "700", fontSize: "1rem" }}
                        title="undefined"
                      >
                        Lecturer
                      </span>
                      <List
                        sx={{
                          width: "100%",
                          maxWidth: 360,
                          bgcolor: "background.paper",
                        }}
                      >
                        <ListItem
                          display="flex"
                          justifyContent="flex-start"
                          alignItems="center"
                          position="relative"
                          textAlign="left"
                          width="100%"
                        >
                          <ListItemAvatar>
                            <Avatar>
                              {schedule?.trainer &&
                                schedule?.trainer?.email
                                  ?.charAt(0)
                                  .toUpperCase()}
                            </Avatar>
                          </ListItemAvatar>
                          <ListItemText
                            sx={{
                              flex: "1 1 auto",
                              minWidth: "0px",
                              marginTop: "4px",
                              marginBottom: "4px",
                            }}
                          >
                            <span
                              style={{
                                fontSize: "16px",
                                lineHeight: "1.5",
                                display: "block",
                              }}
                              className="MuiTypography-root MuiTypography-body1 MuiListItemText-primary css-pk9ffv"
                            >
                              {schedule?.trainer.fullName}
                            </span>{" "}
                            <span
                              style={{
                                fontSize: "12px",
                                lineHeight: "1.5",
                                display: "block",
                                fontStyle: "italic",
                                color: "#333",
                              }}
                              className="MuiTypography-root MuiTypography-body1 MuiListItemText-primary css-pk9ffv"
                            >
                              {schedule?.trainer.email}
                            </span>
                          </ListItemText>
                        </ListItem>
                      </List>
                    </Box>
                  </CardContent>
                </Card>
              </Paper>
              <Paper
                sx={{
                  mt: 3.5,
                  fontWeight: "800",
                  width: "100%",
                  fontSize: "1rem",
                  backgroundColor: "#fff",
                  border: "1px solid #d9d9d9",
                  borderRadius: "8px",
                }}
              >
                <Box sx={{ padding: "16px" }}>
                  <span
                    style={{
                      fontWeight: "600",
                      fontSize: "1.125rem",
                      lineHeight: "1.2",
                      display: "flex",
                      justifyContent: "space-between",
                      alignItems: "center",
                    }}
                  >
                    Class meeting
                    {isAdmin || schedule?.inClass?.trainer?._id === userId ? (
                      <IconButton
                        onClick={handleClickOpenAddClassMeetingDialog}
                      >
                        <AddIcon />
                      </IconButton>
                    ) : (
                      <IconButton
                        onClick={handleLoadMeetingData}
                        className={openBackDrop ? "reload-button-rotate" : ""}
                      >
                        <RefreshIcon />
                      </IconButton>
                    )}
                  </span>
                  <ul
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      listStyle: "none",
                      padding: "0",
                    }}
                  >
                    {meetings &&
                      meetings?.map((meeting) => (
                        <li
                          key={meeting?._id}
                          style={{
                            marginTop: "10px",
                            borderLeft: "4px solid #0078d4",
                            borderRadius: "8px",
                            backgroundColor: "#f7f7f7",
                          }}
                        >
                          <Box
                            sx={{
                              padding: "11px 10px",
                              border: "1px solid #f7f7f7",
                              display: "inline-flex",
                              textDecoration: "none",
                              color: "#111",
                              borderRadius: "8px",
                              width: "100%",
                              minHeight: "100px",
                            }}
                          >
                            <Box
                              sx={{
                                width: "100%",
                                display: "flex",
                                flexWrap: "wrap",
                                justifyContent: "space-between",
                                alignItems: "flex-start",
                              }}
                            >
                              <Box
                                sx={{
                                  flex: "0 0 auto",
                                  pr: 1.5,
                                  pl: 1.5,
                                }}
                              >
                                <Box>
                                  <Box
                                    sx={{
                                      overflow: "hidden",
                                      textOverflow: "ellipsis",
                                      whiteSpace: "nowrap",
                                      fontWeight: "600",
                                    }}
                                  >
                                    {" "}
                                    {meeting?.meetingName}
                                  </Box>
                                  <Box
                                    sx={{
                                      fontSize: "14px",
                                      color: "rgba(0, 0, 0, 0.5)",
                                    }}
                                  >
                                    {dayjs(meeting?.meetingFrom).format(
                                      "D MMM ddd HH:mm"
                                    )}{" "}
                                    -{" "}
                                    {dayjs(meeting?.meetingTo).format(
                                      "D MMM ddd HH:mm"
                                    )}
                                  </Box>
                                </Box>
                                <Box sx={{ marginTop: "10px" }}>
                                  {isAdmin ||
                                  schedule?.inClass?.trainer?._id === userId ? (
                                    dayjs().isBetween(
                                      meeting?.meetingFrom,
                                      meeting?.meetingTo,
                                      null,
                                      "[]"
                                    ) ? (
                                      <Link
                                        to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                        target="_blank"
                                      >
                                        <Chip
                                          label="Join Now"
                                          color="success"
                                          variant="outlined"
                                          sx={{ cursor: "pointer" }}
                                        />
                                      </Link>
                                    ) : dayjs(meeting?.meetingTo).isBefore(
                                        dayjs()
                                      ) ? (
                                      <>
                                        <Chip
                                          label="Restart"
                                          sx={{ cursor: "pointer" }}
                                          onClick={() =>
                                            handleOpenTimePickerDialog(meeting)
                                          }
                                        />
                                        <TimePickerDialog
                                          updateMeetingTime={updateMeetingTime}
                                          openTimePickerDialog={
                                            openTimePickerDialog
                                          }
                                          handleCloseTimePickerDialog={
                                            handleCloseTimePickerDialog
                                          }
                                          meeting={selectedMeeting}
                                        />
                                      </>
                                    ) : dayjs(meeting?.meetingFrom).isAfter(
                                        dayjs()
                                      ) ? (
                                      <Link
                                        to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                        target="_blank"
                                      >
                                        <Chip
                                          color="primary"
                                          label="Upcoming"
                                          variant="outlined"
                                        />
                                      </Link>
                                    ) : null
                                  ) : dayjs().isBetween(
                                      meeting?.meetingFrom,
                                      meeting?.meetingTo,
                                      null,
                                      "[]"
                                    ) ? (
                                    <Link
                                      to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                      target="_blank"
                                    >
                                      <Chip
                                        label="Join Now"
                                        color="success"
                                        variant="outlined"
                                        sx={{ cursor: "pointer" }}
                                      />
                                    </Link>
                                  ) : dayjs(meeting?.meetingTo).isBefore(
                                      dayjs()
                                    ) ? (
                                    <Chip label="Ended" />
                                  ) : dayjs(meeting?.meetingFrom).isAfter(
                                      dayjs()
                                    ) ? (
                                    <Link
                                      to={`${window.location.origin}/meeting/${meeting?.meetingId}`}
                                      target="_blank"
                                    >
                                      <Chip
                                        color="primary"
                                        label="Upcoming"
                                        variant="outlined"
                                      />
                                    </Link>
                                  ) : null}
                                </Box>
                              </Box>
                              <Box>
                                {/* Nút để xoá cuộc họp */}
                                {isAdmin ||
                                schedule?.inClass?.trainer?._id === userId ? (
                                  // Chỉ hiển thị nút xoá khi người dùng là admin hoặc trainer
                                  <IconButton
                                    sx={{ cursor: "pointer" }}
                                    color="error"
                                    aria-label="delete"
                                    onClick={() => handleDeleteClicked(meeting)}
                                  >
                                    <CloseIcon />
                                  </IconButton>
                                ) : null}
                              </Box>
                            </Box>
                          </Box>
                        </li>
                      ))}
                  </ul>
                </Box>
              </Paper>
            </Grid>
          </Grid>
          <Dialog
            open={openScheduleContentDialog}
            onClose={handleCloseScheduleContentDialog}
            fullWidth
            maxWidth={"md"}
          >
            <DialogTitle>Schedule Content</DialogTitle>
            <Divider />
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                label="Content"
                type="text"
                value={scheduleContent}
                fullWidth
                required
                multiline
                rows={5}
                variant="outlined"
                onChange={(event) => setScheduleContent(event.target.value)}
              />
            </DialogContent>
            <DialogActions>
              <Button
                onClick={() => setScheduleContent(schedule?.content || "")}
              >
                Reset
              </Button>
              <Button color="error" onClick={handleCloseScheduleContentDialog}>
                Cancel
              </Button>
              <Button color="success" onClick={handleUpdateGeneralInformation}>
                Save
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog
            open={openAddClassMeetingDialog}
            keepMounted
            onClose={handleCloseAddClassMeetingDialog}
            aria-describedby="class-meeting-dialog-slide-description"
            maxWidth="lg"
          >
            <DialogTitle sx={{ padding: "0" }}>
              <Paper
                sx={{
                  display: "flex",
                  justifyContent: "space-between",
                  width: "100%",
                  paddingRight: "10px",
                  paddingLeft: "10px",
                  alignItems: "center",
                }}
              >
                <Box>Class Meeting</Box>
                <Box>
                  <IconButton onClick={handleCloseAddClassMeetingDialog}>
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                </Box>
              </Paper>
            </DialogTitle>
            <DialogContent sx={{ width: "800px" }}>
              <TextField
                margin="dense"
                label="Meeting Name"
                type="text"
                fullWidth
                required
                helperText={meetingNameHelperText}
                error={meetingNameError}
                value={meetingName}
                onChange={handleMeetingNameChange}
                sx={{ marginBottom: "10px" }}
              />
              <>
                {schedule?.inClass ? (
                  <>
                    <Grid item xs={12} sm={12} md={12}>
                      <TextField
                        margin="dense"
                        disabled
                        required
                        id="subject-code"
                        name="subject-code"
                        label="Subject Code"
                        value={
                          schedule?.inClass?.subject?.subjectCode
                            ? schedule?.inClass?.subject?.subjectCode
                            : ""
                        }
                        fullWidth
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={12}>
                      <TextField
                        margin="dense"
                        disabled
                        id="class-name"
                        name="class-name"
                        label="Class Name"
                        fullWidth
                        value={schedule?.inClass?.className}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={12}>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={selectAll}
                            onChange={handleToggleSelectAll}
                            color="primary"
                          />
                        }
                        label="Select All Students"
                      />
                      <Autocomplete
                        multiple
                        id="tags-outlined"
                        options={schedule?.inClass?.students}
                        value={selectedStudents}
                        onChange={handleUserSelection}
                        getOptionLabel={(option) => option.email}
                        filterSelectedOptions
                        renderInput={(params) => (
                          <TextField
                            margin="dense"
                            sx={{ maxWidth: "100%" }}
                            {...params}
                            required
                            label="Invite Users"
                            placeholder="Select a Users"
                          />
                        )}
                      />
                    </Grid>
                  </>
                ) : (
                  <></>
                )}
              </>
              {/* Date-Time Picker for 'From' date-time */}
              <MobileDateTimePicker
                sx={{
                  marginTop: "15px",
                  width: "100%",
                }}
                label="From"
                fullWidth
                disablePast
                value={dateTimeMeetingFrom}
                onChange={handleDateTimeMeetingFromChange}
                renderInput={(params) => <TextField {...params} />}
                PopperProps={{
                  placement: "right",
                }}
              />

              {/* Date-Time Picker for 'To' date-time */}
              <MobileDateTimePicker
                sx={{
                  marginTop: "15px",
                  width: "100%",
                }}
                label="To"
                disablePast
                fullWidth
                value={dateTimeMeetingTo}
                onChange={handleDateTimeMeetingToChange}
                renderInput={(params) => <TextField {...params} />}
                PopperProps={{
                  placement: "right",
                }}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseAddClassMeetingDialog}>Cancel</Button>
              <Button onClick={handleAddNewVideoConference} color="primary">
                Create
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog
            fullWidth
            maxWidth={"sm"}
            open={openDeleteMeetingDialog}
            onClose={handleCloseDeleteMeetingDialog}
          >
            <DialogTitle>Delete Meeting</DialogTitle>
            <Divider />
            <DialogContent>
              <Typography variant="h6">
                You want to delete meeting name:{" "}
                <span style={{ color: "#ff0000" }}>
                  {selectedMeeting?.meetingName}
                </span>
              </Typography>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleCloseDeleteMeetingDialog}>Cancel</Button>
              <Button color="error" onClick={handleDeleteMeeting}>
                Delete
              </Button>
            </DialogActions>
          </Dialog>
          <Backdrop
            sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
            open={openBackDrop}
            onClick={handleCloseBackDrop}
          >
            <CircularProgress color="inherit" />
          </Backdrop>
        </Paper>
      </LocalizationProvider>
    );
  }
  return content;
}

export default SlotDetails;
