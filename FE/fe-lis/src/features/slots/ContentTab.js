import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  Divider,
  Grid,
} from '@mui/material';
import { React, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import QuizIcon from '@mui/icons-material/Quiz';
import ConstructiveQuestionDialog from '../constructiveQuestions/ConstructiveQuestionDialog';
import {
  useAddNewCQMutation,
  useUpdateCQMutation,
  useUpdateCQTimeMutation,
} from '../constructiveQuestions/constructiveQuestionApiSlice';
import { enqueueSnackbar } from 'notistack';
import TimePickerDialog from './TimePickerDialog';
import dayjs from 'dayjs';
import useAuth from '../../hooks/useAuth';

const ContentTab = ({
  scheduleCQ,
  lessons,
  schedule,
  updateSchedule,
  refetch,
  scheduleSlot,
}) => {
  const { isAdmin, userLogin } = useAuth();
  const [selectedCQ, setSelectedCQ] = useState(null);
  const [openConstructiveQuestionDialog, setOpenConstructiveQuestionDialog] =
    useState(false);
  const handleOpenConstructiveQuestionDialog = () => {
    setOpenConstructiveQuestionDialog(true);
  };
  const handleCloseConstructiveQuestionDialog = () => {
    setOpenConstructiveQuestionDialog(false);
  };
  const [openTimePickerDialog, setOpenTimePickerDialog] = useState(false);
  const handleOpenTimePickerDialog = (CQ) => {
    setSelectedCQ(CQ);
    setOpenTimePickerDialog(true);
  };
  const handleCloseTimePickerDialog = () => {
    setOpenTimePickerDialog(false);
  };

  const handleViewConstructiveQuestions = (data) => {
    setSelectedCQ(data);
    handleOpenConstructiveQuestionDialog();
  };

  const [
    addNewCQ,
    {
      data: createdCQRes,
      isSuccess: addNewCQIsSuccess,
      isError: addNewCQIsError,
      isLoading: addNewCQIsLoading,
      error: addNewCQError,
    },
  ] = useAddNewCQMutation();

  useEffect(() => {
    if (addNewCQIsError) {
      enqueueSnackbar(
        'Constructive Question Created Fail : ' + addNewCQError?.data?.message,
        { variant: 'error' }
      );
    }
  }, [addNewCQIsError]);

  useEffect(() => {
    if (addNewCQIsSuccess) {
      const { createdCQ } = createdCQRes;
      enqueueSnackbar('New Constructive Question Created ', {
        variant: 'success',
      });
      handleAddCQToSchedule(createdCQ?._id);
      handleCloseConstructiveQuestionDialog();
    }
  }, [addNewCQIsSuccess]);

  const handleAddCQToSchedule = async (CQId) => {
    try {
      updateSchedule({
        scheduleId: schedule?._id,
        constructiveQuestion: CQId,
      });
    } catch (error) {
      enqueueSnackbar('Error with handleUpdate Function !', {
        variant: 'error',
      });
    }
  };

  const [
    updateCQTime,
    {
      isSuccess: updateCQTimeIsSuccess,
      isError: updateCQTimeIsError,
      isLoading: updateCQTimeIsLoading,
      error: updateCQTimeError,
    },
  ] = useUpdateCQTimeMutation();

  useEffect(() => {
    if (updateCQTimeIsSuccess) {
      handleCloseTimePickerDialog();
      enqueueSnackbar('Constructive Question Started', { variant: 'success' });
      refetch();
    }
    if (updateCQTimeIsLoading) {
    }
    if (updateCQTimeIsError) {
      enqueueSnackbar(
        'Constructive Question Time Setup Fail : ' +
          updateCQTimeError?.data?.message,
        { variant: 'error' }
      );
    }
  }, [updateCQTimeIsSuccess, updateCQTimeIsError, updateCQTimeIsLoading]);

  const [
    updateCQ,
    {
      isSuccess: updateCQIsSuccess,
      isError: updateCQIsError,
      isLoading: updateCQIsLoading,
      error: updateCQError,
    },
  ] = useUpdateCQMutation();

  useEffect(() => {
    if (updateCQIsSuccess) {
      enqueueSnackbar('Constructive Question Updated ', {
        variant: 'success',
      });
      refetch();
      handleCloseConstructiveQuestionDialog();
    }
  }, [updateCQIsSuccess]);

  useEffect(() => {
    if (updateCQIsError) {
      enqueueSnackbar(
        'Constructive Question Updated Fail : ' + addNewCQError?.data?.message,
        { variant: 'error' }
      );
    }
  }, [updateCQIsError]);

  return (
    <>
      {userLogin._id === schedule.inClass.trainer._id ||
      userLogin._id === schedule.trainer._id ||
      isAdmin ? (
        <Grid container mb={2}>
          <Button
            variant="contained"
            onClick={handleOpenConstructiveQuestionDialog}
          >
            Create Question
          </Button>
        </Grid>
      ) : null}
      {lessons &&
        lessons.map((lesson) =>
          lesson.constructiveQuestions.map((constructiveQuestion) => (
            <Box key={constructiveQuestion?._id}>
              <Box
                sx={{
                  width: '100%',
                  position: 'relative',
                  padding: '0 20px',
                  background: '#f5f5f5',
                  borderRadius: '8px',
                  marginBottom: '20px',
                }}
              >
                <Box sx={{ padding: '18px 0 14px', display: 'flex' }}>
                  <Box
                    sx={{
                      flex: '0 0 18px',
                      height: '18px',
                      marginRight: '8px',
                    }}
                  >
                    <QuizIcon fontSize="large" color="warning" />
                  </Box>
                  <Box>
                    <Link
                      style={{
                        fontSize: '16px',
                        color: '#3c3c3c',
                        lineHeight: '1.4',
                        display: 'inline-flex',
                        flexWrap: 'wrap',
                        width: '100%',
                        textDecoration: 'none',
                      }}
                      to={`/common/question-details?cqid=${constructiveQuestion._id}&scheduleId=${schedule._id}&slot=${scheduleSlot}`}
                    >
                      <span
                        style={{
                          fontSize: '16px',
                          color: '#3c3c3c',
                          lineHeight: '1.4',
                          display: 'inline-flex',
                          flexWrap: 'wrap',
                          width: '100%',
                          marginBottom: '10px',
                          fontWeight: '600',
                        }}
                      >
                        (Question) {constructiveQuestion?.title}
                      </span>
                      <span
                        style={{
                          color: '#eb5757',
                          backgroundColor: 'inherit',
                          width: '135px',
                          lineHeight: '1.2',
                          padding: '4px 8px',
                          borderRadius: '8px',
                          display: 'flex',
                          alignItems: 'center',
                          marginRight: '5px',
                          marginBottom: '5px',
                          cursor: 'pointer',
                          fontSize: '0.75rem',
                          letterSpacing: '0.5px',
                        }}
                      >
                        System provided
                      </span>
                      {constructiveQuestion?.from &&
                      constructiveQuestion?.to &&
                      dayjs(constructiveQuestion?.from).isBefore(dayjs()) &&
                      dayjs(constructiveQuestion?.to).isAfter(dayjs()) ? (
                        <span
                          style={{
                            lineHeight: 1.2,
                            padding: '4px 8px',
                            borderRadius: '8px',
                            display: 'flex',
                            alignItems: 'center',
                            marginRight: '5px',
                            marginBottom: '5px',
                            cursor: 'pointer',
                            backgroundColor: '#dff6dd',
                            color: '#00ac47',
                            fontSize: '0.75rem',
                            letterSpacing: '0.5px',
                            width: 'fit-content',
                            fontWeight: '400',
                          }}
                        >
                          On Going
                        </span>
                      ) : constructiveQuestion?.done &&
                        constructiveQuestion?.from &&
                        constructiveQuestion?.to &&
                        dayjs().isAfter(dayjs(constructiveQuestion?.to)) ? (
                        <span
                          style={{
                            lineHeight: 1.2,
                            padding: '4px 8px',
                            borderRadius: '8px',
                            display: 'flex',
                            alignItems: 'center',
                            marginRight: '5px',
                            marginBottom: '5px',
                            cursor: 'pointer',
                            backgroundColor: '#dff6dd',
                            color: '#00ac47',
                            fontSize: '0.75rem',
                            letterSpacing: '0.5px',
                            width: 'fit-content',
                            fontWeight: '400',
                          }}
                        >
                          Finished
                        </span>
                      ) : (
                        <span
                          style={{
                            lineHeight: 1.2,
                            padding: '4px 8px',
                            borderRadius: '8px',
                            display: 'flex',
                            alignItems: 'center',
                            marginRight: '5px',
                            marginBottom: '5px',
                            cursor: 'pointer',
                            background: '#cccccc',
                            color: '#404040',
                            fontSize: '0.75rem',
                            letterSpacing: '0.5px',
                            width: 'fit-content',
                            fontWeight: '400',
                          }}
                        >
                          Not Yet
                        </span>
                      )}
                    </Link>
                  </Box>
                  {userLogin._id === schedule.inClass.trainer._id ||
                  userLogin._id === schedule.trainer._id ||
                  isAdmin ? (
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        width: '100%',
                      }}
                    >
                      {constructiveQuestion?.done ||
                      (constructiveQuestion?.from &&
                        constructiveQuestion?.to &&
                        dayjs(constructiveQuestion?.from).isBefore(dayjs()) &&
                        dayjs(constructiveQuestion?.to).isAfter(dayjs())) ? (
                        <Button
                          variant="outlined"
                          onClick={() =>
                            handleOpenTimePickerDialog(constructiveQuestion)
                          }
                        >
                          Restart
                        </Button>
                      ) : (
                        <Button
                          variant="outlined"
                          onClick={() =>
                            handleOpenTimePickerDialog(constructiveQuestion)
                          }
                        >
                          Start
                        </Button>
                      )}
                    </Box>
                  ) : null}
                </Box>
              </Box>
            </Box>
          ))
        )}
      {scheduleCQ &&
        scheduleCQ.map((constructiveQuestion, index) => (
          <Box key={constructiveQuestion?._id + index}>
            <Box
              sx={{
                width: '100%',
                position: 'relative',
                padding: '0 20px',
                background: '#f5f5f5',
                borderRadius: '8px',
                marginBottom: '20px',
              }}
            >
              <Box sx={{ padding: '18px 0 14px', display: 'flex' }}>
                <Box
                  sx={{
                    flex: '0 0 18px',
                    height: '18px',
                    marginRight: '8px',
                  }}
                >
                  <QuizIcon fontSize="large" color="warning" />
                </Box>
                <Box>
                  <Link
                    style={{
                      fontSize: '16px',
                      color: '#3c3c3c',
                      lineHeight: '1.4',
                      display: 'inline-flex',
                      flexWrap: 'wrap',
                      width: '100%',
                      textDecoration: 'none',
                    }}
                    to={`/common/question-details?cqid=${constructiveQuestion._id}&scheduleId=${schedule._id}&slot=${scheduleSlot}`}
                  >
                    <span
                      style={{
                        fontSize: '16px',
                        color: '#3c3c3c',
                        lineHeight: '1.4',
                        display: 'inline-flex',
                        flexWrap: 'wrap',
                        width: '100%',
                        marginBottom: '10px',
                        fontWeight: '600',
                      }}
                    >
                      (Question) {constructiveQuestion?.title}
                    </span>
                    <span
                      style={{
                        color: '#eb5757',
                        backgroundColor: 'inherit',
                        width: '135px',
                        lineHeight: '1.2',
                        padding: '4px 8px',
                        borderRadius: '8px',
                        display: 'flex',
                        alignItems: 'center',
                        marginRight: '5px',
                        marginBottom: '5px',
                        cursor: 'pointer',
                        fontSize: '0.75rem',
                        letterSpacing: '0.5px',
                      }}
                    >
                      Custom
                    </span>
                    {constructiveQuestion?.from &&
                    constructiveQuestion?.to &&
                    dayjs(constructiveQuestion?.from).isBefore(dayjs()) &&
                    dayjs(constructiveQuestion?.to).isAfter(dayjs()) ? (
                      <span
                        style={{
                          lineHeight: 1.2,
                          padding: '4px 8px',
                          borderRadius: '8px',
                          display: 'flex',
                          alignItems: 'center',
                          marginRight: '5px',
                          marginBottom: '5px',
                          cursor: 'pointer',
                          backgroundColor: '#dff6dd',
                          color: '#00ac47',
                          fontSize: '0.75rem',
                          letterSpacing: '0.5px',
                          width: 'fit-content',
                          fontWeight: '400',
                        }}
                      >
                        On Going
                      </span>
                    ) : constructiveQuestion?.done &&
                      constructiveQuestion?.from &&
                      constructiveQuestion?.to &&
                      dayjs().isAfter(dayjs(constructiveQuestion?.to)) ? (
                      <span
                        style={{
                          lineHeight: 1.2,
                          padding: '4px 8px',
                          borderRadius: '8px',
                          display: 'flex',
                          alignItems: 'center',
                          marginRight: '5px',
                          marginBottom: '5px',
                          cursor: 'pointer',
                          backgroundColor: '#dff6dd',
                          color: '#00ac47',
                          fontSize: '0.75rem',
                          letterSpacing: '0.5px',
                          width: 'fit-content',
                          fontWeight: '400',
                        }}
                      >
                        Finished
                      </span>
                    ) : (
                      <span
                        style={{
                          lineHeight: 1.2,
                          padding: '4px 8px',
                          borderRadius: '8px',
                          display: 'flex',
                          alignItems: 'center',
                          marginRight: '5px',
                          marginBottom: '5px',
                          cursor: 'pointer',
                          background: '#cccccc',
                          color: '#404040',
                          fontSize: '0.75rem',
                          letterSpacing: '0.5px',
                          width: 'fit-content',
                          fontWeight: '400',
                        }}
                      >
                        Not Yet
                      </span>
                    )}
                  </Link>
                </Box>
                {userLogin._id === schedule.inClass.trainer._id ||
                userLogin._id === schedule.trainer._id ||
                isAdmin ? (
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      alignItems: 'flex-end',
                      justifyContent: 'flex-end',
                      width: '100%',
                      marginRight: 'auto',
                    }}
                  >
                    <Button
                      color="success"
                      onClick={() =>
                        handleViewConstructiveQuestions(constructiveQuestion)
                      }
                    >
                      Setting
                    </Button>

                    {constructiveQuestion?.done ||
                    (constructiveQuestion?.from &&
                      constructiveQuestion?.to &&
                      dayjs(constructiveQuestion?.from).isBefore(dayjs()) &&
                      dayjs(constructiveQuestion?.to).isAfter(dayjs())) ? (
                      <Button
                        variant="outlined"
                        onClick={() =>
                          handleOpenTimePickerDialog(constructiveQuestion)
                        }
                      >
                        Restart
                      </Button>
                    ) : (
                      <Button
                        variant="outlined"
                        onClick={() =>
                          handleOpenTimePickerDialog(constructiveQuestion)
                        }
                      >
                        Start
                      </Button>
                    )}
                  </Box>
                ) : null}
              </Box>
            </Box>
          </Box>
        ))}

      <ConstructiveQuestionDialog
        updateCQ={updateCQ}
        selectedCQ={selectedCQ}
        openConstructiveQuestionDialog={openConstructiveQuestionDialog}
        handleCloseConstructiveQuestionDialog={
          handleCloseConstructiveQuestionDialog
        }
        addNewCQ={addNewCQ}
      />

      <TimePickerDialog
        CQ={selectedCQ}
        updateCQTime={updateCQTime}
        openTimePickerDialog={openTimePickerDialog}
        handleCloseTimePickerDialog={handleCloseTimePickerDialog}
        schedule={schedule}
      />
    </>
  );
};

export default ContentTab;
