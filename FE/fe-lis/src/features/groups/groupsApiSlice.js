import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

const groupAdapter = createEntityAdapter({});

const initialState = groupAdapter.getInitialState();

export const groupApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getGroupsByScheduleId: builder.query({
      query: (id) => ({
        url: "/groups/" + id,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
    getGroupOfStudentId: builder.query({
      query: (query) => ({
        url: "/groups/" + query,
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    createGroups: builder.mutation({
      query: (initialGroup) => ({
        url: "/groups",
        method: "POST",
        body: {
          ...initialGroup,
        },
      }),
    }),
    combineNewGroups: builder.mutation({
      query: (initialGroup) => ({
        url: "/groups/combine",
        method: "POST",
        body: {
          ...initialGroup,
        },
      }),
    }),

    removeStudentOutOfGroup: builder.mutation({
      query: (initialGroup) => ({
        url: "/groups/remove",
        method: "PATCH",
        body: {
          ...initialGroup,
        },
      }),
    }),

    moveStudentToGroup: builder.mutation({
      query: (initialGroup) => ({
        url: "/groups/move",
        method: "PATCH",
        body: {
          ...initialGroup,
        },
      }),
    }),
  }),
});

export const {
  useGetGroupsByScheduleIdQuery,
  useGetGroupOfStudentIdQuery,
  useCreateGroupsMutation,
  useMoveStudentToGroupMutation,
  useRemoveStudentOutOfGroupMutation,
  useCombineNewGroupsMutation,
} = groupApiSlice;
