import CloseIcon from "@mui/icons-material/Close";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {
  Backdrop,
  Box,
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import Tab from "@mui/material/Tab";
import { enqueueSnackbar } from "notistack";
import React, { useEffect, useMemo, useState } from "react";
import * as XLSX from "xlsx";
import { useCreateGroupsMutation } from "./groupsApiSlice";

const CreateGroupDialog = ({
  selectedSchedule,
  classData,
  openCreateGroupsDialog,
  handleCloseCreateGroupsDialog,
}) => {
  const [openBackdrop, setOpenBackdrop] = useState(false);
  const handleOpenBackdrop = () => {
    setOpenBackdrop(true);
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  const [
    createGroups,
    {
      isSuccess: createGroupsIsSuccess,
      isLoading: createGroupsIsLoading,
      isError: createGroupsIsError,
      error: createGroupsError,
    },
  ] = useCreateGroupsMutation();
  const [viewXLSXTable, setViewXLSXTable] = useState(false);
  const [viewRandomTable, setViewRandomTable] = useState(false);
  const [groupsNumber, setGroupsNumber] = useState(1);

  const handleGroupsNumberChange = (event) => {
    const inputValue = event.target.value;
    const minAllowed = 1;
    const maxAllowed = Math.ceil(classData?.students?.length / 1);
    let sanitizedValue = inputValue;

    // Perform validation to ensure the value is within the allowed range
    if (inputValue < minAllowed) {
      sanitizedValue = minAllowed;
    } else if (inputValue > maxAllowed) {
      sanitizedValue = maxAllowed;
    }

    // Update the state with the sanitized value
    setGroupsNumber(sanitizedValue);
  };


  // on change states
  const [selectedFile, setSelectedFile] = useState(null);
  const [excelFile, setExcelFile] = useState(null);
  const [excelFileError, setExcelFileError] = useState(null);

  // submit
  const [excelData, setExcelData] = useState(null);
  // it will contain array of objects

  const [members, setMembers] = useState(null);
  const [randomMembers, setRandomMembers] = useState(null);
  // handle File
  const fileType = [
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ];

  const checkValidSheet = (worksheet) => {
    return (
      worksheet?.A1.v.trim() === "email" &&
      worksheet?.B1.v.trim() === "fullName" &&
      worksheet?.C1.v.trim() === "group"
    );
  };

  const reformExcelDataToMembersData = (excelData) => {
    let membersData = [];

    if (excelData !== null) {
      excelData.forEach((data) => {
        try {
          const email = data?.["email"];
          const fullName = data?.["fullName"];
          const group = parseInt(data?.["group"]);
          if (isNaN(group)) {
            return; // Skip this data item if Slot is not a valid number or Day is not a valid date
          }
          const newMember = {
            email,
            fullName,
            group,
          };
          membersData.push(newMember); // Push the new schedule into the array
        } catch (error) {
          // If any error occurs during processing, skip this data item
          return;
        }
      });
    }

    return membersData.sort((a, b) => a.group - b.group);
  };

  const handleFile = (e) => {
    setSelectedFile(e.target.files[0]);
    setViewXLSXTable(false);
    setExcelData(null);
    setMembers(null);
  };

  // submit function
  const handleSubmit = () => {
    if (excelFile !== null) {
      const workbook = XLSX.read(excelFile, { type: "buffer" });
      const worksheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[worksheetName];
      const data = XLSX.utils.sheet_to_json(worksheet);
      if (checkValidSheet(worksheet)) {
        setExcelData(data);
      } else {
        setExcelFileError("Please make sure your import right file !");
        setExcelFile(null);
        setExcelData(null);
        setMembers(null);
      }
    } else {
      setExcelData(null);
    }
  };

  useEffect(() => {
    if (selectedFile) {
      if (selectedFile && fileType.includes(selectedFile.type)) {
        let reader = new FileReader();
        reader.readAsArrayBuffer(selectedFile);
        reader.onload = (e) => {
          setExcelFileError(null);
          setExcelFile(e.target.result);
        };
      } else {
        setExcelFileError("Please select only excel file types");
        setExcelFile(null);
        setExcelData(null);
        setMembers(null);
      }
    } else {
      console.log("please select your xlsx file");
    }
  }, [selectedFile]);

  useEffect(() => {
    if (excelFile) {
      handleSubmit();
    }
  }, [excelFile]);

  useEffect(() => {
    if (excelData) {
      setMembers(reformExcelDataToMembersData(excelData));
    } else {
      setMembers(null);
    }
  }, [excelData]);

  const [valueCreateGroups, setValueCreateGroups] = useState("1");

  const handleCreateGroupsChange = (event, newValue) => {
    setValueCreateGroups(newValue);
  };

  const handleExportXLSX = () => {
    if (classData && classData?.students && classData?.students.length !== 0) {
      const studentsDataSheet = classData?.students.map((student) => {
        return {
          email: student.email,
          fullName: student.fullName,
          group: null,
        };
      });
      const wb = XLSX.utils.book_new();
      const ws = XLSX.utils.json_to_sheet(studentsDataSheet);
      XLSX.utils.book_append_sheet(wb, ws, "MySheet1");
      XLSX.writeFile(
        wb,
        `${classData?.className}-${classData?.subject?.subjectCode}-Students.xlsx`
      );
    } else {
      enqueueSnackbar("Something wrong with students data", {
        variant: "error",
      });
    }
  };

  const xlsxDataTableContent = useMemo(() => {
    return (
      <>
        <Paper sx={{ p: 3, mb: 3 }}>
          <Grid container justifyContent={"space-between"}>
            <Grid item>
              <Typography variant="h6">XLSX Data Table</Typography>
            </Grid>
          </Grid>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell sx={{ color: "indianred" }}>No.</TableCell>
                <TableCell sx={{ color: "indianred", width: "30%" }}>
                  Email
                </TableCell>
                <TableCell sx={{ color: "indianred" }}>Full Name</TableCell>
                <TableCell sx={{ color: "indianred" }}>Group</TableCell>
              </TableRow>
            </TableHead>
            {members !== null ? (
              <TableBody>
                {members.map((member, index) => (
                  <TableRow key={index}>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      {index + 1}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {member.email}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {member?.fullName}
                    </TableCell>
                    <TableCell>{member.group}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            ) : null}
          </Table>
        </Paper>
      </>
    );
  }, [members]);

  const randomMembersToGroups = (students, groupLimit) => {
    if (students && students?.length !== 0 && Array.isArray(students)) {
      let newRandomMembers = [];
      const studentsPerGroup = Math.ceil(students.length / groupLimit);
      students.forEach((student, index) => {
        newRandomMembers.push({
          email: student?.email,
          fullName: student?.fullName,
          group: Math.ceil((index + 1) / studentsPerGroup),
        });
      });
      setRandomMembers(newRandomMembers);
    }
  };

  const handlePreviewRandomTable = () => {
    setViewRandomTable(true);
    randomMembersToGroups(classData.students, groupsNumber);
  };

  const randomMembersTableContent = useMemo(() => {
    return (
      <>
        <Paper sx={{ p: 3, mb: 3 }}>
          <Grid container justifyContent={"space-between"}>
            <Grid item>
              <Typography variant="h6">Random Data Table</Typography>
            </Grid>
          </Grid>
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell sx={{ color: "indianred" }}>No.</TableCell>
                <TableCell sx={{ color: "indianred", width: "30%" }}>
                  Email
                </TableCell>
                <TableCell sx={{ color: "indianred" }}>Full Name</TableCell>
                <TableCell sx={{ color: "indianred" }}>Group</TableCell>
              </TableRow>
            </TableHead>
            {randomMembers !== null ? (
              <TableBody>
                {randomMembers.map((member, index) => (
                  <TableRow key={index}>
                    <TableCell sx={{ wordBreak: "break-word" }}>
                      {index + 1}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {member.email}
                    </TableCell>
                    <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                      {member?.fullName}
                    </TableCell>
                    <TableCell>{member.group}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            ) : null}
          </Table>
        </Paper>
      </>
    );
  }, [randomMembers]);

  const handleCancel = () => {
    handleCloseCreateGroupsDialog();
    setExcelData(null);
    setExcelFile(null);
    setMembers(null);
    setRandomMembers(null);
    setGroupsNumber(1);
    setSelectedFile(null);
    setViewXLSXTable(false);
    setViewRandomTable(false);
  };

  const handleFinish = async (data) => {
    try {
      await createGroups({ scheduleId: selectedSchedule?._id, members: data });
    } catch (error) {
      enqueueSnackbar("Error with Finish Function " + error);
    }
  };

  useEffect(() => {
    if (createGroupsIsSuccess) {
      enqueueSnackbar("Create Groups Successful !", { variant: "success" });
      handleCancel();
      handleCloseBackdrop();
    }
    if (createGroupsIsError) {
      enqueueSnackbar(
        "Create Groups Fail !" + createGroupsError?.data?.message,
        { variant: "error" }
      );
      handleCloseBackdrop();
    }
    if (createGroupsIsLoading) {
      handleOpenBackdrop();
    }
  }, [createGroupsIsSuccess, createGroupsIsError, createGroupsIsLoading]);

  return (
    <>
      {/* Create Group  */}
      <Dialog
        open={openCreateGroupsDialog}
        keepMounted
        onClose={handleCancel}
        aria-describedby="groups-dialog-slide-description"
        maxWidth="lg"
        className="dialog-groups"
      >
        <DialogTitle sx={{ padding: "0" }}>
          <Paper
            sx={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              paddingRight: "10px",
              paddingLeft: "10px",
              alignItems: "center",
            }}
          >
            <Box>Create Groups</Box>
            <Box>
              <IconButton onClick={handleCancel}>
                <CloseIcon fontSize="inherit" />
              </IconButton>
            </Box>
          </Paper>
        </DialogTitle>
        <DialogContent sx={{ width: "1200px" }}>
          <Grid container sx={{ width: "100%" }}>
            <Grid
              item
              xs={12}
              sx={{
                textAlign: "center",
                padding: "24px 0",
                width: "100%",
                fontSize: "1.125rem",
              }}
            >
              <span
                style={{ marginTop: "24px" }}
              >{`${classData?.className} - ${classData?.subject?.subjectCode}`}</span>
            </Grid>
            <Grid item xs={12}>
              <TabContext value={valueCreateGroups}>
                <Box>
                  <TabList
                    className="tablist-create-groups"
                    onChange={handleCreateGroupsChange}
                    sx={{
                      ".Mui-selected": {
                        color: "rgb(237, 108, 2)",
                      },
                    }}
                    TabIndicatorProps={{
                      style: { background: "rgb(237, 108, 2)" },
                    }}
                  >
                    <Tab label="Upload file" value="1" />
                    <Tab label="Random" value="2" />
                    {/* <Tab label="Choose from schedule" value="3" /> */}
                  </TabList>
                </Box>
                <TabPanel sx={{ padding: "0" }} value="1">
                  <Box
                    sx={{
                      width: "100%",
                      display: "flex",
                      flexDirection: "column",
                      textAlign: "center",
                      alignItems: "center",
                      fontSize: "1.125rem",
                    }}
                  >
                    <Box
                      sx={{
                        borderBottom: "1px solid #c8c8c8",
                        padding: "24px 0",
                        width: "100%",
                      }}
                    >
                      <span>
                        You need to download the File of Student List to create
                        groups
                      </span>
                      <Button onClick={handleExportXLSX}>Download</Button>
                    </Box>

                    <Box
                      sx={{
                        borderBottom: "1px solid #c8c8c8",
                        display: "flex",
                        flexDirection: "column",
                        textAlign: "center",
                        alignItems: "center",
                        padding: "24px 0",
                        width: "100%",
                      }}
                    >
                      <span>Please click "Upload" to import the file</span>
                      {excelFileError !== null
                        ? excelFileError
                        : selectedFile?.name}
                      <label htmlFor="upload-xlsx">
                        <input
                          style={{ display: "none" }}
                          id="upload-xlsx"
                          name="upload-xlsx"
                          type="file"
                          onChange={handleFile}
                        />
                        <Button
                          color="success"
                          variant="contained"
                          component="span"
                        >
                          Upload
                        </Button>
                      </label>
                    </Box>
                    <DialogActions>
                      <Button onClick={handleCancel}>Cancel</Button>
                      <Button
                        variant="contained"
                        color="primary"
                        disabled={!members}
                        onClick={() => setViewXLSXTable((prev) => !prev)}
                      >
                        Preview
                      </Button>
                      {members && members.length !== 0 && (
                        <Button
                          color="success"
                          onClick={() => handleFinish(members)}
                        >
                          Finish
                        </Button>
                      )}
                    </DialogActions>
                    {viewXLSXTable ? (
                      <Box sx={{ width: "100%" }}>{xlsxDataTableContent}</Box>
                    ) : (
                      <Box
                        sx={{
                          borderBottom: "1px solid #c8c8c8",
                          display: "flex",
                          padding: "24px 0",
                          width: "100%",
                        }}
                      >
                        <h5
                          style={{
                            fontSize: "1.125rem",
                            fontWeight: "400",
                            textAlign: "left",
                          }}
                        >
                          <h4 style={{ marginBottom: "0.75rem" }}>
                            Upload File
                          </h4>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 1: To create groups, you need to download the
                            student list by clicking link "File of Student List"
                          </p>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 2: Add group names to the column "Group Name".
                            The first member of each group is defaulted as a
                            group leader
                          </p>
                          <p style={{ marginBottom: "0.75rem" }}></p>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 3: Click button "Upload" to upload the file of
                            group list
                          </p>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 4: Click "Preview" to preview groups. The
                            groups are displayed on the screen.
                          </p>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 5: Click "Finish" to complete creating groups.
                            Then you can start the constructive question.
                          </p>
                        </h5>
                      </Box>
                    )}
                  </Box>
                </TabPanel>
                <TabPanel sx={{ padding: "0" }} value="2">
                  <Box
                    sx={{
                      width: "100%",
                      display: "flex",
                      flexDirection: "column",
                      textAlign: "center",
                      alignItems: "center",
                      fontSize: "1.125rem",
                    }}
                  >
                    <Box
                      sx={{
                        borderBottom: "1px solid #c8c8c8",
                        padding: "24px 0",
                        width: "100%",
                      }}
                    >
                      <span>
                        Class size:{" "}
                        <span style={{ color: "rgb(237, 108, 2)" }}>
                          {classData?.students.length}
                        </span>{" "}
                        students
                      </span>
                    </Box>
                    <Box
                      sx={{
                        borderBottom: "1px solid #c8c8c8",
                        display: "flex",
                        flexDirection: "column",
                        textAlign: "center",
                        alignItems: "center",
                        padding: "24px 0",
                        width: "100%",
                      }}
                    >
                      <span>How many groups do you want to create?</span>
                      <TextField
                        margin="dense"
                        fullWidth
                        autoFocus
                        value={groupsNumber}
                        onChange={handleGroupsNumberChange} // Use the custom onChange handler
                        label="Enter number"
                        type="number"
                        placeholder="Enter number"
                        sx={{ width: "300px" }}
                        inputProps={{
                          min: 1,
                          max: Math.ceil(classData?.students?.length / 1),
                        }}
                      />
                    </Box>
                    <DialogActions>
                      <Button onClick={handleCancel}>Cancel</Button>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={handlePreviewRandomTable}
                      >
                        Preview
                      </Button>
                      {randomMembers && randomMembers.length !== 0 && (
                        <Button
                          color="success"
                          onClick={() => handleFinish(randomMembers)}
                        >
                          Finish
                        </Button>
                      )}
                    </DialogActions>
                    {viewRandomTable ? (
                      <Box sx={{ width: "100%" }}>
                        {randomMembersTableContent}
                      </Box>
                    ) : (
                      <Box
                        sx={{
                          borderBottom: "1px solid #c8c8c8",
                          display: "flex",
                          padding: "24px 0",
                          width: "100%",
                        }}
                      >
                        <h5
                          style={{
                            fontSize: "1.125rem",
                            fontWeight: "400",
                            textAlign: "left",
                          }}
                        >
                          <h4 style={{ marginBottom: "0.75rem" }}>
                            Create random groups
                          </h4>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 1: The system will display the number of
                            students in a slot and ask you to type the number of
                            groups to create.
                          </p>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 2: Click "Preview" to preview groups with
                            random members. The first member of each group is
                            defaulted as the group leader.
                          </p>
                          <p style={{ marginBottom: "0.75rem" }}></p>
                          <p style={{ marginBottom: "0.75rem" }}>
                            Step 3: Click "Finish" to complete creating groups
                            randomly.
                          </p>
                        </h5>
                      </Box>
                    )}
                  </Box>
                </TabPanel>
                <TabPanel sx={{ padding: "0" }} value="3">
                  <Box
                    sx={{
                      width: "100%",
                      display: "flex",
                      flexDirection: "column",
                      textAlign: "center",
                      alignItems: "center",
                      fontSize: "1.125rem",
                    }}
                  >
                    <Box
                      sx={{
                        borderBottom: "1px solid #c8c8c8",
                        padding: "24px 0",
                        width: "100%",
                      }}
                    >
                      <span>
                        Class size:{" "}
                        <span style={{ color: "rgb(237, 108, 2)" }}>
                          {classData?.students.length}
                        </span>{" "}
                        students
                      </span>
                    </Box>
                  </Box>
                </TabPanel>
              </TabContext>
            </Grid>
          </Grid>
        </DialogContent>
        <Backdrop
          open={openBackdrop}
          onClick={handleCloseBackdrop}
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>
    </>
  );
};

export default CreateGroupDialog;
