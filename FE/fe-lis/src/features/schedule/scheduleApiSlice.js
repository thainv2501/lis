import { createEntityAdapter } from '@reduxjs/toolkit';
import { apiSlice } from '../../app/api/apiSlice';

// Tạo adapter cho danh sách Lesson
const scheduleAdapter = createEntityAdapter({});

// Khởi tạo state ban đầu cho danh sách Lesson
const initialState = scheduleAdapter.getInitialState();

// Tạo slice API cho Lesson
export const scheduleApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    // get schedules with classId
    getSchedules: builder.query({
      query: (query) => ({
        url: '/schedules' + query,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
    // get schedule by scheduleId
    getScheduleById: builder.query({
      query: (id) => ({
        url: '/schedules/' + id,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),

    createNewSchedule: builder.mutation({
      query: (initialData) => ({
        url: '/schedules',
        method: 'POST',
        body: {
          ...initialData,
        },
      }),
    }),

    importSchedulesFromXLSX: builder.mutation({
      query: (initialData) => ({
        url: '/schedules/XLSX',
        method: 'POST',
        body: {
          ...initialData,
        },
      }),
    }),

    filterSchedulesByTabAndSemester: builder.mutation({
      query: (initialScheduleData) => ({
        url: '/schedules/scheduleFilter',
        method: 'POST',
        body: {
          ...initialScheduleData,
        },
      }),
      invalidatesTags: [{ type: 'Schedule', id: 'LIST' }],
      transformResponse: (responseData) => {
        const loadedSchedules = responseData.schedules.map((data) => {
          data.id = data._id;
          return data;
        });
        return { ...responseData, schedules: loadedSchedules };
      },
    }),

    updateSchedule: builder.mutation({
      query: (initialData) => ({
        url: '/schedules/',
        method: 'PATCH',
        body: {
          ...initialData,
        },
      }),
    }),

    getSchedulesByClassId: builder.query({
      query: (classId) => ({
        url: '/schedules/classObj/' + classId,
        validateStatus: (response, result) => {
          return response.status === 200 && !result.isError;
        },
      }),
      transformResponse: (responseData) => {
        return responseData;
      },
    }),
  }),
});

// Export các hooks và actions từ slice API của Lesson
export const {
  useGetSchedulesQuery,
  useCreateNewScheduleMutation,
  useImportSchedulesFromXLSXMutation,
  useGetScheduleByIdQuery,
  useUpdateScheduleMutation,
  useFilterSchedulesByTabAndSemesterMutation,
  useGetSchedulesByClassIdQuery,
} = scheduleApiSlice;
