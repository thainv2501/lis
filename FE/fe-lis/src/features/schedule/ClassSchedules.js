import {
  Button,
  Grid,
  Pagination,
  Paper,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect, useState } from "react";
import Title from "../../components/Title";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useGetClassWithIdQuery } from "../class/classApiSlice";
import { useGetSchedulesQuery } from "./scheduleApiSlice";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import EditNoteIcon from "@mui/icons-material/EditNote";
import dayjs from "dayjs";
import ImportScheduleFromXLSXDialog from "./ImportScheduleFromXLSXDialog";

const ClassSchedules = () => {
  const { classId } = useParams();
  const navigate = useNavigate();
  let content;
  const [page, setPage] = useState(1);
  const [openImportXLSXDialog, setOpenImportXLSXDialog] = useState(false);

  const handleOpenImportXLSXDialog = () => {
    setOpenImportXLSXDialog(true);
  };
  const handleCloseImportXLSXDialog = () => {
    setOpenImportXLSXDialog(false);
  };

  const {
    data: schedulesDataRes,
    isLoading: getSchedulesDataIsLoading,
    isSuccess: getSchedulesDataIsSuccess,
    isError: getSchedulesDataIsError,
    error: getSchedulesDataError,
    refetch,
  } = useGetSchedulesQuery(`?classId=${classId}&page=${page}`, {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  const {
    data: classData,
    isLoading: getClassDataIsLoading,
    isSuccess: getClassDataIsSuccess,
    isError: getClassDataIsError,
    error: getClassDataError,
  } = useGetClassWithIdQuery(classId);

  const handlePaginationChanged = (event, value) => {
    setPage(value);
  };

  if (getClassDataIsLoading || getSchedulesDataIsLoading) {
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );
  }
  let tableContent = null;

  if (getSchedulesDataIsError) {
    tableContent = (
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Typography variant="h5" color={"red"}>
            {getSchedulesDataError?.data?.message}
          </Typography>
        </Grid>
      </Grid>
    );
  }

  if (getClassDataIsError) {
    navigate("/404");
  }

  const handleSlotTime = (slot) => {
    let slotStartAtFloat = 7.5 + (slot - 1) * 1.5;
    if (slot >= 4) {
      slotStartAtFloat = 12.5 + (slot - 4) * 1.5;
    }
    const hours = Math.floor(slotStartAtFloat);
    const minutes = Math.floor((slotStartAtFloat - hours) * 60);
    const slotStartAtTime = dayjs().hour(hours).minute(minutes).format("HH:mm");
    const slotEndAtTime = dayjs()
      .hour(hours + 1)
      .minute(minutes + 30)
      .format("HH:mm");

    return `${slotStartAtTime} - ${slotEndAtTime}`;
  };

  if (getSchedulesDataIsSuccess && getClassDataIsSuccess) {
    const { schedules, schedulesCount } = schedulesDataRes;
    const pages = Math.ceil(schedulesCount / 10);
    tableContent = (
      <>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell sx={{ color: "indianred" }}>No.</TableCell>
              <TableCell sx={{ color: "indianred" }}>Day</TableCell>
              <TableCell sx={{ color: "indianred" }}>Slot</TableCell>
              <TableCell sx={{ color: "indianred" }}>Time</TableCell>
              <TableCell sx={{ color: "indianred" }}>Content</TableCell>
              <TableCell sx={{ color: "indianred" }}>Status</TableCell>
              <TableCell align="center" sx={{ color: "indianred" }}>
                Actions
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {schedules &&
              schedules.map((data, index) => (
                <TableRow key={data?._id} hover>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {(page - 1) * 10 + index + 1}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {dayjs(data?.day).format("dddd,MMMM D,YYYY")}
                  </TableCell>
                  <TableCell>{data?.slotInDay}</TableCell>
                  <TableCell>{handleSlotTime(data?.slotInDay)}</TableCell>

                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {data?.lessons &&
                      data?.lessons?.map((lesson) => (
                        <p>
                          Chapter {lesson?.lessonChapter} - Lesson{" "}
                          {lesson?.lessonNumber}
                        </p>
                      ))}
                  </TableCell>
                  <TableCell>
                    {data?.done ? (
                      <Typography color={"green"}>Done</Typography>
                    ) : (
                      <Typography color={"gray"}>Not Yet</Typography>
                    )}
                  </TableCell>
                  <TableCell>
                    <Grid container justifyContent={"flex-end"}>
                      <Tooltip title={"View and Edit"} placement="top" arrow>
                        <Link to={data?._id}>
                          <Button color="success">
                            <EditNoteIcon fontSize="large" />
                          </Button>
                        </Link>
                      </Tooltip>
                      {/* <Tooltip title={"Delete"} placement="top" arrow>
                        <Button color="error">
                          <DeleteForeverIcon />
                        </Button>
                      </Tooltip> */}
                    </Grid>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
        <Pagination
          sx={{ paddingTop: "18px" }}
          count={pages}
          page={page}
          onChange={handlePaginationChanged}
          variant="outlined"
          color="primary"
        />
      </>
    );
  }

  content = (
    <Paper sx={{ p: 2 }}>
      <Grid container spacing={3}>
        {/* Recent Dash Board */}
        <Grid item xs={12}>
          <Grid
            item
            container
            spacing={3}
            xs={12}
            justifyContent={"space-between"}
          >
            <Grid item>
              <Title>
                {`${classData?.semester?.code} - ${classData?.semester?.name} -
                ${classData?.className} - ${classData?.subject?.subjectCode} -
                ${classData?.subject?.subjectName} Schedules`}
              </Title>
            </Grid>
            <Grid item display={"flex"} gap={3}>
              <Link to={"new-schedule"}>
                <Button variant="contained">Add New Schedule</Button>
              </Link>
              <Button
                variant="contained"
                color="success"
                onClick={handleOpenImportXLSXDialog}
              >
                import xlsx
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          {tableContent}
        </Grid>
      </Grid>
      <ImportScheduleFromXLSXDialog
        refetch={refetch}
        inClass={classData}
        handleCloseImportXLSXDialog={handleCloseImportXLSXDialog}
        openImportXLSXDialog={openImportXLSXDialog}
      />
    </Paper>
  );
  return content;
};

export default ClassSchedules;
