import CloseIcon from "@mui/icons-material/Close";
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import Backdrop from "@mui/material/Backdrop";
import CircularProgress from "@mui/material/CircularProgress";
import { React, useEffect, useState } from "react";
import dayjs from "dayjs";
import * as XLSX from "xlsx";
import { useImportSchedulesFromXLSXMutation } from "./scheduleApiSlice";
import { enqueueSnackbar } from "notistack";
import useAuth from "../../hooks/useAuth";

const ImportScheduleFromXLSXDialog = ({
  handleCloseImportXLSXDialog,
  openImportXLSXDialog,
  inClass,
  refetch,
}) => {
  const { userLogin } = useAuth();
  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };

  const [
    importSchedulesFromXLSX,
    {
      isSuccess: importSchedulesFromXLSXIsSuccess,
      isError: importSchedulesFromXLSXIsError,
      isLoading: importSchedulesFromXLSXIsLoading,
      error: importSchedulesFromXLSXError,
    },
  ] = useImportSchedulesFromXLSXMutation();
  // on change states
  const [selectedFile, setSelectedFile] = useState(null);
  const [excelFile, setExcelFile] = useState(null);
  const [excelFileError, setExcelFileError] = useState(null);

  // submit
  const [excelData, setExcelData] = useState(null);
  // it will contain array of objects

  const [schedules, setSchedules] = useState(null);
  // handle File
  const fileType = [
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  ];

  const checkValidSheet = (worksheet) => {
    try {
      return (
        worksheet?.A1.v.trim() === "Day" &&
        worksheet?.B1.v.trim() === "Slot" &&
        worksheet?.C1.v.trim() === "Lessons"
      );
    } catch (error) {
      return false;
    }
  };

  const reformExcelDataToSchedulesData = (excelData) => {
    let schedulesData = [];

    if (excelData !== null) {
      excelData.forEach((data) => {
        try {
          const day = dayjs(data?.["Day"]).format("YYYY-MM-DD");
          const slotInDay = parseInt(data?.["Slot"]);
          let lessons = [];

          if (isNaN(data?.["Lessons"])) {
            const lessonsString = data?.["Lessons"].split(",");
            lessonsString.forEach((lesson) => {
              const lessonNumber = parseInt(lesson.trim());
              if (!isNaN(lessonNumber)) {
                lessons.push(lessonNumber);
              }
            });
          } else {
            lessons.push(data?.["Lessons"]);
          }

          if (isNaN(slotInDay) || day === "Invalid Date") {
            return; // Skip this data item if Slot is not a valid number or Day is not a valid date
          }

          const newSchedule = {
            day,
            slotInDay,
            lessons,
          };

          schedulesData.push(newSchedule); // Push the new schedule into the array
        } catch (error) {
          // If any error occurs during processing, skip this data item
          return;
        }
      });
    }

    return schedulesData;
  };

  const handleFile = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  // submit function
  const handleSubmit = () => {
    if (excelFile !== null) {
      const workbook = XLSX.read(excelFile, { type: "buffer" });
      const worksheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[worksheetName];
      const data = XLSX.utils.sheet_to_json(worksheet);
      if (checkValidSheet(worksheet)) {
        setExcelData(data);
      } else {
        setExcelFileError("Please make sure your import right file !");
        setExcelFile(null);
        setExcelData(null);
        setSchedules(null);
      }
    } else {
      setExcelData(null);
    }
  };

  useEffect(() => {
    if (selectedFile) {
      if (selectedFile && fileType.includes(selectedFile.type)) {
        let reader = new FileReader();
        reader.readAsArrayBuffer(selectedFile);
        reader.onload = (e) => {
          setExcelFileError(null);
          setExcelFile(e.target.result);
        };
      } else {
        setExcelFileError("Please select only excel file types");
        setExcelFile(null);
        setExcelData(null);
        setSchedules(null);
      }
    } else {
      console.log("please select your xlsx file");
    }
  }, [selectedFile]);

  useEffect(() => {
    if (excelFile) {
      handleSubmit();
    }
  }, [excelFile]);

  useEffect(() => {
    if (excelData) {
      setSchedules(reformExcelDataToSchedulesData(excelData));
    } else {
      setSchedules(null);
    }
  }, [excelData]);

  const xlsxDataTableContent = (
    <>
      <Paper sx={{ p: 3, mb: 3 }}>
        <Grid container justifyContent={"space-between"}>
          <Grid item>
            <Typography variant="h6">XLSX Data Table</Typography>
          </Grid>
        </Grid>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell sx={{ color: "indianred" }}>No.</TableCell>
              <TableCell sx={{ color: "indianred", width: "30%" }}>
                Day
              </TableCell>
              <TableCell sx={{ color: "indianred" }}>Slot</TableCell>
              <TableCell sx={{ color: "indianred" }}>Lessons</TableCell>
            </TableRow>
          </TableHead>
          {schedules !== null ? (
            <TableBody>
              {schedules.map((schedule, index) => (
                <TableRow key={index}>
                  <TableCell sx={{ wordBreak: "break-word" }}>
                    {index + 1}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {dayjs(schedule?.day, "YYYY-MM-DD").format(
                      "dddd,MMMM D,YYYY"
                    )}
                  </TableCell>
                  <TableCell sx={{ wordBreak: "break-word", width: "20%" }}>
                    {schedule?.slotInDay}
                  </TableCell>
                  <TableCell>
                    {schedule?.lessons.map((lesson) => (
                      <Typography>Lesson {lesson}</Typography>
                    ))}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          ) : null}
        </Table>
      </Paper>
    </>
  );

  const handleImportXlSXData = async () => {
    try {
      handleOpenBackDrop();
      await importSchedulesFromXLSX({
        inClass,
        schedules,
        createdBy: userLogin?._id,
      });
    } catch (error) {
      enqueueSnackbar("Error while import from XlSX", { variant: "error" });
    }
  };

  useEffect(() => {
    if (importSchedulesFromXLSXIsError) {
      enqueueSnackbar(
        "Import from XLSX Error : " +
          importSchedulesFromXLSXError?.data?.message,
        { variant: "error" }
      );
      handleCloseBackDrop();
    }
  }, [importSchedulesFromXLSXIsError]);
  useEffect(() => {
    if (importSchedulesFromXLSXIsSuccess) {
      enqueueSnackbar("Import from XLSX Success", { variant: "success" });
      handleCloseBackDrop();
      handleCloseImportXLSXDialog();
      setExcelFile(null);
      setExcelData(null);
      setSchedules(null);
      refetch();
    }
  }, [importSchedulesFromXLSXIsSuccess]);

  const handleExportXLSX = () => {
    const dataSheet = [1, 2, 3, 4, 5].map((index) => {
      return {
        Day: dayjs().format("YYYY-MM-DD"),
        Slot: index,
        Lessons: `${index - 1},${index}`,
      };
    });
    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(dataSheet);
    XLSX.utils.book_append_sheet(wb, ws, "MySheet1");
    XLSX.writeFile(
      wb,
      `${inClass.semester.code}-${inClass.className}-${inClass.subject.subjectCode}-SchedulesTemplate.xlsx`
    );
  };

  return (
    <>
      <Dialog
        open={openImportXLSXDialog}
        keepMounted
        onClose={handleCloseImportXLSXDialog}
        aria-describedby="groups-dialog-slide-description"
        maxWidth="lg"
        className="dialog-groups"
      >
        <DialogTitle sx={{ padding: "0" }}>
          <Paper
            sx={{
              display: "flex",
              justifyContent: "space-between",
              width: "100%",
              paddingRight: "10px",
              paddingLeft: "10px",
              alignItems: "center",
            }}
          >
            <Box>Import Schedules</Box>
            <Box>
              <IconButton>
                <CloseIcon
                  fontSize="inherit"
                  onClick={handleCloseImportXLSXDialog}
                />
              </IconButton>
            </Box>
          </Paper>
        </DialogTitle>
        <DialogContent sx={{ width: "1200px" }}>
          <Grid
            container
            justifyContent={"center"}
            sx={{
              borderBottom: "1px solid #c8c8c8",
              padding: "24px 0",
              width: "100%",
            }}
          >
            <span>You could download template here</span>
            <Button onClick={handleExportXLSX}>Download</Button>
          </Grid>
          <Grid container justifyContent={"center"} mt={2}>
            <span>
              You need to upload correct .xlsx file to import schedules
            </span>
          </Grid>
          <Grid container justifyContent={"center"} padding={3}>
            <label htmlFor="upload-xlsx">
              <input
                style={{ display: "none" }}
                id="upload-xlsx"
                name="upload-xlsx"
                type="file"
                onChange={handleFile}
              />
              <Button color="success" variant="contained" component="span">
                Upload
              </Button>
            </label>
          </Grid>
          <Grid container justifyContent={"center"}>
            <Typography
              variant="subtitle1"
              color={excelFileError !== null ? "red" : ""}
            >
              {excelFileError !== null ? excelFileError : selectedFile?.name}
            </Typography>
          </Grid>
          <Grid>{xlsxDataTableContent}</Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseImportXLSXDialog}>Cancel</Button>
          <Button
            variant="contained"
            color="primary"
            disabled={excelData == null}
            onClick={handleImportXlSXData}
          >
            Save
          </Button>
        </DialogActions>
        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </Dialog>
    </>
  );
};

export default ImportScheduleFromXLSXDialog;
