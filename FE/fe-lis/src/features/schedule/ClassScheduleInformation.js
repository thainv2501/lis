import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import {
  Dialog,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Skeleton,
  Tab,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import Backdrop from "@mui/material/Backdrop";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import { Box } from "@mui/system";
import { DatePicker } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import dayjs from "dayjs";
import { enqueueSnackbar } from "notistack";
import React, { useEffect, useMemo, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Title from "../../components/Title";
import ClassTrainersDialog from "../class/ClassTrainersDialog";
import { useGetClassWithIdQuery } from "../class/classApiSlice";
import ConstructiveQuestionsTable from "../constructiveQuestions/ConstructiveQuestionsTable";
import {
  useAddNewCQMutation,
  useUpdateCQMutation,
} from "../constructiveQuestions/constructiveQuestionApiSlice";
import LessonsDialog from "../lesson/LessonsDialog";
import LessonsTable from "../lesson/LessonsTable";
import MaterialsTable from "../material/MaterialsTable";
import {
  useCreateNewScheduleMutation,
  useGetScheduleByIdQuery,
  useUpdateScheduleMutation,
} from "./scheduleApiSlice";
import useAuth from "../../hooks/useAuth";
import { useAddNewMaterialsMutation } from "../material/materialApiSlice";

const ClassScheduleInformation = () => {
  const { userLogin } = useAuth();
  const navigate = useNavigate();
  const { classId, scheduleId } = useParams();
  const slots = [1, 2, 3, 4, 5, 6, 7];
  const [tabValue, setTabValue] = useState("1");
  const [openAddTrainersDialog, setOpenAddTrainersDialog] = useState(false);
  const [openLessonsDialog, setOpenLessonDialog] = useState(false);
  const [editMode, setEditMode] = useState(false);

  const [openBackDrop, setOpenBackDrop] = useState(false);
  const handleCloseBackDrop = () => {
    setOpenBackDrop(false);
  };
  const handleOpenBackDrop = () => {
    setOpenBackDrop(true);
  };

  const {
    data: scheduleData,
    isSuccess: getScheduleByIdIsSuccess,
    isLoading: getScheduleByIdIsLoading,
    isError: getScheduleByIdIsError,
    error: getScheduleByIdError,
    refetch,
  } = useGetScheduleByIdQuery(scheduleId);

  if (getScheduleByIdIsError && scheduleId !== undefined) {
    navigate("/404");
  }

  const scheduleDefault = {
    slotInDay: 1,
    content: "",
    lessons: [],
    createdBy: userLogin?._id,
    done: false,
  };

  const [scheduleObj, setScheduleObj] = useState(
    scheduleData || scheduleDefault
  );

  const handleScheduleObjChange = (key, value) => {
    setScheduleObj((prevValues) => {
      const updatedValues = { ...prevValues };
      const keys = key.split(".");
      let nestedValue = updatedValues;
      for (let i = 0; i < keys.length; i++) {
        const currentKey = keys[i];
        if (i === keys.length - 1) {
          nestedValue[currentKey] = value;
        } else {
          nestedValue[currentKey] = { ...nestedValue[currentKey] };
          nestedValue = nestedValue[currentKey];
        }
      }
      return updatedValues;
    });
  };

  useEffect(() => {
    if (scheduleData) {
      setScheduleObj(scheduleData);
    }
  }, [scheduleData]);


  const canAdd = useMemo(
    () => scheduleObj?.lessons.length !== 0,
    [scheduleObj?.lessons]
  );

  const [
    createNewSchedule,
    {
      data: createdScheduleRes,
      isSuccess: createNewScheduleIsSuccess,
      isError: createNewScheduleIsError,
      isLoading: createNewScheduleIsLoading,
      error: createNewScheduleError,
    },
  ] = useCreateNewScheduleMutation();

  const handleCreateNewSchedule = async () => {
    try {
      await createNewSchedule(scheduleObj);
    } catch (error) {
      enqueueSnackbar("Error while create new schedule", { variant: "error" });
    }
  };

  useEffect(() => {
    if (createNewScheduleIsSuccess) {
      enqueueSnackbar("Created new schedule", { variant: "success" });
      handleCloseBackDrop();
      navigate(`/dash-board/class-schedules/${classId}`);
    }
  }, [createNewScheduleIsSuccess]);
  useEffect(() => {
    if (createNewScheduleIsError) {
      enqueueSnackbar(
        "Create schedule : " + createNewScheduleError?.data?.message,
        { variant: "error" }
      );
      handleCloseBackDrop();
    }
  }, [createNewScheduleIsError]);
  useEffect(() => {
    if (createNewScheduleIsLoading) {
      handleOpenBackDrop();
    }
  }, [createNewScheduleIsLoading]);

  const {
    data: classData,
    isLoading: getClassDataIsLoading,
    isSuccess: getClassDataIsSuccess,
    isError: getClassDataIsError,
    error: getClassDataError,
  } = useGetClassWithIdQuery(classId);

  const [
    addNewCQ,
    {
      data: createdCQRes,
      isSuccess: addNewCQIsSuccess,
      isError: addNewCQIsError,
      isLoading: addNewCQIsLoading,
      error: addNewCQError,
    },
  ] = useAddNewCQMutation();

  useEffect(() => {
    if (addNewCQIsSuccess) {
      const { createdCQ } = createdCQRes;
      enqueueSnackbar("New Constructive Question Created ", {
        variant: "success",
      });
      handleAddCQToSchedule(createdCQ?._id);
    }
  }, [addNewCQIsSuccess]);

  useEffect(() => {
    if (addNewCQIsError) {
      enqueueSnackbar(
        "Constructive Question Created Fail : " + addNewCQError?.data?.message,
        { variant: "error" }
      );
    }
  }, [addNewCQIsError]);

  useEffect(() => {
    if (!scheduleData) {
      handleScheduleObjChange("trainer", classData?.trainer);
      handleScheduleObjChange("inClass", classData);
      handleScheduleObjChange("day", classData?.semester?.from);
    }
  }, [classData]);

  const [
    updateSchedule,
    {
      isSuccess: updateScheduleIsSuccess,
      isLoading: updateScheduleIsLoading,
      isError: updateScheduleIsError,
      error: updateScheduleError,
    },
  ] = useUpdateScheduleMutation();

  const handleUpdateGeneralInformation = async () => {
    try {
      updateSchedule({
        inClass: classId,
        scheduleId: scheduleId,
        day: scheduleObj?.day,
        slotInDay: scheduleObj?.slotInDay,
        content: scheduleObj?.content,
        trainer: scheduleObj?.trainer,
        done: scheduleObj?.done,
      });
    } catch (error) {
      enqueueSnackbar("Error with handleUpdate Function !", {
        variant: "error",
      });
    }
  };

  const handleAddCQToSchedule = async (CQId) => {
    try {
      updateSchedule({
        scheduleId: scheduleId,
        constructiveQuestion: CQId,
      });
    } catch (error) {
      enqueueSnackbar("Error with handleUpdate Function !", {
        variant: "error",
      });
    }
  };

  const handleAddNewMaterialToSchedule = async (materialId) => {
    try {
      updateSchedule({
        scheduleId: scheduleId,
        material: materialId,
      });
    } catch (error) {
      enqueueSnackbar("Error with handleUpdate Function !", {
        variant: "error",
      });
    }
  };

  useEffect(() => {
    if (updateScheduleIsLoading) {
      handleOpenBackDrop();
    }
  }, [updateScheduleIsLoading]);
  useEffect(() => {
    if (updateScheduleIsSuccess) {
      enqueueSnackbar("Update Schedule Successful !", { variant: "success" });
      refetch();
      setEditMode(false);
      handleCloseBackDrop();
    }
  }, [updateScheduleIsSuccess]);
  useEffect(() => {
    if (updateScheduleIsError) {
      enqueueSnackbar(
        "Update Schedule Error : " + updateScheduleError?.data?.message,
        {
          variant: "error",
        }
      );
      handleCloseBackDrop();
    }
  }, [updateScheduleIsError]);

  const [
    updateCQ,
    {
      isSuccess: updateCQIsSuccess,
      isError: updateCQIsError,
      isLoading: updateCQIsLoading,
      error: updateCQError,
    },
  ] = useUpdateCQMutation();

  useEffect(() => {
    if (updateCQIsSuccess) {
      enqueueSnackbar("Constructive Question Updated ", {
        variant: "success",
      });
      refetch();
    }
  }, [updateCQIsSuccess]);

  useEffect(() => {
    if (updateCQIsError) {
      enqueueSnackbar(
        "Constructive Question Updated Fail : " + addNewCQError?.data?.message,
        { variant: "error" }
      );
    }
  }, [updateCQIsError]);

  const [
    addNewMaterial,
    {
      data: createdMaterialRes,
      isSuccess: addNewMaterialIsSuccess,
      isLoading: addNewMaterialIsLoading,
      isError: addNewMaterialIsError,
      error: addNewMaterialError,
    },
  ] = useAddNewMaterialsMutation();

  useEffect(() => {
    if (addNewMaterialIsSuccess) {
      try {
        const { createdMaterial } = createdMaterialRes;
        handleAddNewMaterialToSchedule(createdMaterial?._id);
      } catch (error) {
        enqueueSnackbar("Add material to lesson fail ! try again !", {
          variant: "error",
        });
      }
    }
  }, [addNewMaterialIsSuccess]);

  const handleChangeTab = (event, newValue) => {
    setTabValue(newValue);
  };

  const handleOpenAddTrainersDialog = () => {
    setOpenAddTrainersDialog(true);
  };
  const handleCloseAddTrainersDialog = () => {
    setOpenAddTrainersDialog(false);
  };

  const handleOpenAddLessonsDialog = () => {
    setOpenLessonDialog(true);
  };
  const handleCloseAddLessonsDialog = () => {
    setOpenLessonDialog(false);
  };

  const handleSlotTime = (slot) => {
    let slotStartAtFloat = 7.5 + (slot - 1) * 1.5;
    if (slot >= 4) {
      slotStartAtFloat = 12.5 + (slot - 4) * 1.5;
    }
    const hours = Math.floor(slotStartAtFloat);
    const minutes = Math.floor((slotStartAtFloat - hours) * 60);
    const slotStartAtTime = dayjs().hour(hours).minute(minutes).format("HH:mm");
    const slotEndAtTime = dayjs()
      .hour(hours + 1)
      .minute(minutes + 30)
      .format("HH:mm");

    return `${slotStartAtTime} - ${slotEndAtTime}`;
  };

  let content;

  if (getClassDataIsError) {
    enqueueSnackbar("Get inClass error : " + getClassDataError?.data?.message, {
      variant: "error",
    });
    navigate("/dash-board/inClass-list");
  }

  if (getClassDataIsLoading)
    content = (
      <Box sx={{ width: "100%" }}>
        <Skeleton />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
        <Skeleton animation="wave" />
        <Skeleton animation={false} />
      </Box>
    );

  if (getClassDataIsSuccess) {
    content = (
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Paper sx={{ p: 2 }}>
          <Grid item xs={12}>
            <Title>
              {`${classData?.semester?.code} - ${classData?.semester?.name} -
                ${classData?.className} - ${classData?.subject?.subjectCode} -
                ${classData?.subject?.subjectName} Schedule Information`}
            </Title>
          </Grid>
          <TabContext value={tabValue}>
            <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
              <TabList
                onChange={handleChangeTab}
                aria-label="lab API tabs example"
              >
                <Tab label="General Information" value="1" />
                <Tab label="Lessons" value="2" />
                <Tab label="Materials" value="3" />
                <Tab label="Constructive Questions" value="4" />
              </TabList>
            </Box>
            {/* General information tab */}
            <TabPanel value="1">
              <Grid container spacing={3}>
                <Grid item xs={12} sm={12} md={4} lg={3} mb={2}>
                  <DatePicker
                    label="Date"
                    value={dayjs(scheduleObj?.day)}
                    minDate={dayjs(classData?.semester?.from)}
                    maxDate={dayjs(classData?.semester?.to)}
                    onChange={(newValue) =>
                      handleScheduleObjChange(
                        "day",
                        dayjs(newValue).format("YYYY-MM-DD")
                      )
                    }
                    disabled={!editMode && scheduleData}
                  />
                </Grid>

                <Grid item xs={12} sm={12} md={3} mb={2}>
                  <FormControl fullWidth>
                    <InputLabel id="slot" required>
                      Slot
                    </InputLabel>
                    <Select
                      id="slot-select"
                      label="Slot"
                      value={scheduleObj?.slotInDay}
                      onChange={(event) =>
                        handleScheduleObjChange("slotInDay", event.target.value)
                      }
                      disabled={!editMode && scheduleData}
                      MenuProps={{
                        PaperProps: {
                          style: {
                            maxHeight: "200px",
                            width: 250,
                          },
                        },
                      }}
                    >
                      {slots?.map((slot, index) => (
                        <MenuItem key={index} value={slot}>
                          {slot}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>

                <Grid container item xs={12} sm={12} md={6} spacing={3}>
                  <Grid item>
                    <TextField
                      id="time"
                      name="time"
                      label="Time"
                      value={handleSlotTime(scheduleObj?.slotInDay)}
                      fullWidth
                      disabled
                    />
                  </Grid>
                  {scheduleId && (
                    <Grid item>
                      <FormControl fullWidth>
                        <InputLabel id="status" required>
                          Status
                        </InputLabel>
                        <Select
                          id="status"
                          label="Status"
                          value={scheduleObj?.done ? "Done" : "Not yet"}
                          MenuProps={{
                            PaperProps: {
                              style: {
                                maxHeight: "200px",
                                width: 250,
                              },
                            },
                          }}
                          onChange={(event) =>
                            handleScheduleObjChange(
                              "done",
                              event.target.value === "Done"
                            )
                          }
                          disabled={!editMode && scheduleData}
                        >
                          {["Done", "Not yet"]?.map((status, index) => (
                            <MenuItem key={index} value={status}>
                              {status}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Grid>
                  )}
                </Grid>

                <Grid item xs={12}>
                  <Paper sx={{ p: 2 }}>
                    <Grid display={"flex"} justifyContent={"space-between"}>
                      <Title>Trainer</Title>
                      <Grid>
                        <Button
                          onClick={() =>
                            handleScheduleObjChange(
                              "trainer",
                              classData?.trainer
                            )
                          }
                          disabled={!editMode && scheduleData}
                        >
                          Reset
                        </Button>
                        <Button
                          onClick={handleOpenAddTrainersDialog}
                          disabled={!editMode && scheduleData}
                        >
                          Change
                        </Button>
                      </Grid>
                    </Grid>
                    <Grid container spacing={3}>
                      <Grid item sm={12} md={6} lg={6}>
                        <TextField
                          disabled
                          id="full-name"
                          name="Full Name"
                          label="Full Name"
                          value={
                            scheduleObj?.trainer?.fullName
                              ? scheduleObj?.trainer?.fullName
                              : ""
                          }
                          fullWidth
                        />
                      </Grid>
                      <Grid item sm={12} md={6} lg={6}>
                        <TextField
                          disabled
                          id="phone-number"
                          name="Phone Number"
                          label="Phone Number"
                          value={
                            scheduleObj?.trainer?.phoneNumber
                              ? scheduleObj?.trainer?.phoneNumber
                              : ""
                          }
                          fullWidth
                        />
                      </Grid>
                      <Grid item sm={12} md={6} lg={6}>
                        <TextField
                          disabled
                          id="email"
                          name="Email"
                          label="Email"
                          value={
                            scheduleObj?.trainer?.email
                              ? scheduleObj?.trainer?.email
                              : ""
                          }
                          fullWidth
                        />
                      </Grid>
                    </Grid>
                  </Paper>

                  <Dialog
                    fullWidth
                    maxWidth={"md"}
                    open={openAddTrainersDialog}
                    onClose={handleCloseAddTrainersDialog}
                  >
                    <ClassTrainersDialog
                      subjectId={classData?.subject?._id}
                      selectedTrainer={scheduleObj?.trainer}
                      setTrainer={null}
                      handleScheduleObjChange={handleScheduleObjChange}
                      handleCloseAddTrainersDialog={
                        handleCloseAddTrainersDialog
                      }
                    />
                  </Dialog>
                </Grid>

                <Grid item xs={12} sm={12} md={12} mb={2}>
                  <TextField
                    id="content"
                    name="content"
                    label="Content"
                    value={scheduleObj?.content}
                    multiline
                    rows={6}
                    fullWidth
                    onChange={(event) =>
                      handleScheduleObjChange("content", event.target.value)
                    }
                    disabled={!editMode && scheduleData}
                  />
                </Grid>

                <Grid item display={"flex"} gap={3}>
                  {scheduleId ? (
                    <>
                      <Button
                        variant="contained"
                        onClick={() => setEditMode(true)}
                        disabled={editMode && scheduleData}
                      >
                        Edit
                      </Button>
                      {editMode && (
                        <>
                          <Button
                            variant="contained"
                            color="success"
                            onClick={handleUpdateGeneralInformation}
                          >
                            Save
                          </Button>
                          <Button
                            variant="contained"
                            onClick={() => {
                              setScheduleObj(scheduleData);
                              setEditMode(false);
                            }}
                          >
                            Reset
                          </Button>
                        </>
                      )}
                    </>
                  ) : (
                    <>
                      <Button
                        variant="contained"
                        color="success"
                        onClick={handleCreateNewSchedule}
                        disabled={!canAdd}
                      >
                        Add
                      </Button>
                      {canAdd ? null : (
                        <Typography color={"red"}>
                          Select lesson is required
                        </Typography>
                      )}
                    </>
                  )}
                </Grid>
              </Grid>
            </TabPanel>
            {/* Lesson Tab tab */}
            <TabPanel value="2" disabled={false}>
              <LessonsTable
                lessons={scheduleObj?.lessons}
                handleOpenAddLessonsDialog={handleOpenAddLessonsDialog}
                handleCloseAddLessonsDialog={handleCloseAddLessonsDialog}
                handleScheduleObjChange={handleScheduleObjChange}
                updateSchedule={updateSchedule}
              />
            </TabPanel>
            {/* Materials tab */}
            <TabPanel value="3" disabled={false}>
              <MaterialsTable
                scheduleId={scheduleId}
                materials={scheduleObj?.materials}
                lessons={scheduleObj?.lessons}
                addNewMaterial={addNewMaterial}
                addNewMaterialIsSuccess={addNewMaterialIsSuccess}
                addNewMaterialIsError={addNewMaterialIsError}
                addNewMaterialIsLoading={addNewMaterialIsLoading}
                addNewMaterialError={addNewMaterialError}
                refetch={refetch}
              />
            </TabPanel>
            <TabPanel value="4" disabled={false}>
              <ConstructiveQuestionsTable
                updateScheduleIsSuccess={updateScheduleIsSuccess}
                scheduleId={scheduleId}
                addNewCQ={addNewCQ}
                updateCQ={updateCQ}
                lessons={scheduleObj?.lessons}
                constructiveQuestions={scheduleObj?.constructiveQuestions}
                refetch={refetch}
              />
            </TabPanel>
          </TabContext>
        </Paper>

        <LessonsDialog
          openLessonsDialog={openLessonsDialog}
          handleCloseAddLessonsDialog={handleCloseAddLessonsDialog}
          lessons={classData?.subject?.lessons}
          scheduleLessons={scheduleObj?.lessons}
          handleScheduleObjChange={handleScheduleObjChange}
          updateSchedule={updateSchedule}
        />

        <Backdrop
          sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={openBackDrop}
          onClick={handleCloseBackDrop}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      </LocalizationProvider>
    );
  }
  return content;
};

export default ClassScheduleInformation;
