import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import PeopleIcon from "@mui/icons-material/People";
import RecentActorsIcon from "@mui/icons-material/RecentActors";
import {
  Autocomplete,
  Avatar,
  Box,
  Card,
  CardContent,
  CardHeader,
  CircularProgress,
  Stack,
  SvgIcon,
  TextField,
} from "@mui/material";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { enqueueSnackbar } from "notistack";
import { React, useEffect, useState } from "react";
import { Link, Outlet } from "react-router-dom";

import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { useGetMasterDataWithTypeQuery } from "../masterData/masterDataApiSlice";
import { useGetDashboardBySemesterIdMutation } from "./dashboardApiSlice";

export default function DasboardInformation() {
  const [semesters, setSemesters] = useState([]);
  const [currentSemester, setCurrentSemester] = useState();
  const [semesterId, setSemesterId] = useState();

  const {
    data: masterDataSemester,
    isSuccess: masterDataIsSuccess,
    isLoading: masterDataIsLoading,
    isError: masterDataIsError,
    error: masterDataError,
  } = useGetMasterDataWithTypeQuery("Semester", {
    refetchOnFocus: true,
    refetchOnMountOrArgChange: true,
  });

  useEffect(() => {
    if (masterDataIsSuccess) {
      const { masterData, currentSemester } = masterDataSemester;
      setSemesters(masterData);
      setCurrentSemester(currentSemester);
      setSemesterId(currentSemester?._id);
    }
  }, [masterDataIsSuccess, currentSemester]);

  const [
    getDashboardBySemesterId,
    {
      data: response,
      isError: getDashboardBySemesterIdIsError,
      isLoading: getDashboardBySemesterIdIsLoading,
      isSuccess: getDashboardBySemesterIdIsSuccess,
      error: getDashboardBySemesterIdError,
    },
  ] = useGetDashboardBySemesterIdMutation();

  const handleGetDashboardBySemesterId = async () => {
    try {
      await getDashboardBySemesterId({ semesterId });
    } catch (error) {
      enqueueSnackbar(`Error with get function !`, { variant: "error" });
    }
  };

  useEffect(() => {
    handleGetDashboardBySemesterId();
  }, [semesterId]);

  const handleSemesterChange = (event, newValue) => {
    if (!newValue) {
      enqueueSnackbar("Please Choose Semester!", {
        variant: "warning",
      });
    } else {
      setSemesterId(newValue?._id);
    }
  };

  let usersDataContent = <CircularProgress />;
  let usersDataCompareContent = <CircularProgress />;
  let subjectsDataContent = <CircularProgress />;
  let subjectsDataCompareContent = <CircularProgress />;
  let classesDataContent = <CircularProgress />;
  let supportQuestionChartContent = (
    <CardContent
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100%",
      }}
    >
      <CircularProgress />
    </CardContent>
  );
  let meetingsChartContent = (
    <CardContent
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100%",
      }}
    >
      <CircularProgress />
    </CardContent>
  );
  if (getDashboardBySemesterIdIsSuccess && response) {
    usersDataContent = response.totalUsersInSemester;
    usersDataCompareContent =
      (response.totalUsersCreatedInSemester / response.totalUsersInSemester) *
      100;
    //
    subjectsDataContent = response.totalSubjectsInSemester;
    subjectsDataCompareContent =
      (response.totalSubjectsCreatedInSemester /
        response.totalSubjectsInSemester) *
      100;
    ///
    classesDataContent = response.totalClassesInSemester;
    supportQuestionChartContent = (
      <CardContent>
        <BarChart
          width={500}
          height={320}
          data={response.questionsAndMeetingsCountsByMonth}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar
            dataKey="supportQuestions"
            name="Class Support Questions"
            stackId="a"
            fill="#8884d8"
          />
          <Bar
            dataKey="subjectQuestions"
            name="Subject Questions"
            stackId="b"
            fill="#82ca9d"
          />
        </BarChart>
      </CardContent>
    );
    meetingsChartContent = (
      <CardContent>
        <BarChart
          width={500}
          height={320}
          data={response.questionsAndMeetingsCountsByMonth}
          margin={{
            top: 20,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="meetings" name="Meetings" stackId="a" fill="#8884d8" />
        </BarChart>
      </CardContent>
    );
  }

  let content;

  content = (
    <Container maxWidth="lg" sx={{ mb: 1 }} style={{ marginTop: "10px" }}>
      <Box sx={{ maxWidth: "1400px", margin: "0 auto" }}>
        <Box sx={{ maxWidth: "20vw" }}>
          <Autocomplete
            sx={{ width: "250px", mb: 2 }}
            id="size-small-outlined"
            size="small"
            onChange={handleSemesterChange}
            value={
              semesters.find((semester) => semester?._id === semesterId) || null
            }
            options={semesters}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => <TextField {...params} label="SEMESTER" />}
          />
        </Box>
      </Box>
      <Outlet />
      <Box sx={{ flexGrow: 1 }}>
        <Grid
          container
          spacing={{ xs: 2, md: 3 }}
          columns={{ xs: 4, sm: 8, md: 12 }}
        >
          <Grid item xs={2} sm={4} md={4}>
            <Link to={"/dash-board/users-list"}>
              <Card style={{ height: "180px" }}>
                <CardContent>
                  <Stack
                    alignItems="flex-start"
                    direction="row"
                    justifyContent="space-between"
                    spacing={3}
                  >
                    <Stack spacing={1}>
                      <Typography color="text.secondary" variant="overline">
                        Total Users
                      </Typography>

                      <Typography variant="h4">
                        <PeopleIcon fontSize="lg" sx={{ mr: 1 }} />
                        {usersDataContent}
                      </Typography>
                    </Stack>
                    <Avatar
                      sx={{
                        backgroundColor: "success.main",
                        height: 56,
                        width: 56,
                      }}
                    >
                      <SvgIcon>
                        <PeopleIcon />
                      </SvgIcon>
                    </Avatar>
                  </Stack>
                  <Stack
                    alignItems="center"
                    direction="row"
                    spacing={2}
                    sx={{ mt: 2 }}
                  >
                    <Stack alignItems="center" direction="row" spacing={0.5}>
                      <SvgIcon color={"success"} fontSize="small">
                        <ArrowUpwardIcon />
                      </SvgIcon>
                      <Typography color={"success.main"} variant="body2">
                        {usersDataCompareContent} %
                      </Typography>
                    </Stack>
                    <Typography color="text.secondary" variant="caption">
                      On this semester
                    </Typography>
                  </Stack>
                </CardContent>
              </Card>
            </Link>
          </Grid>
          <Grid item xs={2} sm={4} md={4}>
            <Link to={"/dash-board/subjects-list"}>
              <Card style={{ height: "180px" }}>
                <CardContent>
                  <Stack
                    alignItems="flex-start"
                    direction="row"
                    justifyContent="space-between"
                    spacing={3}
                  >
                    <Stack spacing={1}>
                      <Typography color="text.secondary" variant="overline">
                        Total Subject
                      </Typography>
                      <Typography variant="h4">
                        <LibraryBooksIcon fontSize="lg" sx={{ mr: 1 }} />
                        {subjectsDataContent}
                      </Typography>
                    </Stack>
                    <Avatar
                      sx={{
                        backgroundColor: "secondary.main",
                        height: 56,
                        width: 56,
                      }}
                    >
                      <SvgIcon>
                        <LibraryBooksIcon />
                      </SvgIcon>
                    </Avatar>
                  </Stack>
                  <Stack
                    alignItems="center"
                    direction="row"
                    spacing={2}
                    sx={{ mt: 2 }}
                  >
                    <Stack alignItems="center" direction="row" spacing={0.5}>
                      <SvgIcon color={"success"} fontSize="small">
                        <ArrowUpwardIcon />
                      </SvgIcon>
                      <Typography color={"success.main"} variant="body2">
                        {subjectsDataCompareContent} %
                      </Typography>
                    </Stack>
                    <Typography color="text.secondary" variant="caption">
                      On this semester
                    </Typography>
                  </Stack>
                </CardContent>
              </Card>
            </Link>
          </Grid>
          <Grid item xs={2} sm={4} md={4}>
            <Link to={"/dash-board/classes-list"}>
              <Card style={{ height: "180px" }}>
                <CardContent>
                  <Stack
                    alignItems="flex-start"
                    direction="row"
                    justifyContent="space-between"
                    spacing={3}
                  >
                    <Stack spacing={1}>
                      <Typography color="text.secondary" variant="overline">
                        Total Classes
                      </Typography>
                      <Typography variant="h4">
                        <RecentActorsIcon fontSize="lg" sx={{ mr: 1 }} />
                        {classesDataContent}
                      </Typography>
                    </Stack>
                    <Avatar
                      sx={{
                        backgroundColor: "primary.main",
                        height: 56,
                        width: 56,
                      }}
                    >
                      <SvgIcon>
                        <RecentActorsIcon />
                      </SvgIcon>
                    </Avatar>
                  </Stack>
                  <Stack
                    alignItems="center"
                    direction="row"
                    spacing={2}
                    sx={{ mt: 2 }}
                  ></Stack>
                </CardContent>
              </Card>
            </Link>
          </Grid>

          <Grid item sm={12} md={6}>
            <Card style={{ height: "400px" }}>
              <CardHeader title="Support Questions" />
              {supportQuestionChartContent}
            </Card>
          </Grid>
          <Grid item sm={12} md={6}>
            <Card style={{ height: "400px" }}>
              <CardHeader title="Meetings" />
              {meetingsChartContent}
            </Card>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );

  return content;
}
