import { createEntityAdapter } from "@reduxjs/toolkit";
import { apiSlice } from "../../app/api/apiSlice";

const dashboardAdapter = createEntityAdapter({});

const initialState = dashboardAdapter.getInitialState();

export const dashboardApiSlice = apiSlice.injectEndpoints({
  endpoints: (builder) => ({
    getDashboardBySemesterId: builder.mutation({
      query: (initialGroup) => ({
        url: "/dashboard",
        method: "POST",
        body: {
          ...initialGroup,
        },
      }),
    }),
  }),
});

export const { useGetDashboardBySemesterIdMutation } = dashboardApiSlice;
