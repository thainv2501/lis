import DataObjectIcon from "@mui/icons-material/DataObject";
import FormatListNumberedRtlIcon from "@mui/icons-material/FormatListNumberedRtl";
import RecentActorsIcon from "@mui/icons-material/RecentActors";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import LibraryBooksIcon from "@mui/icons-material/LibraryBooks";
import SignalCellularAltIcon from "@mui/icons-material/SignalCellularAlt";
import HomeIcon from "@mui/icons-material/Home";
import * as React from "react";
import { Link } from "react-router-dom";

const mainListItems = [
  {
    path: "/dash-board/dashboard-infor",
    name: "Dashboard ",
    icon: <SignalCellularAltIcon />,
  },
  {
    path: "/dash-board/users-list",
    name: "User Management",
    icon: <FormatListNumberedRtlIcon />,
  },
  {
    path: "/dash-board/classes-list",
    name: "Class Management",
    icon: <RecentActorsIcon />,
  },
  {
    path: "/dash-board/subjects-list",
    name: "Subject Management",
    icon: <LibraryBooksIcon />,
  },
];

const secondaryListItems = [
  {
    path: "/dash-board/master-data-list",
    name: "Master Data",
    icon: <DataObjectIcon />,
  },
];
const thirstListItems = [
  {
    path: "/common/courses-list",
    name: "LIS",
    icon: <HomeIcon />,
  },
];

export const mainListItemsDisplay = (
  <React.Fragment>
    {mainListItems.map((item, index) => (
      <Link to={item.path} key={index}>
        <ListItemButton>
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.name} />
        </ListItemButton>
      </Link>
    ))}
  </React.Fragment>
);

export const secondaryListItemsDisplay = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      Other Data
    </ListSubheader>
    {secondaryListItems.map((item, index) => (
      <Link to={item.path} key={index}>
        <ListItemButton>
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.name} />
        </ListItemButton>
      </Link>
    ))}
  </React.Fragment>
);
export const thirstListItemsDisplay = (
  <React.Fragment>
    <ListSubheader component="div" inset>
      Other Access
    </ListSubheader>
    {thirstListItems.map((item, index) => (
      <Link to={item.path} key={index}>
        <ListItemButton>
          <ListItemIcon>{item.icon}</ListItemIcon>
          <ListItemText primary={item.name} />
        </ListItemButton>
      </Link>
    ))}
  </React.Fragment>
);
