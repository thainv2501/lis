import * as React from 'react';

import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { Button } from "@mui/material";
import { Typography } from "@mui/material";
import appCurvyLines from "../../images/appCurvyLines.png";
import dev1 from "../../images/dev1.jpg";
import dev2 from "../../images/dev2.jpg";

const item = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  px: 5,
};

const number = {
  fontSize: 24,
  fontFamily: 'default',
  color: 'secondary.main',
  fontWeight: 'medium',
};

const image = {
    width: '180px',
    height: '180px',
  };

function DevTeam() {
  return (
    <Box
      component="section"
      sx={{ display: "flex", overflow: "hidden",backgroundColor:"#FFF5F8" }}

    >
      <Container
        sx={{
          mt: 10,
          mb: 15,
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Box
          component="img"
          src={appCurvyLines}
          alt="curvy lines"
          sx={{
            pointerEvents: 'none',
            position: 'absolute',
            top: -180,
            opacity: 0.7,
          }}
        />
        
        <Typography variant="h4" marked="center" component="h2" sx={{ mb: 14 , fontFamily:"monospace"}}>
          Dev Team
        </Typography>
        <div>
          <Grid container spacing={5}>
            <Grid item xs={12} md={3}>
              <Box sx={item}>
                <Box
                  component="img"
                  src={dev2}
                  alt="suitcase"
                  sx={image}
                />
                <Typography variant="h5" align="center" style={{fontFamily:"monospace"}}>
                  Thái VG
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={3}>
              <Box sx={item}>
                <Box
                  component="img"
                  src={dev1}
                  alt="suitcase"
                  sx={image}
                />
                <Typography variant="h5" align="center" style={{fontFamily:"monospace"}}>
                  Cương TV
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={3}>
              <Box sx={item}>
                <Box
                  component="img"
                  src={dev2}
                  alt="graph"
                  sx={image}
                />
                <Typography variant="h5" align="center" style={{fontFamily:"monospace"}}>
                  Anh NT
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={3}>
              <Box sx={item}>
                <Box
                  component="img"
                  src={dev1}
                  alt="clock"
                  sx={image}
                />
                <Typography variant="h5" align="center" style={{fontFamily:"monospace"}}>
                  Hai DH
                </Typography>
              </Box>
            </Grid>
          </Grid>
        </div>
      </Container>
    </Box>
  );
}

export default DevTeam;