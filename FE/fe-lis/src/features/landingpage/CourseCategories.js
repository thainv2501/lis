import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import nhatban from "../../images/nhatban.jpg";
import hanquoc from "../../images/hanquoc.jpg";
import triethoc from "../../images/triet-hoc.jpg";
import java from "../../images/java.jpg";
import ccong from "../../images/ngon-ngu-c-.jpg";
import tutuonghcm from "../../images/tutuonghcm.jpg";
import quanlydata from "../../images/quanlydata.jpg";
import kynang from "../../images/kynang.jpg";
import Csharp1 from "../../images/Csharp1.png";
const ImageBackdrop = styled("div")(({ theme }) => ({
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  background: "#000",
  opacity: 0.5,
  transition: theme.transitions.create("opacity"),
}));

const ImageIconButton = styled(ButtonBase)(({ theme }) => ({
  position: "relative",
  display: "block",
  padding: 0,
  borderRadius: 0,
  height: "40vh",
  [theme.breakpoints.down("md")]: {
    width: "100% !important",
    height: 100,
  },
  "&:hover": {
    zIndex: 1,
  },
  "&:hover .imageBackdrop": {
    opacity: 0.15,
  },
  "&:hover .imageMarked": {
    opacity: 0,
  },
  "&:hover .imageTitle": {
    border: "4px solid currentColor",
  },
  "& .imageTitle": {
    position: "relative",
    padding: `${theme.spacing(2)} ${theme.spacing(4)} 14px`,
  },
  "& .imageMarked": {
    height: 3,
    width: 18,
    background: theme.palette.common.white,
    position: "absolute",
    bottom: -2,
    left: "calc(50% - 9px)",
    transition: theme.transitions.create("opacity"),
  },
}));

const images = [
  {
    url: nhatban,
    title: "tiếng nhật",
    width: "40%",
  },
  {
    url: java,
    title: "Java",
    width: "20%",
  },
  {
    url: hanquoc,
    title: "Tiếng hàn",
    width: "40%",
  },
  {
    url: triethoc,
    title: "Triết học",
    width: "38%",
  },
  {
    url: ccong,
    title: "C++",
    width: "38%",
  },
  {
    url: Csharp1,
    title: "C#",
    width: "24%",
  },
  {
    url: tutuonghcm,
    title: "Tư tưởng HCM",
    width: "40%",
  },
  {
    url: quanlydata,
    title: "Quản lý data",
    width: "20%",
  },
  {
    url: kynang,
    title: "Kỹ năng làm việc",
    width: "40%",
  },
];

export default function CourseCategories() {
  return (
    <Container component="section" sx={{ mt: 8, mb: 4 }}>
      <Typography variant="h4" marked="center" align="center" component="h2" fontFamily={"monospace"}>
        Some study course in this web
      </Typography>
      <Box sx={{ mt: 8, display: "flex", flexWrap: "wrap" }}>
        {images.map((image) => (
          <ImageIconButton
            key={image.title}
            style={{
              width: image.width,
            }}
          >
            <Box
              sx={{
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                backgroundSize: "cover",
                backgroundPosition: "center 40%",
                backgroundImage: `url(${image.url})`,
              }}
            />
            <ImageBackdrop className="imageBackdrop" />
            <Box
              sx={{
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                color: "common.white",
              }}
            >
              <Typography
                component="h3"
                variant="h6"
                color="inherit"
                className="imageTitle"
              >
                {image.title}
                <div className="imageMarked" />
              </Typography>
            </Box>
          </ImageIconButton>
        ))}
      </Box>
    </Container>
  );
}
