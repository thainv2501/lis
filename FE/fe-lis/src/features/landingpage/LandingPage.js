import BookIcon from "@mui/icons-material/Book";
import LiveHelpIcon from "@mui/icons-material/LiveHelp";
import PeopleIcon from "@mui/icons-material/People";
import { Button, Container, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import * as React from "react";
import { Link, useNavigate } from "react-router-dom";
import LIS1 from "../../images/LIS1.gif";
import LIS2 from "../../images/LIS2.gif";
import LIS3 from "../../images/LIS3.gif";
import herolayoutbg from "../../images/herolayout.jpg";
import logojpg from "../../images/logojpg-removebg.png";
import AppBar from "./AppBar";
import AppFooter from "./AppFooter";
import CourseCategories from "./CourseCategories";
import DevTeam from "./DevTeam";
import ProductHeroLayout from "./ProductHeroLayout";
import ProductSmokingHero from "./ProductSmokingHero";
import TabImage from "./TabImage";
import Toolbar from "./Toolbar";
import useAuth from "../../hooks/useAuth";
import { useSendLogoutMutation } from "../auth/authApiSlice";
const rightLink = {
  fontSize: 16,
  color: "common.white",
  ml: 3,
};

function LandingPage() {
  const navigate = useNavigate();
  const { userLogin } = useAuth();

  const [sendLogout, { isSuccess }] = useSendLogoutMutation();

  React.useEffect(() => {
    if (isSuccess) navigate("/login");
  }, [isSuccess, navigate]);

  const item = {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    px: 5,
  };

  return (
    <div>
      <ProductHeroLayout
        sxBackground={{
          backgroundImage: `url(${herolayoutbg})`,
          backgroundColor: "#7fc7d9",
          backgroundPosition: "center",
        }}
      >
        <div>
          <AppBar
            sx={{
              justifyContent: "space-between",
              backgroundColor: "transparent",
              position: "absolute",
            }}
          >
            <Toolbar
              sx={{
                justifyContent: "space-between",
              }}
            >
              <img
                style={{ height: "94px", width: "166px", marginLeft: "-40px" }}
                src={logojpg}
              />
              <Box sx={{ flex: 1, justifyContent: "flex-start" }}>
                <Typography
                  variant="h6"
                  underline="none"
                  color="#FFF"
                  sx={{ fontSize: 24 }}
                >
                  {"Learning Interation System"}
                </Typography>
              </Box>
              <Box
                sx={{ flex: 1, display: "flex", justifyContent: "flex-end" }}
              >
                {userLogin ? (
                  <>
                    <Button
                      variant="contained"
                      size="Medium"
                      onClick={sendLogout}
                    >
                      Log out
                    </Button>
                  </>
                ) : (
                  <>
                    <Link
                      variant="h6"
                      underline="none"
                      to={"/login"}
                      sx={rightLink}
                      style={{ marginRight: "20px" }}
                    >
                      <Button variant="contained" size="Medium">
                        sign in
                      </Button>
                    </Link>
                    <Link
                      variant="h6"
                      underline="none"
                      to={"/register"}
                      sx={{ ...rightLink }}
                    >
                      <Button variant="contained" size="Medium">
                        sign up
                      </Button>
                    </Link>
                  </>
                )}
              </Box>
            </Toolbar>
          </AppBar>
        </div>
        <div align="center">
          <Typography
            color="inherit"
            align="center"
            variant="h2"
            marked="center"
          >
            {userLogin ? `YOUR ROLE IS "Guest" ONLY ?` : "STUDY WITH US"}
          </Typography>
          <Typography
            fontFamily={"monospace"}
            color="inherit"
            align="center"
            variant="h5"
            sx={{ mb: 4, mt: { xs: 4, sm: 10 } }}
          >
            {userLogin
              ? `you are maybe only role "Guest" ! So contact with Admin to more actions`
              : `There are no secrets to success. It is the result of preparation,
            hard work, and learning from failure.`}
          </Typography>
          {!userLogin ? (
            <Link to={"/register"}>
              <Button
                variant="contained"
                size="large"
                component="a"
                sx={{ minWidth: 200, height: "50px" }}
              >
                Register
              </Button>
            </Link>
          ) : null}

          <Typography variant="body2" color="inherit" sx={{ mt: 2 }}>
            Discover the web
          </Typography>
        </div>
      </ProductHeroLayout>
      <Box alignContent={"center"}>
        <Typography
          color="inherit"
          align="center"
          variant="h2"
          marked="center"
          marginTop={"30px"}
          fontSize={"40px"}
          fontFamily={"monospace"}
        >
          What LIS can give you ?
        </Typography>
        <Container
          sx={{
            mt: 2,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <div>
            <Typography
              color="inherit"
              align="center"
              variant="h2"
              marked="center"
              marginTop={"30px"}
              fontSize={"20px"}
              fontFamily={"monospace"}
              width={"800px"}
            >
              This system helps learners stick to the curriculum, provide
              questions and debates combined with presentations through online
              learning and thereby the quality of learning will become higher
              and more effective.
            </Typography>
          </div>
        </Container>
      </Box>
      <Box
        component="section"
        sx={{
          display: "flex",
          overflow: "hidden",
        }}
      >
        <Container sx={{ mt: 5, mb: 3, display: "flex", position: "relative" }}>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Grid item xs={5}>
              <Box
                style={{
                  height: "400px",
                  backgroundColor: "transparent",
                }}
              >
                <div>
                  <h2 style={{ fontFamily: "monospace", fontSize: "56px" }}>
                    Learning interaction{" "}
                  </h2>
                </div>
                <div>
                  <Typography fontFamily={"monospace"}>
                    Teaching and learning online on smart devices and the
                    Internet brings a lot of convenience to students and
                    teachers{" "}
                  </Typography>
                </div>
                {/* <div style={{ marginTop: "30px" }}>
                  <Button variant="contained">Contained</Button>
                </div> */}
              </Box>
            </Grid>
            <Grid item xs={7}>
              <img
                style={{ height: "495px", width: "100%", borderRadius: "20px" }}
                src={LIS1}
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box
        component="section"
        sx={{
          display: "flex",
          overflow: "hidden",
        }}
      >
        <Container
          sx={{
            mt: 2,
            mb: 3,
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Grid container spacing={{ xs: 2, md: 3 }}>
            <Grid item xs={7}>
              <img
                style={{ height: "400px", width: "100%", borderRadius: "20px" }}
                src={LIS2}
              />
            </Grid>
            <Grid item xs={5}>
              <Box style={{ height: "400px", backgroundColor: "transparent" }}>
                <div>
                  <h2 style={{ fontFamily: "monospace", fontSize: "56px" }}>
                    Learning everywhere{" "}
                  </h2>
                </div>
                <div>
                  <Typography
                    style={{ fontFamily: "monospace", fontSize: "16px" }}
                  >
                    With no need to go to school or center, you can freely study
                    at home, at a coffee shop or anywhere that makes you feel
                    most receptive to knowledge. Because if you have an ideal
                    study space, you will be in a better mood, easily receptive
                    to lessons, thereby improving the quality of learning.
                  </Typography>
                </div>
                {/* <div style={{ marginTop: "30px" }}>
                  <Button variant="contained">Contained</Button>
                </div> */}
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box
        component="section"
        sx={{
          display: "flex",
          overflow: "hidden",
        }}
      >
        <Container sx={{ mt: 2, mb: 3, display: "flex", position: "relative" }}>
          <Grid
            container
            spacing={{ xs: 2, md: 3 }}
            columns={{ xs: 4, sm: 8, md: 12 }}
          >
            <Grid item xs={5}>
              <Box style={{ height: "400px", backgroundColor: "transparent" }}>
                <div>
                  <h2 style={{ fontFamily: "monospace", fontSize: "56px" }}>
                    Active learning{" "}
                  </h2>
                </div>
                <div>
                  <Typography
                    style={{ fontFamily: "monospace", fontSize: "16px" }}
                  >
                    Active learning helps students understand that creativity
                    grows with effort and hard work. The ability to solve
                    complex problems becomes the most important skill needed for
                    future jobs.
                  </Typography>
                </div>
                {/* <div style={{ marginTop: "30px" }}>
                  <Button variant="contained">Contained</Button>
                </div> */}
              </Box>
            </Grid>
            <Grid item xs={7}>
              <img
                style={{ height: "400px", width: "100%", borderRadius: "20px" }}
                src={LIS3}
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
      <Box alignContent={"center"}>
        <Typography
          color="inherit"
          align="center"
          variant="h2"
          marked="center"
          marginTop={"30px"}
          fontSize={"40px"}
          fontFamily={"monospace"}
        >
          Popular Courses.
        </Typography>
        <Container
          sx={{
            mt: 2,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        ></Container>
      </Box>
      <TabImage />
      <Typography
        color="inherit"
        align="center"
        variant="h2"
        marked="center"
        marginTop={"30px"}
        fontSize={"40px"}
        fontFamily={"monospace"}
      >
        Another service
      </Typography>
      <Box component="section" sx={{ display: "flex", overflow: "hidden" }}>
        <Container
          sx={{ mt: 5, mb: 10, display: "flex", position: "relative" }}
        >
          <Grid container spacing={5}>
            <Grid item xs={12} md={4}>
              <Box sx={item}>
                <BookIcon sx={{ fontSize: 55 }} />
                <Typography
                  variant="h6"
                  sx={{ my: 5, fontFamily: "monospace", fontWeight: "bold" }}
                >
                  Join the schedule
                </Typography>
                <Typography variant="h5" fontFamily={"monospace"}>
                  {
                    "From the latest trendy boutique hotel to the iconic palace with XXL pool"
                  }

                  {
                    ", go for a mini-vacation just a few subway stops away from your home."
                  }
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={4}>
              <Box sx={item}>
                <PeopleIcon sx={{ fontSize: 55 }} />
                <Typography
                  variant="h6"
                  sx={{ my: 5, fontWeight: "bold" }}
                  fontFamily={"monospace"}
                >
                  Learning online with trainer
                </Typography>
                <Typography variant="h5" fontFamily={"monospace"}>
                  {
                    "Privatize a pool, take a Japanese bath or wake up in 900m2 of garden… "
                  }

                  {"your Sundays will not be alike."}
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={4}>
              <Box sx={item}>
                <LiveHelpIcon sx={{ fontSize: 55 }} />
                <Typography
                  variant="h6"
                  sx={{ my: 5, fontWeight: "bold" }}
                  fontFamily={"monospace"}
                >
                  Make question instantly
                </Typography>
                <Typography variant="h5" fontFamily={"monospace"}>
                  {
                    "By registering, you will access specially negotiated rates "
                  }
                  {"that you will not find anywhere else."}
                </Typography>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
      <CourseCategories />
      {/* <ProductHowItWorks /> */}

      <DevTeam />
      <ProductSmokingHero />
      <AppFooter />
    </div>
  );
}

export default LandingPage;
