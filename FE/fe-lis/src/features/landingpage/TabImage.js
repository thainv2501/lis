import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { Button, Grid, TextField, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Tab from "@mui/material/Tab";
import * as React from "react";
import javaicon from "../../images/javaicon.png";
import Csharpicon from "../../images/Csharpicon.png";
import cplusicon from "../../images/cplusicon.png";
import pythonicon from "../../images/pythonicon.png";
import jpicon from "../../images/jpicon.png";
import mongoicon from "../../images/mongoicon.png";
import javacontent from "../../images/javacontent.jpg";
import Csharpcontent from "../../images/Csharpcontent.png";
import cpluscontent from "../../images/cpluscontent.png";
import pythoncontent from "../../images/pythoncontent.jpg";
import jpltcontent from "../../images/jpltcontent.jpg";
import datacontent from "../../images/datacontent.jpg";
import productCTAImageDots from "../../images/productCTAImageDots.png";
import { Link } from "react-router-dom";
export default function TabImage() {
  const [value, setValue] = React.useState("1");

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box
      sx={{
        width: "100%",
        typography: "body1",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1 }}>
          <TabList onChange={handleChange}>
            <Tab
              value="1"
              icon={
                <img src={javaicon} style={{ width: "50px", height: "50px" }} />
              }
            />
            <Tab
              value="2"
              icon={
                <img
                  src={Csharpicon}
                  style={{ width: "50px", height: "50px" }}
                />
              }
            />
            <Tab
              value="3"
              icon={
                <img
                  src={cplusicon}
                  style={{ width: "50px", height: "50px" }}
                />
              }
            />
            <Tab
              value="4"
              icon={
                <img
                  src={pythonicon}
                  style={{ width: "50px", height: "50px" }}
                />
              }
            />
            <Tab
              value="5"
              icon={
                <img src={jpicon} style={{ width: "50px", height: "50px" }} />
              }
            />
            <Tab
              value="6"
              icon={
                <img
                  src={mongoicon}
                  style={{ width: "50px", height: "50px" }}
                />
              }
            />
          </TabList>
        </Box>
        <TabPanel value="1">
          <Container
            component="section"
            sx={{ mt: 10, display: "flex", width: "3960px" }}
          >
            <Grid container>
              <Grid item xs={12} md={6} sx={{ zIndex: 1 }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    bgcolor: "#FFC071",
                    py: 8,
                    px: 3,
                    borderRadius:"20px"
                  }}
                >
                  <Box component="form" sx={{ maxWidth: 400 }}>
                    <Typography
                      variant="h2"
                      component="h2"
                      gutterBottom
                      fontFamily={"monospace"}
                    >
                      JAVA
                    </Typography>
                    <Typography variant="h5" fontFamily={"monospace"}>
                      object-oriented programming language designed to have as
                      few implementation dependencies as possible. It is a
                      general-purpose programming language that allows
                      application developers to write once, run anywhere
                    </Typography>
                    <Link to={"/login"}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      sx={{
                        width: "100%",
                        marginTop: "10px",
                        borderRadius: "20px",
                      }}
                    >
                      Get Started
                    </Button>
                    </Link>
                  </Box>
                </Box>
              </Grid>
              <Grid
                item
                xs={12}
                md={6}
                sx={{
                  display: { md: "block", xs: "none" },
                  position: "relative",
                }}
              >
                <Box
                  sx={{
                    position: "absolute",
                    top: -67,
                    left: -67,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    background: `url(${productCTAImageDots})`,
                  }}
                />
                <Box
                  component="img"
                  src={javacontent}
                  alt="call to action"
                  sx={{
                    position: "absolute",
                    top: -28,
                    left: -28,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    Width: 600,
                    borderRadius:"20px"
                  }}
                />
              </Grid>
            </Grid>
          </Container>
        </TabPanel>
        <TabPanel value="2">
          <Container
            component="section"
            sx={{ mt: 10, display: "flex", width: "3960px" }}
          >
            <Grid container>
              <Grid item xs={12} md={6} sx={{ zIndex: 1 }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    bgcolor: "#FFC071",
                    py: 8,
                    px: 3,
                    borderRadius:"20px"
                  }}
                >
                  <Box component="form" sx={{ maxWidth: 400 }}>
                    <Typography
                      variant="h2"
                      component="h2"
                      gutterBottom
                      fontFamily={"monospace"}
                    >
                      C#
                    </Typography>
                    <Typography variant="h5" fontFamily={"monospace"}>
                      is a powerful, general-purpose object-oriented programming
                      language developed by Microsoft, C# was the beginning of
                      their .NET plan. The name of the language includes the
                      pound character according to Microsoft
                    </Typography>
                    <Link to={"/login"}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      sx={{
                        width: "100%",
                        marginTop: "10px",
                        borderRadius: "20px",
                      }}
                    >
                      Get Started
                    </Button>
                    </Link>
                  </Box>
                </Box>
              </Grid>
              <Grid
                item
                xs={12}
                md={6}
                sx={{
                  display: { md: "block", xs: "none" },
                  position: "relative",
                }}
              >
                <Box
                  sx={{
                    position: "absolute",
                    top: -67,
                    left: -67,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    background: `url(${productCTAImageDots})`,
                  }}
                />
                <Box
                  component="img"
                  src={Csharpcontent}
                  alt="call to action"
                                    sx={{
                    position: "absolute",
                    top: -28,
                    left: -28,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    maxWidth: 600,
                    borderRadius:"20px"
                  }}
                />
              </Grid>
            </Grid>
          </Container>
        </TabPanel>{" "}
        <TabPanel value="3">
          <Container
            component="section"
            sx={{ mt: 10, display: "flex", width: "3960px" }}
          >
            <Grid container>
              <Grid item xs={12} md={6} sx={{ zIndex: 1 }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    bgcolor: "#FFC071",
                    py: 8,
                    px: 3,
                    borderRadius:"20px"
                  }}
                >
                  <Box component="form" sx={{ maxWidth: 400 }}>
                    <Typography
                      variant="h2"
                      component="h2"
                      gutterBottom
                      fontFamily={"monospace"}
                    >
                      C++
                    </Typography>
                    <Typography variant="h5" fontFamily={"monospace"}>
                      is a kind of middle-level programming language. It is a
                      general-purpose programming language created by Bjarne
                      Stroustrup as an extension of the C programming language,
                      or "C with Classes"
                    </Typography>
                    <Link to={"/login"}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      sx={{
                        width: "100%",
                        marginTop: "10px",
                        borderRadius: "20px",
                      }}
                    >
                      Get Started
                    </Button>
                    </Link>
                  </Box>
                </Box>
              </Grid>
              <Grid
                item
                xs={12}
                md={6}
                sx={{
                  display: { md: "block", xs: "none" },
                  position: "relative",
                }}
              >
                <Box
                  sx={{
                    position: "absolute",
                    top: -67,
                    left: -67,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    background: `url(${productCTAImageDots})`,
                  }}
                />
                <Box
                  component="img"
                  src={cpluscontent}
                  alt="call to action"
                                    sx={{
                    position: "absolute",
                    top: -28,
                    left: -28,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    maxWidth: 600,
                    borderRadius:"20px"
                  }}
                />
              </Grid>
            </Grid>
          </Container>
        </TabPanel>{" "}
        <TabPanel value="4">
          <Container
            component="section"
            sx={{ mt: 10, display: "flex", width: "3960px" }}
          >
            <Grid container>
              <Grid item xs={12} md={6} sx={{ zIndex: 1 }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    bgcolor: "#FFC071",
                    py: 8,
                    px: 3,
                    borderRadius:"20px"
                  }}
                >
                  <Box component="form" sx={{ maxWidth: 400 }}>
                    <Typography
                      variant="h2"
                      component="h2"
                      gutterBottom
                      fontFamily={"monospace"}
                    >
                      Python
                    </Typography>
                    <Typography variant="h5" fontFamily={"monospace"}>
                      is a high-level programming language for general-purpose
                      programming, created by Guido van Rossum and first
                      released in 1991. Python is designed with the strong
                      advantage of being easy to read, learn, and remember.
                    </Typography>
                    <Link to={"/login"}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      sx={{
                        width: "100%",
                        marginTop: "10px",
                        borderRadius: "20px",
                      }}
                    >
                      Get Started
                    </Button>
                    </Link>
                  </Box>
                </Box>
              </Grid>
              <Grid
                item
                xs={12}
                md={6}
                sx={{
                  display: { md: "block", xs: "none" },
                  position: "relative",
                }}
              >
                <Box
                  sx={{
                    position: "absolute",
                    top: -67,
                    left: -67,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    background: `url(${productCTAImageDots})`,
                  }}
                />
                <Box
                  component="img"
                  src={pythoncontent}
                  alt="call to action"
                                    sx={{
                    position: "absolute",
                    top: -28,
                    left: -28,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    maxWidth: 600,
                    borderRadius:"20px"
                  }}
                />
              </Grid>
            </Grid>
          </Container>
        </TabPanel>{" "}
        <TabPanel value="5">
          <Container
            component="section"
            sx={{ mt: 10, display: "flex", width: "3960px" }}
          >
            <Grid container>
              <Grid item xs={12} md={6} sx={{ zIndex: 1 }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    bgcolor: "#FFC071",
                    py: 8,
                    px: 3,
                    borderRadius:"20px"
                  }}
                >
                  <Box component="form" sx={{ maxWidth: 400 }}>
                    <Typography
                      variant="h2"
                      component="h2"
                      gutterBottom
                      fontFamily={"monospace"}
                    >
                      Japanese
                    </Typography>
                    <Typography variant="h5" fontFamily={"monospace"}>
                      Learning Japanese will help you appreciate your own
                      culture as well as your own language as it improves your
                      communication and cognitive skills
                    </Typography>
                    <Link to={"/login"}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      sx={{
                        width: "100%",
                        marginTop: "10px",
                        borderRadius: "20px",
                      }}
                    >
                      Get Started
                    </Button>
                    </Link>
                  </Box>
                </Box>
              </Grid>
              <Grid
                item
                xs={12}
                md={6}
                sx={{
                  display: { md: "block", xs: "none" },
                  position: "relative",
                }}
              >
                <Box
                  sx={{
                    position: "absolute",
                    top: -67,
                    left: -67,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    background: `url(${productCTAImageDots})`,
                  }}
                />
                <Box
                  component="img"
                  src={jpltcontent}
                  alt="call to action"
                                    sx={{
                    position: "absolute",
                    top: -28,
                    left: -28,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    maxWidth: 600,
                    borderRadius:"20px"
                  }}
                />
              </Grid>
            </Grid>
          </Container>
        </TabPanel>{" "}
        <TabPanel value="6">
          <Container
            component="section"
            sx={{ mt: 10, display: "flex", width: "3960px" }}
          >
            <Grid container>
              <Grid item xs={12} md={6} sx={{ zIndex: 1 }}>
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "center",
                    bgcolor: "#FFC071",
                    py: 8,
                    px: 3,
                    borderRadius:"20px"
                  }}
                >
                  <Box component="form" sx={{ maxWidth: 400 }}>
                    <Typography
                      variant="h2"
                      component="h2"
                      gutterBottom
                      fontFamily={"monospace"}
                    >
                      Data Management
                    </Typography>
                    <Typography variant="h5" fontFamily={"monospace"}>
                      Data management is the process of ingesting, storing,
                      organizing and maintaining the data created and collected
                      by an organization. Effective data management is a crucial
                      piece of deploying the IT systems that run business
                      applications and provide analytical information
                    </Typography>
                    <Link to={"/login"}>
                    <Button
                      type="submit"
                      color="primary"
                      variant="contained"
                      sx={{
                        width: "100%",
                        marginTop: "10px",
                        borderRadius: "20px",
                      }}
                    >
                      Get Started
                    </Button>
                    </Link>

                  </Box>
                </Box>
              </Grid>
              <Grid
                item
                xs={12}
                md={6}
                sx={{
                  display: { md: "block", xs: "none" },
                  position: "relative",
                }}
              >
                <Box
                  sx={{
                    position: "absolute",
                    top: -67,
                    left: -67,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    background: `url(${productCTAImageDots})`,
                  }}
                />
                <Box
                  component="img"
                  src={datacontent}
                  alt="call to action"
                  sx={{
                    position: "absolute",
                    top: -28,
                    left: -28,
                    right: 0,
                    bottom: 0,
                    width: "100%",
                    maxWidth: 600,
                    borderRadius:"20px"
                  }}
                />
              </Grid>
            </Grid>
          </Container>
        </TabPanel>
      </TabContext>
    </Box>
  );
}
