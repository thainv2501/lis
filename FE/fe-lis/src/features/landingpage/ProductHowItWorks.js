import * as React from 'react';
import { Link } from "react-router-dom";
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { Button } from "@mui/material";
import { Typography } from "@mui/material";
import appCurvyLines from "../../images/appCurvyLines.png";
import join from "../../images/join.png";
import studywithtrainer from "../../images/studywithtrainer.jpg";
import makequestion from "../../images/makequestion.jpg";

const item = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  px: 5,
};

const number = {
  fontSize: 24,
  fontFamily: 'default',
  color: 'secondary.main',
  fontWeight: 'medium',
};

const image = {
    height: '180px',
  };

function ProductHowItWorks() {
  return (
    <Box
      component="section"
      sx={{ display: "flex", overflow: "hidden",backgroundColor:"#FFF5F8" }}

    >
      <Container
        sx={{
          mt: 10,
          mb: 15,
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Box
          component="img"
          src={appCurvyLines}
          alt="curvy lines"
          sx={{
            pointerEvents: 'none',
            position: 'absolute',
            top: -180,
            opacity: 0.7,
          }}
        />
        
        <Typography variant="h4" marked="center" component="h2" sx={{ mb: 14 }}>
          How it works
        </Typography>
        <div>
          <Grid container spacing={5}>
            <Grid item xs={12} md={4}>
              <Box sx={item}>
                <Box sx={number}>1.</Box>
                <Box
                  component="img"
                  src={join}
                  alt="suitcase"
                  sx={image}
                />
                <Typography variant="h5" align="center">
                  Join Class
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={4}>
              <Box sx={item}>
                <Box sx={number}>2.</Box>
                <Box
                  component="img"
                  src={studywithtrainer}
                  alt="graph"
                  sx={image}
                />
                <Typography variant="h5" align="center">
                  study with trainer
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={12} md={4}>
              <Box sx={item}>
                <Box sx={number}>3.</Box>
                <Box
                  component="img"
                  src={makequestion}
                  alt="clock"
                  sx={image}
                />
                <Typography variant="h5" align="center">
                  make convertastion
                </Typography>
              </Box>
            </Grid>
          </Grid>
        </div>
        <Link to={"/login"}>
        <Button
          variant="contained"
          size="large"
          component="a"
          sx={{ minWidth: 200, height: "50px" , marginTop:"50px"}}
          
        >
          Join now
          </Button>
        </Link>

        
      </Container>
    </Box>
  );
}

export default ProductHowItWorks;