import { Route, Routes } from "react-router-dom";
import "./App.css";
import CommonLayout from "./components/CommonLayout";
import Layout from "./components/Layout";
import { Page404 } from "./components/Page404";
import VerifyEmail from "./components/VerifyEmail";
import { ROLES } from "./config/roles";
import ForgotPassword from "./features/auth/ForgotPassword";
import Login from "./features/auth/Login";
import PersistLogin from "./features/auth/PersistLogin";
import Prefetch from "./features/auth/Prefetch";
import { Register } from "./features/auth/Register";
import RegisterSuccessfulNotification from "./features/auth/RegisterSuccessfulNotification";
import RequireAuth from "./features/auth/RequireAuth";
import ResetPassword from "./features/auth/ResetPassword";
import ClassInformation from "./features/class/ClassInformation";
import ClassList from "./features/class/ClassList";
import UserClassDetails from "./features/class/UserClassDetails";
import QuestionDetails from "./features/constructiveQuestions/UserConstructiveQuestionDetails";
import CoursesList from "./features/courses/CoursesList";
import TrainingSlot from "./features/courses/TrainingSlot";
import DasboardInformation from "./features/dashboard/DasboardInformation";
import Dashboard from "./features/dashboard/Dashboard";
import LandingPage from "./features/landingpage/LandingPage";
import LessonDetail from "./features/lesson/LessonDetail";
import LessonList from "./features/lesson/LessonList";
import AddNewMasterData from "./features/masterData/AddNewMasterData";
import MasterDataDetails from "./features/masterData/MasterDataDetails";
import MasterDataList from "./features/masterData/MasterDataList";
import JoinMeeting from "./features/meeting/JoinMeeting";
import SupportMeetingDetails from "./features/meeting/SupportMeetingDetails";
import SupportMeetings from "./features/meeting/SupportMeetings";
import ClassScheduleInformation from "./features/schedule/ClassScheduleInformation";
import ClassSchedules from "./features/schedule/ClassSchedules";
import ClassSupport from "./features/slots/ClassSupport";
import SlotDetails from "./features/slots/SlotDetails";
import SupportQuestionDetail from "./features/slots/SupportQuestionDetails";
import AddNewSubject from "./features/subjects/AddNewSubject";
import SubjectDetails from "./features/subjects/SubjectDetails";
import SubjectQuestion from "./features/subjects/SubjectQuestion";
import SubjectsList from "./features/subjects/SubjectsList";
import UserDetails from "./features/users/UserDetails";
import UsersList from "./features/users/UsersList";
function App() {
  return (
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route path="/*" element={<Page404 />} />
        <Route index element={<LandingPage />} />

        <Route path="register" element={<Register />} />
        <Route path="verify" element={<VerifyEmail />} />
        <Route path="/forgot-password" element={<ForgotPassword />} />
        <Route path="/reset-password" element={<ResetPassword />} />
        <Route path="login" element={<Login />} />
        <Route
          path="registerSuccessful"
          element={<RegisterSuccessfulNotification />}
        />
        <Route element={<PersistLogin />}>
          <Route element={<Prefetch />}>
            <Route path="waitingPage" element={<LandingPage />} />
            {/* protect route */}
            <Route
              element={<RequireAuth allowedRoles={[...Object.values(ROLES)]} />}
            >
              <Route
                path="meeting/:meetingId"
                element={<JoinMeeting />}
              ></Route>

              <Route path="common" element={<CommonLayout />}>
                <Route path="user-details/:userId" element={<UserDetails />} />
                <Route path="class-support" element={<ClassSupport />} />
                <Route path="subject-question" element={<SubjectQuestion />} />
                <Route path="courses-list" element={<CoursesList />} />
                <Route path="courses-list">
                  <Route index element={<CoursesList />} />
                  <Route
                    path="class-details"
                    element={<UserClassDetails />}
                  />
                </Route>
                <Route path="courses-list">
                  <Route index element={<CoursesList />} />
                  <Route
                    path="slot-details/:scheduleId"
                    element={<SlotDetails />}
                  />
                </Route>
                <Route path="support-meetings">
                  <Route index element={<SupportMeetings />} />

                  <Route
                    path="support-meeting-details/:meetingId"
                    element={<SupportMeetingDetails />}
                  />
                </Route>
                <Route path="training-slot" element={<TrainingSlot />} />
                <Route path="question-details" element={<QuestionDetails />} />
                <Route
                  path="support-question-details/:supportQuestionId"
                  element={<SupportQuestionDetail />}
                />
              </Route>
              {/* end user route */}
              <Route element={<RequireAuth allowedRoles={[ROLES.Admin]} />}>
                <Route path="dash-board" element={<Dashboard />}>
                  <Route path="lessons-list/:subjectId">
                    <Route index element={<LessonList />} />
                  </Route>
                  <Route
                    path="lesson-details/:lessonId"
                    element={<LessonDetail />}
                  />
                  <Route path="users-list">
                    <Route index element={<UsersList />} />
                    <Route
                      path="user-details/:userId"
                      element={<UserDetails />}
                    />
                  </Route>
                  {/* end user list route */}
                  <Route path="classes-list">
                    <Route index element={<ClassList />} />
                    <Route
                      path="add-new-class"
                      element={<ClassInformation />}
                    />
                    <Route path=":classId" element={<ClassInformation />} />
                  </Route>
                  {/* end class list route */}
                  <Route path="class-schedules/:classId">
                    <Route index element={<ClassSchedules />} />
                    <Route
                      path="new-schedule"
                      element={<ClassScheduleInformation />}
                    />
                    <Route
                      path=":scheduleId"
                      element={<ClassScheduleInformation />}
                    />
                  </Route>
                  {/* end class list route */}
                  <Route path="master-data-list">
                    <Route index element={<MasterDataList />} />
                    <Route
                      path="master-data-details"
                      element={<MasterDataDetails />}
                    />
                  </Route>
                  <Route
                    path="add-new-master-data"
                    element={<AddNewMasterData />}
                  />

                  <Route path="subjects-list">
                    <Route index element={<SubjectsList />} />
                    <Route
                      path="subject-details/:subjectId"
                      element={<SubjectDetails />}
                    />
                  </Route>

                  <Route path="add-new-subject" element={<AddNewSubject />} />
                  <Route
                    path="dashboard-infor"
                    element={<DasboardInformation />}
                  />
                </Route>
              </Route>
              {/* end admin route */}
            </Route>
          </Route>
          {/* end prefetch */}
        </Route>
        {/* end PersistLogin */}
      </Route>
    </Routes>
  );
}

export default App;
