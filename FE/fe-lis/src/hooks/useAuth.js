import { useSelector } from 'react-redux';
import { selectCurrentToken } from '../features/auth/authSlice';
import jwtDecode from 'jwt-decode';

const useAuth = () => {
  const token = useSelector(selectCurrentToken);
  let isManager = false;
  let isAdmin = false;
  let isTrainer = false;
  let isTrainee = false;
  let status = 'Guest';

  if (token) {
    const decoded = jwtDecode(token);
    const { userLogin } = decoded.data;

    isManager = userLogin.roles.includes('Manager');
    isAdmin = userLogin.roles.includes('Admin');

    isTrainer = userLogin.roles.includes('Trainer');
    isTrainee = userLogin.roles.includes('Trainee');

    if (isManager) status = 'Manager';
    if (isAdmin) status = 'Admin';

    return {
      userLogin,
      status,
      isManager,
      isAdmin,
      isTrainer,
      isTrainee,
    };
  }

  return {
    email: '',
    roles: [],
    isManager,
    isAdmin,
    isTrainer,
    isTrainee,
    status,
  };
};
export default useAuth;
