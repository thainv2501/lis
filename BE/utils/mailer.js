const nodeMailer = require("nodemailer");
const mailConfig = require("../config/mailConfig");
require("dotenv/config");

const transport = nodeMailer.createTransport({
  host: mailConfig.HOST,
  port: mailConfig.PORT,
  secure: false,
  auth: {
    user: mailConfig.USERNAME,
    pass: mailConfig.PASSWORD,
  },
});

exports.connectMail = () => {
  transport.verify((err, success) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Gmail build successful");
    }
  });
};

exports.sendMail = (to, subject, htmlContent) => {
  const options = {
    from: mailConfig.FROM_ADDRESS,
    to: to,
    subject: subject,
    html: htmlContent,
  };
  return transport.sendMail(options);
};
