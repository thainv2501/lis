require("dotenv").config();
require("express-async-errors");
const express = require("express");
const app = express();
const path = require("path");
const errorHandler = require("./middleware/errorHandler");
const cors = require("cors");
const corsOptions = require("./config/corsOptions");
const cookieParser = require("cookie-parser");
const connectDB = require("./config/dbConn");
const mailer = require("./utils/mailer");
const mongoose = require("mongoose");
const PORT = process.env.PORT || 3500;

console.log(process.env.NODE_ENV);

connectDB();
mailer.connectMail();

app.use(cors(corsOptions));

app.use(express.json());

app.use(cookieParser());

app.use("/", express.static(path.join(__dirname, "public")));

app.use("/auth", require("./routes/authRoutes"));
app.use("/dashboard", require("./routes/dashboardRoutes"));
app.use("/users", require("./routes/userRoutes"));
app.use("/master-data", require("./routes/masterDataRoutes"));
app.use("/subjects", require("./routes/subjectRoutes"));
app.use("/lessons", require("./routes/lessonRoutes"));
app.use("/class", require("./routes/classRoutes"));
app.use("/groups", require("./routes/groupRoutes"));
app.use("/materials", require("./routes/materialRoutes"));
app.use("/meetings", require("./routes/meetingRoutes"));
app.use("/schedules", require("./routes/scheduleRoutes"));
app.use("/comments", require("./routes/commentRoutes"));
app.use("/votes", require("./routes/voteRoutes"));
app.use("/presentations", require("./routes/presentationRoutes"));
app.use("/grade", require("./routes/gradeRoutes"));
app.use("/replies", require("./routes/replyRoutes"));
app.use("/supportQuestions", require("./routes/supportQuestionRoutes"));
app.use(
  "/constructiveQuestions",
  require("./routes/constructiveQuestionRoutes")
);
app.all("*", (req, res) => {
  res.status(404);
  if (req.accepts("html")) {
    res.sendFile(path.join(__dirname, "views", "404.html"));
  } else if (req.accepts("json")) {
    res.json({ message: "404 Not Found" });
  } else {
    res.type("txt").send("404 Not Found");
  }
});

app.use(errorHandler);

mongoose.connection.once("open", () => {
  console.log("Connected to MongoDB");
  app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
});

mongoose.connection.on("error", (err) => {
  console.log(err);
});

module.exports = app;
