const mongoose = require("mongoose");

const constructiveQuestionSchema = new mongoose.Schema(
  {
    schedule: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Schedule",
    },
    title: { type: String, required: true },
    content: { type: String, required: true },
    from: { type: Date },
    to: { type: Date },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    displaySetting: {
      displayStudentsName: { type: Boolean },
      allowOutsideGroupViewComments: { type: Boolean },
    },
    voteSetting: {
      redCard: {
        trainerVote: {
          star: { type: Number, default: 4 },
          quantity: { type: Number, default: 2 },
        },
        insideVote: {
          star: { type: Number, default: 4 },
          quantity: { type: Number, default: 2 },
        },
        outsideVote: {
          star: { type: Number, default: 4 },
          quantity: { type: Number, default: 2 },
        },
      },
      blueCard: {
        trainerVote: {
          star: { type: Number, default: 3 },
          quantity: { type: Number, default: 2 },
        },
        insideVote: {
          star: { type: Number, default: 3 },
          quantity: { type: Number, default: 2 },
        },
        outsideVote: {
          star: { type: Number, default: 3 },
          quantity: { type: Number, default: 2 },
        },
      },
      greenCard: {
        trainerVote: {
          star: { type: Number, default: 2 },
          quantity: { type: Number, default: 2 },
        },
        insideVote: {
          star: { type: Number, default: 2 },
          quantity: { type: Number, default: 2 },
        },
        outsideVote: {
          star: { type: Number, default: 2 },
          quantity: { type: Number, default: 2 },
        },
      },
      blackCard: {
        trainerVote: {
          star: { type: Number, default: 1 },
          quantity: { type: Number, default: 2 },
        },
        insideVote: {
          star: { type: Number, default: 1 },
          quantity: { type: Number, default: 2 },
        },
        outsideVote: {
          star: { type: Number, default: 1 },
          quantity: { type: Number, default: 2 },
        },
      },
    },
    createdDate: { type: Date, default: Date.now },
    done: { type: Boolean, default: false },
    active: { type: Boolean, default: true },
  },
  { strict: false }
);

module.exports = mongoose.model(
  "ConstructiveQuestion",
  constructiveQuestionSchema
);
