const mongoose = require('mongoose');

const replySchema = new mongoose.Schema(
  {
    content: { type: String, required: true },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User',
    },
    createdDate: { type: Date, default: Date.now },
  },
  { strict: false }
);

module.exports = mongoose.model('Reply', replySchema);
