const mongoose = require("mongoose");

const masterDataSchema = new mongoose.Schema(
  {
    type: { type: String, require: true },
    code: { type: String, require: true },
    name: { type: String, require: true },
    displayOrder: { type: Number, select: true },
    description: { type: String },
    active: { type: Boolean, require: true, default: true },
  },
  { strict: false }
);

module.exports = mongoose.model("MasterData", masterDataSchema);
