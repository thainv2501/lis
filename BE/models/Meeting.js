const mongoose = require("mongoose");

const meetingSchema = new mongoose.Schema(
  {
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    meetingId: { type: String, required: true },
    meetingName: { type: String, required: true },
    meetingType: {
      type: String,
      required: true,
      enum: ["one-to-one", "video-conference", "anyone-can-join"],
    },
    invitedUsers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],
    meetingFrom: {
      type: Date,
      required: true,
    },
    meetingTo: {
      type: Date,
      required: true,
    },
    maxUsers: {
      type: Number,
      required: true,
    },
    class: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Class",
    },
    status: {
      type: Boolean,
      default: true,
    },
    createdAt: { type: Date, default: Date.now },
  },
  { strict: false }
);

module.exports = mongoose.model("Meeting", meetingSchema);
