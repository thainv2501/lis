const mongoose = require("mongoose");

const gradeSchema = new mongoose.Schema(
  {
    presentation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Presentation",
    },

    votedForGroup: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Group",
    },
    votedForUser: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    votedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    hardWorking: {
      type: Number,
      default: 0,
    },
    goodSkill: {
      type: Number,
      default: 0,
    },
    goodInformation: {
      type: Number,
      default: 0,
    },
    teamWorking: {
      type: Number,
      default: 0,
    },
    keepTime: {
      type: Number,
      default: 0,
    },
    meetRequirements: {
      type: Number,
    },
    presentations: {
      type: Number,
    },
  },
  { strict: false }
);

module.exports = mongoose.model("Grade", gradeSchema);
