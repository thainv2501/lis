const mongoose = require("mongoose");

const voteSchema = new mongoose.Schema(
  {
    schedule: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Schedule",
    },
    constructiveQuestion: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ConstructiveQuestion",
    },
    comment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Comment",
    },
    votedBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    type: {
      type: String,
      enum: ["trainer", "inside", "outside"],
    },
    card: {
      type: String,
      enum: ["red", "blue", "green", "black"],
    },
    star: { type: Number, required: true },
  },
  { strict: false }
);

module.exports = mongoose.model("Vote", voteSchema);
