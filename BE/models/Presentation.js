const mongoose = require("mongoose");

const presentationSchema = new mongoose.Schema(
  {
    schedule: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Schedule",
    },
    constructiveQuestion: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "ConstructiveQuestion",
    },
    presentationGroup: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Group",
    },
    reviewGroup: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Group",
    },
    grade: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Grade",
      },
    ],
    createdAt: { type: Date, default: Date.now },
  },
  { strict: false }
);

module.exports = mongoose.model("Presentation", presentationSchema);
