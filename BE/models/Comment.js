const mongoose = require("mongoose");

const commentSchema = new mongoose.Schema(
  {
    schedule: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Schedule",
    },
    constructiveQuestion: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "ConstructiveQuestion",
    },
    content: { type: String, require: true },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    votes: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Vote",
      },
    ],
    reply: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Reply",
      },
    ],
    createdAt: { type: Date, default: Date.now },
  },
  { strict: false }
);

module.exports = mongoose.model("Comment", commentSchema);
