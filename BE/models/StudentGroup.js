const mongoose = require('mongoose');

const studentGroupSchema = new mongoose.Schema(
  {
    isLeader: { type: Boolean, default: false },
    student: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
      },
    group: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Group',
      },
  },
  { strict: false }
);

module.exports = mongoose.model('StudentGroup', studentGroupSchema);
