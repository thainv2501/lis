const mongoose = require("mongoose");

const materialSchema = new mongoose.Schema(
  {
    content: { type: String, required: true },
    sources: [
      {
        name: { type: String },
        url: { type: String, required: true },
        type: { type: String },
      },
    ],
    uploadedBy: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    subject: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Subject",
    },
    createdAt: { type: Date, default: Date.now },
  },
  { strict: false }
);

module.exports = mongoose.model("Material", materialSchema);
