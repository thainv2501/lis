const mongoose = require('mongoose');

const supportQuestionSchema = new mongoose.Schema(
  {
    type: {
      type: String,
      required: true,
      enum: ['subject-question', 'support-question'],
    },
    subject: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Subject',
    },
    schedule: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Schedule',
    },
    title: { type: String, required: true },
    content: { type: String, required: true },
    sources: [
      {
        name: { type: String },
        url: { type: String, required: true },
        type: { type: String },
      },
    ],
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User',
    },
    reply: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: 'Reply',
    },
    createdDate: { type: Date, default: Date.now },
    status: {
      type: String,
      enum: ['no-answer', 'discussing', 'answered'],
      default: 'no-answer',
    },
  },
  { strict: false }
);

module.exports = mongoose.model('SupportQuestion', supportQuestionSchema);
