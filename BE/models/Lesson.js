const mongoose = require("mongoose");

const lessonSchema = new mongoose.Schema(
  {
    subject: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Subject",
    },
    lessonChapter: {
      type: Number,
      required: true,
    },
    lessonNumber: {
      type: Number,
      required: true,
    },
    lessonContent: {
      type: String,
      required: true,
    },
    constructiveQuestions: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "ConstructiveQuestion",
    },
    materials: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Material",
        populate: { path: "uploadedBy", model: "User" },
      },
    ],
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    status: {
      type: Boolean,
      default: true,
    },
  },
  { strict: false }
);

module.exports = mongoose.model("Lesson", lessonSchema);
