const mongoose = require("mongoose");

const subjectSchema = new mongoose.Schema(
  {
    subjectCode: { type: String, required: true },
    subjectName: { type: String, required: true },
    subjectDescription: { type: String },
    createdDate: { type: Date, default: Date.now },
    chapterLimit: { type: Number, default: 1 },
    lessons: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "Lesson",
    },
    trainers: {
      type: [mongoose.Schema.Types.ObjectId],
      required: true,
      ref: "User",
    },
    materials: {
      type: [mongoose.Schema.Types.ObjectId],
      required: true,
      ref: "Material",
    },
    status: { type: Boolean, default: true },
  },
  { strict: false }
);

module.exports = mongoose.model("Subject", subjectSchema);
