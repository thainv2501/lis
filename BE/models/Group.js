const mongoose = require("mongoose");

const groupSchema = new mongoose.Schema(
  {
    schedule: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Schedule",
    },
    groupNumber: { type: Number, required: true },
    members: [
      {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "User",
      },
    ],
    leader: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
  },

  { strict: false }
);

module.exports = mongoose.model("Group", groupSchema);
