const mongoose = require("mongoose");

const classSchema = new mongoose.Schema(
  {
    className: { type: String, required: true },
    subject: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Subject",
    },
    semester: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "MasterData",
    },
    trainer: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    students: {
      type: [mongoose.Schema.Types.ObjectId],
      required: true,
      ref: "User",
    },
    note: { type: String, required: false },
    active: { type: Boolean, default: true },
  },
  { strict: false }
);

module.exports = mongoose.model("Class", classSchema);
