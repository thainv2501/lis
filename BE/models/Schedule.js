const mongoose = require("mongoose");

const scheduleSchema = new mongoose.Schema(
  {
    inClass: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "Class",
    },
    day: {
      type: Date,
      required: true,
    },
    slotInDay: {
      type: Number,
      required: true,
    },
    trainer: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    createdBy: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    content: {
      type: String,
    },
    lessons: {
      type: [Object],
    },
    constructiveQuestions: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "ConstructiveQuestion",
    },
    materials: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: "Material",
    },
    done: { type: Boolean, default: false },
  },
  { strict: false }
);

module.exports = mongoose.model("Schedule", scheduleSchema);
