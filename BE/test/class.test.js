const request = require("supertest");
const app = require("../server"); // Assuming you have an Express app defined in app.js
const Class = require("../models/Class");
const { default: mongoose } = require("mongoose");
const User = require("../models/User");
const Subject = require("../models/Subject");
const bcrypt = require("bcrypt");
const mailer = require("../utils/mailer");

// ...

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("GET /class/:id", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should get class data for an Admin", async () => {
    // Mock Class.findById to return class data
    const mockClassData = {
      _id: "64d4d276b8b81b9b992efedc",
      subject: "subject id",
      semester: "Semester id",
      trainer: "Trainer id",
      students: ["Student 1", "Student 2"],
      // ...other class data properties
    };
    jest.spyOn(Class, "findById").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockClassData),
    });

    const response = await request(app)
      .get(`/class/${mockClassData._id}`)
      .set("Authorization", "Bearer your_token_here")
      .set("userLogin", JSON.stringify({ _id: "user_id", roles: ["Admin"] }));

    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockClassData);
  });

  it("should deny access for unauthorized user with empty roles", async () => {
    // Set mockedRoles to "empty" to simulate empty roles
    request(app)
      .get(`/class/64d4d276b8b81b9b992efedc`)
      .set("Authorization", "Bearer your_token_here")
      .set("mockedRoles", "empty")
      .expect(403)
      .expect((response) => {
        expect(response.body.message).toBe("Unauthorized");
      });
  });

  it("should return 400 if class is not found", async () => {
    jest.clearAllMocks();
    const nonexistent_id = new mongoose.Types.ObjectId(); // Replace with a valid ObjectId

    // Mock Class.findById to return null (class not found)
    jest.spyOn(Class, "findById").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });
    const response = await request(app).get(`/class/${nonexistent_id}`);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No Class Found");
  });

  // Add more test cases as needed
});

describe("GET /class/semester/:id", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get class data for an Admin", async () => {
    // Mock Class.find to return class data
    const mockClassData = [
      {
        _id: "64d5a871776ff9a3bde9b898",
        subject: "subject id",
        semester: "Semester id",
        trainer: "Trainer id",
        students: ["Student 1", "Student 2"],
        // ...other class data properties
      },
      // ...add more mock class data if needed
    ];
    jest.spyOn(Class, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockClassData),
    });

    const response = await request(app).get(`/class/semester/semester_id`); // Use the valid semester ID here

    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockClassData);
  });

  it("should get class data for a Trainer or Trainee", async () => {
    // Mock userLogin middleware
    app.use((req, res, next) => {
      req.userLogin = {
        _id: "user_id",
        roles: ["Trainer"], // Mocking Trainer role for testing
      };
      next();
    });

    // Mock Class.find to return class data
    const mockClassData = [
      {
        _id: "64d5a871776ff9a3bde9b899",
        subject: "subject id",
        semester: "Semester id",
        trainer: "user_id", // Trainer or Trainee's ID
        students: ["Student 1", "Student 2"],
        // ...other class data properties
      },
      // ...add more mock class data if needed
    ];
    jest.spyOn(Class, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockClassData),
    });

    const response = await request(app).get(`/class/semester/semester_id`); // Use the valid semester ID here

    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockClassData);
  });

  it("should return empty response for Guest role or unauthenticated users", async () => {
    const response = await request(app)
      .get(`/class/semester/semester_id`)
      .set("Authorization", "Bearer your_token_here")
      .set("mockedRoles", "undefined"); // Use the valid semester ID here

    expect(response.status).toBe(200);
    expect(response.body).toEqual([]);
  });

});

describe("GET /class", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get class data based on search query", async () => {
    // Mock User.find and Subject.find to return empty arrays
    jest.spyOn(User, "find").mockResolvedValue([]);
    jest.spyOn(Subject, "find").mockResolvedValue([]);

    // Mock Class.find to return class data
    const mockClassData = [
      {
        _id: new mongoose.Types.ObjectId(),
        subject: "subject id",
        semester: "Semester id",
        trainer: "Trainer id",
        students: ["Student 1", "Student 2"],
      },
      {
        _id: new mongoose.Types.ObjectId(),
        subject: "subject id",
        semester: "Semester id",
        trainer: "Trainer id",
        students: ["Student 1", "Student 2"],
      },
      {
        _id: new mongoose.Types.ObjectId(),
        subject: "subject id",
        semester: "Semester id",
        trainer: "Trainer id",
        students: ["Student 1", "Student 2"],
      },
    ];
    jest.spyOn(Class, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockClassData),
    });
    jest.spyOn(Class, "countDocuments").mockReturnValue({
      exec: jest.fn().mockResolvedValue(3),
    });

    const response = await request(app).get("/class").send({
      page: 1,
      semester: "semester_id",
      active: "active",
      keyword: "search_keyword",
    });

    expect(response.status).toBe(200);
    // Check the response body structure here
    expect(response.body).toHaveProperty("classCount");
    expect(response.body).toHaveProperty("classData");
    // Add more assertions for the response body if needed
  });

  it("should return 400 if no class data found", async () => {
    // Mock Class.countDocuments to return 0
    jest.spyOn(Class, "countDocuments").mockReturnValue({
      exec: jest.fn().mockResolvedValue(0),
    });

    const response = await request(app).get("/class").send({
      page: 1,
      semester: "semester_id",
      active: "active",
      keyword: "search_keyword",
    });

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No data found");
  });

  // Add more test cases as needed
});

describe("POST /class", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should create a new class successfully", async () => {
    // Mock Class.findOne and Class.prototype.save
    jest.spyOn(Class, "findOne").mockResolvedValue(null);
    jest.spyOn(Class.prototype, "save").mockResolvedValue({
      _id: new mongoose.Types.ObjectId(),
      className: "Sample Class",
      subject: "subject_id",
      semester: "semester_id",
      trainer: "trainer_id",
      students: ["student_id1", "student_id2"],
      note: "Sample note",
    });

    const requestBody = {
      className: "Sample Class",
      subjectId: "subject_id",
      semesterId: "semester_id",
      trainerId: "trainer_id",
      studentIds: ["student_id1", "student_id2"],
      note: "Sample note",
    };

    const response = await request(app).post("/class").send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Class Created successfully");
    expect(response.body.createdClass).toHaveProperty("_id");
    // Add more assertions for the response body if needed
  });

  it("should return 412 if required fields are missing", async () => {
    // Send a request without required fields
    const requestBody = {
      // Missing required fields
    };

    const response = await request(app).post("/class").send(requestBody);

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required fields");
  });

  it("should return 412 if a class with the same details already exists", async () => {
    // Mock Class.findOne to return an existing class
    jest.spyOn(Class, "findOne").mockResolvedValue({
      _id: new mongoose.Types.ObjectId(),
      className: "Sample Class",
      subject: "subject_id",
      semester: "semester_id",
    });

    const requestBody = {
      className: "Sample Class",
      subjectId: "subject_id",
      semesterId: "semester_id",
      trainerId: "trainer_id",
      studentIds: ["student_id1", "student_id2"],
      note: "Sample note",
    };

    const response = await request(app).post("/class").send(requestBody);

    expect(response.status).toBe(412);
    expect(response.body.message).toBe(
      "A class with the same className, subjectId, and semesterId already exists"
    );
  });

  it("should return 400 if class creation fails", async () => {
    // Mock Class.findOne to return null
    jest.spyOn(Class, "findOne").mockResolvedValue(null);
    // Mock Class.prototype.save to throw an error
    jest
      .spyOn(Class.prototype, "save")
      .mockRejectedValue(new Error("Mock Error with save class function"));

    const requestBody = {
      className: "Sample Class",
      subjectId: "subject_id",
      semesterId: "semester_id",
      trainerId: "trainer_id",
      studentIds: ["student_id1", "student_id2"],
      note: "Sample note",
    };

    const response = await request(app).post("/class").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Class Created fail");
  });

  // Add more test cases as needed
});

describe("PATCH /class", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should update class data successfully", async () => {
    // Mock Class.findById, Class.prototype.updateOne, and Class.findOne
    const existingClassData = {
      _id: new mongoose.Types.ObjectId(),
      className: "Sample Class",
      semester: "semester_id",
      subject: "subject_id",
    };
    jest.spyOn(Class, "findById").mockReturnValue({
      exec: jest.fn().mockResolvedValue(existingClassData),
    });
    jest.spyOn(Class, "updateOne").mockResolvedValue({});
    jest.spyOn(Class, "findOne").mockReturnValue({
      exec: jest.fn().mockResolvedValue(null),
    });
    const requestBody = {
      id: existingClassData._id.toString(),
      className: "Updated Class Name",
      active: true,
      note: "Updated note",
    };

    const response = await request(app).patch("/class").send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      `${existingClassData.className} updated`
    );
  });

  it("should return 400 if id is missing", async () => {
    // Send a request without the id field
    const requestBody = {
      // Missing id field
    };

    const response = await request(app).patch("/class").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Id are required");
  });

  it("should return 400 if data to update is not found", async () => {
    // Mock Class.findById to return null
    jest.spyOn(Class, "findById").mockReturnValue({
      exec: jest.fn().mockResolvedValue(null),
    });
    const requestBody = {
      id: "invalid_id",
      className: "Updated Class Name",
      active: true,
      note: "Updated note",
    };

    const response = await request(app).patch("/class").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("data not found");
  });

  it("should return 400 if updated data is duplicated", async () => {
    // Mock Class.findById, Class.findOne, and existing class data
    const existingClassData = {
      _id: new mongoose.Types.ObjectId(),
      className: "Sample Class",
      semester: "semester_id",
      subject: "subject_id",
    };
    jest.spyOn(Class, "findById").mockReturnValue({
      exec: jest.fn().mockResolvedValue(existingClassData),
    });
    jest.spyOn(Class, "findOne").mockReturnValue({
      exec: jest.fn().mockResolvedValue(existingClassData),
    });
    const requestBody = {
      id: existingClassData._id.toString(),
      className: "Sample Class",
      active: true,
      note: "Updated note",
    };

    const response = await request(app).patch("/class").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("New Update Data Duplicated");
  });

  it("should return 500 if class update fails", async () => {
    // Mock Class.findById and Class.prototype.updateOne to throw an error
    const existingClassData = {
      _id: new mongoose.Types.ObjectId(),
      className: "Sample Class",
      semester: "semester_id",
      subject: "subject_id",
    };
    jest.spyOn(Class, "findById").mockImplementation(() => {
      throw new Error("Mock Error With update class function");
    });
    jest.spyOn(Class, "findOne").mockReturnValue({
      exec: jest.fn().mockResolvedValue(null),
    });

    const requestBody = {
      id: "valid_id",
      className: "Updated Class Name",
      active: true,
      note: "Updated note",
    };

    const response = await request(app).patch("/class").send(requestBody);

    console.log(response.body);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe(
      "Error during process : Error: Mock Error With update class function"
    );
  });

  // Add more test cases as needed
});

describe("POST /class/add-students", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should add students to the class", async () => {
    // Mock User.findOne to return existing student
    jest.spyOn(User, "findOne").mockResolvedValue({
      _id: "existing_student_id",
      email: "existing@student.com",
      roles: ["Trainee"],
    });

    // Mock User.updateOne
    jest.spyOn(User, "updateOne").mockResolvedValue({});

    // Mock Class.updateOne
    jest.spyOn(Class, "updateOne").mockResolvedValue({});

    // Mock bcrypt.hash
    jest.spyOn(bcrypt, "hash").mockResolvedValue("hashed_email");

    // Mock mailer.sendMail
    jest.spyOn(mailer, "sendMail").mockResolvedValue({});

    const requestBody = {
      classId: "class_id",
      students: [
        {
          email: "existing@student.com",
          fullName: "Existing Student",
          phoneNumber: "1234567890",
        },
        {
          email: "new@student.com",
          fullName: "New Student",
          phoneNumber: "9876543210",
        },
      ],
    };

    const response = await request(app)
      .post("/class/add-students")
      .send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Students added successfully");
  });

  it("should handle errors during processing", async () => {
    // Mock User.findOne to throw an error
    jest.spyOn(User, "findOne").mockImplementation(() => {
      throw new Error("Mock Error : Mock User.findOne to throw an error");
    });

    const requestBody = {
      classId: "class_id",
      students: [
        {
          email: "existing@student.com",
          fullName: "Existing Student",
          phoneNumber: "1234567890",
        },
      ],
    };

    try {
      const response = await request(app)
        .post("/class/add-students")
        .send(requestBody);
      expect(response.status).toBe(500);
      expect(response.body.message).toBe("An error occurred during processing");
    } catch (error) {
      // Handle the error if needed
      // For example: console.error(error);
    }
    jest.clearAllMocks();
  });
});

describe("POST /class/add-student", () => {
  it("should add an existing student to the class", async () => {
    // Mock User.findOne to return an existing user
    const existingUser = {
      _id: "existing_user_id",
      email: "existing@student.com",
    };
    jest.spyOn(User, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(existingUser),
    });

    // Mock Class.findByIdAndUpdate to return a successful update
    jest.spyOn(Class, "findByIdAndUpdate").mockResolvedValue(null);

    const requestBody = {
      classId: "class_id",
      email: "existing@student.com",
    };

    const response = await request(app)
      .post("/class/add-student")
      .send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Add done");
  });

  it("should add a new student to the class", async () => {
    // Mock User.findOne to return null (new user)
    jest.spyOn(User, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });

    // Mock User.prototype.save to return a created user
    const createdUser = {
      _id: "created_user_id",
      email: "new@student.com",
    };
    const saveMock = jest.fn().mockResolvedValue(createdUser);
    jest.spyOn(User.prototype, "save").mockImplementation(saveMock);

    // Mock Class.findByIdAndUpdate to return a successful update
    jest.spyOn(Class, "findByIdAndUpdate").mockResolvedValue(null);

    const requestBody = {
      classId: "class_id",
      email: "new@student.com",
    };

    const response = await request(app)
      .post("/class/add-student")
      .send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Students added successfully");
  });

  it("should handle errors during processing", async () => {
    // Mock User.findOne to throw an error
    jest.spyOn(User, "findOne").mockImplementation(() => {
      throw new Error("Mock Error with findOne user function");
    });
    const requestBody = {
      classId: "class_id",
      email: "existing@student.com",
    };

    const response = await request(app)
      .post("/class/add-student")
      .send(requestBody);

    expect(response.status).toBe(401);
    expect(response.body.message).toBe("Add student to class error ");
  });
});
