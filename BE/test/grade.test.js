const request = require("supertest");
const app = require("../server"); // Replace with the path to your Express app
const Presentation = require("../models/Presentation"); // Import the Presentation model
const Grade = require("../models/Grade");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("POST /grade", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get presentations with grades", async () => {
    // Mock Presentation.find to return an array of mock presentations
    const mockPresentations = [
      {
        _id: "presentation_id1",
        schedule: "schedule_id",
        constructiveQuestion: "CQ_id",
      },
      {
        _id: "presentation_id2",
        schedule: "schedule_id",
        constructiveQuestion: "CQ_id",
      },
    ];
    jest.spyOn(Presentation, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockResolvedValue(mockPresentations),
    });

    const requestBody = {
      scheduleId: "schedule_id", // Replace with the actual schedule ID
      CQId: "CQ_id", // Replace with the actual constructive question ID
    };

    const response = await request(app).post("/grade").send(requestBody);

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // You can check response.body to validate the presentations returned
  });

  it("should handle errors when fetching presentations", async () => {
    // Mock Presentation.find to throw an error
    jest.spyOn(Presentation, "find").mockImplementation(() => {
      throw new Error("Mock Error while finding presentations");
    });

    const requestBody = {
      scheduleId: "schedule_id", // Replace with the actual schedule ID
      CQId: "CQ_id", // Replace with the actual constructive question ID
    };

    const response = await request(app).post("/grade").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe(
      "no presentation foundError: Mock Error while finding presentations"
    );
  });
});

describe("POST /grade/create", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should create a new grade", async () => {
    const mockGrade = {
      _id: "grade_id",
    };
    const mockPresentation = {
      _id: "presentation_id",
    };
    jest.spyOn(Grade, "findOne").mockReturnValue(null);
    jest.spyOn(Grade.prototype, "save").mockResolvedValue(mockGrade);
    jest
      .spyOn(Presentation, "findByIdAndUpdate")
      .mockResolvedValue(mockPresentation);

    const requestBody = {
      presentation: "presentation_id",
      votedFor: "votedFor_id",
      votedBy: "votedBy_id",
    };

    const response = await request(app).post("/grade/create").send(requestBody);

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // You can check response.body.message or response.body for validation
  });

  it("should update an existing grade", async () => {
    const mockExistingGrade = {
      _id: "64d5a871776ff9a3bde9b898",
      presentation: "presentation_id",
      votedBy: "votedBy_id",
    };
    const mockUpdatedGrade = {
      _id: "64d5a871776ff9a3bde9b898",
      presentation: "presentation_id",
      votedBy: "votedBy_id",
    };
    jest.spyOn(Grade, "findOne").mockReturnValue(mockExistingGrade);
    jest.spyOn(Grade.prototype, "save").mockResolvedValue(mockUpdatedGrade);

    const requestBody = {
      presentation: "presentation_id",
      votedBy: "votedBy_id",
    };

    const response = await request(app).post("/grade/create").send(requestBody);

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // You can check response.body.message or response.body for validation
  });

  it("should handle errors when creating a grade", async () => {
    // Mock Grade.prototype.save to throw an error
    jest.spyOn(Grade.prototype, "save").mockImplementation(() => {
      throw new Error("Mock Error while saving grade");
    });

    const requestBody = {
      votedBy: "votedBy_id",
      // Other fields
    };

    const response = await request(app).post("/grade/create").send(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Error create grade");
  });
});
