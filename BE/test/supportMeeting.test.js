const mongoose = require('mongoose');
const {
  getMeetingsWithSearch,
  getMeetings,
  createMeeting,
  updateMeeting,
  getMeetingById,
  getMeetingsByClassId,
  deleteMeeting,
  updateMeetingTime,
} = require('../controllers/meetingController');
const Meeting = require('../models/Meeting');
const Class = require('../models/Class');
const User = require('../models/User');
jest.mock('../middleware/verifyJWT.js', () => (req, res, next) => {
  req.userLogin = {
    _id: 'user_id',
    roles: ['Admin'],
  };

  if (req.mockedRoles === 'empty') {
    req.userLogin = {
      _id: 'user_id',
      roles: [],
    };
  }
  if (req.mockedRoles === 'undefined') {
    req.userLogin = undefined;
  }
  next();
});

describe('createMeeting function', () => {
  beforeAll(async () => {
    await mongoose.connect('mongodb://localhost:27017/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  beforeEach(async () => {
    await Meeting.deleteMany({});
  });

  it('should create a new meeting', async () => {
    const mockReq = {
      body: {
        createdBy: new mongoose.Types.ObjectId(),
        meetingId: 'meeting123',
        meetingName: 'Sample Meeting',
        meetingType: 'video-conference',
        invitedUsers: [new mongoose.Types.ObjectId()],
        meetingFrom: new Date(),
        meetingTo: new Date(),
        classId: new mongoose.Types.ObjectId(),
        maxUsers: 10,
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await createMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(201);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        success: true,
        data: expect.any(Object),
      })
    );
  });

  it('should return an error if a meeting with the same meetingId already exists', async () => {
    // Tạo một họp giả lập để kiểm tra trường hợp xung đột
    await Meeting.create({
      meetingId: 'meeting123',
      meetingName: 'Existing Meeting',
      meetingType: 'video-conference',
      invitedUsers: [new mongoose.Types.ObjectId()],
      meetingFrom: new Date(),
      meetingTo: new Date(),
      classId: new mongoose.Types.ObjectId(),
      maxUsers: 10,
    });

    const mockReq = {
      body: {
        createdBy: new mongoose.Types.ObjectId(),
        meetingId: 'meeting123',
        meetingName: 'Sample Meeting',
        meetingType: 'video-conference',
        invitedUsers: [new mongoose.Types.ObjectId()],
        meetingFrom: new Date(),
        meetingTo: new Date(),
        classId: new mongoose.Types.ObjectId(),
        maxUsers: 10,
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await createMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(412);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        message: 'A meeting with the same meetingId already exists',
      })
    );
  });

  it('should return an error if there is an internal server error', async () => {
    // Force an internal error by not providing a required field (e.g., createdBy)
    const mockReq = {
      body: {
        // Missing createdBy
        // Missing meetingId
        meetingName: 'Sample Meeting',
        meetingType: 'video-conference',
        invitedUsers: [new mongoose.Types.ObjectId()],
        meetingFrom: new Date(),
        meetingTo: new Date(),
        classId: new mongoose.Types.ObjectId(),
        maxUsers: 10,
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await createMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        success: false,
        error: expect.any(String),
      })
    );
  });
});

describe('updateMeeting function', () => {
  beforeAll(async () => {
    await mongoose.connect('mongodb://localhost:27017/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  beforeEach(async () => {
    await Meeting.deleteMany({});
  });
  it('should update an existing meeting', async () => {
    // Tạo một họp giả lập để cập nhật
    const existingMeeting = await Meeting.create({
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting123',
      meetingName: 'Existing Meeting',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date(),
      meetingTo: new Date(),
      classId: new mongoose.Types.ObjectId(),
      maxUsers: 3,
    });

    const updatedMeetingData = {
      meetingName: 'Updated Meeting Name',
      invitedUsers: [
        ...existingMeeting.invitedUsers,
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date(),
      meetingTo: new Date(),
      maxUsers: 4,
      status: false,
    };

    const mockReq = {
      body: {
        meetingId: existingMeeting.meetingId,
        ...updatedMeetingData,
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await updateMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        success: true,
        data: expect.objectContaining(updatedMeetingData),
      })
    );

    // Kiểm tra xem dữ liệu trong database đã được cập nhật chính xác
    const updatedMeeting = await Meeting.findOne({
      meetingId: existingMeeting.meetingId,
    });
    expect(updatedMeeting.meetingName).toBe(updatedMeetingData.meetingName);
  });

  it('should return an error if the meeting to be updated is not found', async () => {
    const mockReq = {
      body: {
        createdBy: new mongoose.Types.ObjectId(),
        meetingId: 'nonExistentMeetingId',
        meetingName: 'nonExistent Meeting',
        meetingType: 'video-conference',
        invitedUsers: [
          new mongoose.Types.ObjectId(),
          new mongoose.Types.ObjectId(),
          new mongoose.Types.ObjectId(),
        ],
        meetingFrom: new Date(),
        meetingTo: new Date(),
        classId: new mongoose.Types.ObjectId(),
        maxUsers: 3,
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await updateMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(404);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        success: false,
        error: 'Meeting not found',
      })
    );
  });

  it('should return an error if there is an internal server error', async () => {
    // Tạo một họp giả lập để kiểm tra trường hợp lỗi nội bộ
    const existingMeeting = await Meeting.create({
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting123',
      meetingName: 'Existing Meeting',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date(),
      meetingTo: new Date(),
      classId: new mongoose.Types.ObjectId(),
      maxUsers: 3,
    });

    // Gây ra lỗi bằng cách không cung cấp một trường bắt buộc (ví dụ: meetingName)
    const mockReq = {
      body: {
        // meetingId: existingMeeting.meetingId,
        // Thiếu meetingName
        invitedUsers: [
          ...existingMeeting.invitedUsers,
          new mongoose.Types.ObjectId(),
        ],
        meetingFrom: new Date(),
        meetingTo: new Date(),
        maxUsers: 4,
        status: false,
      },
    };

    const mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await updateMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(404);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        success: false,
        error: expect.any(String),
      })
    );
  });
});

describe('deleteMeeting function', () => {
  beforeAll(async () => {
    await mongoose.connect('mongodb://localhost:27017/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  beforeEach(async () => {
    await Meeting.deleteMany({});
  });

  it('should delete a meeting successfully', async () => {
    // Tạo một họp giả lập để xoá
    const existingMeeting = await Meeting.create({
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting123',
      meetingName: 'Existing Meeting',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date(),
      meetingTo: new Date(),
      classId: new mongoose.Types.ObjectId(),
      maxUsers: 3,
    });

    const mockReq = {
      body: {
        id: existingMeeting._id.toString(),
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await deleteMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(201);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        message: 'Meeting delete successfully',
      })
    );

    // Kiểm tra xem dữ liệu trong database đã được xoá
    const deletedMeeting = await Meeting.findById(existingMeeting._id);
    expect(deletedMeeting).toBeNull();
  });

  it('should return an error if required fields are missing', async () => {
    const mockReq = {
      body: {
        // Thiếu trường id
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await deleteMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(412);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        message: 'Missing required fields',
      })
    );
  });

  it('should return an error if the meeting to be deleted is not found', async () => {
    const mockReq = {
      body: {
        id: 'nonExistentMeetingId',
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await deleteMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        success: false,
        error: expect.any(String),
      })
    );
  });

  it('should return an error if there is an internal server error', async () => {
    // Tạo một họp giả lập để kiểm tra trường hợp lỗi nội bộ
    const existingMeeting = await Meeting.create({
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting123',
      meetingName: 'Existing Meeting',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date(),
      meetingTo: new Date(),
      classId: new mongoose.Types.ObjectId(),
      maxUsers: 3,
    });

    // Gây ra lỗi bằng cách cố tình truyền một id không hợp lệ (ví dụ: id không đúng định dạng)
    const mockReq = {
      body: {
        id: 'invalidIdFormat',
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await deleteMeeting(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        success: false,
        error: expect.any(String),
      })
    );
  });
});

describe('getMeetingsByClassId function', () => {
  beforeAll(async () => {
    await mongoose.connect('mongodb://localhost:27017/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  beforeEach(async () => {
    await Meeting.deleteMany({});
  });

  it('should return meetings for the given classId', async () => {
    // Tạo một số họp giả lập
    const classId = new mongoose.Types.ObjectId();
    await Meeting.create({
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting123',
      meetingName: 'Meeting 1',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date('2023-08-01'),
      meetingTo: new Date('2023-09-01'),
      class: classId,
      maxUsers: 3,
    });

    await Meeting.create({
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting456',
      meetingName: 'Meeting 2',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date('2023-08-07'),
      meetingTo: new Date('2023-08-09'),
      class: classId,
      maxUsers: 4,
    });

    const mockReq = {
      params: {
        classId: classId.toString(),
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await getMeetingsByClassId(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(200);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.arrayContaining([
        expect.objectContaining({
          class: classId,
          status: true,
        }),
      ])
    );
  });

  it('should return a 404 error if no meetings are found for the given classId', async () => {
    const nonExistentClassId = new mongoose.Types.ObjectId();

    const mockReq = {
      params: {
        classId: nonExistentClassId.toString(),
      },
    };

    const mockRes = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const data = await getMeetingsByClassId(mockReq, mockRes);
    if (data && data.length === 0) {
      expect(mockRes.status).toHaveBeenCalledWith(404);
      expect(mockRes.json).toHaveBeenCalledWith(
        expect.objectContaining({
          error: 'No meetings found for the given classId',
        })
      );
    }
  });

  it('should return a 500 error if there is an internal server error', async () => {
    // Tạo lỗi giả lập bằng cách cố tình gây ra lỗi trong quá trình truy vấn
    const mockReq = {
      params: {
        classId: 'invalidIdFormat', // Điều này có thể gây ra lỗi vì classId không hợp lệ
      },
    };

    const mockRes = {
      status: jest.fn(() => mockRes),
      json: jest.fn(),
    };

    await getMeetingsByClassId(mockReq, mockRes);

    expect(mockRes.status).toHaveBeenCalledWith(500);
    expect(mockRes.json).toHaveBeenCalledWith(
      expect.objectContaining({
        error: expect.any(String),
      })
    );
  });
});

describe('getMeetingById function', () => {
  beforeAll(async () => {
    await mongoose.connect('mongodb://localhost:27017/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  });

  afterAll(async () => {
    await mongoose.connection.close();
  });

  beforeEach(async () => {
    await Meeting.deleteMany({});
  });
  it('Should get meeting by valid meetingId', async () => {
    // Tạo một cuộc họp giả lập trong cơ sở dữ liệu
    const mockMeeting = {
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting123',
      meetingName: 'Meeting Test',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date('2023-08-07'),
      meetingTo: new Date('2023-08-09'),
      class: new mongoose.Types.ObjectId(),
      maxUsers: 4,
    };
    await Meeting.create(mockMeeting);

    const req = { params: { meetingId: mockMeeting.meetingId } };
    const res = {
      json: jest.fn((data) => data),
      status: jest.fn((code) => ({ json: jest.fn((data) => data) })),
    };

    await getMeetingById(req, res);

    expect(res.json).toHaveBeenCalledWith(
      expect.objectContaining({
        meetingId: mockMeeting.meetingId,
      })
    );

    // Xóa cuộc họp giả lập sau khi hoàn thành kiểm thử
    await Meeting.deleteMany();
  });

  it('Should return 404 for invalid meetingId', async () => {
    const mockMeeting = {
      createdBy: new mongoose.Types.ObjectId(),
      meetingId: 'meeting123',
      meetingName: 'Meeting Test',
      meetingType: 'video-conference',
      invitedUsers: [
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
        new mongoose.Types.ObjectId(),
      ],
      meetingFrom: new Date('2023-08-07'),
      meetingTo: new Date('2023-08-09'),
      class: new mongoose.Types.ObjectId(),
      maxUsers: 4,
    };
    await Meeting.create(mockMeeting);
    const invalidMeetingId = new mongoose.Types.ObjectId();

    const req = { params: { meetingId: invalidMeetingId.toString() } };
    const res = {
      json: jest.fn((data) => data),
      status: jest.fn((code) => ({ json: jest.fn((data) => data) })),
    };

    const data = await getMeetingById(req, res);
    if (!data) {
      expect(res.status).toHaveBeenCalledWith(404);
      expect(res.json).toHaveBeenCalledWith({
        success: false,
        error: 'Meeting not found',
      });
    }
  });
});

describe('updateMeetingTime', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should update meeting time and return updated meeting on success', async () => {
    const updatedMeetingData = {
      meetingId: 'valid-meeting-id',
      meetingFrom: new Date('2023-07-30T10:00:00Z'),
      meetingTo: new Date('2023-07-30T12:00:00Z'),
    };

    const findOneAndUpdateMock = jest
      .spyOn(Meeting, 'findOneAndUpdate')
      .mockResolvedValue(updatedMeetingData);

    const req = {
      body: updatedMeetingData,
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await updateMeetingTime(req, res);

    expect(findOneAndUpdateMock).toHaveBeenCalledWith(
      { meetingId: 'valid-meeting-id' },
      {
        $set: {
          meetingFrom: updatedMeetingData.meetingFrom,
          meetingTo: updatedMeetingData.meetingTo,
        },
      },
      { new: true }
    );
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({
      success: true,
      data: updatedMeetingData,
    });
  });

  it('should return a 404 status and error message if meeting is not found', async () => {
    const findOneAndUpdateMock = jest
      .spyOn(Meeting, 'findOneAndUpdate')
      .mockResolvedValue(null);

    const req = {
      body: {
        meetingId: 'non-existent-meeting-id',
        meetingFrom: new Date(),
        meetingTo: new Date(),
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await updateMeetingTime(req, res);

    expect(findOneAndUpdateMock).toHaveBeenCalledWith(
      { meetingId: 'non-existent-meeting-id' },
      expect.any(Object),
      expect.any(Object)
    );
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      success: false,
      error: 'Meeting not found',
    });
  });

  it('should return a 500 status and error message on database error', async () => {
    const findOneAndUpdateMock = jest
      .spyOn(Meeting, 'findOneAndUpdate')
      .mockRejectedValue(new Error('Database error'));

    const req = {
      body: {
        meetingId: 'valid-meeting-id',
        meetingFrom: new Date(),
        meetingTo: new Date(),
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await updateMeetingTime(req, res);

    expect(findOneAndUpdateMock).toHaveBeenCalledWith(
      { meetingId: 'valid-meeting-id' },
      expect.any(Object),
      expect.any(Object)
    );
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      success: false,
      error: 'Database error',
    });
  });
});

describe('getMeetingsWithSearch function', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should handle invalid tab value', async () => {
    const req = {
      body: {
        page: 1,
        tab: 'invalid-tab-value',
      },
      userLogin: {
        roles: ['Trainer'],
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await getMeetingsWithSearch(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ error: 'Invalid tab value' });
  });
});
