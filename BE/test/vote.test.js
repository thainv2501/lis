const request = require("supertest");
const app = require("../server"); // Import your Express app
const Vote = require("../models/Vote"); // Import your Vote model
const Comment = require("../models/Comment"); // Import your Comment model
const ConstructiveQuestion = require("../models/ConstructiveQuestion");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };
  next();
});

describe("vote POST /votes", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return 400 if missing required field", async () => {
    const response = await request(app).post("/votes").send({});
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Missing required field");
  });

  it("should remove vote if duplicate vote found", async () => {
    const mockVoteId = "vote_id_1";
    jest.spyOn(Vote, "findOne").mockReturnValue({
      _id: mockVoteId,
      schedule: "schedule_id_1",
      constructiveQuestion: "cq_id_1",
      comment: "comment_id_1",
      type: "inside",
      card: "card_value",
      star: 5,
      votedBy: "user_id",
    });
    jest.spyOn(Vote, "findByIdAndDelete").mockReturnValue({});

    const response = await request(app).post("/votes").send({
      scheduleId: "schedule_id_1",
      CQId: "cq_id_1",
      commentId: "comment_id_1",
      type: "inside-group",
      card: "card_value",
      star: 5,
      votedBy: "user_id",
    });

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Voted remove");

    expect(Vote.findByIdAndDelete).toHaveBeenCalledWith(mockVoteId);
  });

  it("should update vote if existing vote on comment found", async () => {
    const mockVoteId = "vote_id_1";
    const mockVotedOnComment = {
      scheduleId: "64d8525a0f230c62cba232f4",
      CQId: "74d8525a0f230c62cba232f5",
      commentId: "54d8525a0f230c62cba232f5",
      type: "inside-group",
      card: "black",
      star: 1,
      votedBy: "37d8525a0f230c62cba232f4",
    };
    jest
      .spyOn(Vote, "findOne")
      .mockReturnValueOnce(null) // First call
      .mockReturnValueOnce(mockVotedOnComment); // Second call
    jest.spyOn(Vote, "findByIdAndUpdate").mockReturnValue({});
    jest.spyOn(Comment, "findByIdAndUpdate").mockReturnValue({});

    const response = await request(app).post("/votes").send({
      scheduleId: "64d8525a0f230c62cba232f4",
      CQId: "74d8525a0f230c62cba232f5",
      commentId: "54d8525a0f230c62cba232f5",
      type: "inside-group",
      card: "red",
      star: 4,
      votedBy: "37d8525a0f230c62cba232f4",
    });

    console.log(response.body);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Vote updated");
  });

  it("should create a new vote", async () => {
    jest.spyOn(Vote, "findOne").mockReturnValue(null);
    jest.spyOn(Vote.prototype, "save").mockReturnValue({ _id: "new_vote_id" });
    jest.spyOn(Comment, "findByIdAndUpdate").mockReturnValue({});

    const response = await request(app).post("/votes").send({
      scheduleId: "schedule_id_1",
      CQId: "cq_id_1",
      commentId: "comment_id_1",
      type: "outside-group",
      card: "card_value",
      star: 3,
      votedBy: "user_id",
    });

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Voted successfully");
  });
});

describe("getRemainVotesOfUser POST /votes/remain", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return remaining votes", async () => {
    const mockCQId = "cq_id_1";
    const mockUserId = "user_id_1";

    // Mock ConstructiveQuestion vote settings
    const mockVoteSetting = {
      redCard: { insideVote: { quantity: 5 }, outsideVote: { quantity: 3 } },
      blueCard: { insideVote: { quantity: 10 }, outsideVote: { quantity: 7 } },
      greenCard: { insideVote: { quantity: 8 }, outsideVote: { quantity: 6 } },
      blackCard: {
        insideVote: { quantity: 15 },
        outsideVote: { quantity: 10 },
      },
    };
    jest.spyOn(ConstructiveQuestion, "findById").mockReturnValue({
      voteSetting: mockVoteSetting,
    });

    // Mock votes
    const mockVotes = [
      { card: "red" },
      { card: "blue" },
      { card: "blue" },
      { card: "green" },
      // ... other mock votes
    ];
    jest.spyOn(Vote, "find").mockReturnValue(mockVotes);

    // Send request to the route
    const response = await request(app).post("/votes/remain").send({
      scheduleId: "schedule_id_1",
      CQId: mockCQId,
      userId: mockUserId,
      filter: "outside-group",
      isTrainer: false,
    });

    console.log(response.body);

    // Assertions
    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      redCard: Math.max(3 - 1, 0),
      blueCard: Math.max(7 - 2, 0),
      greenCard: Math.max(6 - 1, 0),
      blackCard: Math.max(10 - 0, 0),
    });
  });
});
