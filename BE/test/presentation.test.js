const request = require("supertest");
const app = require("../server"); // Import your Express app
const mongoose = require("mongoose"); // Import mongoose if needed
const Presentation = require("../models/Presentation"); // Import your Presentation model
const Grade = require("../models/Grade");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("createPresentation POST /presentations", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should 400 because presentation created", async () => {
    const mockScheduleId = "schedule_id_1";
    const mockCQId = "cq_id_1";
    const mockPresentationGroup = "presentation_group_1";
    const mockReviewGroup = "review_group_1";

    // Mock new presentation data
    const mockNewPresentation = {
      _id: "presentation_id_1",
      schedule: mockScheduleId,
      constructiveQuestion: mockCQId,
      presentationGroup: mockPresentationGroup,
      reviewGroup: mockReviewGroup,
    };
    jest
      .spyOn(Presentation.prototype, "save")
      .mockReturnValue(mockNewPresentation);

    // Mock existing presentation data
    const mockExistingPresentation = {
      _id: "existing_presentation_id",
      schedule: mockScheduleId,
      constructiveQuestion: mockCQId,
      presentationGroup: mockPresentationGroup,
      reviewGroup: mockReviewGroup,
    };
    jest
      .spyOn(Presentation, "findOne")
      .mockReturnValue(mockExistingPresentation);

    // Send request to the route
    const response = await request(app).post("/presentations").send({
      scheduleId: mockScheduleId,
      CQId: mockCQId,
      presentationGroup: mockPresentationGroup,
      reviewGroup: mockReviewGroup,
    });

    // Assertions
    expect(response.status).toBe(400); // Duplicate presentation
    expect(response.body.message).toBe("This Presentation created !");
  });
  it("should create a new presentation", async () => {
    const mockScheduleId = "schedule_id_1";
    const mockCQId = "cq_id_1";
    const mockPresentationGroup = "presentation_group_1";
    const mockReviewGroup = "review_group_1";

    // Mock new presentation data
    const mockNewPresentation = {
      _id: "presentation_id_1",
      schedule: mockScheduleId,
      constructiveQuestion: mockCQId,
      presentationGroup: mockPresentationGroup,
      reviewGroup: mockReviewGroup,
    };
    jest
      .spyOn(Presentation.prototype, "save")
      .mockReturnValue(mockNewPresentation);

    jest.spyOn(Presentation, "findOne").mockReturnValue(null);

    // Send request to the route with non-duplicate data
    const responseNonDuplicate = await request(app)
      .post("/presentations")
      .send({
        scheduleId: mockScheduleId,
        CQId: "different_cq_id",
        presentationGroup: "different_presentation_group",
        reviewGroup: "different_review_group",
      });

    // Assertions
    expect(responseNonDuplicate.status).toBe(200);
    expect(responseNonDuplicate.body.message).toBe(
      "create presentation success"
    );
  });
});

describe("getPresentations POST /presentations/get", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return presentations", async () => {
    const mockScheduleId = "schedule_id_1";
    const mockCQId = "cq_id_1";

    // Mock presentations data
    const mockPresentations = [
      {
        _id: "presentation_id_1",
        schedule: mockScheduleId,
        constructiveQuestion: mockCQId,
        presentationGroup: {
          _id: "presentation_group_id",
          name: "Presentation Group",
        },
        reviewGroup: {
          _id: "review_group_id",
          name: "Review Group",
        },
        grade: {
          _id: "grade_id",
          value: 90,
        },
        createdAt: "2023-08-13T03:37:25.410Z",
        // Other presentation data
      },
      // Other mock presentations
    ];

    // Mock Presentation.find() and its chained methods
    jest.spyOn(Presentation, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockResolvedValue(mockPresentations),
    });
    // Send request to the route
    const response = await request(app)
      .post("/presentations/get")
      .send({ scheduleId: mockScheduleId, CQId: mockCQId });

    // Assertions
    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockPresentations);
  });

  it("should handle error and return error response", async () => {
    const mockScheduleId = "schedule_id_1";
    const mockCQId = "cq_id_1";

    // Mock error message
    const mockError = "Some error message";

    jest.spyOn(Presentation, "find").mockImplementation(() => {
      throw new Error(mockError);
    });

    // Send request to the route
    const response = await request(app)
      .post("/presentations/get")
      .send({ scheduleId: mockScheduleId, CQId: mockCQId });

    // Assertions
    expect(response.status).toBe(400);
    expect(response.body.message).toBe(
      "no presentation foundError: Some error message"
    );
  });
});

describe("restartPresentation PATCH /presentations", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return 400 if missing required field", async () => {
    const response = await request(app)
      .patch("/presentations/restart")
      .send({});
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Missing required field");
  });

  it("should return 400 if presentation not found", async () => {
    const mockPresentationId = "presentation_id_1";
    jest.spyOn(Presentation, "findById").mockReturnValue(null);

    const response = await request(app)
      .patch("/presentations/restart")
      .send({ presentation: mockPresentationId });

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No Presentation found !");
  });

  it("should restart presentation and delete associated grades", async () => {
    const mockPresentationId = "presentation_id_1";
    const mockPresentation = {
      _id: mockPresentationId,
      grade: ["grade_id_1", "grade_id_2"],
    };

    jest.spyOn(Presentation, "findById").mockReturnValue(mockPresentation);
    jest.spyOn(Grade, "deleteMany").mockReturnValue({});

    const response = await request(app)
      .patch("/presentations/restart")
      .send({ presentation: mockPresentationId });

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("restart success");

    expect(Grade.deleteMany).toHaveBeenCalledWith({
      _id: { $in: mockPresentation.grade },
    });
  });
});
