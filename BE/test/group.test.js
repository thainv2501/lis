const request = require("supertest");
const app = require("../server"); // Replace with the path to your Express app
const Group = require("../models/Group"); // Import the Group model
const Schedule = require("../models/Schedule");
const User = require("../models/User");
const Vote = require("../models/Vote");
const Comment = require("../models/Comment");
const { default: mongoose } = require("mongoose");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("GET /groups", () => {
  it("should get group data for a student", async () => {
    const mockGroupData = {
      _id: "group_id",
      groupNumber: 1,
    };
    jest.spyOn(Group, "findOne").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      exec: jest.fn().mockResolvedValue(mockGroupData),
    });
    const response = await request(app).get("/groups").query({
      scheduleId: "64d4d276b8b81b9b992efedc",
      studentId: "student_id",
    }); // Replace with actual query params

    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockGroupData);
  });

  it("should handle missing required fields", async () => {
    const response = await request(app).get("/groups");

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Missing required filed !");
  });

  it("should handle no group found", async () => {
    jest.spyOn(Group, "findOne").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      exec: jest.fn().mockResolvedValue(null),
    });
    const response = await request(app).get("/groups").query({
      scheduleId: "64d4d276b8b81b9b992efedc",
      studentId: "student_id",
    }); // Replace with actual query params

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No Group Found !");
  });

  it("should handle errors while getting groups", async () => {
    jest.spyOn(Group, "findOne").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      exec: jest.fn().mockRejectedValue(new Error("Mock Error")),
    });

    const response = await request(app).get("/groups").query({
      scheduleId: "64d4d276b8b81b9b992efedc",
      studentId: "student_id",
    }); // Replace with actual query params

    expect(response.status).toBe(400);
    expect(response.body.message).toBe(
      "Error while get groups !Error: Mock Error"
    );
  });
});

describe("GET /groups/:scheduleId", () => {
  it("should get groups data by scheduleId", async () => {
    const mockGroupsData = [
      { _id: "group_id_1", groupNumber: 1, members: [], leader: null },
      { _id: "group_id_2", groupNumber: 2, members: [], leader: null },
      // Other mock data
    ];
    jest.spyOn(Group, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      exec: jest.fn().mockResolvedValue(mockGroupsData),
    });
    const response = await request(app).get("/groups/64d4d276b8b81b9b992efedc"); // Replace with actual scheduleId

    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockGroupsData);
  });

  it("should handle missing scheduleId", async () => {
    const response = await request(app).get("/groups/");

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Missing required filed !");
  });

  it("should handle no groups found", async () => {
    jest.spyOn(Group, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      exec: jest.fn().mockResolvedValue([]),
    });
    const response = await request(app).get("/groups/64d4d276b8b81b9b992efedc"); // Replace with actual scheduleId

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No Group Found !");
  });

  it("should handle errors while getting groups", async () => {
    jest.spyOn(Group, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      exec: jest.fn().mockRejectedValue(new Error("Mock Error")),
    });
    const response = await request(app).get("/groups/64d4d276b8b81b9b992efedc"); // Replace with actual scheduleId

    expect(response.status).toBe(400);
    expect(response.body.message).toBe(
      "Error while get groups !Error: Mock Error"
    );
  });
});

describe("POST /groups", () => {
  it("should create or update groups successfully", async () => {
    const mockScheduleId = "64d4d276b8b81b9b992efedc";
    const mockMembers = [
      { email: "student1@example.com", group: 1 },
      { email: "student2@example.com", group: 1 },
      { email: "student3@example.com", group: 2 },
      // Other mock data
    ];
    const mockUserDocuments = [
      { email: "student1@example.com", _id: "user_id_1" },
      { email: "student2@example.com", _id: "user_id_2" },
      { email: "student3@example.com", _id: "user_id_3" },
      // Other mock data
    ];
    const mockInClass = {
      _id: "class_id_1",
      students: [
        { email: "student1@example.com", _id: "student_id_1" },
        { email: "student2@example.com", _id: "student_id_2" },
        { email: "student3@example.com", _id: "student_id_3" },
        // Other mock student data
      ],
    };

    const mockExistingGroup = { _id: "existing_group_id" };

    jest.spyOn(Schedule, "findOne").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      exec: jest.fn().mockReturnValue({
        _id: mockScheduleId,
        trainer: "trainer_id",
        inClass: mockInClass,
      }),
    });

    jest.spyOn(User, "find").mockReturnValue(mockUserDocuments);
    jest.spyOn(Group, "findOne").mockReturnValue(mockExistingGroup);
    jest.spyOn(Group, "findByIdAndUpdate").mockReturnValue({}); // Mock the update response
    jest.spyOn(Group.prototype, "save").mockReturnValue({}); // Mock the save response

    const response = await request(app)
      .post("/groups")
      .send({ scheduleId: mockScheduleId, members: mockMembers });

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Groups created or updated successfully"
    );
  });

  it("should handle missing scheduleId", async () => {
    const response = await request(app).post("/groups").send({});

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Missing required field");
  });

  it("should handle schedule not found", async () => {
    jest.spyOn(Schedule, "findOne").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      exec: jest.fn().mockReturnValue(null),
    });
    const response = await request(app)
      .post("/groups")
      .send({ scheduleId: "non_existing_schedule_id", members: [] });

    expect(response.status).toBe(404);
    expect(response.body.message).toBe("Schedule not found");
  });

  it("should handle errors while creating or updating groups", async () => {
    jest.spyOn(Schedule, "findOne").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      exec: jest.fn().mockRejectedValue(new Error("Mock Error")),
    });

    const response = await request(app)
      .post("/groups")
      .send({ scheduleId: "64d4d276b8b81b9b992efedc", members: [] });

    expect(response.status).toBe(400);
    expect(response.body.message).toBe(
      "Groups created or updated FailError: Mock Error"
    );
  });
});

describe("POST /groups/remove", () => {
  it("should remove student out of group and reset related data", async () => {
    const mockGroupId = "group_id_1";
    const mockStudentId = "student_id_1";

    const mockStudent = { _id: mockStudentId };
    jest.spyOn(User, "findById").mockReturnValue(mockStudent);

    const mockGroup = {
      _id: mockGroupId,
      leader: mockStudentId,
      members: [
        { _id: new mongoose.Types.ObjectId(mockStudentId) }, // Convert to ObjectId
        { _id: new mongoose.Types.ObjectId("student_id_2") }, // Convert to ObjectId
        // Other mock members
      ],
      schedule: "schedule_id_1",
    };
    jest.spyOn(Group, "findById").mockReturnValue(mockGroup);
    jest.spyOn(Group, "findByIdAndUpdate").mockReturnValue({});

    jest.spyOn(Vote, "deleteMany").mockReturnValue({});

    const mockComments = [
      {
        _id: "comment_id_1",
        votes: ["vote_id_1", "vote_id_2"],
        schedule: "schedule_id_1",
      },
      // Other mock comments
    ];
    jest.spyOn(Comment, "find").mockReturnValue(mockComments);
    jest.spyOn(Vote, "deleteMany").mockReturnValue({});

    const response = await request(app)
      .patch("/groups/remove")
      .send({ groupId: mockGroupId, studentId: mockStudentId });

    console.log(response.body);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Student remove out of Group");
  });

  it("should return 400 if group not found", async () => {
    const mockGroupId = "non_existent_group_id";
    const mockStudentId = "student_id_1";

    jest.spyOn(Group, "findById").mockReturnValue(null);

    const response = await request(app)
      .patch("/groups/remove")
      .send({ groupId: mockGroupId, studentId: mockStudentId });

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Group not found");
  });

  // Add more test cases as needed for edge cases and error scenarios
});

describe("moveStudentToGroup", () => {
  it("should move student to a new group and reset related data", async () => {
    const mockOldGroupId = "old_group_id_1";
    const mockNewGroupId = "new_group_id_1";
    const mockStudentId = "student_id_1";

    // Mock student data
    const mockStudent = { _id: mockStudentId };
    jest.spyOn(User, "findById").mockReturnValue(mockStudent);

    // Mock old and new group data
    const mockOldGroup = {
      _id: mockOldGroupId,
      leader: mockStudentId,
      members: [
        { _id: new mongoose.Types.ObjectId(mockStudentId) },
        { _id: new mongoose.Types.ObjectId("student_id_2") },
        // Other mock members
      ],
      schedule: "schedule_id_1",
    };
    const mockNewGroup = {
      _id: mockNewGroupId,
      leader: "leader_id_1", // Assuming new group has a leader
      members: [
        { _id: new mongoose.Types.ObjectId("student_id_3") },
        { _id: new mongoose.Types.ObjectId("student_id_4") },
        // Other mock members
      ],
      schedule: "schedule_id_1",
    };
    jest
      .spyOn(Group, "findById")
      .mockReturnValueOnce(mockOldGroup)
      .mockReturnValueOnce(mockNewGroup);
    jest.spyOn(Group, "findByIdAndUpdate").mockReturnValue({});

    // Mock comments and votes
    const mockComments = [
      {
        _id: "comment_id_1",
        votes: ["vote_id_1", "vote_id_2"],
        schedule: "schedule_id_1",
        createdBy: mockStudentId,
      },
      // Other mock comments
    ];
    jest.spyOn(Comment, "find").mockReturnValue(mockComments);
    jest.spyOn(Vote, "deleteMany").mockReturnValue({});

    // Send request to the route
    const response = await request(app).patch("/groups/move").send({
      oldGroupId: mockOldGroupId,
      newGroupId: mockNewGroupId,
      studentId: mockStudentId,
    });

    console.log(response.body);

    // Assertions
    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Student move in Group");
  });
});

describe("combineNewGroup", () => {
  it("should create a new group by combining students", async () => {
    const mockScheduleId = "schedule_id_1";
    const mockGroupNumber = 3;

    // Mock students data
    const mockStudents = [
      { _id: "student_id_1" },
      { _id: "student_id_2" },
      { _id: "student_id_3" },
      // Other mock students
    ];

    // Mock new group data
    const mockNewGroup = {
      _id: "new_group_id_1",
      schedule: mockScheduleId,
      groupNumber: mockGroupNumber,
      members: mockStudents.map((student) => student._id),
      leader: mockStudents[0]._id,
    };
    jest.spyOn(Group.prototype, "save").mockReturnValue(mockNewGroup);

    // Send request to the route
    const response = await request(app).post("/groups/combine").send({
      scheduleId: mockScheduleId,
      students: mockStudents,
      groupNumber: mockGroupNumber,
    });

    console.log(response.body);

    // Assertions
    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Student move in Group");
  });
});
