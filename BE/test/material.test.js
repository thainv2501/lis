const request = require("supertest");
const app = require("../server"); // Assuming you have an Express app defined in app.js
const Material = require("../models/Material"); // Import the Material model

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("POST /materials", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should create a new material", async () => {
    // Mock Material.save to return a mock created material
    const mockCreatedMaterial = {
      _id: "material_id",
      content: "Sample content",
      sources: ["source_1", "source_2"],
      uploadedBy: "user_id",
    };
    jest
      .spyOn(Material.prototype, "save")
      .mockResolvedValue(mockCreatedMaterial);

    const requestBody = {
      content: "Sample content",
      sources: ["source_1", "source_2"],
      uploadedBy: "user_id",
    };

    const response = await request(app).post("/materials").send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Material Created successfully");
    expect(response.body.createdMaterial).toEqual(mockCreatedMaterial);
  });

  it("should handle errors during material creation", async () => {
    // Mock Material.save to throw an error
    jest.spyOn(Material.prototype, "save").mockImplementation(() => {
      throw new Error("Mock Error creating material");
    });

    const requestBody = {
      content: "Sample content",
      sources: ["source_1", "source_2"],
      uploadedBy: "user_id",
    };

    const response = await request(app).post("/materials").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("createdMaterial is not defined");
  });

  it("should handle missing required fields", async () => {
    const requestBody = {
      // Missing content, sources, and uploadedBy
    };

    const response = await request(app).post("/materials").send(requestBody);

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required fields");
  });
});

describe("DELETE /materials", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should delete a material", async () => {
    // Mock Material.findByIdAndDelete to return a successful deletion
    jest.spyOn(Material, "findByIdAndDelete").mockResolvedValue({});

    const requestBody = {
      id: "material_id",
    };

    const response = await request(app).delete("/materials").send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Material Delete successfully");
  });

  it("should handle errors during material deletion", async () => {
    // Mock Material.findByIdAndDelete to throw an error
    jest.spyOn(Material, "findByIdAndDelete").mockImplementation(() => {
      throw new Error("Mock Error deleting material");
    });

    const requestBody = {
      id: "material_id",
    };

    const response = await request(app).delete("/materials").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Material delete fail");
  });

  it("should handle missing required fields", async () => {
    const requestBody = {
      // Missing id
    };

    const response = await request(app).delete("/materials").send(requestBody);

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required fields");
  });
});

describe("PATCH /materials", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should update a material", async () => {
    // Mock Material.findByIdAndUpdate to return a successful update
    jest.spyOn(Material, "findByIdAndUpdate").mockResolvedValue({});

    const requestBody = {
      id: "material_id",
      content: "Updated Content",
      sources: ["Source 1", "Source 2"],
      uploadedBy: "user_id",
    };

    const response = await request(app).patch("/materials").send(requestBody);

    console.log(response.body);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Material update successfully");
  });

  it("should handle errors during material update", async () => {
    // Mock Material.findByIdAndUpdate to throw an error
    jest.spyOn(Material, "findByIdAndUpdate").mockImplementation(() => {
      throw new Error("Mock Error updating material");
    });

    const requestBody = {
      id: "material_id",
      content: "Updated Content",
      sources: ["Source 1", "Source 2"],
      uploadedBy: "user_id",
    };

    const response = await request(app).patch("/materials").send(requestBody);

    console.log(response.body);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Material Update Fail");
  });

  it("should handle missing required fields", async () => {
    const requestBody = {
      // Missing id
      content: "Updated Content",
      sources: ["Source 1", "Source 2"],
      uploadedBy: "user_id",
    };

    const response = await request(app).patch("/materials").send(requestBody);

    console.log(response.body);

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required fields");
  });
});
