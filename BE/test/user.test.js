const request = require("supertest");
const app = require("../server"); // Assuming you have an Express app defined in app.js
const User = require("../models/User");
const bcrypt = require("bcrypt");
const mailer = require("../utils/mailer");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };
  next();
});

describe("GET /users/:id", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get user by ID", async () => {
    const userId = "user_id"; // Replace with a valid user ID from your database
    const mockUser = {
      _id: userId,
      fullName: "John Doe",
      email: "johndoe@example.com",
      // ...other user properties
    };

    // Mock the User.findById method to return a user
    jest.spyOn(User, "findById").mockReturnValue({
      select: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockUser),
    });

    // Perform the API call using supertest
    const response = await request(app).get(`/users/${userId}`);

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockUser);

    // Assert that User.findById was called with the provided user ID
    expect(User.findById).toHaveBeenCalledWith(userId);
  });

  it("should return 400 if no user is found", async () => {
    const userId = "nonexistent_id"; // Replace with a non-existent user ID

    // Mock the User.findById method to return a user
    jest.spyOn(User, "findById").mockReturnValue({
      select: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });

    // Perform the API call using supertest
    const response = await request(app).get(`/users/${userId}`);

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No user found");

    // Assert that User.findById was called with the provided user ID
    expect(User.findById).toHaveBeenCalledWith(userId);
  });
});

describe("GET /users", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return users based on search criteria", async () => {
    const keyword = "test";
    const page = 1;
    const role = "Admin";
    const active = "active";

    const mockUsers = [
      {
        _id: "user_id_1",
        email: "user1@example.com",
        fullName: "User 1",
        role: ["Admin", "Guest"],
      },
      {
        _id: "user_id_2",
        email: "user2@example.com",
        fullName: "User 2",
        role: ["Admin", "Guest"],
      },
      // ...add more mock users as needed
    ];

    jest.spyOn(User, "find").mockReturnValue({
      select: jest.fn().mockReturnThis(),
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockUsers),
    });

    jest.spyOn(User, "countDocuments").mockReturnValue({
      exec: jest.fn().mockResolvedValue(2),
    });

    const response = await request(app)
      .get("/users")
      .query({ keyword, page, role, active })
      .expect(200);

    expect(response.body.usersCount).toBe(2);
    expect(response.body.users).toEqual(mockUsers);
  });

  it("should return a 400 status if no users are found", async () => {
    // Mock the User.find method to return an empty array
    jest.spyOn(User, "find").mockReturnValue({
      select: jest.fn().mockReturnThis(),
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue([]),
    });
    // Mock the User.countDocuments method to return 0
    jest.spyOn(User, "countDocuments").mockReturnValue({
      exec: jest.fn().mockResolvedValue(0),
    });
    // Perform the API call using supertest
    const response = await request(app).get("/users");

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No users found");

    // Assert that User.find was called
    expect(User.find).toHaveBeenCalled();

    // Assert that User.countDocuments was called
    expect(User.countDocuments).toHaveBeenCalled();
  });
});

describe("POST /users", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should create a new user", async () => {
    // Set the request body with valid user data
    const requestBody = {
      email: "user@example.com",
      fullName: "John Doe",
      phoneNumber: "123456789",
      roles: ["Admin", "User"],
    };

    // Mock the User.findOne method to return null (no duplicate email)
    jest.spyOn(User, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });

    // Mock the bcrypt.hash method to return a hashed password
    jest.spyOn(bcrypt, "hash").mockResolvedValue("hashedPassword");

    // Mock the mailer.sendMail method to do nothing
    jest.spyOn(mailer, "sendMail").mockResolvedValue();

    // Mock the User.create method to return the new user
    jest.spyOn(User, "create").mockResolvedValue({ email: requestBody.email });

    // Perform the API call using supertest
    const response = await request(app)
      .post("/users")
      .send(requestBody)
      .set("Content-Type", "application/json");

    // Assert the response
    expect(response.status).toBe(201);
    expect(response.body.message).toBe(`New user ${requestBody.email} created`);
  });

  it("should return a 400 status if invalid user data is received", async () => {
    // Set the request body with invalid user data (missing required fields)
    const requestBody = {
      email: "user@example.com",
      fullName: "John Doe",
      // Missing phoneNumber and roles
    };
    // Perform the API call using supertest
    const response = await request(app)
      .post("/users")
      .send(requestBody)
      .set("Content-Type", "application/json");

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("All fields are required");

    // Assert that User.findOne, bcrypt.hash, mailer.sendMail, and User.create were not called
    expect(User.findOne).not.toHaveBeenCalled();
    expect(bcrypt.hash).not.toHaveBeenCalled();
    expect(mailer.sendMail).not.toHaveBeenCalled();
    expect(User.create).not.toHaveBeenCalled();
  });

  it("should return a 409status if duplicate email is found", async () => {
    // Set the request body with valid user data
    const requestBody = {
      email: "user@example.com",
      fullName: "John Doe",
      phoneNumber: "123456789",
      roles: ["Admin", "User"],
    };

    // Mock the User.findOne method to return a duplicate user
    jest.spyOn(User, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue({ email: requestBody.email }),
      exec: jest.fn().mockResolvedValue({ email: requestBody.email }),
    });

    // Perform the API call using supertest
    const response = await request(app)
      .post("/users")
      .send(requestBody)
      .set("Content-Type", "application/json");

    // Assert the response
    expect(response.status).toBe(409);
    expect(response.body.message).toBe("Duplicate emails");

    // Assert that User.findOne was called with the provided email
    expect(User.findOne).toHaveBeenCalledWith({ email: requestBody.email });

    // Assert that bcrypt.hash, mailer.sendMail, and User.create were not called
    expect(bcrypt.hash).not.toHaveBeenCalled();
    expect(mailer.sendMail).not.toHaveBeenCalled();
    expect(User.create).not.toHaveBeenCalled();
  });

  it("should return a 500 status if an error occurs during hashing", async () => {
    // Set the request body with valid user data
    const requestBody = {
      email: "user@example.com",
      fullName: "John Doe",
      phoneNumber: "123456789",
      roles: ["Admin", "User"],
    };

    // Mock the User.findOne method to return null (no duplicate email)
    jest.spyOn(User, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
      exec: jest.fn().mockResolvedValue(null),
    });

    // Mock the bcrypt.hash method to reject with an error
    jest.spyOn(bcrypt, "hash").mockRejectedValue(new Error("Hashing error"));

    // Mock the mailer.sendMail method to reject with an error
    jest.spyOn(mailer, "sendMail").mockRejectedValue();

    // Perform the API call using supertest
    const response = await request(app)
      .post("/users")
      .send(requestBody)
      .set("Content-Type", "application/json");

    // Assert the response
    expect(response.status).toBe(500);
    expect(response.body.message).toBe("An error while hashing email");

    // Assert that User.findOne and User.create were called
    expect(User.findOne).toHaveBeenCalledWith({ email: requestBody.email });
  });
  it("should return a 500 status if an error occurs during sending email", async () => {
    // Set the request body with valid user data
    const requestBody = {
      email: "user@example.com",
      fullName: "John Doe",
      phoneNumber: "123456789",
      roles: ["Admin", "User"],
    };

    // Mock the User.findOne method to return null (no duplicate email)
    jest.spyOn(User, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
      exec: jest.fn().mockResolvedValue(null),
    });

    // Mock the mailer.sendMail method to reject with an error
    jest
      .spyOn(mailer, "sendMail")
      .mockRejectedValue(new Error("Email sending error"));

    // Perform the API call using supertest
    const response = await request(app)
      .post("/users")
      .send(requestBody)
      .set("Content-Type", "application/json");

    // Assert the response
    expect(response.status).toBe(500);
    expect(response.body.message).toBe(
      "An error occurred during user creation"
    );

    // Assert that User.findOne and User.create were called
    expect(User.findOne).toHaveBeenCalledWith({ email: requestBody.email });
  });
});

describe("PATCH users", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should update a user", async () => {
    const userId = "user_id"; // Replace with a valid user ID from your database

    // Mock the User.findById method to return a user
    jest.spyOn(User, "findById").mockReturnValue({
      exec: jest.fn().mockResolvedValue({
        _id: userId,
        email: "test@example.com",
        fullName: "Test User",
        phoneNumber: "1234567890",
        active: true,
        roles: ["Admin"],
      }),
    });

    // Mock the User.updateOne method to simulate successful updates
    jest.spyOn(User, "updateOne").mockResolvedValue();

    // Perform the API call using supertest
    const response = await request(app)
      .patch(`/users`)
      .send({
        id: userId,
        fullName: "Updated User",
        phoneNumber: "9876543210",
        active: false,
        roles: ["User"],
        note: "Some note",
      });

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body.message).toBe("test@example.com updated");
  });

  it("return error when update a nonexistent user", async () => {
    const userId = "nonexistent_user_id"; // Replace with a valid user ID from your database

    // Mock the User.findById method to return a user
    jest.spyOn(User, "findById").mockReturnValue({
      exec: jest.fn().mockResolvedValue(null),
    });

    // Mock the User.updateOne method to simulate successful updates
    jest.spyOn(User, "updateOne").mockResolvedValue();

    // Perform the API call using supertest
    const response = await request(app)
      .patch(`/users`)
      .send({
        id: userId,
        fullName: "Updated User",
        phoneNumber: "9876543210",
        active: false,
        roles: ["User"],
        note: "Some note",
      });

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("User not found");
  });
});

describe("GET users/:id", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should get user by ID", async () => {
    const userId = "user_id"; // Replace with a valid user ID from your database
    const mockUser = {
      _id: userId,
      fullName: "John Doe",
      email: "johndoe@example.com",
      // ...other user properties
    };

    // Mock the User.findById method to return a user
    jest.spyOn(User, "findById").mockReturnValue({
      select: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockUser),
    });

    // Perform the API call using supertest
    const response = await request(app).get(`/users/${userId}`);

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockUser);

    // Assert that User.findById was called with the provided user ID
    expect(User.findById).toHaveBeenCalledWith(userId);
  });
  it("return error with status 400 and message when no user found", async () => {
    const userId = "nonexistent_user_id"; // Replace with a valid user ID from your database

    // Mock the User.findById method to return a user
    jest.spyOn(User, "findById").mockReturnValue({
      select: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });
    // Perform the API call using supertest
    const response = await request(app).get(`/users/${userId}`);

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toEqual("No user found");
  });
});

describe("GET users/role/:role", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should get users by role", async () => {
    const role = "Admin"; // Replace with the role you want to test
    const mockUsers = [
      {
        _id: "user_id_1",
        fullName: "User 1",
        email: "user1@example.com",
        // ...other user properties
      },
      {
        _id: "user_id_2",
        fullName: "User 2",
        email: "user2@example.com",
        // ...other user properties
      },
      // ...add more mock users as needed
    ];

    // Mock the User.find method to return users with the specified role
    jest.spyOn(User, "find").mockReturnValue({
      select: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockUsers),
    });

    // Perform the API call using supertest
    const response = await request(app).get(`/users/role/${role}`);

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockUsers);

    // Assert that User.find was called with the specified role
    expect(User.find).toHaveBeenCalledWith({ roles: { $in: [role] } });
  });
});
