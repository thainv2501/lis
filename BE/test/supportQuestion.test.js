const SupportQuestion = require('../models/SupportQuestion');
const Semester = require('../models/MasterData');
const Class = require('../models/Class');
const Schedule = require('../models/Schedule');
const Subject = require('../models/Subject');
const mongoose = require('mongoose');

const {
  getSupportQuestions,
  getSubjectQuestions,
  createSupportQuestion,
  deleteSupportQuestion,
  updateSupportQuestion,
  updateSupportQuestionStatus,
  getSupportQuestionById,
} = require('../controllers/supportQuestionController'); // Update the path

jest.mock('../models/SupportQuestion.js', () => ({
  countDocuments: jest.fn(),
  findById: jest.fn(),
  create: jest.fn(),
  findByIdAndUpdate: jest.fn(),
  findByIdAndDelete: jest.fn(),
}));

jest.mock('../models/Subject', () => ({
  findById: jest.fn(),
}));
jest.mock('../models/Schedule', () => ({
  findById: jest.fn(),
}));

// Mock the verifyJWT middleware
jest.mock('../middleware/verifyJWT.js', () => (req, res, next) => {
  req.userLogin = {
    _id: 'user_id',
    roles: ['Admin'],
  };

  if (req.mockedRoles === 'empty') {
    req.userLogin = {
      _id: 'user_id',
      roles: [],
    };
  }
  if (req.mockedRoles === 'undefined') {
    req.userLogin = undefined;
  }
  next();
});

describe('getSupportQuestions', () => {
  it('should get support questions based on tab', async () => {
    const req = {
      body: {
        tab: 'all-question',
        scheduleId: new mongoose.Types.ObjectId(),
        page: 1,
        type: 'support-question',
      },
    };

    const res = {
      json: jest.fn(),
    };

    const mockSupportQuestions = [
      // mock data here
    ];

    SupportQuestion.countDocuments = jest.fn().mockResolvedValue(5);
    SupportQuestion.find = jest.fn().mockResolvedValue(mockSupportQuestions);

    const data = await getSupportQuestions(req, res);
    if (data) {
      expect(SupportQuestion.countDocuments).toHaveBeenCalledWith(
        expect.objectContaining({
          type: req.body.type,
          schedule: req.body.scheduleId,
        })
      );
      expect(SupportQuestion.find).toHaveBeenCalledWith(
        expect.objectContaining({
          type: req.body.type,
          schedule: req.body.scheduleId,
        })
      );
      expect(res.json).toHaveBeenCalledWith({
        totalSupportQuestionsCount: 5,
        supportQuestions: mockSupportQuestions,
      });
    }
  });

  // Write other test cases for different scenarios
});

describe('getSubjectQuestions', () => {
  it('should get subject questions based on tab', async () => {
    const req = {
      body: {
        tab: 'all-question',
        subjectId: new mongoose.Types.ObjectId(),
        page: 1,
        type: 'subject-question',
      },
    };

    const res = {
      json: jest.fn(),
    };

    const mockSubjectQuestions = [
      // mock data here
    ];

    SupportQuestion.countDocuments = jest.fn().mockResolvedValue(3);
    SupportQuestion.find = jest.fn().mockResolvedValue(mockSubjectQuestions);

    const data = await getSubjectQuestions(req, res);
    if (data) {
      expect(SupportQuestion.countDocuments).toHaveBeenCalledWith(
        expect.objectContaining({
          type: req.body.type,
          subject: req.body.subjectId,
        })
      );
      expect(SupportQuestion.find).toHaveBeenCalledWith(
        expect.objectContaining({
          type: req.body.type,
          subject: req.body.subjectId,
        })
      );
      expect(res.json).toHaveBeenCalledWith({
        totalSubjectQuestionsCount: 3,
        subjectQuestions: mockSubjectQuestions,
      });
    }
  });

  // Write other test cases for different scenarios
});

describe('createSupportQuestion', () => {
  it('should create a new support question', async () => {
    const req = {
      body: {
        type: 'support-question',
        scheduleId: new mongoose.Types.ObjectId(),
        title: 'Sample Title',
        content: 'Sample Content',
        sources: [
          {
            name: 'Sample Source',
            url: 'http://sample-url.com',
            type: 'Sample Type',
          },
        ],
        createdBy: new mongoose.Types.ObjectId(),
      },
    };

    const res = {
      status: jest.fn(),
      json: jest.fn(),
    };

    const mockSubject = {
      _id: req.body.subjectId,
    };
    const mockSchedule = {
      _id: req.body.scheduleId,
    };
    const mockSavedQuestion = {
      _id: new mongoose.Types.ObjectId(),
      type: req.body.type,
      title: req.body.title,
      content: req.body.content,
      sources: req.body.sources,
      createdBy: req.body.createdBy,
    };

    const Subject = {
      findById: jest.fn().mockResolvedValue(mockSubject),
    };

    const Schedule = {
      findById: jest.fn().mockResolvedValue(mockSchedule),
    };

    SupportQuestion.create.mockResolvedValue(mockSavedQuestion);

    const supportQuestion = await createSupportQuestion(req, res, {
      Subject,
      Schedule,
      SupportQuestion,
    });

    console.log(mockSavedQuestion, 'mockSavedQuestion');
    if (supportQuestion) {
      expect(res.json).toHaveBeenCalledWith({
        message: 'Support question created successfully',
        data: mockSavedQuestion,
      });
    } else {
      expect(res.json).toHaveBeenCalledWith({
        error: 'Server error',
      });
    }
  });

  it('should handle invalid type', async () => {
    const req = {
      body: {
        type: 'invalid-type',
      },
    };

    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await createSupportQuestion(req, res, { SupportQuestion });

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ error: 'Invalid type' });
  });

  // Add more test cases if needed
});
describe('deleteSupportQuestion', () => {
  it('should delete a support question', async () => {
    const req = {
      body: {
        id: new mongoose.Types.ObjectId(),
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await deleteSupportQuestion(req, res);

    expect(SupportQuestion.findByIdAndDelete).toHaveBeenCalledWith(req.body.id);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Support question delete successfully',
    });
  });

  it('should handle missing id', async () => {
    const req = {
      body: {},
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await deleteSupportQuestion(req, res);

    expect(res.status).toHaveBeenCalledWith(412);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Missing required fields',
    });
  });
});

describe('updateSupportQuestion', () => {
  it('should update a support question', async () => {
    const req = {
      body: {
        questionId: new mongoose.Types.ObjectId(),
        title: 'Updated Title',
        content: 'Updated Content',
        sources: [],
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    const mockUpdatedQuestion = {
      _id: req.body.questionId,
      title: req.body.title,
      content: req.body.content,
      sources: req.body.sources,
    };

    SupportQuestion.findByIdAndUpdate.mockResolvedValue(mockUpdatedQuestion);

    await updateSupportQuestion(req, res);

    expect(SupportQuestion.findByIdAndUpdate).toHaveBeenCalledWith(
      req.body.questionId,
      {
        title: req.body.title,
        content: req.body.content,
        sources: req.body.sources,
      },
      { new: true }
    );
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({
      success: true,
      data: mockUpdatedQuestion,
    });
  });
});

describe('updateSupportQuestionStatus', () => {
  it('should update support question status', async () => {
    const req = {
      body: {
        questionId: new mongoose.Types.ObjectId(),
        newStatus: 'answered',
      },
    };
    const res = {
      json: jest.fn(),
    };

    const mockSupportQuestion = {
      _id: req.body.questionId,
      status: req.body.newStatus,
      save: jest.fn(),
    };

    SupportQuestion.findById.mockResolvedValue(mockSupportQuestion);

    await updateSupportQuestionStatus(req, res);

    expect(SupportQuestion.findById).toHaveBeenCalledWith(req.body.questionId);
    expect(mockSupportQuestion.save).toHaveBeenCalled();
    expect(res.json).toHaveBeenCalledWith({
      message: 'Support question status updated successfully',
    });
  });
});

describe('getSupportQuestionById', () => {
  it('should get support question by ID with valid data', async () => {
    const supportQuestionId = new mongoose.Types.ObjectId();
    const createdByUserId = new mongoose.Types.ObjectId();
    const subjectId = new mongoose.Types.ObjectId();
    const trainerId = new mongoose.Types.ObjectId();

    const mockSupportQuestion = {
      _id: supportQuestionId,
      createdBy: createdByUserId,
      subject: subjectId,
      schedule: { trainer: trainerId },
      reply: [new mongoose.Types.ObjectId()],
      // ...other properties
    };

    const populateSpy = jest.fn();
    const populateMock = jest.fn().mockReturnValue({
      populate: populateSpy,
    });

    const findByIdMock = jest
      .fn()
      .mockResolvedValue({ populate: populateMock, ...mockSupportQuestion });

    const statusMock = jest.fn();
    const jsonMock = jest.fn();
    const res = { status: statusMock, json: jsonMock };

    SupportQuestion.findById = findByIdMock;

    await getSupportQuestionById({ params: { id: supportQuestionId } }, res);

    expect(findByIdMock).toHaveBeenCalledWith(supportQuestionId);
  });

  it('should handle non-existing support question', async () => {
    const supportQuestionId = new mongoose.Types.ObjectId();

    const findByIdMock = jest.fn().mockResolvedValue(null);

    const statusMock = jest.fn();
    const jsonMock = jest.fn();
    const res = { status: statusMock, json: jsonMock };

    SupportQuestion.findById = findByIdMock;

    await getSupportQuestionById({ params: { id: supportQuestionId } }, res);

    expect(findByIdMock).toHaveBeenCalledWith(supportQuestionId);
  });
});
