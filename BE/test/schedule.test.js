const request = require("supertest");
const app = require("../server"); // Assuming you have an Express app defined in app.js
const Schedule = require("../models/Schedule");
const Lesson = require("../models/Lesson");
const User = require("../models/User");
const Class = require("../models/Class");
const { default: mongoose } = require("mongoose");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("POST /schedules", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should create a new schedule", async () => {
    const requestBody = {
      inClass: "64d4d276b8b81b9b992efedc",
      day: "Monday",
      slotInDay: "Morning",
      trainer: "trainer_id",
      createdBy: "user_id",
      content: "Schedule content",
      lessons: ["lesson_id_1", "lesson_id_2"],
      constructiveQuestions: ["question_id_1"],
      materials: ["material_id_1"],
      done: false,
    };

    jest.spyOn(Schedule, "findOne").mockReturnValue(null);
    jest.spyOn(Schedule.prototype, "save").mockResolvedValue({
      _id: "schedule_id",
      ...requestBody,
    });

    const response = await request(app).post("/schedules").send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Schedule Created successfully");
    expect(response.body.createdSchedule).toBeDefined();
    expect(response.body.createdSchedule._id).toBe("schedule_id");
  });

  it("should return 400 if schedule is a duplicate", async () => {
    const requestBody = {
      inClass: "64d4d276b8b81b9b992efedc",
      day: "Monday",
      slotInDay: "Morning",
      trainer: "trainer_id",
      createdBy: "user_id",
      content: "Schedule content",
      lessons: ["lesson_id_1", "lesson_id_2"],
      constructiveQuestions: ["question_id_1"],
      materials: ["material_id_1"],
      done: false,
    };

    jest.spyOn(Schedule, "findOne").mockReturnValue({
      _id: "existing_schedule_id",
      ...requestBody,
    });

    const response = await request(app).post("/schedules").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Schedule Duplicate");
  });

  it("should return 412 for missing required fields", async () => {
    // Omitting required fields intentionally
    const requestBody = {};

    const response = await request(app).post("/schedules").send(requestBody);

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required fields");
  });

  it("should handle errors during schedule creation", async () => {
    jest.spyOn(Schedule, "findOne").mockReturnValue(null);
    jest
      .spyOn(Schedule.prototype, "save")
      .mockRejectedValue(new Error("Mock Error : Schedule save error"));

    const requestBody = {
      inClass: "64d4d276b8b81b9b992efedc",
      day: "Monday",
      slotInDay: "Morning",
      trainer: "trainer_id",
      createdBy: "user_id",
      content: "Schedule content",
      lessons: ["lesson_id_1", "lesson_id_2"],
      constructiveQuestions: ["question_id_1"],
      materials: ["material_id_1"],
      done: false,
    };

    const response = await request(app).post("/schedules").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Schedule Created fail");
  });
});

describe("POST /schedules/XLSX", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should import schedules from XLSX file", async () => {
    const requestBody = {
      inClass: {
        subject: { _id: "subject_id" }, // Make sure to set subject._id correctly
        semester: { from: "2023-08-01", to: "2023-12-31" },
      },
      createdBy: "user_id",
      content: "Schedule content",
      constructiveQuestions: ["question_id_1"],
      materials: ["material_id_1"],
      schedules: [
        {
          day: "2023-08-15",
          slotInDay: "Morning",
          lessons: ["lesson_id_1", "lesson_id_2"],
        },
      ],
    };

    jest.spyOn(Schedule, "findOne").mockReturnValue(null);
    jest.spyOn(Schedule.prototype, "save").mockResolvedValue({
      _id: "schedule_id",
      ...requestBody,
    });
    const lessonsMock = [
      {
        _id: "lesson_id_1",
        subject: { _id: "subject_id" },
        createdBy: { _id: "user_id" },
        materials: [{ _id: "material_id_1", uploadedBy: { _id: "user_id" } }],
        constructiveQuestions: [
          { _id: "question_id_1", createdBy: { _id: "user_id" } },
        ],
      },
      // Add more lessons as needed
    ];

    jest.spyOn(Lesson, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(lessonsMock),
    });

    const response = await request(app)
      .post("/schedules/XLSX")
      .send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Import Schedules from XLSX is successful"
    );
  });

  it("should return 412 for missing required fields", async () => {
    // Omitting required fields intentionally
    const requestBody = {};

    const response = await request(app)
      .post("/schedules/XLSX")
      .send(requestBody);

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required fields");
  });

  it("should handle invalid dates", async () => {
    const requestBody = {
      inClass: { semester: { from: "2023-08-01", to: "2023-12-31" } },
      createdBy: "user_id",
      content: "Schedule content",
      constructiveQuestions: ["question_id_1"],
      materials: ["material_id_1"],
      schedules: [
        {
          day: "2023-07-15", // Invalid date
          slotInDay: "Morning",
          lessons: ["lesson_id_1", "lesson_id_2"],
        },
      ],
    };

    const response = await request(app)
      .post("/schedules/XLSX")
      .send(requestBody);

    expect(response.status).toBe(200); // Since invalid dates are logged, not prevented
    expect(response.body.message).toBe(
      "Import Schedules from XLSX is successful"
    );
  });

  it("should handle errors during schedule import", async () => {
    jest.spyOn(Schedule, "findOne").mockReturnValue(null);
    jest
      .spyOn(Schedule.prototype, "save")
      .mockRejectedValue(new Error("Mock Error : schedule save XLSX error"));

    const requestBody = {
      inClass: { semester: { from: "2023-08-01", to: "2023-12-31" } },
      createdBy: "user_id",
      content: "Schedule content",
      constructiveQuestions: ["question_id_1"],
      materials: ["material_id_1"],
      schedules: [
        {
          day: "2023-08-15",
          slotInDay: "Morning",
          lessons: ["lesson_id_1", "lesson_id_2"],
        },
      ],
    };

    const response = await request(app)
      .post("/schedules/XLSX")
      .send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Schedule Created fail");
  });
});

describe("GET /schedules", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should return schedules for an Admin", async () => {
    // Mock userLogin
    const adminUser = {
      _id: "admin_user_id",
      roles: ["Admin"],
    };
    jest.spyOn(User, "findById").mockReturnValue(adminUser);

    // Mock Schedule.find to return schedules
    const schedulesMock = [
      {
        _id: "schedule_id_1",
        inClass: "64d4d276b8b81b9b992efedc",
        day: "2023-08-15",
        slotInDay: "Morning",
        lessons: [],
        createdBy: "user_id",
        constructiveQuestions: [],
      },
      // Add more schedules as needed
    ];
    jest.spyOn(Schedule, "find").mockReturnValue({
      sort: jest.fn().mockReturnThis(),
      populate: jest.fn().mockReturnThis(),
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(schedulesMock),
    });

    const response = await request(app)
      .get("/schedules")
      .query({ classId: "64d4d276b8b81b9b992efedc", page: 1 });

    expect(response.status).toBe(200);
    expect(response.body.schedules.length).toBe(schedulesMock.length);
    // Add more expectations as needed
  });

  it("should return schedules for a Trainer or Student", async () => {
    // Mock userLogin
    const trainerUser = {
      _id: "trainer_user_id",
      roles: ["Trainer"],
    };
    jest.spyOn(User, "findById").mockReturnValue(trainerUser);

    // Mock Schedule.find to return schedules
    const schedulesMock = [
      {
        _id: "schedule_id_1",
        inClass: "64d4d276b8b81b9b992efedc",
        day: "2023-08-15",
        slotInDay: "Morning",
        lessons: [],
        createdBy: "user_id",
        constructiveQuestions: [],
      },
      // Add more schedules as needed
    ];
    jest.spyOn(Schedule, "find").mockReturnValue({
      sort: jest.fn().mockReturnThis(),
      populate: jest.fn().mockReturnThis(),
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(schedulesMock),
    });

    // Mock Class.exists to return true for Trainer/Student access
    jest.spyOn(Class, "exists").mockResolvedValue(true);

    const response = await request(app)
      .get("/schedules")
      .query({ classId: "64d4d276b8b81b9b992efedc", page: 1 });

    expect(response.status).toBe(200);
    expect(response.body.schedules.length).toBe(schedulesMock.length);
    // Add more expectations as needed
    jest.clearAllMocks();
  });

  it("should return 403 for unauthorized users", async () => {
    // Mock Class.exists to return false for unauthorized access
    jest.spyOn(Class, "exists").mockResolvedValue(false);
    const response = await request(app)
      .get("/schedules")
      .query({ classId: "64d4d276b8b81b9b992efedc", page: 1 })
      .set("mockedRoles", "empty");

    expect(response.status).toBe(403);
    expect(response.body.message).toBe("Unauthorized");
  });

  it("should handle errors", async () => {
    // Mock User.findById to throw an error
    jest.spyOn(Schedule, "find").mockImplementation(() => {
      throw new Error("Mock Error find schedule with is should error");
    });
    const response = await request(app)
      .get("/schedules")
      .query({ classId: "64d4d276b8b81b9b992efedc", page: 1 });

    expect(response.status).toBe(400);
  });
});

describe("PATCH /schedules", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should update schedule general information", async () => {
    // Mock Schedule.findById to return a schedule
    const existingSchedule = {
      _id: new mongoose.Types.ObjectId(),
      inClass: "class_id",
      day: new Date(),
      slotInDay: 1,
    };
    jest.spyOn(Schedule, "findById").mockReturnValue(existingSchedule);

    // Mock Schedule.findOne to return null for duplicate check
    jest.spyOn(Schedule, "findOne").mockResolvedValue(null);

    // Mock Schedule.findByIdAndUpdate to return the updated schedule
    jest
      .spyOn(Schedule, "findByIdAndUpdate")
      .mockResolvedValue(existingSchedule);

    const requestBody = {
      scheduleId: existingSchedule._id,
      inClass: "class_id",
      day: new Date(),
      slotInDay: 2,
      trainer: "trainer_id",
      content: "Updated content",
      done: true,
    };

    const response = await request(app).patch("/schedules").send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Update Schedule : " + existingSchedule._id + " successful"
    );
  });

  it("should update schedule other fields", async () => {
    // Mock Schedule.findById to return a schedule
    const existingSchedule = {
      _id: new mongoose.Types.ObjectId(),
      inClass: "class_id",
      day: new Date(),
      slotInDay: 1,
    };
    jest.spyOn(Schedule, "findById").mockReturnValue(existingSchedule);

    // Mock Schedule.findByIdAndUpdate to return the updated schedule
    jest
      .spyOn(Schedule, "findByIdAndUpdate")
      .mockResolvedValue(existingSchedule);

    const requestBody = {
      scheduleId: existingSchedule._id,
      lessons: ["lesson_id"],
      material: "material_id",
      constructiveQuestion: "question_id",
      content: "Updated content",
    };

    const response = await request(app).patch("/schedules").send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Update Schedule : " + existingSchedule._id + " successful"
    );
  });

  it("should handle errors during update", async () => {
    // Mock Schedule.findById to throw an error
    jest.spyOn(Schedule, "findById").mockRejectedValue(new Error("Mock Error"));

    const requestBody = {
      scheduleId: "valid_schedule_id",
      // Other fields here
    };

    const response = await request(app).patch("/schedules").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toContain(
      "Something went wrong while update Schedule"
    );
  });
});

describe("POST /schedules/scheduleFilter", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should filter schedules by tab and semester", async () => {
    // Mock Schedule.find to return an array of mock schedules
    const mockSchedules = [
      {
        _id: new mongoose.Types.ObjectId(),
        inClass: {
          _id: "class_id_1",
          semester: { _id: "semester_id_1" },
        },
        constructiveQuestions: [{ from: new Date() }],
        done: false,
      },
      // Add more mock schedules as needed
    ];
    jest.spyOn(Schedule, "find").mockReturnValue({
      sort: jest.fn().mockReturnThis(),
      populate: jest.fn().mockReturnThis(),
      exec: jest.fn().mockResolvedValue(mockSchedules),
    });

    // Mock Set
    const mockSet = new Set();
    mockSet.add("class_id_1");
    jest.spyOn(Set.prototype, "has").mockReturnValue(true);

    // Mock User
    const userLogin = {
      _id: "user_id",
      roles: ["Trainer"],
    };

    const requestBody = {
      tab: "all",
      semester: "semester_id_1",
      page: 1,
    };

    const response = await request(app)
      .post("/schedules/scheduleFilter")
      .send(requestBody)
      .set("mockedRoles", JSON.stringify(userLogin));

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // You can check response.body.totalSchedulesCount and response.body.schedules
  });

  it("should handle errors during filtering", async () => {
    // Mock Schedule.find to throw an error
    jest.spyOn(Schedule, "find").mockImplementation(() => {
      throw new Error(
        "Mock Error find schedule with is should error in filter"
      );
    });
    const requestBody = {
      tab: "all",
      semester: "semester_id_1",
      page: 1,
    };

    const response = await request(app)
      .post("/schedules/scheduleFilter")
      .send(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Server error");
  });
});
