const request = require("supertest");
const jwt = require("jsonwebtoken");
const app = require("../server"); // Assuming you have an Express app defined in app.js

describe("verifyJWT middleware", () => {
  it("should return 401 Unauthorized if no token is provided", async () => {
    const response = await request(app)
      .get("/protected-route")
      .set("Authorization", "");

    expect(response.status).toBe(401);
    expect(response.body).toEqual({ message: "Unauthorized" });
  });

  it("should return 403 Forbidden if an invalid token is provided", async () => {
    const invalidToken = "invalid_token";
    const response = await request(app)
      .get("/protected-route")
      .set("Authorization", `Bearer ${invalidToken}`);

    expect(response.status).toBe(403);
    expect(response.body).toEqual({ message: "Forbidden" });
  });

  it("should set req.email and req.roles if a valid token is provided", async () => {
    const validToken = jwt.sign(
      { data: { userLogin: { email: "test@example.com", roles: ["admin"] } } },
      process.env.ACCESS_TOKEN_SECRET
    );

    const response = await request(app)
      .get("/protected-route")
      .set("Authorization", `Bearer ${validToken}`);

    expect(response.status).toBe(200); // Assuming the protected route returns 200 for successful authentication
    expect(response.body).toEqual({}); // Assuming the protected route returns an empty response
    expect(response.req.email).toBe("test@example.com");
    expect(response.req.roles).toEqual(["admin"]);
  });
});
