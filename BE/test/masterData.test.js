const request = require("supertest");
const app = require("../server"); // Assuming you have an Express app defined in app.js
const MasterData = require("../models/MasterData");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("GET /master-data", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should get master data based on search criteria", async () => {
    const page = 1;
    const type = "category"; // Replace with the desired type
    const active = "active";
    const keyword = "code"; // Replace with the desired keyword

    const mockMasterData = [
      {
        _id: "data_id_1",
        code: "code1",
        name: "Data 1",
        type: "category",
        active: true,
        // ...other data properties
      },
      {
        _id: "data_id_2",
        code: "code2",
        name: "Data 2",
        type: "category",
        active: true,
        // ...other data properties
      },
      // ...add more mock data as needed
    ];

    // Mock the MasterData.find method to return master data
    jest.spyOn(MasterData, "find").mockReturnValue({
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockMasterData),
    });

    // Mock the MasterData.countDocuments method to return the number of master data
    jest.spyOn(MasterData, "countDocuments").mockReturnValue({
      exec: jest.fn().mockResolvedValue(2),
    });

    // Perform the API call using supertest
    const response = await request(app)
      .get("/master-data")
      .query({ page, type, active, keyword });

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body.masterDataCount).toBe(2);
    expect(response.body.masterData).toEqual(mockMasterData);

    // Assert that MasterData.find was called with the expected search criteria
    expect(MasterData.find).toHaveBeenCalledWith({
      $or: [
        { code: { $regex: keyword, $options: "i" } },
        { name: { $regex: keyword, $options: "i" } },
      ],
      active: true,
      type: "category",
    });

    // Assert that MasterData.countDocuments was called with the expected search criteria
    expect(MasterData.countDocuments).toHaveBeenCalledWith({
      $or: [
        { code: { $regex: keyword, $options: "i" } },
        { name: { $regex: keyword, $options: "i" } },
      ],
      active: true,
      type: "category",
    });
  });
  it("return error status 400 and error message when no data found", async () => {
    const page = 1;
    const type = "category"; // Replace with the desired type
    const active = "active";
    const keyword = "nonexistent_code"; // Replace with the desired keyword

    const mockMasterData = [
      {
        _id: "data_id_1",
        code: "code1",
        name: "Data 1",
        type: "category",
        active: true,
        // ...other data properties
      },
      {
        _id: "data_id_2",
        code: "code2",
        name: "Data 2",
        type: "category",
        active: true,
        // ...other data properties
      },
      // ...add more mock data as needed
    ];

    // Mock the MasterData.find method to return master data
    jest.spyOn(MasterData, "find").mockReturnValue({
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue([]),
    });

    // Mock the MasterData.countDocuments method to return the number of master data
    jest.spyOn(MasterData, "countDocuments").mockReturnValue({
      exec: jest.fn().mockResolvedValue(0),
    });

    // Perform the API call using supertest
    const response = await request(app)
      .get("/master-data")
      .query({ page, type, active, keyword });

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toEqual("No data found");

    // Assert that MasterData.find was called with the expected search criteria
    expect(MasterData.find).toHaveBeenCalledWith({
      $or: [
        { code: { $regex: keyword, $options: "i" } },
        { name: { $regex: keyword, $options: "i" } },
      ],
      active: true,
      type: "category",
    });

    // Assert that MasterData.countDocuments was called with the expected search criteria
    expect(MasterData.countDocuments).toHaveBeenCalledWith({
      $or: [
        { code: { $regex: keyword, $options: "i" } },
        { name: { $regex: keyword, $options: "i" } },
      ],
      active: true,
      type: "category",
    });
  });
});

describe("GET /master-data/:type", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should return master data based on the provided type", async () => {
    const type = "exampleType"; // Replace with the desired type for testing

    // Mock the MasterData.find method to return dummy master data
    const mockMasterData = [
      { _id: "masterDataId1", type, name: "Data 1", active: true },
      { _id: "masterDataId2", type, name: "Data 2", active: true },
      // Add more dummy master data as needed
    ];
    jest.spyOn(MasterData, "find").mockReturnValue({
      sort: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockMasterData),
    });

    // Perform the API call using Supertest
    const response = await request(app).get(`/master-data/${type}`);

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockMasterData);

    // Assert that MasterData.find was called with the expected parameters
    expect(MasterData.find).toHaveBeenCalledWith({ type, active: true });
    expect(MasterData.find().lean).toHaveBeenCalled();
  });

  it("should return a 400 status if no master data is found", async () => {
    const type = "nonexistentType"; // Replace with a type that does not exist

    // Mock the MasterData.find method to return an empty array
    jest.spyOn(MasterData, "find").mockReturnValue({
      sort: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });

    // Perform the API call using Supertest
    const response = await request(app).get(`/master-data/${type}`);

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No data found");

    // Assert that MasterData.find was called with the expected parameters
    expect(MasterData.find).toHaveBeenCalledWith({ type, active: true });
    expect(MasterData.find().lean).toHaveBeenCalled();
  });
});

describe("POST /master-data", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should create new master data", async () => {
    const mockMasterData = {
      type: "exampleType",
      code: "exampleCode",
      name: "Example Data",
      description: "Example description",
      displayOrder: 1,
    };

    // Mock the MasterData.findOne method to return null (no duplicate)
    jest.spyOn(MasterData, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });

    // Mock the MasterData.create method to return the created master data
    jest.spyOn(MasterData, "create").mockResolvedValue(mockMasterData);

    // Perform the API call using Supertest
    const response = await request(app)
      .post("/master-data")
      .send(mockMasterData);

    // Assert the response
    expect(response.status).toBe(201);
    expect(response.body.message).toBe("New master data created");

    // Assert that MasterData.findOne was called with the expected parameters
    expect(MasterData.findOne).toHaveBeenCalledWith({
      $or: [{ code: mockMasterData.code }, { name: mockMasterData.name }],
    });
    expect(MasterData.findOne().collation).toHaveBeenCalledWith({
      locale: "en",
      strength: 2,
    });
    expect(MasterData.findOne().lean).toHaveBeenCalled();
  });

  it("should return a 409 status if duplicate master data is found", async () => {
    const mockMasterData = {
      type: "exampleType",
      code: "duplicateCode",
      name: "Duplicate Data",
    };

    // Mock the MasterData.findOne method to return a duplicate master data
    jest.spyOn(MasterData, "findOne").mockReturnValue({
      collation: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockMasterData),
    });

    // Perform the API call using Supertest
    const response = await request(app)
      .post("/master-data")
      .send(mockMasterData);

    // Assert the response
    expect(response.status).toBe(409);
    expect(response.body.message).toBe("Duplicate master data code or name !");

    // Assert that MasterData.findOne was called with the expected parameters
    expect(MasterData.findOne).toHaveBeenCalledWith({
      $or: [{ code: mockMasterData.code }, { name: mockMasterData.name }],
    });
    expect(MasterData.findOne().collation).toHaveBeenCalledWith({
      locale: "en",
      strength: 2,
    });
    expect(MasterData.findOne().lean).toHaveBeenCalled();

    // Assert that MasterData.create was not called
    expect(MasterData.create).not.toHaveBeenCalled();
  });

  it("should return a 400 status if required data is missing", async () => {
    const mockMasterData = {
      type: "exampleType",
      name: "Missing Code",
      description: "Example description",
    };

    // Perform the API call using Supertest
    const response = await request(app)
      .post("/master-data")
      .send(mockMasterData);

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("code are required data");

    // Assert that MasterData.findOne and MasterData.create were not called
    expect(MasterData.findOne).not.toHaveBeenCalled();
    expect(MasterData.create).not.toHaveBeenCalled();
  });
});

describe("PATCH /master-data", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should update master data", async () => {
    const mockMasterData = {
      id: "exampleId",
      code: " master-data-code",
      name: "Updated Data",
      active: false,
      description: "Updated description",
      displayOrder: 2,
    };

    // Mock the MasterData.findById method to return the existing master data
    jest.spyOn(MasterData, "findById").mockReturnValue({
      exec: jest.fn().mockResolvedValue({
        _id: mockMasterData.id,
        code: " master-data-code",
        name: "Original Data",
        active: true,
        description: "Original description",
        displayOrder: 1,
      }),
    });

    // Mock the MasterData.updateOne method
    jest.spyOn(MasterData, "updateOne").mockImplementation();

    // Perform the API call using Supertest
    const response = await request(app)
      .patch("/master-data")
      .send(mockMasterData);

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body.message).toBe(`${mockMasterData.code} updated`);

    // Assert that MasterData.findById was called with the expected id
    expect(MasterData.findById).toHaveBeenCalledWith(mockMasterData.id);

    // Assert that MasterData.updateOne was called with the expected update operations
    expect(MasterData.updateOne).toHaveBeenCalledWith(
      { _id: mockMasterData.id },
      {
        name: mockMasterData.name,
        active: mockMasterData.active,
        description: mockMasterData.description,
        displayOrder: mockMasterData.displayOrder,
      }
    );
  });

  it("should return a 400 status if data is not found", async () => {
    const mockMasterData = {
      id: "nonExistentId",
      name: "Updated Data",
    };

    // Mock the MasterData.findById method to return null (data not found)
    jest.spyOn(MasterData, "findById").mockReturnValue({
      exec: jest.fn().mockResolvedValue(null),
    });

    // Perform the API call using Supertest
    const response = await request(app)
      .patch("/master-data")
      .send(mockMasterData);

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("data not found");

    // Assert that MasterData.findById was called with the expected id
    expect(MasterData.findById).toHaveBeenCalledWith(mockMasterData.id);

    // Assert that MasterData.updateOne was not called
    expect(MasterData.updateOne).not.toHaveBeenCalled();
  });

  it("should return a 400 status if id is missing", async () => {
    const mockMasterData = {
      name: "Updated Data",
    };

    // Perform the API call using Supertest
    const response = await request(app)
      .patch("/master-data")
      .send(mockMasterData);

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Id are required");

    // Assert that MasterData.findById and MasterData.updateOne were not called
    expect(MasterData.findById).not.toHaveBeenCalled();
    expect(MasterData.updateOne).not.toHaveBeenCalled();
  });
});
