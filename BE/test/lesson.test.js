const request = require("supertest");
const app = require("../server"); // Assuming you have an Express app defined in app.js
const Schedule = require("../models/Schedule");
const Lesson = require("../models/Lesson");
const User = require("../models/User");
const Class = require("../models/Class");
const { default: mongoose } = require("mongoose");
const Subject = require("../models/Subject");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("GET /lessons/:subjectId", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it("should get lessons with search condition", async () => {
    // Mock Lesson.find to return an array of mock lessons
    const mockLessons = [
      {
        _id: "64d60b8390732b52e2245f2a",
        subject: "subject_id",
        lessonContent: "Sample Lesson",
        status: "active",
        createdBy: { _id: "user_id" },
      },
      {
        _id: "64d60b8390732b52e2245f2a",
        subject: "subject_id",
        lessonContent: "Sample Lesson",
        status: "active",
        createdBy: { _id: "user_id" },
      },
      {
        _id: "64d60b8390732b52e2245f2a",
        subject: "subject_id",
        lessonContent: "Sample Lesson",
        status: "active",
        createdBy: { _id: "user_id" },
      },
      // Add more mock lessons as needed
    ];
    jest.spyOn(Lesson, "find").mockReturnValue({
      sort: jest.fn().mockReturnThis(),
      populate: jest.fn().mockReturnThis(),
      limit: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockLessons),
    });
    jest.spyOn(Lesson, "countDocuments").mockReturnValue({
      exec: jest.fn().mockResolvedValue(3),
    });

    const requestBody = {
      page: 1,
      status: "active",
      keyword: "Sample",
    };

    const response = await request(app)
      .get("/lessons/subject_id")
      .query(requestBody);

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // You can check response.body.lessonsCount and response.body.lessons
  });

  it("should handle errors when fetching lessons", async () => {
    // Mock Lesson.find to throw an error
    jest.spyOn(Lesson, "find").mockImplementation(() => {
      throw new Error("Mock Error find lessons is should error");
    });
    const requestBody = {
      page: 1,
      status: "active",
      keyword: "Sample",
    };

    const response = await request(app)
      .get("/lessons/subject_id")
      .query(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe(
      "Error during processError: Mock Error find lessons is should error"
    );
  });
});

describe("GET /lessons/lesson-details/:lessonId", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get lesson by ID", async () => {
    // Mock Lesson.findById to return a mock lesson
    const mockLesson = {
      _id: "lesson_id",
      subject: "subject_id",
      lessonContent: "Sample Lesson",
      createdBy: { _id: "64d60b8390732b52e2245f2a" },
      materials: [
        {
          _id: "material_id",
          uploadedBy: { _id: "64d60b8390732b52e2245f2a" },
        },
      ],
      constructiveQuestions: [
        {
          _id: "question_id",
          createdBy: { _id: "64d60b8390732b52e2245f2a" },
        },
      ],
    };
    jest.spyOn(Lesson, "findById").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(mockLesson),
    });

    const response = await request(app).get(
      "/lessons/lesson-details/lesson_id"
    );

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // For example: expect(response.body._id).toBe('lesson_id');
  });

  it("should handle not found lesson by ID", async () => {
    // Mock Lesson.findById to return null
    jest.spyOn(Lesson, "findById").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      lean: jest.fn().mockResolvedValue(null),
    });

    const response = await request(app).get(
      "/lessons/lesson-details/non_existent_id"
    );

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("No Lesson found");
  });

  it("should handle errors when fetching lesson by ID", async () => {
    // Mock Lesson.findById to throw an error
    jest.spyOn(Lesson, "findById").mockImplementation(() => {
      throw new Error("Mock Error finding lesson by ID");
    });

    const response = await request(app).get(
      "/lessons/lesson-details/lesson_id"
    );

    expect(response.status).toBe(500);
    expect(response.body.message).toBe(
      "error : Error: Mock Error finding lesson by ID"
    );
  });
});

describe("POST /lessons", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should create a new lesson", async () => {
    // Mock Lesson.create to return a mock lesson
    const mockLesson = {
      _id: "lesson_id",
      subject: "subject_id",
      lessonChapter: 1,
      lessonNumber: 1,
      lessonContent: "Sample Lesson",
      createdBy: "user_id",
    };
    jest.spyOn(Lesson, "create").mockResolvedValue(mockLesson);

    // Mock Subject.updateOne to return a successful update
    jest.spyOn(Subject, "updateOne").mockResolvedValue(null);

    const requestBody = {
      subject: "subject_id",
      lessonChapter: 1,
      lessonNumber: 1,
      lessonContent: "Sample Lesson",
      createdBy: "user_id",
    };

    const response = await request(app).post("/lessons").send(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Something went wrong");
  });

  it("should update an existing lesson", async () => {
    // Mock Lesson.findOne to return an existing lesson
    const existingLesson = {
      _id: "existing_lesson_id",
      subject: "subject_id",
      lessonChapter: 1,
      lessonNumber: 1,
    };
    jest.spyOn(Lesson, "findOne").mockResolvedValue(existingLesson);

    // Mock Lesson.updateOne to return a successful update
    jest.spyOn(Lesson, "updateOne").mockResolvedValue(null);

    const requestBody = {
      subject: "subject_id",
      lessonChapter: 1,
      lessonNumber: 1,
      lessonContent: "Updated Lesson",
      createdBy: "user_id",
    };

    const response = await request(app).post("/lessons").send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("New lesson created");
  });

  it("should handle errors during lesson creation", async () => {
    // Mock Lesson.create to throw an error
    jest.spyOn(Lesson, "create").mockImplementation(() => {
      throw new Error("Mock Error creating lesson");
    });

    const requestBody = {
      subject: "subject_id",
      lessonChapter: 1,
      lessonNumber: 1,
      lessonContent: "Sample Lesson",
      createdBy: "user_id",
    };

    const response = await request(app).post("/lessons").send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("New lesson created");
  });
});

describe("POST /lessons/add-lessons", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should import lessons from XLSX", async () => {
    // Mock Lesson.findOne to return null for all lessons
    jest.spyOn(Lesson, "findOne").mockResolvedValue(null);

    // Mock Lesson.create to return a mock lesson
    const mockLesson = {
      _id: "lesson_id",
      subject: "subject_id",
      lessonChapter: 1,
      lessonNumber: 1,
      lessonContent: "Sample Lesson",
      createdBy: "user_id",
    };
    jest.spyOn(Lesson, "create").mockResolvedValue(mockLesson);

    // Mock Subject.updateOne to return a successful update
    jest.spyOn(Subject, "updateOne").mockResolvedValue(null);

    const requestBody = {
      subject: "subject_id",
      lessons: [
        {
          lessonChapter: 1,
          lessonNumber: 1,
          lessonContent: "Sample Lesson 1",
        },
        {
          lessonChapter: 2,
          lessonNumber: 2,
          lessonContent: "Sample Lesson 2",
        },
      ],
      createdBy: "user_id",
    };

    const response = await request(app)
      .post("/lessons/add-lessons")
      .send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Lessons added successfully");
  });

  it("should update existing lessons from XLSX", async () => {
    // Mock Lesson.findOne to return an existing lesson
    const existingLesson = {
      _id: "existing_lesson_id",
      subject: "subject_id",
      lessonChapter: 1,
      lessonNumber: 1,
    };
    jest.spyOn(Lesson, "findOne").mockResolvedValue(existingLesson);

    // Mock Lesson.updateOne to return a successful update
    jest.spyOn(Lesson, "updateOne").mockResolvedValue(null);

    const requestBody = {
      subject: "subject_id",
      lessons: [
        {
          lessonChapter: 1,
          lessonNumber: 1,
          lessonContent: "Updated Lesson 1",
        },
      ],
      createdBy: "user_id",
    };

    const response = await request(app)
      .post("/lessons/add-lessons")
      .send(requestBody);

    expect(response.status).toBe(201);
    expect(response.body.message).toBe("Lessons added successfully");
  });

  it("should handle errors during lesson import", async () => {
    // Mock Lesson.create to throw an error
    jest.spyOn(Lesson, "findOne").mockImplementation(() => {
      throw new Error("Mock Error XLSX lessons something should wrong");
    });

    const requestBody = {
      subject: "subject_id",
      lessons: [
        {
          lessonChapter: 1,
          lessonNumber: 1,
          lessonContent: "Sample Lesson 1",
        },
        {
          lessonChapter: 2,
          lessonNumber: 2,
          lessonContent: "Sample Lesson 2",
        },
      ],
      createdBy: "user_id",
    };

    const response = await request(app)
      .post("/lessons/add-lessons")
      .send(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Something went wrong");
  });
});

describe("PATCH /lessons", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should update a lesson successfully", async () => {
    // Mock Lesson.findByIdAndUpdate to return a mock updated lesson
    const mockUpdatedLesson = {
      _id: "lesson_id",
      lessonChapter: 2,
      lessonContent: "Updated Lesson Content",
      materials: ["material_id"],
      constructiveQuestions: ["cq_id"],
      status: "active",
    };
    jest
      .spyOn(Lesson, "findByIdAndUpdate")
      .mockResolvedValue(mockUpdatedLesson);

    const requestBody = {
      lessonId: "lesson_id",
      lessonChapter: 2,
      lessonContent: "Updated Lesson Content",
      materialId: "material_id",
      CQId: "cq_id",
      status: "active",
    };

    const response = await request(app).patch("/lessons").send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("Update successful !");
    expect(response.body.updatedLesson).toEqual(mockUpdatedLesson);
  });

  it("should handle update failure", async () => {
    // Mock Lesson.findByIdAndUpdate to return null for update failure
    jest.spyOn(Lesson, "findByIdAndUpdate").mockResolvedValue(null);

    const requestBody = {
      lessonId: "lesson_id",
      lessonChapter: 2,
      lessonContent: "Updated Lesson Content",
      materialId: "material_id",
      CQId: "cq_id",
      status: "active",
    };

    const response = await request(app).patch("/lessons").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Update fail !");
  });

  it("should handle errors during lesson update", async () => {
    // Mock Lesson.findByIdAndUpdate to throw an error
    jest.spyOn(Lesson, "findByIdAndUpdate").mockImplementation(() => {
      throw new Error("Mock Error updating lesson");
    });

    const requestBody = {
      lessonId: "lesson_id",
      lessonChapter: 2,
      lessonContent: "Updated Lesson Content",
      materialId: "material_id",
      CQId: "cq_id",
      status: "active",
    };

    const response = await request(app).patch("/lessons").send(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Something went wrong");
  });
});

describe("DELETE /lessons/CQ", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should delete a constructive question from a lesson", async () => {
    // Mock Lesson.findOneAndUpdate to return a mock updated lesson
    const mockUpdatedLesson = {
      _id: "lesson_id",
      constructiveQuestions: ["cq_id_1", "cq_id_2"],
    };
    jest.spyOn(Lesson, "findOneAndUpdate").mockResolvedValue(mockUpdatedLesson);

    const requestBody = {
      lessonId: "lesson_id",
      CQId: "cq_id_1",
    };

    const response = await request(app).delete("/lessons/CQ").send(requestBody);

    console.log(response.body);

    expect(response.status).toBe(200);
    expect(response.body.newLesson).toEqual(mockUpdatedLesson);
  });

  it("should handle errors during constructive question deletion", async () => {
    // Mock Lesson.findOneAndUpdate to throw an error
    jest.spyOn(Lesson, "findOneAndUpdate").mockImplementation(() => {
      throw new Error("Mock Error deleting constructive question");
    });

    const requestBody = {
      lessonId: "lesson_id",
      CQId: "cq_id_1",
    };

    const response = await request(app).delete("/lessons/CQ").send(requestBody);

    console.log(response.body);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Something went wrong");
  });
});
