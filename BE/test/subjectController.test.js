const Subject = require('../models/Subject');
const User = require('../models/User');

// Import các hàm cần được unit test
const {
  getSubjectsWithSearch,
  getSubjectById,
  createSubject,
  updateSubject,
  deleteSubject,
} = require('../controllers/subjectController');

// Mock Subject model
jest.mock('../../models/Subject', () => ({
  find: jest.fn(),
  countDocuments: jest.fn(),
  findById: jest.fn(),
  findOne: jest.fn(),
  create: jest.fn(),
  findByIdAndUpdate: jest.fn(),
  findByIdAndDelete: jest.fn(),
}));

// Mock User model
jest.mock('../../models/User.js', () => ({
  find: jest.fn(),
}));

describe('getSubjectsWithSearch', () => {
  it('should return subjects with search condition', async () => {
    const mockSubjects = ['Subject 1', 'Subject 2'];
    const mockSubjectsCount = 2;
    Subject.find.mockResolvedValue(mockSubjects);
    Subject.countDocuments.mockResolvedValue(mockSubjectsCount);

    const req = {
      query: {
        page: '1',
        status: 'active',
        keyword: 'subject',
      },
    };

    const res = {
      json: jest.fn(),
    };

    await getSubjectsWithSearch(req, res);

    expect(Subject.find).toHaveBeenCalledWith(
      expect.objectContaining({
        $and: [
          {
            $or: [
              {
                subjectCode: { $regex: expect.any(String), $options: 'i' },
              },
              {
                subjectName: { $regex: expect.any(String), $options: 'i' },
              },
            ],
          },
          { status: true },
        ],
      })
    );
    expect(Subject.find).toHaveBeenCalledTimes(1);
    expect(Subject.countDocuments).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      subjectsCount: mockSubjectsCount,
      subjects: mockSubjects,
    });
  });

  it('should handle no subjects found', async () => {
    const mockSubjectsCount = 0;
    Subject.countDocuments.mockResolvedValue(mockSubjectsCount);

    const req = {
      query: {
        page: '1',
        status: 'deactived',
        keyword: 'subject',
      },
    };

    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await getSubjectsWithSearch(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ message: 'No subjects found' });
  });
});

describe('getSubjectById', () => {
  it('should return the subject with trainers', async () => {
    const mockSubject = {
      _id: 'subject-id',
      subjectCode: 'SUB001',
      subjectName: 'Subject 1',
      trainers: ['trainer-id-1', 'trainer-id-2'],
    };
    const mockTrainers = [
      { _id: 'trainer-id-1', name: 'Trainer 1' },
      { _id: 'trainer-id-2', name: 'Trainer 2' },
    ];
    Subject.findById.mockResolvedValue(mockSubject);
    User.find.mockResolvedValue(mockTrainers);

    const req = {
      params: {
        id: 'subject-id',
      },
    };

    const res = {
      json: jest.fn(),
    };

    await getSubjectById(req, res);

    expect(Subject.findById).toHaveBeenCalledWith('subject-id');
    expect(User.find).toHaveBeenCalledWith({
      _id: { $in: ['trainer-id-1', 'trainer-id-2'] },
    });
    expect(res.json).toHaveBeenCalledWith({
      ...mockSubject,
      trainers: mockTrainers,
    });
  });

  it('should handle errors and return error message', async () => {
    const mockError = new Error('Subject not found');
    Subject.findById.mockResolvedValue(null);
    const req = {
      params: {
        id: 'subject-id',
      },
    };

    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await getSubjectById(req, res);

    expect(Subject.findById).toHaveBeenCalledWith('subject-id');
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ message: 'No subject found' });
  });
});
describe('createSubject', () => {
  it('should create a new subject', async () => {
    const mockSubject = {
      subjectCode: 'SUB001',
      subjectName: 'Subject 1',
      subjectDescription: 'Description 1',
      chapterLimit: 10,
      lessons: ['Lesson 1', 'Lesson 2'],
      trainers: ['trainer-id-1', 'trainer-id-2'],
      materials: ['Material 1', 'Material 2'],
    };
    Subject.findOne.mockResolvedValue(null);
    Subject.create.mockResolvedValue(mockSubject);

    const req = {
      body: mockSubject,
    };

    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await createSubject(req, res);

    expect(Subject.findOne).toHaveBeenCalledWith({
      $or: [{ subjectCode: 'SUB001' }, { subjectName: 'Subject 1' }],
    });
    expect(Subject.create).toHaveBeenCalledWith(mockSubject);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith({
      message: `New subject ${mockSubject.subjectCode} created`,
    });
  });

  it('should handle duplicate subject code or name', async () => {
    const mockSubject = {
      id: 'subject-id',
      subjectCode: 'SUB001',
      subjectName: 'Subject 1',
    };
    const mockDuplicateSubject = {
      _id: 'duplicate-subject-id',
      subjectCode: 'SUB001',
      subjectName: 'Subject 1',
    };

    const req = {
      body: mockSubject,
    };

    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    Subject.findOne.mockResolvedValueOnce(mockDuplicateSubject);

    await createSubject(req, res);

    // expect(Subject.findOne).toHaveBeenCalledWith({
    //   $or: [
    //     { subjectCode: mockSubject.subjectCode },
    //     { subjectName: mockSubject.subjectName },
    //   ],
    // });
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      message:
        'Subject with the same subjectCode or subjectName already exists',
    });
  });

  it('should handle error and return error message', async () => {
    const mockError = new Error('Internal Server Error');
    Subject.create.mockRejectedValue(mockError);

    const req = {
      body: {
        subjectCode: 'SUB001',
        subjectName: 'Subject 1',
      },
    };

    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await createSubject(req, res);

    expect(Subject.create).toHaveBeenCalledTimes(2);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Something went wrong',
    });
  });
});

describe('updateSubject', () => {
  it('should return success message if subject is updated successfully', async () => {
    // Mock the Subject.findOne method to return null (no existing subject with same code or name)
    Subject.findOne = jest.fn().mockResolvedValue(null);

    // Mock the Subject.findByIdAndUpdate method to return the updated subject
    const updatedSubject = {
      _id: 'subjectId',
      subjectCode: 'ENG101',
      subjectName: 'English',
    };
    Subject.findByIdAndUpdate = jest.fn().mockResolvedValue(updatedSubject);

    // Mock req and res objects
    const req = {
      body: {
        id: 'subjectId',
        subjectCode: 'ENG101',
        subjectName: 'English',
        subjectDescription: 'English course',
        chapterLimit: 10,
        lessons: ['Lesson 1', 'Lesson 2'],
        trainers: ['Trainer 1', 'Trainer 2'],
        materials: ['Material 1', 'Material 2'],
        status: 'active',
      },
    };
    const res = {
      json: jest.fn(),
    };

    // Call the updateSubject function
    await updateSubject(req, res);

    // Check if Subject.findOne is called with the correct parameters
    expect(Subject.findOne).toHaveBeenCalledWith({
      $and: [
        { _id: { $ne: 'subjectId' } },
        {
          $or: [{ subjectCode: 'ENG101' }, { subjectName: 'English' }],
        },
      ],
    });

    // Check if Subject.findByIdAndUpdate is called with the correct parameters
    expect(Subject.findByIdAndUpdate).toHaveBeenCalledWith(
      'subjectId',
      {
        subjectCode: 'ENG101',
        subjectName: 'English',
        subjectDescription: 'English course',
        chapterLimit: 10,
        lessons: ['Lesson 1', 'Lesson 2'],
        trainers: ['Trainer 1', 'Trainer 2'],
        materials: ['Material 1', 'Material 2'],
        status: 'active',
      },
      { new: true }
    );

    // Check if the response json is called with the success message
    expect(res.json).toHaveBeenCalledWith({ message: 'ENG101 updated' });
  });

  it('should return error if subject with same code or name already exists', async () => {
    // Mock the Subject.findOne method to return an existing subject
    const existingSubject = {
      _id: 'existingSubjectId',
      subjectCode: 'ENG101',
      subjectName: 'English',
    };
    Subject.findOne = jest.fn().mockResolvedValue(existingSubject);

    // Mock req and res objects
    const req = {
      body: {
        id: 'subjectId',
        subjectCode: 'ENG101',
        subjectName: 'English',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    // Call the updateSubject function
    await updateSubject(req, res);

    // Check if Subject.findOne is called with the correct parameters
    expect(Subject.findOne).toHaveBeenCalledWith({
      $and: [
        { _id: { $ne: 'subjectId' } },
        {
          $or: [{ subjectCode: 'ENG101' }, { subjectName: 'English' }],
        },
      ],
    });

    // Check if the response status and json are called with the error message
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      message:
        'Subject with the same subjectCode or subjectName already exists',
    });
  });

  it('should return error if subject to update is not found', async () => {
    // Mock the Subject.findOne method to return null (no existing subject with same code or name)
    Subject.findOne = jest.fn().mockResolvedValue(null);

    // Mock the Subject.findByIdAndUpdate method to return null (subject not found)
    Subject.findByIdAndUpdate = jest.fn().mockResolvedValue(null);

    // Mock req and res objects
    const req = {
      body: {
        id: 'subjectId',
        subjectCode: 'ENG101',
        subjectName: 'English',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    // Call the updateSubject function
    await updateSubject(req, res);

    // Check if Subject.findOne is called with the correct parameters
    expect(Subject.findOne).toHaveBeenCalledWith({
      $and: [
        { _id: { $ne: 'subjectId' } },
        {
          $or: [{ subjectCode: 'ENG101' }, { subjectName: 'English' }],
        },
      ],
    });

    // Check if Subject.findByIdAndUpdate is called with the correct parameters
    expect(Subject.findByIdAndUpdate).toHaveBeenCalledWith(
      'subjectId',
      {
        subjectCode: 'ENG101',
        subjectName: 'English',
      },
      { new: true }
    );

    // Check if the response status and json are called with the error message
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({ message: 'Subject not found' });
  });

  it('should return error if an exception occurs', async () => {
    // Mock the Subject.findOne method to throw an error
    const errorMessage = 'Some error occurred';
    Subject.findOne = jest.fn().mockRejectedValue(new Error(errorMessage));

    // Mock req and res objects
    const req = {
      body: {
        id: 'subjectId',
        subjectCode: 'ENG101',
        subjectName: 'English',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    // Call the updateSubject function
    await updateSubject(req, res);

    // Check if Subject.findOne is called with the correct parameters
    expect(Subject.findOne).toHaveBeenCalledWith({
      $and: [
        { _id: { $ne: 'subjectId' } },
        {
          $or: [{ subjectCode: 'ENG101' }, { subjectName: 'English' }],
        },
      ],
    });

    // Check if the response status and json are called with the error message
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ message: 'Something went wrong' });
  });
});

describe('deleteSubject', () => {
  it('should return success true and empty data if subject is found and deleted', async () => {
    // Mock the Subject.findByIdAndDelete method
    const mockDeletedSubject = { _id: 'subjectId' };
    Subject.findByIdAndDelete = jest.fn().mockResolvedValue(mockDeletedSubject);

    // Mock req and res objects
    const req = { params: { id: 'subjectId' } };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    // Call the deleteSubject function
    await deleteSubject(req, res);

    // Check if Subject.findByIdAndDelete is called with the correct id
    expect(Subject.findByIdAndDelete).toHaveBeenCalledWith('subjectId');

    // Check if the response status and JSON are called with the correct values
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({ success: true, data: {} });
  });

  it('should return error if subject is not found', async () => {
    // Mock the Subject.findByIdAndDelete method to return null
    Subject.findByIdAndDelete = jest.fn().mockResolvedValue(null);

    // Mock req and res objects
    const req = { params: { id: 'invalidSubjectId' } };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    // Call the deleteSubject function
    await deleteSubject(req, res);

    // Check if Subject.findByIdAndDelete is called with the correct id
    expect(Subject.findByIdAndDelete).toHaveBeenCalledWith('invalidSubjectId');

    // Check if the response status and JSON are called with the correct values
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      success: false,
      error: 'Subject not found with id invalidSubjectId',
    });
  });

  it('should return error if an exception occurs', async () => {
    // Mock the Subject.findByIdAndDelete method to throw an error
    const errorMessage = 'Some error occurred';
    Subject.findByIdAndDelete = jest
      .fn()
      .mockRejectedValue(new Error(errorMessage));

    // Mock req and res objects
    const req = { params: { id: 'subjectId' } };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    // Call the deleteSubject function
    await deleteSubject(req, res);

    // Check if Subject.findByIdAndDelete is called with the correct id
    expect(Subject.findByIdAndDelete).toHaveBeenCalledWith('subjectId');

    // Check if the response status and JSON are called with the correct values
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      success: false,
      error: errorMessage,
    });
  });
});
