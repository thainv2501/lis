const request = require("supertest");
const app = require("../server"); // Replace with the path to your Express app
const ConstructiveQuestion = require("../models/ConstructiveQuestion"); // Import the ConstructiveQuestion model
const Schedule = require("../models/Schedule");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("POST /constructiveQuestions", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should create a constructive question", async () => {
    // Mock ConstructiveQuestion.prototype.save to return a successful save
    const saveMock = jest.fn().mockResolvedValue({
      _id: "cq_id",
      scheduleId: "schedule_id",
      title: "Sample Title",
      content: "Sample Content",
      from: new Date(),
      to: new Date(),
      createdBy: "user_id",
      displaySetting: "public",
      voteSetting: "upvote",
    });
    jest
      .spyOn(ConstructiveQuestion.prototype, "save")
      .mockImplementation(saveMock);

    const requestBody = {
      scheduleId: "schedule_id",
      title: "Sample Title",
      content: "Sample Content",
      from: new Date(),
      to: new Date(),
      createdBy: "user_id",
      displaySetting: "public",
      voteSetting: "upvote",
    };

    const response = await request(app)
      .post("/constructiveQuestions")
      .send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Constructive question created successfully"
    );
    expect(response.body.createdCQ).toEqual({
      _id: "cq_id",
      scheduleId: "schedule_id",
      title: "Sample Title",
      content: "Sample Content",
      from: expect.any(String),
      to: expect.any(String),
      createdBy: "user_id",
      displaySetting: "public",
      voteSetting: "upvote",
    });
    expect(saveMock).toHaveBeenCalled();
  });

  it("should handle errors during constructive question creation", async () => {
    // Mock ConstructiveQuestion.prototype.save to throw an error
    jest
      .spyOn(ConstructiveQuestion.prototype, "save")
      .mockImplementation(() => {
        throw new Error("Mock Error creating constructive question");
      });

    const requestBody = {
      scheduleId: "schedule_id",
      title: "Sample Title",
      content: "Sample Content",
      from: new Date(),
      to: new Date(),
      createdBy: "user_id",
      displaySetting: "public",
      voteSetting: "upvote",
    };

    const response = await request(app)
      .post("/constructiveQuestions")
      .send(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe(
      "Failed to create constructive question"
    );
  });
});

describe("PATCH /constructiveQuestions/time", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should update constructive question time and related schedule", async () => {
    // Mock ConstructiveQuestion.findById to return a mock constructive question
    const mockConstructiveQuestion = {
      _id: "cq_id",
      from: new Date("2023-08-10T10:00:00Z"),
      to: new Date("2023-08-10T12:00:00Z"),
    };
    jest
      .spyOn(ConstructiveQuestion, "findById")
      .mockResolvedValue(mockConstructiveQuestion);

    // Mock Schedule.findById to return a mock schedule
    const mockSchedule = {
      _id: "schedule_id",
      constructiveQuestions: ["cq_id"],
      lessons: [
        {
          constructiveQuestions: [
            { _id: "cq_id", from: null, to: null, done: false },
          ],
        },
      ],
    };
    jest.spyOn(Schedule, "findById").mockResolvedValue(mockSchedule);

    // Mock ConstructiveQuestion.findByIdAndUpdate and Schedule.findByIdAndUpdate
    const updateCQMock = jest.fn().mockResolvedValue({
      ...mockConstructiveQuestion,
      from: new Date("2023-08-10T15:00:00Z"),
      to: new Date("2023-08-10T17:00:00Z"),
      done: true,
    });
    const updateScheduleMock = jest.fn().mockResolvedValue({
      ...mockSchedule,
      done: true,
    });
    jest
      .spyOn(ConstructiveQuestion, "findByIdAndUpdate")
      .mockImplementation(updateCQMock);
    jest
      .spyOn(Schedule, "findByIdAndUpdate")
      .mockImplementation(updateScheduleMock);

    const requestBody = {
      constructiveQuestionId: "cq_id",
      scheduleId: "schedule_id",
      from: "2023-08-10T15:00:00Z",
      to: "2023-08-10T17:00:00Z",
    };

    const response = await request(app)
      .patch("/constructiveQuestions/time")
      .send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Constructive question time updated successfully"
    );
  });

  // Add more test cases for error handling and different scenarios
});

describe("PATCH /constructiveQuestions", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should update constructive question", async () => {
    // Mock ConstructiveQuestion.findByIdAndUpdate to return a mock updated constructive question
    const mockUpdatedCQ = {
      _id: "cq_id",
      title: "Updated Title",
      content: "Updated Content",
      from: new Date("2023-08-10T10:00:00Z"),
      to: new Date("2023-08-10T12:00:00Z"),
      createdBy: "user_id",
      displaySetting: "updated_setting",
      voteSetting: "updated_setting",
    };
    jest
      .spyOn(ConstructiveQuestion, "findByIdAndUpdate")
      .mockResolvedValue(mockUpdatedCQ);

    const requestBody = {
      constructiveQuestionId: "cq_id",
      updates: {
        title: "Updated Title",
        content: "Updated Content",
        from: "2023-08-10T10:00:00Z",
        to: "2023-08-10T12:00:00Z",
        createdBy: "user_id",
        displaySetting: "updated_setting",
        voteSetting: "updated_setting",
      },
    };

    const response = await request(app)
      .patch("/constructiveQuestions")
      .send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Constructive question updated successfully"
    );
  });

  // Add more test cases for error handling and different scenarios
});

describe("DELETE /constructiveQuestions", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should delete constructive question by ID", async () => {
    // Mock ConstructiveQuestion.findByIdAndDelete to resolve successfully
    jest
      .spyOn(ConstructiveQuestion, "findByIdAndDelete")
      .mockResolvedValue(null);

    const requestBody = {
      id: "cq_id",
    };

    const response = await request(app)
      .delete("/constructiveQuestions")
      .send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Constructive question delete successfully"
    );
  });

  it("should handle error during deletion", async () => {
    // Mock ConstructiveQuestion.findByIdAndDelete to throw an error
    jest
      .spyOn(ConstructiveQuestion, "findByIdAndDelete")
      .mockImplementation(() => {
        throw new Error("Mock Error during deletion");
      });

    const requestBody = {
      id: "cq_id",
    };

    const response = await request(app)
      .delete("/constructiveQuestions")
      .send(requestBody);

    expect(response.status).toBe(500);
    expect(response.body.message).toBe(
      "Failed to delete constructive question"
    );
  });

  it("should handle missing required field", async () => {
    const response = await request(app)
      .delete("/constructiveQuestions")
      .send({}); // Sending an empty request body to simulate missing required field

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required Field !");
  });

  // Add more test cases as needed
});

describe("GET /constructiveQuestions/:id", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get constructive question by ID", async () => {
    // Mock ConstructiveQuestion.findById to return a mock constructive question
    const mockCQ = {
      _id: "cq_id",
      title: "Sample Question",
      content: "Sample Content",
      // Add more fields as needed
    };
    jest.spyOn(ConstructiveQuestion, "findById").mockResolvedValue(mockCQ);

    const response = await request(app)
      .post("/constructiveQuestions/getCQ")
      .send({ id: "cq_id" });

    expect(response.status).toBe(200);
    expect(response.body).toEqual(mockCQ);
    // Add your assertions here based on the expected behavior
    // You can check response.body properties to match the mockCQ
  });

  it("should handle constructive question not found", async () => {
    // Mock ConstructiveQuestion.findById to return null (not found)
    jest.spyOn(ConstructiveQuestion, "findById").mockResolvedValue(null);

    const response = await request(app)
      .post("/constructiveQuestions/getCQ")
      .send({ id: "Non_existed_id" });

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Constructive Question not found !");
  });

  it("should handle missing required field", async () => {
    const response = await request(app)
      .post("/constructiveQuestions/getCQ")
      .send();

    expect(response.status).toBe(412);
    expect(response.body.message).toBe("Missing required Field !");
  });

  // Add more test cases as needed
});
