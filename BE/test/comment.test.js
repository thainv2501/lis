const request = require("supertest");
const app = require("../server"); // Replace with the path to your Express app
const Comment = require("../models/Comment"); // Import the Comment model
const Reply = require("../models/Reply");
const Vote = require("../models/Vote");

// Mock the verifyJWT middleware
jest.mock("../middleware/verifyJWT", () => (req, res, next) => {
  req.userLogin = {
    _id: "user_id",
    roles: ["Admin"],
  };

  if (req.mockedRoles === "empty") {
    req.userLogin = {
      _id: "user_id",
      roles: [],
    };
  }
  if (req.mockedRoles === "undefined") {
    req.userLogin = undefined;
  }
  next();
});

describe("POST /comments", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should create a new comment", async () => {
    const requestBody = {
      scheduleId: "schedule_id",
      CQId: "cq_id",
      createdBy: "user_id",
      content: "Sample Comment Content",
    };

    // Mock Comment.save to return a mock created comment
    const mockCreatedComment = {
      _id: "comment_id",
      schedule: "schedule_id",
      constructiveQuestion: "cq_id",
      createdBy: "user_id",
      content: "Sample Comment Content",
      // Add more fields as needed
    };
    jest.spyOn(Comment.prototype, "save").mockResolvedValue(mockCreatedComment);

    const response = await request(app).post("/comments").send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("comment created successfully");
  });

  it("should handle missing required field", async () => {
    const requestBody = {
      // Missing required fields
    };

    const response = await request(app).post("/comments").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe("Missing required field");
  });

  // Add more test cases as needed
});

describe("POST /comments/filter", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get comments inside the group", async () => {
    const requestBody = {
      scheduleId: "schedule_id",
      CQId: "cq_id",
      filter: "inside-group",
      members: [{ _id: "member_id1" }, { _id: "member_id2" }],
      page: 1,
    };

    // Mock Comment.find to return an array of mock comments
    const mockComments = [
      {
        _id: "comment_id1",
        schedule: "schedule_id",
        constructiveQuestion: "cq_id",
        createdBy: "member_id1",
      },
      {
        _id: "comment_id2",
        schedule: "schedule_id",
        constructiveQuestion: "cq_id",
        createdBy: "member_id2",
      },
    ];
    jest.spyOn(Comment, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      skip: jest.fn().mockReturnThis(),
      exec: jest.fn().mockResolvedValue(mockComments),
    });
    jest
      .spyOn(Comment, "countDocuments")
      .mockResolvedValue(mockComments.length);

    const response = await request(app)
      .post("/comments/filter")
      .send(requestBody);

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // You can check response.body.commentsCount and response.body.comments
  });

  // Add more test cases for other filters (e.g., "outside-group") and error cases
});

describe("POST /comments/all", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should get all comments for the specified schedule and CQ", async () => {
    const requestBody = {
      scheduleId: "schedule_id",
      CQId: "cq_id",
    };

    // Mock Comment.find to return an array of mock comments
    const mockComments = [
      {
        _id: "comment_id1",
        schedule: "schedule_id",
        constructiveQuestion: "cq_id",
        createdBy: "user_id1",
        // Add more fields as needed
      },
      {
        _id: "comment_id2",
        schedule: "schedule_id",
        constructiveQuestion: "cq_id",
        createdBy: "user_id2",
        // Add more fields as needed
      },
    ];
    jest.spyOn(Comment, "find").mockReturnValue({
      populate: jest.fn().mockReturnThis(),
      sort: jest.fn().mockReturnThis(),
      exec: jest.fn().mockResolvedValue(mockComments),
    });

    const response = await request(app).post("/comments/all").send(requestBody);

    expect(response.status).toBe(200);
    // Add your assertions here based on the expected behavior
    // You can check response.body for the array of comments
  });

  it("should handle errors when fetching comments", async () => {
    // Mock Comment.find to throw an error
    jest.spyOn(Comment, "find").mockImplementation(() => {
      throw new Error("Mock Error while fetching comments");
    });

    const requestBody = {
      scheduleId: "schedule_id",
      CQId: "cq_id",
    };

    const response = await request(app).post("/comments/all").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe(
      "Get Comments FailError: Mock Error while fetching comments"
    );
  });
});

describe("PATCH /comments", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should update the content of a comment", async () => {
    const requestBody = {
      commentId: "comment_id",
      content: "Updated content",
    };

    // Mock Comment.findByIdAndUpdate to return the updated comment
    const updatedComment = {
      _id: "comment_id",
      content: "Updated content",
    };
    jest.spyOn(Comment, "findByIdAndUpdate").mockReturnValue(updatedComment);

    const response = await request(app).patch("/comments").send(requestBody);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe("update success !");
    // Add your assertions here based on the expected behavior
    // You can check response.body for the updated comment
  });

  it("should handle errors when updating a comment", async () => {
    // Mock Comment.findByIdAndUpdate to throw an error
    jest.spyOn(Comment, "findByIdAndUpdate").mockImplementation(() => {
      throw new Error("Mock Error while updating comment");
    });

    const requestBody = {
      commentId: "comment_id",
      content: "Updated content",
    };

    const response = await request(app).patch("/comments").send(requestBody);

    expect(response.status).toBe(400);
    expect(response.body.message).toBe(
      "comment created FailError: Mock Error while updating comment"
    );
  });
});

describe("DELETE /comments", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should delete a comment and associated replies and votes", async () => {
    // Mock Comment.findById to return a comment with associated replies and votes
    const commentWithAssociations = {
      _id: "64d4d276b8b81b9b992efedc",
      reply: ["reply_id1", "reply_id2"], // IDs of associated replies
      votes: ["vote_id1", "vote_id2"], // IDs of associated votes
      // Add more fields as needed
    };
    jest.spyOn(Comment, "findById").mockReturnValue(commentWithAssociations);

    // Mock DeleteMany methods for Reply and Vote models
    jest.spyOn(Reply, "deleteMany").mockResolvedValue({});
    jest.spyOn(Vote, "deleteMany").mockResolvedValue({});

    const response = await request(app)
      .delete("/comments")
      .send({ id: "64d4d276b8b81b9b992efedc" }); // Replace with the actual comment ID

    console.log(response.body);

    expect(response.status).toBe(200);
    expect(response.body.message).toBe(
      "Comment and associated replies deleted successfully"
    );
    // Add your assertions here based on the expected behavior
  });

  it("should handle errors when deleting a comment", async () => {
    // Mock Comment.findById to throw an error
    jest.spyOn(Comment, "findById").mockImplementation(() => {
      throw new Error("Mock Error while finding comment");
    });

    const response = await request(app)
      .delete("/comments")
      .send({ id: "comment_id" }); // Replace with the actual comment ID

    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Server error");
  });
});
