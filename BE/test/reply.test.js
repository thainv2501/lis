const mongoose = require('mongoose');
const {
  createReply,
  deleteReply,
  updateReply,
  getAllReplyBySupportQuestionId,
} = require('../controllers/replyController');
const Reply = require('../models/Reply');
const SupportQuestion = require('../models/SupportQuestion');

jest.mock('../models/Reply');
jest.mock('../models/SupportQuestion');

describe('createReply', () => {
  it('should create a new reply for a support question', async () => {
    const req = {
      body: {
        content: 'Sample Reply Content',
        createdBy: new mongoose.Types.ObjectId(),
        id: new mongoose.Types.ObjectId(),
        isComment: false,
      },
    };

    // Create mock functions for res
    const statusMock = jest.fn();
    const jsonMock = jest.fn();
    const res = { status: statusMock, json: jsonMock };

    // Mock the parent object (SupportQuestion)
    const mockParentObject = {
      _id: req.body.id,
      reply: [], // Initialize with an empty array
      save: jest.fn(),
    };

    // Mock the saved reply
    const mockSavedReply = {
      _id: new mongoose.Types.ObjectId(),
      content: req.body.content,
      createdBy: req.body.createdBy,
    };

    // Mock the SupportQuestion.findById function
    SupportQuestion.findById.mockResolvedValue(mockParentObject);

    // Mock the new Reply instance and its save method
    Reply.mockReturnValueOnce({
      ...mockSavedReply,
      save: jest.fn().mockResolvedValue(mockSavedReply),
    });

    await createReply(req, res);

    expect(SupportQuestion.findById).toHaveBeenCalledWith(req.body.id);
    expect(Reply).toHaveBeenCalledWith({
      content: req.body.content,
      createdBy: req.body.createdBy,
    });
    expect(mockParentObject.reply).toContain(mockSavedReply._id);
    expect(mockParentObject.save).toHaveBeenCalled();
    expect(jsonMock).toHaveBeenCalledWith({
      message: 'Reply created successfully',
      reply: mockSavedReply,
    });
  });

  it('should handle non-existing parent object', async () => {
    const req = {
      body: {
        content: 'Sample Content',
        createdBy: new mongoose.Types.ObjectId(),
        id: new mongoose.Types.ObjectId(),
        isComment: false,
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    SupportQuestion.findById.mockResolvedValue(null);

    await createReply(req, res);

    expect(SupportQuestion.findById).toHaveBeenCalledWith(req.body.id);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      error: 'Support question object not found',
    });
  });
});

describe('deleteReply', () => {
  it('should delete a reply', async () => {
    const req = {
      body: {
        id: new mongoose.Types.ObjectId(),
      },
    };
    const res = {
      json: jest.fn(),
    };

    await deleteReply(req, res);

    expect(Reply.findByIdAndDelete).toHaveBeenCalledWith(req.body.id);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Reply deleted successfully',
    });
  });

  it('should handle missing required field', async () => {
    const req = {
      body: {},
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    await deleteReply(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Missing required field !',
    });
  });
});

describe('updateReply', () => {
  it('should update a reply', async () => {
    const req = {
      body: {
        replyId: new mongoose.Types.ObjectId(),
        content: 'Updated Content',
      },
    };

    const statusMock = jest.fn();
    const jsonMock = jest.fn();
    const res = { status: statusMock, json: jsonMock };

    const mockReply = {
      save: jest.fn().mockResolvedValue(true),
    };

    Reply.findById.mockResolvedValue(mockReply);

    await updateReply(req, res);

    expect(Reply.findById).toHaveBeenCalledWith(req.body.replyId);
    expect(mockReply.save).toHaveBeenCalled();
    expect(jsonMock).toHaveBeenCalledWith({ success: true });
  });

  it('should handle non-existing reply', async () => {
    const req = {
      body: {
        replyId: new mongoose.Types.ObjectId(),
        content: 'Updated Content',
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    Reply.findById.mockResolvedValue(null);

    await updateReply(req, res);

    expect(Reply.findById).toHaveBeenCalledWith(req.body.replyId);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      success: false,
      error: 'Reply not found',
    });
  });
});

describe('getAllReplyBySupportQuestionId', () => {
  it('should get all replies for a support question', async () => {
    const req = {
      body: {
        id: new mongoose.Types.ObjectId(),
        filter: 'latest',
        page: 1,
      },
    };
    const statusMock = jest.fn().mockReturnThis();
    const jsonMock = jest.fn();
    const res = { status: statusMock, json: jsonMock };

    const mockSupportQuestion = {
      _id: req.body.id,
      reply: [new mongoose.Types.ObjectId()],
    };

    const mockReplies = [
      {
        _id: mockSupportQuestion.reply[0],
        content: 'Sample Reply Content',
        createdBy: new mongoose.Types.ObjectId(),
        createdDate: new Date(),
      },
    ];

    SupportQuestion.findById.mockResolvedValue(mockSupportQuestion);

    const data = await getAllReplyBySupportQuestionId(req, res);
    if (data) {
      expect(SupportQuestion.findById).toHaveBeenCalledWith(req.body.id);
      expect(res.json).toHaveBeenCalledWith({
        totalRepliesCount: 1,
        replies: mockReplies,
      });
    } else {
      expect(res.json).toHaveBeenCalledWith({
        error: 'Server error',
      });
    }
  });

  it('should handle non-existing support question', async () => {
    const req = {
      body: {
        id: 'sp_id',
        filter: 'latest',
        page: 1,
      },
    };
    const res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };

    SupportQuestion.findById.mockResolvedValue(null);

    await getAllReplyBySupportQuestionId(req, res);

    expect(SupportQuestion.findById).toHaveBeenCalledWith(req.body.id);
    expect(res.status).toHaveBeenCalledWith(500);
  });
});
