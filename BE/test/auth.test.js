const request = require("supertest");
const app = require("../server"); // Assuming you have an Express app defined in app.js
const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mailer = require("../utils/mailer");

describe("Authentication API", () => {
  describe("POST /auth", () => {
    it("should return an access token when provided with valid credentials", async () => {
      // Mock dependencies
      jest.spyOn(User, "findOne").mockResolvedValueOnce({
        email: "user@example.com",
        password: await bcrypt.hash("password", 10),
        active: true,
        emailVerified: true,
      });
      jest.spyOn(bcrypt, "compare").mockResolvedValueOnce(true);
      jest.spyOn(jwt, "sign").mockReturnValueOnce("mocked-access-token");

      const response = await request(app)
        .post("/auth")
        .send({ email: "user@example.com", password: "password" });
      expect(response.status).toBe(200);
      expect(response.body.accessToken).toBe("mocked-access-token");
    });

    it("should return an error when email or password is missing", async () => {
      const response = await request(app).post("/auth").send({});

      expect(response.status).toBe(400);
      expect(response.body.message).toBe("All fields are required");
    });

    it("should return a 401 status if the user is not found", async () => {
      // Mock the User.findOne function to return null (user not found)
      User.findOne.mockResolvedValue(null);

      // Perform the API call using supertest
      const response = await request(app)
        .post("/auth")
        .send({ email: "nonexistent@example.com", password: "password" });

      // Assert the response
      expect(response.status).toBe(401);
      expect(response.body.message).toBe("Unauthorized");
    });

    it("should return a 401 status if the password is incorrect", async () => {
      // Mock the User.findOne function to return a user
      User.findOne.mockResolvedValue({
        email: "user@example.com",
        password: "hashedPassword",
        active: true,
        emailVerified: true,
      });

      // Mock the bcrypt.compare function to return false
      bcrypt.compare.mockResolvedValue(false);

      // Perform the API call using supertest
      const response = await request(app)
        .post("/auth")
        .send({ email: "user@example.com", password: "incorrectPassword" });

      // Assert the response
      expect(response.status).toBe(401);
      expect(response.body.message).toBe("Wrong email or password");
    });

    it("should return a 401 status if the email is not verified", async () => {
      // Mock the User.findOne function to return a user with email not verified
      User.findOne.mockResolvedValue({
        email: "user@example.com",
        password: "hashedPassword",
        active: true,
        emailVerified: false,
      });

      // Mock the bcrypt.compare function to return true
      bcrypt.compare.mockResolvedValue(true);

      // Perform the API call using supertest
      const response = await request(app)
        .post("/auth")
        .send({ email: "user@example.com", password: "password" });

      // Assert the response
      expect(response.status).toBe(401);
      expect(response.body.message).toBe(
        "Check your email and verify your email !"
      );
    });

    // Add more test cases for different scenarios
  });

  describe("GET auth/refresh", () => {
    test("should return a new access token when provided with a valid refresh token", async () => {
      // Create a mock user
      const mockUser = new User({
        email: "user@example.com",
        password: "password",
      });

      // Generate a refresh token
      const refreshToken = jwt.sign(
        { email: mockUser.email },
        process.env.REFRESH_TOKEN_SECRET,
        { expiresIn: "7d" }
      );

      // Make a request to the refresh endpoint with the refresh token
      const response = await request(app)
        .get("/auth/refresh")
        .set("Cookie", `jwt=${refreshToken}`)
        .expect(200);

      // Verify the response contains a new access token
      expect(response.body.accessToken).toBeDefined();
    });

    test("should return 401 Unauthorized if no refresh token is provided", async () => {
      // Make a request to the refresh endpoint without a refresh token
      const response = await request(app).get("/auth/refresh").expect(401);

      // Verify the response contains the expected error message
      expect(response.body.message).toBe("Unauthorized");
    });

    // Add more test cases as needed
  });

  describe("POST /auth/logout", () => {
    test("should clear the cookie and return a success message if a cookie exists", async () => {
      // Make a request to the logout endpoint
      const response = await request(app)
        .post("/auth/logout")
        .set("Cookie", "jwt=mocked-refresh-token")
        .expect(200);

      // Verify the response contains the expected success message
      expect(response.body.message).toBe("Cookie cleared");
    });

    test("should return 204 No Content if no cookie exists", async () => {
      // Make a request to the logout endpoint without a cookie
      const response = await request(app).post("/auth/logout").expect(204);

      // Verify the response does not contain any content
      expect(response.body).toEqual({});
    });
    // Add more test cases as needed
  });

  describe("POST /auth/register", () => {
    test("should create a new user and return a success message", async () => {
      // Mock request body
      const requestBody = {
        fullName: "John Doe",
        phoneNumber: "123456789",
        email: "john@example.com",
        password: "password",
        roles: ["user"],
      };

      // Mock the User.findOne method to return null (no duplicate email)
      User.findOne = jest.fn().mockResolvedValue(null);

      // Mock bcrypt.hash method to return a hashed password
      bcrypt.hash = jest.fn().mockResolvedValue("hashedPassword");

      // Mock the User.create function
      const mockUserCreate = jest.spyOn(User, "create");
      mockUserCreate.mockResolvedValue({}); // Mock the return value as needed

      // Make a request to the register endpoint
      const response = await request(app)
        .post("/auth/register")
        .send(requestBody)
        .expect(201);
      // Verify the response contains the expected success message
      expect(response.body.message).toBe(
        `New user ${requestBody.email} created`
      );
    });

    test("should return an error message if required fields are missing", async () => {
      // Mock request body with missing fields
      const requestBody = {
        fullName: "John Doe",
        phoneNumber: "123456789",
      };

      // Make a request to the register endpoint with missing fields
      const response = await request(app)
        .post("/auth/register")
        .send(requestBody)
        .expect(400);

      // Verify the response contains the expected error message
      expect(response.body.message).toBe("All fields are required");
    });

    test("should return an error message if a duplicate email is found", async () => {
      // Mock request body
      const requestBody = {
        fullName: "John Doe",
        phoneNumber: "123456789",
        email: "john@example.com",
        password: "password",
        roles: ["user"],
      };

      // Mock the User.findOne method to return a user object (duplicate email found)
      User.findOne = jest.fn().mockResolvedValue({ email: requestBody.email });

      // Make a request to the register endpoint
      const response = await request(app)
        .post("/auth/register")
        .send(requestBody)
        .expect(409);

      // Verify the response contains the expected error message
      expect(response.body.message).toBe("Duplicate emails");
    });

    // Add more test cases as needed
  });
});

describe("POST /auth/sendResetPassword", () => {
  it("should send reset password request email successfully", async () => {
    const email = "user@example.com";

    // Mock User.findOne to return a user
    const mockUser = {
      email,
    };
    jest.spyOn(User, "findOne").mockResolvedValue(mockUser);

    // Mock bcrypt.hash to resolve
    jest.spyOn(bcrypt, "hash").mockResolvedValue("hashedEmail");

    // Mock mailer.sendMail to resolve
    jest.spyOn(mailer, "sendMail").mockResolvedValue();

    // Send a POST request to the route
    const response = await request(app)
      .post("/auth/sendResetPassword")
      .send({ email });

    // Verify the response
    expect(response.status).toBe(200);
    expect(response.body).toEqual({
      message: "Send reset password request email successful!",
    });

    // Verify that User.findOne was called with the correct email
    expect(User.findOne).toHaveBeenCalledWith({ email });

    // Verify that bcrypt.hash was called with the correct email and salt rounds
    expect(bcrypt.hash).toHaveBeenCalledWith(email, 10);

    // Verify that mailer.sendMail was called with the correct email and hashedEmail
    expect(mailer.sendMail).toHaveBeenCalledWith(
      email,
      "Request reset Password from LIS",
      expect.any(String)
    );
  });

  it("should return an error if email is missing", async () => {
    // Send a POST request without an email
    const response = await request(app)
      .post("/auth/sendResetPassword")
      .send({});

    // Verify the response
    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: "All fields are required" });
  });

  it("should return an error if no user with the given email exists", async () => {
    const email = "nonexistent@example.com";

    // Mock User.findOne to return null
    jest.spyOn(User, "findOne").mockResolvedValue(null);

    // Send a POST request with a nonexistent email
    const response = await request(app)
      .post("/auth/sendResetPassword")
      .send({ email });

    // Verify the response
    expect(response.status).toBe(409);
    expect(response.body).toEqual({ message: "No existing email" });

    // Verify that User.findOne was called with the correct email
    expect(User.findOne).toHaveBeenCalledWith({ email });
  });

  it("should return an error if an error occurs while hashing the email", async () => {
    const email = "user@example.com";

    // Mock User.findOne to return a user
    const mockUser = {
      email,
    };
    jest.spyOn(User, "findOne").mockResolvedValue(mockUser);

    // Mock bcrypt.hash to reject
    jest.spyOn(bcrypt, "hash").mockRejectedValue(new Error("Hashing error"));

    // Send a POST request to the route
    const response = await request(app)
      .post("/auth/sendResetPassword")
      .send({ email });

    // Verify the response
    expect(response.status).toBe(400);
    expect(response.body).toEqual({
      message: "An error occurred while hashing email",
    });

    // Verify that User.findOne was called with the correct email
    expect(User.findOne).toHaveBeenCalledWith({ email });

    // Verify that bcrypt.hash was called with the correct email and salt rounds
    expect(bcrypt.hash).toHaveBeenCalledWith(email, 10);
  });
});

describe("POST /auth/verifyEmail", () => {
  it("should return status 200 and success message if email and token match", async () => {
    // Mock User.findOne to return a user
    const mockUser = { _id: "user_id", email: "test@example.com" };
    jest.spyOn(User, "findOne").mockResolvedValue(mockUser);

    // Mock bcrypt.compare to return true
    jest.spyOn(bcrypt, "compare").mockResolvedValue(true);

    // Mock User.updateOne
    jest.spyOn(User, "updateOne").mockResolvedValue();

    // Send a request to the endpoint
    const response = await request(app)
      .post("/auth/verifyEmail")
      .send({ email: "test@example.com", token: "token" });

    // Verify the response
    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: "verify email successful !" });
  });

  it("should return status 400 if user is not found", async () => {
    // Mock User.findOne to return null
    jest.spyOn(User, "findOne").mockResolvedValue(null);

    // Send a request to the endpoint
    const response = await request(app)
      .post("/auth/verifyEmail")
      .send({ email: "test@example.com", token: "token" });

    // Verify the response
    expect(response.status).toBe(400);
    expect(response.body).toEqual({ message: "User not found" });
  });

  it("should return status 400 if email and token do not match", async () => {
    // Mock User.findOne to return a user
    const mockUser = { _id: "user_id", email: "test@example.com" };
    jest.spyOn(User, "findOne").mockResolvedValue(mockUser);

    // Mock bcrypt.compare to return false
    jest.spyOn(bcrypt, "compare").mockResolvedValue(false);

    // Send a request to the endpoint
    const response = await request(app)
      .post("/auth/verifyEmail")
      .send({ email: "test@example.com", token: "token" });

    // Verify the response
    expect(response.status).toBe(400);
    expect(response.body).toEqual({
      message: "there are something wrong with email and token !",
    });
  });

  it("should return status 500 if an error occurs during user update", async () => {
    // Mock User.findOne to return a user
    const mockUser = { _id: "user_id", email: "test@example.com" };
    jest.spyOn(User, "findOne").mockResolvedValue(mockUser);

    // Mock bcrypt.compare to return true
    jest.spyOn(bcrypt, "compare").mockResolvedValue(true);

    // Mock User.updateOne to throw an error
    const error = new Error("Update error");
    jest.spyOn(User, "updateOne").mockRejectedValue(error);

    // Send a request to the endpoint
    const response = await request(app)
      .post("/auth/verifyEmail")
      .send({ email: "test@example.com", token: "token" });

    // Verify the response
    expect(response.status).toBe(500);
    expect(response.body).toEqual({ message: "verify email fail !" });
  });
});

describe("PATCH /auth/resetPassword", () => {
  it("should update the user password", async () => {
    const email = "user@example.com";
    const password = "newPassword";

    // Mock the User.findOne function to return a user
    User.findOne.mockResolvedValue({
      _id: "user_id",
      email: "user@example.com",
    });

    // Mock the User.updateOne function to resolve successfully
    User.updateOne.mockResolvedValue();

    // Mock the bcrypt.hash function to return the password directly without hashing
    bcrypt.hash.mockResolvedValue(password);

    // Perform the API call using supertest
    const response = await request(app)
      .patch("/auth/resetPassword")
      .send({ email, password });

    // Assert the response
    expect(response.status).toBe(200);
    expect(response.body.message).toBe(`Password for ${email} updated`);

    // Assert that User.findOne was called with the provided email
    expect(User.findOne).toHaveBeenCalledWith({ email });

    // Assert that User.updateOne was called with the user's ID and the hashed password
    expect(User.updateOne).toHaveBeenCalledWith(
      { _id: "user_id" },
      { password: password }
    );
  });

  it("should return a 400 status if email or password is missing", async () => {
    // Perform the API call with missing fields using supertest
    const response = await request(app).patch("/auth/resetPassword").send({});

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("All fields are required");
  });

  it("should return a 400 status if the user is not found", async () => {
    // Mock the User.findOne function to return null (user not found)
    User.findOne.mockResolvedValue(null);

    // Perform the API call using supertest
    const response = await request(app)
      .patch("/auth/resetPassword")
      .send({ email: "nonexistent@example.com", password: "newPassword" });

    // Assert the response
    expect(response.status).toBe(400);
    expect(response.body.message).toBe("User not found");
  });

  it("should return a 500 status if there is an error while updating the password", async () => {
    // Mock the User.findOne function to return a user
    User.findOne.mockResolvedValue({
      _id: "user_id",
      email: "user@example.com",
    });

    // Mock the User.updateOne function to throw an error
    User.updateOne.mockRejectedValue(new Error("Database error"));

    // Perform the API call using supertest
    const response = await request(app)
      .patch("/auth/resetPassword")
      .send({ email: "user@example.com", password: "newPassword" });

    // Assert the response
    expect(response.status).toBe(500);
    expect(response.body.message).toBe("Reset password failed");
  });
});
