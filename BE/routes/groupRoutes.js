const express = require("express");
const router = express.Router();
const groupController = require("../controllers/groupController");

router
  .route("/")
  .get(groupController.getGroupOfStudentId)
  .post(groupController.createGroups);
router.route("/:scheduleId").get(groupController.getGroupsByScheduleId);
router.route("/move").patch(groupController.moveStudentToGroup);
router.route("/remove").patch(groupController.removeStudentOutGroup);
router.route("/combine").post(groupController.combineNewGroup);
module.exports = router;
