const express = require('express');
const router = express.Router();
const scheduleController = require('../controllers/scheduleController');
const verifyJWT = require('../middleware/verifyJWT');

router.use(verifyJWT);

router.route('/:scheduleId').get(scheduleController.getScheduleById);

router
  .route('/')
  .get(scheduleController.getSchedules)
  .post(scheduleController.createNewSchedule)
  .patch(scheduleController.updateSchedule);

router.route('/XLSX').post(scheduleController.importSchedulesFromXLSX);
router
  .route('/scheduleFilter')
  .post(scheduleController.filterSchedulesByTabAndSemester);

router
  .route('/classObj/:classId')
  .get(scheduleController.getSchedulesByClassId);

module.exports = router;
