const express = require("express");
const router = express.Router();
const authController = require("../controllers/authController");
const loginLimiter = require("../middleware/loginLimiter");

router.route("/").post(loginLimiter, authController.login);

router.route("/refresh").get(authController.refresh);

router.route("/logout").post(authController.logout);

router.route("/register").post(authController.register);

router.route("/verifyEmail").post(authController.verifyEmail);

router.route("/sendResetPassword").post(authController.sendResetPassword);

router.route("/resetPassword").patch(authController.resetPassword);

module.exports = router;
