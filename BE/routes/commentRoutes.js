const express = require("express");
const router = express.Router();
const commentController = require("../controllers/commentController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
  .route("/")
  .post(commentController.createComment)
  .patch(commentController.updateComment)
  .delete(commentController.deleteComment);
router.route("/filter").post(commentController.getCommentsWithFilter);
router.route("/all").post(commentController.getAllComments);

module.exports = router;
