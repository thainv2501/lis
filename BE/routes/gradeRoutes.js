const express = require("express");
const router = express.Router();
const gradeController = require("../controllers/gradeController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router.route("/").post(gradeController.getGrade);
router.route("/create").post(gradeController.createGrade);

module.exports = router;
