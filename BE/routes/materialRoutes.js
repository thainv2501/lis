const express = require("express");
const router = express.Router();
const materialController = require("../controllers/materialController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
  .route("/")
  .post(materialController.createNewMaterial)
  .patch(materialController.updateMaterial)
  .delete(materialController.deleteMaterial);

module.exports = router;
