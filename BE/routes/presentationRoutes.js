const express = require("express");
const router = express.Router();
const presentationController = require("../controllers/presentationController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router.route("/").post(presentationController.createPresentation);
router.route("/get").post(presentationController.getPresentations);
router.route("/restart").patch(presentationController.restartPresentation);
module.exports = router;
