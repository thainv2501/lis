const express = require("express");
const router = express.Router();
const lessonController = require("../controllers/lessonsController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
  .route("/")
  .post(lessonController.createLesson)
  .patch(lessonController.updateLesson);

router.route("/:subjectId").get(lessonController.getLessonWithSearch);
router.route("/lesson-details/:lessonId").get(lessonController.getLessonById);

router.route("/add-lessons").post(lessonController.importFromXlSX);
router.route("/CQ").delete(lessonController.deleteCQInLessonByCQId);

module.exports = router;
