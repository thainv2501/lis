const express = require("express");
const router = express.Router();
const masterDataController = require("../controllers/masterDataController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
  .route("/")
  .get(masterDataController.getMasterDataWithSearch)
  .post(masterDataController.createMasterData)
  .patch(masterDataController.updateMaterData)
  .delete(masterDataController.deleteMasterDataByKeyword);

router.route("/:type").get(masterDataController.getMasterDataWithType);

module.exports = router;
