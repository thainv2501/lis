const express = require('express');
const router = express.Router();
const replyController = require('../controllers/replyController');
const verifyJWT = require('../middleware/verifyJWT');

router.use(verifyJWT);

router
  .route('/')
  .post(replyController.createReply)
  .patch(replyController.updateReply)
  .delete(replyController.deleteReply);

router.route('/reply').post(replyController.getAllReplyBySupportQuestionId);

module.exports = router;
