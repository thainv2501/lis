const express = require("express");
const router = express.Router();
const classController = require("../controllers/classController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
  .route("/")
  .get(classController.getClassBySearchQuery)
  .post(classController.createClass)
  .patch(classController.updateClass);

router.route("/import").post(classController.importClasses);
router.route("/add-students").post(classController.addStudentsToClass);
router.route("/add-student").post(classController.addStudentToClass);
router.route("/remove-student").post(classController.removeStudentOutClass);
router.route("/:id").get(classController.getClassById);
router.route("/semester/:semesterId").get(classController.getClassBySemesterId);

module.exports = router;
