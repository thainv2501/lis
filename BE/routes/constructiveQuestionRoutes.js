const express = require("express");
const router = express.Router();
const constructiveQuestionController = require("../controllers/constructiveQuestionController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
  .route("/")
  .post(constructiveQuestionController.createConstructiveQuestion)
  .patch(constructiveQuestionController.updateConstructiveQuestion)
  .delete(constructiveQuestionController.deleteCQById);

router.route("/getCQ").post(constructiveQuestionController.getCQById);
router
  .route("/time")
  .patch(constructiveQuestionController.updateConstructiveQuestionTime);

module.exports = router;
