const express = require("express");
const router = express.Router();
const voteController = require("../controllers/voteController");
const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router.route("/").post(voteController.vote);
router.route("/remain").post(voteController.getRemainVotesOfUser);

module.exports = router;
