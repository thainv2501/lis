const express = require("express");
const router = express.Router();
const meetingController = require("../controllers/meetingController");

const verifyJWT = require("../middleware/verifyJWT");

router.use(verifyJWT);

router
  .route("/")
  .post(meetingController.createMeeting)
  .patch(meetingController.updateMeeting)
  .delete(meetingController.deleteMeeting);
router.route("/:meetingId").get(meetingController.getMeetingById);
router.route("/class/:classId").get(meetingController.getMeetingsByClassId);
router.route("/meetingSearch").post(meetingController.getMeetingsWithSearch);
router.route("/restart").patch(meetingController.updateMeetingTime);
module.exports = router;
