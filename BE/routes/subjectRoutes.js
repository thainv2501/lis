const express = require('express');
const router = express.Router();
const subjectController = require('../controllers/subjectController');

const verifyJWT = require('../middleware/verifyJWT');

router.use(verifyJWT);

router
  .route('/')
  .get(subjectController.getSubjectsWithSearch)
  .post(subjectController.createSubject)
  .patch(subjectController.updateSubject)
  .delete(subjectController.deleteSubject);
router.route('/:id').get(subjectController.getSubjectById);
router.route('/allSubjects/all').get(subjectController.getSubjects);
module.exports = router;
