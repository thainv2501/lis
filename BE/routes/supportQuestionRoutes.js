const express = require('express');
const router = express.Router();
const supportQuestionController = require('../controllers/supportQuestionController');
const verifyJWT = require('../middleware/verifyJWT');

router.use(verifyJWT);

router
  .route('/')
  .post(supportQuestionController.createSupportQuestion)
  .patch(supportQuestionController.updateSupportQuestion)
  .delete(supportQuestionController.deleteSupportQuestion);
router
  .route('/status')
  .patch(supportQuestionController.updateSupportQuestionStatus);

router
  .route('/supportQuestion')
  .post(supportQuestionController.getSupportQuestions);
router
  .route('/subjectQuestion')
  .post(supportQuestionController.getSubjectQuestions);

router.route('/:id').get(supportQuestionController.getSupportQuestionById);
module.exports = router;
