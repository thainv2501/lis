const allowedOrigins = ["http://localhost:3000", "https://edu-lis.com"];

module.exports = allowedOrigins;
