const Meeting = require("../models/Meeting");
const Class = require("../models/Class");
const dayjs = require("dayjs");

// @desc Get all meetings with search condition
// @route GET / meetings
// @access Private
const getMeetingsWithSearch = async (req, res) => {
  const { page, tab } = req.body;
  const { userLogin } = req;
  console.log(page, tab);
  const limit = 8;
  // Get all subjects from MongoDB
  // validate search information
  // let tabFilter, semesterFilter;
  let totalMeetingsCount = 0;
  const now = dayjs();
  const sortQuery = { meetingFrom: 1 };

  let filterCondition = {};
  switch (tab) {
    case "all":
      filterCondition = {};
      break;
    case "joinNow":
      filterCondition = {
        $expr: {
          $and: [
            { $lte: ["$meetingFrom", now.toDate()] },
            { $gte: ["$meetingTo", now.toDate()] },
            { $eq: ["$status", true] },
          ],
        },
      };
      break;
    case "ended":
      filterCondition = {
        $expr: {
          $and: [
            { $lt: ["$meetingTo", now.toDate()] },
            { $eq: ["$status", true] },
          ],
        },
      };
      break;
    case "upcoming":
      filterCondition = {
        $expr: {
          $and: [
            { $gt: ["$meetingFrom", now.toDate()] },
            { $eq: ["$status", true] },
          ],
        },
      };
      break;
    case "cancelled":
      filterCondition = {
        status: false,
      };
      break;
    default:
      // 'All' or other cases
      return res.status(400).json({ error: "Invalid tab value" });
  }

  if (userLogin.roles.includes("Admin")) {
    // If the user is an Admin, get all meetings
    // No additional filtering required
  } else {
    if (
      userLogin.roles.includes("Trainer") &&
      userLogin.roles.includes("Trainee")
    ) {
      // If the user has both Trainer and Trainee roles
      // Combine filtering conditions for both Trainer and Trainee roles
      const classesWithTrainer = await Class.find({
        trainer: userLogin._id,
      }).select("_id");

      const classIdsWithTrainer = classesWithTrainer.map(
        (classObject) => classObject._id
      );

      if (!filterCondition.$or) {
        filterCondition.$or = [];
      }

      // Combine conditions for Trainer role
      filterCondition.$or.push({
        createdBy: userLogin._id,
      });
      filterCondition.$or.push({
        class: { $in: classIdsWithTrainer },
      });

      // Combine conditions for Trainee role
      filterCondition.$or.push({
        meetingType: "video-conference",
        invitedUsers: userLogin._id,
      });
      filterCondition.$or.push({
        meetingType: "anyone-can-join",
      });
    } else {
      // If the user has only one role
      if (userLogin.roles.includes("Trainer")) {
        const classesWithTrainer = await Class.find({
          trainer: userLogin._id,
        }).select("_id");

        const classIdsWithTrainer = classesWithTrainer.map(
          (classObject) => classObject._id
        );

        if (!filterCondition.$or) {
          filterCondition.$or = [];
        }

        filterCondition.$or.push({
          createdBy: userLogin._id,
        });
        filterCondition.$or.push({
          class: { $in: classIdsWithTrainer },
        });
      }

      if (userLogin.roles.includes("Trainee")) {
        if (!filterCondition.$or) {
          filterCondition.$or = [];
        }

        filterCondition.$or.push({
          meetingType: "video-conference",
          invitedUsers: userLogin._id,
        });
        filterCondition.$or.push({
          meetingType: "anyone-can-join",
        });
      }
    }
  }

  // Count total meetings
  totalMeetingsCount = await Meeting.countDocuments(filterCondition);

  // Get meetings for the current page
  const meetings = await Meeting.find(filterCondition)
    .sort(sortQuery)
    .populate("class")
    .populate({
      path: "class",
      populate: {
        path: "subject",
        model: "Subject",
      },
    })
    .skip((page - 1) * limit)
    .limit(limit)
    .exec();
  console.log(meetings);
  res.json({ totalMeetingsCount, meetings });
};

// Get all meetings
const getMeetings = async (req, res) => {
  try {
    const meetings = await Meeting.find();
    res.json(meetings);
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

// Create a new meeting
const createMeeting = async (req, res) => {
  try {
    const {
      createdBy,
      meetingId,
      meetingName,
      meetingType,
      invitedUsers,
      meetingFrom,
      meetingTo,
      classId,
      maxUsers,
    } = req.body;

    const existingMeeting = await Meeting.findOne({ meetingId: meetingId });

    if (existingMeeting) {
      return res.status(412).json({
        message: "A meeting with the same meetingId already exists",
      });
    }

    const meeting = await Meeting.create({
      createdBy,
      meetingId,
      meetingName,
      meetingType,
      invitedUsers,
      meetingFrom,
      meetingTo,
      maxUsers,
      class: classId,
      status: true,
    });

    res.status(201).json({ success: true, data: meeting });
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

// Update an existing meeting
const updateMeeting = async (req, res) => {
  try {
    const {
      meetingId,
      meetingName,
      invitedUsers,
      meetingFrom,
      meetingTo,
      maxUsers,
      status,
    } = req.body;

    const meeting = await Meeting.findOneAndUpdate(
      { meetingId },
      {
        $set: {
          meetingName,
          invitedUsers,
          meetingFrom,
          meetingTo,
          maxUsers,
          status,
        },
      },
      { new: true }
    );

    if (!meeting) {
      return res
        .status(404)
        .json({ success: false, error: "Meeting not found" });
    }

    res.status(200).json({ success: true, data: meeting });
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

// Get a meeting by ID
const getMeetingById = async (req, res) => {
  try {
    const { meetingId } = req.params;
    const meeting = await Meeting.findOne({ meetingId: meetingId })
      .populate("class")
      .populate({
        path: "class",
        populate: {
          path: "subject",
          model: "Subject",
        },
      })
      .populate({
        path: "class",
        populate: {
          path: "students",
          model: "User",
        },
      })
      .populate({
        path: "class",
        populate: {
          path: "semester",
          model: "MasterData",
        },
      })
      .lean();

    if (!meeting) {
      return res
        .status(404)
        .json({ success: false, error: "Meeting not found" });
    }

    res.json(meeting);
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

const getMeetingsByClassId = async (req, res) => {
  const { classId } = req.params;

  try {
    const meetings = await Meeting.find({
      class: classId,
      status: true,
    })
      .sort({ meetingFrom: 1 })
      .populate({ path: "createdBy", model: "User" });

    if (meetings && meetings.lenght === 0) {
      return res
        .status(404)
        .json({ error: "No meetings found for the given classId" });
    }

    res.status(200).json(meetings);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

const deleteMeeting = async (req, res) => {
  try {
    const { id } = req.body;

    if (!id) {
      return res.status(412).json({ message: "Missing required fields" });
    }
    // Tìm câu hỏi hỗ trợ cần xoá trong CSDL
    await Meeting.findByIdAndDelete(id);
    return res.status(201).json({
      message: "Meeting delete successfully",
    });
    // Trả về kết quả xoá thành công
    res.status(200).json({ success: true });
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

const updateMeetingTime = async (req, res) => {
  try {
    const { meetingId, meetingFrom, meetingTo } = req.body;
    console.log(meetingId, meetingFrom, meetingTo, "méo");
    const updatedMeeting = await Meeting.findOneAndUpdate(
      { meetingId },
      {
        $set: {
          meetingFrom,
          meetingTo,
        },
      },
      { new: true }
    );

    if (!updatedMeeting) {
      return res.status(404).json({
        success: false,
        error: "Meeting not found",
      });
    }

    res.status(200).json({ success: true, data: updatedMeeting });
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

// const updateMeetingCreatedDate = async (req, res) => {
//   await Meeting.updateMany({}, { createdAt: new Date() });
//   res.json({ message: "done" });
// };

module.exports = {
  getMeetingsWithSearch,
  getMeetings,
  createMeeting,
  updateMeeting,
  getMeetingById,
  getMeetingsByClassId,
  deleteMeeting,
  updateMeetingTime,
  // updateMeetingCreatedDate,
};
