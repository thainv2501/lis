const dayjs = require('dayjs');
const Schedule = require('../models/Schedule');
const Lesson = require('../models/Lesson');
const User = require('../models/User');
const Class = require('../models/Class');
const Subject = require('../models/Subject');
const mongoose = require('mongoose');

// @access Private
// method POST /schedules
const createNewSchedule = async (req, res) => {
  const {
    inClass,
    day,
    slotInDay,
    trainer,
    createdBy,
    content,
    lessons,
    constructiveQuestions,
    materials,
    done,
  } = req.body;

  if (!inClass || !day || !slotInDay || !trainer || !createdBy) {
    return res.status(412).json({ message: 'Missing required fields' });
  }

  try {
    // check duplicate schedule
    const duplicate = await Schedule.findOne({ inClass, day, slotInDay });
    if (duplicate) {
      return res.status(400).json({ message: 'Schedule Duplicate' });
    }
    const newSchedule = new Schedule({
      inClass,
      day,
      slotInDay,
      trainer,
      content,
      lessons,
      constructiveQuestions,
      materials,
      createdBy,
      done,
    });
    const createdSchedule = await newSchedule.save();
    return res.status(201).json({
      message: 'Schedule Created successfully',
      createdSchedule,
    });
  } catch (error) {
    console.log(error);
    return res.status(400).json({ message: 'Schedule Created fail' });
  }
};

// @access Private
// method POST /schedules/XLSX
const importSchedulesFromXLSX = async (req, res) => {
  const {
    inClass,
    createdBy,
    content,
    constructiveQuestions,
    materials,
    schedules,
  } = req.body;

  if (!inClass || !schedules) {
    return res.status(412).json({ message: 'Missing required fields' });
  }

  try {
    await Promise.all(
      schedules.map(async (schedule) => {
        //check valid date
        if (
          dayjs(schedule.day).isBefore(dayjs(inClass.semester.from)) ||
          dayjs(schedule.day).isAfter(dayjs(inClass.semester.to))
        ) {
          console.log('invalid date');
          return;
        }
        // check duplicate schedule
        const duplicate = await Schedule.findOne({
          inClass,
          day: schedule.day,
          slotInDay: schedule.slotInDay,
        });
        if (duplicate) {
          console.log('Duplicate Schedule');
          // await Schedule.findByIdAndUpdate(duplicate?._id,{lessons})
          return;
        } else {
          const lessons = await Lesson.find({
            subject: inClass.subject._id,
            lessonNumber: { $in: schedule.lessons },
          })
            .populate({ path: 'createdBy', model: 'User' })
            .populate('subject')
            .populate({
              path: 'materials',
              model: 'Material',
              populate: { path: 'uploadedBy', model: 'User' },
            })
            .populate({
              path: 'constructiveQuestions',
              model: 'ConstructiveQuestion',
              populate: { path: 'createdBy', model: 'User' },
            })
            .lean();

          if (lessons.length === 0) {
            return;
          } else {
            const newSchedule = new Schedule({
              inClass,
              day: schedule.day,
              slotInDay: schedule.slotInDay,
              trainer: inClass.trainer,
              content,
              lessons,
              constructiveQuestions,
              materials,
              createdBy,
            });
            const createdSchedule = await newSchedule.save();
            console.log('created new schedule');
          }
        }
      })
    );
    return res
      .status(200)
      .json({ message: 'Import Schedules from XLSX is successful' });
  } catch (error) {
    console.log(error);
    return res.status(400).json({ message: 'Schedule Created fail' });
  }
};

// @access Private
// method GET /schedules
const getSchedules = async (req, res) => {
  const { classId, page } = req.query;
  const { userLogin } = req;
  const limit = 10;

  // Check if classId is provided
  if (!classId) {
    return res.status(412).json({ message: 'Missing required fields' });
  }

  try {
    // Check if the user is an Admin
    if (userLogin && userLogin.roles.includes('Admin')) {
      // If the user is an Admin, allow access to all schedules for the class
      const schedules = await Schedule.find({ inClass: classId })
        .sort({
          day: 1,
          slotInDay: 1,
          'lessons.lessonChapter': 1,
          'lessons.lessonNumber': 1,
        })
        .populate({ path: 'createdBy', model: 'User' })
        .populate({
          path: 'constructiveQuestions',
          model: 'ConstructiveQuestion',
        })
        .limit(limit)
        .skip((parseInt(page) - 1) * limit)
        .lean();
      const schedulesCount = await Schedule.countDocuments({
        inClass: classId,
      }).exec();
      if (schedules.length === 0) {
        return res.status(200).json({ schedules: [], schedulesCount: 0 });
      }
      return res.status(200).json({ schedules, schedulesCount });
    }

    // Check if the user is a Trainer of the class or a Student in the class
    const isTrainerOrStudent = await Class.exists({
      _id: classId,
      $or: [{ trainer: userLogin?._id }, { students: userLogin?._id }],
    });

    if (!isTrainerOrStudent) {
      // If the user is neither an Admin, Trainer, nor Student in the class, deny access
      return res.status(403).json({ message: 'Unauthorized' });
    }

    // If the user is a Trainer or Student in the class, allow access to schedules
    const schedules = await Schedule.find({ inClass: classId })
      .sort({
        day: 1,
        slotInDay: 1,
        'lessons.lessonChapter': 1,
        'lessons.lessonNumber': 1,
      })
      .populate({ path: 'createdBy', model: 'User' })
      .populate({
        path: 'constructiveQuestions',
        model: 'ConstructiveQuestion',
      })
      .limit(limit)
      .skip((parseInt(page) - 1) * limit)
      .lean();
    const schedulesCount = await Schedule.countDocuments({
      inClass: classId,
    }).exec();

    if (schedules.length === 0) {
      return res.status(200).json({ schedules: [], schedulesCount: 0 });
    }
    return res.status(200).json({ schedules, schedulesCount });
  } catch (error) {
    console.log(error);
    return res.status(400).json({ message: 'Find Schedule error ' + error });
  }
};

// @access Private
// method GET /schedules/ScheduleId
const getScheduleById = async (req, res) => {
  const { scheduleId } = req.params;
  const { userLogin } = req;

  if (!scheduleId) {
    return res.status(412).json({ message: 'Missing required fields' });
  }

  try {
    // Find the schedule and populate the 'inClass' field
    const schedule = await Schedule.findOne({ _id: scheduleId })
      .populate({ path: 'createdBy', model: 'User' })
      .populate({ path: 'trainer', model: 'User' })
      .populate({ path: 'inClass', model: 'Class' })
      .populate({ path: 'materials', model: 'Material' })
      .populate({
        path: 'constructiveQuestions',
        model: 'ConstructiveQuestion',
      })
      .lean();

    await User.populate(schedule.constructiveQuestions, {
      path: 'createdBy',
      model: 'User',
    });

    await User.populate(schedule.materials, {
      path: 'uploadedBy',
      model: 'User',
    });

    await User.populate(schedule.inClass, {
      path: 'trainer',
      model: 'User',
    });

    await User.populate(schedule.inClass, {
      path: 'students',
      model: 'User',
    });

    await Class.populate(schedule.inClass, {
      path: 'semester',
      model: 'MasterData',
    });
    await Subject.populate(schedule.inClass, {
      path: 'subject',
      model: 'Subject',
    });

    // Check if the schedule exists
    if (!schedule) {
      return res.status(404).json({ message: 'Schedule not found' });
    }

    // Check if the user is an Admin
    if (userLogin && userLogin.roles.includes('Admin')) {
      // If the user is an Admin, allow access to all schedule data
      // ... (existing code to populate and return the schedule for Admin)
      return res.status(200).json(schedule);
    }

    // Check if the user is the Trainer of the schedule
    if (schedule.trainer && schedule.trainer._id.equals(userLogin?._id)) {
      // User is the Trainer of the schedule, allow access to the schedule data
      // ... (existing code to populate and return the schedule for Trainer)
      return res.status(200).json(schedule);
    }

    // Check if the user is a Trainer or Student in the inClass
    const isInClass = await Class.exists({
      _id: schedule.inClass._id, // Use the ObjectId of the referenced class
      $or: [{ trainer: userLogin?._id }, { students: userLogin?._id }],
    });

    if (!isInClass) {
      // If the user is neither an Admin, Trainer in inClass, nor Student in inClass, deny access
      return res.status(403).json({ message: 'Unauthorized' });
    }

    // User is a Trainer or Student in the inClass, allow access to the schedule data
    // ... (existing code to populate and return the schedule for Trainer/Student)
    return res.status(200).json(schedule);
  } catch (error) {
    console.log(error);
    return res.status(400).json({ message: 'Find Schedule error ' + error });
  }
};

// @access Private
// method PATCH /schedules
const updateSchedule = async (req, res) => {
  const {
    scheduleId,
    inClass,
    day,
    slotInDay,
    trainer,
    content,
    lessons,
    material,
    constructiveQuestion,
    done,
  } = req.body;
  if (!scheduleId) {
    return res.status(412).json({ message: 'Missing required fields' });
  }

  try {
    // check data schedule
    const schedule = await Schedule.findById(scheduleId);
    //update for general information
    if (day && slotInDay) {
      //if day ,slot not change then can do :
      if (
        dayjs(schedule.day).isSame(day, 'day') &&
        schedule.slotInDay === slotInDay
      ) {
        const scheduleUpdated = await Schedule.findByIdAndUpdate(
          { _id: scheduleId },
          {
            trainer,
            content,
            done,
          }
        );
        if (!scheduleUpdated) {
          return res.status(400).json({ message: 'No Data Updated' });
        }
        return res
          .status(200)
          .json({ message: 'Update Schedule : ' + scheduleId + ' successful' });
      } else {
        //your schedule change date
        //have to check is duplicate time or not
        const duplicate = await Schedule.findOne({ inClass, day, slotInDay });
        if (duplicate) {
          return res
            .status(400)
            .json({ message: 'New Update Duplicated Day and Slot' });
        }
        const scheduleUpdated = await Schedule.findByIdAndUpdate(
          { _id: scheduleId },
          {
            day,
            slotInDay,
            trainer,
            content,
            done,
          }
        );
        if (!scheduleUpdated) {
          return res.status(400).json({ message: 'No Data Updated' });
        }
        return res
          .status(200)
          .json({ message: 'Update Schedule : ' + scheduleId + ' successful' });
      }
    }

    //update other filed
    if (!day && !slotInDay) {
      const scheduleUpdated = await Schedule.findByIdAndUpdate(
        { _id: scheduleId },
        {
          lessons,
          $push: {
            materials: material,
            constructiveQuestions: constructiveQuestion,
          },
          content,
        }
      );
      if (!scheduleUpdated) {
        return res.status(400).json({ message: 'No Data Updated' });
      }
      return res
        .status(200)
        .json({ message: 'Update Schedule : ' + scheduleId + ' successful' });
    }
  } catch (error) {
    console.log(error);
    return res.status(400).json({
      message: 'Something went wrong while update Schedule : ' + error,
    });
  }
};

//method POST /schedules/scheduleFilter
const filterSchedulesByTabAndSemester = async (req, res) => {
  try {
    const { tab, semester, page } = req.body;
    console.log(tab, semester, page, 'haha');
    const { userLogin } = req;
    const limit = 8;
    let totalSchedulesCount = 0;
    let schedules = [];
    let schedulesFilter = [];
    let filterClassesCondition = {};
    let classes = [];
    const now = dayjs();
    if (semester) {
      if (userLogin.roles.includes('Admin')) {
        filterClassesCondition = { semester: semester };
      } else {
        filterClassesCondition.$and = [{ semester: semester }];
        if (
          userLogin.roles.includes('Trainer') &&
          userLogin.roles.includes('Trainee')
        ) {
          const filterWithTrainerAndTraineeRoles = {};
          filterWithTrainerAndTraineeRoles.$or = [];

          filterWithTrainerAndTraineeRoles.$or.push({
            trainer: userLogin._id,
          });

          filterWithTrainerAndTraineeRoles.$or.push({
            students: userLogin._id,
          });

          filterClassesCondition.$and.push(filterWithTrainerAndTraineeRoles);
        } else {
          if (userLogin.roles.includes('Trainer')) {
            filterClassesCondition.$and.push({
              trainer: userLogin._id,
            });
          }
          if (userLogin.roles.includes('Trainee')) {
            filterClassesCondition.$and.push({
              students: userLogin._id,
            });
          }
        }
      }
      classes = await Class.find(filterClassesCondition)
        .populate({ path: 'subject', model: 'Subject' })
        .populate({ path: 'semester', model: 'MasterData' })
        .populate({ path: 'trainer', model: 'User' })
        .populate({ path: 'students', model: 'User' })
        .populate({
          path: 'subject',
          populate: { path: 'lessons', model: 'Lesson' },
        })
        .lean();
    }

    const classIds = classes.map((classObj) => classObj._id);

    // Query schedules based on the filtered conditions.
    schedules = await Schedule.find({ inClass: { $in: classIds } })
      .sort({ day: 1, slotInDay: 1 })
      .populate({ path: 'inClass', model: 'Class' })
      .populate({ path: 'trainer', model: 'User' })
      .populate({
        path: 'inClass',
        populate: {
          path: 'subject',
          model: 'Subject',
        },
      })
      .populate({
        path: 'inClass',
        populate: {
          path: 'semester',
          model: 'MasterData',
        },
      })
      .populate({
        path: 'constructiveQuestions',
        model: 'ConstructiveQuestion',
      })
      .exec();

    if (tab) {
      switch (tab) {
        case 'all':
          schedulesFilter = schedules;
          break;
        case 'on-going':
          schedulesFilter = schedules.filter((schedule) => {
            return (
              schedule.constructiveQuestions.some((question) => {
                return (
                  question.from &&
                  question.to &&
                  now.isAfter(dayjs(question.from)) &&
                  now.isBefore(dayjs(question.to))
                );
              }) ||
              schedule.lessons.some((lesson) => {
                return lesson.constructiveQuestions.some((question) => {
                  return (
                    question?.from &&
                    question?.to &&
                    now.isAfter(dayjs(question.from)) &&
                    now.isBefore(dayjs(question.to))
                  );
                });
              })
            );
          });
          break;
        case 'upcoming':
          if (
            schedules.some((schedule) => {
              return now.isAfter(dayjs(schedule.inClass.semester.to));
            })
          ) {
            schedulesFilter = [];
          } else {
            schedulesFilter = schedules.filter((schedule) => {
              return schedule.done === false;
            });
          }
          break;
        case 'completed':
          schedulesFilter = schedules.filter((schedule) => {
            return (
              now.isAfter(dayjs(schedule.inClass.semester.to)) ||
              (schedule.done &&
                schedule.constructiveQuestions.every(
                  (question) => question.to && now.isAfter(dayjs(question.to))
                ) &&
                schedule.lessons.every((lesson) =>
                  lesson.constructiveQuestions.every(
                    (question) => question.to && now.isAfter(dayjs(question.to))
                  )
                ))
            );
          });
          break;
        default:
          return res.status(400).json({ message: 'Invalid tab parameter.' });
      }
    }

    totalSchedulesCount = schedulesFilter.length;

    // Calculate the current page of data to be returned
    const startIndex = (page - 1) * limit;
    const endIndex = Math.min(startIndex + limit, totalSchedulesCount);
    schedules = schedulesFilter.slice(startIndex, endIndex);
    res.json({ totalSchedulesCount, schedules });
  } catch (error) {
    console.error('Error fetching schedules:', error);
    return res.status(500).json({ message: 'Server error' });
  }
};

// @access Private
// method GET /schedules
const getSchedulesByClassId = async (req, res) => {
  try {
    const { classId } = req.params;
    console.log(classId);
    let schedules = [];

    // Validate if classId is a valid ObjectId
    if (!mongoose.Types.ObjectId.isValid(classId)) {
      return res.json(schedules);
    }

    let index = 1; // Initialize index

    schedules = await Schedule.find({ inClass: classId })
      .sort({
        day: 1,
        slotInDay: 1,
        'lessons.lessonChapter': 1,
        'lessons.lessonNumber': 1,
      })
      .populate({ path: 'createdBy', model: 'User' })
      .populate({
        path: 'constructiveQuestions',
        model: 'ConstructiveQuestion',
      })
      .lean();

    if (!schedules) {
      return res.json(schedules);
    }

    // Add index to each schedule
    const schedulesWithIndex = schedules.map((schedule) => ({
      ...schedule,
      index: index++,
    }));

    res.json(schedulesWithIndex);
  } catch (error) {
    console.log(error);
    return res.status(400).json({ message: 'Find Schedule error ' + error });
  }
};

module.exports = {
  getSchedules,
  getScheduleById,
  createNewSchedule,
  importSchedulesFromXLSX,
  updateSchedule,
  filterSchedulesByTabAndSemester,
  getSchedulesByClassId,
};
