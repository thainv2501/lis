const Group = require("../models/Group");
const Vote = require("../models/Vote");
const Grade = require("../models/Grade");
const Schedule = require("../models/Schedule");
const User = require("../models/User");
const Comment = require("../models/Comment");

// @desc Get groups
// @route GET /groups
// @access Private
const getGroupOfStudentId = async (req, res) => {
  const { scheduleId, studentId } = req.query;
  if (!scheduleId || !studentId) {
    console.log("Missing required filed !");
    return res.status(400).json({ message: "Missing required filed !" });
  }
  try {
    const groupData = await Group.findOne({
      schedule: scheduleId,
      members: studentId,
    })
      .populate({
        path: "members",
        model: "User",
      })
      .populate({
        path: "leader",
        model: "User",
      })
      .sort({ groupNumber: 1 })
      .exec();
    if (!groupData) {
      return res.status(400).json({ message: "No Group Found !" });
    }
    return res.status(200).json(groupData);
  } catch (error) {
    console.error(error);
    return res
      .status(400)
      .json({ message: "Error while get groups !" + error });
  }
};
// @desc Get groups
// @route GET /groups/:scheduleId
// @access Private
const getGroupsByScheduleId = async (req, res) => {
  const { scheduleId } = req.params;
  if (!scheduleId) {
    console.log("Missing required filed !");
    return res.status(400).json({ message: "Missing required filed !" });
  }
  try {
    const groupsData = await Group.find({ schedule: scheduleId })
      .populate({
        path: "members",
        model: "User",
      })
      .populate({
        path: "leader",
        model: "User",
      })
      .sort({ groupNumber: 1 })
      .exec();
    if (groupsData.length === 0) {
      return res.status(400).json({ message: "No Group Found !" });
    }
    return res.status(200).json(groupsData);
  } catch (error) {
    console.error(error);
    return res
      .status(400)
      .json({ message: "Error while get groups !" + error });
  }
};

// @desc Get groups
// @route POST /groups
// @access Private
const createGroups = async (req, res) => {
  try {
    const { scheduleId, members } = req.body;
    if (!scheduleId) {
      return res.status(400).json({ message: "Missing required field" });
    }

    // Find the Schedule document based on the provided scheduleId
    // Find the schedule and populate the 'inClass' field
    const schedule = await Schedule.findOne({ _id: scheduleId })
      .populate({ path: "trainer", model: "User" })
      .populate({
        path: "inClass",
        model: "Class",
        populate: { path: "students", model: "User" },
      })
      .exec();

    if (!schedule) {
      return res.status(404).json({ message: "Schedule not found" });
    }

    // Extract the student emails from the "inclass" array of the Schedule
    const inClassStudents = schedule.inClass.students.map(
      (student) => student.email
    );

    // Filter the members array to include only students present in the "inclass" array
    const validMembers = members.filter((member) =>
      inClassStudents.includes(member.email)
    );

    // Map the User documents to their respective group numbers
    const userMap = {};
    const userDocuments = await User.find({
      email: { $in: inClassStudents },
    });
    userDocuments.forEach((user) => {
      userMap[user.email] = user._id;
    });

    // get all group information in members data
    const uniqueGroupNumbers = [
      ...new Set(validMembers.map((member) => member.group)),
    ];

    for (const groupNumber of uniqueGroupNumbers) {
      const groupMembers = validMembers.filter(
        (member) => member.group === groupNumber
      );
      const existingGroup = await Group.findOne({
        schedule: scheduleId,
        groupNumber,
      });

      if (existingGroup) {
        // If that group exists in the database, then update it
        await Group.findByIdAndUpdate(existingGroup._id, {
          members: groupMembers.map((member) => userMap[member.email]),
          leader: userMap[groupMembers[0].email],
        });
      } else {
        // If that group doesn't exist in the database, create a new group
        const newGroup = new Group({
          schedule: scheduleId,
          groupNumber: groupNumber,
          members: groupMembers.map((member) => userMap[member.email]),
          leader: userMap[groupMembers[0].email],
        });

        await newGroup.save();
      }
    }

    console.log("Groups created or updated successfully");
    return res
      .status(200)
      .json({ message: "Groups created or updated successfully" });
  } catch (error) {
    console.error("Error creating or updating groups:", error);
    return res
      .status(400)
      .json({ message: "Groups created or updated Fail" + error });
  }
};

// @desc Get groups
// @route PATCH /groups/remove
// @access Private
const removeStudentOutGroup = async (req, res) => {
  try {
    const { groupId, studentId } = req.body;
    if (!groupId) {
      return res.status(400).json({ message: "Missing required field" });
    }

    // Find the User documents based on the provided emails
    const student = await User.findById(studentId);

    if (!student) {
      return res.status(400).json({ message: "student not found" });
    }

    // Find the Group document to which the student belongs
    const group = await Group.findById(groupId);
    if (!group) {
      return res.status(400).json({ message: "Group not found" });
    }
    if (group.leader.toString() === studentId) {
      const newLeader = group.members.find(
        (member) => !member._id.equals(studentId)
      );
      if (!newLeader) {
        // If there is no new leader, clear the leader field of the group
        await Group.findByIdAndUpdate(groupId, {
          $pull: { members: studentId },
          $unset: { leader: 1 },
        });
      } else {
        // Update the group with the new leader
        await Group.findByIdAndUpdate(groupId, {
          $pull: { members: studentId },
          leader: newLeader._id,
        });
      }
    } else {
      await Group.findByIdAndUpdate(groupId, { $pull: { members: studentId } });
    }

    await Vote.deleteMany({ votedBy: studentId, schedule: group.schedule });

    // reset this user comment voted
    const comments = await Comment.find({
      schedule: group.schedule,
      createdBy: studentId,
    });

    comments.forEach(async (comment) => {
      await Vote.deleteMany({ _id: { $in: comment.votes } });
    });

    //remove student out of this group
    return res.status(200).json({ message: "Student remove out of Group" });
  } catch (error) {
    console.error("Error creating or updating groups:", error);
    return res.status(400).json({ message: "Groups updated Fail" + error });
  }
};

// @desc Get groups
// @route PATCH /groups/move
// @access Private
const moveStudentToGroup = async (req, res) => {
  try {
    const { oldGroupId, newGroupId, studentId } = req.body;
    if (!newGroupId) {
      return res.status(400).json({ message: "Missing required field" });
    }

    // Find the Group document to which the student belongs
    const oldGroup = await Group.findById(oldGroupId);
    const newGroup = await Group.findById(newGroupId);
    if (!newGroup) {
      return res.status(400).json({ message: "Group old or new not found" });
    }

    // Find the User documents based on the provided emails
    const student = await User.findById(studentId);

    if (!student) {
      return res.status(400).json({ message: "student not found" });
    }

    if (oldGroup) {
      //check move student is leader or not, if leader have to remove them out leader filed and set a new leader
      if (oldGroup.leader.toString() === studentId) {
        const newLeader = oldGroup.members.find(
          (member) => !member._id.equals(studentId)
        );
        if (!newLeader) {
          // If there is no new leader, clear the leader field of the group
          await Group.findByIdAndUpdate(oldGroupId, {
            $pull: { members: studentId },
            $unset: { leader: 1 },
          });
        } else {
          // Update the group with the new leader
          await Group.findByIdAndUpdate(oldGroupId, {
            $pull: { members: studentId },
            leader: newLeader._id,
          });
        }
      } else {
        await Group.findByIdAndUpdate(oldGroupId, {
          $pull: { members: studentId },
        });
      }
    }
    if (!newGroup.leader) {
      await Group.findByIdAndUpdate(newGroupId, {
        $addToSet: { members: studentId },
        leader: studentId,
      });
    } else {
      await Group.findByIdAndUpdate(newGroupId, {
        $addToSet: { members: studentId },
      });
    }

    await Vote.deleteMany({ votedBy: studentId, schedule: newGroup.schedule });

    // reset this user comment voted
    const comments = await Comment.find({
      schedule: newGroup.schedule,
      createdBy: studentId,
    });

    comments.forEach(async (comment) => {
      await Vote.deleteMany({ _id: { $in: comment.votes } });
    });

    return res.status(200).json({ message: "Student move in Group" });
  } catch (error) {
    console.error("Error creating or updating groups:", error);
    return res.status(400).json({ message: "Groups updated Fail" + error });
  }
};

// @route POST /groups/combine
// @access Private
const combineNewGroup = async (req, res) => {
  try {
    const { scheduleId, students, groupNumber } = req.body;
    // If that group doesn't exist in the database, create a new group
    const newGroup = new Group({
      schedule: scheduleId,
      groupNumber,
      members: students.map((member) => member._id),
      leader: students[0]._id,
    });

    await newGroup.save();

    return res.status(200).json({ message: "Student move in Group" });
  } catch (error) {
    console.error("Error creating or updating groups:", error);
    return res.status(400).json({ message: "Groups updated Fail" + error });
  }
};

module.exports = {
  getGroupsByScheduleId,
  createGroups,
  removeStudentOutGroup,
  moveStudentToGroup,
  getGroupOfStudentId,
  combineNewGroup,
};
