const Subject = require('../models/Subject');
const User = require('../models/User');
// @desc Get subjects
// @route GET /subjects
// @access Private
const getSubjects = async (req, res) => {
  try {
    const subjects = await Subject.find({ status: true });
    res.json(subjects);
  } catch (error) {
    res.status(400).json({ success: false, error: error.message });
  }
};

// Hàm chuẩn hóa regex để tìm kiếm không dấu
function normalizeRegex(text) {
  const normalizedText = text
    .replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // Escaping special regex characters
    .replace(/a|á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, '[aáàảãạăắằẳẵặâấầẩẫậ]') // Handling "a" variations
    .replace(/đ/gi, '[dđ]') // Handling "đ"
    .replace(/e|é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, '[eéèẻẽẹêếềểễệ]') // Handling "e" variations
    .replace(/i|í|ì|ỉ|ĩ|ị/gi, '[iíìỉĩị]') // Handling "i" variations
    .replace(/o|ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, '[oóòỏõọôốồổỗộơớờởỡợ]') // Handling "o" variations
    .replace(/u|ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, '[uúùủũụưứừửữự]') // Handling "u" variations
    .replace(/y|ý|ỳ|ỷ|ỹ|ỵ/gi, '[yýỳỷỹỵ]'); // Handling "y" variations

  return normalizedText;
}

// @desc Get all subjects with search condition
// @route GET /subjects
// @access Private
const getSubjectsWithSearch = async (req, res) => {
  const { page, status, keyword } = req.query;
  const limit = 10;
  // Get all subjects from MongoDB
  // validate search information
  let keywordSearch, statusFilter;
  keywordSearch = keyword ? keyword.trim() : '';
  statusFilter = status === 'all' ? undefined : status === 'active';
  const searchObj = {
    $and: [
      {
        $or: [
          {
            subjectCode: {
              $regex: normalizeRegex(keywordSearch),
              $options: 'i',
            },
          },
          {
            subjectName: {
              $regex: normalizeRegex(keywordSearch),
              $options: 'i',
            },
          },
        ],
      },
      typeof statusFilter === 'undefined' ? {} : { status: statusFilter },
    ],
  };

  const subjects = await Subject.find(searchObj)
    .limit(limit)
    .skip((parseInt(page) - 1) * limit);
  const subjectsCount = await Subject.countDocuments(searchObj).exec();
  // // If no subjects
  if (subjectsCount === 0) {
    return res.status(400).json({ message: 'No subjects found' });
  }

  res.json({ subjectsCount, subjects });
};

// @desc Get subject by id
// @route GET /subjects
// @access Private
const getSubjectById = async (req, res) => {
  // Get all users from MongoDB
  const { id } = req.params;
  const subject = await Subject.findById(id).lean();
  const trainers = await User.find({
    _id: { $in: subject?.trainers },
  });
  // If no subject
  if (!subject) {
    return res.status(400).json({ message: 'No subject found' });
  }
  console.log(trainers);
  res.json({ ...subject, trainers });
};

// @desc Create new subject
// @route POST /subjects
// @access Private
const createSubject = async (req, res) => {
  try {
    const {
      subjectCode,
      subjectName,
      subjectDescription,
      chapterLimit,
      lessons,
      trainers,
      materials,
    } = req.body;

    // Check if the subject with the same subjectCode or subjectName already exists
    let subject = await Subject.findOne({
      $or: [{ subjectCode }, { subjectName }],
    });
    if (subject) {
      return res.status(400).json({
        message:
          'Subject with the same subjectCode or subjectName already exists',
      });
    }

    // Create a new subject
    subject = await Subject.create({
      subjectCode,
      subjectName,
      subjectDescription,
      chapterLimit,
      lessons,
      trainers,
      materials,
    });

    res.status(201).json({ message: `New subject ${subjectCode} created` });
  } catch (error) {
    console.log(error);
    res.status(400).json({ message: 'Something went wrong' });
  }
};

// @desc Update subject
// @route PUT /subjects
// @access Private
const updateSubject = async (req, res) => {
  try {
    const {
      id,
      subjectCode,
      subjectName,
      subjectDescription,
      chapterLimit,
      lessons,
      trainers,
      materials,
      status,
    } = req.body;
    let subject;
    if ((subjectCode, subjectName)) {
      // Check if the subject with the same subjectCode or subjectName already exists
      subject = await Subject.findOne({
        $and: [
          { _id: { $ne: id } },
          {
            $or: [{ subjectCode: subjectCode }, { subjectName: subjectName }],
          },
        ],
      });
    }
    if (subject && subject._id.toString() !== id) {
      return res.status(400).json({
        message:
          'Subject with the same subjectCode or subjectName already exists',
      });
    }

    // Update the subject
    subject = await Subject.findByIdAndUpdate(
      id,
      {
        subjectCode,
        subjectName,
        subjectDescription,
        chapterLimit,
        lessons,
        trainers,
        materials,
        status,
      },
      { new: true }
    );

    if (!subject) {
      return res.status(404).json({ message: 'Subject not found' });
    }
    res.json({ message: `${subjectCode} updated` });
  } catch (error) {
    res.status(500).json({ message: 'Something went wrong' });
  }
};

// @desc Delete subject
// @route PATCH /subjects
// @access Private
const deleteSubject = async (req, res) => {
  try {
    const deletedSubject = await Subject.findByIdAndDelete(req.params.id);
    if (!deletedSubject) {
      return res.status(404).json({
        success: false,
        error: `Subject not found with id ${req.params.id}`,
      });
    }
    res.status(200).json({ success: true, data: {} });
  } catch (error) {
    res.status(400).json({ success: false, error: error.message });
  }
};

module.exports = {
  getSubjects,
  getSubjectsWithSearch,
  getSubjectById,
  createSubject,
  updateSubject,
  deleteSubject,
};
