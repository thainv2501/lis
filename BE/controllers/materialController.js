const Material = require("../models/Material");

// @desc create a new material
// @route POST /materials
// @access Private
const createNewMaterial = async (req, res) => {
  const { content, sources, uploadedBy } = req.body;

  if (!content || !sources || !uploadedBy) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    const newMaterial = new Material({
      content,
      sources,
      uploadedBy,
    });

    const createdMaterial = await newMaterial.save();
    console.log(createdMaterial);
    return res.status(201).json({
      message: "Material Created successfully",
      createdMaterial,
    });
  } catch (error) {
    return res
      .status(400)
      .json({ message: "Material Created fail", createdMaterial });
  }
};

// @desc create a new material
// @route DELETE /materials
// @access Private
const deleteMaterial = async (req, res) => {
  const { id } = req.body;

  if (!id) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    await Material.findByIdAndDelete(id);
    return res.status(201).json({
      message: "Material Delete successfully",
    });
  } catch (error) {
    return res.status(400).json({ message: "Material delete fail" });
  }
};

// @desc create a new material
// @route PATCH /materials
// @access Private
const updateMaterial = async (req, res) => {
  const { id, content, sources, uploadedBy } = req.body;

  if (!content || !sources || !uploadedBy || !id) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    await Material.findByIdAndUpdate(id, { content, sources, uploadedBy });
    return res.status(201).json({
      message: "Material update successfully",
    });
  } catch (error) {
    return res.status(400).json({ message: "Material Update Fail" });
  }
};

module.exports = {
  createNewMaterial,
  deleteMaterial,
  updateMaterial,
};
