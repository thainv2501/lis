const User = require("../models/User");
const bcrypt = require("bcrypt");
const mailer = require("../utils/mailer");

// @desc Get all users
// @route GET /users
// @access Private
const getAllUsers = async (req, res) => {
  // Get all users from MongoDB
  const usersCount = await User.countDocuments().exec();
  const users = await User.find().select("-password").lean();

  // If no users
  if (!users?.length) {
    return res.status(400).json({ message: "No users found" });
  }

  res.json({ usersCount, users });
};

// @desc Get users with id
// @route GET /users/:id
// @access Private
const getUserWithId = async (req, res) => {
  // Get all users from MongoDB
  const { id } = req.params;
  try {
    const user = await User.findById(id).select("-password").lean();
    if (!user) {
      return res.status(400).json({ message: "No user found" });
    }
    return res.status(200).json(user);
  } catch (error) {
    return res.status(400).json({ message: "No user found" });
  }
};

// @desc Get all users with search condition
// @route GET /users
// @access Private
const getUsersWithSearch = async (req, res) => {
  const { page, role, active, keyword } = req.query;
  const { email: emailLogin, userLogin } = req;
  const limit = 10;
  // Get all users from MongoDB
  //validate search information
  const searchObj = { email: { $nin: emailLogin } };

  if (keyword) {
    searchObj.$or = [
      { email: { $regex: keyword.trim(), $options: "i", $nin: emailLogin } }, // Case-insensitive regex search for email
      { phoneNumber: { $regex: keyword.trim(), $options: "i" } }, // Case-insensitive regex search for phone number
      { fullName: { $regex: keyword.trim(), $options: "i" } }, // Case-insensitive regex search for fullname
    ];
  }
  if (active !== "all") searchObj.active = active === "active";
  if (role !== "all") searchObj.roles = role;

  const users = await User.find(searchObj)
    .select("-password")
    .limit(limit)
    .skip((parseInt(page) - 1) * limit)
    .lean();
  const usersCount = await User.countDocuments(searchObj).exec();
  // // If no users
  if (usersCount === 0) {
    return res.status(400).json({ message: "No users found" });
  }

  res.json({ usersCount, users });
};

// @desc Create new user
// @route POST /users
// @access Private
const createNewUser = async (req, res) => {
  const { email, fullName, phoneNumber, roles } = req.body;

  // Confirm data
  if (
    !email ||
    !phoneNumber ||
    !fullName ||
    !roles ||
    roles?.length === 0 ||
    !Array.isArray(roles)
  ) {
    return res.status(400).json({ message: "All fields are required" });
  }

  // Check for duplicate email
  const duplicate = await User.findOne({ email })
    .collation({ locale: "en", strength: 2 })
    .lean();
  if (duplicate) {
    return res.status(409).json({ message: "Duplicate emails" });
  }

  //else no duplicate user email then :
  //create a random password for new user
  const length = 6;
  const charset =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let password = "";

  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * charset.length);
    password += charset[randomIndex];
  }

  try {
    const hashedEmail = await bcrypt.hash(email, 10);
    try {
      await mailer.sendMail(
        email,
        "Your account have been created to Learning Interactions Systems",
        `<p>Your password to login in the system</p>
      <p>your password is :${password} </p>
      <p>Please reset your password after to login easier ! </p>
      <p>Verify this email to finish your register and login to system</p>
    <p>click on the link : </p>
    <a href="${process.env.APP_URL}/verify?email=${email}&token=${hashedEmail}"> Verify </a>`
      );
    } catch (error) {
      return res.status(500).json({ message: "An error during sending email" });
    }
  } catch (error) {
    return res.status(500).json({ message: "An error while hashing email" });
  }

  // Hash password
  const hashedPwd = await bcrypt.hash(password, 10); // salt rounds

  const userObject =
    !Array.isArray(roles) || !roles.length
      ? { email, password: hashedPwd, fullName: fullName.trim(), phoneNumber }
      : {
          email,
          password: hashedPwd,
          fullName: fullName.trim(),
          phoneNumber,
          roles,
        };

  // Create and store new user
  const user = await User.create(userObject);

  if (user) {
    //created
    return res.status(201).json({ message: `New user ${email} created` });
  } else {
    return res.status(400).json({ message: "Invalid user data received" });
  }
};

// @desc Update a user
// @route PATCH /users
// @access Private
const updateUser = async (req, res) => {
  const { id, fullName, phoneNumber, active, roles, note } = req.body;

  // Confirm data
  if (!id) {
    return res.status(400).json({ message: "Id are required" });
  }

  // Does the user exist to update?
  const user = await User.findById(id).exec();

  if (!user) {
    return res.status(400).json({ message: "User not found" });
  }

  if (fullName) {
    await User.updateOne(
      { _id: user._id },
      { fullName: fullName.trim() }
    ).catch((error) => {
      console.log(error);
      return res.status(500).json({ message: "update name fail !" });
    });
  }

  if (roles) {
    await User.updateOne({ _id: user._id }, { roles }).catch((error) => {
      console.log(error);
      return res.status(500).json({ message: "update roles fail !" });
    });
  }

  if (active != undefined) {
    await User.updateOne({ _id: user._id }, { active }).catch((error) => {
      console.log(error);
      return res.status(500).json({ message: "update status fail !" });
    });
  }

  if (note != undefined) {
    await User.updateOne({ _id: user._id }, { note: note.trim() }).catch(
      (error) => {
        console.log(error);
        return res.status(500).json({ message: "update note fail !" });
      }
    );
  }
  if (phoneNumber != undefined) {
    await User.updateOne({ _id: user._id }, { phoneNumber }).catch((error) => {
      console.log(error);
      return res.status(500).json({ message: "update phone number fail !" });
    });
  }
  res.json({ message: `${user.email} updated` });
};

// @desc Delete a user
// @route DELETE /users
// @access Private
const deleteUser = async (req, res) => {
  const { id } = req.body;

  // // Confirm data
  // if (!id) {
  //   return res.status(400).json({ message: "User ID Required" });
  // }

  const { keyword } = req.body;

  try {
    const result = await User.deleteMany({
      $or: [
        { email: { $regex: keyword, $options: "i" } },
        { fullName: { $regex: keyword, $options: "i" } },
      ],
    });

    if (result.deletedCount > 0) {
      return res.status(200).json({ message: "Users deleted successfully" });
    } else {
      return res.status(404).json({ message: "No users found" });
    }
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: "Internal server error" });
  }
};

// @author: CuongTV
// @desc Get users with Trainer Role
// @route GET /users/role/:role
// @access Private
const getUsersWithRole = async (req, res) => {
  const { role } = req.params;
  // Get all trainers from MongoDB
  const users = await User.find({
    roles: { $in: [role] },
  })
    .select("-password")
    .lean();
  // If no trainers
  if (!users) {
    return res.status(400).json({ message: "No user found" });
  }
  console.log("get thành công");
  res.json(users);
};

module.exports = {
  getUsersWithSearch,
  getUserWithId,
  getAllUsers,
  createNewUser,
  updateUser,
  deleteUser,
  getUsersWithRole,
};
