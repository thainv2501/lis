const SupportQuestion = require('../models/SupportQuestion');
const Semester = require('../models/MasterData');
const Class = require('../models/Class');
const Schedule = require('../models/Schedule');
const Subject = require('../models/Subject');
const mongoose = require('mongoose');

const getSupportQuestions = async (req, res) => {
  try {
    const { tab, keyword, scheduleId, page, type } = req.body;
    console.log(tab, scheduleId, page, type);
    const limit = 5;
    let supportQuestions = [];
    let totalSupportQuestionsCount = 0;
    let filterCondition = {};
    const { userLogin } = req;

    // Check if scheduleId is undefined
    if (!mongoose.Types.ObjectId.isValid(scheduleId)) {
      return res.json({ totalSupportQuestionsCount, supportQuestions });
    }

    if (type === 'support-question') {
      filterCondition = {
        type: type,
        schedule: scheduleId,
      };
    }

    if (tab) {
      switch (tab) {
        case 'my-question':
          filterCondition = {
            createdBy: userLogin._id,
            type: type,
            schedule: scheduleId,
          };
          break;
        case 'all-question':
          filterCondition = {
            type: type,
            schedule: scheduleId,
          };

          break;
        default:
          // 'All' or other cases
          return res.status(400).json({ error: 'Invalid tab value' });
      }
    }
    if (keyword) {
      filterCondition.$or = [
        { title: { $regex: keyword.trim(), $options: 'i' } },
        { content: { $regex: keyword.trim(), $options: 'i' } },
      ];
    }

    totalSupportQuestionsCount = await SupportQuestion.countDocuments(
      filterCondition
    );

    supportQuestions = await SupportQuestion.find(filterCondition)
      .sort({ createdDate: -1 })
      .populate({ path: 'createdBy', model: 'User' })
      .populate('reply')
      .skip((page - 1) * limit)
      .limit(limit)
      .exec();
    // Trả về thông tin câu hỏi hỗ trợ
    console.log(totalSupportQuestionsCount, 'ma');
    res.json({ totalSupportQuestionsCount, supportQuestions });
  } catch (error) {
    res.json({ success: false, error: error.message });
  }
};

const getSubjectQuestions = async (req, res) => {
  try {
    const { tab, keyword, subjectId, page, type } = req.body;
    console.log(tab, keyword, subjectId, page, type);
    console.log('hello');
    const limit = 5;
    let subjectQuestions = [];
    let totalSubjectQuestionsCount = 0;
    let filterCondition = {};
    const { userLogin } = req;

    switch (tab) {
      case 'my-question':
        filterCondition = {
          createdBy: userLogin._id,
          type: type,
          subject: subjectId,
        };
        break;
      case 'all-question':
        filterCondition = {
          type: type,
          subject: subjectId,
        };

        break;
      default:
        // 'All' or other cases
        return res.status(400).json({ error: 'Invalid tab value' });
    }

    if (keyword) {
      filterCondition.$or = [
        { title: { $regex: keyword.trim(), $options: 'i' } },
        { content: { $regex: keyword.trim(), $options: 'i' } },
      ];
    }

    totalSubjectQuestionsCount = await SupportQuestion.countDocuments(
      filterCondition
    );

    subjectQuestions = await SupportQuestion.find(filterCondition)
      .sort({ createdDate: -1 })
      .populate({ path: 'createdBy', model: 'User' })
      .populate('reply')
      .skip((page - 1) * limit)
      .limit(limit)
      .exec();
    // Trả về thông tin câu hỏi hỗ trợ
    res.json({ totalSubjectQuestionsCount, subjectQuestions });
  } catch (error) {
    res.json({ success: false, error: error.message });
  }
};

const createSupportQuestion = async (req, res) => {
  try {
    const { type, subjectId, scheduleId, title, content, sources, createdBy } =
      req.body;
    // Kiểm tra xem type có hợp lệ hay không
    if (!['subject-question', 'support-question'].includes(type)) {
      return res.status(400).json({ error: 'Invalid type' });
    }

    // Kiểm tra xem subjectId và scheduleId có tồn tại hay không
    if (type === 'subject-question' && !subjectId) {
      return res
        .status(400)
        .json({ error: 'Subject ID is required for subject-question' });
    }

    if (type === 'support-question' && !scheduleId) {
      return res
        .status(400)
        .json({ error: 'Schedule ID is required for support-question' });
    }

    // Tạo câu hỏi mới
    const newQuestion = new SupportQuestion({
      type: type,
      title: title,
      content: content,
      sources: sources,
      createdBy: createdBy,
    });

    // Thêm câu hỏi vào ref Subject hoặc ref Schedule
    if (type === 'subject-question') {
      // Kiểm tra xem subject có tồn tại hay không
      const subject = await Subject.findById(subjectId);
      if (!subject) {
        return res.status(404).json({ error: 'Subject not found' });
      }
      newQuestion.subject = subjectId;
    } else if (type === 'support-question') {
      // Kiểm tra xem schedule có tồn tại hay không
      const schedule = await Schedule.findById(scheduleId);
      if (!schedule) {
        return res.status(404).json({ error: 'Schedule not found' });
      }
      newQuestion.schedule = scheduleId;
    }

    // Lưu câu hỏi mới vào database
    const savedQuestion = await newQuestion.save();

    res.json({
      message: 'Support question created successfully',
      data: savedQuestion,
    });
  } catch (err) {
    // console.error('Error creating support question:', err);
    res.json({ error: 'Server error' });
  }
};

const deleteSupportQuestion = async (req, res) => {
  try {
    const { id } = req.body;

    if (!id) {
      return res.status(412).json({ message: 'Missing required fields' });
    }
    // Tìm câu hỏi hỗ trợ cần xoá trong CSDL
    await SupportQuestion.findByIdAndDelete(id);
    return res.status(201).json({
      message: 'Support question delete successfully',
    });
    // Trả về kết quả xoá thành công
    res.status(200).json({ success: true });
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

// Update an existing Support Question
const updateSupportQuestion = async (req, res) => {
  try {
    const { questionId, title, content, sources } = req.body;
    const supportQuestion = await SupportQuestion.findByIdAndUpdate(
      questionId,
      {
        title,
        content,
        sources,
      },
      { new: true }
    );

    if (!supportQuestion) {
      return res
        .status(404)
        .json({ success: false, error: 'Support question not found' });
    }

    res.status(200).json({ success: true, data: supportQuestion });
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

const updateSupportQuestionStatus = async (req, res) => {
  const { questionId, newStatus } = req.body;

  try {
    // Tìm supportQuestion cần cập nhật trong CSDL
    const supportQuestion = await SupportQuestion.findById(questionId);

    if (!supportQuestion) {
      return res.status(404).json({ error: 'Support question not found' });
    }

    // Thay đổi giá trị của trường status
    supportQuestion.status = newStatus;

    // Lưu lại sự thay đổi vào CSDL
    await supportQuestion.save();

    return res.json({
      message: 'Support question status updated successfully',
    });
  } catch (error) {
    return res
      .status(500)
      .json({ error: 'Error updating support question status' });
  }
};

const getSupportQuestionById = async (req, res) => {
  try {
    const { id } = req.params;
    console.log(id);
    // Truy vấn câu hỏi hỗ trợ dựa trên ID
    const supportQuestion = await SupportQuestion.findById(id)
      .populate({ path: 'createdBy', model: 'User' })
      .populate({ path: 'subject', model: 'Subject' })
      .populate({
        path: 'subject',
        populate: { path: 'trainers', model: 'User' },
      })
      .populate({
        path: 'schedule',
        populate: { path: 'trainer', model: 'User' },
      })
      .populate('reply')
      .lean();

    // Kiểm tra xem câu hỏi có tồn tại hay không
    if (!supportQuestion) {
      return res.status(500).json({ error: 'Support question not found' });
    }

    // Trả về thông tin câu hỏi hỗ trợ
    res.json(supportQuestion);
  } catch (error) {
    res.json({ error: error.message });
  }
};
module.exports = {
  getSupportQuestions,
  createSupportQuestion,
  deleteSupportQuestion,
  updateSupportQuestion,
  updateSupportQuestionStatus,
  getSupportQuestionById,
  getSubjectQuestions,
};
