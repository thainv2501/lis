const ConstructiveQuestion = require("../models/ConstructiveQuestion");
const Lesson = require("../models/Lesson");
const Material = require("../models/Material");
const Subject = require("../models/Subject");
// @desc Get Lesson
// @route GET /lessons
// @access Private
const getLesson = async (req, res) => {
  try {
    const Lesson = await Subject.find();
    res.status(200).json(Lesson);
  } catch (error) {
    res.status(400).json({ success: false, error: error.message });
  }
};

// @desc Get all Lesson with search condition
// @route GET /lessons/:subjectId
// @access Private
const getLessonWithSearch = async (req, res) => {
  const { page, status, keyword } = req.query;
  const { subjectId } = req.params;
  const limit = 10;
  try {
    // Get all Lesson from MongoDB
    // validate search information
    let keywordSearch, statusFilter;
    keywordSearch = keyword ? keyword.trim() : "";
    statusFilter = status === "all" ? undefined : status === "active";
    const searchObj = {
      subject: subjectId,
      $and: [
        {
          $or: [{ lessonContent: { $regex: keywordSearch, $options: "i" } }],
        },
        typeof statusFilter === "undefined" ? {} : { status: statusFilter },
      ],
    };

    const lessons = await Lesson.find(searchObj)
      .sort({ lessonChapter: 1, lessonNumber: 1 })
      .populate("subject")
      .populate({ path: "createdBy", model: "User" })
      .limit(limit)
      .skip((parseInt(page) - 1) * limit)
      .lean();
    const lessonsCount = await Lesson.countDocuments(searchObj).exec();
    // // If no Lesson
    if (lessonsCount === 0) {
      return res.status(400).json({ message: "No Lesson found" });
    }

    return res.status(200).json({ lessonsCount, lessons });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Error during process" + error });
  }
};

// @route GET /lessons/lesson-details/:lessonId
// @access Private
const getLessonById = async (req, res) => {
  // Get all Lesson from MongoDB
  const { lessonId } = req.params;
  try {
    const lesson = await Lesson.findById(lessonId)
      .populate({ path: "createdBy", model: "User" })
      .populate("subject")
      .populate({ path: "materials", model: "Material" })
      .populate({
        path: "constructiveQuestions",
        model: "ConstructiveQuestion",
      })
      .lean();
    // If no lesson
    if (!lesson) {
      return res.status(400).json({ message: "No Lesson found" });
    }

    // Populate the uploadedBy field within each material
    await Material.populate(lesson.materials, { path: "uploadedBy" });
    await ConstructiveQuestion.populate(lesson.constructiveQuestions, {
      path: "createdBy",
    });

    return res.status(200).json(lesson);
  } catch (error) {
    return res.status(500).json({ message: "error : " + error });
  }
};

// @desc Create new lesson
// @route POST /lessons
// @access Private
const createLesson = async (req, res) => {
  try {
    const { subject, lessonChapter, lessonContent, lessonNumber, createdBy } =
      req.body;

    const existingLesson = await Lesson.findOne({
      subject,
      lessonNumber,
      lessonChapter,
    });
    if (existingLesson) {
      await Lesson.updateOne(
        { _id: existingLesson?._id },
        { lessonContent, createdBy }
      );
      res.status(201).json({ message: `New lesson created` });
    } else {
      // Create a new Lesson
      const lesson = await Lesson.create({
        subject,
        lessonChapter,
        lessonContent,
        lessonNumber,
        createdBy,
      });

      await Subject.updateOne(
        { _id: subject },
        { $push: { lessons: lesson?._id } }
      );

      res.status(201).json({ message: `New lesson created` });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

// @desc Create new lesson
// @route POST /lessons/add-lessons
// @access Private
const importFromXlSX = async (req, res) => {
  const { subject, lessons, createdBy } = req.body;
  if (!subject || !lessons) {
    return res.status(412).json({ message: "Missing required fields" });
  }
  try {
    await Promise.all(
      lessons.map(async (lesson) => {
        const existingLesson = await Lesson.findOne({
          subject,
          lessonNumber: lesson.lessonNumber,
        });
        if (existingLesson) {
          await Lesson.updateOne(
            { _id: existingLesson?._id },
            {
              lessonContent: lesson.lessonContent,
              lessonChapter: lesson.chapter,
              createdBy,
            }
          );
          console.log("added existed lesson to class");
        } else {
          // Create a new Lesson
          const createdLesson = await Lesson.create({
            subject,
            lessonChapter: lesson?.lessonChapter,
            lessonContent: lesson?.lessonContent,
            lessonNumber: lesson?.lessonNumber,
            createdBy,
          });

          await Subject.updateOne(
            { _id: subject },
            { $push: { lessons: createdLesson?._id } }
          );

          console.log("created a new lesson to class");
        }
      })
    );
    return res.status(201).json({
      message: "Lessons added successfully",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

// @desc Update lesson
// @route PATCH /lessons
// @access Private
const updateLesson = async (req, res) => {
  try {
    const { lessonId, lessonChapter, lessonContent, materialId, CQId, status } =
      req.body;

    // Update the lesson
    const updatedLesson = await Lesson.findByIdAndUpdate(lessonId, {
      lessonChapter,
      lessonContent,
      $push: { materials: materialId },
      $addToSet: { constructiveQuestions: CQId },
      status,
    });
    if (!updatedLesson) {
      return res.status(400).json({ message: "Update fail !" });
    }
    return res
      .status(200)
      .json({ message: "Update successful !", updatedLesson });
  } catch (error) {
    res.status(500).json({ message: "Something went wrong" });
  }
};

// @desc Delete lesson
// @route PATCH /Lesson
// @access Private
const deleteLesson = async (req, res) => {
  try {
    const deleteLesson = await Subject.findByIdAndDelete(req.params.id);
    if (!deleteLesson) {
      return res.status(404).json({
        success: false,
        error: `Lesson not found with id ${req.params.id}`,
      });
    }
    res.status(200).json({ success: true, data: {} });
  } catch (error) {
    res.status(400).json({ success: false, error: error.message });
  }
};

// @desc Delete CQ in a lesson
// @route DELETE /lessons/CQ
// @access Private
const deleteCQInLessonByCQId = async (req, res) => {
  const { lessonId, CQId } = req.body;
  try {
    const newLesson = await Lesson.findOneAndUpdate(
      { _id: lessonId }, // Replace lessonId with the actual ID of the lesson document
      { $pull: { constructiveQuestions: CQId } } // Replace constructiveQuestionId with the ID of the object you want to remove
    );
    res.status(200).json({ newLesson });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "Something went wrong" });
  }
};

module.exports = {
  getLesson,
  getLessonWithSearch,
  importFromXlSX,
  getLessonById,
  createLesson,
  updateLesson,
  deleteLesson,
  deleteCQInLessonByCQId,
};
