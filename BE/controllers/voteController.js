const Comment = require("../models/Comment");
const ConstructiveQuestion = require("../models/ConstructiveQuestion");
const Schedule = require("../models/Schedule");
const Vote = require("../models/Vote");

// @route POST /votes
// @access Private
const vote = async (req, res) => {
  try {
    const {
      scheduleId,
      CQId,
      commentId,
      type,
      star,
      votedBy,
      card,
      isTrainer,
    } = req.body;
    if (
      !scheduleId ||
      !CQId ||
      !commentId ||
      !type ||
      !votedBy ||
      !star ||
      !card
    ) {
      return res.status(400).json({ message: "Missing required field" });
    }
    const schedule = await Schedule.findById(scheduleId);
    let constructiveQuestion = null;

    if (schedule) {
      // Iterate through lessons in the schedule
      for (const lesson of schedule.lessons) {
        // Iterate through constructiveQuestions in the lesson
        for (const cq of lesson.constructiveQuestions) {
          if (cq._id.toString() === CQId) {
            // Found the desired constructiveQuestion
            constructiveQuestion = cq;
          }
        }
      }
      if (!constructiveQuestion) {
        const cq = await ConstructiveQuestion.findById(CQId);
        constructiveQuestion = cq;
      }
    } else {
      return res
        .status(400)
        .json({ message: "comment created Fail : no schedule match" });
    }

    if (constructiveQuestion) {
      if (new Date(constructiveQuestion.to) > new Date()) {
        // Determine the filter keyword based on isTrainer and filter values
        const filterKeyWord = isTrainer
          ? "trainer"
          : type === "inside-group"
          ? "inside"
          : "outside";

        const duplicateVote = await Vote.findOne({
          schedule: scheduleId,
          constructiveQuestion: CQId,
          comment: commentId,
          type: filterKeyWord,
          card,
          star,
          votedBy,
        });

        if (duplicateVote) {
          await Vote.findByIdAndDelete(duplicateVote._id);
          return res.status(200).json({ message: "Voted remove" });
        }

        const existedVoteOnComment = await Vote.findOne({
          schedule: scheduleId,
          constructiveQuestion: CQId,
          comment: commentId,
          type: filterKeyWord,
          votedBy,
        });

        if (existedVoteOnComment) {
          await Vote.findByIdAndUpdate(existedVoteOnComment._id, {
            card,
            star,
          });
          return res.status(200).json({ message: "Vote updated" });
        }

        const newVote = new Vote({
          schedule: scheduleId,
          constructiveQuestion: CQId,
          comment: commentId,
          type: filterKeyWord,
          card,
          star,
          votedBy,
        });

        const updatedComment = await Comment.findByIdAndUpdate(commentId, {
          $addToSet: { votes: newVote._id },
        });

        if (!updatedComment) {
          return res.status(400).json({ message: "vote created Fail" + error });
        }

        await newVote.save();
        return res.status(200).json({ message: "Voted successfully" });
      } else {
        return res.status(400).json({ message: "Discuss time is over !" });
      }
    } else {
      return res.status(400).json({ message: "No CQ found !" });
    }
  } catch (error) {
    console.error("Error creating  vote:", error);
    return res.status(400).json({ message: "vote created Fail" + error });
  }
};
// @route POST /votes/remain
// @access Private
const getRemainVotesOfUser = async (req, res) => {
  try {
    const { scheduleId, CQId, userId, filter, isTrainer } = req.body;
    if (!scheduleId || !CQId || !userId) {
      return res.status(400).json({ message: "Missing required field" });
    }

    const constructiveQuestion = await ConstructiveQuestion.findById(CQId);

    // Determine the filter keyword based on isTrainer and filter values
    const filterKeyWord = isTrainer
      ? "trainer"
      : filter === "inside-group"
      ? "inside"
      : "outside";

    // Fetch votes that match the given criteria
    const votes = await Vote.find({
      schedule: scheduleId,
      constructiveQuestion: CQId,
      votedBy: userId,
      type: filterKeyWord,
    });

    // Get the vote settings for the specified filter keyword
    const voteSettingOfUser = {
      redCard: constructiveQuestion.voteSetting.redCard[`${filterKeyWord}Vote`],
      blueCard:
        constructiveQuestion.voteSetting.blueCard[`${filterKeyWord}Vote`],
      greenCard:
        constructiveQuestion.voteSetting.greenCard[`${filterKeyWord}Vote`],
      blackCard:
        constructiveQuestion.voteSetting.blackCard[`${filterKeyWord}Vote`],
    };

    // Calculate the total votes for each card from the votes array
    const totalVotes = votes.reduce((acc, vote) => {
      acc[vote.card] = (acc[vote.card] || 0) + 1;
      return acc;
    }, {});

    // Calculate the remaining votes for each card by subtracting the total votes from the vote settings' quantity
    const remainingVotes = {
      redCard: Math.max(
        voteSettingOfUser.redCard.quantity - (totalVotes["red"] || 0),
        0
      ),
      blueCard: Math.max(
        voteSettingOfUser.blueCard.quantity - (totalVotes["blue"] || 0),
        0
      ),
      greenCard: Math.max(
        voteSettingOfUser.greenCard.quantity - (totalVotes["green"] || 0),
        0
      ),
      blackCard: Math.max(
        voteSettingOfUser.blackCard.quantity - (totalVotes["black"] || 0),
        0
      ),
    };
    return res.status(200).json(remainingVotes);
  } catch (error) {
    console.error("Error getting votes:", error);
    return res.status(500).json({ message: "Error getting votes" });
  }
};

module.exports = {
  vote,
  getRemainVotesOfUser,
};
