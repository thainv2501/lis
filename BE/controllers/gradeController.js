const Grade = require("../models/Grade");
const Presentation = require("../models/Presentation");

// @route POST /grade
// @access Private
const getGrade = async (req, res) => {
  try {
    const { scheduleId, CQId } = req.body;
    if (!scheduleId || !CQId) {
      return res.status(400).json({ message: "Missing required field" });
    }
    const presentations = await Presentation.find({
      schedule: scheduleId,
      constructiveQuestion: CQId,
    })
      .populate({ path: "presentationGroup", module: "Group" })
      .populate({ path: "reviewGroup", module: "Group" })
      .populate({ path: "grade", module: "Grade" })
      .sort({ createdAt: 1 });
    return res.status(200).json(presentations);
  } catch (error) {
    console.error("Error :", error);
    return res.status(400).json({ message: "no presentation found" + error });
  }
};
// @route POST /grade/create
// @access Private
const createGrade = async (req, res) => {
  try {
    const {
      presentation,
      votedFor,
      votedBy,
      hardWorking,
      goodSkill,
      goodInformation,
      teamWorking,
      keepTime,
      meetRequirements,
      presentations,
    } = req.body;

    if (!votedBy) {
      return res.status(400).json({ message: "Missing required field" });
    }

    const newGrade = new Grade({
      presentation,
      votedFor,
      votedBy,
      hardWorking,
      goodSkill,
      goodInformation,
      teamWorking,
      keepTime,
      meetRequirements,
      presentations,
    });

    if (presentation) {
      //check this user ever vote for this presentaion ever, if yes then update that grade
      const duplicateGrade = await Grade.findOne({ presentation, votedBy });
      if (duplicateGrade) {
        await Grade.findByIdAndUpdate(duplicateGrade, {
          hardWorking,
          goodSkill,
          goodInformation,
          teamWorking,
          keepTime,
          meetRequirements,
          presentations,
        });
        return res.status(200).json({ message: "Grade success" });
      }
      await Presentation.findByIdAndUpdate(presentation, {
        $addToSet: { grade: newGrade._id },
      });
    }

    await newGrade.save();
    return res.status(200).json({ message: "create grade success" });
  } catch (error) {
    console.error("Error getting votes:", error);
    return res.status(500).json({ message: "Error create grade" });
  }
};

module.exports = {
  getGrade,
  createGrade,
};
