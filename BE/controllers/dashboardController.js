const MasterData = require("../models/MasterData");
const User = require("../models/User");
const Subject = require("../models/Subject");
const Class = require("../models/Class");
const SupportQuestion = require("../models/SupportQuestion");
const Meeting = require("../models/Meeting");

// @route POST /dashboard
// @access Private
const getDashboardBySemesterId = async (req, res) => {
  const { semesterId } = req.body;
  if (!semesterId) {
    return res.status(400).json({ message: `Missing required field!` });
  }

  try {
    const semester = await MasterData.findById(semesterId);

    if (!semester) {
      return res.status(404).json({ message: `Semester not found!` });
    }
    //caculate users
    const totalUsersCreatedInSemester = await User.countDocuments({
      createdAt: {
        $gte: semester.from,
        $lte: semester.to,
      },
      active: true,
    });

    const totalUsersInSemester = await User.countDocuments({
      createdAt: {
        $lte: semester.to,
      },
      active: true,
    });
    //caculate subjects
    const totalSubjectsCreatedInSemester = await Subject.countDocuments({
      createdDate: {
        $gte: semester.from,
        $lte: semester.to,
      },
      status: true,
    });
    const totalSubjectsInSemester = await Subject.countDocuments({
      createdDate: {
        $lte: semester.to,
      },
      status: true,
    });

    // caculate class
    const totalClassesInSemester = await Class.countDocuments({
      semester: semesterId,
      active: true,
    });
    // caculate support questions

    // Initialize an object to hold the counts for each month
    const questionsAndMeetingsCountsByMonth = {};

    // Create an array of month-year strings within the semester
    const startDate = new Date(semester.from);
    const endDate = new Date(semester.to);

    let currentDate = new Date(startDate);
    while (currentDate <= endDate) {
      const monthYear = currentDate.toISOString().substring(0, 7);
      questionsAndMeetingsCountsByMonth[monthYear] = {
        supportQuestions: 0,
        subjectQuestions: 0,
        meetings: 0,
      };
      currentDate.setUTCMonth(currentDate.getUTCMonth() + 1);
    }

    // Calculate support questions by month within the semester
    const supportQuestionsInSemester = await SupportQuestion.find({
      createdDate: {
        $gte: semester.from,
        $lte: semester.to,
      },
      type: "support-question",
    });

    supportQuestionsInSemester.forEach((question) => {
      const createdDate = question.createdDate;
      const monthYear = createdDate.toISOString().substring(0, 7);

      if (questionsAndMeetingsCountsByMonth[monthYear]) {
        questionsAndMeetingsCountsByMonth[monthYear].supportQuestions++;
      }
    });

    // Calculate subject questions by month within the semester
    const subjectQuestionsInSemester = await SupportQuestion.find({
      createdDate: {
        $gte: semester.from,
        $lte: semester.to,
      },
      type: "subject-question",
    });

    subjectQuestionsInSemester.forEach((question) => {
      const createdDate = question.createdDate;
      const monthYear = createdDate.toISOString().substring(0, 7);

      if (questionsAndMeetingsCountsByMonth[monthYear]) {
        questionsAndMeetingsCountsByMonth[monthYear].subjectQuestions++;
      }
    });
    // Calculate meetings by month within the semester
    const meetingsInSemester = await Meeting.find({
      createdAt: {
        $gte: semester.from,
        $lte: semester.to,
      },
    });

    meetingsInSemester.forEach((meeting) => {
      const createdDate = meeting.createdAt;
      const monthYear = createdDate.toISOString().substring(0, 7);

      if (questionsAndMeetingsCountsByMonth[monthYear]) {
        questionsAndMeetingsCountsByMonth[monthYear].meetings++;
      }
    });

    // Convert questionsAndMeetingsCountsByMonth object to an array of objects
    const resultArray = Object.keys(questionsAndMeetingsCountsByMonth).map(
      (monthYear) => ({
        name: monthYear,
        supportQuestions:
          questionsAndMeetingsCountsByMonth[monthYear].supportQuestions,
        subjectQuestions:
          questionsAndMeetingsCountsByMonth[monthYear].subjectQuestions,
        meetings: questionsAndMeetingsCountsByMonth[monthYear].meetings,
      })
    );

    //return result
    const response = {
      totalUsersCreatedInSemester,
      totalUsersInSemester,
      totalSubjectsCreatedInSemester,
      totalSubjectsInSemester,
      totalClassesInSemester,
      questionsAndMeetingsCountsByMonth: resultArray,
    };

    return res.status(200).json(response);
  } catch (error) {
    return res.status(500).json({ message: "Error: " + error });
  }
};

module.exports = {
  getDashboardBySemesterId,
};
