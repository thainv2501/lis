const SupportQuestion = require('../models/SupportQuestion');
const Comment = require('../models/Comment');
const Reply = require('../models/Reply');

// Hàm để tạo mới một reply cho một support question
const createReply = async (req, res) => {
  try {
    const { content, createdBy, id, isComment } = req.body;

    let parentObject;

    if (isComment) {
      // Kiểm tra xem support question có tồn tại hay không
      parentObject = await Comment.findById(id);
    } else {
      parentObject = await SupportQuestion.findById(id);
    }

    // Kiểm tra xem đối tượng cha có tồn tại hay không
    if (!parentObject) {
      return res.status(404).json({
        error: `${isComment ? 'Comment' : 'Support question'} object not found`,
      });
    }

    // Tạo mới một reply
    const newReply = new Reply({
      content,
      createdBy,
    });

    // Lưu reply vào database
    const savedReply = await newReply.save();

    // Thêm reply vào trường reply của support question
    parentObject.reply.push(savedReply._id);
    await parentObject.save();

    res.json({ message: 'Reply created successfully', reply: savedReply });
  } catch (error) {
    console.error('Error creating reply:', error);
    return res.status(500).json({ error: 'Server error' });
  }
};

// Hàm để xoá một reply trong một câu hỏi SupportQuestion
const deleteReply = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id) {
      return res.status(400).json({ message: 'Missing required field !' });
    }

    await Reply.findByIdAndDelete(id);

    res.json({ message: 'Reply deleted successfully' });
  } catch (error) {
    console.error('Error deleting reply:', error);
    return res.status(500).json({ message: 'Server error' });
  }
};

const updateReply = async (req, res) => {
  try {
    const { replyId, content } = req.body;
    console.log(replyId, content);
    // Tìm replyQuestion cần cập nhật trong CSDL
    const reply = await Reply.findById(replyId);

    if (!reply) {
      return res.status(404).json({ success: false, error: 'Reply not found' });
    }

    // Cập nhật nội dung cho reply
    if (content) {
      reply.content = content;
    }

    // Lưu lại sự thay đổi vào CSDL
    await reply.save();

    // Trả về kết quả cập nhật thành công
    res.json({ success: true });
  } catch (error) {
    res.status(500).json({ success: false, error: error.message });
  }
};

const getAllReplyBySupportQuestionId = async (req, res) => {
  try {
    const { id, filter, page } = req.body;
    const limit = 8;
    let totalRepliesCount = 0;

    // Truy vấn câu hỏi hỗ trợ dựa trên ID
    const supportQuestion = await SupportQuestion.findById(id).populate({
      path: 'createdBy',
      model: 'User',
    });

    // Kiểm tra xem câu hỏi có tồn tại hay không
    if (!supportQuestion) {
      return res.status(404).json({ error: 'Support question not found' });
    }

    totalRepliesCount = await Reply.countDocuments({
      _id: { $in: supportQuestion.reply },
    });

    // Lấy danh sách các reply liên quan đến câu hỏi này
    let replies = await Reply.find({ _id: { $in: supportQuestion.reply } })
      .sort(filter === 'latest' ? { createdDate: -1 } : { createdDate: 1 })
      .populate({ path: 'createdBy', model: 'User' })
      .skip((page - 1) * limit)
      .limit(limit)
      .exec();
    // Trả về thông tin câu hỏi hỗ trợ và danh sách các reply
    res.json({ totalRepliesCount, replies });
  } catch (error) {
    res.status(500).json({ error: 'Server error' });
  }
};

module.exports = {
  createReply,
  deleteReply,
  updateReply,
  getAllReplyBySupportQuestionId,
};
