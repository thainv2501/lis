const ConstructiveQuestion = require("../models/ConstructiveQuestion");
const Schedule = require("../models/Schedule");

//method POST /constructiveQuestions
const createConstructiveQuestion = async (req, res) => {
  try {
    const {
      scheduleId,
      title,
      content,
      from,
      to,
      createdBy,
      displaySetting,
      voteSetting,
    } = req.body;


    // Create a new ConstructiveQuestion document
    const newConstructiveQuestion = new ConstructiveQuestion({
      scheduleId,
      title: title.trim(),
      content: content.trim(),
      from,
      to,
      createdBy,
      displaySetting,
      voteSetting,
    });

    // Save the new ConstructiveQuestion to the database
    const createdCQ = await newConstructiveQuestion.save();

    res.status(200).json({
      message: "Constructive question created successfully",
      createdCQ,
    });
  } catch (error) {
    console.error("Error creating constructive question:", error);
    res.status(500).json({ message: "Failed to create constructive question" });
  }
};

//method /PATCH /constructiveQuestions
const updateConstructiveQuestion = async (req, res) => {
  try {
    const { constructiveQuestionId, updates } = req.body;
    if (!constructiveQuestionId) {
      return res.status(400).json({ message: "Missing required field" });
    }
    const constructiveQuestion = await ConstructiveQuestion.findByIdAndUpdate(
      constructiveQuestionId,
      updates,
      { new: true }
    );

    if (!constructiveQuestion) {
      return res
        .status(404)
        .json({ message: "Constructive question not found" });
    }

    res.status(200).json({
      message: "Constructive question updated successfully",
      constructiveQuestion,
    });
  } catch (error) {
    console.error("Error updating constructive question:", error);
    res.status(500).json({ message: "Failed to update constructive question" });
  }
};

//method PATCH /constructiveQuestions/time
const updateConstructiveQuestionTime = async (req, res) => {
  try {
    const { constructiveQuestionId, scheduleId, from, to } = req.body;
    if (!constructiveQuestionId) {
      return res.status(400).json({ message: "Missing required field" });
    }

    // Update the top-level constructiveQuestion
    const constructiveQuestion = await ConstructiveQuestion.findById(
      constructiveQuestionId
    );

    if (!constructiveQuestion) {
      return res
        .status(404)
        .json({ message: "Constructive question not found" });
    }

    // Find and update the lessons containing the same constructiveQuestionId
    const schedule = await Schedule.findById(scheduleId);

    if (!schedule) {
      return res.status(404).json({ message: "Schedule not found" });
    }

    // Check if the constructiveQuestionId exists in the schedule
    const cqInSchedule = schedule.constructiveQuestions.some(
      (cq) => cq.toString() === constructiveQuestionId
    );
    if (cqInSchedule) {
      await ConstructiveQuestion.findByIdAndUpdate(constructiveQuestionId, {
        from,
        to,
        done: true,
      });
      await Schedule.findByIdAndUpdate(scheduleId, { done: true });
      return res.status(200).json({
        message: "Constructive question time updated successfully",
        constructiveQuestion,
      });
    } else {
      schedule.lessons.forEach((lesson) => {
        if (
          lesson.constructiveQuestions &&
          lesson.constructiveQuestions.some(
            (cq) => cq._id.toString() === constructiveQuestionId
          )
        ) {
          lesson.constructiveQuestions = lesson.constructiveQuestions.map(
            (cq) => {
              if (cq._id.toString() === constructiveQuestionId) {
                return {
                  ...constructiveQuestion.toObject(),
                  from,
                  to,
                  done: true,
                };
              }
              return cq;
            }
          );
        }
      });

      // Save the updated schedule document
      await Schedule.findByIdAndUpdate(scheduleId, {
        lessons: schedule.lessons,
        done: true,
      });
      return res.status(200).json({
        message: "Constructive question time updated successfully",
        constructiveQuestion,
      });
    }
  } catch (error) {
    console.error("Error updating constructive question:", error);
    res.status(500).json({ message: "Failed to update constructive question" });
  }
};

//method DELETE /constructiveQuestions
const deleteCQById = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id) {
      return res.status(412).json({ message: "Missing required Field !" });
    }

    await ConstructiveQuestion.findByIdAndDelete(id);
    res.status(200).json({
      message: "Constructive question delete successfully",
    });
  } catch (error) {
    console.error("Error delete constructive question:", error);
    res.status(500).json({ message: "Failed to delete constructive question" });
  }
};

//method GET /constructiveQuestions/:id
const getCQById = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id) {
      return res.status(412).json({ message: "Missing required Field !" });
    }

    const constructiveQuestion = await ConstructiveQuestion.findById(id);
    if (!constructiveQuestion) {
      return res
        .status(500)
        .json({ message: "Constructive Question not found !" });
    }
    return res.status(200).json(constructiveQuestion);
  } catch (error) {
    console.error("Error find constructive question:", error);
    return res
      .status(500)
      .json({ message: "Failed to find constructive question" });
  }
};

module.exports = {
  createConstructiveQuestion,
  updateConstructiveQuestion,
  updateConstructiveQuestionTime,
  deleteCQById,
  getCQById,
};
