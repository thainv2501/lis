const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mailer = require("../utils/mailer");

// @desc Login
// @route POST /auth
// @access Public
const login = async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).json({ message: "All fields are required" });
  }

  const foundUser = await User.findOne({ email: email.trim() });

  if (!foundUser || !foundUser.active) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  const match = await bcrypt.compare(password, foundUser.password);

  if (!match) {
    return res.status(401).json({ message: "Wrong email or password" });
  }

  if (!foundUser.emailVerified) {
    return res
      .status(401)
      .json({ message: "Check your email and verify your email !" });
  }

  const accessToken = jwt.sign(
    {
      data: {
        userLogin: foundUser,
      },
    },
    process.env.ACCESS_TOKEN_SECRET,
    { expiresIn: "60m" }
  );

  const refreshToken = jwt.sign(
    { email: foundUser.email },
    process.env.REFRESH_TOKEN_SECRET,
    { expiresIn: "7d" }
  );

  // Create secure cookie with refresh token
  res.cookie("jwt", refreshToken, {
    httpOnly: true, //accessible only by web server
    secure: true, //https
    sameSite: "None", //cross-site cookie
    maxAge: 7 * 24 * 60 * 60 * 1000, //cookie expiry: set to match rT
  });

  // Send accessToken containing email and roles
  res.json({ accessToken });
};

// @desc Refresh
// @route GET /auth/refresh
// @access Public - because access token has expired
const refresh = (req, res) => {
  const cookies = req.cookies;

  if (!cookies?.jwt) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  const refreshToken = cookies.jwt;

  jwt.verify(
    refreshToken,
    process.env.REFRESH_TOKEN_SECRET,
    async (err, decoded) => {
      if (err) return res.status(403).json({ message: "Forbidden" });

      const foundUser = await User.findOne({
        email: decoded.email,
      });

      if (!foundUser) return res.status(401).json({ message: "Unauthorized" });

      const accessToken = jwt.sign(
        {
          data: {
            userLogin: foundUser,
          },
        },
        process.env.ACCESS_TOKEN_SECRET,
        { expiresIn: "15m" }
      );

      res.json({ accessToken });
    }
  );
};

// @desc Logout
// @route POST /auth/logout
// @access Public - just to clear cookie if exists
const logout = (req, res) => {
  const cookies = req.cookies;
  if (!cookies?.jwt) return res.sendStatus(204); //No content
  res.clearCookie("jwt", { httpOnly: true, sameSite: "None", secure: true });
  res.json({ message: "Cookie cleared" });
};

// @desc register
// @route Post /register
// @access Private
const register = async (req, res) => {
  const { fullName, phoneNumber, email, password, roles } = req.body;

  // Confirm data
  if (!email || !password || !fullName || !phoneNumber) {
    return res.status(400).json({ message: "All fields are required" });
  }

  // Check for duplicate email
  const duplicate = await User.findOne({ email });

  if (duplicate) {
    return res.status(409).json({ message: "Duplicate emails" });
  }

  //else no duplicate user email then :

  bcrypt
    .hash(email, 10)
    .then((hashedEmail) => {
      console.log(
        `${process.env.APP_URL}/verify?email=${email}&token=${hashedEmail}`
      );
      mailer
        .sendMail(
          email,
          "Verify Email from LIS",
          `<p>Verify this email to finish your register and login to system</p>
        <p>click on the link : </p>
        <a href="${process.env.APP_URL}/verify?email=${email}&token=${hashedEmail}"> Verify </a>`
        )
        .catch((error) => {
          console.log(error);
          res.status(400).json({ message: "Email sent failed !" });
        });
    })
    .catch(() => {
      res.status(400).json({ message: "An error while hashing email" });
    });

  // Hash password
  const hashedPwd = await bcrypt.hash(password, 10); // salt rounds

  const userObject =
    !Array.isArray(roles) || !roles.length
      ? { email, password: hashedPwd, fullName, phoneNumber }
      : { email, password: hashedPwd, fullName, phoneNumber, roles };

  // Create and store new user
  const user = await User.create(userObject);

  if (user) {
    //created
    res.status(201).json({ message: `New user ${email} created` });
  } else {
    res.status(400).json({ message: "Invalid user data received" });
  }
};

// @desc send reset password to email
// @route post /auth/sendResetPassword
// @access Private

const sendResetPassword = async (req, res) => {
  const { email } = req.body;

  // Confirm data
  if (!email) {
    return res.status(400).json({ message: "All fields are required" });
  }

  // Check for  email
  const user = await User.findOne({ email });

  if (!user) {
    return res.status(409).json({ message: "No existing email" });
  }

  //else no duplicate user email then :
  // Flag to track if response has been sent
  let responseSent = false;

  // Send reset password email
  try {
    const hashedEmail = await bcrypt.hash(email, 10);
    mailer
      .sendMail(
        email,
        "Request reset Password from LIS",
        `<p>Verify this email to reset your password and login to system</p>
      <p>Click on the link:</p>
      <a href="${process.env.APP_URL}/reset-password?email=${email}&token=${hashedEmail}">Reset Password</a>`
      )
      .catch((error) => {
        console.log(error);
        if (!responseSent) {
          res.status(400).json({ message: "Email sent failed!" });
          responseSent = true;
        }
      });
    if (!responseSent) {
      res
        .status(200)
        .json({ message: "Send reset password request email successful!" });
      responseSent = true;
    }
  } catch (error) {
    if (!responseSent) {
      res
        .status(400)
        .json({ message: "An error occurred while hashing email" });
      responseSent = true;
    }
  }

  // Create and store new user
};

// @desc verify email
// @route post /auth/verifyEmail
// @access Private

const verifyEmail = async (req, res) => {
  const { email, token } = req.body;
  const user = await User.findOne({ email });
  if (!user) {
    return res.status(400).json({ message: "User not found" });
  }
  const match = await bcrypt.compare(email, token);
  if (match) {
    try {
      await User.updateOne({ _id: user._id }, { emailVerified: true });
      return res.status(200).json({ message: "verify email successful !" });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "verify email fail !" });
    }
  } else {
    res.status(400).json({
      message: "there are something wrong with email and token !",
    });
  }
};

// @desc Update a user password
// @route PATCH /auth/resetPassword
// @access Private
const resetPassword = async (req, res) => {
  const { email, password } = req.body;

  // Confirm data
  if (!email || !password) {
    return res.status(400).json({ message: "All fields are required" });
  }

  // Does the user exist to update?
  // Check for  email
  const user = await User.findOne({ email });

  if (!user) {
    return res.status(400).json({ message: "User not found" });
  }

  // Hash password
  const hashPassword = await bcrypt.hash(password, 10); // salt rounds

  try {
    await User.updateOne({ _id: user._id }, { password: hashPassword });
    return res
      .status(200)
      .json({ message: `Password for ${user.email} updated` });
  } catch (error) {
    return res.status(500).json({ message: "Reset password failed" });
  }
};

module.exports = {
  login,
  refresh,
  logout,
  register,
  verifyEmail,
  sendResetPassword,
  resetPassword,
};
