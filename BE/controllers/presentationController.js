const Comment = require("../models/Comment");
const ConstructiveQuestion = require("../models/ConstructiveQuestion");
const Grade = require("../models/Grade");
const Presentation = require("../models/Presentation");
const Vote = require("../models/Vote");

// @desc Get presentations
// @route POST /presentations/get
// @access Private
const getPresentations = async (req, res) => {
  try {
    const { scheduleId, CQId } = req.body;
    if (!scheduleId || !CQId) {
      return res.status(400).json({ message: "Missing required field" });
    }
    const presentations = await Presentation.find({
      schedule: scheduleId,
      constructiveQuestion: CQId,
    })
      .populate({ path: "presentationGroup", module: "Group" })
      .populate({ path: "reviewGroup", module: "Group" })
      .populate({ path: "grade", module: "Grade" })
      .sort({ createdAt: 1 });
    return res.status(200).json(presentations);
  } catch (error) {
    console.error("Error creating  vote:", error);
    return res.status(400).json({ message: "no presentation found" + error });
  }
};
// @desc create presentation
// @route POST /presentations
// @access Private
const createPresentation = async (req, res) => {
  try {
    const { scheduleId, CQId, presentationGroup, reviewGroup } = req.body;
    if (!scheduleId || !CQId || !presentationGroup || !reviewGroup) {
      return res.status(400).json({ message: "Missing required field" });
    }

    const duplicatePresentation = await Presentation.findOne({
      schedule: scheduleId,
      constructiveQuestion: CQId,
      presentationGroup,
      reviewGroup,
    });
    if (duplicatePresentation) {
      return res.status(400).json({ message: "This Presentation created !" });
    }
    const newPresentation = new Presentation({
      schedule: scheduleId,
      constructiveQuestion: CQId,
      presentationGroup,
      reviewGroup,
    });

    await newPresentation.save();
    return res.status(200).json({ message: "create presentation success" });
  } catch (error) {
    console.error("Error getting votes:", error);
    return res.status(500).json({ message: "Error create presentation" });
  }
};
// @desc restart presentation
// @route Patch /presentations
// @access Private
const restartPresentation = async (req, res) => {
  try {
    const { presentation } = req.body;
    if (!presentation) {
      return res.status(400).json({ message: "Missing required field" });
    }

    const presentationFound = await Presentation.findById(presentation);
    if (!presentationFound) {
      return res.status(400).json({ message: "No Presentation found !" });
    }
    // Delete associated vote
    await Grade.deleteMany({ _id: { $in: presentationFound.grade } });
    return res.status(200).json({ message: "restart success" });
  } catch (error) {
    console.error("Error getting votes:", error);
    return res.status(500).json({ message: "Error create presentation" });
  }
};

module.exports = {
  getPresentations,
  createPresentation,
  restartPresentation,
};
