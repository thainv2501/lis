const Comment = require("../models/Comment");
const Reply = require("../models/Reply");
const Vote = require("../models/Vote");
const Schedule = require("../models/Schedule");
const ConstructiveQuestion = require("../models/ConstructiveQuestion");

// @route POST /comments
// @access Private
const createComment = async (req, res) => {
  try {
    const { scheduleId, CQId, createdBy, content } = req.body;
    if (!scheduleId || !CQId || !content || !createdBy) {
      return res.status(400).json({ message: "Missing required field" });
    }
    // check inside CQ of lessons is overtime or not
    const schedule = await Schedule.findById(scheduleId);
    let constructiveQuestion = null;

    if (schedule) {
      // Iterate through lessons in the schedule
      for (const lesson of schedule.lessons) {
        // Iterate through constructiveQuestions in the lesson
        for (const cq of lesson.constructiveQuestions) {
          if (cq._id.toString() === CQId) {
            // Found the desired constructiveQuestion
            constructiveQuestion = cq;
          }
        }
      }
      if (!constructiveQuestion) {
        const cq = await ConstructiveQuestion.findById(CQId);
        constructiveQuestion = cq;
      }
    } else {
      return res
        .status(400)
        .json({ message: "comment created Fail : no schedule match" });
    }

    if (constructiveQuestion) {
      if (new Date(constructiveQuestion.to) > new Date()) {
        const newComment = new Comment({
          schedule: scheduleId,
          constructiveQuestion: CQId,
          createdBy,
          content,
        });
        await newComment.save();
        return res
          .status(200)
          .json({ message: "comment created successfully" });
      } else {
        // console.log(constructiveQuestion);
        return res.status(400).json({ message: "Discuss time is over !" });
      }
    } else {
      return res.status(400).json({ message: "Questions Data Not Found !" });
    }
  } catch (error) {
    console.error("Error creating  comment:", error);
    return res.status(400).json({ message: "comment created Fail" + error });
  }
};

// @route POST /comments/filter
// @access Private
const getCommentsWithFilter = async (req, res) => {
  try {
    const { scheduleId, CQId, filter, members, page } = req.body;
    const limit = 5;
    if (!scheduleId || !CQId || !members || !filter) {
      return res.status(400).json({ message: "Missing required field" });
    }

    const memberIds = members.map((member) => member._id);

    // Check the filter value. If it is "inside-group," fetch comments with the specified conditions.
    if (filter === "inside-group") {
      // Replace 'CommentModel' with the actual model representing your comments.
      // Assuming the comments are stored in a MongoDB collection named 'comments'.
      const comments = await Comment.find({
        schedule: scheduleId,
        constructiveQuestion: CQId,
        createdBy: { $in: memberIds }, // Use $in to match createdBy against the members array.
      })
        .populate({ path: "createdBy", model: "User" })
        .populate({ path: "votes", model: "Vote" })
        .populate({
          path: "reply",
          model: "Reply",
          populate: { path: "createdBy", model: "User" },
          options: { sort: { createdDate: -1 } },
        })
        .sort({ createdAt: -1 })
        .skip((parseInt(page) - 1) * limit);
      const commentsCount = await Comment.countDocuments({
        schedule: scheduleId,
        constructiveQuestion: CQId,
        createdBy: { $in: memberIds }, // Use $in to match createdBy against the members array.
      });
      return res.status(200).json({ comments, commentsCount });
    }
    // Check the filter value. If it is "inside-group," fetch comments with the specified conditions.
    if (filter === "outside-group") {
      // Replace 'CommentModel' with the actual model representing your comments.
      // Assuming the comments are stored in a MongoDB collection named 'comments'.
      const comments = await Comment.find({
        schedule: scheduleId,
        constructiveQuestion: CQId,
        createdBy: { $nin: memberIds }, // Use $in to match createdBy against the members array.
      })
        .populate({ path: "createdBy", model: "User" })
        .populate({ path: "votes", model: "Vote" })
        .populate({
          path: "reply",
          model: "Reply",
          populate: { path: "createdBy", model: "User" },
          options: { sort: { createdDate: -1 } },
        })
        .sort({ createdAt: -1 })
        .skip((parseInt(page) - 1) * limit);

      const commentsCount = await Comment.countDocuments({
        schedule: scheduleId,
        constructiveQuestion: CQId,
        createdBy: { $in: memberIds }, // Use $in to match createdBy against the members array.
      });
      return res.status(200).json({ comments, commentsCount });
    }
  } catch (error) {
    console.error("Get comment:", error);
    return res.status(400).json({ message: "Get Comments Fail" + error });
  }
};
// @route POST /comments/all
// @access Private
const getAllComments = async (req, res) => {
  try {
    const { scheduleId, CQId } = req.body;
    if (!scheduleId || !CQId) {
      return res.status(400).json({ message: "Missing required field" });
    }
    // Replace 'CommentModel' with the actual model representing your comments.
    // Assuming the comments are stored in a MongoDB collection named 'comments'.
    const comments = await Comment.find({
      schedule: scheduleId,
      constructiveQuestion: CQId,
    })
      .populate({ path: "createdBy", model: "User" })
      .populate({ path: "votes", model: "Vote" })
      .populate({
        path: "reply",
        model: "Reply",
        populate: { path: "createdBy", model: "User" },
        options: { sort: { createdDate: -1 } },
      })
      .sort({ createdAt: -1 });
    return res.status(200).json(comments);
  } catch (error) {
    console.error("Get Comments :", error);
    return res.status(400).json({ message: "Get Comments Fail" + error });
  }
};
// @route PATCH /comments
// @access Private
const updateComment = async (req, res) => {
  try {
    const { commentId, content } = req.body;
    if (!commentId) {
      return res.status(400).json({ message: "Missing required field" });
    }

    await Comment.findByIdAndUpdate(commentId, { content });

    return res.status(200).json({ message: "update success !" });
  } catch (error) {
    console.error("Error creating  comment:", error);
    return res.status(400).json({ message: "comment created Fail" + error });
  }
};

// @route DELETE /comments
// @access Private
const deleteComment = async (req, res) => {
  try {
    const { id } = req.body;
    if (!id) {
      return res.status(400).json({ message: "Missing required field !" });
    }

    const comment = await Comment.findById(id);
    if (!comment) {
      return res.status(404).json({ message: "Comment not found" });
    }

    // Delete associated replies
    await Reply.deleteMany({ _id: { $in: comment.reply } });
    // Delete associated vote
    await Vote.deleteMany({ _id: { $in: comment.votes } });

    // Delete the comment
    await Comment.findByIdAndDelete(id);

    res.json({
      message: "Comment and associated replies deleted successfully",
    });
  } catch (error) {
    console.error("Error deleting comment:", error);
    return res.status(500).json({ message: "Server error" });
  }
};

module.exports = {
  createComment,
  getAllComments,
  getCommentsWithFilter,
  updateComment,
  deleteComment,
};
