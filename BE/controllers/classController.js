const Class = require("../models/Class");
const User = require("../models/User");
const Subject = require("../models/Subject");
const bcrypt = require("bcrypt");
const mailer = require("../utils/mailer");
const Lesson = require("../models/Lesson");
const Material = require("../models/Material");
const MasterData = require("../models/MasterData");

// @desc Get class data with id
// @route GET /class/:id
// @access Private
const getClassById = async (req, res) => {
  const { id } = req.params;
  const { userLogin } = req;

  try {
    // Check if the user is an Admin
    if (userLogin && userLogin.roles.includes("Admin")) {
      // If the user is an Admin, allow access to all class data
      const classData = await Class.findById(id)
        .populate("subject")
        .populate({ path: "semester", model: "MasterData" })
        .populate("trainer")
        .populate({ path: "students", model: "User" })
        .lean();

      if (!classData) {
        return res.status(400).json({ message: "No Class Found" });
      }
      await Lesson.populate(classData?.subject, {
        path: "lessons",
        model: "Lesson",
        options: { sort: { lessonChapter: 1, lessonNumber: 1 } },
        populate: [
          {
            path: "constructiveQuestions",
            model: "ConstructiveQuestion",
            populate: {
              path: "createdBy",
              model: "User",
            },
          },
          {
            path: "createdBy",
            model: "User",
          },
        ],
      });

      await Material.populate(classData?.subject?.lessons, {
        path: "materials",
        model: "Material",
        populate: {
          path: "uploadedBy",
          model: "User",
        },
      });

      return res.status(200).json(classData);
    }

    // Check if the user is a Trainer of the class or a Student in the class
    const isTrainerOrStudent = await Class.exists({
      _id: id,
      $or: [{ trainer: userLogin?._id }, { students: userLogin?._id }],
    });

    if (!isTrainerOrStudent) {
      // If the user is neither an Admin, Trainer, nor Student in the class, deny access
      return res.status(403).json({ message: "Unauthorized" });
    }

    // If the user is a Trainer or Student in the class, allow access to class data
    const classData = await Class.findById(id)
      .populate("subject")
      .populate({ path: "semester", model: "MasterData" })
      .populate("trainer")
      .populate({ path: "students", model: "User" })
      .lean();
    if (!classData) {
      return res.status(400).json({ message: "No Class Found" });
    }
    await Lesson.populate(classData?.subject, {
      path: "lessons",
      model: "Lesson",
      options: { sort: { lessonChapter: 1, lessonNumber: 1 } },
      populate: [
        {
          path: "constructiveQuestions",
          model: "ConstructiveQuestion",
          populate: {
            path: "createdBy",
            model: "User",
          },
        },
        {
          path: "createdBy",
          model: "User",
        },
      ],
    });

    await Material.populate(classData?.subject?.lessons, {
      path: "materials",
      model: "Material",
      populate: {
        path: "uploadedBy",
        model: "User",
      },
    });

    return res.status(200).json(classData);
  } catch (error) {
    return res.status(400).json({ message: "Error" + error });
  }
};

// @desc Get class data with semester id
// @route GET /class/semester/:id
// @access Private
// @author CuongTV
const getClassBySemesterId = async (req, res) => {
  try {
    const { semesterId } = req.params;
    const { userLogin } = req;
    console.log(semesterId);
    let filterClassesCondition = { active: true };
    if (semesterId) {
      if (userLogin && userLogin.roles.includes("Admin")) {
        filterClassesCondition = { semester: semesterId };
      } else {
        filterClassesCondition.$and = [{ semester: semesterId }];
        if (
          userLogin &&
          userLogin.roles.includes("Trainer") &&
          userLogin.roles.includes("Trainee")
        ) {
          const filterWithTrainerAndTraineeRoles = {};
          filterWithTrainerAndTraineeRoles.$or = [];

          filterWithTrainerAndTraineeRoles.$or.push({
            trainer: userLogin._id,
          });

          filterWithTrainerAndTraineeRoles.$or.push({
            students: userLogin._id,
          });

          filterClassesCondition.$and.push(filterWithTrainerAndTraineeRoles);
        } else {
          if (userLogin && userLogin.roles.includes("Trainer")) {
            filterClassesCondition.$and.push({
              trainer: userLogin._id,
            });
          }

          if (userLogin && userLogin.roles.includes("Trainee")) {
            filterClassesCondition.$and.push({
              students: userLogin._id,
            });
          }
        }
      }
      const classData = await Class.find(filterClassesCondition)
        .populate("subject")
        .populate({ path: "semester", model: "MasterData" })
        .populate("trainer")
        .populate({ path: "students", model: "User" })
        .populate({
          path: "subject",
          populate: { path: "lessons", model: "Lesson" },
        })
        .lean();
      return res.json(classData);
    } else {
      return res.json([]);
    }
  } catch (error) {
    return res.status(400).json({ message: "Error " + error });
  }
};

// @desc Get class data with query
// @route GET /class
// @access Private
const getClassBySearchQuery = async (req, res) => {
  const { page, semester, active, keyword } = req.query;
  const limit = 10;
  // Get all class data from MongoDB
  //validate search information
  const searchObj = {};

  if (active !== "all") searchObj.active = active === "active";
  if (semester !== "all") searchObj.semester = semester;

  // Search for referenced 'trainer' field
  const trainers = await User.find({
    $or: [
      { email: { $regex: keyword ? keyword.trim() : "", $options: "i" } },
      { fullName: { $regex: keyword ? keyword.trim() : "", $options: "i" } },
    ],
  });

  // Search for referenced 'subject' field
  const subjects = await Subject.find({
    $or: [
      {
        subjectName: { $regex: keyword ? keyword.trim() : "", $options: "i" },
      },
      {
        subjectCode: { $regex: keyword ? keyword.trim() : "", $options: "i" },
      },
    ],
  });

  // Create an array of trainer and subject IDs for searching
  const trainerIds = trainers.map((trainer) => trainer._id);
  const subjectIds = subjects.map((subject) => subject._id);

  if (trainerIds.length > 0) {
    searchObj.trainer = { $in: trainerIds };
  }

  if (subjectIds.length > 0) {
    searchObj.subject = { $in: subjectIds };
  }

  searchObj.$or = [
    { className: { $regex: keyword ? keyword.trim() : "", $options: "i" } }, // Case-insensitive regex search for
    { trainer: { $in: trainerIds } }, // Case-insensitive regex search for
    { subject: { $in: subjectIds } }, // Case-insensitive regex search for
  ];

  const classData = await Class.find(searchObj)
    .populate("subject") // Populate the 'subject' field using the referenced model
    .populate({
      path: "semester",
      model: "MasterData",
    }) // Populate the 'semester' field using the referenced model
    // .sort({ "semester.to": 1 }) // Sort the 'semester' field by 'to' in ascending order
    .populate("trainer") // Populate the 'trainer' field using the referenced model
    .populate({ path: "students", model: "User" }) // Populate the 'students' field using the referenced model
    .limit(limit)
    .skip((parseInt(page) - 1) * limit)
    .lean();

  classData.sort((a, b) => new Date(b.semester.to) - new Date(a.semester.to));

  const classCount = await Class.countDocuments(searchObj).exec();
  // // If no data
  if (classCount === 0) {
    return res.status(400).json({ message: "No data found" });
  }

  res.json({ classCount, classData });
};
// @desc Get class data with semester
// @route POST /class
// @access Private
const createClass = async (req, res) => {
  const { className, subjectId, semesterId, trainerId, note, studentIds } =
    req.body;

  if (!className || !subjectId || !semesterId || !trainerId || !studentIds) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    const existingClass = await Class.findOne({
      className: className,
      subject: subjectId,
      semester: semesterId,
    });

    if (existingClass) {
      return res.status(412).json({
        message:
          "A class with the same className, subjectId, and semesterId already exists",
      });
    }

    const newClass = new Class({
      className: className,
      subject: subjectId,
      semester: semesterId,
      trainer: trainerId,
      students: studentIds,
      note,
    });

    const createdClass = await newClass.save();
    return res.status(201).json({
      message: "Class Created successfully",
      createdClass,
    });
  } catch (error) {
    return res.status(400).json({ message: "Class Created fail" });
  }
};
// @route POST /class/import
// @access Private
const importClasses = async (req, res) => {
  const { excelData } = req.body;
  // const { className, subjectId, semesterId, trainerId, note, studentIds } =
  //   req.body;

  if (!excelData) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    await Promise.all(
      excelData.map(async (classItem) => {
        //check data is valid or not
        const semester = await MasterData.findOne({
          code: classItem.semesterCode,
        });
        const subject = await Subject.findOne({
          subjectCode: classItem.subjectCode,
        }).populate({ path: "trainers", model: "User" });
        if (semester && subject) {
          // now check trainer is in that subject or not
          if (
            subject.trainers.some(
              (trainer) => trainer.email === classItem.trainerEmail
            )
          ) {
            const trainer = await User.findOne({
              email: classItem.trainerEmail,
            });
            const existingClass = await Class.findOne({
              className: classItem.className,
              subject: subject._id,
              semester: semester._id,
            });

            if (existingClass) {
              console.log(
                `Duplicated Class ${semester.name} - ${classItem.className} - ${subject.subjectName}`
              );
              return;
            }

            const newClass = new Class({
              className: classItem.className,
              subject: subject,
              semester: semester,
              trainer,
              students: [],
            });

            await newClass.save();
            console.log(
              `Class created success ${semester.name} - ${classItem.className} - ${subject.subjectName}!`
            );
            return;
          } else {
            console.log(`Trainer not in this subject`);
            return;
          }
        } else {
          console.log(`Invalid data !`);
          return;
        }
      })
    );
    return res
      .status(200)
      .json({ message: "Import Classes from XLSX is successful" });
  } catch (error) {
    console.log(error);
    return res.status(400).json({ message: "Import classes fail" });
  }
};

// @route PATCH /class
// @access Private
const updateClass = async (req, res) => {
  const { id, className, active, note } = req.body;

  // Confirm data
  if (!id) {
    return res.status(400).json({ message: "Id are required" });
  }
  try {
    // Does the data exist to update?
    const classData = await Class.findById(id).exec();

    if (!classData) {
      return res.status(400).json({ message: "data not found" });
    }
    // Does the data exist to update?
    const duplicate = await Class.findOne({
      className,
      semester: classData?.semester,
      subject: classData?.subject,
    }).exec();

    if (duplicate) {
      return res.status(400).json({ message: "New Update Data Duplicated" });
    }

    await Class.updateOne({ _id: classData._id }, { className, active, note });

    res.json({ message: `${classData?.className} updated` });
  } catch (error) {
    return res.status(500).json({ message: `Error during process : ` + error });
  }
};

// @desc Get class data with semester
// @route POST /class/add-students
// @access Private
const addStudentsToClass = async (req, res) => {
  const { classId, students } = req.body;
  if (!classId || !students) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    await Promise.all(
      students.map(async (student) => {
        const existedStudent = await User.findOne({ email: student?.email });
        if (existedStudent) {
          if (!existedStudent?.roles?.includes("Trainee")) {
            try {
              await User.updateOne(
                { _id: existedStudent?._id },
                { roles: [...existedStudent.roles, "Trainee"] }
              );
              console.log("updated role");
            } catch (error) {
              console.log("update user: " + existedStudent?._id + " failed");
            }
          }
          try {
            await Class.updateOne(
              { _id: classId },
              { $addToSet: { students: existedStudent?._id } }
            );
            console.log("add existed student to class");
          } catch (error) {
            console.log("add existed student to class fail");
            return res.status(400).json({
              message: "Student add failed!",
            });
          }
          return;
        } else {
          // Create a random password for new user
          const length = 6;
          const charset =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          let password = "";

          for (let i = 0; i < length; i++) {
            const randomIndex = Math.floor(Math.random() * charset.length);
            password += charset[randomIndex];
          }
          try {
            const hashedEmail = await bcrypt.hash(student.email, 10);
            console.log(
              `${process.env.APP_URL}/verify?email=${student.email}&token=${hashedEmail}`
            );
            try {
              await mailer.sendMail(
                student.email,
                "Your account has been created for Learning Interactions Systems",
                `<p>Your password to login to the system:</p>
                <p>Your password is: ${password}</p>
                <p>Please reset your password after logging in for easier access.</p>
                <p>Verify this email to complete your registration and login to the system.</p>
                <p>Click on the link:</p>
                <a href="${process.env.APP_URL}/verify?email=${student.email}&token=${hashedEmail}">Verify</a>`
              );
            } catch (error) {
              return res
                .status(500)
                .json({ message: "An error occurred while sending the email" });
            }
          } catch (error) {
            return res
              .status(500)
              .json({ message: "An error occurred while hashing the email" });
          }

          // Hash password
          const hashedPwd = await bcrypt.hash(password, 10); // salt rounds
          const newUser = new User({
            email: student?.email,
            password: hashedPwd,
            fullName: student?.fullName,
            phoneNumber: student?.phoneNumber,
            emailVerified: true,
            roles: ["Trainee"],
          });
          try {
            const createdUser = await newUser.save();
            try {
              await Class.updateOne(
                { _id: classId },
                { $addToSet: { students: createdUser?._id } }
              );
            } catch (error) {
              return res.status(400).json({
                message: "Student add failed!",
              });
            }
          } catch (error) {
            console.log(`Error with student: ${student?.email}`);
          }
        }
      })
    );
  } catch (error) {
    console.log(`Error occurred during processing`);
  }
  return res.status(201).json({
    message: "Students added successfully",
  });
};
// @desc Get class data with semester
// @route POST /class/add-student
// @access Private
const addStudentToClass = async (req, res) => {
  const { classId, email } = req.body;
  if (!classId || !email) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    // Check for duplicate email
    const duplicate = await User.findOne({ email })
      .collation({ locale: "en", strength: 2 })
      .lean();
    if (duplicate) {
      await Class.findByIdAndUpdate(classId, {
        $addToSet: { students: duplicate._id },
      });
      return res.status(200).json({ message: "Add done" });
    } else {
      //else no duplicate user email then :
      //create a random password for new user
      const length = 6;
      const charset =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      let password = "";

      for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * charset.length);
        password += charset[randomIndex];
      }

      try {
        const hashedEmail = await bcrypt.hash(email, 10);
        try {
          await mailer.sendMail(
            email,
            "Your account have been created to Learning Interactions Systems",
            `<p>Your password to login in the system</p>
      <p>your password is :${password} </p>
      <p>Please reset your password after to login easier ! </p>
      <p>Verify this email to finish your register and login to system</p>
    <p>click on the link : </p>
    <a href="${process.env.APP_URL}/verify?email=${email}&token=${hashedEmail}"> Verify </a>`
          );
        } catch (error) {
          return res
            .status(500)
            .json({ message: "An error during sending email" });
        }
      } catch (error) {
        return res
          .status(500)
          .json({ message: "An error while hashing email" });
      }

      // Hash password
      const hashedPwd = await bcrypt.hash(password, 10); // salt rounds
      const newUser = new User({
        email,
        password: hashedPwd,
        fullName: `User${Math.random() * 1000000}`,
        phoneNumber: "0000000000",
        emailVerified: true,
        roles: ["Trainee"],
      });
      try {
        const createdUser = await newUser.save();
        try {
          await Class.findByIdAndUpdate(classId, {
            $addToSet: { students: createdUser?._id },
          });
        } catch (error) {
          return res.status(400).json({
            message: "Student add failed!",
          });
        }
      } catch (error) {
        console.log(`Error with student: ${student?.email}`);
      }
    }
  } catch (error) {
    console.log(`Error occurred during processing`);
    console.log(error);
    return res.status(401).json({ message: "Add student to class error " });
  }
  return res.status(201).json({
    message: "Students added successfully",
  });
};
// @route POST /class/remove-student
// @access Private
const removeStudentOutClass = async (req, res) => {
  const { classId, studentId } = req.body;
  if (!classId || !studentId) {
    return res.status(412).json({ message: "Missing required fields" });
  }

  try {
    await Class.findByIdAndUpdate(classId, {
      $pull: { students: studentId },
    });
    return res.status(200).json({ message: "remove success" });
  } catch (error) {
    console.log(`Error occurred during processing`);
    console.log(error);
    return res.status(401).json({ message: "remove student out class error " });
  }
};

module.exports = {
  createClass,
  importClasses,
  getClassById,
  getClassBySemesterId,
  getClassBySearchQuery,
  updateClass,
  addStudentsToClass,
  addStudentToClass,
  removeStudentOutClass,
};
