const MasterData = require("../models/MasterData");

// @desc Get all master data
// @access Private
const getMasterData = async (req, res) => {
  // Get all master data from MongoDB
  const masterData = await MasterData.find().lean();

  // If no users
  if (!masterData?.length) {
    return res.status(400).json({ message: "No master data found" });
  }

  res.json(masterData);
};

// @desc Get all master data with search condition
// @route GET /masterData
// @access Private

const getMasterDataWithSearch = async (req, res) => {
  const { page, type, active, keyword } = req.query;
  const limit = 10;
  // Get all master data from MongoDB
  //validate search information
  const searchObj = {};
  if (keyword) {
    searchObj.$or = [
      { code: { $regex: keyword.trim(), $options: "i" } }, // Case-insensitive regex search for phone number
      { name: { $regex: keyword.trim(), $options: "i" } }, // Case-insensitive regex search for fullname
    ];
  }
  if (active !== "all") searchObj.active = active === "active";
  if (type !== "all") searchObj.type = type;

  const masterData = await MasterData.find(searchObj)
    .limit(limit)
    .skip((parseInt(page) - 1) * limit)
    .lean();
  const masterDataCount = await MasterData.countDocuments(searchObj).exec();
  // // If no data
  if (masterDataCount === 0) {
    return res.status(400).json({ message: "No data found" });
  }

  res.json({ masterDataCount, masterData });

  // console.log({ searchObj, page, type });
};

// @desc Get master data with type
// @route GET /master-data/:type
// @access Private
const getMasterDataWithType = async (req, res) => {
  // Get all master data with from MongoDB
  const { type } = req.params;
  const masterData = await MasterData.find({ type, active: true })
    .sort({ displayOrder: 1, to: "desc" })
    .lean();

  // If no data
  if (!masterData) {
    return res.status(400).json({ message: "No data found" });
  }

  if (type === "Semester") {
    const currentDateTime = new Date();
    let currentSemester;
    for (let index = 0; index < masterData.length; index++) {
      const element = masterData[index];
      if (
        new Date(element.from) <= currentDateTime &&
        new Date(element.to) >= currentDateTime
      ) {
        currentSemester = element;
      }
    }
    return res.json({ masterData, currentSemester });
  } else {
    return res.json(masterData);
  }
};

// @desc Get master data with type
// @route POST /master-data
// @access Private
const createMasterData = async (req, res) => {
  const { type, code, name, description, displayOrder, from, to } = req.body;

  // Confirm data
  if (!code) {
    return res.status(400).json({ message: "code are required data" });
  }
  if (!type) {
    return res.status(400).json({ message: "type are required data" });
  }
  if (!name) {
    return res.status(400).json({ message: "name are required data" });
  }

  // validate data
  const duplicate = await MasterData.findOne({
    $or: [{ code }, { name }],
  })
    .collation({ locale: "en", strength: 2 })
    .lean();

  if (duplicate) {
    return res
      .status(409)
      .json({ message: "Duplicate master data code or name !" });
  }

  const masterDataObject = !description
    ? { type, code: code.trim(), name: name.trim() }
    : {
        type,
        code: code.trim(),
        name: name.trim(),
        description: description.trim(),
      };

  // console.log(displayOrder);

  !displayOrder
    ? (masterDataObject.displayOrder = 0)
    : (masterDataObject.displayOrder = displayOrder);

  masterDataObject.from = from;
  masterDataObject.to = to;

  const masterData = await MasterData.create(masterDataObject);

  if (masterData) {
    //created
    res.status(201).json({ message: `New master data created` });
  } else {
    res.status(400).json({ message: "Invalid user data received" });
  }
};

// @desc Get master data with type
// @route PATCH /master-data
// @access Private
const updateMaterData = async (req, res) => {
  const { id, name, active, description, displayOrder, from, to } = req.body;

  console.log(active);

  // Confirm data
  if (!id) {
    return res.status(400).json({ message: "Id are required" });
  }

  // Does the data exist to update?
  const masterData = await MasterData.findById(id).exec();

  if (!masterData) {
    return res.status(400).json({ message: "data not found" });
  }

  await MasterData.updateOne(
    { _id: masterData._id },
    {
      name: name.trim(),
      active,
      description: description ? description.trim() : null,
      from,
      to,
      displayOrder,
    }
  );

  res.json({ message: `${masterData?.code} updated` });
};

const deleteMasterDataByKeyword = async (req, res) => {
  const { keyword } = req.body;
  try {
    // Find documents that have any field containing the keyword
    const filter = {
      $or: [
        { type: { $regex: keyword, $options: "i" } },
        { code: { $regex: keyword, $options: "i" } },
        { name: { $regex: keyword, $options: "i" } },
        { description: { $regex: keyword, $options: "i" } },
      ],
    };

    // Delete the matching documents
    const result = await MasterData.deleteMany(filter);

    console.log(`documents deleted.`);
  } catch (error) {
    console.error("Error deleting documents:", error);
  }
};

module.exports = {
  getMasterData,
  getMasterDataWithSearch,
  getMasterDataWithType,
  createMasterData,
  updateMaterData,
  deleteMasterDataByKeyword,
};
